import React, {useLayoutEffect} from 'react';
import { Text, Button, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Props } from './src/routes/Route';

const TabDirectory: React.FC<Props<'TabDirectory'>> = ({ navigation }) => {

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  }, [navigation])


  const onPress = () =>
  {
    console.log("onPress");
  }

  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator>
    </Tab.Navigator>
  );
};

export default TabDirectory;