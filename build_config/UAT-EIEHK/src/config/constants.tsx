//HCRM
export const entityId = 'EIE';

export const loginSecret = '57b3e39762koid4e5a9c13398pof8&de9363';

export const allowSkipSMS = false;

export const applauncherURL = 'https://uat-ao-api.empfs.net/getmetadata.php';
export const applauncherBase = 'https://dbs6z606lrzqn.cloudfront.net/';
export const applauncherSecret = '78214125432A462D';
export const licenseKey = 'IOosg7FUbyp8GaopjWsn';
export const version = '1.0';
export const buildDate = '20221206'

export const appStoreLink = 'https://apps.apple.com/app/idyourappid'
export const playStoreLink = 'https://play.google.com/store/apps/details?id=com.yourpackagename'

export const iosCodePushDeploymentKey = 'dBn1OHQPE2e3u5l3ONwdUy6N3tB1s889Wzyh3'; //iOS Staging
export const androidCodePushDeploymentKey = 'Zw5xO0jGTCWzfep3WMruV3se-CKqaJn8WVfCo'; //Android Staging

// MIO
export const MIOAuthDomain = 'https://mio-uat-user.auth.ap-southeast-1.amazoncognito.com'
export const MIOClientId = '1flqcgoilamuppppk89f40ir2r'
export const MIOClientSecret = 'ankhfvrmdsrpmom3tifp70m04ulebjccia7pslct8l2pe7mp9r3'
export const MIOWebDomain = 'https://uat-mio-cp.empfs.net'
export const MIOUserTokenDomain = 'https://4n4tympo21.execute-api.ap-southeast-1.amazonaws.com/uat/encrypt'

export const captchaID = '82a29c2006278b0676ace7a4de2b20c2';

export const cognito_identityPoolId = '';
export const cognito_region = 'ap-southeast-1';
export const cognito_userPoolId = 'ap-southeast-1_IFXkofYMj';
export const cognito_userPoolWebClientId = '3rvebvb5jlnf2olk2eum0e3t5d';

export const percentageOrderSpread = 5;

export const dialogDisplaySecond = 2;
export const resendCodeSecond = 60;
export const refreshTokenMinutes = 60;

export const priceChangePercentageDP = 2;
export const chartPort = 18081;
export const timeoutMinutes = 30;

export const actionTimeoutMilliseconds = 10000;

export const certPinConfig = {
    'm-finance.net': {
        includeSubdomains: true,
        publicKeyHashes: [
            'oYQ5xsgKUUxSFxkR8nrDAArUyEBApzjlFJlE8URN5Gg=',
            '8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=',
            'Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=',
            'VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8='
        ],
    },
    'empfs.net': {
        includeSubdomains: true,
        publicKeyHashes: [
            'dpK8k8vMUj5tjDqlRCYoEL+0K61tcxEtodAlbTV/gEY=',
            '8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8='
        ],
    },
    'emxpro.net': {
        includeSubdomains: true,
        publicKeyHashes: [
            'b2Me4q8+lgAYblj8YvwkvroeSo6bfotiWz3nDIxocsg=',
            '8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=',
            'Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=',
            'VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8='
        ],
    },
}