import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path,Circle,Text,TSpan } from "react-native-svg"

const MeLogo= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={config.style?.width ?? 30}
          height={config.style?.height ?? 30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1663 -1014)">
              <Circle
                  data-name="Ellipse 79"
                  cx={13.636}
                  cy={13.636}
                  r={13.636}
                  transform="translate(1664.363 1015.364)"
                  fill={backgroundColor}
              />
              <Text
                  transform="translate(1664.997 1032.064)"
                  fontSize={9}
                  fill={color}
                  fontFamily="Montserrat-Regular, Montserrat"
              >
                  <TSpan x={0} y={0}>
                      {"LOGO"}
                  </TSpan>
              </Text>
              <Path
                  data-name="Rectangle 1011"
                  transform="translate(1663 1014)"
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeLogo
