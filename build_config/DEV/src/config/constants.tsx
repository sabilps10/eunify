export const appVersion = '1.0.0_DEV'
export const loginSecret = '57b3e39762koid4e5a9c13398pof8&de9363';

export const rsaPublicKey =
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu4GqYYjna8Y30PHYzlem
1istoIWkHu8i+q+VHKABbytwWQTEXFVYQOQaPMCyywA4GsEaMqReKnBuC3EC2Y9K
KpTtf1JNg7Vrldj0wclLIVA7nRp2R+Nu21e5zeBku44CAP8XwMERQcIRChWlKaoA
6pb8fe1sk4qMAk2/KN1Z6ZyyA2blsLFJHwgs6321AAcLSD4v4z28HOkTEnc8EsCB
31jx45SkPRk0L5d7o97x5bNNnhoqYOTCr3qiiGlb9Ita4CdVGhb+FWA8UAIitVB5
fv3n80t4xN9HKZoDgSdxpINuEtSFEGl1dMFkgQk8cMpGt1CcnVp7omtcE5U1CPjo
hwIDAQAB
-----END PUBLIC KEY-----`;

export const allowSkipSMS = true;

export const applauncherURL = 'https://ejapplauncher.mfcloud.net:2083/getmetadata.php';
export const applauncherSecret = '78214125432A462D';
export const licenseKey = 'yn0I8y7gHCT0QI8f5gGx'; //DEV
// export const licenseKey = 'VZVI0pBOBG0x7Sn2tcoe'; //QA
export const version = '1.0';
export const buildDate = '20221103a'

export const iosCodePushDeploymentKey = 'cC5dlZT_00TsJualS4plZpDyhZX878SqkGNf5'; //iOS Staging
export const androidCodePushDeploymentKey = 'w617FLZgaCo2skGEcilJgoWKCK8TR-Y4ZoZe1'; //Android Staging

export const captchaID = '82a29c2006278b0676ace7a4de2b20c2';

//MF
// export const cognito_identityPoolId = 'ap-southeast-1:2f1877b9-de33-454c-ab20-2c8683574b73';
// export const cognito_region = 'ap-southeast-1';
// export const cognito_userPoolId = 'ap-southeast-1_21arO7LuA';
// export const cognito_userPoolWebClientId = '5oo54eprlfticqdlgk22ken5ka';

// MF w/ EMP 2
export const cognito_identityPoolId = 'ap-southeast-1:2f1877b9-de33-454c-ab20-2c8683574b73';
export const cognito_region = 'ap-southeast-1';
export const cognito_userPoolId = 'ap-southeast-1_mlX1jmEOE';
export const cognito_userPoolWebClientId = '21cammh2s5pcviiv9jnapu8m4h';

// //MF w/ EMP 3
// export const cognito_identityPoolId = 'ap-southeast-1:2f1877b9-de33-454c-ab20-2c8683574b73';
// export const cognito_region = 'ap-southeast-1';
// export const cognito_userPoolId = 'ap-southeast-1_RWpKbjjRI';
// export const cognito_userPoolWebClientId = '7gfs1cgipin1lfodrm4drsdtop';


//EMP //QA
// export const cognito_identityPoolId = 'ap-southeast-1:3aa40950-836b-4eaf-b5b3-0e111e72a664';
// export const cognito_region = 'ap-southeast-1';
// export const cognito_userPoolId = 'ap-southeast-1_Ud2XLofKJ';
// export const cognito_userPoolWebClientId = '62ftmvu5itq9kmgl0l9n0vtjhh';

export const percentageOrderSpread = 5;

export const dialogDisplaySecond = 2;
export const resendCodeSecond = 60;
export const refreshTokenMinutes = 60;

export const priceChangePercentageDP = 2;
export const chartPort = 9000;