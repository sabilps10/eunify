# Mobile Config

1. Change LOGO photo<br />
    - Mobile (Need rebuild app & publish)<br />
		- React Native:<br />
			- /assets/icon/company_logo.png<br />
			- /assets/svg/MeLogo.tsx<br />
		- Android:<br />
			- /android/app/src/main/res/drawable/company_logo.png<br />
		- iOS:<br />
			- /ios/CopyMaster/Images.xcassets/company_logo.imageset<br />

2. Change Language<br />
	- Mobile (Code Push update)<br />
		- /src/locales/<br />
			- en.json<br />
			- sc.json<br />
			- tc.json<br />

3. Change Terms of Service content<br />
    - Mobile (Code Push update)<br />
        - /src/locales/<br />
			- terms_of_service_en.json<br />
			- terms_of_service_tc.json<br />

4. Mobile config<br />
	- constants.tsx<br />
		- entityId: Hardcoded mobile app entityId<br />
		- applauncherURL: Applauncher URL<br />
		- licenseKey: App License Key in Applauncher<br />
		- version: App version in Applauncher<br />
		- buildDate: Display in App Setting Version<br />
		- iosCodePushDeploymentKey & androidCodePushDeploymentKey: Code push key<br />
		- MIOAuthDomain MIOClientId MIOClientSecret MIOWebDomain MIOUserTokenDomain: MIO Values<br />
		- captchaID: GeeTest Captcha ID<br />
		- cognito_region cognito_userPoolId cognito_userPoolWebClientId: Cognito Values<br />
		- percentageOrderSpread: Order Page % Spread default value<br />
		- dialogDisplaySecond: Toast Dialog display second<br />
		- resendCodeSecond: Resend code count down second<br />
		- refreshTokenMinutes: App refresh WS connection minutes<br />
		- priceChangePercentageDP: Contract % change decimal place value<br />
		- timeoutMinutes: App session timeout minutes<br />
		
5. App Name<br />
	- Android: <br />
		- android/app/src/main/res/values/strings.xml<br />
			- app_name<br />
	- iOS: <br />
		- Open project with Xcode<br />
		- Open project navigator on left hand side<br />
		- Click project root and it will be open<br />
		- Select the target you would be change under TARGETS section<br />
		- Now you can change it under Identity-> Display name<br />

6. App Icon<br />
	- Android: <br />
		- android/app/src/main/res/mipmap-hdpi/ic_launcher.png<br />
		- android/app/src/main/res/mipmap-mdpi/ic_launcher.png<br />
		- android/app/src/main/res/mipmap-xhdpi/ic_launcher.png<br />
		- android/app/src/main/res/mipmap-xxhdpi/ic_launcher.png<br />
		- android/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png<br />
	- iOS:<br />
		- Open project with Xcode<br />
		- command+shift+o, search ‘.xcassets’ and open the appropriate one<br />
		- Looking for AppIcon or use the filter search ‘AppIcon’ in below<br />
		- Now you can change the icon with specify image size<br />

7. Cert Pinning config
	- Info.plist
		- Key: TSKConfiguration
			- Example with "m-finance.net" cert
		- Public key sha256 hash generate method: get_pin_from_certificate.py
			- Ref: https://prishanmaduka.medium.com/implementing-ssl-pinning-public-key-in-react-native-applications-in-native-way-74cbc991e & https://github.com/datatheorem/TrustKit
