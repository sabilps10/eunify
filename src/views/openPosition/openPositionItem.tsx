import React from 'react';
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Pressable,
} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';

import { useIsFocused } from '@react-navigation/native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles, LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { toCurrencyDisplay } from '../../utils/stringUtils'
import { useEffect } from 'react';
import { priceChangePercentageDP } from '../../config/constants';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { useTranslation } from 'react-i18next';
import { displayContractCode, productName, timestampToDateWithFormat } from '../utils/CommonFunction';
import ClosePosition from '../../../assets/svg/ClosePosition';
import Edit from '../../../assets/svg/Edit';
import ArrowRight from '../../../assets/svg/ArrowRight';
import StackView from '../../components/stackView.component';
import PressableComponent from '../utils/PressableComponent';

const OpenPositionItem = ({ isDetailsPage, item, onPressDetails, onPressClose, onPressEdit }: { isDetailsPage: boolean, item: string, onPressDetails: Function, onPressClose: Function, onPressEdit: Function }) => {
    
    const [t, i18n] = useTranslation();
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const dispatch = useDispatch();
    const { SetLastOrderId } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    const isFocused = useIsFocused();
    //console.debug("OpenPositionItem1: " + item + " " + isFocused);

    const openPosition = useSelector((state: State) => state.trading.positions && state.trading.positions[parseInt(item)]);

    const symbol = useSelector((state: State) => openPosition && state.trading.symbols && state.trading.symbols[openPosition.Symbol]);
    const islastOrder = useSelector((state: State) => {
        if (state.trading.lastOrderId && item === state.trading.lastOrderId)
        {
            return true;
        }
        else
        {
            return false;
        }
    });

    useEffect(()=> {
        if (islastOrder) {
            console.debug("SET Last ORder ID: " + item)
            setTimeout(function(){ 
                SetLastOrderId(undefined) }, 3000
            );
        }
    }, [islastOrder])

    const PriceView = () => {
        const positionCurrentPrice = useSelector((state: State) => {
            if (state.trading.prices[openPosition?.Symbol] && isFocused)
                if (openPosition.Type === 0)
                    return state.trading.prices[openPosition?.Symbol].Bid
                else if (openPosition.Type === 1)
                    return state.trading.prices[openPosition?.Symbol].Ask
        }); 
        if (positionCurrentPrice)
        {
            return (
                <View>
                    {
                        isDetailsPage ? 
                            <Text style={[globalStyles.Note]}>
                                {openPosition.OpenPrice.toFixed(symbol?.Digits)}
                            </Text> 
                        :
                            <Text style={[globalStyles.Note]}>
                                {openPosition.OpenPrice.toFixed(symbol?.Digits)}
                                <Text> &gt; </Text>
                                <Text>{positionCurrentPrice.toFixed(symbol?.Digits)}</Text>
                            </Text>
                    }
                    
                </View>
            )
        }
        else{
            return null;
        }
    }

    const PLView = () => {
        const positionCurrentPrice = useSelector((state: State) => {
            if (state.trading.prices[openPosition?.Symbol] && isFocused)
                if (openPosition.Type === 0)
                    return state.trading.prices[openPosition?.Symbol].Bid
                else if (openPosition.Type === 1)
                    return state.trading.prices[openPosition?.Symbol].Ask
        }); 

        const positionProfit = useSelector((state: State) => {
            if (state.trading.profits[parseInt(item)] !== undefined && isFocused)
                return state.trading.profits[parseInt(item)]
        });

        if (positionCurrentPrice)
        {
            const displayProfit = positionProfit >= 0 ? '+$' + toCurrencyDisplay(positionProfit) : '-$' + toCurrencyDisplay(Math.abs(positionProfit));
            const change = openPosition.Type === 0 ? ((positionCurrentPrice - openPosition.OpenPrice) / positionCurrentPrice * 100) : ((openPosition.OpenPrice - positionCurrentPrice) / openPosition.OpenPrice * 100);
            const displayChange = '(' + (change > 0 ? change.toFixed(priceChangePercentageDP) : change.toFixed(priceChangePercentageDP)) + '%)';
            return (
                <Text style={[globalStyles.Note, { color: positionProfit > 0 ? colors.Green : (positionProfit === 0 ? colors.MainFont : colors.Red) }]}>
                    {displayProfit + ' ' + displayChange}
                </Text>
            )
        }
        else{
            return null;
        }
    }

    return (
        <StackView 
            spacing={2} 
            style={[styles.listItemContainer, { borderColor: colors.WrapperBg, backgroundColor: islastOrder ? colors.Grey1 : colors.MainBg }]}>
            {/* {console.debug(`${new Date()}, openPositions: ${openPosition?.Symbol}, ${openPosition?.Ticket}`)} */}
            <View style={[styles.listHeaderItem, {flex: 0.35}]}>
                {
                    isDetailsPage ? 
                        <View>
                            <View style={styles.listItemLeftRow}>
                                <Text style={[globalStyles.Note, { color: openPosition.Type == 0 ? colors.Up : colors.Down }]}>
                                    {openPosition.Type == 0 ? t('trade.buy') : t('trade.sell')}
                                </Text>
                                <View style={{ width: 5 }} />
                                <Text style={[globalStyles.Note]}>
                                    {openPosition.Volume.toFixed(2)}
                                </Text>
                            </View>
                            <View style={{padding: 1}}/>
                            <Text style={[globalStyles.Tiny_Note]}>
                                {openPosition.OpenTime && timestampToDateWithFormat(openPosition.OpenTime, 'YYYY-MM-DD HH:mm:ss')}
                            </Text>
                        </View> 
                    :
                        <View>
                            <Text style={[globalStyles.Note, { color: colors.Text, fontSize: 10 }]}>
                                {productName(displayContractCode(symbol?.SymbolName ?? ''))}
                            </Text>
                            {/* <View style={{padding: 1}}/> */}
                            <Text style={[globalStyles.H4, { color: colors.Brand2 }]}>
                                {displayContractCode(openPosition.Symbol)}
                            </Text>
                            {/* <View style={{padding: 1}}/> */}
                            <Text style={[globalStyles.Tiny_Note]}>
                                {openPosition.OpenTime && timestampToDateWithFormat(openPosition.OpenTime, 'YYYY-MM-DD HH:mm:ss')}
                            </Text>
                        </View> 
                }
                
            </View>
            <View style={[styles.listHeaderItem, {flex: 0.35}]}>
                {
                    !isDetailsPage &&
                    <View style={styles.listItemLeftRow}>
                        <Text style={[globalStyles.Note, { color: openPosition.Type == 0 ? colors.Up : colors.Down }]}>
                            {openPosition.Type == 0 ? t('trade.buy') : t('trade.sell')}
                        </Text>
                        <View style={{ width: 5 }} />
                        <Text style={[globalStyles.Note]}>
                            {openPosition.Volume.toFixed(2)}
                        </Text>
                    </View>
                }
                <View style={{padding: 1}}/>
                <PriceView />
                <View style={{padding: 1}}/>
                <PLView />
            </View>
           
            <View style={[styles.listHeaderItemRight, {flex: 0.30, height: '100%'}]}>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={()=>{onPressClose(openPosition.Ticket)}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ClosePosition color={colors.Brand3} />
                    </View>
                </PressableComponent>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={()=>{onPressEdit(openPosition.Ticket)}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Edit color={colors.Brand3} />
                    </View>
                </PressableComponent>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={() => {onPressDetails(openPosition.Ticket)}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ArrowRight color={colors.Brand3} />
                    </View>
                    
                </PressableComponent>
            </View> 
        </StackView>
    )
};

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
    input: {
        width: 200,
        borderWidth: 1,
        borderColor: '#555',
        borderRadius: 5,
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 10,
    },
    mediaItemTxt: {
        color: '#333333',
        fontSize: 13,
        marginTop: 4,
    },
    listHeaderItem: {
        flex: 0.38,
    },
    listHeaderItemRight: {
        flex: 0.24,
        flexDirection: 'row'
    },
    listHeaderItemRightButton: {
        flex: 1,
        // backgroundColor: '#555555', 
        alignItems:'center',
        alignSelf:'center',
        marginStart: 5
    },
    ItemTxt: {
        color: '#333333',
        fontSize: 13,
        marginBottom: 10,
        marginTop: 10,
        marginRight: 5,
        marginLeft: 5,
    },
    container: {
        //flex: 1,
        width: '100%',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
    },
    rightSwipeContainer: {
        width: 211,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    swipeContainerPriceBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    listItemContainer: {
        width: '100%',
        borderTopWidth: 1,
        // height: 72,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        paddingTop: 7,
        paddingBottom: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    listItemLeftRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default React.memo(OpenPositionItem);
