import React, { useContext, useEffect, useMemo, useState } from 'react';
import { FlatList, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { useTheme } from '@react-navigation/native';
import commonStyles, { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';

import OpenPositionItem from './openPositionItem';

import { useTranslation } from 'react-i18next';
import { SortingType } from '../portfolio/portfolio';

// import { Tabs } from 'react-native-collapsible-tab-view'
import { displayContractCode } from '../utils/CommonFunction';

import { Segmented, useIsFocused } from 'react-native-collapsible-segmented-view'
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { PortfolioTabType } from '../../redux/state/types';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';


const OpenPosition = ({ isDetailsPage, isInCollapsibleTab, symbol, onPressDetails, onPressClose, onPressEdit }: { isDetailsPage: boolean, isInCollapsibleTab: boolean, symbol: string[], onPressDetails: Function, onPressClose: Function, onPressEdit: Function }) => {
    const [t, i18n] = useTranslation();

    console.debug("OpenPosition: " + symbol)

    const { colors } = useTheme();

    const selectedSortingType = useSelector((state: State) => state.selection.selected?.portfolioSortingType);
    // const onTouch = useSelector((state: State) => state.state.onTouch);

    const openPositionsKey: number[] = useSelector((state: State) => {
        if (state.trading.positions) {
            var filterList = Object.values(state.trading.positions);
            if (symbol && !symbol.includes(t('historyDealQuery.all'))) {
                filterList = filterList.filter(x => symbol.includes(x.Symbol))
            }

            var sortList = filterList.sort((a, b) => b.OpenTime - a.OpenTime)
            if (selectedSortingType === SortingType.DateDesc) {
                //Default sorting
            } else if (selectedSortingType === SortingType.DateAsc) {
                sortList = filterList.sort((a, b) => a.OpenTime - b.OpenTime)
            } else {
                var profits = state.trading.profits
                if (selectedSortingType === SortingType.ProfitDesc) {
                    sortList = filterList.sort((a, b) => profits[b.Ticket] - profits[a.Ticket])
                } else if (selectedSortingType === SortingType.ProfitAsc) {
                    sortList = filterList.sort((a, b) => profits[a.Ticket] - profits[b.Ticket])
                }
            }

            return sortList.map(a => a.Ticket);
        }
    }, shallowEqual);

    const renderItem = ({ item }) => (
        <OpenPositionItem
            isDetailsPage={isDetailsPage}
            item={item.toString()}
            onPressClose={onPressClose}
            onPressEdit={onPressEdit}
            onPressDetails={() => onPressDetails(item)} />
    )

    const isFocused = isInCollapsibleTab ? useIsFocused() : false
    const dispatch = useDispatch();
    const { SetPortfolioCurrentTab, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSelected } = bindActionCreators(SelectionActionCreators, dispatch);
    useEffect(()=>{
        //console.log('OpenPosition isFocused', isFocused)
        if (isFocused) {
            SetPortfolioCurrentTab(PortfolioTabType.Position)
        }
    }, [isFocused])

    return (
        isInCollapsibleTab ?
                <Segmented.FlatList
                    style={{backgroundColor: colors.MainBg}}
                    initialNumToRender={5}
                    windowSize={5}
                    data={openPositionsKey}
                    keyExtractor={item => item}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    renderItem={renderItem}
                    removeClippedSubviews={true}
                    // onTouchStart={()=>{SetOnTouch(!onTouch)}}
                />
        :
            <View style={[commonStyles.container, { backgroundColor: colors.MainBg }]}>
                <View style={{ flex: 1 }}>
                    <FlatList
                        initialNumToRender={5}
                        windowSize={5}
                        data={openPositionsKey}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        renderItem={renderItem}
                        removeClippedSubviews={true}
                        // onTouchStart={()=>{SetOnTouch(!onTouch)}}
                    />
                </View>
            </View>
    );
}

export default React.memo(OpenPosition);