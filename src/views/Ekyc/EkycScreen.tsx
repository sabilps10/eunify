import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import Sumsub from '../../components/Sumsub';

interface EkycScreenProps {
  navigation?: any;
}

const AccountEkyc: React.FC<EkycScreenProps> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>EKYC</Text>
      <Sumsub navigation={navigation} />
    </View>
  );
};

export default AccountEkyc;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 8,
  },
  button: {
    margin: 24,
  },
});
