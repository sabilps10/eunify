import React, { useLayoutEffect } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import { Props } from '../../routes/route'

import commonStyles, { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import TextLabel from '../../components/forms/labels/textLabel.component';
import {
  UIActivityIndicator
} from 'react-native-indicators';


const CheckUpdate: React.FC<Props<'CheckUpdate'>> = ({ route, navigation }) => {

  const [t, i18n] = useTranslation();
  const dispatch = useDispatch();
  const { SetSetting } = bindActionCreators(StateActionCreators, dispatch);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)

  return (
    <View style={styles.container} >
      <View style={{alignContent: 'center'}}>
        {/* <TextLabel label={t('common.checkingUpdate')} labelStyle={[globalStyles.H2, {color: colors.White}]} /> */}
        <UIActivityIndicator color={colors.Grey3} size={60}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0ed4df',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
  },
});

export default CheckUpdate;