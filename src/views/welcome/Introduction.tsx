import React, { useState, useContext, useEffect, useLayoutEffect } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Alert,
  Image,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { Props } from '../../routes/route'

import commonStyles from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { useSwipe } from '../utils/useSwipe';
import { useTranslation } from 'react-i18next';
import BigButton from '../../components/buttons/bigButton.component';
import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import {
  getImage_intro
} from '../../../assets/constants';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { SafeAreaView } from 'react-native-safe-area-context';
import SmallButton from '../../components/buttons/smallButton.component';


const Introduction: React.FC<Props<'Introduction'>> = ({ route, navigation }) => {

  const [t, i18n] = useTranslation();
  const dispatch = useDispatch();
  const { SetSetting } = bindActionCreators(StateActionCreators, dispatch);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  const { colors } = useTheme();
  const [showButton, SetShowButton] = useState<boolean>(false);
  const [currPage, SetCurrPage] = useState<number>(0);
  const introPage = 3;

  const { onTouchStart, onTouchEnd } = useSwipe(onSwipeLeft, onSwipeRight, 6);

  function onSwipeLeft() {
    var page = currPage + 1;
    if (page < introPage) {
      updatePage(page)
    }
  }

  function onSwipeRight() {
    var page = currPage - 1;
    if (page >= 0) {
      updatePage(page)
    }
  }

  const updatePage = (page: number) => {
    SetCurrPage(page);
    if (page === introPage - 1) {
      SetShowButton(true);
    } else {
      SetShowButton(false);
    }
  }

  const toMainMenu = async () => {
    SetSetting({
      IsFirstTime: false
    })
    navigation.navigate('TabDirectory')
  }

  const toLogin = async () => {
    navigation.navigate('CognitoLogin', {backPage: 'Introduction'})
  }

  const toSignup = async () => {
    navigation.navigate('CognitoSignUp')
  }

  console.log(`Dimensions.get('screen'): ${Dimensions.get('screen').height}; Dimensions.get('window'): ${Dimensions.get('window').height}`)

  return (
    <ImageBackground source={getImage_intro(Number(currPage), i18n.language)} resizeMode="cover" style={
      Platform.OS === 'ios' ? {
        overflow: "hidden", position: "absolute", top: 0, width: Dimensions.get('window').width, height: Dimensions.get('window').height 
      } : {
        flex: 1, justifyContent: 'center', height: Platform.OS === 'android' ? Dimensions.get('screen').height : null, overflow: "visible"
        // width: "100%", height: "100%", position: "absolute", top: 0, overflow: "visible" // s22 with arrow button navbar 732 height, with swipe gesture navbar 765 height, window dimension height 705, screen dimension height 780
      }} imageStyle={{top: Platform.OS === 'ios' ? (1074/486*Dimensions.get('window').width-Dimensions.get('window').height)/2 : 0, overflow: "visible"}}>

      <SafeAreaView
        edges={['bottom']}  
        style={[commonStyles.container, { /*backgroundColor: "#0ed4df"*/ }]} 
        onTouchStart={onTouchStart} 
        onTouchEnd={onTouchEnd}>

          <View style={[commonStyles.bottomContainer, { marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>

            <View style={{ height: 10 }} />
            {
              showButton ?
                <BigButton title={t('welcome.login')} type='Outline' onPress={toLogin} style={{ borderColor: colors.fixedBorder, backgroundColor: "#0ed4dfdd" }} textStyle={{ color: colors.fixedText }} />
                :
                <View style={{ height: 50 }} />
            }
            <View style={{ height: 10 }} />
            {
              showButton ?
                <BigButton title={t('welcome.signupNow')} type='Solid' onPress={toSignup} />
                :
                <View style={{ height: 50 }} />
            }
            <View style={{ height: 10 }} />
            {
              showButton ? //<></>
                <SmallButton title={t('welcome.signupLater')} type='White' onPress={toMainMenu} style={{ borderColor: colors.fixedBorder }} textStyle={{ color: colors.fixedText }} />
                :
                <View style={{ height: 50 }} />
            }
            <View style={{ height: 10 }} />
          </View>

      </SafeAreaView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  upperContainer: {
    paddingTop: 100,
    paddingBottom: 30
  },
  lowerContainer: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    flex: 1
  },
});

export default Introduction;