import React, { useState, useContext, useCallback, useMemo, useEffect, useLayoutEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import {
  StyleSheet,
  useColorScheme,
  View,
  Alert,
  Image,
  BackHandler,
  ScrollView
} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import TextLabel from '../../components/forms/labels/textLabel.component';
import BigButton from '../../components/buttons/bigButton.component';
import Region from '../../components/region/region.component';

import commonStyles, { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import {
  getIcon_companyLogo
} from '../../../assets/constants';
import {
  getIcon_companyLogo2
} from '../../../assets/constants';
import { StateActionCreators } from '../../redux/state/actionCreator';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import { getTranslationDict } from '../utils/CommonFunction';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';

import { CountryOptionType } from '../../components/countryOptionView/countryOptionView';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getApplicationName } from 'react-native-device-info';
import { format } from 'react-string-format';
import { SelectionContextLocation } from '../../redux/selection/type';
import { entityId } from '../../config/constants';

const Welcome: React.FC<Props<'Welcome'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();
  const dispatch = useDispatch();
  const { SetSetting } = bindActionCreators(StateActionCreators, dispatch);
  const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);

  const userLanguage = useSelector((state: State) => state.state.setting?.UserLanguage);
  const regionDataList = useSelector((state: State) => state.state.regionDataList);
  const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

  const languageOption = ["en", "tc", "sc"];

  const [selectedRegion, setSelectedRegion] = useState<string>('');
  const selectedLanguage = useSelector((state: State) => state.selection.selected?.settingLanguage?? i18n.language );

  console.debug(`Welcome: ${selectedLanguage}`);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)
  const scheme = useColorScheme();

  //On View Enter
  useEffect(() => {
    setSelectedRegion(regionDataList && regionDataList[0] && regionDataList[0].regionCode);
  }, [regionDataList]);

  useEffect(() => {
    if (selectedLanguage)
    {
        SetSetting({
            UserLanguage: selectedLanguage
        })
        
        i18n.changeLanguage(selectedLanguage);
    }
}, [selectedLanguage]);


  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
        Alert.alert("", t('common.exitApp'), [
          {
            text: t('common.cancel'),
            onPress: () => null,
            style: "cancel"
          },
          { text: t('common.yes'), onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };

      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );

      return () => {
        backHandler.remove();
      }
    }, [])
  );

  const onPressAgree = () => {
    SetSetting({
      UserRegion: selectedRegion,
      UserLanguage: selectedLanguage
    })
    navigation.navigate('Introduction');
  }

  const onSelectRegionOption = (value: string) => {
    setSelectedRegion(value);
  }

  const onPressLanguage = () => {
    OpenCloseSelection({
        selectedOption: userLanguage ?? "",
        selectOptions: getTranslationDict(languageOption, "language", t),
        isOpen: !isOpen,
        location: SelectionContextLocation.SettingLanguage
    })
  }

  return (
    <SafeAreaView
      style={[commonStyles.container, {backgroundColor: colors.White}]}
      edges={['bottom']}>
      <View style={[styles.upperContainer, { backgroundColor: colors.fixedBackground }]}>
        <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
          <View style={[styles.companyLogoView]}>
            <Image style={styles.companyLogo} source={getIcon_companyLogo2(scheme)} />
          </View>
          <TextLabel label={format(t(`welcome.title_${entityId}`), getApplicationName())} labelStyle={[globalStyles.H2, {color: colors.White}]} />
        </View>
      </View>
      <View style={[commonStyles.container, { backgroundColor: colors.fixedBackground }]}>
        <View style={[styles.lowerContainer, {backgroundColor: colors.White}]}>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
            <TextLabel label={t('welcome.termsTitle')} labelStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
            <ScrollView>
              <TextLabel label={t(`welcome.terms_${entityId}`)} />
            </ScrollView>
          </View>
        </View>
      </View>
      <View style={[styles.bottomContainer, { paddingHorizontal: LAYOUT_PADDING_HORIZONTAL, backgroundColor: colors.White }]}>
        <View style={{ flexDirection: 'row', paddingVertical: 15 }}>
          <MSmallButton component={<Region region={selectedRegion} />} type='White' textStyle={globalStyles.Note} 
            onPress={() => navigation.navigate('CountryOptionView', {type: CountryOptionType.REGION, onSelect: onSelectRegionOption, selectedId: selectedRegion})} 
          />
          <View style={{ width: 20 }} />
          <MSmallButton title={t(`language.${selectedLanguage}`)} type='White' textStyle={globalStyles.Note} onPress={onPressLanguage} />
        </View>
        <BigButton
          type='Solid'
          title={t('common.agree')}
          onPress={onPressAgree}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  upperContainer: {
    paddingTop: 50,
    paddingBottom: 40,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  lowerContainer: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    paddingTop: 30,
    flex: 1
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 10
  },
  companyLogo: {
    height: 40,
    width: 43
  },
  companyLogoView: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 100,
  },
});

export default Welcome;