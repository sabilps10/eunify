import React, { useLayoutEffect } from 'react';
import {
  View,
  Text
} from 'react-native';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import commonStyles, { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { ScrollView } from 'react-native-gesture-handler';
import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { entityId } from '../../config/constants';

const TermsOfService: React.FC<Props<'TermsOfService'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation("terms");
  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () =>
        <Header
          leftComponents={<HeaderButton 
            iconComponent={<TopBarBack color={colors.MainFont} />} 
            title={t('title')} 
            titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
            onPress={onPressBackBtn} />}
        />
    });
  }, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }
  
  return (
    <ScrollView
      style={{backgroundColor: colors.MainBg}}
      contentContainerStyle={commonStyles.ScrollViewContainerStyle}
      showsVerticalScrollIndicator={false}>
      <View style={commonStyles.contentContainer}>
        <View style={commonStyles.inputPadding} />
        <Text style={[globalStyles.Note, {color: colors.MainFont}]}>{t(`content_${entityId}`)}</Text>
      </View>
      
    </ScrollView>
  );
};

export default TermsOfService;