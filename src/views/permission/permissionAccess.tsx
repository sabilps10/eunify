import React, { useState, useLayoutEffect } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Image,
  Linking
} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import TextLabel from '../../components/forms/labels/textLabel.component';
import ActionButton from '../../components/buttons/actionButton.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';

import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';


import { StateActionCreators } from '../../redux/state/actionCreator';
import Header from '../../components/header/header.component';
import { lv3Registration } from '../../utils/api/userApi';
import BigButton from '../../components/buttons/bigButton.component';
import { AccountUtils } from '../../utils/accountUtils';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { SafeAreaView } from 'react-native-safe-area-context';
import TopBarBack from '../../../assets/svg/TopBarBack';
import HeaderButton from '../../components/header/headerButton.component';
import ErrorLogin from '../../../assets/svg/ErrorLogin';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { ActionUtils } from '../utils/ActionUtils';
import { MessageDialogContent } from '../../redux/state/types'
import { aoEntry } from '../../utils/navigation';

export enum PermissionAccessType {
  member_only = 0,
  real_account_only = 1,
  lv2_no_demo = 2,
  lv2_has_demo = 3
}

const PermissionAccess: React.FC<Props<'PermissionAccess'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();
  const { type } = route.params;

  const dispatch = useDispatch();
  const { SetSetting, SetIsShowMessageDialog, SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);
  const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

  const {individual} = useSelector((state: State) => state.accountOpening);

  const { onPressRegisterDemo } = ActionUtils();

  useLayoutEffect(() => {
    navigation.setOptions({
        header: () => <Header 
          leftComponents={<HeaderButton 
              iconComponent={<TopBarBack color={colors.MainFont} />} 
              onPress={onPressBackBtn} />}
        />
    });
}, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)
  const scheme = useColorScheme();


  const onPressSmallButton = () => {
    if (type === PermissionAccessType.member_only) {
      navigation.navigate('CognitoLogin', {backPage: 'PermissionAccess', backPageParam: route.params})
    } else {
      onPressBackBtn();
    }
  }

  const onPressButton = () => {
    if (type === PermissionAccessType.member_only) {
      navigation.navigate('CognitoSignUp')
    } else {
      aoEntry(individual, navigation);
    }
  }

  const successRegisterDemoRoute = () => {
    navigation.navigate('TabDirectory', {screen: TabDirectoryTab.QUOTE})
  }

  const onPressActivateDemo = () => {
    SetIsShowMessageDialog({isShowDialog: true, trackingContext: MessageDialogContent.TrackingContext.ActivateDemoTrade, title: t('demo.activateTitle'), message: t('demo.activateDesc'), callback: () => onPressRegisterDemo(successRegisterDemoRoute), isShowCancel: true, btnLabel: t('demo.activateButton')})
  }

  const buttonComponent = () => {
    if (type === PermissionAccessType.member_only ) {
      return (
        <View style={[styles.bottomContainer, { marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
          <View style={{ flexDirection: 'row' }}>
            <MSmallButton title={t('permissionAccess.login')} type='White' textStyle={globalStyles.Note} onPress={onPressSmallButton} />
          </View>
          <ActionButton
            title={t('permissionAccess.signupNow')}
            onPress={onPressButton}
          />
        </View>
      )
    } else if (type === PermissionAccessType.lv2_no_demo ) {
      return (
        <View style={[styles.bottomContainer, { backgroundColor: colors.White, marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
          <BigButton title={t('permissionAccess.activateDemoTrade')} type='Outline' onPress={onPressActivateDemo} />
          <ActionButton
            title={t('permissionAccess.openRealAccount')}
            onPress={onPressButton}
          />
        </View>
      )
    } else if (type === PermissionAccessType.lv2_has_demo ) {
      return (
        <View style={[styles.bottomContainer, { backgroundColor: colors.White, marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
          <ActionButton
            title={t('permissionAccess.openRealAccount')}
            onPress={onPressButton}
          />
        </View>
      )
    } else {
      return (
        <View style={[styles.bottomContainer, { backgroundColor: colors.White, marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
          <View style={{ flexDirection: 'row' }}>
            <MSmallButton title={t('permissionAccess.openLater')} type='White' textStyle={globalStyles.Note} onPress={onPressSmallButton} />
          </View>
          <ActionButton
            title={t('permissionAccess.openRealAccount')}
            onPress={onPressButton}
          />
        </View>
      )
    }
}
  return (
    <SafeAreaView 
      edges={['bottom']}
      style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg }]}>
      <View style={[styles.container]}>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
            <View style={styles.centerWarningContainer}>
                <ErrorLogin color={colors.Red} />
                <View style={{padding: 5}}/>
                <TextLabel label={type === PermissionAccessType.lv2_has_demo || type === PermissionAccessType.lv2_no_demo ? t('permissionAccess.liveOnlyTitle') : (type === PermissionAccessType.member_only ? t('permissionAccess.memberOnlyTitle') : t('permissionAccess.realOnlyTitle'))} labelStyle={[globalStyles.H4, { color: colors.Brand2, textAlign: 'center', marginHorizontal: 100 }]} />
                <TextLabel label={type === PermissionAccessType.lv2_has_demo || type === PermissionAccessType.lv2_no_demo  ? t('permissionAccess.liveOnlyDesc') : (type === PermissionAccessType.member_only ? t('permissionAccess.memberOnlyDesc') : t('permissionAccess.realOnlyDesc'))} />
            </View>
          </View>
      </View>
      {buttonComponent()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  centerWarningContainer: { 
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 10
  },
  row: {
    flexDirection: 'row'
  },
  companyLogo: {
    height: 40,
    width: 40
  },
  companyLogoView: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 100,
  },
});

export default PermissionAccess;