import React, { useState, useContext, useCallback, useMemo, useEffect, useLayoutEffect, useRef } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Alert,
  Pressable,
  Image,
  Text,
  ScrollView
} from 'react-native';

import { useTheme } from '@react-navigation/native';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { makeGlobalStyles } from '../../styles/styles'

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import Header from '../../components/header/header.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { formatDate, isEmpty } from '../../utils/stringUtils'

import { DetailsType, getConvertPrice, getEstMargin, OrderActionType, PendingOrderBuySellType } from '../../utils/orderUtils'
import { toCurrencyDisplay } from '../../utils/stringUtils'
import ClosePosition from '../trade/closePosition';
import { priceChangePercentageDP } from '../../config/constants';
import DashedLineView from '../../components/dashedLineView.component';
import ActionButtons from '../../components/buttons/actionButtons.component';
import { WebsocketUtils } from '../../utils/websocketUtils';
import OrderSlider from '../../components/orderSlider.component';
import OrderConfirmDialog from '../../components/dialogs/orderConfirmDialog';
import { displayContractCode, productName, timestampToDateWithFormat } from '../utils/CommonFunction';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import ContractValueComponent from '../trade/component/contractValue.component';
import MarginValueComponent from '../trade/component/marginValue.component';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import ContractStatusPending from '../../../assets/svg/ContractStatusPending';
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import { OpenPositionContent } from '../../redux/trading/type';
import VerifyCodeTick from '../../../assets/svg/VerifyCodeTick';
import ContractStatusOpen from '../../../assets/svg/ContractStatusOpen';
import ContractStatusClosed from '../../../assets/svg/ContractStatusClosed';
import Share from '../../../assets/svg/Share';
import TouchableOpacityComponent from '../utils/TouchableOpacityComponent';
import { TradingResponseCode } from '../../utils/errorUtil';

const PositionDetails: React.FC<Props<'PositionDetails'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();

  const { type, ticketId } = route.params;

  const { colors } = useTheme();
  const scheme = useColorScheme();
  const globalStyles = makeGlobalStyles(colors)

  const [symbol, SetSymbol] = useState<string>(' ');
  const [isBuy, SetIsBuy] = useState<boolean>(false);
  const [volume, SetVolume] = useState<number>(0);
  const [amount, SetAmount] = useState<number>(0);
  const [amountCurrency, SetAmountCurrency] = useState<string>('USD');
  const [price, SetPrice] = useState<number>(0);
  const [marginRate, SetMarginRate] = useState<number>(0)
  const [digit, SetDigit] = useState<number>(0);
  const [pl, SetPL] = useState<number>(0);
  const [tpPrice, SetTPPrice] = useState<number>(0);
  const [slPrice, SetSLPrice] = useState<number>(0);
  const [interest, SetInterest] = useState<number>(0);
  const [commission, SetCommission] = useState<number>(0);
  const [openTime, SetOpenTime] = useState<string>(' ');
  const [closeTime, SetCloseTime] = useState<string>(' ');
  const [transactionId, SetTransactionId] = useState<string>(' ');
  const [openPrice, SetOpenPrice] = useState<number>(0);
  const [referencePrice, SetReferencePrice] = useState<number>(0);
  const [orderType, SetOrderType] = useState<string>(' ');
  const [validity, SetValidity] = useState<string>(' ');

  const login = useSelector((state: State) => state.trading.accountDetail?.Login?.toString() ?? '-')
  const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
  const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
  const position: OpenPositionContent | null = useSelector((state: State) => type === DetailsType.NEW_POSITION && state.trading.positions && state.trading.positions[ticketId], shallowEqual);
  const positionProfit = useSelector((state: State) => type === DetailsType.NEW_POSITION && state.trading.profits && state.trading.profits[ticketId]);
  const closedPosition = useSelector((state: State) => type === DetailsType.CLOSE_POSITION && state.trading.historyDeal && state.trading.historyDeal[ticketId], shallowEqual);
  const order = useSelector((state: State) => type === DetailsType.PENDING_ORDER && state.trading.orders && state.trading.orders[ticketId], shallowEqual);
  const currentPrice = useSelector((state: State) => type === DetailsType.NEW_POSITION  && state.trading.prices && state.trading.prices[position && position.Symbol]);

  const contractSymbols = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[symbol]! : undefined);
  // const contractPrices = useSelector((state: State) => (type === DetailsType.NEW_POSITION || type === DetailsType.PENDING_ORDER ) && state.trading.prices);
  // const contractPrice = useSelector((state: State) => type === DetailsType.NEW_POSITION && state.trading.prices ? state.trading.prices[symbol] : undefined);
  const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();
  const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);

  const [isShowOrderDialog, SetIsShowOrderDialog] = useState<boolean>(false);

  const dispatch = useDispatch();
  const {  SetIsShowOrderConfirmDialog, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () =>
        <Header
          leftComponents={<HeaderButton 
                            iconComponent={<TopBarBack color={colors.MainFont} />} 
                            title={type === DetailsType.NEW_POSITION || type === DetailsType.CLOSE_POSITION ? t('positionDetails.positiontitle') : t('positionDetails.ordertitle')}
                            titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                            onPress={onPressBackBtn} />}
        />
    });
  }, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }

  useEffect(()=>{
    if (type === DetailsType.CLOSE_POSITION && closedPosition){
      SetSymbol(closedPosition.Symbol)
      SetIsBuy(closedPosition.Type === 0)
      SetVolume(closedPosition.Volume)
      SetPL(closedPosition.Profit)
      SetInterest(closedPosition.Swaps)
      SetCommission(closedPosition.Commission)
      SetOpenTime(timestampToDateWithFormat(closedPosition.OpenTime, 'YYYY-MM-DD HH:mm:ss'))
      SetCloseTime(timestampToDateWithFormat(closedPosition.CloseTime, 'YYYY-MM-DD HH:mm:ss'))
      SetOpenPrice(closedPosition.OpenPrice)
      SetReferencePrice(closedPosition.ClosedPrice)
      SetPrice(closedPosition.ClosedPrice)
      SetTPPrice(closedPosition.TP)
      SetSLPrice(closedPosition.SL)
      SetTransactionId(closedPosition.Ticket)
      
      console.debug('CLOSE_POSITION 1', contractSymbols, closedPosition.Symbol)
      if (contractSymbols) {
        console.debug('CLOSE_POSITION 2', closedPosition.Volume , contractSymbols.ContractSize , closedPosition.OpenPrice)
        SetAmount(closedPosition.Volume * contractSymbols.ContractSize * closedPosition.OpenPrice)
        SetDigit(contractSymbols.Digits)
      }
    }
    else if (type === DetailsType.NEW_POSITION && position){
      // console.debug('NEW_POSITION')
      SetSymbol(position.Symbol)
      SetIsBuy(position.Type === 0)
      SetVolume(position.Volume)
      SetPL(positionProfit)
      SetInterest(position.Swap)
      SetCommission(position.Commission)
      SetOpenTime(timestampToDateWithFormat(position.OpenTime, 'YYYY-MM-DD HH:mm:ss'))
      SetPrice(position.OpenPrice)
      SetMarginRate(position.MarginRate)
      SetTPPrice(position.TP)
      SetSLPrice(position.SL)
      SetOpenPrice(position.OpenPrice)
      SetTransactionId(position.Ticket.toString())
      SetReferencePrice(position.Type === 0 ? currentPrice.Bid : currentPrice.Ask)
      
      if (contractSymbols) {
        SetAmount(position.Volume * contractSymbols.ContractSize * position.OpenPrice)
        SetDigit(contractSymbols.Digits)
      }
    }
    else if (type === DetailsType.PENDING_ORDER && order){
      console.debug('PENDING_ORDER')
      SetSymbol(order.Symbol)
      SetIsBuy(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.BUY_STOP)
      SetVolume(order.RemainVolume)
      SetPrice(order.PriceOrder)
      SetMarginRate(position.Type === 0 ? currentPrice.Bid : currentPrice.Ask)
      SetTPPrice(order.TP)
      SetSLPrice(order.SL)
      SetTransactionId(order.Ticket.toString())
      var sOrderType = '';
      if (order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.SELL_LIMIT) {
        sOrderType = t('positionDetails.limit')
      } else if (order.Type === PendingOrderBuySellType.BUY_STOP || order.Type === PendingOrderBuySellType.SELL_STOP) {
        sOrderType = t('positionDetails.stop')
      }
      SetOrderType(sOrderType)
      SetOpenTime(timestampToDateWithFormat(order.TimeSetup, 'YYYY-MM-DD HH:mm:ss'))
      SetValidity(order.TimeExpiration === 0 ? t('trade.gtc') : t('trade.today'))

      if (contractSymbols) {
        SetAmount(order.RemainVolume * contractSymbols.ContractSize * order.PriceOrder)
        SetDigit(contractSymbols.Digits)

        // var convertPrice = getConvertPrice(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.BUY_STOP, contractSymbols, contractPrices);
        
        // var marginValue =  getEstMargin(contractSymbols, order.RemainVolume, order.PriceOrder, accountLeverage, convertPrice);
        // SetRequiredMargin(marginValue)
      }
    }

  }, [closedPosition, position, order, contractSymbols, currentPrice])

  const Status = () => 
  {
    // Default Pending
    let source = <ContractStatusPending color={colors.Brand2} />
    let title = t('positionDetails.pending')
    let textColor = colors.Brand2

    if (type === DetailsType.CLOSE_POSITION) {
      source = <ContractStatusClosed color={colors.Brand4} />
      title = t('positionDetails.close')
      textColor = colors.Green
    } 
    else if (type === DetailsType.NEW_POSITION) {
      source = <ContractStatusOpen color={colors.Brand3} />
      title = t('positionDetails.open')
      textColor = colors.Brand3
    }
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
        {source}
        <View style={{ width: 5 }} />
          <Text style={[globalStyles.Body_Text_B, {color: textColor}]}>
            {title}
          </Text>
      </View>
    )
  }

  const BuySell = () => 
  {
    return (<Text style={[globalStyles.Body_Text_B, isBuy ? {color: colors.Green} : {color: colors.Red}]}>{isBuy ? t('trade.buy') : t('trade.sell')}</Text>)
  }

  const PL = () => 
  {
    const displayProfit = pl > 0 ? '+' + toCurrencyDisplay(pl) : toCurrencyDisplay(pl);
    const change = (isBuy ? (referencePrice - openPrice) : (openPrice - referencePrice)) / openPrice * 100;
    const displayChange = '(' + (change > 0 ? '+' + change.toFixed(priceChangePercentageDP) : change.toFixed(priceChangePercentageDP)) + '%)';
    return (<Text
      style={[
          globalStyles.Body_Text_B,
          { color: pl > 0 ? colors.Green : colors.Red },
      ]}>
      {displayProfit + ' ' + displayChange}
    </Text>)
  }

  const Lot = () => 
  {
    var vDP = [''];
    if (contractSymbols) {
      vDP = contractSymbols!.VolumeStep.toString().split('.');
    }
    var volumeDP = 0
    if (vDP.length > 1){
        volumeDP = vDP[1].length;
    }
    return (
      <Text style={[globalStyles.Body_Text_B]}>{volume.toFixed(volumeDP)} {t('confirmTrade.lots')}</Text>
    )
  }

  const editPosition = () => {
    if (position) {
      navigation.navigate('EditPosition', {ticketId: position.Ticket})
    }
  }

  const closePosition = () => {
    if (position) {
      navigation.navigate('ClosePosition', {ticketId: position.Ticket})
    }
  }

  const modifyOrder = () => {
    if (order) {
      navigation.navigate('NewOrder', {symbolName: ' ', isBuy: false, transactionId: order.Ticket})
    }
  }

  const cancelOrder = async() => {
    if (order) {
      if (!isEnableTradeConfirmationDialog) {
        var result = await startDeletePendingOrder(order.Ticket);
        if (result.RtnCode === TradingResponseCode.RET_OK || result.RtnCode === TradingResponseCode.RET_CUST_OK) {
          if (order){
              orderSuccessCallback(order.Symbol);
          }
        } else {
          var vDP = contractSymbols.VolumeStep.toString().split('.');
          var nVolumeDP = 0
          if (vDP.length > 1){
              nVolumeDP = vDP[1].length;
          }
          const componentView = (
              <View>
                  <View style={styles.contentItem}>
                      <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                      <DashedLineView style={{paddingHorizontal: 10}} />
                      <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(contractSymbols.SymbolName)}</Text>
                  </View>
                  <View style={styles.contentItem}>
                      <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                      <DashedLineView style={{paddingHorizontal: 10}} />
                      <Text style={[globalStyles.Body_Text_B]}>{productName(displayContractCode(contractSymbols.SymbolName))}</Text>
                  </View>
                  <View style={styles.contentItem}>
                      <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                      <DashedLineView style={{paddingHorizontal: 10}} />
                      <Text style={[globalStyles.Body_Text_B, isBuy ? {color: colors.Green} : {color: colors.Red}]}>{isBuy ? t('trade.buy') : t('trade.sell')}</Text>
                  </View>
                  <View style={styles.contentItem}>
                      <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                      <DashedLineView style={{paddingHorizontal: 10}} />
                      <Text style={[globalStyles.Body_Text_B]}>{volume.toFixed(nVolumeDP)} {t('confirmTrade.lots')}</Text>
                  </View>
              </View>
          )

          if (result.RtnCode === -1) {
              SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.cancelOrderFailure'), message: ''})
          } else if (result.RtnCode === 132) {
              SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.132'), message: ''})
          } 
          else if (result.RtnCode === 999) {
              SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
          }
          else {
              SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode)})
          }
        }
      }
      else {
        // navigation.navigate('ConfirmOrder', 
        // {   
        //     type: OrderActionType.CANCEL_ORDER,
        //     instrument: '', 
        //     contractCode: '',
        //     isSelectBuy: false, 
        //     volume: 0, 
        //     volumeDP: 0,
        //     amount: 0,
        //     amountCurrency: '',
        //     price: 0,
        //     priceDP: 0,
        //     requiredMargin: 0,
        //     isPending: false,
        //     pendingOrderType: 0,
        //     penderOrderValidity: 0,
        //     isProfit: false,
        //     stopLossPrice: 0,
        //     limitProfitPrice: 0,
        //     transactionId: order.Ticket
        // })
        
        // SetIsShowOrderDialog(true)
        SetIsShowOrderConfirmDialog({
            isShowDialog:true,
            type:OrderActionType.CANCEL_ORDER,
            contractCode:'',
            isSelectBuy:false,
            volume:0,
            price:0,
            isPending:true,
            pendingOrderType:0,
            pendingOrderValidity: order && order.TimeExpiration ? 1 : 0,
            isProfit:false,
            stopLossPrice:0,
            limitProfitPrice:0,
            transactionId:order && order.Ticket,
            callback:orderSuccessCallback,
        })
      }
    }
  }

  const orderSuccessCallback = (contractCode: string) => {
      navigation.push('ContractDetailView', {contractCode: contractCode, defaultTab: 1, defaultPositionTab: 1, isEnterByOrder: true})
  }

  var product = productName(displayContractCode(symbol));
  // if (isEmpty(product)) {
  //   product = displayContractCode(symbol);
  // }

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: colors.MainBg }]}>
       {/* <OrderConfirmDialog
          isShow={isShowOrderDialog}
          setShow={SetIsShowOrderDialog}
          type={OrderActionType.CANCEL_ORDER}
          contractCode={''}
          isSelectBuy={false} 
          volume={0} 
          price={0}
          isPending={true}
          pendingOrderType={0}
          penderOrderValidity={0}
          isProfit={false}
          stopLossPrice={0}
          limitProfitPrice={0}
          transactionId={order && order.Ticket}
          callback={orderSuccessCallback}
      /> */}
      <ScrollView style={{ flex: 1, paddingHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
        <View style={{ padding: 20 }} />
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.productCode')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          {/* <MSmallButton title={displayContractCode(symbol)} type='White' textStyle={[globalStyles.Contract]} onPress={()=>navigation.navigate('ContractDetailView', { contractCode: symbol })} /> */}
          <TouchableOpacityComponent
            onPress={()=>navigation.navigate('ContractDetailView', { contractCode: symbol })}
            style={[
              styles.buttonContainer,
            ]}>
              <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={[globalStyles.Contract]}>{displayContractCode(symbol)}</Text>
              </View>
              <View style={{ width: 4 }} />
              <View style={styles.buttonArrow}>
                  <CircleArrow20px color={colors.Brand3} backgroundColor={colors.White} />
              </View>
            </TouchableOpacityComponent>
        </View>
        {
          isEmpty(product)?
          null :
          <View style={[styles.contentItem]}>
            <Text style={[globalStyles.Body_Text]}>{t('positionDetails.productName')}</Text>
            <DashedLineView style={{paddingHorizontal: 10}} />
            <Text style={[globalStyles.Body_Text_B]}>{product}</Text>
          </View>
        }
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.account')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <Text style={[globalStyles.Body_Text_B]}>{login}</Text>
        </View>
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.status')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <Status/>
        </View>
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.direction')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <BuySell/>
        </View>
        {
          type === DetailsType.PENDING_ORDER ?
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{t('positionDetails.orderType')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <Text style={[globalStyles.Body_Text_B]}>{orderType}</Text>
            </View>
          :
            null
        }
        <View style={[styles.contentItem]}>
            <Text style={[globalStyles.Body_Text]}>{t('positionDetails.volume')}</Text>
            <DashedLineView style={{paddingHorizontal: 10}} />
            <Lot/>
        </View>
        <ContractValueComponent symbol={symbol} currentLot={volume} isSelectBuy={isBuy} isPending={true} pendingOrderPrice={price}/>
        {
          type === DetailsType.NEW_POSITION &&
          <View style={[styles.contentItem]}>
            <Text style={[globalStyles.Body_Text]}>{t('positionDetails.openPrice')}</Text>
            <DashedLineView style={{paddingHorizontal: 10}} />
            <Text style={[globalStyles.Body_Text_B]}>{openPrice.toFixed(digit)}</Text>
          </View>
        }

        {
          type === DetailsType.PENDING_ORDER ?
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{t('positionDetails.targetPrice')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <Text style={[globalStyles.Body_Text_B]}>{price.toFixed(digit)}</Text>
            </View>
          : 
            null
        }

        {
          type === DetailsType.CLOSE_POSITION &&
          <View>
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{t('positionDetails.openPrice')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <Text style={[globalStyles.Body_Text_B]}>{openPrice.toFixed(digit)}</Text>
            </View>
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{t('positionDetails.closePrice')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <Text style={[globalStyles.Body_Text_B]}>{referencePrice.toFixed(digit)}</Text>
            </View>
          </View>
        }

        {
          (type === DetailsType.NEW_POSITION || type === DetailsType.PENDING_ORDER) &&
            <MarginValueComponent 
              symbol={symbol} 
              currentLot={volume} 
              isSelectBuy={isBuy} 
              isPending={type === DetailsType.PENDING_ORDER || type === DetailsType.NEW_POSITION} 
              price={price} 
              marginRate={marginRate} 
              isRequiredMargin={type === DetailsType.NEW_POSITION} 
              marginMode={contractSymbols?.MarginMode} />
        }

        {
          type === DetailsType.NEW_POSITION || type === DetailsType.CLOSE_POSITION ?
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{type === DetailsType.NEW_POSITION ? t('positionDetails.floatingPL') : t('positionDetails.pl')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <PL/>
            </View>
          : 
            null
        }
        
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.takeProfitPrice')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <Text style={[globalStyles.Body_Text_B]}>{tpPrice === 0 ? t('positionDetails.na') : tpPrice.toFixed(digit)}</Text>
        </View>
        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.stopLossPrice')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <Text style={[globalStyles.Body_Text_B]}>{slPrice === 0 ? t('positionDetails.na') : slPrice.toFixed(digit)}</Text>
        </View>

        {
          type === DetailsType.NEW_POSITION || type === DetailsType.CLOSE_POSITION ?
            <View>
              <View style={[styles.contentItem]}>
                <Text style={[globalStyles.Body_Text]}>{t('positionDetails.interest')}</Text>
                <DashedLineView style={{paddingHorizontal: 10}} />
                <Text style={[globalStyles.Body_Text_B]}>{toCurrencyDisplay(interest)}</Text>
              </View>
              <View style={[styles.contentItem]}>
                <Text style={[globalStyles.Body_Text]}>{t('positionDetails.commission')}</Text>
                <DashedLineView style={{paddingHorizontal: 10}} />
                <Text style={[globalStyles.Body_Text_B]}>{toCurrencyDisplay(commission)}</Text>
              </View>
              <View style={[styles.contentItem]}>
                <Text style={[globalStyles.Body_Text]}>{t('positionDetails.openPositionTime')}</Text>
                <DashedLineView style={{paddingHorizontal: 10}} />
                <Text style={[globalStyles.Body_Text_B]}>{openTime}</Text>
              </View>
            </View>
          : 
            null
        }

        {
          type === DetailsType.CLOSE_POSITION ?
            <View style={[styles.contentItem]}>
              <Text style={[globalStyles.Body_Text]}>{t('positionDetails.closePositionTime')}</Text>
              <DashedLineView style={{paddingHorizontal: 10}} />
              <Text style={[globalStyles.Body_Text_B]}>{closeTime}</Text>
            </View>
          :
            null
        }

        {
          type === DetailsType.PENDING_ORDER ?
            <View>
              <View style={[styles.contentItem]}>
                <Text style={[globalStyles.Body_Text]}>{t('positionDetails.creationTime')}</Text>
                <DashedLineView style={{paddingHorizontal: 10}} />
                <Text style={[globalStyles.Body_Text_B]}>{openTime}</Text>
              </View>
              <View style={[styles.contentItem]}>
                <Text style={[globalStyles.Body_Text]}>{t('positionDetails.validity')}</Text>
                <DashedLineView style={{paddingHorizontal: 10}} />
                <Text style={[globalStyles.Body_Text_B]}>{validity}</Text>
              </View>
            </View>
          : 
            null
        }

        <View style={[styles.contentItem]}>
          <Text style={[globalStyles.Body_Text]}>{t('positionDetails.transactionId')}</Text>
          <DashedLineView style={{paddingHorizontal: 10}} />
          <Text style={[globalStyles.Body_Text_B]}>{transactionId}</Text>
        </View>
      </ScrollView>
      {
        type === DetailsType.NEW_POSITION ?
          <View>
            <ActionButtons
              btn1Title={t('trade.editPosition')}
              btn1Func={editPosition}
              btn2Title={t('trade.closePosition')}
              btn2Func={closePosition}
              btn2Style={{color: colors.FixedMainFont}} />
          </View>
        : 
          null
      }
      {
        type === DetailsType.PENDING_ORDER ?
          <View>
            <ActionButtons
              btn1Title={t('trade.editOrder')}
              btn1Func={modifyOrder}
              btn2Title={t('trade.cancelOrder')}
              btn2Func={cancelOrder}
              btn2Style={{color: colors.FixedMainFont}} />
          </View>
        :
          null
      }
    </SafeAreaView>
  );
};


const styles = StyleSheet.create({
  container: {
      flex: 1
  },
  contentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  dottedLine: {
      flex: 0.8,
      borderStyle: 'dotted',
      borderWidth: 1,
      borderRadius: 1
  },
  buttonContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
  },
  buttonArrow: {
      height: 24, 
      width: 24, 
      justifyContent: 'center', 
      alignItems: 'center',
  },
});

export default PositionDetails;