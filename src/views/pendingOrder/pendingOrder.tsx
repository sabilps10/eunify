import React, { useContext, useEffect, useMemo, useState } from 'react';
import { FlatList, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { useTheme } from '@react-navigation/native';
import commonStyles from '../../styles/styles';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';
import PendingOrderItem from './pendingOrderItem';
import { SortingType } from '../portfolio/portfolio';
// import { Tabs } from 'react-native-collapsible-tab-view';
import { displayContractCode } from '../utils/CommonFunction';

import { Segmented, useIsFocused } from 'react-native-collapsible-segmented-view'
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { PortfolioTabType } from '../../redux/state/types';

const PendingOrder = ({ isDetailsPage, isInCollapsibleTab, symbol, onPressDetails, onPressClose, onPressEdit }: { isDetailsPage: boolean, isInCollapsibleTab: boolean, symbol: string[], onPressDetails: Function, onPressClose: Function, onPressEdit: Function }) => {
    const [t, i18n] = useTranslation();

    const { colors } = useTheme();


    const selectedSortingType = useSelector((state: State) => state.selection.selected?.portfolioSortingType);
    // const onTouch = useSelector((state: State) => state.state.onTouch);

    const pendingOrderKey: number[] = useSelector((state: State) => {
        if (state.trading.orders)
        {
            var filterList = Object.values(state.trading.orders);
            if (symbol && !symbol.includes(t('historyDealQuery.all'))) {
                filterList = filterList.filter(x => symbol.includes(x.Symbol))
            }

            var sortList = filterList.sort((a, b) => b.TimeSetup - a.TimeSetup)
            if (selectedSortingType === SortingType.DateDesc) {
                //Default sorting
            } else if (selectedSortingType === SortingType.DateAsc) {
                sortList = filterList.sort((a, b) => a.TimeSetup - b.TimeSetup)
            }

            return sortList.map(a => a.Ticket);
        }
    }, shallowEqual);

    const renderItem = ({ item }) => (
        <PendingOrderItem
            isDetailsPage={isDetailsPage}
            item={item && item.toString()}
            onPressClose={onPressClose}
            onPressEdit={onPressEdit}
            onPressDetails={() => onPressDetails(item)}/>
    )

    const isFocused = isInCollapsibleTab ? useIsFocused() : false
    const dispatch = useDispatch();
    const { SetPortfolioCurrentTab, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    useEffect(()=>{
        // console.log('PendingOrder isFocused', isFocused)
        if (isFocused) {
            SetPortfolioCurrentTab(PortfolioTabType.Order)
        }
    }, [isFocused])

    return (
        isInCollapsibleTab ?
            <Segmented.FlatList
                style={{backgroundColor: colors.MainBg}}
                initialNumToRender={5}
                windowSize={5}
                data={pendingOrderKey}
                keyExtractor={item => item && item.id}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItem}
                removeClippedSubviews={true}
                // onTouchStart={()=>{SetOnTouch(!onTouch)}}
            />
        :
            <View style={[commonStyles.container, {backgroundColor: colors.MainBg}]}>
                <View style={{flex: 1}}>
                    <FlatList
                        initialNumToRender={5}
                        windowSize={5}
                        data={pendingOrderKey}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        renderItem={renderItem}
                        // onTouchStart={()=>{SetOnTouch(!onTouch)}}
                    />
                </View>
            </View>
    );
}

export default PendingOrder;