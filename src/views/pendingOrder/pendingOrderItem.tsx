import { useTheme } from "@react-navigation/native";
import React from "react";
import { Pressable, StyleSheet, Text, useColorScheme, View } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { State } from "../../redux/root-reducer";
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import { bindActionCreators } from 'redux';
import { OpenPositionsActionCreators } from "../../redux/trading/actionCreator";
import { useEffect } from "react";
import { PendingOrderBuySellType } from "../../utils/orderUtils";
import { useTranslation } from "react-i18next";
import { displayContractCode, productName, timestampToDateWithFormat } from "../utils/CommonFunction";
import Edit from "../../../assets/svg/Edit";
import Remove from "../../../assets/svg/Remove";
import ArrowRight from "../../../assets/svg/ArrowRight";
import PressableComponent from "../utils/PressableComponent";

interface PendingOrderItemParams {
    isDetailsPage: boolean,
    item: string,
    onPressClose?: Function,
    onPressEdit?: Function,
    onPressDetails?: Function,
}

const PendingOrderItem = (params: PendingOrderItemParams) => {
    const [t, i18n] = useTranslation();
    
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    
    const pendingOrder = useSelector((state: State) => state.trading.orders && state.trading.orders[parseInt(params.item)])
    const currentPrice = useSelector((state: State) => pendingOrder && state.trading.prices && state.trading.prices[pendingOrder.Symbol]);
    const symbol = useSelector((state: State) => pendingOrder && state.trading.symbols && state.trading.symbols[pendingOrder.Symbol])
    const islastOrder = useSelector((state: State) => {
        if (state.trading.lastOrderId && params.item === state.trading.lastOrderId)
        {
            return true;
        }
        else
        {
            return false;
        }
    });

    const dispatch = useDispatch();
    const { SetLastOrderId } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const orderPriceComponent = () => {
        return null
    }

    useEffect(()=> {
        if (islastOrder) {
            console.debug("SET Last ORder ID: " + params.item)
            setTimeout(function(){ 
                SetLastOrderId(undefined) }, 3000
            );
        }
    }, [islastOrder])

    return (
        <View style={[styles.listItemContainer, { borderColor: colors.WrapperBg, backgroundColor: islastOrder ? colors.Grey1 : colors.MainBg }]}>
            {/* {console.debug(`${new Date()}, openPositions: ${openPosition?.Symbol}, ${openPosition?.Ticket}`)} */}
            <View style={[styles.listHeaderItem, {flex: 0.45}]}>
                {
                    !params.isDetailsPage &&
                    <View>
                        <Text style={[globalStyles.Note, { color: colors.Text, fontSize: 10 }]}>
                                {productName(displayContractCode(symbol?.SymbolName ?? ''))}
                        </Text>
                        {/* <View style={{padding: 1}}/> */}
                        <Text style={[globalStyles.H4, { color: colors.Brand2 }]}>
                            {displayContractCode(pendingOrder?.Symbol)}
                        </Text>
                    </View>
                }
                <View style={styles.listItemLeftRow}>
                    {
                        params.isDetailsPage &&
                        <View style={{flexDirection: 'row'}}>
                            <Text
                                style={[
                                    globalStyles.Note,
                                    { color: pendingOrder?.Type === PendingOrderBuySellType.BUY_LIMIT || pendingOrder?.Type === PendingOrderBuySellType.BUY_STOP ? colors.Up : colors.Down },
                                ]}>
                                {pendingOrder?.Type === PendingOrderBuySellType.BUY_LIMIT || pendingOrder?.Type === PendingOrderBuySellType.BUY_STOP ? t('trade.buy') : t('trade.sell')}
                            </Text>
                            <View style={{ width: 5 }} />
                            <Text style={[globalStyles.Note]}>{pendingOrder?.InitialVolume.toFixed(2)}</Text>
                        </View>
                    }
                </View>
                {/* <View style={{padding: 1}}/> */}
                <Text style={[globalStyles.Tiny_Note]}>
                    {pendingOrder?.TimeSetup && timestampToDateWithFormat(pendingOrder?.TimeSetup, 'YYYY-MM-DD HH:mm:ss')}
                </Text>
            </View>
            <View style={[styles.listHeaderItem, {flex: 0.25}]}>
                { pendingOrder && currentPrice ?
                <View>
                    {
                        params.isDetailsPage ? 
                        <Text style={[globalStyles.Note]}>
                            {pendingOrder?.PriceOrder.toFixed(symbol?.Digits)}
                        </Text> :
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                <Text
                                    style={[
                                        globalStyles.Note,
                                        { color: pendingOrder?.Type === PendingOrderBuySellType.BUY_LIMIT || pendingOrder?.Type === PendingOrderBuySellType.BUY_STOP ? colors.Up : colors.Down },
                                    ]}>
                                    {pendingOrder?.Type === PendingOrderBuySellType.BUY_LIMIT || pendingOrder?.Type === PendingOrderBuySellType.BUY_STOP ? t('trade.buy') : t('trade.sell')}
                                </Text>
                                <View style={{ width: 5 }} />
                                <Text style={[globalStyles.Note]}>{pendingOrder?.InitialVolume.toFixed(2)}</Text>
                            </View>
                            <View style={{padding: 1}}/>
                            <Text style={[globalStyles.Note]}>
                                {pendingOrder?.PriceOrder.toFixed(symbol?.Digits)}
                            </Text>
                        </View>
                    }
                    
                </View>
                : null}
                {orderPriceComponent()}
            </View>
            <View style={[styles.listHeaderItemRight, {flex: 0.30, height: '100%'}]}>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={()=>{params.onPressClose!(pendingOrder?.Ticket!, pendingOrder?.Symbol!, (pendingOrder?.Type === PendingOrderBuySellType.BUY_LIMIT || pendingOrder?.Type === PendingOrderBuySellType.BUY_STOP), pendingOrder.RemainVolume.toFixed(2))}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Remove color={colors.Brand3} />
                    </View>
                </PressableComponent>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={()=>{params.onPressEdit!(pendingOrder?.Ticket!)}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Edit color={colors.Brand3} />
                    </View>
                </PressableComponent>
                <PressableComponent style={styles.listHeaderItemRightButton} onPress={()=> {params.onPressDetails!()}}>
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <ArrowRight color={colors.Brand3} />
                    </View>
                </PressableComponent>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
    input: {
        width: 200,
        borderWidth: 1,
        borderColor: '#555',
        borderRadius: 5,
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 10,
    },
    mediaItemTxt: {
        color: '#333333',
        fontSize: 13,
        marginTop: 4,
    },
    listHeaderItem: {
        flex: 0.35,
    },
    listHeaderItemRight: {
        flex: 0.30,
        flexDirection: 'row'
    },
    listHeaderItemRightButton: {
        flex: 1,
        // backgroundColor: '#555555', 
        alignItems:'center',
        alignSelf:'center',
        marginStart: 5
    },
    ItemTxt: {
        color: '#333333',
        fontSize: 13,
        marginBottom: 10,
        marginTop: 10,
        marginRight: 5,
        marginLeft: 5,
    },
    container: {
        //flex: 1,
        width: '100%',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
    },
    rightSwipeContainer: {
        width: 211,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    swipeContainerPriceBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    listItemContainer: {
        width: '100%',
        borderTopWidth: 1,
        // height: 72,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        paddingTop: 7,
        paddingBottom: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    listItemLeftRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default React.memo(PendingOrderItem)