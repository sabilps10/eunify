import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  Pressable,
  // Linking,
  BackHandler,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import ProgressHeader from '../../components/header/ProgressHeader';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {entityId} from '../../config/constants';
import Button from '../../components/buttons/Button';
import {ScrollView} from 'react-native-gesture-handler';
import InputText from '../../components/forms/inputs/InputText';
import Dropdown from '../../components/forms/inputs/Dropdown';
import DatePicker from '../../components/forms/inputs/DatePicker';
import InputCheckBox from '../../components/forms/inputs/inputCheckBox.component';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {PersonalInformationType} from '../../redux/accountOpening/type';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {
  getIndividual,
  updatePersonalInformation,
  // validateIdType,
} from '../../utils/api/apiAccountOpening';
import {
  checkMemberLoyalty,
  getIdentificationTypeList,
} from '../../utils/api/apiGeneralAccountOpening';
import moment from 'moment';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {BottomSheetCreators} from '../../redux/BottomSheet/actionCreators';

interface AccountPersonalInformationProps {
  navigation: any;
  route?: any;
}

type FormItem = {
  label: string;
  name: string;
  required?: boolean;
  disabled?: boolean;
  type: 'input-text' | 'dropdown' | 'date-picker';
  textType?:
    | 'letterOnlyInput'
    | 'numberOnlyInput'
    | 'letterNoDigit'
    | 'letterNoSpace';
  options?: any[];
  dropdownType?: 'bottomsheet' | 'page' | 'country' | 'other';
  tooltip?: string;
  onPress?: () => void;
  maxLength?: number;
  errorMessage?: string;
};

type Option = {
  value: string;
  label: string;
};

const RenderFormByType = ({
  label,
  name,
  required,
  disabled,
  type,
  tooltip,
  options,
  dropdownType,
  formData,
  handleFormChange,
  errors,
  onPress,
  textType,
  maxLength,
  errorMessage,
}: FormItem & {
  formData: PersonalInformationType;
  handleFormChange: (val: any, name: string) => void;
  errors: any[];
}) => {
  const handlePress = () => {
    if (disabled) {
      onPress();
    }
  };

  switch (type) {
    case 'input-text':
      return (
        <InputText
          value={formData[name as keyof PersonalInformationType] as string}
          name={name}
          label={label}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          editAble={!disabled}
          errors={errors}
          errorMessage={errorMessage}
          onFocus={handlePress}
          type={textType}
          maxLength={maxLength}
        />
      );
    case 'dropdown':
      return (
        <Dropdown
          value={formData[name as keyof PersonalInformationType]}
          label={label}
          name={name}
          onSelect={handleFormChange}
          options={options ?? []}
          required={required}
          disabled={disabled}
          wrapperStyle={tw`mb-[28px]`}
          errors={errors}
          type={dropdownType}
          onFocus={handlePress}
        />
      );
    case 'date-picker':
      return (
        <DatePicker
          value={formData[name as keyof PersonalInformationType]}
          label={label}
          name={name}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          disabled={disabled}
          tooltip={tooltip}
          errors={errors}
          onFocus={handlePress}
        />
      );
  }
};

const AccountPersonalInformation: React.FC<AccountPersonalInformationProps> = ({
  navigation,
  route,
}) => {
  const {t} = useTranslation();

  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const {width} = Dimensions.get('window');
  const dispatch = useDispatch();

  const {username} = useSelector((state: State) => state.cognitoTemp);
  const {currentToken} = useSelector((state: State) => state.cognito);
  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const {contactInformation, individual} = useSelector(
    (state: State) => state.accountOpening,
  );

  const {
    SetPersonalInformation,
    SetContactInformation,
    SetIndividual,
    SetDepositDialog,
  } = bindActionCreators(AccountOpeningCreators, dispatch);

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const {isOpen: isOpenBottomSheet} = useSelector(
    (state: State) => state.bottomSheet,
  );
  const {CloseBottomSheet} = bindActionCreators(BottomSheetCreators, dispatch);

  const [idenType, setIdenType] = useState<Option[] | []>([]);

  const [errors, setErrors] = useState<any[]>([]);
  const [formData, setFormData] = useState<PersonalInformationType>({
    legalLastNameEn: '',
    legalFirstAndMiddleNameEn: '',
    legalLastName: '',
    legalFirstAndMiddleName: '',
    gender: null,
    jurisdiction: '',
    dateOfBirth: null,
    jurisdictionOfResidence: '',
    identificationType: '',
    identificationNo: '',
    identificationExpiryDate: null,
    usCitizen: true,
    efsgRewardProgram: true,
  });

  const [isMember, setIsMember] = useState<boolean>(false);

  const [formDisabled, setFormDisabled] = useState<boolean>(true);
  const [isOpen, setIsOpen] = useState<string | null>(null);
  const [isExist, setIsExist] = useState<boolean>(false);

  const formList: FormItem[] = [
    {
      label: t('accountOpening.personal_information.legal_last_name_en'),
      name: 'legalLastNameEn',
      required: true,
      disabled: entityId === 'EIE' ? formDisabled : false,
      type: 'input-text',
      textType: 'letterNoDigit',
    },
    {
      label: t(
        'accountOpening.personal_information.legal_first_middle_name_en',
      ),
      name: 'legalFirstAndMiddleNameEn',
      required: true,
      disabled: entityId === 'EIE' ? formDisabled : false,
      type: 'input-text',
      textType: 'letterNoDigit',
    },
    {
      label: t('accountOpening.personal_information.legal_last_name'),
      name: 'legalLastName',
      required: false,
      disabled: false,
      type: 'input-text',
      textType: 'letterNoDigit',
    },
    {
      label: t('accountOpening.personal_information.legal_first_name'),
      name: 'legalFirstAndMiddleName',
      required: false,
      disabled: false,
      type: 'input-text',
      textType: 'letterNoDigit',
    },
    {
      label: t('accountOpening.personal_information.gender'),
      name: 'gender',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: [
        {value: 'Male', label: lang === 'en' ? 'Male' : '男'},
        {value: 'Female', label: lang === 'en' ? 'Female' : '女'},
        {value: 'Other', label: lang === 'en' ? 'Other' : '其他'},
      ],
    },
    {
      label: t('accountOpening.personal_information.country_region_residence'),
      name: 'jurisdictionOfResidence',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'country',
    },
    {
      label: t('accountOpening.personal_information.dob'),
      name: 'dateOfBirth',
      required: true,
      disabled: formDisabled,
      type: 'date-picker',
      tooltip: t('accountOpening.personal_information.dob_tooltip'),
    },
    {
      label: t('accountOpening.personal_information.nationality'),
      name: 'jurisdiction',
      required: true,
      disabled: formDisabled,
      type: 'dropdown',
      dropdownType: 'country',
      options: [],
    },
    {
      label: t('accountOpening.personal_information.id_type'),
      name: 'identificationType',
      required: true,
      disabled: formDisabled,
      type: 'dropdown',
      options: idenType,
    },
    {
      label: t('accountOpening.personal_information.id_passport_number'),
      name: 'identificationNo',
      required: true,
      disabled: formDisabled,
      type: 'input-text',
      textType: 'letterNoSpace',
      maxLength: 20,
      errorMessage: formData.identificationNo
        ? t('accountOpening.personal_information.id_duplicate')
        : t('error.mandatory'),
    },
    {
      label: t('accountOpening.personal_information.id_expiry'),
      name: 'identificationExpiryDate',
      required: formData.identificationType === 'PASSPORT',
      disabled: formDisabled,
      type: 'date-picker',
      tooltip: t('accountOpening.personal_information.expiry_tooltip'),
    },
  ];

  const handleFormChange = (val: any, name: string) => {
    setErrors(errors.filter(el => el !== name));
    if (name === 'identificationType') {
      setFormData({...formData, [name]: val, identificationExpiryDate: null});
    } else {
      setFormData({...formData, [name]: val});
    }
  };

  const onPressBack = () => {
    navigation.navigate('AccountGettingReady');
    return true;
  };

  const getAge = (birthdate: Date): number => {
    if (!birthdate) return 0;
    const today = new Date();
    const diffInMs = today.getTime() - birthdate.getTime();
    const ageDate = new Date(diffInMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };

  const monthDifferenceChecker = (startDate: Date): number => {
    if (!startDate) return 0;

    return moment(new Date()).diff(startDate, 'months', true);
  };

  const handleSave = async () => {
    const error: string[] = [];

    // await validateIdType({
    //   token: currentToken,
    //   idType: formData.identificationType,
    //   idNumber: formData.identificationNo,
    //   onSuccess: data => {
    //     if (data.response === true) {
    //       error.push('identificationNo');
    //     }
    //   },
    //   onFailed: () => {},
    // });

    formList.map((el: FormItem) => {
      if (el.required && !formData[el.name]) {
        error.push(el.name);
      }
    });
    if (!formData.usCitizen) {
      error.push('usCitizen');
    }

    if (getAge(formData.dateOfBirth as Date) < 18) {
      error.push('ageValidation');
    }

    if (
      formData.identificationExpiryDate &&
      monthDifferenceChecker(formData.identificationExpiryDate as Date) > -6
    ) {
      error.push('expValidation');
    }

    if (error.length > 0) {
      setErrors(error);
      return;
    } else {
      const payload = {
        ...formData,
        customerId: contactInformation.customerId,
        dateOfBirth: moment(formData.dateOfBirth).format('YYYY-MM-DD HH:mm:ss'),
        identificationExpiryDate: formData.identificationExpiryDate
          ? moment(formData.identificationExpiryDate).format(
              'YYYY-MM-DD HH:mm:ss',
            )
          : null,
        loyalty: formData.efsgRewardProgram,
        isAdminPortal: false,
        usCitizen: !formData.usCitizen,
      };

      updatePersonalInformation({
        token: currentToken,
        payload,
        onSuccess: () => {
          SetPersonalInformation(formData);
          SetContactInformation({
            ...contactInformation,
            jurisdictionOfResidence: formData.jurisdictionOfResidence,
          });

          setErrors([]);

          if (route?.params?.lastScreen) {
            navigation.navigate(route?.params?.lastScreen);
          } else {
            navigation.navigate('AccountContactInformation');
          }
        },
        onFailed: () => {},
        setLoading: SetIsShowLoading,
      });
    }
  };

  // const handleTnC = async () => {
  //   const tncLink =
  //     lang === 'en'
  //       ? 'https://d3c9gxi9mi8b6e.cloudfront.net/1657011278613laERKBrc3.pdf'
  //       : lang === 'tc'
  //       ? 'https://d3c9gxi9mi8b6e.cloudfront.net/1657008538591uXhzE95bh.pdf'
  //       : 'https://d3c9gxi9mi8b6e.cloudfront.net/1657007975086m6uXdODSS.pdf';

  //   Linking.openURL(tncLink);
  // };

  useEffect(() => {
    getIdentificationTypeList({
      token: currentToken,
      onSuccess: data => {
        const mapOption = data.response.map(el => ({
          value: el.code,
          label:
            lang === 'en' ? el.label : lang === 'tc' ? el.labelTc : el.labelSc,
        }));
        setIdenType(mapOption);
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentToken]);

  useEffect(() => {
    checkMemberLoyalty({
      email: username,
      token: currentToken,
      onSuccess: data => {
        const code = ['E001', 'E002'];
        if (!code.includes(data.errorReasonCode)) {
          setIsMember(true);
        }
      },
      onFailed: () => {},
    });
  }, [currentToken, username]);

  useEffect(() => {
    SetIsShowLoading(true);
    let timer = 30000;
    const interval = setInterval(() => {
      timer -= 3000;

      getIndividual({
        token: currentToken,
        payload: {
          username: individual ? individual.id : username,
        },
        onSuccess: data => {
          if (data.response?.aoOcrIdDoc !== null) {
            setIsExist(true);
            clearInterval(interval);

            SetIndividual(data.response);

            setFormData({
              legalFirstAndMiddleNameEn: data.response.legalFirstAndMiddleNameEn
                ? data.response.legalFirstAndMiddleNameEn
                : data?.response?.aoOcrIdDoc.firstNameEn,
              legalFirstAndMiddleName: data.response.legalFirstAndMiddleName
                ? data.response.legalFirstAndMiddleName
                : data?.response?.aoOcrIdDoc.firstName,
              dateOfBirth: data.response.dateOfBirth
                ? new Date(
                    moment(data.response.dateOfBirth).format('YYYY-MM-DD'),
                  )
                : data?.response?.aoOcrIdDoc.dob
                ? new Date(
                    moment(data?.response?.aoOcrIdDoc.dob).format('YYYY-MM-DD'),
                  )
                : null,
              identificationType:
                data.response.identificationType ??
                data?.response?.aoOcrIdDoc.idDocType,
              identificationNo:
                data.response.identificationNo ??
                data?.response?.aoOcrIdDoc.number,
              identificationExpiryDate: data.response.identificationExpiryDate
                ? new Date(
                    moment(data.response.identificationExpiryDate).format(
                      'YYYY-MM-DD',
                    ),
                  )
                : data?.response?.aoOcrIdDoc.validUntil
                ? new Date(
                    moment(data?.response?.aoOcrIdDoc.validUntil).format(
                      'YYYY-MM-DD',
                    ),
                  )
                : null,
              legalLastNameEn:
                data.response.legalLastNameEn ??
                data?.response?.aoOcrIdDoc.lastNameEn,
              legalLastName: data.response.legalLastName
                ? data.response.legalLastName
                : data?.response?.aoOcrIdDoc.lastName,
              gender: data.response.gender ?? null,
              jurisdiction:
                data.response.jurisdiction ??
                data?.response?.aoOcrIdDoc.country,
              jurisdictionOfResidence:
                data.response.jurisdictionOfResidence ??
                data?.response?.aoOcrIdDoc.country,
              usCitizen: true,
              efsgRewardProgram: true,
              customerId: data.response.id,
            });

            SetContactInformation({
              customerId: data?.response?.id,
              addressLine1:
                data?.response?.aoOcrAddress.formattedAddress ??
                data.response.addressLine1,
              addressLine2:
                data?.response?.aoOcrAddress.subStreet ??
                data.response.addressLine2,
              city: data?.response?.aoOcrAddress.town ?? data.response.city,
              province:
                data?.response?.aoOcrAddress.state ?? data.response.province,
              postcode:
                data?.response?.aoOcrAddress.postCode ?? data.response.postcode,
              jurisdictionOfResidence:
                data?.response?.aoOcrIdDoc.country ??
                data.response.jurisdictionOfResidence,
            });

            return;
          }

          if (timer < 0 && !data?.response?.aoOcrIdDoc) {
            clearInterval(interval);
            setIsExist(true);

            SetIndividual(data.response);

            setFormData({
              legalFirstAndMiddleNameEn:
                data.response.legalFirstAndMiddleNameEn,
              legalFirstAndMiddleName: data.response.legalFirstAndMiddleName,
              dateOfBirth: data.response.dateOfBirth
                ? new Date(
                    moment(data.response.dateOfBirth).format('YYYY-MM-DD'),
                  )
                : null,
              identificationType: data.response.identificationType,
              identificationNo: data.response.identificationNo,
              identificationExpiryDate: data.response.identificationExpiryDate
                ? new Date(
                    moment(data.response.identificationExpiryDate).format(
                      'YYYY-MM-DD',
                    ),
                  )
                : null,
              legalLastNameEn: data.response.legalLastNameEn,
              legalLastName: data.response.legalLastName,
              gender: data.response.gender ?? null,
              jurisdiction: data.response.jurisdiction,
              jurisdictionOfResidence: data.response.jurisdictionOfResidence,
              usCitizen: true,
              efsgRewardProgram: true,
              customerId: data.response.id,
            });

            SetContactInformation({
              customerId: data?.response?.id,
              addressLine1: data.response.addressLine1,
              addressLine2: data.response.addressLine2,
              city: data.response.city,
              province: data.response.province,
              postcode: data.response.postcode,
              jurisdictionOfResidence: data.response.jurisdictionOfResidence,
            });
            return;
          }
        },
        onFailed: () => {},
      });
    }, 3000);

    return () => clearInterval(interval);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isExist) {
      setTimeout(() => {
        SetIsShowLoading(false);
      }, 500);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isExist]);

  useEffect(() => {
    if (
      contactInformation.jurisdictionOfResidence !==
      formData.jurisdictionOfResidence
    ) {
      handleFormChange(
        contactInformation.jurisdictionOfResidence,
        'jurisdictionOfResidence',
      );
    }
  }, [contactInformation]);

  useFocusEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        if (isOpenBottomSheet) {
          CloseBottomSheet();
        } else {
          if (route?.params?.lastScreen) {
            navigation.navigate('AccountReviewApplication');
          } else {
            onPressBack();
          }
        }

        return true;
      },
    );

    return () => backHandler.remove();
  });

  return (
    <View style={[tw`flex flex-col h-full bg-brand3`]}>
      <SafeAreaView edges={['top', 'left', 'right']}>
        <View style={tw`flex flex-row items-center px-[16px]`}>
          <Icon
            // eslint-disable-next-line react-native/no-inline-styles
            style={{fontSize: 28}}
            color="white"
            name="chevron-left"
            onPress={onPressBack}
          />
          <Text style={[tw`ml-[15px]`, globalStyles.H4, {color: colors.White}]}>
            {t('accountOpening.trading_account_opening')}
          </Text>
        </View>
      </SafeAreaView>
      <ProgressHeader currentStep="1" entity={entityId} />
      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: colors.White}}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : null}
        enabled>
        <ScrollView style={{flex: 1}}>
          <View
            style={tw`flex flex-col bg-brand3 flex-grow justify-center pt-5 h-full`}>
            <View
              style={tw`flex flex-col rounded-t-xl bg-white px-[16px] pt-6`}>
              <Text
                style={[
                  tw`mb-[24px]`,
                  globalStyles.H4,
                  {color: colors.secondaryBtn},
                ]}>
                {t('accountOpening.personal_information.title')}
              </Text>
              {formList.map((el: FormItem, idx: number) => (
                <RenderFormByType
                  {...el}
                  key={idx}
                  formData={formData}
                  handleFormChange={handleFormChange}
                  errors={errors}
                  onPress={() => {
                    SetDepositDialog({
                      isOpen: 'edit_profile',
                      url: '',
                      onPress: () => {
                        setFormDisabled(false);
                        setIsOpen(null);
                      },
                    });
                  }}
                />
              ))}
              <View style={tw`mb-[20px]`}>
                <View style={[tw`flex flex-row`, {width: width - 90}]}>
                  <InputCheckBox
                    value={formData.usCitizen}
                    onPress={() =>
                      handleFormChange(!formData.usCitizen, 'usCitizen')
                    }
                  />
                  <Text
                    style={[
                      tw`ml-[10px]`,
                      globalStyles.Note,
                      {color: colors.MainFont},
                    ]}>
                    {t(
                      'accountOpening.personal_information.confirm_non_us_resident',
                    )}
                  </Text>
                </View>
                {errors?.includes('usCitizen') && (
                  <Text
                    style={[
                      tw`mt-1`,
                      globalStyles.Small_Note,
                      {color: colors.Red},
                    ]}>
                    {t('accountOpening.personal_information.citizenship_error')}
                  </Text>
                )}
              </View>
              {entityId === 'EIE' && !isMember && (
                <View
                  style={[tw`flex flex-row mb-[40px]`, {width: width - 90}]}>
                  <InputCheckBox
                    value={formData.efsgRewardProgram}
                    onPress={() =>
                      entityId !== 'EMX' &&
                      handleFormChange(
                        !formData.efsgRewardProgram,
                        'efsgRewardProgram',
                      )
                    }
                  />
                  <Text
                    style={[
                      tw`ml-[10px]`,
                      globalStyles.Note,
                      {color: colors.MainFont},
                    ]}>
                    {t(
                      'accountOpening.personal_information.membership_program',
                    )}
                    <Pressable
                      onPress={() => {
                        navigation.navigate('AccountTermsCondition');
                      }}>
                      <Text style={{color: colors.Brand3}}>
                        {t('accountOpening.personal_information.tnc')}
                      </Text>
                    </Pressable>
                  </Text>
                </View>
              )}
              <View style={tw`py-5`}>
                <Button
                  label={t('common.save_and_next')}
                  onPress={handleSave}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

export default AccountPersonalInformation;
