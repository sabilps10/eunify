import React from 'react';
import {Pressable, Text, View} from 'react-native';
import {Dialog} from '@rneui/themed';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import Clipboard from '@react-native-clipboard/clipboard';
import {useDispatch, useSelector} from 'react-redux';
import {State} from '../../redux/root-reducer';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';

const DialogCustom = () => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const dispatch = useDispatch();

  const {SetDepositDialog} = bindActionCreators(
    AccountOpeningCreators,
    dispatch,
  );

  const {isOpen, onPress, url} = useSelector(
    (state: State) => state.accountOpening.depositDialog,
  );

  const onClose = () => {
    SetDepositDialog({
      url: null,
      isOpen: null,
      onPress: () => {},
    });
  };
  return (
    <View>
      <Dialog
        isVisible={!!isOpen}
        overlayStyle={{
          width: '80%',
          borderRadius: 12,
        }}>
        <Text style={[globalStyles.H2, {color: colors.Grey3}]}>
          {t(
            `accountOpening.application_status.${
              isOpen === 'edd'
                ? 'edd_form'
                : isOpen === 'witness'
                ? 'witness_form'
                : isOpen === 'edit_profile'
                ? 'edit_profile'
                : 'trade_now'
            }`,
          )}
        </Text>
        <Text style={[globalStyles.Note, {color: colors.text}, tw`mt-2`]}>
          {t(
            `accountOpening.application_status.${
              isOpen === 'edd'
                ? 'edd_fill'
                : isOpen === 'witness'
                ? 'witness_fill'
                : isOpen === 'edit_profile'
                ? 'edit_fill'
                : 'fund_to_trade'
            }`,
          )}
        </Text>
        {isOpen === 'deposit' && (
          <Text style={[globalStyles.Note, {color: colors.text}, tw`mt-5`]}>
            {t(`accountOpening.application_status.deposit_fill`)}
          </Text>
        )}
        {isOpen !== 'deposit' && isOpen !== 'edit_profile' && (
          <View style={tw`flex flex-row justify-between items-center mt-4`}>
            <Text
              style={[
                globalStyles.Body_Text_B_120,
                {color: colors.Brand4},
                tw`w-[80%]`,
              ]}>
              {url}
            </Text>
            <Icon
              name="content-copy"
              size={20}
              color={colors.Brand4}
              onPress={() => Clipboard.setString(url)}
            />
          </View>
        )}
        <View style={tw`mt-10`}>
          <Pressable
            onPress={() => {
              onPress();
              onClose();
            }}>
            <View
              style={[
                tw`flex justify-center items-center px-1 py-2 rounded-full`,
                {backgroundColor: colors.Brand1},
              ]}>
              <Text style={{fontWeight: '600', color: colors.text}}>
                {t(
                  `common.${
                    isOpen === 'deposit'
                      ? 'deposit'
                      : isOpen === 'edit_profile'
                      ? 'edit'
                      : 'go'
                  }`,
                )}
              </Text>
            </View>
          </Pressable>
        </View>

        <View style={tw`flex justify-center items-center mt-4`}>
          <Icon
            name="close-circle-outline"
            size={40}
            color={colors.text}
            onPress={onClose}
          />
        </View>
      </Dialog>
    </View>
  );
};

export default DialogCustom;