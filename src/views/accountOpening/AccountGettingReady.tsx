import React, {useLayoutEffect, useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  Pressable,
  Text,
  StyleSheet,
  Platform,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTranslation} from 'react-i18next';
import Button from '../../components/buttons/Button';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {entityId, sumsub_url} from '../../config/constants';
// import Button from '../components/buttons/Button';

import axios from 'axios';
import {bindActionCreators} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import {StateActionCreators} from '../../redux/state/actionCreator';

import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
import {getSumsubToken} from '../../utils/sumsub';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
// import {fetchCheckIndividual} from '../../utils/api/apiAccountOpening';
// import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
// import moment from 'moment';
import reactotron from 'reactotron-react-native';

interface AccountGettingReadyProps {
  navigation: any;
}

interface ProcessCard {
  icon: string;
  context: string;
  items: string[];
  clickable: boolean;
  action: string;
}

const AccountGettingReady: React.FC<AccountGettingReadyProps> = ({
  navigation,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const {individual} = useSelector((state: State) => state.accountOpening);
  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  // const {
  //   SetPersonalInformation,
  //   SetContactInformation,
  //   SetFinancialInformation,
  //   SetTradingExperience,
  //   SetIndividual,
  // } = bindActionCreators(AccountOpeningCreators, dispatch);

  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  const onPressBackBtn = () => {
    navigation.goBack();
  };

  const goto = (action?: string) => {
    if (!action) {
      if (individual?.aoOcrIdDoc === null) {
        return launchSNSMobileSDK();
      } else {
        return navigation.navigate('AccountPersonalInformation');
      }
    }

    if (action === 'EKYC') {
      if (individual?.aoOcrIdDoc === null) {
        return launchSNSMobileSDK();
      } else {
        return navigation.navigate('AccountPersonalInformation');
      }
    } else {
      // if (individual?.aoOcrIdDoc === null) {
      //   return launchSNSMobileSDK();
      // } else {
      if (entityId === 'EIE') {
        return navigation.navigate('AccountFinancialInformation');
      } else {
        return navigation.navigate('AccountContactInformation');
      }
      //   }
    }
  };

  const process_data =
    entityId === 'EMX'
      ? [
          {
            icon: 'tray-arrow-up',
            icontext: t('accountOpening.getting_ready.upload_doc'),
            textitems: [
              t('accountOpening.getting_ready.identification_doc'),
              t('accountOpening.getting_ready.face_verif'),
              t('accountOpening.getting_ready.residental_address'),
            ],
            action: 'EKYC',
          },
          {
            icon: 'text-box-check-outline',
            icontext: t('accountOpening.getting_ready.fill_form'),
            textitems: [
              t('accountOpening.getting_ready.personal_info'),
              t('accountOpening.getting_ready.contact_info'),
            ],
            action: 'form',
          },
          {
            icon: 'shield-check-outline',
            icontext: t('accountOpening.getting_ready.verify'),
            textitems: [
              t('accountOpening.getting_ready.identification_doc'),
              t('accountOpening.getting_ready.face_verif'),
              t('accountOpening.getting_ready.residental_address'),
            ],
          },
        ]
      : [
          {
            icon: 'tray-arrow-up',
            icontext: t('accountOpening.getting_ready.upload_doc'),
            textitems: [
              t('accountOpening.getting_ready.identification_doc'),
              t('accountOpening.getting_ready.face_verif'),
              t('accountOpening.getting_ready.residental_address'),
            ],
            action: 'EKYC',
          },
          {
            icon: 'text-box-check-outline',
            icontext: t('accountOpening.getting_ready.fill_form'),
            textitems: [
              t('accountOpening.getting_ready.personal_info'),
              t('accountOpening.getting_ready.contact_info'),
              t('accountOpening.getting_ready.emp_financial_info'),
              t('accountOpening.getting_ready.trading_exp'),
            ],
            action: 'form',
          },
          {
            icon: 'shield-check-outline',
            icontext: t('accountOpening.getting_ready.verify'),
            textitems: [
              t('accountOpening.getting_ready.identification_doc'),
              t('accountOpening.getting_ready.face_verif'),
              t('accountOpening.getting_ready.residental_address'),
            ],
          },
          {
            icon: 'cash-multiple',
            icontext: t('accountOpening.getting_ready.fund'),
            textitems: [t('accountOpening.getting_ready.deposit')],
          },
        ];

  const ProcessCard = ({
    icon,
    context,
    items,
    clickable,
    action,
  }: ProcessCard) => {
    return (
      <Pressable
        onPress={() => {
          clickable && goto(action);
        }}>
        <View
          style={[
            tw`flex flex-row w-full h-[125px] items-center justify-between pt-[11px]`,
          ]}>
          <View style={tw`flex flex-row items-center`}>
            <View
              style={tw`w-[65px] flex flex-col items-center justify-center mr-[5px]`}>
              <Icon style={[tw`text-brand3 text-[30px]`]} name={icon} />
              <Text
                style={[
                  globalStyles.Tiny_Note,
                  tw`text-[10px] text-center mt-[5px]`,
                  {color: colors.MainFont},
                ]}>
                {context}
              </Text>
            </View>
            <View style={tw`flex flex-col justify-start`}>
              {items.map((r: string, idx: number) => (
                <View
                  style={tw`flex flex-row items-center mt-[10px]`}
                  key={idx}>
                  <Icon
                    name="circle-medium"
                    color="#CFA872"
                    style={styles.font_24}
                  />
                  <Text
                    style={[
                      globalStyles.Small_Note_B,
                      {color: colors.MainFont},
                    ]}>
                    {r}
                  </Text>
                </View>
              ))}
            </View>
          </View>
          {clickable ? (
            <Pressable
              onPress={() => {
                goto(action);
              }}>
              <View>
                <Icon
                  style={styles.font_24}
                  color="#00B1D2"
                  name="chevron-right"
                />
              </View>
            </Pressable>
          ) : null}
        </View>
      </Pressable>
    );
  };

  const [isFinished, setIsFinished] = useState<boolean>(false);
  const [accessToken, setAccessToken] = useState<string>('');

  reactotron.log(isFinished, accessToken);

  // const {username} = useSelector((state: State) => state.cognitoTemp);
  // const {currentToken} = useSelector((state: State) => state.cognito);

  const {SetIsShowLoading, SetIsShowMessageDialog} = bindActionCreators(
    StateActionCreators,
    dispatch,
  );

  const acceptedStatus = [
    'Pending',
    'TemporarilyDeclined',
    'FinallyRejected',
    'Approved',
  ];

  const launchSNSMobileSDK = async () => {
    setIsFinished(false);
    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
      // this is a token expiration handler, will be called if the provided token is invalid or got expired
      // call your backend to fetch a new access token (this is just an example)
      // const data: any = getSumsubToken(cognitoUsername, entityId);
      // const {token} = data;
      // return token;
      const res: any = axios({
        url: sumsub_url,
        method: 'POST',
        data: {
          customerId: individual?.id,
          kycType: entityId === 'EIE' ? 'aoeiehk' : 'aoxpro',
        },
      });
      return res.data.token;
    })
      .withHandlers({
        // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: event => {
          console.log(event, '<< on status change');

          if (acceptedStatus.includes(event.newStatus)) {
            snsMobileSDK.dismiss();
            setIsFinished(true);
          }
        },
        onEvent: event => {
          console.log(event, '<< on event');
        },
      })
      .withDebug(true)
      .withLocale(lang === 'tc' ? 'zh-tw' : lang === 'sc' ? 'zh' : 'en') // Optional, for cases when you need to override the system locale
      .build();

    snsMobileSDK
      .launch()
      .then(result => {
        console.log(result, '<< result');
        if (acceptedStatus.includes(result.status)) {
          snsMobileSDK.dismiss();
          setIsFinished(true);
        }
      })
      .catch(err => {
        reactotron.log('SumSub SDK Error: ' + JSON.stringify(err));
      });
  };

  useEffect(() => {
    const fetchToken = async () => {
      SetIsShowLoading(true);
      try {
        const data: any = await getSumsubToken(individual?.id, entityId);
        const {token} = data;
        setAccessToken(token);
      } catch (error) {
        SetIsShowMessageDialog({
          isShowDialog: true,
          title: t('common.error.network'),
          message: t('common.error.auth'),
        });
        fetchToken();
      } finally {
        SetIsShowLoading(false);
      }
    };
    if (individual?.id && !isFinished) {
      fetchToken();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [individual?.id]);

  useEffect(() => {
    if (isFinished) {
      navigation.navigate('AccountPersonalInformation');
    }
  }, [isFinished]);

  return (
    <View style={[tw`flex flex-col h-full`]}>
      <View style={tw`flex flex-col bg-brand3 flex-grow justify-center`}>
        <View style={[tw`px-[16px] py-3`]}>
          <SafeAreaView edges={['top', 'left', 'right']}>
            <Icon
              style={styles.font_28}
              color="white"
              name="chevron-left"
              onPress={onPressBackBtn}
            />
          </SafeAreaView>
        </View>
        <View style={tw`px-[16px]`}>
          <View style={tw`pt-5`}>
            <Text style={[globalStyles.H1, tw`text-white`]}>
              {t('accountOpening.getting_ready.title')}
            </Text>
          </View>
          <View style={tw`py-2`}>
            <Text style={[globalStyles.Body_Text, {color: colors.MainFont}]}>
              {t(
                `accountOpening.getting_ready.${
                  entityId === 'EIE' ? 'subtitle' : 'subtitle_xpro'
                }`,
              )}
            </Text>
          </View>
        </View>
        <View
          style={tw`flex flex-col bg-white rounded-t-xl flex-grow px-[16px] pt-6 mt-[10px]`}>
          <ScrollView style={tw`h-[100px]`}>
            <View style={tw`py-3`}>
              <Text
                style={[globalStyles.Small_Note_B, {color: colors.MainFont}]}>
                {t('accountOpening.getting_ready.app_process')}
              </Text>
            </View>
            <View>
              {process_data.map((r, idx) => (
                <>
                  <ProcessCard
                    key={idx}
                    icon={r.icon}
                    context={r.icontext}
                    items={r.textitems}
                    clickable={!!r.action}
                    action={r.action ?? ''}
                  />
                  {idx !== process_data.length - 1 && (
                    <View style={styles.dashedBorder} key={idx + 'border'}>
                      <Icon
                        style={[tw`text-brand3`, styles.arrowIcon]}
                        name="menu-down"
                      />
                    </View>
                  )}
                </>
              ))}
            </View>
            <View style={tw`py-5`}>
              <Button
                label={t('common.start')}
                onPress={() => goto()}
                disabled={!accessToken}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dashedBorder: {
    borderBottomWidth: 1,
    borderColor: '#9D9D9D',
    borderStyle: Platform.OS === 'ios' ? 'solid' : 'dashed',
  },
  arrowIcon: {
    fontSize: 32,
    marginBottom: -18,
    backgroundColor: 'white',
    width: 35,
    marginLeft: 18,
    alignItems: 'center',
    textAlign: 'center',
  },
  font_32: {
    fontSize: 32,
  },
  font_28: {
    fontSize: 28,
  },
  font_24: {
    fontSize: 24,
  },
});

export default AccountGettingReady;
