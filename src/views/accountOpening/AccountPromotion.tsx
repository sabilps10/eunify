import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Pressable,
  Text,
  View,
  Platform,
  Linking,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import InputText from '../../components/forms/inputs/InputText';
import Button from '../../components/buttons/Button';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {ScrollView} from 'react-native-gesture-handler';
import {
  setPromotion,
  confirmIndividual,
  validatePromotion,
} from '../../utils/api/apiAccountOpening';
import moment from 'moment';
import {entityId} from '../../config/constants';
import {getPromotionList} from '../../utils/api/apiGeneralAccountOpening';
// import {debounce} from 'lodash';

interface AccountPromotionProps {
  navigation: any;
}

type FormDataType = {
  promo_code: string;
  ref_code: string;
};

const AccountPromotion: React.FC<AccountPromotionProps> = ({navigation}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const {width, height} = Dimensions.get('window');
  const dispatch = useDispatch();

  const {currentToken} = useSelector((state: State) => state.cognito);
  const {individual} = useSelector((state: State) => state.accountOpening);

  const {
    SetPersonalInformation,
    SetContactInformation,
    SetFinancialInformation,
    SetTradingExperience,
    SetIndividual,
    SetPromotions,
  } = bindActionCreators(AccountOpeningCreators, dispatch);

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const [activeSlide, setActiveSlide] = useState<number>(0);

  const [promotionList, setPromotionList] = useState<any>([]);

  const [formData, setFormData] = useState<FormDataType>({
    promo_code: '',
    ref_code: '',
  });

  const [formRef, setFormRef] = useState<FormDataType>({
    promo_code: '',
    ref_code: '',
  });

  const [errors, setErrors] = useState<string[] | []>([]);

  const onPressBack = () => {
    navigation.goBack();
  };

  // const handleRef = (val: string, name: string) => {
  //   setFormRef({...formRef, [name]: val ?? ''});
  // };

  const handleFormChange = (val: string, name: string) => {
    setFormData({...formData, [name]: val ?? ''});

    if (name === 'promo_code' && errors.length > 0) {
      setErrors(errors.filter(el => el !== name));
    } else {
      setErrors(errors.filter(el => el !== name));
    }
  };

  const handleNext = async () => {
    SetIsShowLoading(true);
    const res = [];

    if (formData.promo_code || formData.ref_code) {
      const key =
        formData.ref_code.substring(0, 2) === 'R-'
          ? 'memberCode'
          : 'referralCode';

      await validatePromotion({
        token: currentToken,
        payload: {
          entityId: entityId === 'EMX' ? 'XPro' : 'EIEHK',
          ...(formData.promo_code && {promotionCode: formData.promo_code}),
          ...(formData.ref_code && {[key]: formData.ref_code}),
        },
        onSuccess: data => {
          if (formData.promo_code && data.response.promotionCode !== 'C001') {
            res.push('promo_code');
          }

          if (formData.ref_code && data.response[key] !== 'C001') {
            res.push('ref_code');
          }
        },
        onFailed: () => {},
        // setLoading: SetIsShowLoading,
      });
    }

    if (res.length > 0) {
      setErrors(res);
      SetIsShowLoading(false);
      return;
    } else {
      await setPromotion({
        token: currentToken,
        payload: {
          customerId: individual.id,
          promoCode: formData.promo_code,
          upperIbAcc: formData.ref_code,
          isAdminPortal: false,
        },
        onSuccess: () => {
          confirmIndividual({
            token: currentToken,
            endpoint: individual?.id,
            payload: {
              isEkyc: false, // temp hardcode
              isAdminPortal: false,
            },
            onSuccess: data => {
              SetIndividual(data?.response);
              navigation.navigate('AccountApplicationStatus');
              SetIsShowLoading(false);

              SetPersonalInformation({
                legalLastNameEn: data?.response.legalLastNameEn,
                legalFirstAndMiddleNameEn:
                  data?.response?.legalFirstAndMiddleNameEn,
                legalLastName: data?.response.legalLastName,
                legalFirstAndMiddleName:
                  data?.response?.legalFirstAndMiddleName,
                gender: data?.response?.gender,
                jurisdictionOfResidence: data?.response.jurisdictionOfResidence,
                jurisdiction: data?.response.jurisdiction,
                dateOfBirth: new Date(
                  moment(data?.response?.aoOcrIdDoc.dob).format('YYYY-MM-DD'),
                ),
                identificationType: data?.response?.aoOcrIdDoc.idDocType,
                identificationNo: data?.response?.aoOcrIdDoc.number,
                identificationExpiryDate: new Date(
                  moment(data?.response?.identificationExpiryDate).format(
                    'YYYY-MM-DD',
                  ),
                ),
                usCitizen: data?.response.usCitizen,
                efsgRewardProgram: data?.response.efsgRewardProgram,
              });

              SetContactInformation({
                customerId: data?.response?.id,
                addressLine1: data?.response?.addressLine1,
                addressLine2: data?.response?.addressLine2,
                city: data?.response?.city,
                province: data?.response?.province,
                postcode: data?.response?.postcode,
                jurisdictionOfResidence: data?.response.jurisdictionOfResidence,
              });

              SetFinancialInformation({
                customerId: data?.response?.id,
                employmentStatus: data?.response.employmentStatus,
                industrial: data?.response.industrialOther
                  ? `${t('accountOpening.purpose.OTHER')}: ${
                      data?.response.industrialOther
                    }`
                  : data?.response.industrial,
                jobTitle: data?.response.jobTitleOther
                  ? `${t('accountOpening.purpose.OTHER')}: ${
                      data?.response.jobTitleOther
                    }`
                  : data?.response.jobTitle,
                annualIncome: data?.response.annualIncome,
                liquidNetworth: data?.response.liquidNetworth,
                jurisdictionOfTaxResidence:
                  data?.response.jurisdictionOfTaxResidence,
                sourceOfFunds: data?.response.sourceOfFundsOther
                  ? `${t('accountOpening.purpose.OTHER')}: ${
                      data?.response.sourceOfFundsOther
                    }`
                  : data?.response.sourceOfFunds,
                haveTin: data?.response.haveTin,
                tinNumber: data?.response.tinNumber,
              });

              SetTradingExperience({
                customerId: data?.response?.id,
                fiveOrMorTransactionLastThreeYears:
                  data?.response.fiveOrMorTransactionLastThreeYears,
                tradingExperience: data?.response.yearsOfTrading,
                tradingFrequency: data?.response.frequencyOfTrading,
                accountOpeningPurpose: data?.response.accountOpeningPurposeOther
                  ? `${t('accountOpening.purpose.OTHER')}: ${
                      data?.response.accountOpeningPurposeOther
                    }`
                  : data?.response.purposeOfTrading,
              });

              SetPromotions({
                promocode: formData.promo_code,
                title: formData.promo_code
                  ? promotionList.find(el => el.code === formData.promo_code)
                      .title
                  : '',
              });
            },
            onFailed: () => {
              SetIsShowLoading(false);
            },
            setLoading: () => {},
          });
        },
        onFailed: () => {
          SetIsShowLoading(false);
        },
        setLoading: () => {},
      });
    }
  };

  // const validate = () => {
  //   if (formData.promo_code || formData.ref_code) {
  //     validatePromotion({
  //       token: currentToken,
  //       payload: {
  //         entityId: entityId === 'EMX' ? 'XPro' : 'EIEHK',
  //         ...(formData.promo_code && {promotionCode: formData.promo_code}),
  //         ...(formData.ref_code && {referralCode: formData.ref_code}),
  //       },
  //       onSuccess: data => {
  //         const res = [];

  //         if (formData.promo_code && data.response.promotionCode !== 'C001') {
  //           res.push('promo_code');
  //         }

  //         if (formData.ref_code && data.response.referralCode !== 'C001') {
  //           res.push('ref_code');
  //         }

  //         setErrors(res);
  //       },
  //       onFailed: () => {},
  //       setLoading: SetIsShowLoading,
  //     });
  //   }

  //   if (formData.promo_code === '') {
  //     setErrors(errors.filter(el => el !== 'promo_code'));
  //   }

  //   if (formData.ref_code === '') {
  //     setErrors(errors.filter(el => el !== 'ref_code'));
  //   }
  // };

  // useEffect(() => {
  //   validate();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [formRef]);

  useEffect(() => {
    SetIsShowLoading(true);
    getPromotionList({
      token: currentToken,
      onSuccess: data => {
        setPromotionList(data);
        SetIsShowLoading(false);
      },
      onFailed: () => {},
    });
  }, []);

  const PromotionCard = ({item, index}) => {
    return (
      <View key={index} style={[tw`flex flex-col items-center`]}>
        {item.imagePath && (
          <Pressable onPress={() => Linking.openURL(item.urlPath)}>
            <Image
              source={{uri: item.imagePath}}
              style={[{width: width - 32, height: 158, borderRadius: 12}]}
            />
          </Pressable>
        )}
        <Pressable
          onPress={() => {
            setFormData({...formData, promo_code: item.code});
            setFormRef({...formRef, promo_code: item.code});
            setErrors([]);
          }}
          style={[
            tw`flex flex-row justify-between items-center`,
            {
              width: width - 32,
              padding: 12,
              backgroundColor: colors.Grey0,
              marginTop: 5,
              borderRadius: 12,
            },
          ]}>
          <Text style={globalStyles.Big_Text_B}>
            {t('accountOpening.promotion.code')}: {item.code}
          </Text>
          <View style={tw`flex flex-row items-center`}>
            <Text style={globalStyles.Big_Text}>
              {t('accountOpening.promotion.apply')}
            </Text>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color={colors.Brand3}
              name="chevron-right"
              onPress={() => {
                setFormData({...formData, promo_code: item.code});
                setFormRef({...formRef, promo_code: item.code});
                setErrors([]);
              }}
            />
          </View>
        </Pressable>
      </View>
    );
  };

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.White,
          flexDirection: 'column',
          justifyContent: 'space-between',
          height,
        }}>
        <View>
          <SafeAreaView edges={['top', 'right', 'left']}>
            <View
              style={[
                tw`flex flex-row items-center`,
                {
                  backgroundColor: colors.White,
                  paddingHorizontal: 16,
                  paddingBottom: 16,
                },
              ]}>
              <Icon
                // eslint-disable-next-line react-native/no-inline-styles
                style={{fontSize: 28}}
                color="black"
                name="chevron-left"
                onPress={onPressBack}
              />
              <Text style={[globalStyles.H4, {color: colors.Brand2}]}>
                {t('accountOpening.promotion.title')}
              </Text>
            </View>
          </SafeAreaView>
        </View>
        <ScrollView>
          <View>
            <Carousel
              data={promotionList}
              sliderWidth={width}
              itemWidth={width}
              layout="default"
              onSnapToItem={index => setActiveSlide(index)}
              renderItem={({item, index}) => (
                <PromotionCard item={item} index={index} />
              )}
            />
            <Pagination
              dotsLength={promotionList.length}
              activeDotIndex={activeSlide}
              containerStyle={{marginTop: -20}}
              dotStyle={{
                backgroundColor: colors.Text,
              }}
              inactiveDotOpacity={0.2}
              inactiveDotScale={0.5}
            />
          </View>
          <View style={{marginTop: 60, paddingHorizontal: 16}}>
            <InputText
              label={t('accountOpening.promotion.promo_code')}
              name="promo_code"
              value={formData.promo_code}
              onChange={handleFormChange}
              // onDebounce={debounce(handleRef, 1500)}
              errors={errors}
              errorMessage={t('accountOpening.promotion.invalid_code')}
            />
            <InputText
              label={t('accountOpening.promotion.ref_code')}
              name="ref_code"
              value={formData.ref_code}
              onChange={handleFormChange}
              // onDebounce={debounce(handleRef, 1500)}
              errors={errors}
              errorMessage={t('accountOpening.promotion.invalid_code')}
              wrapperStyle={{marginTop: 28}}
            />
          </View>
          <View style={[tw`py-5 px-[16px]`]}>
            <Button
              label={t('common.next')}
              onPress={handleNext}
              disabled={errors.length > 0}
            />
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AccountPromotion;
