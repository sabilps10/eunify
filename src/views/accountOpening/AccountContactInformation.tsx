import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  Platform,
  BackHandler,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import {ScrollView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import InputText from '../../components/forms/inputs/InputText';
import Dropdown from '../../components/forms/inputs/Dropdown';
import DatePicker from '../../components/forms/inputs/DatePicker';
import ProgressHeader from '../../components/header/ProgressHeader';
import Button from '../../components/buttons/Button';
import {entityId} from '../../config/constants';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {ContactInformation} from '../../redux/accountOpening/type';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {RegionContent} from '../../redux/state/types';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {updateContactInformation} from '../../utils/api/apiAccountOpening';
import {BottomSheetCreators} from '../../redux/BottomSheet/actionCreators';
import reactotron from 'reactotron-react-native';

interface AccountContactInformationProps {
  navigation: any;
  route?: any;
}

type FormItem = {
  label: string;
  name: keyof ContactInformation;
  required?: boolean;
  disabled?: boolean;
  type: 'input-text' | 'dropdown' | 'date-picker';
  options?: any[];
  dropdownType?: 'bottomsheet' | 'page' | 'country' | 'other';
  tooltip?: string;
  maxLength?: number;
};

const RenderFormByType = ({
  label,
  name,
  required,
  disabled,
  type,
  tooltip,
  options,
  dropdownType,
  formData,
  handleFormChange,
  errors,
  maxLength,
}: FormItem & {
  formData: ContactInformation;
  handleFormChange: (val: any, name: string) => void;
  errors: any[];
}) => {
  switch (type) {
    case 'input-text':
      return (
        <InputText
          value={formData[name as keyof ContactInformation] as string}
          name={name}
          label={label}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          editAble={!disabled}
          errors={errors}
          maxLength={maxLength}
        />
      );
    case 'dropdown':
      return (
        <Dropdown
          value={formData[name as keyof ContactInformation]}
          label={label}
          name={name}
          onSelect={handleFormChange}
          options={options ?? []}
          required={required}
          disabled={disabled}
          wrapperStyle={tw`mb-[28px]`}
          errors={errors}
          type={dropdownType}
        />
      );
    case 'date-picker':
      return (
        <DatePicker
          value={formData[name as keyof ContactInformation]}
          label={label}
          name={name}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          disabled={disabled}
          tooltip={tooltip}
          errors={errors}
        />
      );
  }
};

const AccountContactInformation: React.FC<AccountContactInformationProps> = ({
  navigation,
  route,
}) => {
  const {t} = useTranslation();

  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const dispatch = useDispatch();

  const {currentToken} = useSelector((state: State) => state.cognito);

  const regionOption = useSelector(
    (state: State) => state.state.regionDataList,
  );

  const contactInformationState = useSelector(
    (state: State) => state.accountOpening.contactInformation,
  );

  const personalInformationState = useSelector(
    (state: State) => state.accountOpening.personalInformation,
  );

  const {SetContactInformation, SetPersonalInformation} = bindActionCreators(
    AccountOpeningCreators,
    dispatch,
  );

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const {isOpen: isOpenBottomSheet} = useSelector(
    (state: State) => state.bottomSheet,
  );
  const {CloseBottomSheet} = bindActionCreators(BottomSheetCreators, dispatch);

  const [errors, setErrors] = useState<any[]>([]);
  const [formData, setFormData] = useState<ContactInformation>({
    addressLine1: '',
    addressLine2: '',
    city: '',
    province: '',
    postcode: '',
    jurisdictionOfResidence: '',
  });

  const formList: FormItem[] = [
    {
      label: t('accountOpening.contact_information.address_1'),
      name: 'addressLine1',
      required: true,
      disabled: false,
      type: 'input-text',
      maxLength: 200,
    },
    {
      label: t('accountOpening.contact_information.address_2'),
      name: 'addressLine2',
      required: false,
      disabled: false,
      type: 'input-text',
      maxLength: 200,
    },
    {
      label: t('accountOpening.contact_information.city'),
      name: 'city',
      required: true,
      disabled: false,
      type: 'input-text',
      maxLength: 200,
    },
    {
      label: t('accountOpening.contact_information.province'),
      name: 'province',
      required: false,
      disabled: false,
      type: 'input-text',
      maxLength: 200,
    },
    {
      label: t('accountOpening.contact_information.postcode'),
      name: 'postcode',
      required: false,
      disabled: false,
      type: 'input-text',
      maxLength: 200,
    },
    {
      label: t('accountOpening.contact_information.country_region'),
      name: 'jurisdictionOfResidence',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'country',
      options: regionOption.map((el: RegionContent) => ({
        value: el.regionCode,
        label: t(`region.${el.regionCode}`),
      })),
    },
  ];

  const handleFormChange = (val: any, name: string) => {
    setFormData({...formData, [name]: val});
  };

  const onPressBack = () => {
    navigation.goBack();
    return true;
  };

  const handleSave = () => {
    const error: string[] = [];
    formList.map((el: FormItem) => {
      if (el.required && !formData[el.name]) {
        error.push(el.name);
      }
    });

    if (error.length > 0) {
      setErrors(error);
      return;
    }

    setErrors([]);
    SetContactInformation(formData);
    SetPersonalInformation({
      ...personalInformationState,
      jurisdictionOfResidence: formData.jurisdictionOfResidence,
    });

    updateContactInformation({
      token: currentToken,
      payload: {...formData, isAdminPortal: false},
      onSuccess: () => {
        if (route?.params?.lastScreen) {
          navigation.navigate(route?.params?.lastScreen);
        } else {
          if (entityId === 'EIE') {
            navigation.navigate('AccountFinancialInformation');
          } else {
            navigation.navigate('AccountReviewApplication');
          }
        }
      },
      onFailed: () => {},
      setLoading: SetIsShowLoading,
    });
  };

  useEffect(() => {
    reactotron.log(contactInformationState);
    setFormData(contactInformationState);
  }, [contactInformationState]);

  useFocusEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        if (isOpenBottomSheet) {
          CloseBottomSheet();
        } else {
          if (route?.params?.lastScreen) {
            navigation.navigate('AccountReviewApplication');
          } else {
            onPressBack();
          }
        }
        return true;
      },
    );

    return () => backHandler.remove();
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1, backgroundColor: colors.Brand3}}>
      <View style={{height: '15%'}}>
        <SafeAreaView edges={['top', 'left', 'right']}>
          <View style={tw`flex flex-row items-center px-[16px]`}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBack}
            />
            <Text
              style={[tw`ml-[15px]`, globalStyles.H4, {color: colors.White}]}>
              {t('accountOpening.trading_account_opening')}
            </Text>
          </View>
        </SafeAreaView>
        <ProgressHeader currentStep="2" entity={entityId} />
      </View>
      <View
        style={[
          tw`rounded-t-xl`,
          {height: '85%', backgroundColor: colors.White},
        ]}>
        <ScrollView>
          <View
            style={[
              tw`flex flex-col flex-grow px-[16px] pt-6 justify-between mt-[10px]`,
            ]}>
            <View>
              <Text
                style={[
                  tw`mb-[24px]`,
                  globalStyles.H4,
                  {color: colors.secondaryBtn},
                ]}>
                {t('accountOpening.contact_information.title')}
              </Text>
              {formList.map((el: FormItem, idx: number) => (
                <RenderFormByType
                  {...el}
                  key={idx}
                  formData={formData}
                  handleFormChange={handleFormChange}
                  errors={errors}
                />
              ))}
            </View>
            <View style={[tw`py-2 mt-5`]}>
              <Button label={t('common.save_and_next')} onPress={handleSave} />
            </View>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AccountContactInformation;
