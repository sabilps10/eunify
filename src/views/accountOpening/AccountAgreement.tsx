import React, {useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {useTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import tw from '../../utils/tailwind';
import {makeGlobalStyles} from '../../styles/styles';
import Button from '../../components/buttons/Button';
import {entityId} from '../../config/constants';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {bindActionCreators} from 'redux';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {updateTradingAgreement} from '../../utils/api/apiAccountOpening';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';

interface AccountAgreementProps {
  navigation: any;
}

const AccountAgreement: React.FC<AccountAgreementProps> = ({navigation}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const dispatch = useDispatch();

  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const {currentToken} = useSelector((state: State) => state.cognito);
  const {id} = useSelector((state: State) => state.accountOpening.individual);

  const {SetIndividual} = bindActionCreators(AccountOpeningCreators, dispatch);

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const onPressBack = () => {
    navigation.goBack();
  };

  const handleRegister = async () => {
    await updateTradingAgreement({
      token: currentToken,
      payload: {
        customerId: id,
        isAdminPortal: false,
        isAccountAgreementPage: true,
      },
      onSuccess: data => {
        SetIndividual(data.response);
        navigation.navigate('AccountGettingReady');
      },
      onFailed: () => {},
      setLoading: SetIsShowLoading,
    });
  };

  const handleScroll = event => {
    if (!isButtonDisabled) return;
    const {contentOffset, layoutMeasurement, contentSize} = event.nativeEvent;
    const isBottomReached =
      Math.floor(layoutMeasurement.height + contentOffset.y) >=
      Math.floor(contentSize.height);
    setIsButtonDisabled(!isBottomReached);
  };

  return (
    <View style={[tw`flex flex-col h-full bg-brand3`]}>
      <View style={{flex: 0.35}}>
        <SafeAreaView edges={['top', 'left', 'right']}>
          <View style={tw`flex flex-col  px-[16px]`}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBack}
            />
            <Text
              style={[
                tw`ml-[15px] mt-[20px]`,
                globalStyles.H1,
                {color: colors.White},
              ]}>
              {t('accountOpening.agreement.title')}
            </Text>
          </View>
        </SafeAreaView>
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.White,
          borderTopLeftRadius: 12,
          borderTopRightRadius: 12,
        }}>
        <ScrollView
          style={{
            flex: 0.65,
            marginTop: 16,
            backgroundColor: 'white',
            paddingHorizontal: 16,
            borderTopLeftRadius: 12,
            borderTopRightRadius: 12,
          }}
          onScroll={handleScroll}>
          <View>
            <Text
              style={[
                tw`ml-[15px] mt-[20px]`,
                globalStyles.Body_Text,
                {color: colors.Black},
              ]}>
              {t(`accountOpening.agreement.content_${entityId}`)}
            </Text>
          </View>
        </ScrollView>
        <View style={[tw`p-[16px] w-[100%]`]}>
          <Button
            label={t('common.confirm')}
            onPress={handleRegister}
            disabled={isButtonDisabled}
          />
        </View>
      </View>
    </View>
  );
};

export default AccountAgreement;
