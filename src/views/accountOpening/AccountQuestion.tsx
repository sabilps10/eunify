import React, {useCallback, useEffect, useState} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useTranslation} from 'react-i18next';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Dropdown from '../../components/forms/inputs/Dropdown';
import tw from '../../utils/tailwind';
import {makeGlobalStyles} from '../../styles/styles';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {RegionContent} from '../../redux/state/types';
import Picker from '../../components/forms/inputs/Picker';
import {entityId} from '../../config/constants';
import Button from '../../components/buttons/Button';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  AccountQuestionType,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ResponseAccountOpening,
} from '../../redux/accountOpening/type';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {Auth} from 'aws-amplify';
import {
  getIndividual,
  setApplicantInformation,
  updateApplicantInformation,
} from '../../utils/api/apiAccountOpening';
import moment from 'moment';
import {getTradeproductList} from '../../utils/api/apiGeneralAccountOpening';

interface AccountQuestionProps {
  route: any;
  navigation: any;
}

const AccountQuestion: React.FC<AccountQuestionProps> = ({
  route,
  navigation,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const dispatch = useDispatch();

  const {username} = useSelector((state: State) => state.cognitoTemp);

  const {currentToken} = useSelector((state: State) => state.cognito);

  const {
    SetAccountQuestion,
    SetPersonalInformation,
    SetContactInformation,
    SetFinancialInformation,
    SetTradingExperience,
    SetIndividual,
  } = bindActionCreators(AccountOpeningCreators, dispatch);
  const {SetIsShowMessageDialog, SetIsShowLoading} = bindActionCreators(
    StateActionCreators,
    dispatch,
  );
  const regionOption = useSelector(
    (state: State) => state.state.regionDataList,
  );
  const countryList = useSelector((state: State) => state.state.countryList);

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const phoneNumber = useSelector(
    (state: State) => state.account.account.MobileNumber,
  );

  const [products, setProducts] = useState<any>([]);
  const [formData, setFormData] = useState<AccountQuestionType>({
    jurisdictionOfResidence: '',
    hasHKBankAccount: '',
    tradeProducts: [],
  });

  const [formErrors, setFormErrors] = useState<string[]>([]);

  const onPressBack = () => {
    navigation.goBack();
  };

  const handleChange = (val: string, name: keyof AccountQuestionType) => {
    setFormData({...formData, [name]: val});
  };

  const handleConfirm = async () => {
    if (entityId === 'EMX' && formData.hasHKBankAccount === 'true') {
      SetIsShowMessageDialog({
        title: t('Error'),
        message: t('accountOpening.question.have_hk_bank_error'),
        isShowDialog: true,
      });
    } else {
      const error: string[] = [];
      Object.keys(formData).map(el => {
        if (!formData[el as keyof typeof formData]) {
          error.push(el);
        }
      });

      if (error.length > 0) {
        setFormErrors(error);
        return;
      }

      let phone_number = '';

      const session = await Auth.currentSession();
      const sessionPayload = session.getIdToken().payload;

      if (phoneNumber) {
        phone_number = phoneNumber;
      } else {
        phone_number = sessionPayload['phone_number'];
      }

      const countryCode = getCountryCode(phone_number);

      const payload = {
        entity: entityId === 'EIE' ? 'EIEHK' : 'XPro',
        sumsubEkyc: '',
        email: route?.params?.email ?? sessionPayload['email'],
        mobileCountryCode: countryCode,
        mobile: phone_number.substring(countryCode.length),
        tradeProduct: JSON.stringify(formData.tradeProducts),
        jurisdictionOfResidence: formData.jurisdictionOfResidence,
        hasHKBankAccount: formData.hasHKBankAccount,
        tradingAccountType: '',
        promoCode: '',
        upperIbAcc: '',
        remarks: '',
        isAdminPortal: false,
        cognitoId: sessionPayload['cognito:username'],
      };

      await updateApplicantInformation({
        token: currentToken,
        endpoint: sessionPayload['cognito:username'],
        payload: payload,
        onSuccess: data => {
          navigation.navigate('RecommendedTradingAccount');
          SetIndividual(data.response);
          setFormErrors([]);
          SetAccountQuestion(formData);
        },
        onFailed: () => {},
        setLoading: SetIsShowLoading,
      });
    }
  };

  const getCountryCode = useCallback(
    phoneNumber => {
      let countryCode = regionOption.find(el =>
        phoneNumber.includes(`+${el.countryCode}`),
      );
      return `+${countryCode.countryCode}`;
    },
    [regionOption],
  );

  const applicantInformation = useCallback(async () => {
    let phone_number = '';
    let cognitoId = '';

    const session = await Auth.currentSession();
    const sessionPayload = session.getIdToken().payload;
    cognitoId = sessionPayload['cognito:username'];

    if (phoneNumber) {
      phone_number = phoneNumber;
    } else {
      phone_number = sessionPayload['phone_number'];
    }

    const countryCode = getCountryCode(phone_number);
    const payload = {
      entity: entityId === 'EIE' ? 'EIEHK' : 'XPro',
      sumsubEkyc: '',
      email: route?.params?.email ?? sessionPayload['email'],
      mobileCountryCode: countryCode,
      mobile: phone_number.substring(countryCode.length),
      isAdminPortal: false,
      cognitoId,
    };

    setApplicantInformation({
      token: route?.params?.token ?? currentToken,
      payload,
      onSuccess: data => {
        SetPersonalInformation({
          customerId: data?.response?.id,
          legalLastNameEn: data?.response?.legalLastNameEn,
          legalFirstAndMiddleNameEn: data?.response?.legalFirstAndMiddleNameEn,
          legalLastName: data?.response?.legalLastName,
          legalFirstAndMiddleName: data?.response?.legalFirstAndMiddleName,
          gender: data?.response?.gender,
          jurisdictionOfResidence: data?.response?.jurisdictionOfResidence,
          dateOfBirth: new Date(data?.response?.dateOfBirth),
          jurisdiction: data?.response?.jurisdiction,
          identificationType: data?.response?.identificationType,
          identificationNo: data?.response?.identificationNo,
          identificationExpiryDate: new Date(
            data?.response?.identificationExpiryDate,
          ),
          usCitizen: data?.response?.usCitizen,
          efsgRewardProgram: data?.response?.efsgRewardProgram,
        });

        SetContactInformation({
          customerId: data?.response?.id,
          addressLine1: data?.response?.addressLine1,
          addressLine2: data?.response?.addressLine2,
          city: data?.response?.city,
          province: data?.response?.province,
          postcode: data?.response?.postcode,
          jurisdictionOfResidence: data?.response?.jurisdictionOfResidence,
        });

        SetFinancialInformation({
          customerId: data?.response?.id,
          employmentStatus: data?.response?.employmentStatus,
          industrial: data?.response?.industrialOther
            ? `${t('accountOpening.purpose.OTHER')}: ${
                data?.response?.industrialOther
              }`
            : data?.response.industrial,
          jobTitle: data?.response.jobTitleOther
            ? `${t('accountOpening.purpose.OTHER')}: ${
                data?.response.jobTitleOther
              }`
            : data?.response.jobTitle,
          annualIncome: data?.response?.annualIncome,
          liquidNetworth: data?.response?.liquidNetworth,
          jurisdictionOfTaxResidence:
            data?.response?.jurisdictionOfTaxResidence,
          sourceOfFunds: data?.response.sourceOfFundsOther
            ? `${t('accountOpening.purpose.OTHER')}: ${
                data?.response.sourceOfFundsOther
              }`
            : data?.response.sourceOfFunds,
          haveTin: data?.response?.haveTin,
          tinNumber: data?.response?.tinNumber,
        });

        SetTradingExperience({
          customerId: data?.response?.id,
          fiveOrMorTransactionLastThreeYears:
            data?.response?.fiveOrMorTransactionLastThreeYears,
          tradingExperience: data?.response?.yearsOfTrading,
          tradingFrequency: data?.response?.frequencyOfTrading,
          accountOpeningPurpose: data?.response.accountOpeningPurposeOther
            ? `${t('accountOpening.purpose.OTHER')}: ${
                data?.response.accountOpeningPurposeOther
              }`
            : data?.response.purposeOfTrading,
        });
        SetIndividual(data.response);
      },
      onFailed: error => {
        console.log(JSON.stringify(error), '<<');
        SetIsShowMessageDialog({
          title: t('Error'),
          message: t('common.unknownError'),
          isShowDialog: true,
        });
      },
      setLoading: SetIsShowLoading,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    SetContactInformation,
    SetFinancialInformation,
    SetIsShowMessageDialog,
    SetPersonalInformation,
    SetTradingExperience,
    currentToken,
    getCountryCode,
    t,
    username,
    route?.params?.token,
    route?.params?.email,
  ]);

  const checkIndividual = useCallback(async () => {
    const session = await Auth.currentSession();
    const sessionPayload = session.getIdToken().payload;
    getIndividual({
      token: route?.params?.token ?? currentToken,
      payload: {
        username:
          sessionPayload['cognito:username'] ??
          route?.params?.email ??
          username,
      },
      onSuccess: data => {
        if (data?.response !== null) {
          if (data.response.status === 'DRAFT') {
            SetPersonalInformation({
              customerId: data?.response?.id,
              ...data.response,
              dateOfBirth: data?.response?.dateOfBirth
                ? new Date(
                    moment(data?.response?.dateOfBirth).format('YYYY-MM-DD'),
                  )
                : null,
              identificationExpiryDate: data?.response?.identificationExpiryDate
                ? new Date(
                    moment(data?.response?.identificationExpiryDate).format(
                      'YYYY-MM-DD',
                    ),
                  )
                : null,
            });

            SetContactInformation({
              customerId: data?.response?.id,
              ...data.response,
            });

            SetFinancialInformation({
              customerId: data?.response?.id,
              ...data.response,
              tinNumber: data?.response?.tinNumber,
            });

            SetTradingExperience({
              customerId: data?.response?.id,
              ...data.response,
            });
            SetIndividual(data.response);
          } else {
            applicantInformation();
          }
        } else if (data.response.status === 'REJECTED') {
          applicantInformation();
        } else {
          navigation.navigate('AccountStatus');
        }
      },
      onFailed: () => {
        applicantInformation();
      },
      setLoading: SetIsShowLoading,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    SetContactInformation,
    SetFinancialInformation,
    SetPersonalInformation,
    SetTradingExperience,
    applicantInformation,
    currentToken,
    username,
    route?.params?.token,
    route?.params?.email,
  ]);

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  useEffect(() => {
    checkIndividual();

    getTradeproductList({
      token: route?.params?.token ?? currentToken,
      onSuccess: data => {
        const option = data.response.map(el => ({
          label:
            lang === 'en' ? el.label : lang === 'tc' ? el.labelTc : el.labelSc,
          value: el.code,
        }));
        setProducts(option);
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1, backgroundColor: colors.Brand3}}>
      <View style={{paddingHorizontal: 16}}>
        <View>
          <SafeAreaView edges={['top', 'left', 'right']}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBack}
            />
          </SafeAreaView>
        </View>
        <View>
          <View style={tw`pt-5`}>
            <Text style={[globalStyles.H1, tw`text-white`]}>
              {t('accountOpening.question.title')}
            </Text>
          </View>
          <View style={tw`py-2`}>
            <Text style={[globalStyles.Body_Text, {color: colors.MainFont}]}>
              {t('accountOpening.question.subtitle')}
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.White,
          borderTopLeftRadius: 12,
          borderTopRightRadius: 12,
        }}>
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            backgroundColor: colors.White,
            paddingHorizontal: 16,
            borderTopLeftRadius: 12,
            borderTopRightRadius: 12,
          }}>
          <View style={{flex: 0.9, flexGrow: 1, paddingBottom: 10}}>
            <View style={tw`mt-[25px]`}>
              <Dropdown
                label={t('accountOpening.question.country_jurisdiction_region')}
                name="jurisdictionOfResidence"
                value={formData.jurisdictionOfResidence}
                options={countryList?.map((el: any) => ({
                  value: el.value,
                  label:
                    lang === 'en'
                      ? el.NameEn
                      : lang === 'tc'
                      ? el.nameTc
                      : el.nameSc,
                }))}
                onSelect={handleChange}
                type="country"
                errors={formErrors}
                required
              />
            </View>
            <View style={tw`mt-[25px]`}>
              <Picker
                label={t('accountOpening.question.have_hk_bank')}
                name="hasHKBankAccount"
                value={formData.hasHKBankAccount}
                options={[
                  {
                    value: 'true',
                    label: t('accountOpening.yes'),
                  },
                  {value: 'false', label: t('accountOpening.no')},
                ]}
                onPick={handleChange}
                errors={formErrors}
                required
              />
            </View>
            <View style={tw`mt-[25px]`}>
              <Picker
                label={t('accountOpening.question.product_interest')}
                name="tradeProducts"
                value={formData.tradeProducts}
                options={products}
                onPick={handleChange}
                multiplePick
                required
                errors={formErrors}
              />
            </View>
          </View>
          <View style={[tw`py-5 w-[100%]`, {flex: 0.1, flexGrow: 0.15}]}>
            <Button
              label={t('common.confirm')}
              onPress={handleConfirm}
              disabled={
                formData.hasHKBankAccount === null ||
                formData.tradeProducts.length === 0 ||
                !formData.jurisdictionOfResidence
              }
            />
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AccountQuestion;
