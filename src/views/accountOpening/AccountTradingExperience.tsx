import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  View,
  BackHandler,
} from 'react-native';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import ProgressHeader from '../../components/header/ProgressHeader';
import {entityId} from '../../config/constants';
import Picker from '../../components/forms/inputs/Picker';
import Dropdown from '../../components/forms/inputs/Dropdown';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {TradingExperience} from '../../redux/accountOpening/type';
import Button from '../../components/buttons/Button';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {
  getPurposeOpeningList,
  getTradingDurationList,
  getTradingFrequencyList,
} from '../../utils/api/apiGeneralAccountOpening';
import {updateTradingExperience} from '../../utils/api/apiAccountOpening';
import {BottomSheetCreators} from '../../redux/BottomSheet/actionCreators';

interface AccountTradingExperienceProps {
  navigation: any;
  route?: any;
}

type FormItem = {
  label: string;
  name: keyof TradingExperience;
  required?: boolean;
  disabled?: boolean;
  type: 'dropdown' | 'picker';
  options?: any[];
  dropdownType?: 'bottomsheet' | 'page' | 'country' | 'other';
  tooltip?: string;
  hide?: boolean;
};

const RenderFormByType = ({
  label,
  name,
  required,
  disabled,
  type,
  options,
  dropdownType,
  formData,
  handleFormChange,
  errors,
  hide = false,
}: FormItem & {
  formData: TradingExperience;
  handleFormChange: (val: any, name: string) => void;
  errors: any[];
}) => {
  if (hide) return null;
  switch (type) {
    case 'dropdown':
      return (
        <Dropdown
          value={formData[name as keyof TradingExperience]}
          label={label}
          name={name}
          onSelect={handleFormChange}
          options={options ?? []}
          required={required}
          disabled={disabled}
          wrapperStyle={tw`mb-[40px]`}
          errors={errors}
          type={dropdownType}
        />
      );
    case 'picker':
      return (
        <Picker
          value={formData[name as keyof TradingExperience]}
          label={label}
          name={name}
          onPick={handleFormChange}
          options={options ?? []}
          required={required}
          errors={errors}
          wrapperStyle={tw`mb-[40px]`}
        />
      );
  }
};

const AccountTradingExperience: React.FC<AccountTradingExperienceProps> = ({
  navigation,
  route,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const dispatch = useDispatch();
  const {height} = Dimensions.get('window');

  const {currentToken} = useSelector((state: State) => state.cognito);

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const tradingExperienceState = useSelector(
    (state: State) => state.accountOpening.tradingExperience,
  );

  const {individual} = useSelector((state: State) => state.accountOpening);

  const {SetTradingExperience} = bindActionCreators(
    AccountOpeningCreators,
    dispatch,
  );

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const {isOpen: isOpenBottomSheet} = useSelector(
    (state: State) => state.bottomSheet,
  );
  const {CloseBottomSheet} = bindActionCreators(BottomSheetCreators, dispatch);

  const [dropdown, setDropdown] = useState<any>({
    tradingDuration: [],
    tradingFrequency: [],
    purpose: [],
  });

  const [errors, setErrors] = useState<any[]>([]);
  const [formData, setFormData] = useState<TradingExperience>({
    fiveOrMorTransactionLastThreeYears: null,
    haveOtherTrade: null,
    tradingExperience: null,
    tradingFrequency: '',
    accountOpeningPurpose: '',
  });

  const formList: FormItem[] = [
    {
      label: t('accountOpening.trading_experience.question_1'),
      name: 'fiveOrMorTransactionLastThreeYears',
      required: true,
      disabled: false,
      type: 'picker',
      options: [
        {value: true, label: t('accountOpening.yes')},
        {value: false, label: t('accountOpening.no')},
      ],
    },
    {
      label: t('accountOpening.trading_experience.question_2'),
      name: 'tradingExperience',
      required:
        !formData.fiveOrMorTransactionLastThreeYears ||
        formData.fiveOrMorTransactionLastThreeYears === 'false'
          ? false
          : true,
      disabled: false,
      hide:
        !formData.fiveOrMorTransactionLastThreeYears ||
        formData.fiveOrMorTransactionLastThreeYears === 'false'
          ? true
          : false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: dropdown.tradingDuration,
    },
    {
      label: t('accountOpening.trading_experience.question_3'),
      name: 'tradingFrequency',
      required:
        !formData.fiveOrMorTransactionLastThreeYears ||
        formData.fiveOrMorTransactionLastThreeYears === 'false'
          ? false
          : true,
      disabled: false,
      hide:
        !formData.fiveOrMorTransactionLastThreeYears ||
        formData.fiveOrMorTransactionLastThreeYears === 'false'
          ? true
          : false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: dropdown.tradingFrequency,
    },
    {
      label: t('accountOpening.trading_experience.question_4'),
      name: 'haveOtherTrade',
      required:
        formData.fiveOrMorTransactionLastThreeYears === false ? true : false,
      disabled: false,
      hide:
        formData.fiveOrMorTransactionLastThreeYears === false ? false : true,
      type: 'picker',
      options: [
        {value: true, label: t('accountOpening.yes')},
        {value: false, label: t('accountOpening.no')},
      ],
    },
  ];

  const onPressBack = () => {
    navigation.goBack();
  };

  const handleFormChange = (val: any, name: string) => {
    setErrors(errors.filter(el => el !== name));
    if (name === 'fiveOrMorTransactionLastThreeYears') {
      setFormData({
        ...formData,
        [name]: val,
        haveOtherTrade: '',
        tradingExperience: '',
        tradingFrequency: '',
      });
    } else {
      setFormData({...formData, [name]: val});
    }
  };

  const handleSave = () => {
    const error: string[] = [];
    formList.map((el: FormItem) => {
      if (typeof formData[el.name] === 'boolean') {
        if (el.required && formData[el.name] === null) {
          error.push(el.name);
        }
      } else {
        if (el.required && !formData[el.name]) {
          error.push(el.name);
        }
      }
    });

    if (error.length > 0) {
      setErrors(error);
      return;
    }

    updateTradingExperience({
      token: currentToken,
      payload: {
        ...formData,
        isAdminPortal: false,
        ...(formData.accountOpeningPurpose &&
          formData.accountOpeningPurpose.includes(
            t('accountOpening.purpose.OTHER'),
          ) && {
            accountOpeningPurposeOther: formData.accountOpeningPurpose
              .split(':')[1]
              .trim(),
            accountOpeningPurpose: 'OTHER',
          }),
        customerId: individual.id,
      },
      onSuccess: () => {
        SetTradingExperience(formData);
        if (route?.params?.lastScreen) {
          navigation.navigate(route?.params?.lastScreen);
        } else {
          navigation.navigate('AccountReviewApplication');
        }
      },
      onFailed: () => {},
      setLoading: SetIsShowLoading,
    });
  };

  useEffect(() => {
    const fetchOptions = async () => {
      const temp: any = {};
      await getTradingDurationList({
        token: currentToken,
        onSuccess: data => {
          const option = data.response.map(el => ({
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
            value: el.code,
          }));
          temp.tradingDuration = option;
        },
      });
      await getTradingFrequencyList({
        token: currentToken,
        onSuccess: data => {
          const option = data.response.map(el => ({
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
            value: el.code,
          }));
          temp.tradingFrequency = option;
        },
      });
      await getPurposeOpeningList({
        token: currentToken,
        onSuccess: data => {
          const option = data.response.map(el => ({
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
            value: el.code,
          }));
          temp.purpose = option;
        },
      });
      setDropdown(temp);
    };
    fetchOptions();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setFormData(tradingExperienceState);
  }, [tradingExperienceState]);

  useFocusEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        if (isOpenBottomSheet) {
          CloseBottomSheet();
        } else {
          if (route?.params?.lastScreen) {
            navigation.navigate('AccountReviewApplication');
          } else {
            onPressBack();
          }
        }
        return true;
      },
    );

    return () => {
      backHandler.remove();
    };
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1, backgroundColor: colors.Brand3}}>
      <View style={{height: '15%', paddingHorizontal: 16}}>
        <SafeAreaView edges={['top', 'left', 'right']}>
          <View style={tw`flex flex-row items-center`}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBack}
            />
            <Text
              style={[tw`ml-[15px]`, globalStyles.H4, {color: colors.White}]}>
              {t('accountOpening.trading_account_opening')}
            </Text>
          </View>
        </SafeAreaView>
        <ProgressHeader currentStep="4" entity={entityId} />
      </View>
      <View
        style={[
          tw`rounded-t-xl`,
          {height: '85%', backgroundColor: colors.Grey0},
        ]}>
        <ScrollView>
          <View
            style={[
              tw`rounded-b-xl rounded-t-xl pt-6`,
              {
                backgroundColor: colors.White,
                paddingHorizontal: 16,
                flex: 1,
              },
            ]}>
            <Text
              style={[
                tw`mb-[24px]`,
                globalStyles.H4,
                {color: colors.secondaryBtn},
              ]}>
              {t('accountOpening.trading_experience.title')}
            </Text>
            {formList.map((el: FormItem, idx: number) => (
              <RenderFormByType
                {...el}
                key={idx}
                formData={formData}
                handleFormChange={handleFormChange}
                errors={errors}
              />
            ))}
            {formData.fiveOrMorTransactionLastThreeYears === false ? (
              <Text
                style={{
                  ...[tw`mb-1`, {color: colors.Grey3}],
                  marginBottom: 40,
                }}>
                {t('accountOpening.trading_experience.disclosure_agreement')}
              </Text>
            ) : null}
          </View>
          <View
            style={[
              tw`flex flex-shrink flex-col justify-between rounded-b-xl rounded-t-xl pt-6 mt-[12px]`,
              {
                backgroundColor: colors.White,
                paddingHorizontal: 16,
                flex: 1,
                height:
                  formData.fiveOrMorTransactionLastThreeYears === 'false' ||
                  !formData.fiveOrMorTransactionLastThreeYears
                    ? height - 335
                    : '100%',
              },
            ]}>
            <Dropdown
              label={t('accountOpening.trading_experience.purpose')}
              name="accountOpeningPurpose"
              value={formData.accountOpeningPurpose}
              required={false}
              type="page"
              onSelect={handleFormChange}
              options={dropdown.purpose}
            />
            <View style={tw`py-5 mt-[20px] justify-end`}>
              <Button label={t('common.save_and_next')} onPress={handleSave} />
            </View>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AccountTradingExperience;
