import React, {useState, useEffect} from 'react';
import {
  Dimensions,
  Linking,
  Pressable,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import moment from 'moment';
import Button from '../../components/buttons/Button';
import {entityId, reward_whatsappUrl, sumsub_url} from '../../config/constants';
import DashedLineView from '../../components/dashedLineView.component';
import {bindActionCreators} from 'redux';
import {StateActionCreators} from '../../redux/state/actionCreator';
import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
import {getSumsubToken} from '../../utils/sumsub';
import axios from 'axios';

interface AccountReviewApplicationProps {
  navigation: any;
}

const AccountReviewApplication: React.FC<AccountReviewApplicationProps> = ({
  navigation,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const {width} = Dimensions.get('window');
  const dispatch = useDispatch();

  const {
    personalInformation,
    contactInformation,
    financialInformation,
    tradingExperience,
    individual,
  }: any = useSelector((state: State) => state.accountOpening);

  const countryOption = useSelector((state: State) => state.state.countryList);

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const [isFinished, setIsFinished] = useState<boolean>(false);
  const [accessToken, setAccessToken] = useState<string>('');

  // const {username} = useSelector((state: State) => state.cognitoTemp);
  // const {currentToken} = useSelector((state: State) => state.cognito);

  const {SetIsShowLoading, SetIsShowMessageDialog} = bindActionCreators(
    StateActionCreators,
    dispatch,
  );

  const getCountryByLang = (country: string) => {
    const selected = countryOption?.find(el => el.value === country);

    if (selected) {
      return selected[
        lang === 'en' ? 'nameEn' : lang === 'tc' ? 'nameTc' : 'nameSc'
      ];
    }
    return null;
  };

  const personalInformationFields = [
    {
      label: t('accountOpening.personal_information.legal_name_en'),
      value:
        !personalInformation?.legalLastNameEn &&
        !personalInformation?.legalFirstAndMiddleNameEn
          ? 'N/A'
          : `${personalInformation?.legalLastNameEn ?? '-'}, ${
              personalInformation?.legalFirstAndMiddleNameEn ?? '-'
            }`,
    },
    {
      label: t('accountOpening.personal_information.legal_name'),
      value:
        !personalInformation?.legalLastName &&
        !personalInformation?.legalFirstAndMiddleName
          ? 'N/A'
          : `${
              personalInformation?.legalLastName
                ? personalInformation?.legalLastName
                : 'N/A'
            } , ${
              personalInformation?.legalFirstAndMiddleName
                ? personalInformation?.legalFirstAndMiddleName
                : 'N/A'
            }`,
    },
    {
      label: t('accountOpening.personal_information.gender'),
      value: personalInformation?.gender
        ? t(
            `accountOpening.gender.${personalInformation?.gender.toLowerCase()}`,
          )
        : 'N/A',
    },
    {
      label: t('accountOpening.personal_information.country_region_residence'),
      value: personalInformation?.jurisdictionOfResidence
        ? getCountryByLang(personalInformation?.jurisdictionOfResidence)
        : 'N/A',
    },
    {
      label: t('accountOpening.personal_information.dob'),
      value: !personalInformation?.dateOfBirth
        ? 'N/A'
        : moment(personalInformation?.dateOfBirth).format('YYYY-MM-DD'),
    },
    {
      label: t('accountOpening.personal_information.nationality'),
      value: personalInformation?.jurisdiction
        ? getCountryByLang(personalInformation?.jurisdiction)
        : 'N/A',
    },
    {
      label: t('accountOpening.personal_information.id_type'),
      value: personalInformation?.identificationType
        ? t(`accountOpening.id_type.${personalInformation?.identificationType}`)
        : 'N/A',
    },
    {
      label: t('accountOpening.personal_information.id_passport_number'),
      value: personalInformation?.identificationNo ?? 'N/A',
    },
    {
      label: t('accountOpening.personal_information.id_expiry'),
      value: !personalInformation?.identificationExpiryDate
        ? 'N/A'
        : moment(personalInformation?.identificationExpiryDate).format(
            'YYYY-MM-DD',
          ),
    },
  ];

  const contactInformationFields = [
    {
      label: t('accountOpening.contact_information.address'),
      value:
        !contactInformation.addressLine1 &&
        !contactInformation.addressLine2 &&
        !contactInformation.city &&
        !contactInformation.jurisdiction &&
        !contactInformation.postcode &&
        !contactInformation.province
          ? 'N/A'
          : `${contactInformation?.addressLine1}${
              contactInformation?.addressLine2 ? ',' : ''
            } ${
              contactInformation?.addressLine2
                ? contactInformation?.addressLine2
                : ''
            }${contactInformation?.city ? ',' : ''} ${
              contactInformation?.city ? contactInformation?.city : ''
            }${contactInformation?.province ? ',' : ''} ${
              contactInformation?.province ? contactInformation?.province : ''
            }${contactInformation?.postcode ? ',' : ''} ${
              contactInformation?.postcode ? contactInformation?.postcode : ''
            }
      ${contactInformation?.jurisdictionOfResidence && ','} ${
              contactInformation?.jurisdictionOfResidence &&
              getCountryByLang(contactInformation?.jurisdictionOfResidence)
            }`,
    },
  ];

  const financialInformationFields =
    entityId === 'EMX'
      ? []
      : [
          {
            label: t('accountOpening.financial_information.employment_status'),
            value: t(
              `accountOpening.employment_status.${financialInformation?.employmentStatus}`,
            ),
          },
          {
            label: t('accountOpening.financial_information.industrial'),
            value: financialInformation?.industrial
              ? t(
                  `accountOpening.industrial.${financialInformation?.industrial}`,
                )
              : 'N/A',
          },
          {
            label: t('accountOpening.financial_information.job_title'),
            value: financialInformation?.jobTitle
              ? t(`accountOpening.jobtitle.${financialInformation?.jobTitle}`)
              : 'N/A',
          },
          {
            label: t('accountOpening.financial_information.annual_income'),
            value: t(
              `accountOpening.annual_income.${financialInformation?.annualIncome}`,
            ),
          },
          {
            label: t('accountOpening.financial_information.net_worth'),
            value: t(
              `accountOpening.net_worth.${financialInformation?.liquidNetworth}`,
            ),
          },
          {
            label: t(
              'accountOpening.financial_information.source_trading_fund',
            ),
            value: t(
              `accountOpening.trading_fund.${financialInformation?.sourceOfFunds}`,
            ),
          },
          {
            label: t('accountOpening.financial_information.tax_residence'),
            value: getCountryByLang(
              financialInformation?.jurisdictionOfTaxResidence,
            ),
          },
          {
            label: t('accountOpening.financial_information.taxpayer_id'),
            value: financialInformation?.tinNumber
              ? financialInformation?.tinNumber
              : 'N/A',
          },
        ];

  const tradingExperienceFields =
    entityId === 'EMX'
      ? []
      : [
          {
            label: t('accountOpening.trading_experience.currently_trade'),
            value: !tradingExperience?.fiveOrMorTransactionLastThreeYears
              ? t('accountOpening.no')
              : t('accountOpening.yes'),
          },
          {
            label: t('accountOpening.trading_experience.experience'),
            value: tradingExperience?.tradingExperience
              ? t(
                  `accountOpening.experience.${tradingExperience?.tradingExperience}`,
                )
              : 'N/A',
          },
          {
            label: t('accountOpening.trading_experience.trading_frequency'),
            value: tradingExperience?.tradingFrequency
              ? t(
                  `accountOpening.frequency_trading.${tradingExperience?.tradingFrequency}`,
                )
              : 'N/A',
          },
          {
            label: t('accountOpening.trading_experience.purpose'),
            value: tradingExperience?.accountOpeningPurpose
              ? t(
                  `accountOpening.purpose.${tradingExperience?.accountOpeningPurpose}`,
                )
              : 'N/A',
          },
        ];

  const onPressBack = () => {
    navigation.goBack();
  };

  const handleConfirm = () => {
    const emptyFields = [];

    const data = {
      ...personalInformation,
      ...contactInformation,
      ...financialInformation,
      ...tradingExperience,
    };

    const mandatoryFields =
      entityId === 'EIE'
        ? [
            {field: 'legalLastNameEn', screen: personalInformation},
            {field: 'legalFirstAndMiddleNameEn', screen: personalInformation},
            {field: 'gender', screen: personalInformation},
            {field: 'jurisdictionOfResidence', screen: personalInformation},
            {field: 'dateOfBirth', screen: personalInformation},
            {field: 'jurisdiction', screen: personalInformation},
            {field: 'identificationType', screen: personalInformation},
            {field: 'identificationNo', screen: personalInformation},
            {field: 'employmentStatus', screen: financialInformation},
            {field: 'annualIncome', screen: financialInformation},
            {field: 'liquidNetworth', screen: financialInformation},
            {field: 'sourceOfFunds', screen: financialInformation},
            {field: 'jurisdictionOfTaxResidence', screen: financialInformation},
            {
              field: 'fiveOrMorTransactionLastThreeYears',
              screen: tradingExperience,
            },
          ]
        : [
            {field: 'legalLastNameEn', screen: personalInformation},
            {field: 'legalFirstAndMiddleNameEn', screen: personalInformation},
            {field: 'gender', screen: personalInformation},
            {field: 'jurisdictionOfResidence', screen: personalInformation},
            {field: 'dateOfBirth', screen: personalInformation},
            {field: 'jurisdiction', screen: personalInformation},
            {field: 'identificationType', screen: personalInformation},
            {field: 'identificationNo', screen: personalInformation},
            {field: 'identificationNo', screen: personalInformation},
          ];

    for (let key in data) {
      if (key === 'identificationExpiryDate') {
        if (
          personalInformation.identificationType === 'PASSPORT' &&
          !personalInformation.identificationExpiryDate
        ) {
          emptyFields.push(key);
        }
      }

      if (key === 'addressLine1') {
        if (!contactInformation.addressLine1) {
          emptyFields.push(key);
        }
      }

      if (key === 'city') {
        if (!contactInformation.city) {
          emptyFields.push(key);
        }
      }

      if (key === 'jurisdictionOfResidence') {
        if (!contactInformation.jurisdictionOfResidence) {
          emptyFields.push(key);
        }
      }

      if (key === 'industrial') {
        if (
          financialInformation.employmentStatus &&
          financialInformation.employmentStatus !== 'RETIRED' &&
          financialInformation.employmentStatus !== 'UNEMPLOYED' &&
          !financialInformation.industrial
        ) {
          emptyFields.push(key);
        }
      }

      if (key === 'tradingExperience') {
        if (
          tradingExperience.fiveOrMorTransactionLastThreeYears &&
          !tradingExperience.tradingExperience
        ) {
          emptyFields.push(key);
        }
      }

      if (key === 'tradingFrequency') {
        if (
          tradingExperience.fiveOrMorTransactionLastThreeYears &&
          !tradingExperience.tradingFrequency
        ) {
          emptyFields.push(key);
        }
      }

      const field = mandatoryFields.find(el => el.field === key);

      if (field && field.screen[key] === null) {
        emptyFields.push(key);
      }
    }

    if (emptyFields.length > 0) {
      SetIsShowMessageDialog({
        title: t('Error'),
        message: t(
          'accountOpening.application_review.field_missing_validation',
        ),
        isShowDialog: true,
      });
    } else {
      navigation.navigate('AccountPromotion');
    }
  };

  const handleWhatsapp = () => {
    Linking.openURL(reward_whatsappUrl);
  };

  const acceptedStatus = [
    'Pending',
    'TemporarilyDeclined',
    'FinallyRejected',
    'Approved',
  ];

  const launchSNSMobileSDK = async () => {
    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
      // this is a token expiration handler, will be called if the provided token is invalid or got expired
      // call your backend to fetch a new access token (this is just an example)
      // const data: any = getSumsubToken(cognitoUsername, entityId);
      // const {token} = data;
      // return token;
      const res: any = axios({
        url: sumsub_url,
        method: 'POST',
        data: {
          customerId: individual?.id,
          kycType: entityId === 'EIE' ? 'aoeiehk' : 'aoxpro',
        },
      });
      return res.data.token;
    })
      .withHandlers({
        // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: event => {
          console.log(event, '<< on status change');

          if (acceptedStatus.includes(event.newStatus)) {
            snsMobileSDK.dismiss();
            setIsFinished(true);
          }
        },
        onEvent: event => {
          console.log(event, '<< on event');
        },
      })
      .withDebug(true)
      .withLocale(lang === 'tc' ? 'zh-tw' : lang === 'sc' ? 'zh' : 'en') // Optional, for cases when you need to override the system locale
      .build();

    snsMobileSDK
      .launch()
      .then(result => {
        console.log(result, '<< result');
        if (acceptedStatus.includes(result.status)) {
          snsMobileSDK.dismiss();
          setIsFinished(true);
        }
      })
      .catch(err => {
        console.log('SumSub SDK Error: ' + JSON.stringify(err));
      });
  };

  const handleEditPersonalInformation = () => {
    if (individual.aoOcrIdDoc) {
      navigation.navigate('AccountPersonalInformation', {
        lastScreen: 'AccountReviewApplication',
      });
    } else {
      launchSNSMobileSDK();
    }
  };

  const TextContent = ({value, label}: {value: string; label: string}) => {
    return (
      <View style={tw`flex flex-row mt-[10px]`}>
        <Text
          style={[
            globalStyles.Body_Text,
            {fontSize: width / 30, maxWidth: '85%'},
          ]}>
          {label}
        </Text>
        <DashedLineView style={{marginHorizontal: 5, alignSelf: 'center'}} />
        <Text
          style={[
            globalStyles.Body_Text_B,
            {textAlign: 'right', flexShrink: 1, fontSize: width / 30},
          ]}>
          {value}
        </Text>
      </View>
    );
  };

  useEffect(() => {
    const fetchToken = async () => {
      SetIsShowLoading(true);
      try {
        const data: any = await getSumsubToken(individual?.id, entityId);
        const {token} = data;
        setAccessToken(token);
      } catch (error) {
        SetIsShowMessageDialog({
          isShowDialog: true,
          title: t('common.error.network'),
          message: t('common.error.auth'),
        });
        fetchToken();
      } finally {
        SetIsShowLoading(false);
      }
    };
    if (individual?.id && !isFinished) {
      fetchToken();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [individual?.id]);

  useEffect(() => {
    if (isFinished) {
      navigation.navigate('AccountPersonalInformation', {
        lastScreen: 'AccountReviewApplication',
      });
    }
  }, [isFinished]);

  return (
    <>
      <View style={{backgroundColor: colors.White}}>
        <SafeAreaView edges={['top', 'right', 'left']}>
          <View
            style={[
              tw`flex flex-row items-center justify-between`,
              {
                backgroundColor: colors.White,
                paddingHorizontal: 16,
                paddingBottom: 16,
              },
            ]}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="black"
              name="chevron-left"
              onPress={onPressBack}
            />
            {entityId === 'EIE' && (
              <Pressable onPress={handleWhatsapp}>
                <Icon
                  name="whatsapp"
                  color={colors.MainFont}
                  style={{fontSize: 28}}
                />
              </Pressable>
            )}
          </View>
        </SafeAreaView>
      </View>
      <ScrollView>
        <View style={[tw`rounded-b-xl`, {backgroundColor: colors.White}]}>
          <View style={{paddingHorizontal: 16}}>
            <Text style={[globalStyles.H2, {color: colors.Grey3}]}>
              {t('accountOpening.application_review.title')}
            </Text>
            <Text
              style={[globalStyles.H4, {color: colors.Brand2, marginTop: 10}]}>
              {t('accountOpening.personal_information.title')}
            </Text>
          </View>
          <View style={{paddingHorizontal: 16, marginTop: 24}}>
            {personalInformationFields.map((el, idx) => (
              <TextContent value={el.value} label={el.label} key={idx} />
            ))}
            <View
              style={[
                {
                  paddingVertical: 24,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Button
                label={t('common.edit')}
                size="small"
                onPress={handleEditPersonalInformation}
                type="outlined"
                icon
              />
            </View>
          </View>
        </View>
        <View
          style={[
            tw`rounded-b-xl rounded-t-xl`,
            {backgroundColor: colors.White, marginTop: 12},
          ]}>
          <View style={{paddingHorizontal: 16, marginTop: 24}}>
            <Text
              style={[globalStyles.H4, {color: colors.Brand2, marginTop: 10}]}>
              {t('accountOpening.contact_information.title')}
            </Text>
          </View>
          <View style={{paddingHorizontal: 16, marginTop: 24}}>
            {contactInformationFields.map((el, idx) => (
              <TextContent value={el.value} label={el.label} key={idx} />
            ))}
            <View
              style={[
                {
                  paddingVertical: 24,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Button
                label={t('common.edit')}
                size="small"
                onPress={() =>
                  navigation.navigate('AccountContactInformation', {
                    lastScreen: 'AccountReviewApplication',
                  })
                }
                type="outlined"
                icon
              />
            </View>
            {entityId === 'EMX' && (
              <View style={tw`py-5`}>
                <Button
                  label={t('common.confirm_next')}
                  onPress={handleConfirm}
                />
              </View>
            )}
          </View>
        </View>
        {entityId === 'EIE' && (
          <View
            style={[
              tw`rounded-b-xl rounded-t-xl`,
              {backgroundColor: colors.White, marginTop: 12},
            ]}>
            <View style={{paddingHorizontal: 16, marginTop: 24}}>
              <Text
                style={[
                  globalStyles.H4,
                  {color: colors.Brand2, marginTop: 10},
                ]}>
                {t('accountOpening.financial_information.title')}
              </Text>
            </View>
            <View style={{paddingHorizontal: 16, marginTop: 24}}>
              {financialInformationFields.map((el, idx) => (
                <TextContent value={el.value} label={el.label} key={idx} />
              ))}
              <View
                style={[
                  {
                    paddingVertical: 24,
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}>
                <Button
                  label={t('common.edit')}
                  size="small"
                  onPress={() =>
                    navigation.navigate('AccountFinancialInformation', {
                      lastScreen: 'AccountReviewApplication',
                    })
                  }
                  type="outlined"
                  icon
                />
              </View>
            </View>
          </View>
        )}
        {entityId === 'EIE' && (
          <View
            style={[
              tw`rounded-b-xl rounded-t-xl`,
              {backgroundColor: colors.White, marginTop: 12},
            ]}>
            <View style={{paddingHorizontal: 16, marginTop: 24}}>
              <Text
                style={[
                  globalStyles.H4,
                  {color: colors.Brand2, marginTop: 10},
                ]}>
                {t('accountOpening.trading_experience.title')}
              </Text>
            </View>
            <View style={{paddingHorizontal: 16, marginTop: 24}}>
              {tradingExperienceFields.map((el, idx) => (
                <TextContent value={el.value} label={el.label} key={idx} />
              ))}
              <View
                style={[
                  {
                    paddingVertical: 24,
                    justifyContent: 'center',
                    alignItems: 'center',
                  },
                ]}>
                <Button
                  label={t('common.edit')}
                  size="small"
                  onPress={() =>
                    navigation.navigate('AccountTradingExperience', {
                      lastScreen: 'AccountReviewApplication',
                    })
                  }
                  type="outlined"
                  icon
                />
              </View>
              <View style={tw`py-5`}>
                <Button
                  label={t('common.confirm_next')}
                  onPress={handleConfirm}
                />
              </View>
            </View>
          </View>
        )}
      </ScrollView>
    </>
  );
};

export default AccountReviewApplication;
