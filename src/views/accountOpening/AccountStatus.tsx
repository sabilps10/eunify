import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Pressable,
  StyleSheet,
  Dimensions,
  // Linking,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import {ScrollView} from 'react-native-gesture-handler';
import {bindActionCreators} from 'redux';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import InputCheckBox from '../../components/forms/inputs/inputCheckBox.component';
import {entityId} from '../../config/constants';
import DashedLineView from '../../components/dashedLineView.component';
import {
  getIndividual,
  getPersonMessage,
} from '../../utils/api/apiAccountOpening';
import {
  TabDirectoryWebViewPage,
  TabDirectoryWebViewTab,
} from '../tabDirectory/tabDirectoryWebView';
import {MIOType} from '../../components/MIO';
import {Auth} from 'aws-amplify';
import WebView from 'react-native-webview';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {AccountUtils} from '../../utils/accountUtils';
import {AccountActionCreators} from '../../redux/account/actionCreator';
import {CognitoActionCreators} from '../../redux/cognito/actionCreator';
import {OpenPositionsActionCreators} from '../../redux/trading/actionCreator';

interface ProcessCard {
  icon: string;
  context: string;
  contextKey: string;
  textitems: string[];
}

const AccountStatus = ({navigation}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const {width} = Dimensions.get('screen');
  const dispatch = useDispatch();

  const displayName = useSelector(
    (state: State) => state.account.account?.DisplayName,
  );

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const {SetTradingAccount, SetSelectedTradingAccount, SetDepositDialog} =
    bindActionCreators(AccountOpeningCreators, dispatch);

  const {doRefreshToken} = AccountUtils();

  const {SetCurrentToken} = bindActionCreators(CognitoActionCreators, dispatch);

  const {SetAccountInfo} = bindActionCreators(AccountActionCreators, dispatch);

  const {SetAccountDetail} = bindActionCreators(
    OpenPositionsActionCreators,
    dispatch,
  );

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const {currentToken} = useSelector((state: State) => state.cognito);

  const [detail, setDetail] = useState<any>(null);

  const [email, setEmail] = useState<string>('');

  const [webviewLink, setWebviewLink] = useState<string | null>(null);

  const handlePress = (type: string) => {
    if (type === 'deposit') {
      navigation.push('TabDirectoryWebView', {
        page: TabDirectoryWebViewPage.MIO,
        focusTab: TabDirectoryWebViewTab.ME,
        param: {type: MIOType.Deposit},
      });
    } else if (type === 'edd') {
      setWebviewLink(detail?.eddFormLink);
    } else {
      setWebviewLink(detail?.witnessFormLink);
    }
  };

  const handleOpenDialog = (type: string) => {
    SetDepositDialog({
      isOpen: type,
      onPress: () => handlePress(type),
      url: detail?.needEddForm
        ? detail?.eddFormLink
        : detail?.needWitnessForm
        ? detail?.witnessFormLink
        : '',
    });
  };

  const onPressDeposit = () => {
    navigation.push('TabDirectoryWebView', {
      page: TabDirectoryWebViewPage.MIO,
      focusTab: TabDirectoryWebViewTab.ME,
      param: {type: MIOType.Deposit},
    });
  };

  const onPressWithdraw = () => {
    navigation.push('TabDirectoryWebView', {
      page: TabDirectoryWebViewPage.MIO,
      focusTab: TabDirectoryWebViewTab.ME,
      param: {type: MIOType.Withdraw},
    });
  };

  useEffect(() => {
    const fetchUser = async () => {
      const user = await Auth.currentSession();
      const userEmail = user.getIdToken().payload.email;
      setEmail(userEmail);
    };
    fetchUser();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const getIndividualData = async () => {
      const user = await Auth.currentSession();
      const cognitoId = user.getIdToken().payload['cognito:username'];

      await getIndividual({
        token: currentToken,
        payload: {username: cognitoId},
        onSuccess: async data => {
          setDetail(data.response);

          await getPersonMessage({
            token: currentToken,
            payload: {uuid: data.response.id},
            onSuccess: async res => {
              if (res.result === true && res.response.accountId.length > 0) {
                SetTradingAccount(res.response.accountId);
                SetSelectedTradingAccount(res.response.accountId[0]);
                // SetAccountDetail({Login: Number(res.response.accountId[0])});

                if (
                  data.response.status === 'PENDING_DEPOSIT' ||
                  'DEPOSIT_PENDING_APPROVAL' ||
                  'COMPLETED'
                ) {
                  SetAccountInfo({
                    LoginLevel: 3,
                  });
                }
              }
            },
            onFailed: error => {
              console.log(error);
            },
          });
        },
        onFailed: () => {},
        setLoading: SetIsShowLoading,
      });
    };

    if (email) {
      getIndividualData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [email]);

  const process = [
    {
      icon: 'tray-arrow-up',
      icontext: t('accountOpening.getting_ready.upload_doc'),
      key: 'upload_doc',
      textitem: [
        t('accountOpening.getting_ready.identification_doc'),
        t('accountOpening.getting_ready.face_verif'),
        t('accountOpening.getting_ready.residental_address'),
      ],
    },
    {
      icon: 'text-box-check-outline',
      icontext: t('accountOpening.getting_ready.submit_application'),
      key: 'submit_application',
      textitem:
        entityId === 'EMX'
          ? [t('accountOpening.getting_ready.personal_info')]
          : [
              t('accountOpening.getting_ready.personal_info'),
              t('accountOpening.getting_ready.trading_exp'),
              t('accountOpening.getting_ready.emp_financial_info'),
            ],
    },
    {
      icon: 'shield-check-outline',
      icontext: t('accountOpening.getting_ready.verify'),
      key: 'verify',
      textitem: [
        t('accountOpening.getting_ready.identification_doc'),
        t('accountOpening.getting_ready.face_verif'),
        t('accountOpening.getting_ready.residental_address'),
      ],
    },
    ...(entityId === 'EIE'
      ? [
          {
            icon: 'cash-multiple',
            icontext: t('accountOpening.getting_ready.fund'),
            key: 'fund',
            textitem: [t('accountOpening.getting_ready.deposit')],
          },
        ]
      : []),
  ];

  // Verifying
  const items =
    lang === 'en'
      ? {
          title: 'E+ FLEXI (HK)',
          benefits: [
            'Leverage Up To 1:50*',
            'Top Forex Pairs From Global Markets',
            'Top Conditions',
            'Quick Execution',
            'SFC License Type 3',
            'Offer Precious Metals Trading Services',
            'Convenient Deposit & Withdrawal Services',
            '24/5 Live Customer Service',
          ],
          status: detail?.status,
        }
      : lang === 'tc'
      ? {
          title: '環球賬戶',
          benefits: [
            '槓桿高達1:50*', // Leverage Up To 1:50*
            '進入全球最大規模及流動性最好的外匯市場', // Top Forex Pairs From Global Markets
            '頂級交易環境', // Top Conditions
            '輕鬆執行買賣策略', // Quick Execution
            '持有香港證監會第三類牌照', // SFC License Type 3
            '同時提供貴金屬產品交易', // Offer Precious Metals Trading Services
            '快捷方便的提款及存款服務', // Convenient Deposit & Withdrawal Services
            '24/5 真人客戶支援服務', // 24/5 Live Customer Service
          ],
          status: detail?.status,
        }
      : {
          title: '环球账户',
          benefits: [
            '杠杆高达1:50*', // Leverage Up To 1:50*
            '进入全球最大规模及流动性最好的外汇市场', // Top Forex Pairs From Global Markets
            '顶级交易环境', // Top Conditions
            '轻松执行买卖策略', // Quick Execution
            '持有香港证监会第三类牌照', // SFC License Type 3
            '同时提供贵金属产品交易', // Offer Precious Metals Trading Services
            '快捷方便的提款及存款服务', // Convenient Deposit & Withdrawal Services
            '24/5 真人客户支援服务', // 24/5 Live Customer Service
          ],
          status: detail?.status,
        };

  const itemEmx =
    lang === 'en'
      ? {
          title: 'Emperor Xpro',
          benefits: [
            'Leverage Up To 1:400*',
            'Top Forex Pairs From Global Markets',
            'Top Conditions',
            'Quick Execution',
            'Offer Precious Metals Trading Services',
            'Convenient Deposit & Withdrawal Services',
            'Online Customer Support',
          ],
          status: detail?.status,
        }
      : lang === 'tc'
      ? {
          title: 'Emperor Xpro',
          benefits: [
            '槓桿高達1:400*', // Leverage Up To 1:400*
            '進入全球最大規模及流動性最好的外匯市場', // Top Forex Pairs From Global Markets
            '頂級交易環境', // Top Conditions
            '輕鬆執行買賣策略', // Quick Execution
            '支持貴金屬交易', // Offer Precious Metals Trading Services
            '快捷方便的提款及存款服務', // Convenient Deposit & Withdrawal Services
            '線上客戶支援', // Online Customer Supporrt
          ],
          status: detail?.status,
        }
      : {
          title: 'Emperor Xpro',
          benefits: [
            '杠杆高达1:400*', // Leverage Up To 1:50*
            '进入全球最大规模及流动性最好的外汇市场', // Top Forex Pairs From Global Markets
            '顶级交易环境', // Top Conditions
            '轻松执行买卖策略', // Quick Execution
            '支持贵金属交易 ', // Offer Precious Metals Trading Services
            '快捷方便的提款及存款服务', // Convenient Deposit & Withdrawal Services
            '线上客户支援', // Online Customer Supporrt
          ],
          status: detail?.status,
        };

  // const RegisterCard = () => {
  //   const leftContent = items.benefits.slice(
  //     0,
  //     Math.ceil(items.benefits.length / 2),
  //   );
  //   const rightContent = items.benefits.slice(
  //     Math.ceil(items.benefits.length / 2),
  //   );
  //   return (
  //     <View style={[tw`flex flex-col`, styles.registerCard]}>
  //       <Text
  //         style={[
  //           globalStyles.H4,
  //           tw`px-[10-px] py-[14px]`,
  //           {color: colors.text},
  //         ]}>
  //         Register a trading account to
  //       </Text>
  //       <View style={tw`flex flex-row justify-between mt-[8px]`}>
  //         <View style={{width: width / 2 - 28}}>
  //           {leftContent.map(el => (
  //             <View style={tw`flex flex-row items-center mb-[10px]`}>
  //               <Icon
  //                 name="circle-medium"
  //                 color="#CFA872"
  //                 style={styles.dotIcon}
  //               />
  //               <Text style={[globalStyles.Note_120, {color: colors.text}]}>
  //                 {el}
  //               </Text>
  //             </View>
  //           ))}
  //         </View>
  //         <View style={{width: width / 2 - 28}}>
  //           {rightContent.map(el => (
  //             <View style={tw`flex flex-row items-center mb-[10px]`}>
  //               <Icon
  //                 name="circle-medium"
  //                 color="#CFA872"
  //                 style={styles.dotIcon}
  //               />
  //               <Text style={[globalStyles.Note, {color: colors.MainFont}]}>
  //                 {el}
  //               </Text>
  //             </View>
  //           ))}
  //         </View>
  //       </View>
  //       <View style={tw`flex justify-center items-center mt-[20px] mb-[20px]`}>
  //         <Pressable
  //           onPress={() => navigation.navigate('AccountQuestion')}
  //           style={{
  //             borderWidth: 1,
  //             borderColor: colors.Black,
  //             borderRadius: 50,
  //             paddingVertical: 7,
  //             paddingHorizontal: 20,
  //           }}>
  //           <View style={tw`flex flex-row justify-between items-center`}>
  //             <Text style={{fontWeight: '600'}}>
  //               {t('common.register_now')}
  //             </Text>
  //             <Icon
  //               name="chevron-right"
  //               color={colors.Black}
  //               style={styles.dotIcon}
  //             />
  //           </View>
  //         </Pressable>
  //       </View>
  //     </View>
  //   );
  // };

  const STATUS = {
    AWAITING_RESPONSE: t('accountOpening.account_status.awaiting_response'),
    PENDING_VERIFICATION: t('accountOpening.account_status.verifying'),
    PENDING_APPROVAL: t('accountOpening.account_status.verifying'),
    PENDING_DEPOSIT: t('accountOpening.account_status.pending_deposit'),
    DEPOSIT_PENDING_APPROVAL: 'PENDING DEPOSIT APPROVAL',
    COMPLETED: t('accountOpening.account_status.completed'),
    Completed: t('accountOpening.account_status.completed'),
    REJECTED: t('accountOpening.account_status.rejected'),
  };

  const statusLv3 = [
    'PENDING_DEPOSIT',
    'DEPOSIT_PENDING_APPROVAL',
    'REJECTED',
    'COMPLETED',
    'Completed',
  ];

  const AccountCard = ({
    title,
    benefits,
    status,
  }: {
    title: string;
    benefits: string[];
    status: string;
  }) => {
    const [open, setOpen] = useState<boolean>(true);

    const leftContent = benefits.slice(0, Math.ceil(benefits.length / 2));
    const rightContent = benefits.slice(Math.ceil(benefits.length / 2));
    return (
      <View style={tw`pt-6`}>
        <View style={[tw`flex flex-col`, styles.card]}>
          <View style={[tw`flex flex-row justify-between py-[16px]`]}>
            <View style={tw`flex flex-row items-center`}>
              {status === 'rejected' ? (
                <View
                  style={{
                    backgroundColor: colors.Red,
                    borderRadius: 100,
                    marginRight: 10,
                    padding: 3,
                  }}>
                  <Icon
                    name="exclamation-thick"
                    color={colors.White}
                    style={{fontSize: 15}}
                  />
                </View>
              ) : (
                <View
                  style={{
                    backgroundColor: colors.Brand2,
                    borderRadius: 100,
                    marginRight: 10,
                  }}>
                  <Icon
                    name="dots-horizontal"
                    color={colors.White}
                    style={{fontSize: 20}}
                  />
                </View>
              )}
              <View>
                <Text style={[globalStyles.Body_Text_B]}>{title}</Text>
                {status !== 'Completed' ? (
                  <Text
                    style={[
                      globalStyles.Body_Text_B,
                      {
                        color:
                          status === 'REJECTED' ? colors.Red : colors.Brand2,
                        textTransform: 'uppercase',
                        marginTop: 3,
                      },
                    ]}>
                    {STATUS[status]}
                  </Text>
                ) : (
                  <View
                    style={{
                      backgroundColor: colors.Brand2,
                      padding: 4,
                      borderRadius: 5,
                      marginTop: 4,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={[
                        globalStyles.Body_Text_B,
                        {
                          color: colors.White,
                          textTransform: 'uppercase',
                          marginTop: 3,
                        },
                      ]}>
                      {STATUS[status]}
                    </Text>
                  </View>
                )}
              </View>
            </View>
            <View style={tw`flex flex-row items-center`}>
              <Text style={styles.label}>
                {t('accountOpening.application_status.default')}
              </Text>
              <Pressable onPress={() => setOpen(!open)}>
                <Icon
                  name={!open ? 'chevron-right' : 'chevron-down'}
                  size={24}
                  color={colors.fixedBackground}
                />
              </Pressable>
            </View>
          </View>
          {open && (
            <View style={tw`pb-[10px]`}>
              {status !== 'PENDING_VERIFICATION' &&
                status !== 'PENDING_APPROVAL' && (
                  <View style={tw`flex flex-row justify-between mt-[8px]`}>
                    <View style={{width: width / 2 - 28}}>
                      {leftContent.map(el => (
                        <View style={tw`flex flex-row items-center mb-[10px]`}>
                          <Icon
                            name="circle-medium"
                            color="#CFA872"
                            style={styles.dotIcon}
                          />
                          <Text
                            style={[
                              globalStyles.Note,
                              {
                                color: colors.MainFont,
                                flex: 1,
                                flexWrap: 'wrap',
                              },
                            ]}>
                            {el}
                          </Text>
                        </View>
                      ))}
                    </View>
                    <View style={{width: width / 2 - 28}}>
                      {rightContent.map(el => (
                        <View style={tw`flex flex-row items-center mb-[10px]`}>
                          <Icon
                            name="circle-medium"
                            color="#CFA872"
                            style={styles.dotIcon}
                          />
                          <Text
                            style={[
                              globalStyles.Note,
                              {
                                color: colors.MainFont,
                                flex: 1,
                                flexWrap: 'wrap',
                              },
                            ]}>
                            {el}
                          </Text>
                        </View>
                      ))}
                    </View>
                  </View>
                )}

              {status === 'REJECTED' && (
                <WarningCard
                  title={
                    detail?.rejectReason === 'Duplicated Account'
                      ? t('accountOpening.application_status.duplicate_account')
                      : detail?.rejectReason
                  }
                />
              )}

              {status === 'PENDING_DEPOSIT' && (
                <WarningCard
                  title={t(
                    `accountOpening.application_status.${'deposit_to_active'}`,
                  )}
                />
              )}

              {(status === 'verifying' ||
                status === 'PENDING_VERIFICATION' ||
                status === 'PENDING_APPROVAL' ||
                status === 'AWAITING_RESPONSE') && (
                <View>
                  <Text style={globalStyles.Small_Note_B}>
                    {t('accountOpening.application_status.application_status')}
                  </Text>
                  {process.map((el, idx) => (
                    <>
                      <ProcessCard
                        key={idx}
                        icon={el.icon}
                        context={el.icontext}
                        textitems={el.textitem}
                        contextKey={el.key}
                      />
                      {el.key === 'verify' && detail?.needEddForm && (
                        <WarningCard
                          title={t(
                            'accountOpening.application_status.fill_edd',
                          )}
                          arrowIcon
                          onPress={() => handleOpenDialog('edd')}
                          style={{marginTop: 10}}
                        />
                      )}
                      {el.key === 'verify' && detail?.needWitnessForm && (
                        <WarningCard
                          title={t(
                            'accountOpening.application_status.third_party_witness',
                          )}
                          arrowIcon
                          onPress={() => handleOpenDialog('witness')}
                          style={{marginTop: 10}}
                        />
                      )}
                      {idx !== process.length - 1 && (
                        <>
                          <DashedLineView style={{marginTop: 20}} />
                          <Icon
                            style={[tw`text-brand3`, styles.arrowIcon]}
                            name="menu-down"
                          />
                        </>
                      )}
                    </>
                  ))}
                </View>
              )}

              {status === 'PENDING_DEPOSIT' && (
                <View
                  style={tw`flex flex-row justify-center items-center mt-[20px]`}>
                  <Text style={[globalStyles.Big_Text_B, {marginRight: 5}]}>
                    {t('accountOpening.application_status.deposit')}
                  </Text>
                  <Pressable onPress={onPressDeposit}>
                    <CircleArrow20px
                      color={colors.White}
                      backgroundColor={colors.Brand3}
                    />
                  </Pressable>
                </View>
              )}

              {status === 'Completed' && (
                <View
                  style={tw`flex flex-row justify-center items-center mt-[20px]`}>
                  <View style={tw`flex flex-row`}>
                    <Text style={[globalStyles.Big_Text_B, {marginRight: 5}]}>
                      {t('accountOpening.application_status.deposit')}
                    </Text>
                    <Pressable onPress={onPressDeposit}>
                      <CircleArrow20px
                        color={colors.White}
                        backgroundColor={colors.Brand3}
                      />
                    </Pressable>
                  </View>
                  <View style={tw`flex flex-row ml-[10px]`}>
                    <Text style={[globalStyles.Big_Text_B, {marginRight: 5}]}>
                      {t('accountOpening.application_status.withdraw')}
                    </Text>
                    <Pressable onPress={onPressWithdraw}>
                      <CircleArrow20px
                        color={colors.White}
                        backgroundColor={colors.Brand3}
                      />
                    </Pressable>
                  </View>
                </View>
              )}
            </View>
          )}
        </View>
      </View>
    );
  };

  const ProcessCard = ({icon, context, contextKey, textitems}: ProcessCard) => {
    return (
      <View
        style={[
          tw`flex flex-row w-full h-[100px] items-center justify-between pt-[11px]`,
        ]}>
        <View style={tw`flex flex-row items-center`}>
          <View
            style={tw`w-[65px] flex flex-col items-center justify-center mr-[10px]`}>
            {context !== 'fund' && (
              <Icon
                name={
                  contextKey === 'verify' &&
                  detail?.status === 'PENDING_VERIFICATION'
                    ? 'dots-horizontal-circle'
                    : contextKey === 'fund' &&
                      (detail?.status === 'PENDING_APPROVAL' ||
                        detail?.status === 'PENDING_DEPOSIT' ||
                        detail?.status === 'PENDING_VERIFICATION' ||
                        detail?.status === 'AWAITING_RESPONSE' ||
                        detail?.status === 'PENDING_DEPOSIT')
                    ? 'checkbox-blank-circle-outline'
                    : 'check-circle'
                }
                color={
                  contextKey === 'verify' &&
                  detail?.status === 'PENDING_VERIFICATION'
                    ? colors.Brand2
                    : contextKey === 'fund' &&
                      (detail?.status === 'PENDING_APPROVAL' ||
                        detail?.status === 'PENDING_DEPOSIT')
                    ? colors.Brand2
                    : colors.Brand4
                }
                style={{
                  fontSize: 20,
                  marginLeft: -25,
                  marginBottom: -7,
                }}
              />
            )}
            <Icon
              style={{
                color: contextKey === 'fund' ? '#9D9D9D' : colors.Brand3,
                fontSize: 30,
              }}
              name={icon}
            />
            <Text
              style={[
                globalStyles.Mobile_Menu,
                tw`text-[10px] text-center mt-[5px]`,
                {color: colors.MainFont},
              ]}>
              {context}
            </Text>
          </View>
          <View style={tw`flex flex-col justify-start`}>
            {textitems.map((r: string, idx: number) => (
              <View style={tw`flex flex-row items-center mt-[10px]`} key={idx}>
                <Icon
                  name={
                    contextKey === 'verify' &&
                    detail?.status === 'PENDING_VERIFICATION'
                      ? 'dots-horizontal-circle'
                      : contextKey === 'fund'
                      ? 'checkbox-blank-circle-outline'
                      : 'check-circle'
                  }
                  color={
                    contextKey === 'verify' &&
                    detail?.status === 'PENDING_VERIFICATION'
                      ? colors.Brand2
                      : contextKey === 'fund'
                      ? colors.Brand3
                      : colors.Brand4
                  }
                  style={{
                    fontSize: 20,
                    marginRight: 5,
                  }}
                />
                <Text
                  style={[globalStyles.Small_Note, {color: colors.MainFont}]}>
                  {r}
                </Text>
              </View>
            ))}
          </View>
        </View>
      </View>
    );
  };

  const WarningCard = ({
    title,
    arrowIcon = false,
    onPress,
    style,
  }: {
    title: string;
    arrowIcon?: boolean;
    onPress?: () => void;
    style?: any;
  }) => {
    return (
      <Pressable onPress={onPress ? onPress : null}>
        <View
          style={[
            {
              backgroundColor: colors.Brand1,
              padding: 10,
              borderRadius: 12,
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            },
            style,
          ]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 100,
                padding: 2,
                marginRight: 10,
              }}>
              <Icon name="exclamation-thick" />
            </View>
            <Text style={[globalStyles.Small_Note_B, {flex: 1, flexShrink: 1}]}>
              {title}
            </Text>
          </View>
          {arrowIcon && <Icon name="chevron-right" style={{fontSize: 20}} />}
        </View>
      </Pressable>
    );
  };

  return !webviewLink ? (
    <View style={tw`flex flex-col h-full bg-brand3`}>
      <ScrollView style={[{flex: 1, paddingHorizontal: 16}]}>
        <SafeAreaView edges={['top', 'left', 'right', 'bottom']}>
          <View>
            <Icon
              name="chevron-left"
              color={colors.White}
              style={{fontSize: 28}}
              onPress={() =>
                navigation.navigate('TabDirectory', {
                  screen: 'Tab_Me',
                })
              }
            />
            <View>
              <View style={tw`pt-5`}>
                <Text style={[globalStyles.H2, tw`text-white`]}>
                  {t('accountOpening.account_status.title')}
                </Text>
              </View>
              <View style={tw`py-2`}>
                <Text style={[globalStyles.Big_Text, {color: colors.MainFont}]}>
                  {t('accountOpening.account_status.username')} {displayName}
                </Text>
              </View>
            </View>
          </View>

          <View>
            {detail && detail?.status !== 'DRAFT' && (
              <AccountCard {...(entityId === 'EIE' ? items : itemEmx)} />
            )}
            {statusLv3.includes(detail?.status) && (
              <Text
                style={[
                  globalStyles.Note_120,
                  {marginTop: 8, color: colors.White},
                ]}>
                {t('accountOpening.recommended_trading_account.reffer')}
              </Text>
            )}
          </View>
          {detail && items.status !== 'rejected' && (
            <View style={{marginTop: 30}}>
              <Text style={[globalStyles.H4, {color: colors.White}]}>
                {t(
                  `accountOpening.application_status.${
                    detail.promoCode ? 'promo_applied' : 'no_promo'
                  }`,
                )}
              </Text>
              {detail?.promoCode && (
                <View style={tw`mt-[10px]`}>
                  <InputCheckBox
                    value={true}
                    onPress={() => {}}
                    labelComponent={
                      <View>
                        <Text
                          style={[
                            globalStyles.Body_Text_B,
                            {color: colors.White, marginBottom: 5},
                          ]}>
                          {detail.promoCode}
                        </Text>
                        <Text style={[globalStyles.Body_Text]}>
                          Code: {detail.promoCode}
                        </Text>
                      </View>
                    }
                  />
                </View>
              )}
            </View>
          )}
        </SafeAreaView>
      </ScrollView>
    </View>
  ) : (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: 0.1,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
          paddingHorizontal: 16,
          paddingBottom: 16,
        }}>
        <SafeAreaView edges={['top', 'left', 'right']}>
          <Icon
            name="close"
            style={{fontSize: 20}}
            onPress={() => setWebviewLink(null)}
          />
        </SafeAreaView>
      </View>
      <WebView
        containerStyle={{flex: 0.9}}
        source={{uri: webviewLink}}
        javaScriptCanOpenWindowsAutomatically={true}
        javaScriptEnabled={true}
        useWebKit={true}
        experimental={{
          postBody: true,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 5,
    padding: 7,
    backgroundColor: '#EEEEEE',
  },
  registerCard: {
    borderRadius: 12,
    backgroundColor: '#FED661',
    marginTop: '30%',
  },
  label: {
    backgroundColor: '#F4465E',
    borderRadius: 10,
    paddingHorizontal: 8,
    paddingVertical: 3,
    color: 'white',
    marginRight: 14,
  },
  dotIcon: {
    fontSize: 24,
  },
  font_24: {
    fontSize: 24,
  },
  arrowIcon: {
    fontSize: 32,
    marginTop: -15,
    backgroundColor: '#EEEEEE',
    width: 35,
    marginLeft: 18,
    alignItems: 'center',
    textAlign: 'center',
  },
  overlay: {
    width: '80%',
    borderRadius: 12,
  },
});

export default AccountStatus;
