import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  View,
  BackHandler,
} from 'react-native';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import ProgressHeader from '../../components/header/ProgressHeader';
import {entityId} from '../../config/constants';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {FinancialInformation} from '../../redux/accountOpening/type';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {bindActionCreators} from 'redux';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import InputText from '../../components/forms/inputs/InputText';
import Dropdown from '../../components/forms/inputs/Dropdown';
import DatePicker from '../../components/forms/inputs/DatePicker';
import InputCheckBox from '../../components/forms/inputs/inputCheckBox.component';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {RegionContent} from '../../redux/state/types';
import Button from '../../components/buttons/Button';
import {
  getAnnualIncomeList,
  getEmploymentStatusList,
  getIndustriesList,
  getJobTitleList,
  getNetworthList,
  getTradingFundList,
} from '../../utils/api/apiGeneralAccountOpening';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {updateFinancialInformation} from '../../utils/api/apiAccountOpening';
import {BottomSheetCreators} from '../../redux/BottomSheet/actionCreators';

interface AccountFinancialInformationProps {
  navigation: any;
  route?: any;
}

type FormItem = {
  label: string;
  name: keyof FinancialInformation;
  required?: boolean;
  disabled?: boolean;
  hide?: boolean;
  type: 'input-text' | 'dropdown' | 'date-picker';
  options?: any[];
  dropdownType?: 'bottomsheet' | 'page' | 'country' | 'other';
  tooltip?: string;
  maxLength?: number;
};

const RenderFormByType = ({
  label,
  name,
  required,
  disabled,
  type,
  tooltip,
  options,
  dropdownType,
  hide = false,
  formData,
  handleFormChange,
  errors,
  maxLength,
}: FormItem & {
  formData: FinancialInformation;
  handleFormChange: (val: any, name: string) => void;
  errors: any[];
}) => {
  if (hide) return null;
  switch (type) {
    case 'input-text':
      return (
        <InputText
          value={formData[name as keyof FinancialInformation] as string}
          name={name}
          label={label}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          editAble={!disabled}
          errors={errors}
          maxLength={maxLength}
        />
      );
    case 'dropdown':
      return (
        <Dropdown
          value={formData[name as keyof FinancialInformation]}
          label={label}
          name={name}
          onSelect={handleFormChange}
          options={options ?? []}
          required={required}
          disabled={disabled}
          wrapperStyle={tw`mb-[28px]`}
          errors={errors}
          type={dropdownType}
        />
      );
    case 'date-picker':
      return (
        <DatePicker
          value={formData[name as keyof FinancialInformation]}
          label={label}
          name={name}
          onChange={handleFormChange}
          required={required}
          wrapperStyle={tw`mb-[28px]`}
          disabled={disabled}
          tooltip={tooltip}
          errors={errors}
        />
      );
  }
};

const AccountFinancialInformation: React.FC<
  AccountFinancialInformationProps
> = ({navigation, route}) => {
  const {t} = useTranslation();
  const {width} = Dimensions.get('window');
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const dispatch = useDispatch();

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const {currentToken} = useSelector((state: State) => state.cognito);

  const regionOption = useSelector(
    (state: State) => state.state.regionDataList,
  );

  const financialInformationState = useSelector(
    (state: State) => state.accountOpening.financialInformation,
  );

  const {individual} = useSelector((state: State) => state.accountOpening);

  const {SetFinancialInformation} = bindActionCreators(
    AccountOpeningCreators,
    dispatch,
  );
  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const {isOpen: isOpenBottomSheet} = useSelector(
    (state: State) => state.bottomSheet,
  );
  const {CloseBottomSheet} = bindActionCreators(BottomSheetCreators, dispatch);

  const [pageLoading, setPageLoading] = useState<boolean>(false);
  const [dropdown, setDropdown] = useState({
    employmentStatus: [],
    annualIncome: [],
    networh: [],
    tradingFund: [],
    industries: [],
    jobTitles: [],
  });

  const [errors, setErrors] = useState<any[]>([]);
  const [formData, setFormData] = useState<FinancialInformation>({
    customerId: '',
    employmentStatus: '',
    industrial: '',
    jobTitle: '',
    annualIncome: '',
    liquidNetworth: '',
    jurisdictionOfTaxResidence: '',
    sourceOfFunds: '',
    haveTin: true,
    tinNumber: null,
  });

  const formList: FormItem[] = [
    {
      label: t('accountOpening.financial_information.employment_status'),
      name: 'employmentStatus',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: dropdown.employmentStatus,
    },
    {
      label: t('accountOpening.financial_information.industrial'),
      name: 'industrial',
      required:
        !formData.employmentStatus ||
        formData.employmentStatus === 'RETIRED' ||
        formData.employmentStatus === 'UNEMPLOYED'
          ? false
          : true,
      disabled: false,
      hide:
        !formData.employmentStatus ||
        formData.employmentStatus === 'RETIRED' ||
        formData.employmentStatus === 'UNEMPLOYED',
      type: 'dropdown',
      dropdownType: 'page',
      options: dropdown.industries,
    },
    {
      label: t('accountOpening.financial_information.job_title'),
      name: 'jobTitle',
      required: false,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'page',
      options: dropdown.jobTitles,
    },
    {
      label: t('accountOpening.financial_information.annual_income'),
      name: 'annualIncome',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: dropdown.annualIncome,
    },
    {
      label: t('accountOpening.financial_information.net_worth'),
      name: 'liquidNetworth',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'bottomsheet',
      options: dropdown.networh,
    },
    {
      label: t('accountOpening.financial_information.source_trading_fund'),
      name: 'sourceOfFunds',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'page',
      options: dropdown.tradingFund,
    },
    {
      label: t('accountOpening.financial_information.tax_residence'),
      name: 'jurisdictionOfTaxResidence',
      required: true,
      disabled: false,
      type: 'dropdown',
      dropdownType: 'country',
      options: regionOption.map((el: RegionContent) => ({
        value: el.regionCode,
        label: t(`region.${el.regionCode}`),
      })),
    },
  ];

  const handleFormChange = (val: any, name: string) => {
    setErrors(errors.filter(el => el !== name));

    if (name === 'haveTin' && val === true) {
      setFormData({...formData, [name]: val, tinNumber: null});
      return;
    }

    if (name === 'employmentStatus') {
      setFormData({...formData, [name]: val, industrial: null});
      return;
    }
    setFormData({...formData, [name]: val});
  };

  const onPressBack = () => {
    navigation.goBack();
    return true;
  };

  const handleSave = () => {
    const error: string[] = [];
    formList.map((el: FormItem) => {
      if (el.required && !formData[el.name]) {
        error.push(el.name);
      }
    });

    if (!formData.haveTin && !formData.tinNumber) {
      error.push('tinNumber');
    }

    if (error.length > 0) {
      setErrors(error);
      return;
    }

    setErrors([]);

    updateFinancialInformation({
      token: currentToken,
      payload: {
        ...formData,
        ...(formData.industrial &&
          formData.industrial.includes(t('accountOpening.purpose.OTHER')) && {
            industrialOther: formData.industrial.split(':')[1].trim(),
            industrial: 'OTHER',
          }),
        ...(formData.jobTitle &&
          formData.jobTitle.includes(t('accountOpening.purpose.OTHER')) && {
            jobTitleOther: formData.jobTitle.split(':')[1].trim(),
            jobTitle: 'OTHER',
          }),
        ...(formData.sourceOfFunds &&
          formData.sourceOfFunds.includes(
            t('accountOpening.purpose.OTHER'),
          ) && {
            sourceOfFundsOther: formData.sourceOfFunds.split(':')[1].trim(),
            sourceOfFunds: 'OTHER',
          }),
        isAdminPortal: false,
        customerId: individual?.id,
        haveTin: !formData.haveTin,
      },
      onSuccess: () => {
        SetFinancialInformation(formData);

        if (route?.params?.lastScreen) {
          navigation.navigate(route?.params?.lastScreen);
        } else {
          navigation.navigate('AccountTradingExperience');
        }
      },
      onFailed: () => {},
      setLoading: SetIsShowLoading,
    });
  };

  useEffect(() => {
    const fetchOptions = async () => {
      SetIsShowLoading(true);
      setPageLoading(true);
      const temp: any = {};
      await getEmploymentStatusList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.employmentStatus = mapOption;
        },
      });

      await getAnnualIncomeList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.annualIncome = mapOption;
        },
      });

      await getNetworthList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.networh = mapOption;
        },
      });

      await getTradingFundList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.tradingFund = mapOption;
        },
      });

      await getIndustriesList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.industries = mapOption;
        },
      });

      await getJobTitleList({
        token: currentToken,
        onSuccess: data => {
          const mapOption = data.response.map(el => ({
            value: el.code,
            label:
              lang === 'en'
                ? el.label
                : lang === 'tc'
                ? el.labelTc
                : el.labelSc,
          }));
          temp.jobTitles = mapOption;
        },
      });

      setDropdown(temp);
      SetIsShowLoading(false);
      setPageLoading(false);
    };

    fetchOptions();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentToken]);

  useEffect(() => {
    if (!pageLoading && dropdown.jobTitles.length > 0) {
      setFormData({
        ...financialInformationState,
        haveTin:
          financialInformationState.haveTin !== null
            ? !financialInformationState.haveTin
            : true,
        tinNumber: individual?.tinNumber,
      });
    }
  }, [financialInformationState, pageLoading, dropdown]);

  useFocusEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        if (isOpenBottomSheet) {
          CloseBottomSheet();
        } else {
          if (route?.params?.lastScreen) {
            navigation.navigate('AccountReviewApplication');
          } else {
            onPressBack();
          }
        }
        return true;
      },
    );

    return () => backHandler.remove();
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1, backgroundColor: colors.Brand3}}>
      <View style={{height: '15%', paddingHorizontal: 16}}>
        <SafeAreaView edges={['top', 'left', 'right']}>
          <View style={tw`flex flex-row items-center`}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBack}
            />
            <Text
              style={[tw`ml-[15px]`, globalStyles.H4, {color: colors.White}]}>
              {t('accountOpening.trading_account_opening')}
            </Text>
          </View>
        </SafeAreaView>
        <ProgressHeader currentStep="3" entity={entityId} />
      </View>
      <View
        style={[
          tw`rounded-t-xl`,
          {
            height: '85%',
            backgroundColor: colors.White,
          },
        ]}>
        <ScrollView>
          <View
            style={[
              tw`flex flex-col flex-grow px-[16px] pt-6 justify-between mt-[10px]`,
            ]}>
            <Text
              style={[tw`mb-[24px]`, globalStyles.H4, {color: colors.Brand2}]}>
              {t('accountOpening.financial_information.title')}
            </Text>
            {formList.map((el: FormItem, idx: number) => (
              <RenderFormByType
                {...el}
                key={idx}
                formData={formData}
                handleFormChange={handleFormChange}
                errors={errors}
              />
            ))}
            <View style={tw`mb-[20px]`}>
              <View style={[tw`flex flex-row`, {width: width - 90}]}>
                <InputCheckBox
                  value={formData.haveTin}
                  onPress={() => {
                    handleFormChange(!formData.haveTin, 'haveTin');
                  }}
                />
                <Text
                  style={[
                    tw`ml-[10px]`,
                    globalStyles.Note,
                    {color: colors.MainFont},
                  ]}>
                  {t('accountOpening.financial_information.checkbox')}
                </Text>
              </View>
            </View>
            {!formData.haveTin && (
              <InputText
                value={
                  formData['tinNumber' as keyof FinancialInformation] as string
                }
                name={'tinNumber'}
                label={t(
                  'accountOpening.financial_information.taxpayer_identification_number',
                )}
                onChange={handleFormChange}
                required={true}
                wrapperStyle={tw`mb-[28px]`}
                errors={errors}
              />
            )}
            <View style={[tw`py-5`]}>
              <Button label={t('common.save_and_next')} onPress={handleSave} />
            </View>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AccountFinancialInformation;
