import React, {useEffect, useLayoutEffect, useState} from 'react';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  StyleSheet,
  Pressable,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import {makeGlobalStyles} from '../../styles/styles';
import tw from '../../utils/tailwind';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../../components/buttons/Button';
import InputCheckBox from '../../components/forms/inputs/inputCheckBox.component';
import {useDispatch, useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../redux/root-reducer';
import {entityId} from '../../config/constants';
import {updateTradingAgreement} from '../../utils/api/apiAccountOpening';
import {bindActionCreators} from 'redux';
import {StateActionCreators} from '../../redux/state/actionCreator';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
interface RecommendedTradingAccountProps {
  navigation: any;
}

const RecommendedTradingAccount: React.FC<RecommendedTradingAccountProps> = ({
  navigation,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const {height, width} = Dimensions.get('window');
  const dispatch = useDispatch();

  const [checkbox, setCheckbox] = useState<string>('');
  const lang = useSelector((state: State) => state.state.setting.UserLanguage);
  const {currentToken} = useSelector((state: State) => state.cognito);
  const {id} = useSelector((state: State) => state.accountOpening.individual);

  const {SetIndividual} = bindActionCreators(AccountOpeningCreators, dispatch);
  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const onPressBackBtn = () => {
    navigation.goBack();
  };

  const onPressRegister = async () => {
    await updateTradingAgreement({
      token: currentToken,
      payload: {
        customerId: id,
        isAdminPortal: false,
        isRecommendedTradingAccountPage: true,
      },
      onSuccess: data => {
        SetIndividual(data.response);
        navigation.navigate('AccountAgreement');
      },
      onFailed: () => {},
      setLoading: SetIsShowLoading,
    });
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, [navigation]);

  const items = [
    ...(lang === 'en'
      ? [
          {
            title: 'EIEHK',
            benefits: [
              'Leverage Up To 1:50*',
              'Top Forex Pairs From Global Markets',
              'Top Conditions',
              'Quick Execution',
              'SFC License Type 3',
              'Offer Precious Metals Trading Services',
              'Convenient Deposit & Withdrawal Services',
              '24/5 Live Customer Service',
            ],
            recommended: true,
          },
        ]
      : []),
    ...(lang === 'tc'
      ? [
          {
            title: 'EIEHK',
            benefits: [
              '槓桿高達1:50*', // Leverage Up To 1:50*
              '進入全球最大規模及流動性最好的外匯市場', // Top Forex Pairs From Global Markets
              '頂級交易環境', // Top Conditions
              '輕鬆執行買賣策略', // Quick Execution
              '持有香港證監會第三類牌照', // SFC License Type 3
              '同時提供貴金屬產品交易', // Offer Precious Metals Trading Services
              '快捷方便的提款及存款服務', // Convenient Deposit & Withdrawal Services
              '24/5 真人客戶支援服務', // 24/5 Live Customer Service
            ],
            recommended: true,
          },
        ]
      : []),
    ...(lang === 'sc'
      ? [
          {
            title: 'EIEHK',
            benefits: [
              '杠杆高达1:50*', // Leverage Up To 1:50*
              '进入全球最大规模及流动性最好的外汇市场', // Top Forex Pairs From Global Markets
              '顶级交易环境', // Top Conditions
              '轻松执行买卖策略', // Quick Execution
              '持有香港证监会第三类牌照', // SFC License Type 3
              '同时提供贵金属产品交易', // Offer Precious Metals Trading Services
              '快捷方便的提款及存款服务', // Convenient Deposit & Withdrawal Services
              '24/5 真人客户支援服务', // 24/5 Live Customer Service
            ],
            recommended: true,
          },
        ]
      : []),
  ];

  const itemEmx = [
    ...(lang === 'en'
      ? [
          {
            title: 'Emperor Xpro',
            benefits: [
              'Leverage Up To 1:400*',
              'Top Forex Pairs From Global Markets',
              'Top Conditions',
              'Quick Execution',
              'Offer Precious Metals Trading Services',
              'Convenient Deposit & Withdrawal Services',
              'Online Customer Support',
            ],
            recommended: true,
          },
        ]
      : []),
    ...(lang === 'tc'
      ? [
          {
            title: 'Emperor Xpro',
            benefits: [
              '槓桿高達1:400*', // Leverage Up To 1:400*
              '進入全球最大規模及流動性最好的外匯市場', // Top Forex Pairs From Global Markets
              '頂級交易環境', // Top Conditions
              '輕鬆執行買賣策略', // Quick Execution
              '支持貴金屬交易', // Offer Precious Metals Trading Services
              '快捷方便的提款及存款服務', // Convenient Deposit & Withdrawal Services
              '線上客戶支援', // Online Customer Supporrt
            ],
            recommended: true,
          },
        ]
      : []),
    ...(lang === 'sc'
      ? [
          {
            title: 'Emperor Xpro',
            benefits: [
              '杠杆高达1:400*', // Leverage Up To 1:50*
              '进入全球最大规模及流动性最好的外汇市场', // Top Forex Pairs From Global Markets
              '顶级交易环境', // Top Conditions
              '轻松执行买卖策略', // Quick Execution
              '支持贵金属交易 ', // Offer Precious Metals Trading Services
              '快捷方便的提款及存款服务', // Convenient Deposit & Withdrawal Services
              '线上客户支援', // Online Customer Supporrt
            ],
            recommended: true,
          },
        ]
      : []),
  ];

  useEffect(() => {
    setCheckbox(entityId === 'EIE' ? 'EIEHK' : 'Emperor Xpro');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const AccountCard = ({
    title,
    benefits,
    recommended,
  }: {
    recommended: boolean;
    title: string;
    benefits: string[];
  }) => {
    const [open, setOpen] = useState<boolean>(checkbox === title);

    const leftContent =
      benefits.length > 4
        ? benefits.slice(0, Math.ceil(benefits.length / 2))
        : benefits;
    const rightContent =
      benefits.length > 4 ? benefits.slice(Math.ceil(benefits.length / 2)) : [];
    return (
      <View style={tw`pt-6`}>
        {recommended && (
          <View style={styles.recommended}>
            <Text
              style={[
                globalStyles.Big_Text_B,
                tw`text-white`,
                {fontSize: width / 28},
              ]}>
              {t('accountOpening.recommended_trading_account.recommended')}
            </Text>
          </View>
        )}
        <View
          style={[
            tw`flex flex-col`,
            recommended ? styles.cardRecommended : styles.card,
          ]}>
          <View style={[tw`flex flex-row justify-between py-[16px]`]}>
            <View style={tw`flex flex-row items-center`}>
              <InputCheckBox
                value={checkbox === title ? true : false}
                onPress={() => setCheckbox(title)}
                label={title}
                textStyle={{
                  color: colors.MainFont,
                  fontWeight: '600',
                  fontSize: 13,
                }}
              />
            </View>
            <View style={tw`flex flex-row items-center`}>
              <Text style={styles.label}>
                {t('accountOpening.recommended_trading_account.offer')}
              </Text>
              <Pressable onPress={() => setOpen(!open)}>
                <MaterialCommunityIcons
                  name={!open ? 'chevron-right' : 'chevron-down'}
                  size={24}
                  color={colors.fixedBackground}
                />
              </Pressable>
            </View>
          </View>
          {open && (
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.5, flexShrink: 1}}>
                {leftContent.map(
                  el =>
                    el && (
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          flexShrink: 1,
                          marginTop: 6,
                        }}>
                        <Icon
                          name="circle-medium"
                          color="#CFA872"
                          style={[styles.dotIcon, {flex: 0.2}]}
                        />
                        <Text
                          style={[
                            globalStyles.Note_B,
                            tw`text-black`,
                            {flex: 0.9},
                          ]}>
                          {el}
                        </Text>
                      </View>
                    ),
                )}
              </View>
              <View style={{flex: 0.5, flexShrink: 1}}>
                {rightContent.map(
                  el =>
                    el && (
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          flexShrink: 1,
                          marginTop: 6,
                        }}>
                        <Icon
                          name="circle-medium"
                          color="#CFA872"
                          style={[styles.dotIcon, {flex: 0.2}]}
                        />
                        <Text
                          style={[
                            globalStyles.Note_B,
                            tw`text-black`,
                            {flex: 0.9},
                          ]}>
                          {el}
                        </Text>
                      </View>
                    ),
                )}
              </View>
            </View>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={[tw`flex flex-col`]}>
      <View
        style={[
          tw`flex flex-col bg-brand3 flex-grow justify-center`,
          {height},
        ]}>
        <View style={{paddingHorizontal: 16}}>
          <SafeAreaView edges={['top', 'left', 'right']}>
            <Icon
              // eslint-disable-next-line react-native/no-inline-styles
              style={{fontSize: 28}}
              color="white"
              name="chevron-left"
              onPress={onPressBackBtn}
            />
          </SafeAreaView>
          <View style={tw`pt-5`}>
            <Text style={[globalStyles.H1, tw`text-white`]}>
              {t('accountOpening.recommended_trading_account.title')}
            </Text>
          </View>
          <View style={tw`py-2`}>
            <Text style={[globalStyles.Body_Text, {color: colors.MainFont}]}>
              {t('accountOpening.recommended_trading_account.subtitle')}
            </Text>
          </View>
        </View>
        <ScrollView style={[tw`rounded-t-xl bg-white`]}>
          <View style={[tw`flex flex-col flex-grow px-[16px]`]}>
            {(entityId === 'EIE' ? items : itemEmx).map(el => (
              <AccountCard
                title={el.title}
                benefits={el.benefits}
                recommended={el.recommended}
              />
            ))}
            <Text
              style={[
                globalStyles.Note_120,
                {marginTop: 8, color: colors.Grey5},
              ]}>
              {t('accountOpening.recommended_trading_account.reffer')}
            </Text>
          </View>
        </ScrollView>
        <View style={tw`bg-white py-5 px-[16px]`}>
          <Button
            label={t('accountOpening.recommended_trading_account.register')}
            onPress={onPressRegister}
            disabled={!checkbox}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  recommended: {
    backgroundColor: '#CFA872',
    width: 150,
    paddingHorizontal: 8,
    paddingVertical: 3,
    alignItems: 'center',
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
  },
  cardRecommended: {
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#CFA872',
    borderRadius: 5,
    borderTopLeftRadius: 0,
    padding: 5,
    backgroundColor: '#EEEEEE',
  },
  card: {
    borderRadius: 5,
    borderTopLeftRadius: 0,
    padding: 7,
    backgroundColor: '#EEEEEE',
  },
  label: {
    backgroundColor: '#F4465E',
    borderRadius: 5,
    paddingHorizontal: 8,
    paddingVertical: 3,
    color: 'white',
    marginRight: 14,
  },
  dotIcon: {
    fontSize: 24,
  },
});

export default RecommendedTradingAccount;
