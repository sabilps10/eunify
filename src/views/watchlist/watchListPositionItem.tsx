import { useIsFocused, useTheme } from '@react-navigation/native';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { ColorValue, Image, Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { useSelector } from 'react-redux';
import StackView from '../../components/stackView.component'
import { State } from '../../redux/root-reducer';
import { makeGlobalStyles } from '../../styles/styles';
import { THEME_TYPE } from '../../types/types';
import { toCurrencyDisplay } from '../../utils/stringUtils';
import ArrowRight from '../../../assets/svg/ArrowRight';
import Edit from '../../../assets/svg/Edit';
import ClosePosition from '../../../assets/svg/ClosePosition';
import PriceArrowDown from '../../../assets/svg/PriceArrowDown';
import PriceArrowUp from '../../../assets/svg/PriceArrowUp';
import WatchListPLIconComponent from './watchListPLIconComponent';
import PressableComponent from '../utils/PressableComponent';

interface WatchListPositionItem {
    positionTicket: number,
    onPressPositionDetails: (openPosition: number) => void,
    onPressPositionLiqidation: (openPosition: number) => void,
    onPressPositionEdit: (openPosition: number) => void,
}

const WatchListPositionItem = (params: WatchListPositionItem) => {

    const [t] = useTranslation();

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    
    const positionTicket = params.positionTicket
    const isFocused = useIsFocused();
    
    const position = useSelector((state: State) => state.trading.positions && state.trading.positions[params.positionTicket])

    const FloatingPnLComponent = (() => {
        const positionProfit = useSelector((state: State) => 
        {
            if (isFocused && state.trading.profits && state.trading.profits[params.positionTicket] !== undefined){
                return state.trading.profits[params.positionTicket]
            }
        });

        const symbols = useSelector((state: State) => state.trading.symbols)

        const floatingPnLColor = useMemo(() => {
            if (positionProfit > 0)
                return colors.Green;
            else if (positionProfit < 0)
                return colors.Red;
            else
                return colors.MainFont;
        }, [positionProfit]);

        const floatingPnL = useMemo(() => {
            if (symbols && symbols[position.Symbol] && positionProfit !== undefined)
            {
                if (positionProfit >= 0) 
                    // return "+" + positionProfit.toFixed(symbols[position.Symbol].Digits)
                    return '+' + toCurrencyDisplay(positionProfit)
                else
                    // return positionProfit.toFixed(symbols[position.Symbol].Digits)
                    return toCurrencyDisplay(positionProfit)
            }
            return "-";
        }, [positionProfit, symbols, position.Symbol]);

        return (
            <Text style={[globalStyles.Note, styles.floatingPnL, {color: floatingPnLColor}]}>
                {floatingPnL}
            </Text>
        )
    });

    return (
        position?
        <PressableComponent onPress={() => params.onPressPositionDetails(positionTicket)}>
            <StackView 
                style={styles.stackView}
                spacing={12}>
                <StackView spacing={5}>
                    {/* <FloatingPnLImage /> */}
                    <WatchListPLIconComponent positionTicket={positionTicket}/>
                    <Text style={[globalStyles.Note, styles.diraction, {color: position.Type == 0 ? colors.Green : colors.Red}]}>
                        {position.Type == 0 ? t('watchlist.BUY') : t('watchlist.SELL')}
                    </Text>
                    <Text style={[globalStyles.Note, styles.lot]}>
                        {position.Volume?.toFixed(2) ?? '-'}
                    </Text>
                </StackView>
                <FloatingPnLComponent/>
                <StackView spacing={12}>
                    <PressableComponent 
                        onPress={() => params.onPressPositionLiqidation(positionTicket)}
                        style={styles.liquidation}>
                        <ClosePosition color={colors.Brand3} />
                    </PressableComponent>
                    <PressableComponent 
                        onPress={() => params.onPressPositionEdit(positionTicket)}
                        style={styles.edit}>
                        <Edit color={colors.Brand3} />
                    </PressableComponent>
                    <View style={styles.details}>
                        <ArrowRight color={colors.Brand3} />
                    </View>
                </StackView>
            </StackView>
        </PressableComponent>
        :
        <>
        </>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    stackView: {
        marginLeft: 47,
        marginRight: 33,
        flex: 1,
        height: 40,
    },
    pnlStatus: {
        justifyContent: 'center'
    },
    diraction: {
        alignSelf: 'center',
    },
    lot: {
        alignSelf: 'center',
        color: colors.MainFont,
    },
    floatingPnL: {
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
    },
    liquidation: {
        justifyContent: 'center'
    },
    edit: {
        justifyContent: 'center'
    },
    details: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 24,
    }
})

export default React.memo(WatchListPositionItem)