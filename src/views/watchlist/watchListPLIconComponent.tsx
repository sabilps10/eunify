import { useIsFocused, useTheme } from '@react-navigation/native';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { ColorValue, Image, Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { useSelector } from 'react-redux';
import StackView from '../../components/stackView.component'
import { State } from '../../redux/root-reducer';
import { makeGlobalStyles } from '../../styles/styles';
import { THEME_TYPE } from '../../types/types';
import { toCurrencyDisplay } from '../../utils/stringUtils';
import ArrowRight from '../../../assets/svg/ArrowRight';
import Edit from '../../../assets/svg/Edit';
import ClosePosition from '../../../assets/svg/ClosePosition';
import PriceArrowDown from '../../../assets/svg/PriceArrowDown';
import PriceArrowUp from '../../../assets/svg/PriceArrowUp';

interface WatchListPositionItem {
    positionTicket: number
}

const WatchListPLIconComponent = (params: WatchListPositionItem) => {

    const [t] = useTranslation();

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    
    const positionTicket = params.positionTicket
    const isFocused = useIsFocused();

    const PriceArrowUpMemo = React.memo(PriceArrowUp);
    const PriceArrowDownMemo = React.memo(PriceArrowDown);

    const positionProfitsType = useSelector((state: State) => isFocused && state.trading.profitsType && state.trading.profitsType[params.positionTicket] ? state.trading.profitsType[params.positionTicket] : 0);

    return (
        <View style={styles.pnlStatus}>
            {positionProfitsType > 0  && <PriceArrowUpMemo color={colors.Up} />}
            {positionProfitsType < 0 && <PriceArrowDownMemo color={colors.Down} />}
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    stackView: {
        marginLeft: 47,
        marginRight: 33,
        flex: 1,
        height: 40,
    },
    pnlStatus: {
        justifyContent: 'center'
    },
    diraction: {
        alignSelf: 'center',
    },
    lot: {
        alignSelf: 'center',
        color: colors.MainFont,
    },
    floatingPnL: {
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
    },
    liquidation: {
        justifyContent: 'center'
    },
    edit: {
        justifyContent: 'center'
    },
    details: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 24,
    }
})

export default React.memo(WatchListPLIconComponent)