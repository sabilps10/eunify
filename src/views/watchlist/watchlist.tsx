import React, { Fragment, useLayoutEffect, useState, useMemo, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, Pressable, useColorScheme, Image, SectionList, Alert, Linking } from 'react-native';
import { BottomTabProps, Props } from '../../routes/route';
import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import HeaderButton from '../../components/header/headerButton.component';
import { useTranslation } from 'react-i18next';
import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import StackView from '../../components/stackView.component'

import { bindActionCreators } from 'redux';
import { ActionUtils } from '../utils/ActionUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { categoryName, displayContractCode, getTranslationDict } from '../utils/CommonFunction';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import { DetailsType } from '../../utils/orderUtils';
import { PermissionAccessType } from '../permission/permissionAccess';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { lv3Registration } from '../../utils/api/userApi';
import { THEME_TYPE } from '../../types/types';
import WatchListComponent from './WatchListComponent';
import Plus from '../../../assets/svg/Plus';
import Setting from '../../../assets/svg/Setting';
import ArrowRight from '../../../assets/svg/ArrowRight';
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import TopBarNoticeNew from '../../../assets/svg/TopBarNoticeNew';
import TopBarMoney from '../../../assets/svg/TopBarMoney';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { store } from '../../redux/store';
import { addWatchList } from '../../utils/api/tradingApi';
import { MIOType } from '../../components/MIO';
import symbolicateStackTrace from 'react-native/Libraries/Core/Devtools/symbolicateStackTrace';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { SelectionContextLocation } from '../../redux/selection/type';

const WatchlistView: React.FC<BottomTabProps<'Tab_WatchlistView'>> = ({ route, navigation }) => {



    const [t] = useTranslation();
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const styles = makeStyles(colors)
    const dispatch = useDispatch();

    console.debug("watchlistView: ");

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const myList = useSelector((state: State) => state.state.myList ?? []);
    const symbols = useSelector((state: State) => state.trading.symbols);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const watchlistCategory = useSelector((state: State) => state.selection.selected?.watchlistCategory ?? 'MyList')
    const watchlistPriceChange = useSelector((state: State) => state.selection.selected?.watchlistPriceChange ?? 'percentageChange')

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const region = useSelector((state: State) => state.state.setting?.UserRegion)
    const setting = useSelector((state: State) => state.state.setting)
    const demoTradingAccount = useSelector((state: State) => state.account.demoTradingAccount);
    const enterDemoTab = useSelector((state: State) => state.account.enterDemoTab ?? TabDirectoryTab.QUOTE);

    const positions = useSelector((state: State) => state.trading.positions);

    const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

    const { SetIsShowLoading, SetMyList, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSelected, OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

    const { onPressExitDemo } = ActionUtils();

    useEffect(() => {
        if (!positions) return;

        const state: State = store.getState();
        const accessToken = state.account.account?.AccessToken;
        if (!accessToken) return;

        const watchlist = [...myList];
        Object.values(positions).forEach((position) =>{
            const symbol = displayContractCode(position.Symbol)

            const isAddedWatchlist = watchlist.find(obj => obj === symbol)
            if (!isAddedWatchlist)
            {            
                addWatchList(accessToken, symbol);
                watchlist.push(symbol)
            }
        })

        if (myList.length !== watchlist.length)
        {
            SetMyList([...new Set(watchlist)])
        }    
            
    }, [positions])

    const sectionData = useMemo(() => {
        if (!symbols) return
        // console.debug('Object.keys(symbols):' + Object.keys(symbols))

        if (!watchlistCategory) return

        console.debug('watchlistCategory:' + watchlistCategory)
        // console.debug('Object.keys(positions).length:' + Object.keys(positions).length)
        // console.debug('recommendList:' + recommendList)

        var filteredList = []
        let isMyList = watchlistCategory === 'MyList'
        let isShowAll = watchlistCategory === 'ShowAll'
        //let symbolKeys = Object.keys(symbols)
        let symbolKeys = Object.keys(symbols).filter((symbol) => symbol.indexOf(".") > 0);


        if (isMyList) {
            // Use `MyList` to map current region supporting Symbol(s).
            filteredList = myList.map((symbol) => {
                // Found `MyList` symbol in current region supporting Symbol(s) index.
                let index = symbolKeys.findIndex((value) => symbol === value.split('.', 1).toString())
                // if found means this Sybmol in `MyList` is support in this region.
                // Otherwise, means not support and which should NOT be display.
                return index > -1 ? symbolKeys[index] : null
            })
                .filter(element => element)
        }
        else if (isShowAll) {
            filteredList = symbolKeys
        }
        else {
            // Not select to display `MyList`, so use recommand list
            filteredList = symbolKeys
            if (!symbols) return

            // Not select to display `MyList`, `ShowAll`. So, apply filter by Category.
            filteredList = filteredList.filter((symbol) => {
                let symbolParam = symbols[symbol]

                if (!symbolParam) return false
                return symbolParam.DisplayCategory === watchlistCategory
            })
        }

        console.debug('filteredList:' + filteredList)

        let positionsValue = positions ? Object.values(positions) : []

        // Map all display Contract with corresponding position of Ticket(s).
        let sectionData: { symbol: string, data: number[] }[] = filteredList.flatMap((symbol) => {
            let tickets: number[] = positionsValue.filter((position) => position.Symbol === symbol)
                // Sort by Open Position create time in Descending order.
                .sort((a, b) => a.OpenTime < b.OpenTime ? 1 : -1)
                .flatMap((position) => position.Ticket)
            return { symbol: symbol, data: tickets }
        })
        //console.debug('sectionData:' + JSON.stringify(sectionData))
        //console.debug('sectionData.length:' + sectionData.length)

        return sectionData

    }, [watchlistCategory, myList, positions, symbols])

    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            return symbol.DisplayCategory
        });

        //console.debug(JSON.stringify(categories));

        return [...new Set(categories)];
    }, [symbols])

    useLayoutEffect(() => {
        var rightComponents = [
            // <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        if (isDemoAccount) {
            rightComponents = [
                // <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />,
                <HeaderButton onPress={()=> onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton />} />
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents.push(
                <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.White} />} />
            )
        }

        navigation.setOptions({
            header: () => <ExpandableHeader
                isDemo={isDemoAccount}
                rightComponents={rightComponents}
            />,
        });
    }, [navigation, stateLoginLevel, isDemoAccount, categories])

    const onPressDeposit = async () => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.QUOTE, param: { type: MIOType.Deposit }})
    }

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    const onPressSearch =() => {
        let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
        navigation.navigate('SearchProductView', { categories: searchCategories })
    };

    const onPressRegisterRealTradingAccount = () => {
        Linking.openURL(lv3Registration)
    }

    const onPressNotifications = () => {
        console.debug('onPressNotifications')
        // navigation.push('')
    }

    const onPressShowPriceChange = () => {
        SetSelected({
            watchlistPriceChange: watchlistPriceChange === 'priceChange' ? 'percentageChange' : 'priceChange'
        });
    }

    const onPressCategory = () => {
        const selectionOptions = ['MyList', 'ShowAll', ...categories];
        OpenCloseSelection({
            selectedOption: watchlistCategory,
            selectOptions: getTranslationDict(selectionOptions, 'contractCategory', t),
            isOpen: !isOpen,
            location: SelectionContextLocation.WatchlistCategory
        })
        console.debug(`[WatchlistView onPressCategory] OpenCloseSelection`)
    }

    const onPressAddWatchList = () => {
        let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
        navigation.navigate('SearchProductView', { categories: searchCategories })
    }

    const onPressSetting = () => {
        navigation.navigate('EditWatchlistView')
    }

    const onPressPlaceOrder = useCallback((contractCode: string, isBuy: boolean, loginLevel: number, isNonTrading: boolean) => {
        console.debug("onPressPlaceOrder 1", loginLevel)
        if (loginLevel === 1) {
            navigation.navigate('PermissionAccess', { type: PermissionAccessType.member_only })
            return
        } else if (loginLevel === 2) {
            console.debug("onPressPlaceOrder 2", demoTradingAccount)
            console.debug("onPressPlaceOrder 3", isDemoAccount)
            if (!demoTradingAccount) {
                navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_no_demo})
                return
            } else if (!isDemoAccount) {
                navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_has_demo})
                return
            }
        }

        if (isNonTrading) {
            SetIsShowMessageDialog({ isShowDialog: true, title: t('common.alert'), message: t('watchlist.nonTradableContract') })
            return
        }

        navigation.navigate('NewOrder', { symbolName: contractCode, isBuy: isBuy })
    }, [demoTradingAccount, isDemoAccount]);

    const onPressItem = useCallback((contractCode: string) => {
        console.debug(contractCode)
        navigation.navigate('ContractDetailView', { contractCode: contractCode })
    }, []);

    const onPressPositionDetails = useCallback((positionTicket: number) => {
        navigation.navigate('PositionDetails', { type: DetailsType.NEW_POSITION, ticketId: positionTicket })
    }, []);

    const onPressPositionLiqidation = useCallback((positionTicket: number) => {
        navigation.navigate('ClosePosition', { ticketId: positionTicket })
    }, []);

    const onPressPositionEdit = useCallback((positionTicket: number) => {
        navigation.navigate('EditPosition', { ticketId: positionTicket })
    }, []);

    // const listEmptyComponent = () => {
    //     return (
    //         <View
    //             style={{
    //                 flex: 1,
    //                 justifyContent: 'center',
    //             }}
    //         >
    //             <Text
    //                 style={{
    //                     textAlign: 'center',
    //                     marginHorizontal: 10,
    //                 }}
    //                 numberOfLines={0}
    //             >
    //                 No data
    //             </Text>
    //         </View>
    //     )
    // }

    return (
        <Fragment>
            <View style={styles.container}>
                <View style={styles.titleContainer}>
                    <Text style={[globalStyles.H2, styles.title]}>{t('watchlist.title')}</Text>
                    <View style={styles.watchlistButtons}>
                        <StackView direction='row' spacing={4}>
                            <PressableComponent disabled={sectionData === undefined} onPress={onPressAddWatchList}>
                                <TopBarSearch color={sectionData === undefined ? colors.Grey3 : colors.MainFont} />
                            </PressableComponent>
                            <PressableComponent disabled={sectionData === undefined} onPress={onPressSetting}>
                                <Setting color={sectionData === undefined ? colors.Grey3 : colors.MainFont} />
                            </PressableComponent>
                        </StackView>
                    </View>
                </View>
                <View style={styles.watchlistContainer}>
                    <View style={styles.watchlistInfoBar}>
                        <PressableComponent disabled={sectionData === undefined} style={styles.watchlistInfoBarWrapper} onPress={onPressShowPriceChange}>
                            <StackView spacing={8} style={{ alignItems: 'center' }}>
                                <Text style={[globalStyles.Note, {color: sectionData === undefined ? colors.Grey3 : colors.MainFont}]}>{t('watchlist.' + watchlistPriceChange)}</Text>
                                <ArrowRight color={colors.Brand3} />
                            </StackView>
                        </PressableComponent>
                        <PressableComponent disabled={sectionData === undefined} style={styles.watchlistInfoBarWrapper} onPress={onPressCategory}>
                            <StackView spacing={8} style={{ alignItems: 'center' }}>
                                <Text style={[globalStyles.Big_Text_B, {color: sectionData === undefined ? colors.Grey3 : colors.MainFont}]}>{categoryName(watchlistCategory)}</Text>
                                <CircleArrow20px color={colors.White} backgroundColor={colors.Brand3} />
                            </StackView>
                        </PressableComponent>
                    </View>
                    {
                        sectionData &&
                        <WatchListComponent
                            sectionData={sectionData}
                            onPressItem={onPressItem}
                            onPressPlaceOrder={onPressPlaceOrder}
                            onPressPositionDetails={onPressPositionDetails}
                            onPressPositionLiqidation={onPressPositionLiqidation}
                            onPressPositionEdit={onPressPositionEdit} />
                    }
                </View>
            </View>
        </Fragment>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WrapperBg,
    },
    titleContainer: {
        marginHorizontal: 16,
        marginTop: 33,
        marginBottom: 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        color: colors.SubTitleFont,
    },
    watchlistButtons: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    holdingContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    // watchlist
    watchlistContainer: {
        flex: 1,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: colors.MainBg,
    },
    watchlistInfoBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 74,
        paddingLeft: 14,
        paddingRight: 19,
    },
    watchlistInfoBarWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    sectionSeparator: {
        height: 1,
        marginHorizontal: 15,
        backgroundColor: colors.WrapperBg,
    },
});

export default WatchlistView;