import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, Image, Pressable, StyleSheet, Text, TextStyle, useColorScheme, View } from 'react-native';
import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

interface ComponentInterface {
    contract: string,
    textStyle?: TextStyle
}

const BidPriceComponent = ({contract, textStyle}: ComponentInterface) => {

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    const [previousBid, setPreviousBid] = useState(0)
    const [bidPrice, setBidPrice] = useState('-')
    const [bidColor, setBidColor] = useState(colors.Text)
    const isFocused = useIsFocused()

    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract])
    const bid = useSelector((state: State) => {
        if (isFocused === true && state.trading.prices && state.trading.prices[contract]?.Bid)
            return state.trading.prices[contract].Bid
    })

    useFocusEffect(
        useCallback(() => {
            let digits = symbol?.Digits

            if (bid == null) return
            if (digits == null) return
            if (bid === previousBid) return

            if (bid > previousBid) {
                setBidColor(colors.Up)
            }
            else if (bid < previousBid) {
                setBidColor(colors.Down)
            }
            setPreviousBid(bid);
            setBidPrice(bid.toFixed(digits))

            const bidTimeout = setTimeout(function () { setBidColor(colors.Text) }, 500);
            
            return function cleanup() {
                clearTimeout(bidTimeout);
            }
        }, [bid])
    );

    const bidPriceComponent = useMemo(() => {
        return (
            <Text style={[globalStyles.H3, styles.priceQuote, textStyle, { color: bidColor }]}>
                {bidPrice}
            </Text>
        )
    }, [bidColor, bidPrice])

    return (
        <Fragment>
            {bidPriceComponent}
        </Fragment>
    )
}

const styles = StyleSheet.create({
    priceQuote: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
});

export default BidPriceComponent;