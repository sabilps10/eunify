import React, { Fragment, useLayoutEffect, useState, useMemo, useEffect } from 'react';
import { View, Text, StyleSheet, Pressable, useColorScheme, Image, SectionList, Alert, Linking } from 'react-native';
import { useIsFocused, useTheme } from '@react-navigation/native';
import WatchListItem from './watchListItem';
import StackView from '../../components/stackView.component'
import WatchListPositionItem from './watchListPositionItem';
import { THEME_TYPE } from '../../types/types';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { State } from '../../redux/root-reducer';

interface ComponentInterface {
    sectionData: { symbol: string, data: number[] }[],
    onPressItem: (contractCode: string) => void,
    onPressPlaceOrder: (contractCode: string, isBuy: boolean, loginLevel: number, isNonTrading: boolean) => void,
    onPressPositionDetails: (positionTicket: number) => void,
    onPressPositionLiqidation: (positionTicket: number) => void,
    onPressPositionEdit: (positionTicket: number) => void
}

const WatchListComponent = ({ sectionData, onPressItem, onPressPlaceOrder, onPressPositionDetails, onPressPositionLiqidation, onPressPositionEdit }: ComponentInterface) => {

    console.debug("WatchListComponent: " + sectionData.length)
    const { colors } = useTheme()
    const styles = makeStyles(colors)

    // const onTouch = useSelector((state: State) => state.state.onTouch);

    const dispatch = useDispatch();
    const { SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    
    const renderSectionHeader = ({ item }: { item: string }) => (
        <WatchListItem
            contractCode={item}
            onPressItem={onPressItem}
            onPressPlaceOrder={onPressPlaceOrder} />
    )

    const renderItem = ({ positionTicket }: { positionTicket: number }) => {
        return <WatchListPositionItem
            positionTicket={positionTicket}
            onPressPositionDetails={onPressPositionDetails}
            onPressPositionLiqidation={onPressPositionLiqidation}
            onPressPositionEdit={onPressPositionEdit} />
    }

    const renderSectionFooter = ({ positionCount }: { positionCount: number }) => {
        let sectionSeparator = <View style={styles.sectionSeparator} />

        return (
            <StackView direction='column'>
                {positionCount === 0 ? null : sectionSeparator}
                <View style={{ height: 20 }} />
            </StackView>
        )
    }

    return (
        <SectionList
            showsVerticalScrollIndicator={false}
            sections={sectionData}
            keyExtractor={(item, index) => (item + " " + index).toString()}
            stickySectionHeadersEnabled={false}
            initialNumToRender={20}
            windowSize={5}
            renderSectionHeader={(info) => renderSectionHeader({ item: info.section.symbol })}
            renderItem={({ item }) => renderItem({ positionTicket: item })}
            renderSectionFooter={(info) => renderSectionFooter({ positionCount: info.section.data.length })} 
            // onTouchStart={()=>{SetOnTouch(!onTouch)}}
            />
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    sectionSeparator: {
        height: 1,
        marginHorizontal: 15,
        backgroundColor: colors.WrapperBg,
    },
});

export default React.memo(WatchListComponent);