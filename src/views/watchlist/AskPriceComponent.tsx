import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, Image, Pressable, StyleSheet, Text, TextStyle, useColorScheme, View } from 'react-native';
import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

interface ComponentInterface {
    contract: string,
    textStyle?: TextStyle
}

const AskPriceComponent = ({contract, textStyle}: ComponentInterface) => {
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const isFocused = useIsFocused()
    
    const [previousAsk, setPreviousAsk] = useState(0)
    const [askPrice, setAskPrice] = useState('-')
    const [askColor, setAskColor] = useState(colors.Text)

    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract])
    const ask = useSelector((state: State) => {
        if (isFocused === true && state.trading.prices && state.trading.prices[contract]?.Ask)
            return state.trading.prices[contract].Ask
    })

    useFocusEffect(
        useCallback(() => {
            let digits = symbol?.Digits

            if (ask == null) return
            if (digits == null) return
            if (ask === previousAsk) return;
    
            if (ask > previousAsk) {
                setAskColor(colors.Up)
            }
            else if (ask < previousAsk) {
                setAskColor(colors.Down)
            }
            setAskPrice(ask.toFixed(digits)) 
            setPreviousAsk(ask);

            const askTimeout = setTimeout(function () { setAskColor(colors.Text) }, 500);

            return function cleanup() {
                clearTimeout(askTimeout);
            }
        }, [ask])
    );

    const askPriceComponent = useMemo(() => {
        return (
            <Text style={[globalStyles.H3, styles.priceQuote, textStyle, { color: askColor }]}>
                {askPrice}
            </Text>
        )
    }, [askColor, askPrice])

    return (
        <Fragment>
            {askPriceComponent}
        </Fragment>
    )
}

const styles = StyleSheet.create({
    priceQuote: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
});

export default AskPriceComponent;