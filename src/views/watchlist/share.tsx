import React, { useLayoutEffect } from 'react'
import { Image, Pressable, StyleSheet, Text, View, useColorScheme } from 'react-native'
import { Props } from '../../routes/route';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import ActionButton from '../../components/buttons/actionButton.component';
import { useTranslation } from 'react-i18next';
import PressableComponent from '../utils/PressableComponent';
import Remove from '../../../assets/svg/Remove';

const ShareView: React.FC<Props<'ShareView'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: false,
        });
    }, [navigation]);

    const onPressShare = () => { console.debug('onPressShare') }

    return (
        <View style={styles.container}>
            <View style={[styles.card, { backgroundColor: colors.White }]}>
                <View style={styles.titleContainer}>
                    <Text style={[globalStyles.H2, { color: colors.Grey3 }]}>Loco London Gold</Text>
                    <Text style={[globalStyles.H2, { color: colors.Up }]}>+6.76%</Text>
                </View>
                <View style={{ height: 15 }} />
                <View style={[styles.bannerContainer, { backgroundColor: colors.Brand2 }]}>

                </View>
                <View style={{ height: 15 }} />
                <ActionButton title={t('share.share')} onPress={onPressShare} />
                <View style={{ height: 15 }} />
                <PressableComponent>
                    <Remove style={{height: 32, width: 32}} color={colors.MainFont} />
                </PressableComponent>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        width: 290,
        borderRadius: 12,
        alignItems: 'center',
        paddingHorizontal: 12,
        paddingTop: 30,
        paddingBottom: 20
    },
    titleContainer: {
        width: '100%',
    },
    bannerContainer: {
        width: '100%',
        height: 140,
        borderRadius: 12,
    }
});

export default ShareView