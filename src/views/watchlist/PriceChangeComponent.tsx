import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, Image, Pressable, StyleSheet, Text, TextStyle, useColorScheme, View } from 'react-native';
import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { priceChangePercentageDP } from '../../config/constants';

interface ComponentInterface {
    contract: string,
    type?: string,
    textStyle?: TextStyle,
}

const PriceChangeComponent = ({ contract, type, textStyle }: ComponentInterface) => {

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    const [changePrice, setChangePrice] = useState('-')
    const [changePricePercentage, setChangePricePercentage] = useState('-')
    const [changePriceColor, setChangePriceColor] = useState(colors.Text)
    const isFocused = useIsFocused()

    const close = useSelector((state: State) => (state.trading.prices && state.trading.prices[contract]?.Close) ?? 0)
    const bid = useSelector((state: State) => {
        if (isFocused === true && state.trading.prices && state.trading.prices[contract]?.Bid)
            return state.trading.prices[contract].Bid
    })
    
    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract])

    useFocusEffect(
        useCallback(() => {
            if (bid == null || !symbol) return;
        
            const digits = symbol.Digits;
            const haveClosePrice = close !== 0;
            const changePrice = haveClosePrice ? bid - close : 0;
            const changePricePercentage = haveClosePrice ? ((changePrice / close) * 100.0) : 0;
            const formattedChangePrice = `${changePrice > 0 ? '+' : ''}${changePrice.toFixed(digits)}`;
            const formattedChangePricePercentage = `${changePricePercentage > 0 ? '+' : ''}${changePricePercentage.toFixed(priceChangePercentageDP)}`;
        
            setChangePriceColor(changePrice > 0 ? colors.Up : changePrice < 0 ? colors.Down : colors.Text);
            setChangePrice(formattedChangePrice);
            setChangePricePercentage(formattedChangePricePercentage);
        }, [bid, close])
    );
      
      return (
        <Text style={[globalStyles.Body_Text_B, textStyle, { color: changePriceColor }]}>
          {type === "priceChange" ? changePrice : type === "percentageChange" ? `${changePricePercentage}%` :
            <Fragment>
              {changePrice} ({changePricePercentage}%)
            </Fragment>
          }
        </Text>
      );
}

export default React.memo(PriceChangeComponent);