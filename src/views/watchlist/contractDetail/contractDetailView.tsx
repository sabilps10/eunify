import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';
import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, BackHandler, Image, Platform, Pressable, StatusBar, StyleSheet, Text, useColorScheme, useWindowDimensions, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Props } from '../../../routes/route';

import OverView from './contractDetailTab/ContractDetailOverviewTab';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../../styles/styles';
import { useTranslation } from 'react-i18next';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { State } from '../../../redux/root-reducer';
import ExpandableHeader from '../../../components/header/expandableHeader.component';
import HeaderButton from '../../../components/header/headerButton.component';
import ExitDemoButton from '../../../components/buttons/exitDemo.component';
import { AccountUtils } from '../../../utils/accountUtils';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../../redux/state/actionCreator';
import { WebsocketUtils } from '../../../utils/websocketUtils';
import Orientation, {
    useOrientationChange,
    PORTRAIT,
    OrientationType,
    useDeviceOrientationChange,
    LANDSCAPE,
} from 'react-native-orientation-locker';
import { Dimensions } from 'react-native';
import { PermissionAccessType } from '../../permission/permissionAccess';
import { TabDirectoryTab } from '../../tabDirectory/tabDirectory';
import { THEME_TYPE } from '../../../types/types';
import PortraitComponent from './PortraitComponent';
import LandscapeComponent from './LandscapeComponent';
import TopBarSearch from '../../../../assets/svg/TopbarSearch';
import TopBarBack from '../../../../assets/svg/TopBarBack';
import TopBarMoney from '../../../../assets/svg/TopBarMoney';
import { ConnectionActionCreators } from '../../../redux/connection/actionCreator';
import { MIOType } from '../../../components/MIO';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../../tabDirectory/tabDirectoryWebView';
import { store } from '../../../redux/store';
import { ActionUtils } from '../../utils/ActionUtils';

const ContractDetailView: React.FC<Props<'ContractDetailView'>> = ({ route, navigation }) => {

    console.debug("ContractDetailView")

    const [t] = useTranslation();
    
    const isFocused = useIsFocused();
    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    const { height, width } = useWindowDimensions();

    const { contractCode, defaultTab = 0, defaultPositionTab = 0, isEnterByOrder = false } = route.params
    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contractCode])
    const symbols = useSelector((state: State) => state.trading.symbols)

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1)
    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount)
    const demoTradingAccount = useSelector((state: State) => state.account.demoTradingAccount);

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
    const { onPressExitDemo } = ActionUtils();
    const { subscribeSymbolDetail, unsubscribeSymbolDetail } = WebsocketUtils();

    const [orientation, setOrientation] = useState<OrientationType>(OrientationType.PORTRAIT);
    const [isSystemOrientationLock, setIsSystemOrientationLock] = useState<boolean>(false);
    const [tempDefaultTab, setTempDefaultTab] = useState<number>(defaultTab);
    
    ////////
    // Work around of iOS 16 orientation issue.
    // https://github.com/wonday/react-native-orientation-locker/pull/243
    // https://github.com/wonday/react-native-orientation-locker/issues/239
    Dimensions.addEventListener('change', async() => {
        Orientation.getOrientation((orientation) => {
            console.debug('Dimensions:' + orientation, isSystemOrientationLock)
            setOrientation(orientation)
        });
    });
    //
    ////////

    useFocusEffect(
        useCallback(() => {
            console.debug("Tab Directory");

            const backAction = async() => {
                onPressBack();
                return true;
            };

            const backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );

            return () => {
                backHandler.remove();
            }
        }, [])
    );

    useEffect(() => {
        console.debug('Orientation.isLocked()', Orientation.isLocked())
        if (Platform.OS === 'android') {
            Orientation.getAutoRotateState((rotationLock) => setIsSystemOrientationLock(!rotationLock));
        }
        //Orientation.unlockAllOrientations();
        setTempDefaultTab(0)

        return function cleanup() {
            Orientation.lockToPortrait();
        };
    }, []);

    useOrientationChange(async(o) => {
        var isLock = isSystemOrientationLock;
        // console.error('isLock 2.1', isLock, isSystemOrientationLock)
        if (Platform.OS === 'android') {
            await Orientation.getAutoRotateState((rotationLock) => {
                setIsSystemOrientationLock(!rotationLock);
                isLock = !rotationLock
                if (!isLock) {
                    console.debug(`${Date.now()} useOrientationChange: ${o}, height: ${height}, width: ${width}`);
                    if (o && o !== OrientationType.UNKNOWN)
                        setOrientation(o)
                }
            });
        } else {
            console.debug(`${Date.now()} useOrientationChange: ${o}, height: ${height}, width: ${width}`);
            if (o && o !== OrientationType.UNKNOWN)
                setOrientation(o)
        }
    });

    useDeviceOrientationChange(async(o) => {
        if (isFocused) {
            var isLock = isSystemOrientationLock;
            if (Platform.OS === 'android') {
                await Orientation.getAutoRotateState((rotationLock) => {
                    setIsSystemOrientationLock(!rotationLock);
                    isLock = !rotationLock
                    if (o && o !== OrientationType.UNKNOWN && isFocused && !isLock)
                    {
                        if (orientation !== o && Orientation.isLocked()){
                            console.debug(`${Date.now()} useDeviceOrientationChange: ${o}, height: ${height}, width: ${width}`);
                            Orientation.unlockAllOrientations();
                        }
                    } else {
                        if (orientation !== OrientationType['LANDSCAPE-LEFT'] && orientation !== OrientationType['LANDSCAPE-RIGHT']) {
                            Orientation.lockToPortrait();
                        }
                    }
                });
            }
            else {
                if (orientation !== o && Orientation.isLocked()){
                    console.debug(`${Date.now()} useDeviceOrientationChange: ${o}, height: ${height}, width: ${width}`);
                    Orientation.unlockAllOrientations();
                } else {
                    Orientation.lockToPortrait();
                }
            }
        } else {
            Orientation.lockToPortrait();
        }
    });

    const categories = useMemo(() => {
        if (symbols === null) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            if (symbol.SymbolName.indexOf(".") > 0)
                return symbol.DisplayCategory
        });

        return [...new Set(categories)];
    }, [symbols])

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    useLayoutEffect(() => {
        var leftComponents = [
            <HeaderButton onPress={onPressBack} iconComponent={<TopBarBack color={colors.White} />} />
        ]
        var rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        if (isDemoAccount) {
            leftComponents = [
                <HeaderButton onPress={onPressBack} iconComponent={<TopBarBack color={colors.Text} />} />
            ]
            rightComponents = [
                // <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />,
                <HeaderButton onPress={() => onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton />} />
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents = [
                <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.White} />} />
            ]
        }

        navigation.setOptions({
            headerShown: orientation === OrientationType.PORTRAIT && height > width ? true : false,
            header: () => <ExpandableHeader
                isDemo={isDemoAccount}
                leftComponents={leftComponents}
                rightComponents={rightComponents}
            />,
        });
    }, [navigation, stateLoginLevel, isDemoAccount, orientation, height, width, categories])

    useEffect(() => {
        const fetchData = async () => {
            await subscribeSymbolDetail(contractCode);
        }
        fetchData().catch(console.error);

        async function cleanup() {
            await unsubscribeSymbolDetail(contractCode);
        };

    }, [contractCode])

    const onPressDeposit = async () => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.QUOTE, param: { type: MIOType.Deposit }})
    }


    const onPressBack = () => {
        if (isEnterByOrder) {
            navigation.navigate("TabDirectory", {screen: TabDirectoryTab.QUOTE})
        } else {
            navigation.goBack()
        }
    }

    const onPressPriceQuote = useCallback((isBuy: boolean) => {
        Orientation.lockToPortrait()
        
        if (stateLoginLevel === 1) {
            navigation.navigate('PermissionAccess', { type: PermissionAccessType.member_only })
            return
        }
        if (stateLoginLevel === 2) {
            if (!demoTradingAccount) {
                navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_no_demo})
                return
            }
            if (!isDemoAccount) {
                navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_has_demo})
                return
            }
        }
        let isTrading = symbol.isTrading

        if (isTrading === 0 || symbol.TradeMode === 0) {
            SetIsShowMessageDialog({ isShowDialog: true, title: t('common.alert'), message: t('watchlist.nonTradableContract') })
            return
        }
        
        navigation.navigate('NewOrder', { symbolName: contractCode, isBuy: isBuy })
        
    }, [stateLoginLevel, demoTradingAccount, symbol, isDemoAccount]);



    const onPressSearch = () => {
        console.debug("onPressSearch")
        let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
        console.debug("onPressSearch, ", searchCategories)
        navigation.push('SearchProductView', { filter: undefined, categories: searchCategories })
    };

    return (
        <SafeAreaView
            edges={['left', 'right', 'bottom']}
            style={[styles.safeArea, { backgroundColor: colors.WrapperBg }]}>
            <StatusBar hidden={orientation === PORTRAIT ? false : true} />
            {
                orientation === PORTRAIT && height > width ? 
                    <PortraitComponent 
                        contractCode={contractCode} 
                        defaultTab={tempDefaultTab} 
                        defaultPositionTab={defaultPositionTab} 
                        onPressPriceQuote={onPressPriceQuote} />
                    : 
                    (orientation === OrientationType["LANDSCAPE-LEFT"] || orientation === OrientationType["LANDSCAPE-RIGHT"]) && width > height?
                        <LandscapeComponent contractCode={contractCode} onPressPriceQuote={onPressPriceQuote}/>
                        :
                        <View></View>
            }
        </SafeAreaView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    safeArea: {
        justifyContent: 'space-between',
        flex: 1,
    },
    contractInfoStackView: {
        marginTop: 10,
        paddingLeft: 16,
        paddingRight: 11.5,
    },
    productName: {
        color: colors.MainFont,
    },
    contractInfoContainer: {
        height: 33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    contractInfoQuoteContainer: {
        alignItems: 'center',
    },
    symbolName: {
        color: colors.Brand2,
    },
    contractInfoAction: {
        height: 24,
        width: 24,
    },
    priceQuoteStackView: {
        height: 70,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    priceQuoteBox: {
        flex: 1,
        height: 50,
        borderRadius: 12,
        paddingHorizontal: 5,
        backgroundColor: colors.MainBg,
    },
    priceQuoteBoxLandscape: {
        height: 50,
        borderRadius: 12,
        paddingHorizontal: 10,
        marginBottom: 10
    },
    priceQuoteTitleContainer: {
        justifyContent: 'center',
        marginTop: 4,
    },
    priceQuoteTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    priceQuoteContainer: {
        justifyContent: 'center',
        height: 28,
    },
    priceQuote: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default ContractDetailView;