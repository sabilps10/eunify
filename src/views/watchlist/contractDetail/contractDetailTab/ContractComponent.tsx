import React, { Fragment, useEffect, useMemo, useState } from "react"
import { useTranslation } from "react-i18next"
import { ColorValue, StyleSheet, Text, useColorScheme, View } from "react-native"
import { useTheme } from '@react-navigation/native'
import StackView from '../../../../components/stackView.component'
import commonStyles, { makeGlobalStyles } from '../../../../styles/styles'
import ExpandableView from '../../../../components/expandableView.component'
import { THEME_TYPE } from "../../../../types/types"
import { useDispatch, useSelector } from "react-redux"
import { State } from "../../../../redux/root-reducer"
import DashedLineView from "../../../../components/dashedLineView.component"
import moment from "moment"
import { store } from "../../../../redux/store"
import { displayContractCode, productName } from "../../../utils/CommonFunction"
import { isEmpty } from "../../../../utils/stringUtils"

interface ComponentInterface {
    contract: string
}

const ContractComponent = ({ contract }: ComponentInterface) => {

    const [t, i18n] = useTranslation()
    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract])
    const tradingHours = useSelector((state: State) => (state.trading.symbols && state.trading.symbols[contract] && state.trading.symbols[contract].TradingSessions) ?? [])
    const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);
    const percentage = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract] && state.trading.symbols[contract].Percentage)
    const marginMode = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract] && state.trading.symbols[contract].MarginMode)

    const [marginRequirement, setMarginRequirement] = useState<string>('-')
    const [contractInfos, setContractInfos] = useState<{ title: string, value: string }[] | null>(null)

    useEffect(() => {
        // console.log('ContractComponent, accountLeverage:' + accountLeverage)
        // console.log('ContractComponent, percentage:' + percentage)
        // console.log('ContractComponent, marginMode:' + marginMode)

        if (!accountLeverage || !percentage) {
            setMarginRequirement('-')
            return
        }

        if (marginMode === 0 || marginMode === 4)
        {
            setMarginRequirement((percentage * 100 / accountLeverage).toFixed(2)  + '%')
        }
        else
        {
            setMarginRequirement((percentage * 100).toFixed(2) + '%')
        }
        
    }, [accountLeverage, percentage, marginMode])

    useEffect(() => {
        if (!symbol) return

        var ticker = productName(displayContractCode(symbol.SymbolName))
        if (isEmpty(ticker)) {
            ticker = displayContractCode(symbol.SymbolName)
        }
        
        const currency = symbol.Currency
        const contractSize = symbol.ContractSize + ' units'
        const minLot = symbol.VolumeMin.toString()
        const maxLot = symbol.VolumeMax.toString()
        let swapUnit = "";
        if (symbol.SwapType === 0)
            swapUnit = "pts"
        else if (symbol.SwapType === 1)
            swapUnit = symbol.Currency
        else if (symbol.SwapType === 2)
            swapUnit = "%"
        else if (symbol.SwapType === 3)
            swapUnit = symbol.MarginCurrency

        const shortPositionOvernightInterest = symbol.SwapShort.toString() + swapUnit
        const longPositionOvernightInterest = symbol.SwapLong.toString() + swapUnit

        const contractInfos: { title: string, value: string }[] = [
            { title: t('contractDetail.overView.contract.product'), value: ticker },
            // { title: t('contractDetail.overView.contract.currency'), value: currency },
            { title: t('contractDetail.overView.contract.contractSize'), value: contractSize },
            { title: t('contractDetail.overView.contract.minLot'), value: minLot },
            { title: t('contractDetail.overView.contract.maxLot'), value: maxLot },
            { title: t('contractDetail.overView.contract.marginRequirement'), value: marginRequirement },
            { title: t('contractDetail.overView.contract.positionOvernightInterestShort'), value: shortPositionOvernightInterest },
            { title: t('contractDetail.overView.contract.positionOvernightInterestLong'), value: longPositionOvernightInterest },
        ]
        setContractInfos(contractInfos)
    }, [symbol, marginRequirement])


    const contractTradingHourComponent = () => {
        const tradingHoursOfWeek: string[] = tradingHours.flatMap((entityItem, entityIndex) => {
            if (entityItem.trade.length === 0) return 'NIL'

            return entityItem.trade.flatMap((item, index) => {
                return item.Open + ' - ' + item.Close
            }).join(', ')
        })
        if (tradingHoursOfWeek.length === 0) return null

        return (
            <View style={{ paddingLeft: 28, }}>
                {
                    tradingHoursOfWeek.map((item, index) => {
                        const week = moment.weekdaysShort(index)

                        return (
                            <View style={{ height: 21, flexDirection: 'row', }}>
                                <Text style={[globalStyles.Body_Text_B, { width: 60 }]}>{t('contractDetail.week.' + week)}</Text>
                                <Text style={[globalStyles.Body_Text_B]}>{item}</Text>
                            </View>
                        )
                    })
                }
            </View>
        )
    }

    const separatorComponent = () => {
        return <View style={[styles.separator, { backgroundColor: colors.DashedLine }]} />
    }

    const contractRemarkComponent = () => {
        return (
            <StackView spacing={4} direction={'column'}>
                <Text style={[globalStyles.Note_B, { color: colors.TabSelectedFont }]}>
                    {t('contractDetail.overView.contract.remark')}
                </Text>
                <Text style={globalStyles.Tiny_Note}>
                    {t('contractDetail.overView.contract.remark_desc')}
                </Text>
            </StackView>
        )
    }

    const contractItemComponent = (title: string, value: string) => {
        return (
            <StackView style={styles.contractItem} spacing={10}>
                <Text style={[globalStyles.Body_Text, styles.contractItemTitle]}>
                    {title}
                </Text>
                <DashedLineView />
                <Text style={[globalStyles.Body_Text_B, styles.contractItemTitle]}>
                    {value}
                </Text>
            </StackView>
        )
    }


    const expandComponent = (
        <StackView
            spacing={10}
            direction='column'>
            {
                contractInfos ?
                    contractInfos.map((item, index) => {
                        return contractItemComponent(item.title, item.value)
                    })
                :
                    null
            }
            <StackView
                spacing={10}
                style={styles.contractItem}>
                <Text style={[globalStyles.Body_Text, styles.contractItemTitle]}>
                    {t('contractDetail.overView.contract.tradingHour')}
                </Text>
                <DashedLineView />
            </StackView>
            <StackView
                spacing={10}
                direction={'column'}>
                {contractTradingHourComponent()}
            </StackView>
            {contractRemarkComponent()}
            <View style={{marginTop: 10}}/>
        </StackView>
    )

    return (
        <Fragment>
            <ExpandableView
                title={t('contractDetail.overView.contract.title')}
                expandComponent={expandComponent}
                expanded={false} />
            {separatorComponent()}
        </Fragment>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    separator: {
        backgroundColor: 'black',
        height: 1
    },
    contractItemContainer: {
        justifyContent: 'center',
    },
    contractItem: {
        height: 21,
        alignItems: 'center',
    },
    contractItemTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default ContractComponent