import { useNavigation, useTheme } from "@react-navigation/native"
import React, { useState } from "react"
import { Image, Pressable, StyleSheet, Text, useColorScheme, View } from "react-native"
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from "../../../../styles/styles"
import TabBarView from "../../../../components/tabBarView.component"
import OpenPosition from "../../../openPosition/openPosition"
import { useTranslation } from "react-i18next"
import { ScreenNavigationProp } from "../../../../routes/route"
import PendingOrder from "../../../pendingOrder/pendingOrder"
import { DetailsType, OrderActionType } from "../../../../utils/orderUtils"
import { TabDirectoryTab } from "../../../tabDirectory/tabDirectory"
import StackView from "../../../../components/stackView.component"
import { useDispatch, useSelector } from 'react-redux'

import { WebsocketUtils } from "../../../../utils/websocketUtils"
import { State } from "../../../../redux/root-reducer"
import { THEME_TYPE } from "../../../../types/types"
import { bindActionCreators } from "redux"
import { StateActionCreators } from "../../../../redux/state/actionCreator"
import ArrowLeft from "../../../../../assets/svg/ArrowLeft"
import ArrowRight from "../../../../../assets/svg/ArrowRight"
import { PortfolioTabType } from "../../../../redux/state/types"
import PressableComponent from "../../../utils/PressableComponent"

enum PositionType {
    OpenPositions = 0,
    PendingOrders = 1,
}

interface ComponentInterface {
    contractCode: string,
    defaultTab: number
}


const ContractDetailPositionTab = ({contractCode, defaultTab} : ComponentInterface) => {

    
    console.debug("PositionView: " + defaultTab + " " + contractCode);
    const navigation = useNavigation<ScreenNavigationProp<'ContractDetailView'>>();
    
    const scheme = useColorScheme()
    const { colors }  = useTheme()
    const styles = makeStyles(colors)

    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    
    const [t] = useTranslation()

    const [positionType, setPositionType] = useState(defaultTab)

    const { startDeletePendingOrder } = WebsocketUtils();

    const dispatch = useDispatch();
    const { SetIsShowOrderConfirmDialog, SetPortfolioCurrentTab } = bindActionCreators(StateActionCreators, dispatch);

    const onPressTabBarItem = (index: number) => {
        setPositionType(index)
    }

    const onPressPositionDetails = (Ticket: number) => {
        navigation.navigate('PositionDetails', {type: DetailsType.NEW_POSITION, ticketId: Ticket})
    }

    const onPressPositionClose = (Ticket: number) => {
        navigation.navigate('ClosePosition', {ticketId: Ticket})
    }

    const onPressPositionEdit = (Ticket: number) => {
        navigation.navigate('EditPosition', {ticketId: Ticket})
    }

    const onPressOrderDetails = (Ticket: number) => {
        navigation.navigate('PositionDetails', {type: DetailsType.PENDING_ORDER, ticketId: Ticket})
    }

    const onPressOrderClose = async (Ticket: number) => {
        if (!isEnableTradeConfirmationDialog) {
            var result = await startDeletePendingOrder(Ticket);
        }
        else {
            SetIsShowOrderConfirmDialog({
                isShowDialog:true,
                type:OrderActionType.CANCEL_ORDER,
                contractCode:'',
                isSelectBuy:false,
                volume:0,
                price:0,
                isPending:true,
                pendingOrderType:0,
                pendingOrderValidity:0,
                isProfit:false,
                stopLossPrice:0,
                limitProfitPrice:0,
                transactionId:Ticket
            })
        }
    }

    const onPressOrderEdit = (Ticket: number) => {
        console.debug(`[ContractDetailPositionTab onPressOrderEdit] will navigation.navigate NewOrder+UpdatePendingOrder ${Ticket}`)
        navigation.navigate('NewOrder+UpdatePendingOrder', {symbolName: ' ', isBuy: false, transactionId: Ticket})
    }

    const onPressViewAllPositions = () => {
        SetPortfolioCurrentTab(PortfolioTabType.Position)
        navigation.navigate("TabDirectory", {
            screen: "Tab_Portfolio",
            params: {defaultTab: PortfolioTabType.Position}
        })
    }

    const onPressViewAllOrders = () => {
        navigation.navigate("TabDirectory", {
            screen: "Tab_Portfolio",
            params: {defaultTab: PortfolioTabType.Order}
        })
    }

    return (
        <View style={styles.container}>
            <TabBarView 
                style={styles.tabBarView}
                labelTitles={[t('contractDetail.position.tab.openPositions'), t('contractDetail.position.tab.pendingOrders')]}
                labelColor={colors.MainMenuSelectedFont}
                onPressTabBarItem={onPressTabBarItem}
                selectedIndex={PositionType.OpenPositions === positionType ? 0 : 1} />
            {
                PositionType.OpenPositions === positionType ?
                    <OpenPosition 
                        isInCollapsibleTab={false}
                        isDetailsPage={true}
                        symbol={[contractCode]}
                        onPressDetails={onPressPositionDetails}
                        onPressClose={onPressPositionClose}
                        onPressEdit={onPressPositionEdit} />
                :
                    <PendingOrder 
                        isInCollapsibleTab={false}
                        isDetailsPage={true}
                        symbol={[contractCode]}
                        onPressDetails={onPressOrderDetails}
                        onPressClose={onPressOrderClose}
                        onPressEdit={onPressOrderEdit} />
            }
            <StackView style={styles.shortcut}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 20}}>
                    <PressableComponent 
                        onPress={() => navigation.navigate('TabDirectory', {screen: TabDirectoryTab.QUOTE})}
                        style={styles.shortcutButton}>
                        <StackView 
                            spacing={8} 
                            style={{alignItems: 'center'}}>
                            <ArrowLeft color={colors.Brand3} />
                            <Text style={styles.shortcutText}>
                                {t('contractDetail.position.watchlist')}
                            </Text>
                        </StackView>
                    </PressableComponent>
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end', paddingRight: 20}}>
                    <PressableComponent 
                        onPress={onPressViewAllPositions}
                        style={styles.shortcutButton}>
                        <StackView spacing={8} style={{alignItems: 'center'}}>
                            <Text style={styles.shortcutText}>
                                { t('contractDetail.position.viewAllPositions') }
                            </Text>
                            <ArrowRight color={colors.Brand3} />
                        </StackView>
                    </PressableComponent>
                </View>                   
            </StackView>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 8,
        backgroundColor: colors.WrapperBg,
    },
    tabBarView: {
        height: 68,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: colors.MainBg,
    },
    shortcut: {
        height: 88,
        backgroundColor: colors.MainBg,
    },
    shortcutButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    shortcutText: {
        color: colors.MainFont,
    }
})
export default React.memo(ContractDetailPositionTab)