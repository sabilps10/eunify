import { useTheme } from '@react-navigation/native'
import React, { Fragment, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { ColorValue, ScrollView, StyleSheet, Text, useColorScheme, View } from "react-native"
import { Props } from '../../../../routes/route'
import commonStyles, { makeGlobalStyles } from '../../../../styles/styles'
import ChartComponent, { IntervalType } from '../../../../components/chart/chart.component'
import { OrientationType } from 'react-native-orientation-locker'
import { THEME_TYPE } from '../../../../types/types'
import LiveStatisticsComponent from './LiveStatisticsComponent'
import TraderSentimentComponent from './TraderSentimentComponent'
import ContractComponent from './ContractComponent'


interface ComponentInterface {
    contractCode: string
}

const ContractDetailOverviewTab = ({contractCode} : ComponentInterface) => {
    console.debug("ContractDetailOverviewComponent: " + contractCode)
    
    const [t, i18n] = useTranslation()
    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    return (
        <View style={styles.container}>
            <ScrollView
                overScrollMode='never'
                removeClippedSubviews={true}
                contentContainerStyle={{ flexGrow: 1 }}
                showsVerticalScrollIndicator={false}
                style={[styles.scrollView]}>
                <View style={{ justifyContent: 'center', height: 400, paddingBottom: 5 }} >
                    <ChartComponent contract={contractCode} interval={IntervalType.ONE_DAY} orientation={OrientationType.PORTRAIT} />
                </View>
                <View style={commonStyles.contentContainer}>
                    <TraderSentimentComponent contract={contractCode}/>
                    <LiveStatisticsComponent contract={contractCode} />
                    <ContractComponent contract={contractCode} />
                </View>
                <View style={{ height: 105 }} />
            </ScrollView>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 8,
        backgroundColor: colors.WrapperBg,
    },
    scrollView: {
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: colors.MainBg,
    },
})

export default React.memo(ContractDetailOverviewTab)