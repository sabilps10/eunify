import React, { useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { ColorValue, StyleSheet, Text, useColorScheme, View } from "react-native"
import { useTheme } from '@react-navigation/native'
import StackView from '../../../../components/stackView.component'
import commonStyles, { makeGlobalStyles } from '../../../../styles/styles'
import ExpandableView from '../../../../components/expandableView.component'
import { THEME_TYPE } from "../../../../types/types"
import { useDispatch, useSelector } from "react-redux"
import { State } from "../../../../redux/root-reducer"
import { LiveStatContent, PriceData } from "../../../../redux/trading/type"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { bindActionCreators } from "redux"
import { StateActionCreators } from "../../../../redux/state/actionCreator"
import Info from "../../../../../assets/svg/Info"

interface ComponentInterface {
    contract: string
}

const LiveStatisticsComponent = ({contract}: ComponentInterface) => {

    const [t, i18n] = useTranslation()
    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contract])
    const liveStatState = useSelector((state: State) => state.trading.liveStat && state.trading.liveStat[contract])

    const [fiveMinPercentage, setFiveMinPercentage] = useState<string>('0.00');
    const [fiveMinPercentageColor, setFiveMinPercentageColor] = useState<ColorValue>(colors.MainFont);
    const [fiveMinHigh, setFiveMinHigh] = useState<string>('0.00');
    const [fiveMinLow, setFiveMinLow] = useState<string>('0.00');

    const [sixtyMinPercentage, setSixtyMinPercentage] = useState<string>('0.00');
    const [sixtyMinPercentageColor, setSixtyMinPercentageColor] = useState<ColorValue>(colors.MainFont);
    const [sixtyMinHigh, setSixtyMinHigh] = useState<string>('0.00');
    const [sixtyMinLow, setSixtyMinLow] = useState<string>('0.00');

    const [oneDayPercentage, setOneDayPercentage] = useState<string>('0.00');
    const [oneDayPercentageColor, setOneDayPercentageColor] = useState<ColorValue>(colors.MainFont);
    const [oneDayHigh, setOneDayHigh] = useState<string>('0.00');
    const [oneDayLow, setOneDayLow] = useState<string>('0.00');
    
    const [liveStat, setLiveStat] = useState<LiveStatContent[]>(liveStatState);
    const [updateLiveStat, setUpdateLiveStat] = useState<boolean>(false);
    
    const priceQuote = useSelector((state: State) => state.trading.prices && state.trading.prices[contract])

    const dispatch = useDispatch();
    const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

    useEffect(() => {
        if (liveStatState) {
            setLiveStat(liveStatState)
        }
    }, [liveStatState])

    useEffect(() => {
        if (liveStat && symbol) {
            for (var i = 0; i < liveStat.length; i++) {
                var stat = liveStat[i];

                var priceData = stat.PriceData;
                var priceSize = priceData.length;

                if (priceSize === 0 )
                    return;
                var perc = (priceData[priceSize - 1].Close - priceData[0].Open) / priceData[0].Open * 100;
                var percValue = Number(perc.toFixed(2));
                var sign = percValue >= 0 ? '+' : ''
                var color = percValue === 0 ? colors.Text : (percValue >= 0 ? colors.Green : colors.Red)

                var high = Object.keys(priceData).map((key) => priceData[key].High);
                var low = Object.keys(priceData).map((key) => priceData[key].Low);

                if (stat.Type === 0) {
                    setFiveMinPercentageColor(color)
                    setFiveMinPercentage(sign + percValue.toFixed(2))
                    setFiveMinHigh(Math.max(...high).toFixed(symbol.Digits))
                    setFiveMinLow(Math.min(...low).toFixed(symbol.Digits))
                } else if (stat.Type === 1) {
                    setSixtyMinPercentageColor(color)
                    setSixtyMinPercentage(sign + percValue.toFixed(2))
                    setSixtyMinHigh(Math.max(...high).toFixed(symbol.Digits))
                    setSixtyMinLow(Math.min(...low).toFixed(symbol.Digits))
                } else if (stat.Type === 2) {
                    setOneDayPercentageColor(color)
                    setOneDayPercentage(sign + percValue.toFixed(2))
                    setOneDayHigh(Math.max(...high).toFixed(symbol.Digits))
                    setOneDayLow(Math.min(...low).toFixed(symbol.Digits))
                }
            }
        }
    }, [liveStat, updateLiveStat])

    useEffect(() => {
        if (priceQuote && liveStat) {
            var tempLiveStat = liveStat;
            for (var i = 0; i < tempLiveStat.length; i++) {
                var stat = tempLiveStat[i];
                var priceData: PriceData[] = stat.PriceData;
                var priceSize = priceData.length;

                var tempPriceData: PriceData = { ...priceData[priceSize - 1] };

                tempPriceData.Close = priceQuote.Bid;
                if (tempPriceData.High < priceQuote.Bid) {
                    tempPriceData.High = priceQuote.Bid;
                }

                if (tempPriceData.Low > priceQuote.Bid) {
                    tempPriceData.Low = priceQuote.Bid;
                }

                var isNewTick = false;

                var date = Date;
                var currDate = new Date(date.now());

                var lastDate = new Date(tempPriceData.Time * 1000);

                if (stat.Type === 0 || stat.Type === 1) {
                    if (currDate.getMinutes() !== lastDate.getMinutes() - 1 && currDate.getHours() === lastDate.getHours() && currDate.getDate()) {
                        isNewTick = true;
                    }
                } else {
                    if (currDate.getMinutes() >= lastDate.getMinutes() && currDate.getHours() === lastDate.getHours() && currDate.getDate()) {
                        isNewTick = true;
                    }
                }

                if (isNewTick) {
                    var timeDiff = 1;
                    if (stat.Type === 2) {
                        timeDiff = 5;
                    }
                    tempPriceData.Open = tempPriceData.Close;
                    tempPriceData.Time = ~~((date.now() + timeDiff * 60000) / 1000);
                    priceData.shift();
                    priceData.push(tempPriceData);
                } else {
                    priceData[priceSize - 1] = tempPriceData;
                }
            }
            setLiveStat(tempLiveStat)
        }
        setUpdateLiveStat(!updateLiveStat)
    }, [priceQuote])

    const onPressLiveStatisticsDetail = () => {
        SetIsShowMessageDialog({alternateStyle:true, isShowDialog: true, title: t('contractDetail.dialog.statistics.title'), message: t('contractDetail.dialog.statistics.message')})
    }

    const expandComponent = (
        <StackView
            style={{ marginTop: 20, marginBottom: 27 }}
            direction='column'
            spacing={8} >
            <StackView
                spacing={10}
                style={{ flex: 1 }}>
                <StackView
                    style={styles.liveStatisticsMinsStackView}>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsTitle]}>
                        {t('contractDetail.overView.liveStatistics.5min')}
                    </Text>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsValue, { color: fiveMinPercentageColor }]}>
                        {fiveMinPercentage}%
                    </Text>
                </StackView>
                <StackView
                    style={[styles.liveStatisticsHighLowStackView]}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.High')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {fiveMinHigh}
                    </Text>
                </StackView>
                <StackView
                    style={styles.liveStatisticsHighLowStackView}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.Low')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {fiveMinLow}
                    </Text>
                </StackView>
            </StackView>
            <StackView
                spacing={10}
                style={{ flex: 1 }}>
                <StackView style={styles.liveStatisticsMinsStackView}>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsTitle]}>
                        {t('contractDetail.overView.liveStatistics.60min')}
                    </Text>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsValue, { color: sixtyMinPercentageColor }]}>
                        {sixtyMinPercentage}%
                    </Text>
                </StackView>
                <StackView
                    style={styles.liveStatisticsHighLowStackView}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.High')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {sixtyMinHigh}
                    </Text>
                </StackView>
                <StackView
                    style={styles.liveStatisticsHighLowStackView}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.Low')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {sixtyMinLow}
                    </Text>
                </StackView>
            </StackView>
            <StackView
                spacing={10}
                style={{ flex: 1 }}>
                <StackView style={styles.liveStatisticsMinsStackView}>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsTitle]}>
                        {t('contractDetail.overView.liveStatistics.1day')}
                    </Text>
                    <Text style={[globalStyles.Note_B, styles.liveStatisticsMinsValue, { color: oneDayPercentageColor }]}>
                        {oneDayPercentage}%
                    </Text>
                </StackView>
                <StackView style={styles.liveStatisticsHighLowStackView}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.High')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {oneDayHigh}
                    </Text>
                </StackView>
                <StackView style={styles.liveStatisticsHighLowStackView}>
                    <Text style={[globalStyles.Small_Note]}>
                        {t('contractDetail.overView.liveStatistics.Low')}
                    </Text>
                    <Text style={[globalStyles.Note_B]}>
                        {oneDayLow}
                    </Text>
                </StackView>
            </StackView>
        </StackView>
    )
    return (
        <ExpandableView
            title={t('contractDetail.overView.liveStatistics.title')}
            expandComponent={expandComponent}
            detailImage={<Info color={colors.MainFont} />}
            onPressDetail={onPressLiveStatisticsDetail} />
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    liveStatisticsMinsStackView: {
        flex: 0.45,
        justifyContent: 'space-between',
    },
    liveStatisticsMinsTitle: {
        flex: 0.6,
    },
    liveStatisticsMinsValue: {
        flex: 0.4,
        textAlign: 'left',
    },
    liveStatisticsHighLowStackView: {
        flex: 0.275,
        justifyContent: 'space-between',
    },
    liveStatisticsHighLowTitle: {
        // flex: 0.3,
        justifyContent: 'space-between',
    },
    liveStatisticsHighLowValue: {
        // flex: 0.3,
        justifyContent: 'space-between',
    },
})

export default LiveStatisticsComponent