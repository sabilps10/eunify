import React, { useEffect, useMemo, useState } from "react"
import { useTranslation } from "react-i18next"
import { ColorValue, StyleSheet, Text, useColorScheme, View } from "react-native"
import { useTheme } from '@react-navigation/native'
import StackView from '../../../../components/stackView.component'
import commonStyles, { makeGlobalStyles } from '../../../../styles/styles'
import ExpandableView from '../../../../components/expandableView.component'
import { THEME_TYPE } from "../../../../types/types"
import { useDispatch, useSelector } from "react-redux"
import { State } from "../../../../redux/root-reducer"
import { LiveStatContent, PriceData } from "../../../../redux/trading/type"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { bindActionCreators } from "redux"
import { StateActionCreators } from "../../../../redux/state/actionCreator"
import Info from "../../../../../assets/svg/Info"

interface ComponentInterface {
    contract: string
}

const TraderSentimentComponent = ({contract}: ComponentInterface) => {

    const [t, i18n] = useTranslation()
    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    const tradeSentiment = useSelector((state: State) => state.trading.tradeSentiment && state.trading.tradeSentiment[contract])

    const [sellerPercentage, setSellerPercentage] = useState<number>(50)
    const [buyerPercentage, setBuyerPercentage] = useState<number>(50)

    const dispatch = useDispatch();
    const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    
    useEffect(() => {
        console.debug("tradeSentiment");
        if (tradeSentiment) {
            setSellerPercentage(tradeSentiment.SellPercentage)
            setBuyerPercentage(tradeSentiment.BuyPercentage)
        }
    }, [tradeSentiment])

    const onPressTraderSentimentDetail = () => {
        SetIsShowMessageDialog({alternateStyle:true, isShowDialog: true, title: t('contractDetail.dialog.sentiment.title'), message: t('contractDetail.dialog.sentiment.message')})
    }

    const traderSentimentSellerComponent = useMemo(() => {
        return (
            <Text style={[globalStyles.Note_B, styles.traderSentimentSeller]}>
                {sellerPercentage}% {t('contractDetail.overView.traderSentiment.sellers')}
            </Text>
        )
    }, [sellerPercentage])

    const traderSentimentBuyerComponent = useMemo(() => {
        return (
            <Text style={[globalStyles.Note_B, styles.traderSentimentBuyer]}>
                {buyerPercentage}% {t('contractDetail.overView.traderSentiment.buyers')}
            </Text>
        )
    }, [buyerPercentage])

    const traderSentimentSellerPercentageComponent = useMemo(() => {
        return <View style={[styles.traderSentimentSellerPercentage, { width: `${sellerPercentage}%` }]} />
    }, [sellerPercentage])

    const expandComponent = (
        <StackView
            direction='column' spacing={2}
            style={{ paddingTop: 21, paddingBottom: 35 }}>
            <StackView
                style={{ alignItems: 'center' }}
                spacing={10}>
                {traderSentimentSellerComponent}
                {traderSentimentBuyerComponent}
            </StackView>
            <View style={styles.traderSentimentBuyerPercentage}>
                {traderSentimentSellerPercentageComponent}
            </View>
        </StackView>
    )
    return (
        <ExpandableView
            title={t('contractDetail.overView.traderSentiment.title')}
            expandComponent={expandComponent}
            detailImage={<Info color={colors.MainFont} />}
            onPressDetail={onPressTraderSentimentDetail} />
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    traderSentimentSeller: {
        flex: 1,
        textAlign: 'left',
        color: colors.Down,
    },
    traderSentimentSellerPercentage: {
        backgroundColor: colors.Down,
        borderRadius: 50,
        flex: 1,
    },
    traderSentimentBuyer: {
        flex: 1,
        textAlign: 'right',
        color: colors.Up
    },
    traderSentimentBuyerPercentage: {
        backgroundColor: colors.Up,
        borderRadius: 50,
        height: 8,
    },
})

export default TraderSentimentComponent