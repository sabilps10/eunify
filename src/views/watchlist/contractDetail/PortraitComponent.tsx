import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, Image, Pressable, StyleSheet, Text, TextStyle, useColorScheme, View } from 'react-native';
import { useFocusEffect, useIsFocused, useNavigation, useTheme } from '@react-navigation/native';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../../styles/styles';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../redux/root-reducer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { THEME_TYPE } from '../../../types/types';
import BidPriceComponent from '../BidPriceComponent';
import StackView from '../../../components/stackView.component';
import { displayContractCode, productName } from '../../utils/CommonFunction';
import PriceChangeComponent from '../PriceChangeComponent';
import AskPriceComponent from '../AskPriceComponent';
import { useTranslation } from 'react-i18next';
import ContractDetailOverviewTab from './contractDetailTab/ContractDetailOverviewTab';
import ContractDetailPositionTab from './contractDetailTab/ContractDetailPositionTab';
import FavAdded from '../../../../assets/svg/FavAdded';
import FavAdd from '../../../../assets/svg/FavAdd';
import { addWatchList, removeWatchList } from '../../../utils/api/tradingApi';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../../redux/state/actionCreator';
import { PermissionAccessType } from '../../permission/permissionAccess';
import { ScreenNavigationProp } from '../../../routes/route';
import PressableComponent from '../../utils/PressableComponent';

interface ComponentInterface {
    contractCode: string,
    defaultTab: number,
    defaultPositionTab: number,
    onPressPriceQuote: (isBuy: boolean) => void;
}

const PortraitComponent = ({ contractCode, defaultTab, defaultPositionTab, onPressPriceQuote }: ComponentInterface) => {

    console.debug("PortraitComponent: " + contractCode);
    const [t] = useTranslation();
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const contractName = productName(displayContractCode(contractCode))

    const navigation = useNavigation<ScreenNavigationProp<'ContractDetailView'>>();

    const Tab = createMaterialTopTabNavigator();

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const watchlist = useSelector((state: State) => state.state.myList ?? [])
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1)
    const demoTradingAccount = useSelector((state: State) => state.account.demoTradingAccount);

    const accessToken = useSelector((state: State) => state.account.account?.AccessToken!);
    const positions = useSelector((state: State) => state.trading.positions)
    // const onTouch = useSelector((state: State) => state.state.onTouch)

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowMessageDialog, SetIsShowToastDialog, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    const { SetMyList } = bindActionCreators(StateActionCreators, dispatch)

    const [tempDefaultTab, setTempDefaultTab] = useState<number>(defaultTab);

    useFocusEffect(
        React.useCallback(() => {
            if (tempDefaultTab === 0) {
                navigation.navigate('Tab_ContractDetailOverview')
            } else if (tempDefaultTab === 1) {
                navigation.navigate('Tab_ContractDetailPosition')
            }
        }, [tempDefaultTab])
    );

    const watchlistImage = useMemo(() => {
        let isAddedWatchlist = watchlist.find(obj => obj === displayContractCode(contractCode))
        if (isAddedWatchlist)
            return <FavAdded color={colors.Brand3} />
        else
            return <FavAdd color={colors.Brand3} />
    }, [watchlist, contractCode])



    const onPressWatchlist = async () => {

        try
        {
            console.debug('contractCode:' + contractCode)
            //SetIsShowLoading(true)
            const contract = displayContractCode(contractCode)
            const isLoggedin = !(stateLoginLevel === 1)
            const isAddedWatchlist = watchlist.find(obj => obj === contract)
    
            var success = true
    
            // If Loggedin, call WS update.
            if (isLoggedin && accessToken) {
                // If is Remove Symbol from Watchlist
                if (isAddedWatchlist && positions) {
                    var validate = Object.values(positions).filter(position => displayContractCode(position.Symbol) === contract).length === 0
    
                    if (!validate) {
                        SetIsShowToastDialog({
                            isShowDialog: true,
                            message: t('editWatchlist.unableRemove'),
                            isSuccess: false,
                        })
                        return
                    }
                }
                success = isAddedWatchlist ? await removeWatchList(accessToken, contract) : await addWatchList(accessToken, contract)
            }
            
            if (!success) {
                console.debug(isAddedWatchlist ? 'Add' : 'Remove' + ' Watchlist ' + contract + ' fail.')
                return
            }
    
            if (isAddedWatchlist) {
                SetIsShowToastDialog({
                    isShowDialog: true,
                    message: t('editWatchlist.removeSuccess'),
                    isSuccess: true
                })
                watchlist.splice(watchlist.indexOf(contract), 1)
            }
            else {
                SetIsShowToastDialog({
                    isShowDialog: true,
                    message: t('editWatchlist.addSuccess'),
                    isSuccess: true
                })
                watchlist.push(contract)
            }

            console.debug("watchlist: " + watchlist)

            SetMyList([...new Set(watchlist)])
        }
        finally
        {
            //SetIsShowLoading(false)
        }
    }


    return (
        <>
            <StackView direction='column' style={styles.contractInfoStackView}>
                <Text style={[globalStyles.Note, styles.productName]}>
                    {contractName}
                </Text>
                <View style={styles.contractInfoContainer}>
                    <StackView spacing={10} style={styles.contractInfoQuoteContainer}>
                        <Text style={[globalStyles.H3, styles.symbolName]}>
                            {displayContractCode(contractCode)}
                        </Text>
                        <PriceChangeComponent contract={contractCode} />
                    </StackView>
                    <StackView spacing={5}>
                        {/* <IconAlertAdd color={colors.Brand3} /> */}
                        <PressableComponent
                            onPress={onPressWatchlist}
                            style={styles.contractInfoAction}>
                            {watchlistImage}
                        </PressableComponent>
                    </StackView> 
                </View>
            </StackView>
            <Tab.Navigator
                screenOptions={{
                    swipeEnabled: false,
                    tabBarStyle: {
                        backgroundColor: colors.WrapperBg,
                        marginHorizontal: 16,
                        elevation: 0
                    },
                    tabBarIndicatorStyle: {
                        backgroundColor: colors.TabSelectedFont,
                        height: 2,
                    },
                }}
                initialRouteName={tempDefaultTab === 1 ? "Tab_ContractDetailPosition" : "Tab_ContractDetailOverview"}>
                <Tab.Screen
                    name="Tab_ContractDetailOverview"
                    children={() => <ContractDetailOverviewTab contractCode={contractCode} />}
                    options={{
                        tabBarLabel: (props => <Text style={[{ fontFamily: 'Montserrat', fontStyle: 'normal', fontWeight: '600', fontSize: 13, textAlign: 'center', textAlignVertical: 'center' }, { color: props.focused ? colors.TabSelectedFont : colors.TabFont }]}>{t('contractDetail.tab.overView')}</Text>)
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                        },
                    }}/>
                <Tab.Screen
                    name="Tab_ContractDetailPosition"
                    children={() => <ContractDetailPositionTab contractCode={contractCode} defaultTab={defaultPositionTab} />}
                    options={{
                        tabBarLabel: (props => <Text style={[{ fontFamily: 'Montserrat', fontStyle: 'normal', fontWeight: '600', fontSize: 13, textAlign: 'center', textAlignVertical: 'center' }, { color: props.focused ? colors.TabSelectedFont : colors.TabFont }]}>{t('contractDetail.tab.position')}</Text>)
                    }}
                    listeners={{
                        tabPress: e => {
                            SetOnTouch()
                            if (stateLoginLevel === 1) {
                                e.preventDefault();
                                navigation.navigate('PermissionAccess', { type: PermissionAccessType.member_only })
                            } else if (stateLoginLevel === 2) {
                                if (!demoTradingAccount) {
                                    e.preventDefault();
                                    navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_no_demo})
                                } else if (!isDemoAccount) {
                                    e.preventDefault();
                                    navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_has_demo})
                                }
                            }
                        },
                    }}

                />
            </Tab.Navigator>
            <StackView
                spacing={20}
                style={styles.priceQuoteStackView}>
                <PressableComponent
                    style={styles.priceQuoteBox}
                    onPress={() => { onPressPriceQuote(false) }}>
                    <View style={styles.priceQuoteTitleContainer}>
                        <Text style={[globalStyles.Big_Text, styles.priceQuoteTitle]}>
                            {t('watchlist.SELL')}
                        </Text>
                    </View>
                    <View style={styles.priceQuoteContainer}>
                        <BidPriceComponent contract={contractCode} />
                    </View>
                </PressableComponent>
                <PressableComponent
                    style={styles.priceQuoteBox}
                    onPress={() => { onPressPriceQuote(true) }}>
                    <View style={styles.priceQuoteTitleContainer}>
                        <Text style={[globalStyles.Big_Text, styles.priceQuoteTitle]}>
                            {t('watchlist.BUY')}
                        </Text>
                    </View>
                    <View style={styles.priceQuoteContainer}>
                        <AskPriceComponent contract={contractCode} />
                    </View>
                </PressableComponent>
            </StackView>
            <View style={{padding: 3}}/>
        </>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    safeArea: {
        justifyContent: 'space-between',
        flex: 1,
    },
    contractInfoStackView: {
        marginTop: 10,
        paddingLeft: 16,
        paddingRight: 11.5,
    },
    productName: {
        color: colors.MainFont,
    },
    contractInfoContainer: {
        height: 33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    contractInfoQuoteContainer: {
        alignItems: 'center',
    },
    symbolName: {
        color: colors.Brand2,
    },
    contractInfoAction: {
        height: 24,
        width: 24,
    },
    priceQuoteStackView: {
        height: 70,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    priceQuoteBox: {
        flex: 1,
        height: 60,
        borderRadius: 12,
        paddingHorizontal: 5,
        backgroundColor: colors.MainBg,
    },
    priceQuoteTitleContainer: {
        justifyContent: 'center',
        marginTop: 4,
    },
    priceQuoteTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    priceQuoteContainer: {
        justifyContent: 'center',
        height: 28,
    },
    priceQuote: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default React.memo(PortraitComponent);