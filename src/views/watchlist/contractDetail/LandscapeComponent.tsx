import React, { useEffect, useLayoutEffect, useState, useCallback, useMemo, useContext, Fragment } from 'react';
import { Alert, Image, Pressable, StyleSheet, Text, TextStyle, useColorScheme, View } from 'react-native';
import { useFocusEffect, useIsFocused, useNavigation, useTheme } from '@react-navigation/native';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../../styles/styles';
import { useSelector } from 'react-redux';
import { State } from '../../../redux/root-reducer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { THEME_TYPE } from '../../../types/types';
import BidPriceComponent from '../BidPriceComponent';
import StackView from '../../../components/stackView.component';
import { displayContractCode, productName } from '../../utils/CommonFunction';
import PriceChangeComponent from '../PriceChangeComponent';
import AskPriceComponent from '../AskPriceComponent';
import OverView from './contractDetailTab/ContractDetailOverviewTab';
import PositionView from './contractDetailTab/ContractDetailPositionTab';
import { useTranslation } from 'react-i18next';
import ContractDetailOverviewComponent from './contractDetailTab/ContractDetailOverviewTab';
import ContractDetailPositionView from './contractDetailTab/ContractDetailPositionTab';
import { OrientationType, PORTRAIT } from 'react-native-orientation-locker';
import ChartComponent, { IntervalType } from '../../../components/chart/chart.component';
import { ScreenNavigationProp } from '../../../routes/route';
import PressableComponent from '../../utils/PressableComponent';

interface ComponentInterface {
    contractCode: string,
    onPressPriceQuote: (isBuy: boolean) => void;
}

const LandscapeComponent = ({ contractCode, onPressPriceQuote }: ComponentInterface) => {

    console.debug("LandscapeComponent: " + contractCode);
    const [t] = useTranslation();
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const contractName = productName(displayContractCode(contractCode))

    return (
        <View style={{ flexDirection: 'row', flex: 1 }}>
            <View style={{ flex: 1, marginHorizontal: LAYOUT_PADDING_HORIZONTAL, marginVertical: 10 }} >
                <View style={{ flex: 1 }}>
                    <Text style={[globalStyles.Note, { color: colors.MainFont }]}>
                        {contractName}
                    </Text>
                    <View style={styles.contractInfoContainer}>
                        <StackView spacing={10} style={styles.contractInfoQuoteContainer}>
                            <Text style={[globalStyles.H3, { color: colors.Brand2 }]}>
                                {displayContractCode(contractCode)}
                            </Text>

                        </StackView>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <PriceChangeComponent contract={contractCode} />
                    </View>

                </View>
                <View>
                    <PressableComponent
                        style={[styles.priceQuoteBoxLandscape, { backgroundColor: colors.MainBg }]}
                        onPress={() => { onPressPriceQuote(false) }}>
                        <View style={styles.priceQuoteTitleContainer}>
                            <Text style={[globalStyles.Big_Text, styles.priceQuoteTitle]}>
                                {t('watchlist.SELL')}
                            </Text>
                        </View>
                        <View style={styles.priceQuoteContainer}>
                            <BidPriceComponent contract={contractCode} />
                        </View>
                    </PressableComponent>
                    <PressableComponent
                        style={[styles.priceQuoteBoxLandscape, { backgroundColor: colors.MainBg, paddingHorizontal: 5 }]}
                        onPress={() => { onPressPriceQuote(true) }}>
                        <View style={styles.priceQuoteTitleContainer}>
                            <Text style={[globalStyles.Big_Text, styles.priceQuoteTitle]}>
                                {t('watchlist.BUY')}
                            </Text>
                        </View>
                        <View style={styles.priceQuoteContainer}>
                            <AskPriceComponent contract={contractCode} />
                        </View>
                    </PressableComponent>
                </View>
            </View>
            <View style={{ width: '70%', backgroundColor: colors.MainBg, borderRadius: 12 }}>
                <View style={{ borderRadius: 5, flex: 1, width: '98.5%', justifyContent: 'flex-end', alignSelf: 'flex-end' }}>
                    <ChartComponent contract={contractCode} interval={IntervalType.ONE_DAY} orientation={OrientationType['LANDSCAPE-LEFT']} />
                </View>
            </View>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    safeArea: {
        justifyContent: 'space-between',
        flex: 1,
    },
    contractInfoStackView: {
        marginTop: 10,
        paddingLeft: 16,
        paddingRight: 11.5,
    },
    productName: {
        color: colors.MainFont,
    },
    contractInfoContainer: {
        height: 33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    contractInfoQuoteContainer: {
        alignItems: 'center',
    },
    symbolName: {
        color: colors.Brand2,
    },
    contractInfoAction: {
        height: 24,
        width: 24,
    },
    priceQuoteStackView: {
        height: 70,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    priceQuoteBox: {
        flex: 1,
        height: 50,
        borderRadius: 12,
        paddingHorizontal: 5,
        backgroundColor: colors.MainBg,
    },
    priceQuoteBoxLandscape: {
        height: 55,
        borderRadius: 12,
        paddingHorizontal: 10,
        marginBottom: 10
    },
    priceQuoteTitleContainer: {
        justifyContent: 'center',
        marginTop: 4,
    },
    priceQuoteTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    priceQuoteContainer: {
        justifyContent: 'center',
        height: 28,
    },
    priceQuote: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default React.memo(LandscapeComponent);