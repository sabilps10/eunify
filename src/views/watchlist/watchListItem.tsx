import React, { useState, useContext, useCallback, useEffect } from 'react';
import {
	StyleSheet,
	Text,
	useColorScheme,
	View,
	Image,
	Pressable,
	ColorValue
} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles'

import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';
import { displayContractCode, productName } from '../utils/CommonFunction';
import StackView from '../../components/stackView.component'
import { THEME_TYPE } from '../../types/types';
import AskPriceComponent from './AskPriceComponent';
import BidPriceComponent from './BidPriceComponent';
import PriceChangeComponent from './PriceChangeComponent';
import ArrowRight from '../../../assets/svg/ArrowRight';
import PressableComponent from '../utils/PressableComponent';

interface WatchListItemParams {
	contractCode: string,
	onPressItem: (contractCode: string) => void,
	onPressPlaceOrder: (contractCode: string, isBuy: boolean, loginLevel: number, isNonTrading: boolean) => void
}

const WatchListItem = (params: WatchListItemParams) => {

	const [t] = useTranslation();

	const scheme = useColorScheme();
	const { colors } = useTheme();
	const globalStyles = makeGlobalStyles(colors)
	const styles = makeStyles(colors)	

	const watchlistPriceChange = useSelector((state: State) => state.selection.selected?.watchlistPriceChange ?? 'percentageChange')
	const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
	const symbol = useSelector((state: State) => state.trading.symbols && state.trading.symbols[params.contractCode])

	const contractName = productName(displayContractCode(params.contractCode))

	const toOrder = (isBuy: boolean) => {
		params.onPressPlaceOrder(params.contractCode, isBuy, stateLoginLevel, (symbol && (symbol.isTrading === 0 || symbol.TradeMode === 0)));
		//navigation.navigate('NewOrder', {symbolName: symbolName})
	}

	return (
		<StackView spacing={6} style={styles.stackView}>
			{
				console.debug("watchlistItem: " + params.contractCode)
			}
			<PressableComponent style={styles.contractInfo} onPress={() => params.onPressItem(params.contractCode)}>
				<View style={styles.contractNameContainer}>
					<Text style={[globalStyles.Tiny_Note, styles.contractName]}>{contractName}</Text>
				</View>
				<View style={styles.contractCodeContainer}>
					<Text style={[globalStyles.Big_Text_B, styles.contractCode]}>{displayContractCode(params.contractCode)}</Text>
				</View>
				<View style={styles.priceChangeContainer}>
					<PriceChangeComponent contract={params.contractCode} type={watchlistPriceChange} textStyle={globalStyles.Note}/>
				</View>
			</PressableComponent>
			<PressableComponent style={styles.priceQuoteBox} onPress={() => toOrder(false)}>
				<View style={styles.priceQuoteTitleContainer}>
					<Text style={[globalStyles.Tiny_Note, styles.priceQuoteTitle]}>{t('watchlist.SELL')}</Text>
				</View>
				<BidPriceComponent contract={params.contractCode} textStyle={globalStyles.Body_Text_B} />
			</PressableComponent>
			<PressableComponent style={[styles.priceQuoteBox]} onPress={() => toOrder(true)}>
				<View style={styles.priceQuoteTitleContainer}>
					<Text style={[globalStyles.Tiny_Note, styles.priceQuoteTitle]}>{t('watchlist.BUY')}</Text>
				</View>
				<AskPriceComponent contract={params.contractCode} textStyle={globalStyles.Body_Text_B}/>
			</PressableComponent>
			<PressableComponent style={styles.arrow} onPress={() => params.onPressItem(params.contractCode)}>
				<ArrowRight color={colors.Brand3} />
			</PressableComponent>
		</StackView>
	);
};

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
	container: {
		width: '100%',
		backgroundColor: '#FFFFFF',
		flexDirection: 'row',
	},
	stackView: {
		alignItems: 'center',
		// height: 53,
		marginHorizontal: 15,
	},
	contractInfo: {
		height: '100%',
		flexDirection: 'column',
		flex: 1,
	},
	contractNameContainer: {
		// height: 16,
		flex: 1,
		justifyContent: 'center',
	},
	contractName: {
		color: colors.MainFont,
		textAlign: 'left'
	},
	contractCodeContainer: {
		height: 22,
		flex: 1,
		justifyContent: 'center',
	},
	contractCode: {
		color: colors.Brand2,
		textAlign: 'left',
		flex: 1,
	},
	priceChangeContainer: {
		height: 19,
		flex: 1,
		justifyContent: 'center',
	},
	priceChange: {
		textAlign: 'left',
		flex: 1,
	},
	priceQuoteBox: {
		width: 92,
		height: 40,
		borderRadius: 5,
		backgroundColor: colors.PriceBoxBg,
		paddingHorizontal: 5
	},
	priceQuoteTitleContainer: {
		justifyContent: 'center',
		// height: 16,
		marginTop: 3,
	},
	priceQuoteTitle: {
		textAlign: 'center',
		textAlignVertical: 'center',
	},
	priceQuote: {
		textAlign: 'center',
		textAlignVertical: 'center',
		flex: 1,
		marginBottom: 3,
	},
	arrow: {
		height: 24, 
		width: 24, 
		justifyContent: 'center', 
		alignItems: 'center',
	},
});

export default React.memo(WatchListItem);