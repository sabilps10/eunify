import React, { useEffect, useLayoutEffect, useMemo, useState } from 'react'
import { Alert, Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native'
import Header from '../../../components/header/header.component';
import HeaderButton from '../../../components/header/headerButton.component';
import { Props } from '../../../routes/route';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../../styles/styles'
import { useTheme } from '@react-navigation/native';
import {
    getIcon_back_24
} from '../../../../assets/constants';
import { SafeAreaView } from 'react-native-safe-area-context';
import StackView from '../../../components/stackView.component'
import EditWatchlistItem from './editWatchlistItem';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../redux/root-reducer';
import DraggableFlatList, { RenderItemParams, ScaleDecorator } from 'react-native-draggable-flatlist'
import { WebsocketUtils } from '../../../utils/websocketUtils';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../../redux/state/actionCreator';
import { replaceWatchList } from '../../../utils/api/tradingApi';
import { displayContractCode } from '../../utils/CommonFunction';
import TopBarBack from '../../../../assets/svg/TopBarBack';
import { THEME_TYPE } from '../../../types/types';
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import PressableComponent from '../../utils/PressableComponent';


const EditWatchlistView: React.FC<Props<'EditWatchlistView'>> = ({ route, navigation }) => {

    console.debug("EditWatchlistView");
    const [t] = useTranslation()
    const insets = useSafeAreaInsets()

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const dispatch = useDispatch()
    const { SetMyList } = bindActionCreators(StateActionCreators, dispatch)
    const { SetIsShowLoading, SetIsShowToastDialog } = bindActionCreators(StateActionCreators, dispatch)

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1)
    const watchlist = useSelector((state: State) => state.state.myList ?? [])
    const positions = useSelector((state: State) => state.trading.positions)
    const [data, setData] = useState([...watchlist])
    const accessToken = useSelector((state: State) => state.account.account?.AccessToken);


    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header
                style={{ backgroundColor: colors.WrapperBg }}
                leftComponents={[
                    <HeaderButton
                        iconComponent={<TopBarBack color={colors.MainFont} />}
                        onPress={onPressBack}
                        title={t('editWatchlist.title')}
                        titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
                ]}
            />
        });
    }, [navigation]);

    useEffect(() => {
        console.debug('watchlist:' + watchlist)
    }, [watchlist])

    const onPressBack = () => { navigation.goBack() }

    const onPressDone = async () => {
        SetIsShowLoading(true)

        let isLoggedin = !(stateLoginLevel === 1)
        var success = true

        if (isLoggedin && accessToken) {
            success = await replaceWatchList(accessToken, JSON.stringify(data))
            console.debug('onPressDone, success:' + success ? 'Y' : 'N')
        }
        SetIsShowLoading(false)
        if (!success) {
            console.debug('replaceWatchList fail')
        }
        SetMyList(data)
        navigation.goBack()
    }

    const onPressPriceAlert = () => { console.debug('onPressPriceAlert') }

    const onPressRemove = (symbol: string) => {
        if (positions)
        {
            var validate = Object.values(positions).filter(position => displayContractCode(position.Symbol) === symbol).length === 0

            if (!validate) {
                // Alert.alert(t('contractDetail.unableRemove'))
                SetIsShowToastDialog({
                    isShowDialog: true,
                    message: t('editWatchlist.unableRemove'),
                    isSuccess: false
                })
                return
            }
        }

        /*SetIsShowToastDialog({
            isShowDialog: true,
            message: t('editWatchlist.removeSuccess'),
            isSuccess: false
        })*/
        data.splice(data.indexOf(symbol), 1)
        setData([...data])
    }

    const renderItem = ({ item, drag }: RenderItemParams<string>) => {
        return (
            <ScaleDecorator>
                <EditWatchlistItem
                    symbol={item}
                    onPressPriceAlert={onPressPriceAlert}
                    onPressReorder={drag}
                    onPressRemove={onPressRemove} />
            </ScaleDecorator>
        )
    }

    const flatlistComponent = useMemo(() => {
        console.debug('flatlistComponent')
        return (
            <View style={styles.draggableFlatListContainer}>
                <DraggableFlatList
                    style={styles.draggableFlatList}
                    onDragEnd={({ data }) => setData(data)}
                    keyExtractor={(item) => item}
                    data={data}
                    renderItem={renderItem} />
            </View>
        )
    }, [data])

    return (
        <StackView 
            direction='column' 
            style={styles.stackView}>
            {flatlistComponent}
            <View style={[styles.doneView, { paddingBottom: insets.bottom }]}>
                <PressableComponent 
                    onPress={onPressDone} 
                    style={[styles.done, { backgroundColor: colors.Brand1 }]}>
                    <Text style={[globalStyles.H4, { color: colors.FixedEnableBtnFont }]}>
                        {t('common.done')}
                    </Text>
                </PressableComponent>
            </View>
        </StackView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.WrapperBg,
    },
    stackView: {
        flex: 1,
        paddingTop: 17,
        backgroundColor: colors.WrapperBg,
    },
    draggableFlatListContainer: {
        flex: 1,
        backgroundColor: colors.WrapperBg,
    },
    draggableFlatList: {
        height: '100%',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: colors.MainBg,
    },
    doneView: {
        backgroundColor: colors.MainBg
    },
    done: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        paddingHorizontal: 20,
        paddingVertical: 12,
        margin: 10,
    }
})

export default EditWatchlistView