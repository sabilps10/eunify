import React, { useMemo } from 'react'
import { Pressable, StyleSheet, Text, useColorScheme } from 'react-native'
import { makeGlobalStyles } from '../../../styles/styles'
import { useTheme } from '@react-navigation/native';
import StackView from '../../../components/stackView.component'
import { productName } from '../../utils/CommonFunction';
import { State } from '../../../redux/root-reducer';
import { useSelector } from 'react-redux';
import MoveArrow from '../../../../assets/svg/MoveArrow';
import Remove from '../../../../assets/svg/Remove';
import PressableComponent from '../../utils/PressableComponent';
import { THEME_TYPE } from '../../../types/types';

interface EditWatchlistItemParams {
    symbol: string,
    onPressPriceAlert: () => void,
    onPressReorder: () => void,
    onPressRemove: (symbol: string) => void
}

const EditWatchlistItem = (params: EditWatchlistItemParams) => {

    console.debug("EditWatchListItem");
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyle(colors)

    const symbol = params.symbol

    const symbols = useSelector((state: State) => state.trading.symbols)

    const isDisable = useMemo(() => {
        let genericSymbols = Object.keys(symbols).filter((symbol) => symbol.indexOf(".") > 0).map((symbol) => symbol.split('.', 1).toString())
        return genericSymbols.indexOf(symbol) === -1;
    }, [symbols])

    return (
        <PressableComponent
            style={{flex: 1}}
            onLongPress={params.onPressReorder}>
            <StackView 
                style={styles.stackView}
                spacing={5}>
                <Text style={[globalStyles.Big_Text_B, styles.symbolName, isDisable ? { opacity: 0.3 } : null]}>
                    {symbol ?? '-'}
                </Text>
                <Text style={[globalStyles.Note, styles.title, isDisable ? { opacity: 0.3 } : null]}>
                    {productName(symbol)}
                </Text>
                <StackView spacing={6} style={{ height: '100%' }}>
                    <PressableComponent
                        disabled={isDisable}
                        style={styles.pressable}
                        onLongPress={params.onPressReorder}>
                        <MoveArrow color={colors.Brand3} />
                    </PressableComponent>
                    <PressableComponent
                        disabled={isDisable}
                        style={styles.pressable}
                        onPress={() => params.onPressRemove(symbol)}>
                        <Remove color={colors.Brand3} />
                    </PressableComponent>
                </StackView>
            </StackView>
        </PressableComponent>
    )
}

const makeStyle = (colors: THEME_TYPE) => StyleSheet.create({
    stackView: {
        height: 60,
        paddingHorizontal: 16,
        paddingVertical: 9,
        alignItems: 'center'
    },
    symbolName: {
        color: colors.Brand2, 
        flex: 1,
    },
    title: { 
        color: colors.MainFont, 
        flex: 1,
    },
    pressable: {
        alignSelf: 'center'
    }
})

export default EditWatchlistItem