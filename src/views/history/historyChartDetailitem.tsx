import React from 'react';
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles, LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { useTranslation } from 'react-i18next';
import { formatDate, toCurrencyDisplay } from '../../utils/stringUtils'
import { timestampToDateWithFormat } from '../utils/CommonFunction';

const HistoryChartDetailitem = ({ item }: { item: string }) => {


    const [t, i18n] = useTranslation();

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const historyTransaction = useSelector((state: State) => state.trading.historyTransaction && state.trading.historyTransaction[parseInt(item)]);

    console.debug("HistoryChartDetailitem: " + historyTransaction.Ticket + " " + historyTransaction.Profit + " " + historyTransaction.Type);

    const displayProfit = (profit) => {
        return profit > 0 ? '+' + toCurrencyDisplay(profit) : toCurrencyDisplay(profit)
    }

    return (
        historyTransaction ?
            <View style={[styles.listItem, { borderColor: colors.Grey0, backgroundColor: colors.White, justifyContent: 'center', alignItems: 'center' }]}>
                <View style={{ flex: 0.80, flexDirection: 'row' }}>
                    <View style={{flex: 1}}>
                        <Text style={[globalStyles.H4, { color: colors.Brand2 }]}>
                            {
                                historyTransaction.Type === 6 ?
                                    historyTransaction.Profit > 0 ?
                                        t('historyChartDetail.deposit')
                                        :
                                        t('historyChartDetail.withdraw')
                                    :
                                    t('historyChartDetail.transaction')
                            }
                        </Text>
                        <Text style={[globalStyles.Tiny_Note]}>{t('historyChartDetail.tranID')} {historyTransaction.Ticket}</Text>
                    </View>
                    <View style={[{ flex: 1, alignItems: 'flex-end', paddingEnd: 20, alignContent: 'center', justifyContent: 'center' }]}>
                        <Text
                            style={[
                                globalStyles.Body_Text,
                                { color: historyTransaction.Profit > 0 ? colors.Green : colors.Red },
                            ]}>
                            {displayProfit(historyTransaction.Profit)}
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 0.20, alignItems: 'flex-end' }}>
                    <View>
                    <View>
                            <Text style={[globalStyles.Tiny_Note]}>{timestampToDateWithFormat(historyTransaction.CloseTime!, 'YYYY-MM-DD')}</Text>
                        </View>
                        <View>
                            <Text style={[globalStyles.Tiny_Note]}>{timestampToDateWithFormat(historyTransaction.CloseTime!, 'HH:mm:ss')}</Text>
                        </View>
                    </View>
                </View>
            </View>
            :
            <View></View>
    )
};

const styles = StyleSheet.create({
    listItem: {
        width: '100%',
        borderTopWidth: 1,
        height: 72,
        flexDirection: 'row',
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },

});

export default React.memo(HistoryChartDetailitem);
