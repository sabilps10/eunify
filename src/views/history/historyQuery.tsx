import React, { useState, useEffect, useLayoutEffect } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Pressable,
  TextInput,
  Text
} from 'react-native';

import { useTheme } from '@react-navigation/native';
import { shallowEqual, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import ActionButton from '../../components/buttons/actionButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';

import Header from '../../components/header/header.component';
import BigButton from '../../components/buttons/bigButton.component';

import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';

import { formatDateYYYY } from '../../utils/stringUtils'

import DatePicker from 'react-native-date-picker'
import { ScrollView } from 'react-native-gesture-handler';
import { DetailsType } from '../../utils/orderUtils';
import SelectionItem from '../../components/bottomSelectionSheet/SelectionItem.component';
import { getDealHistorySymbolList } from '../../utils/api/tradingApi';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import Calendar from '../../../assets/svg/Calendar';
import PressableComponent from '../utils/PressableComponent';

interface typeInterface {
  key: string,
  title: string,
  value: string
}

const HistoryQuery: React.FC<Props<'HistoryQuery'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();

  const { type, inFrom, inTo, inCurrency, onQuery } = route.params;

  const { colors } = useTheme();
  const scheme = useColorScheme();
  const globalStyles = makeGlobalStyles(colors)

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () =>
        <Header
          leftComponents={<HeaderButton 
            iconComponent={<TopBarBack color={colors.MainFont} />} 
            title={t('historyDealQuery.title')} 
            titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
            onPress={onPressBackBtn} />}
        />
    });
  }, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }

  const [isOpenSelectDate, SetIsOpenSelectDate] = useState<boolean>(false);
  const [from, SetFrom] = useState<Date>(inFrom);
  const [to, SetTo] = useState<Date>(inTo);
  const [selectedDate, SetSelectedDate] = useState<string>('from');
  const [selectedOptionCode, setSelectedOptCode] = useState<string[]>([t('historyDealQuery.all')]);
  const [selectionOptions, SetSelectionOptions] = useState<typeInterface[]>([{ key: t('historyDealQuery.all'), title: t('historyDealQuery.all'), value: t('historyDealQuery.all') }]);
  const [isReset, SetIsReset] = useState<boolean>(false);

  const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login);
  const access_token = useSelector((state: State) => state.account.account && state.account.account.AccessToken);

  const orderSymbolList = useSelector((state: State) => {
    if (state.trading.orders)
      return Array.from(new Set(Object.values(state.trading.orders).map(function (k) { return k.Symbol })))
  }, shallowEqual);

  const positionSymbolList = useSelector((state: State) => {
    if (state.trading.positions)
      return Array.from(new Set(Object.values(state.trading.positions).map(function (k) { return k.Symbol })))
  }, shallowEqual);

  const [symbolList, setSymbolList] = useState<string[]>([]);

  const getHistorySymbol = async () => {
    try {
      const response = await getDealHistorySymbolList(access_token, login, Math.trunc(from.getTime() / 1000), Math.trunc(to.getTime() / 1000));
      if (response.status == 0) {
        setSymbolList(response.data);
      }
    }
    catch (error) {
      console.debug(error)
    }
  }
  
  useEffect(() => {
    console.debug("useEffect historyQuery: " + type + "from : " + from);
    if (type === DetailsType.NEW_POSITION) {
      setSymbolList(positionSymbolList);
    }
    else if (type === DetailsType.PENDING_ORDER) {
      setSymbolList(orderSymbolList);
    }
    else if (type === DetailsType.CLOSE_POSITION) {
      getHistorySymbol()
    }
  }, [orderSymbolList, positionSymbolList, to, from])

  useEffect(() => {
    var options: typeInterface[] = [{ key: t('historyDealQuery.all'), title: t('historyDealQuery.all'), value: t('historyDealQuery.all') }]
    var options2: typeInterface[] = [];
    for (var i = 0; i < symbolList.length; i++) {
      console.debug(symbolList[i]);
      options2.push({
        key: symbolList[i],
        title: symbolList[i],
        value: symbolList[i]
      })
    }

    options2 = options2.sort((a,b)=>{
      return a.value.localeCompare(b.value)
    })

    options = options.concat(options2)
    SetSelectionOptions(options)
    if (options.find(x => x.key === inCurrency[0]) && !isReset)
      setSelectedOptCode(inCurrency)
    else
      setSelectedOptCode(t('historyDealQuery.all'))

  }, [symbolList])

  const restore = () => {
    const fromDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 89)
    fromDate.setHours(0, 0, 0, 0);
    SetFrom(fromDate)
    const toDate = new Date()
    toDate.setHours(23, 59, 59, 0);
    SetTo(toDate)
    setSelectedOptCode([t('historyDealQuery.all')])
    SetIsReset(true)
  }

  const onSelect = (item: typeInterface) => {
    console.debug('onSelect', item.value)
    var list = [];
    list = [item.value]
    setSelectedOptCode(list)

    // setSelectedOptId(item.id)
    if (type !== DetailsType.CLOSE_POSITION) {
      onQuery(list)
      onPressBackBtn()
    }
  }

  const minimumDate = () => {
    let date = new Date()
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() - 89)
    return date
  }

  return (
    <SafeAreaView edges={['bottom']} style={{flex: 1, backgroundColor: colors.MainBg}}>
      <View style={[styles.container, { backgroundColor: colors.WrapperBg }]}>
        <View style={styles.contentWrapper}>
          {
            type === DetailsType.CLOSE_POSITION ?
              <View style={[styles.upperContainer, { backgroundColor: colors.MainBg }]}>
                <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL, flexDirection: 'row' }}>
                  <PressableComponent style={{ flex: 1 }} onPress={() => { SetSelectedDate('from'); SetIsOpenSelectDate(true) }}>
                    <View style={{}}>
                      <TextLabel label={t('historyDealQuery.from')} />
                      <View style={{ flexDirection: 'row', backgroundColor: colors.contentBackground, borderRadius: 5, paddingHorizontal: 5, alignItems: 'center' }}>
                        <Text style={[globalStyles.Form_Infield, {flex: 1, paddingVertical: 15}]}>{formatDateYYYY(from)}</Text>
                        <Calendar color={colors.MainFont} />
                      </View>
                    </View>
                  </PressableComponent>
                  <View style={{ padding: 5 }} />
                  <PressableComponent style={{ flex: 1 }} onPress={() => { SetSelectedDate('to'); SetIsOpenSelectDate(true) }}>
                    <View style={{}}>
                      <TextLabel label={t('historyDealQuery.to')} />
                      <View style={{ flexDirection: 'row', backgroundColor: colors.contentBackground, borderRadius: 5, paddingHorizontal: 5, alignItems: 'center' }}>
                        <Text style={[globalStyles.Form_Infield, {flex: 1, paddingVertical: 15}]}>{formatDateYYYY(to)}</Text>
                        <Calendar color={colors.MainFont} />
                      </View>
                    </View>
                  </PressableComponent>
                  <DatePicker
                    modal
                    open={isOpenSelectDate}
                    date={selectedDate === 'from' ? from : to}
                    mode='date'
                    maximumDate={new Date()}
                    minimumDate={minimumDate()}
                    onCancel={() => { SetIsOpenSelectDate(false) }}
                    onConfirm={(date) => {
                      selectedDate === 'from' ? SetFrom(date) : SetTo(date)
                      SetIsOpenSelectDate(false)
                      SetIsReset(false)
                    }}
                    title={t('historyDealQuery.dateTitle')}
                    confirmText={t('historyDealQuery.dateConfirm')}
                    cancelText={t('historyDealQuery.dateCancel')}
                    locale={i18n.language=== 'sc' ? 'zh-Hans' : (i18n.language=== 'tc' ? 'zh-Hant' : 'en') }
                  />
                </View>
                <View style={{ padding: 15 }} />
              </View> : null
          }

          {
            type === DetailsType.CLOSE_POSITION ?
              <View style={{ padding: 5 }} /> : null
          }

          <View style={[type === DetailsType.CLOSE_POSITION ? styles.lowerContainer : null, { flex: 1, backgroundColor: colors.MainBg }]}>
            <View>
              <View style={{ padding: 15 }} />
              <ScrollView style={{marginBottom: 40}}>
                <View>
                  {
                    selectionOptions.map(item => {
                      return (
                        <View key={item.key}>
                          <View style={{ padding: 0.5, backgroundColor: colors.background }} />
                          <SelectionItem
                            key={item.key}
                            item={item}
                            isSelected={selectedOptionCode.includes(item.key)}
                            onSelect={() => {onSelect(item); SetIsReset(false)}}
                            isContract={true} />
                        </View>
                      )
                    })
                  }
                </View>
              </ScrollView>
            </View>
          </View>
          {
            type === DetailsType.CLOSE_POSITION ?
              <View style={[styles.bottomContainer, { backgroundColor: colors.MainBg }]}>
                <BigButton title={t('historyDealQuery.restore')} type='Outline' onPress={() => { restore() }} />
                <ActionButton
                  title={t('common.done')}
                  onPress={() => { onQuery(from, to, selectedOptionCode); onPressBackBtn() }}
                />
              </View> 
            : 
              null
          }
        </View>
      </View>
    </SafeAreaView>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  upperContainer: {
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12
  },
  lowerContainer: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: 8,
    paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
  },
  contentWrapper: {
    flex: 1,
    // marginTop: 36,
    // marginHorizontal: 16,
  },
  contentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  dottedLine: {
    flex: 0.8,
    borderStyle: 'dotted',
    borderWidth: 1,
    borderRadius: 1
  }
});

export default HistoryQuery;