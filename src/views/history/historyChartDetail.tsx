import React, { useState, useContext, useCallback, useMemo, useEffect, useLayoutEffect, useRef } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Alert,
  Pressable,
  Image,
  TextInput,
  Text,
  FlatList
} from 'react-native';

import { useFocusEffect, useTheme } from '@react-navigation/native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import HeaderButton from '../../components/header/headerButton.component';

import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import SelectionButton from '../../components/buttons/selection.component';
import HistoryChartDetailitem from './historyChartDetailitem';
import { getDealHistory, getTransactionHistory } from '../../utils/api/tradingApi';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import HistroyChartSelectionButton from '../../components/buttons/historyChartSelection.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import TopBarMoney from '../../../assets/svg/TopBarMoney';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import { AccountUtils } from '../../utils/accountUtils';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { ActionUtils } from '../utils/ActionUtils';
import { store } from '../../redux/store';

enum Time {
  ONE_DAY = '1D',
  SEVEN_DAY = '7D',
  ONE_MONTH = '1M',
  THREE_MONTH = '3M',
}

const HistoryChartDetail: React.FC<Props<'HistoryChartDetail'>> = ({ route, navigation }) => {
  
  const [t, i18n] = useTranslation();
  const insets = useSafeAreaInsets()

  console.debug("HistoryChartDetail");

  const { colors } = useTheme();
  const scheme = useColorScheme();
  const globalStyles = makeGlobalStyles(colors)

  const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);

  const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login);
  const access_token = useSelector((state: State) => state.account.account && state.account.account.AccessToken);
  const [selectedTime, setSelectedTime] = useState<string>(Time.THREE_MONTH);

  const symbols = useSelector((state: State) => state.trading.symbols)

  // const onTouch = useSelector((state: State) => state.state.onTouch);

  const dispatch = useDispatch();
  const { SetIsShowLoading, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
  const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
  const { onPressExitDemo } = ActionUtils();
  
  const exitDemoSuccessRoute = () => {
    const state: State = store.getState();
    const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
    navigation.navigate("TabDirectory", {screen: enterDemoTab})
  }

  useLayoutEffect(() => {
    var leftComponents = [
      <HeaderButton onPress={onPressBackBtn} iconComponent={<TopBarBack color={colors.White} />} />
    ];

    var rightComponents = [
      <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.White} />} />
    ]    

    if (isDemoAccount) {
        leftComponents = [
            <HeaderButton onPress={onPressBackBtn} iconComponent={<TopBarBack color={colors.Text} />} />
        ]
        rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.Text} />} />,
            <HeaderButton onPress={()=>onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton />} />
        ]
    }

    navigation.setOptions({
      header: props => <ExpandableHeader
        isDemo={isDemoAccount}
        rightComponents={rightComponents}
        leftComponents={leftComponents}
      />
    });
  }, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }

  const onPressDeposit = async () => {
    // navigation.push('MIO', { type: MIOType.Deposit })
    navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.PORTFOLIO, param: { type: MIOType.Deposit }})
}

  const categories = useMemo(() => {
      if (symbols === null) return [];

      var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
          if (symbol.SymbolName.indexOf(".") > 0)
              return symbol.DisplayCategory
      });

      return [...new Set(categories)];
  }, [symbols])

  const onPressSearch = () => {
      console.debug("onPressSearch")
      let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
      navigation.navigate('SearchProductView', { categories: searchCategories })
  };


  const from = useMemo(() => {
    let from = new Date();
    if (selectedTime === Time.ONE_DAY) {
      from = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1);
    } else if (selectedTime === Time.SEVEN_DAY) {
      from = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7);
    } else if (selectedTime === Time.ONE_MONTH) {
      from = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate());
    } else if (selectedTime === Time.THREE_MONTH) {
      from = new Date(new Date().getFullYear(), new Date().getMonth() - 3, new Date().getDate());
    }
    from.setHours(0, 0, 0, 0);
    return from;
  }, [selectedTime])

  const HistoricalTransaction = (({ from, to }) => {

    console.debug("HistoricalTransaction")

    from.setHours(0, 0, 0, 0);
    to.setHours(23, 59, 59, 0);
    const dispatch = useDispatch();
    const { SetIsShowLoading, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    const { SetHistoryTransaction, AddHistoryTransaction } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const historyTransactionKey = useSelector((state: State) => state.trading.historyTransaction && Object.keys(state.trading.historyTransaction), shallowEqual);

    const [offset, setOffset] = useState<number>(0);

    const limit = 20;

    useFocusEffect(
      React.useCallback(() => {
        if (!access_token) return;

        setOffset(0);
        getHistory(0);
      }, [access_token])
    );

    const getHistory = async (offset: number) => {

      const response = await getTransactionHistory(access_token, login, Math.trunc(from.getTime() / 1000), Math.trunc(to.getTime() / 1000), offset, limit);
      console.debug(response.data.map(x => x.Ticket))
      if (response.status == 0) {
        if (offset === 0)
          SetHistoryTransaction(response.data);
        else
          AddHistoryTransaction(response.data);
      }
      SetIsShowLoading(false);
    }

    const renderItem = ({ item }) => (
      <HistoryChartDetailitem
        item={item.toString()} />
    )

    const onEndReached = async () => {
      console.debug('offset', offset, historyTransactionKey!.length)
      var end = offset;
      if (historyTransactionKey!.length >= end) {
        SetIsShowLoading(true)
        let newOffset = end + limit;
        setOffset(newOffset)
        await getHistory(newOffset);
      }
    }

    return (
      historyTransactionKey && historyTransactionKey.length > 0 ?
        <FlatList
          initialNumToRender={5}
          data={historyTransactionKey}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          onEndReached={onEndReached}
          renderItem={renderItem}
          // onTouchStart={()=>{SetOnTouch(!onTouch)}}
        />
        :
        <></>
    )
  });


  return (
    <>
      <View style={[styles.container, { backgroundColor: colors.background, paddingBottom: insets.bottom}]}>
        
        <View style={[styles.upperContainer]}>
          <Text style={[globalStyles.H4, { color: colors.Brand2 }]} >{t('portfolio.records')}</Text>
          <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', paddingTop: 10 }}>
            <HistroyChartSelectionButton title={t('historyChartDetail.1D')} onPress={() => { setSelectedTime(Time.ONE_DAY) }} isSelected={selectedTime === Time.ONE_DAY} />
            <HistroyChartSelectionButton title={t('historyChartDetail.7D')} onPress={() => { setSelectedTime(Time.SEVEN_DAY) }} isSelected={selectedTime === Time.SEVEN_DAY} />
            <HistroyChartSelectionButton title={t('historyChartDetail.1M')} onPress={() => { setSelectedTime(Time.ONE_MONTH) }} isSelected={selectedTime === Time.ONE_MONTH} />
            <HistroyChartSelectionButton title={t('historyChartDetail.3M')} onPress={() => { setSelectedTime(Time.THREE_MONTH) }} isSelected={selectedTime === Time.THREE_MONTH} />
          </View>
        </View>
        <View style={{ backgroundColor: colors.background, height: 10 }} />
        <View style={[globalStyles.container_mainBg, styles.lowerContainer]}>
          <View style={{ padding: 20 }} />
          <HistoricalTransaction from={from} to={new Date()} />
        </View>
      </View>
    </>
  )
};


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  upperContainer: {
    flex: 0,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    paddingTop: 20,
    paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
  },
  lowerContainer: {
    flex: 1,
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12
  }
});

export default HistoryChartDetail;