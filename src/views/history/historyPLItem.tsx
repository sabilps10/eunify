import React from 'react';
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Pressable,
} from 'react-native';

import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles, LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { useTranslation } from 'react-i18next';
import { toCurrencyDisplay } from '../../utils/stringUtils'
import { priceChangePercentageDP } from '../../config/constants';
import { displayContractCode, productName, timestampToDateWithFormat } from '../utils/CommonFunction';
import ArrowRight from '../../../assets/svg/ArrowRight';
import PressableComponent from '../utils/PressableComponent';

const HistoryPLItem = ({ item, onPressDetails }: { item: string, onPressDetails: Function }) => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const historyDeal = useSelector((state: State) => state.trading.historyDeal && state.trading.historyDeal[parseInt(item)]);
    const symbol = useSelector((state: State) => historyDeal && state.trading.symbols && state.trading.symbols[historyDeal.Symbol]);

    const displayChange = (openPrice, closePrice) => {
        var change = (closePrice - openPrice) / openPrice * 100;
        if (historyDeal.Type !== 0) {
            change = (openPrice - closePrice) / closePrice * 100;
        }
        return '(' + (change > 0 ? '+' + change.toFixed(priceChangePercentageDP) : change.toFixed(priceChangePercentageDP)) + '%)'
    }

    const displayProfit = (profit) => {
        return profit > 0 ? '+' + toCurrencyDisplay(profit) : toCurrencyDisplay(profit)
    }

    return (
        historyDeal && symbol ? 
            <View style={[styles.listItem, { borderColor: colors.WrapperBg, backgroundColor: colors.MainBg }]}>
                <View style={styles.listItemContainerTop}>
                    <Text style={[globalStyles.Note, { color: colors.Text, fontSize: 10 }]}>
                        {productName(displayContractCode(symbol?.SymbolName ?? ''))}
                    </Text>
                </View>
                <View style={styles.listItemContainerTop}>
                    <View style={styles.listHeaderItem}>
                        {/* <View>
                            <Text style={[globalStyles.Note, { color: colors.Text, fontSize: 10 }]}>
                                    {productName(displayContractCode(symbol?.SymbolName ?? ''))}
                            </Text> */}
                            <Text style={[globalStyles.H4, { color: colors.Brand2 }]}>
                                {displayContractCode(historyDeal.Symbol)}
                            </Text>
                        {/* </View> */}
                        <View style={{ width: 10 }} />
                        <Text
                            style={[
                                globalStyles.Body_Text,
                                { color: historyDeal.Type == 0 ? colors.Green : colors.Red },
                            ]}>
                            {historyDeal.Type == 0 ? t('trade.buy') : t('trade.sell')}
                        </Text>
                        <View style={{ width: 5 }} />
                        <Text style={[globalStyles.Note]}>{historyDeal.Volume.toFixed(2)}</Text>
                    </View>
                    <PressableComponent onPress={() => {onPressDetails(item)}}>
                    <View style={styles.listHeaderItem}>
                        <Text
                            style={[
                                globalStyles.Body_Text,
                                { color: historyDeal.Profit > 0 ? colors.Green : colors.Red },
                            ]}>
                            {displayProfit(historyDeal.Profit) + ' ' + displayChange(historyDeal.OpenPrice, historyDeal.ClosedPrice)}
                        </Text>
                        <View style={{ width: 10 }} />
                        <ArrowRight color={colors.Brand3} />
                    </View>
                    </PressableComponent>
                </View>
                <View style={styles.listItemContainerBottom}>
                    <View style={[styles.bottomList]}>
                        <View style={[styles.bottomListItemLeft, {flexDirection: 'row'}]}>
                            <Text style={[globalStyles.Note, {flex: 0.5, textAlign: 'right'}]}>{t('historyPL.open') + ':'}</Text>
                            <Text style={[globalStyles.Note, {flex: 0.5, textAlign: 'right'}]}>{historyDeal.OpenPrice.toFixed(symbol.Digits)}</Text>
                        </View>
                        <Text style={[globalStyles.Tiny_Note, styles.bottomListItemRight]}>{timestampToDateWithFormat(historyDeal.OpenTime!, 'YYYY-MM-DD HH:mm:ss')}</Text>
                    </View>
                    <View style={[styles.bottomList]}>
                        <View style={[styles.bottomListItemLeft, {flexDirection: 'row'}]}>
                            <Text style={[globalStyles.Note, {flex: 0.5, textAlign: 'right'}]}>{t('historyPL.close') + ':'}</Text>
                            <Text style={[globalStyles.Note, {flex: 0.5, textAlign: 'right'}]}>{historyDeal.ClosedPrice.toFixed(symbol.Digits)}</Text>
                        </View>
                        <Text style={[globalStyles.Tiny_Note, styles.bottomListItemRight]}>{timestampToDateWithFormat(historyDeal.CloseTime!, 'YYYY-MM-DD HH:mm:ss')}</Text>
                    </View>
                </View>
            </View>
        :
            <View />
    )
};

const styles = StyleSheet.create({
    listHeaderItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    listItem: {
        width: '100%',
        borderTopWidth: 1,
        paddingBottom: 5
        // height: 82,
    },
    listItemContainerTop: {
        marginTop: 5,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    listItemContainerBottom: {
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    listItemLeftRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    bottomList: {
        flexDirection: 'row',
        paddingTop: 5
    },
    bottomListItemLeft: {
        flex: 1, 
        textAlign: 'left'
    },
    bottomListItemRight: {
        flex: 1, 
        textAlign: 'right'
    }
});

export default React.memo(HistoryPLItem);
