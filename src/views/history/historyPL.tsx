import React, { useContext, useEffect, useMemo, useState } from 'react';
import { FlatList, Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { t } from 'i18next';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles, LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';

import HistoryPLItem from './historyPLItem';
import { useFocusEffect } from '@react-navigation/native';
// import { Tabs, useFocusedTab } from 'react-native-collapsible-tab-view';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { bindActionCreators } from 'redux';
import { SortingType } from '../portfolio/portfolio';
import { getDealHistory } from '../../utils/api/tradingApi';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';

import { Segmented, useIsFocused } from 'react-native-collapsible-segmented-view'
import { PortfolioTabType } from '../../redux/state/types';

const HistoryPL = ({ symbol, from, to, onPressDetails }: { symbol: string[], from: Date, to: Date, onPressDetails: Function }) => {

    console.debug("histOryPLL " + from + " " + to + " " + symbol)

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    // const focusedTab = useFocusedTab()
    // const focusedTab = PortfolioTabType.History
    const isFocused = useIsFocused()

    const selectedSortingType = useSelector((state: State) => state.selection.selected?.portfolioSortingType);

    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login);
    const access_token = useSelector((state: State) => state.account.account && state.account.account.AccessToken);

    const historyDealKey = useSelector((state: State) => state.trading.historyDeal && Object.keys(state.trading.historyDeal), shallowEqual);

    // const onTouch = useSelector((state: State) => state.state.onTouch);

    const [offset, setOffset] = useState<number>(0);

    const limit = 20;

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetPortfolioCurrentTab, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    const { SetHistoryDeal, AddHistoryDeal } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    //console.debug("historyDealKey: " + historyDealKey);

    useFocusEffect(
        React.useCallback(() => {
            if (!access_token) return;

            from.setHours(0, 0, 0, 0);
            to.setHours(23, 59, 59, 0);
            if (isFocused) {
                setOffset(0);
                getHistory(0);
                SetPortfolioCurrentTab(PortfolioTabType.History)
            }

        }, [isFocused, from, to, symbol, selectedSortingType, login, access_token])
    );

    const getHistory = async (offset: number) => {
        var inSymbol = [];
        if (!symbol.includes(t('historyDealQuery.all'))) {
            inSymbol = [...symbol];
        }

        console.debug('getHistory inSymbol', inSymbol)

        var sortType = 0;
        if (selectedSortingType === SortingType.DateDesc) {
            //Default sorting
        } else if (selectedSortingType === SortingType.DateAsc) {
            sortType = 1;
        } else if (selectedSortingType === SortingType.ProfitDesc) {
            sortType = 2;
        } else if (selectedSortingType === SortingType.ProfitAsc) {
            sortType = 3;
        }

        const response = await getDealHistory(access_token, login, Math.trunc(from.getTime() / 1000), Math.trunc(to.getTime() / 1000), offset, limit, sortType, inSymbol.length > 0 ? inSymbol[0] : "");

        if (response.status == 0) {

            console.debug(response.data.map(x => x.Ticket))
            if (offset === 0)
                SetHistoryDeal(response.data);
            else
                AddHistoryDeal(response.data);
        }
        SetIsShowLoading(false);
    }

    const onEndReached = async () => {
        console.debug('offset', offset, historyDealKey!.length)
        var end = offset;
        if (historyDealKey!.length >= end) {
            SetIsShowLoading(true)
            let newOffset = end + limit;
            setOffset(newOffset)
            await getHistory(newOffset);
        }
    }

    const renderItem = ({ item }) => (
        <HistoryPLItem
            item={item.toString()}
            onPressDetails={onPressDetails}/>
    )


    return (
        historyDealKey && historyDealKey.length > 0 ?
            <View style={[styles.container, { backgroundColor: colors.MainBg }]}>
                <Segmented.FlatList
                    initialNumToRender={5}
                    keyExtractor={item => item.id}
                    data={historyDealKey}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    onEndReached={onEndReached}
                    renderItem={renderItem}
                    // onTouchStart={()=>{SetOnTouch(!onTouch)}}
                />
            </View>
        :
            <View style={[styles.container, { backgroundColor: colors.MainBg }]} />
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    showAllWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },
    listContainer: {
        paddingTop: 15,
        paddingBottom: 15,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    listHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },
    listHeaderItem: {
        flex: 0.5,
    },

    listItemSwipeRightContainer: {
        flex: 1
    },
    swipeContainerPriceBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 24.5,
        borderRadius: 5
    }
})

export default React.memo(HistoryPL);