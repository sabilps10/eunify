import { useIsFocused, useTheme, useNavigation } from '@react-navigation/native'
import React from 'react'
import { Linking, StyleSheet, Text, View } from 'react-native'
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { lv3Registration } from '../../utils/api/userApi';
import { useTranslation } from 'react-i18next';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import ActionButton2 from '../../components/buttons/actionButton2.component';
import { toCurrencyDisplay } from '../../utils/stringUtils';
import { makeGlobalStyles } from '../../styles/styles';

import PressableComponent from '../utils/PressableComponent';
import { bindActionCreators } from "redux"
import Info from '../../../assets/svg/Info';

import { useDispatch } from "react-redux"
import { StateActionCreators } from "../../redux/state/actionCreator"
import { ScrollView } from 'react-native-gesture-handler';
import { aoEntry } from '../../utils/navigation';

interface Interface {
    onPressHistory: Function,
    onPressDeposit: () => void,
    onPressWithdrawal: () => void
}

const PortfolioUserComponent = ({
    onPressHistory,
    onPressDeposit,
    onPressWithdrawal
}: Interface) => {
    const [t, i18n] = useTranslation();

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const isFocused = useIsFocused();
    const navigation = useNavigation();

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);

    const {individual} = useSelector((state: State) => state.accountOpening);

    const dispatch = useDispatch();
    const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

    const onPressShowNote = () => {
        const componentView = (
            <View>
                <ScrollView style={{maxHeight: 300}} contentContainerStyle={{flexGrow: 1}}>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.equity')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.equityContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.floatingPL')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.floatingPLContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.margin')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.marginContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.balance')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.balanceContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.credit')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.creditContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.available')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.availableContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.liqMargin')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.liqMarginContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.marginLevel')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.marginLevelContent')}</Text>

                    <View style={styles.glossarySpacing}/>
                    <Text style={[globalStyles.Body_Text_B]}>{t('portfolioUser.component.marginRequirement')}</Text>
                    <Text style={[globalStyles.Body_Text]}>{t('portfolioUser.component.marginRequirementContent')}</Text>
                    
                    <View style={styles.glossarySpacing}/>
                </ScrollView>
                
            </View>
        )
        SetIsShowMessageDialog({isShowDialog: true, title: t('portfolioUser.component.portfolio'), message: '', detailComponent: componentView, isShowCancel: true, isShowBtn: false})
    }

    const AccountDetailComponent = () => {
        const accountDetail = useSelector((state: State) => {
            if (state.trading.accountDetail && isFocused)
                return state.trading.accountDetail
        });

        const InfoMemo = React.memo(Info);

        return (
            <>
                <View style={[styles.accountContainer, { backgroundColor: isDemoAccount ? colors.Brand1 : colors.Brand3 }]}>
                    <Text style={[globalStyles.Big_Text, { color: isDemoAccount ? colors.MainFont : colors.TitleFont }]}>
                        {t('header.equity')}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={[globalStyles.H1, { color: isDemoAccount ? colors.MainFont : colors.TitleFont}]}>
                            {toCurrencyDisplay(accountDetail?.Equity)}
                        </Text>
                        <View>
                            <Text style={[globalStyles.Big_Text, { color: isDemoAccount ? colors.MainFont : colors.TitleFont, position: 'absolute', bottom: 0}]}>
                                {' USD'}
                            </Text>
                        </View>
                    </View>
                    <View style={{alignItems: "flex-end"}}>
                        <PressableComponent
                            onPress={onPressShowNote} >
                            {<InfoMemo color={colors.MainFont} />}
                        </PressableComponent>
                    </View>
                </View>

                <View style={styles.expandContainer}>
                    <View style={styles.expandContainerItem}>
                        <View>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.floatingPL')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.margin')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.balance')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{accountDetail?.Credit > 0 ? t('header.credit') : ''}</Text>
                        </View>
                        <View style={styles.expandContainerItemValueWrapper}>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{(accountDetail?.Profit > 0 ? '+':'') + toCurrencyDisplay(accountDetail?.Profit)}{/*PL*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{toCurrencyDisplay(accountDetail?.Margin)}{/*margin*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{toCurrencyDisplay(accountDetail?.Balance)}{/*balance*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{accountDetail?.Credit > 0 ? toCurrencyDisplay(accountDetail?.Credit) : ''}{/*credit*/}</Text>
                        </View>
                    </View>
                    <View style={styles.expandContainerItem}>
                        <View>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.available')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.liqMargin')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}>{t('header.marginLevel')}</Text>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.MainFont }]}></Text>
                        </View>
                        <View style={styles.expandContainerItemValueWrapper}>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{toCurrencyDisplay(accountDetail?.MarginFree)}{/*available*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{toCurrencyDisplay(accountDetail?.LiqMargin ?? 0)}{/*available*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}>{toCurrencyDisplay(accountDetail?.MarginLevel)}%{/*available*/}</Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]}></Text>
                        </View>
                    </View>
                </View>
            </>
        )
    }

    const onPressRegisterRealTradingAccount = () => {
    aoEntry(individual, navigation);
    };

    return (
        <View style={[styles.container, { backgroundColor: isDemoAccount ? colors.Brand1 : colors.Brand3 }]} >
            <AccountDetailComponent/>
            <View style={styles.buttonContainer}>
                {
                    !isDemoAccount ?
                        <View style={{ flexDirection: 'row' }}>
                            <MSmallButton 
                                title={t('portfolio.deposit')} 
                                style={{ paddingRight: 16 }} 
                                type='White' 
                                onPress={onPressDeposit} />
                            <MSmallButton 
                                title={t('portfolio.withdraw')} 
                                style={{ paddingRight: 16 }} 
                                type='White' onPress={onPressWithdrawal} />
                        </View>
                    : 
                        null
                }
                <MSmallButton 
                    title={t('portfolio.records')} 
                    style={{ paddingRight: 16 }} 
                    textStyle={{ color: isDemoAccount ? colors.MainFont : colors.TitleFont }} 
                    type='White' onPress={() => onPressHistory()} />
            </View>
            {
                stateLoginLevel === 2 && isDemoAccount &&
                <View>
                    <ActionButton2 
                        label={t('header.guest.registerRealTradingAccount')} 
                        labelStyle={[globalStyles.ButtonText, {alignSelf: 'center'}]} 
                        onPress={onPressRegisterRealTradingAccount} />
                    <View style={{padding: 10}}/>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    accountContainer: {
        marginHorizontal: 16,
        marginTop: 5,
        marginBottom: 6,
        justifyContent: 'space-between'
    },
    expandContainer: {
        marginHorizontal: 16,
        marginTop: 5,
        marginBottom: 7,
        flexDirection: 'row',
        alignItems: 'center'
    },
    expandContainerItem: {
        flex: 0.5,
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    expandContainerItemValueWrapper: {
        flex: 1,
        alignItems: 'flex-end'
    },
    buttonContainer: {
        marginHorizontal: 16,
        marginTop: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    glossarySpacing: {
        marginBottom: 20
    }
})

export default React.memo(PortfolioUserComponent)