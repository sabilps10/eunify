import React, { useCallback, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { Text, StyleSheet, View, useColorScheme, Dimensions, Platform, StatusBar, } from 'react-native';
import { BottomTabProps } from '../../routes/route';
import { State } from '../../redux/root-reducer';
import { useIsFocused, useTheme } from '@react-navigation/native';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import HeaderButton from '../../components/header/headerButton.component';
import { useTranslation } from 'react-i18next';
import {
    LAYOUT_PADDING_HORIZONTAL,
    makeGlobalStyles
} from '../../styles/styles';

import { DetailsType, OrderActionType } from '../../utils/orderUtils'
import { AccountUtils } from '../../utils/accountUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { bindActionCreators } from 'redux';
import { WebsocketUtils } from '../../utils/websocketUtils';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { displayContractCode, getTranslationDict, productName } from '../utils/CommonFunction';
import PortfolioUserComponent from './portfolioUser.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import StackView from '../../components/stackView.component'
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import { PortfolioTabType } from '../../redux/state/types';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import OpenPositionItem from '../openPosition/openPositionItem';
import PendingOrderItem from '../pendingOrder/pendingOrderItem';
import HistoryPLItem from '../history/historyPLItem';
import { getDealHistory } from '../../utils/api/tradingApi';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderScrollableTabView from '../utils/HeaderScrollableTabView';
import { store } from '../../redux/store';
import { ActionUtils } from '../utils/ActionUtils';
import DashedLineView from '../../components/dashedLineView.component';
import { TradingResponseCode } from '../../utils/errorUtil';
import { SelectionContextLocation } from '../../redux/selection/type';

export enum TabKey {
    Position = 'Position',
    Order = 'Order',
    History = 'History'
}

export enum SortingType {
    DateDesc = 'orderByDateDesc',
    DateAsc = 'orderByDateAsc',
    ProfitDesc = 'orderByProfitDesc',
    ProfitAsc = 'orderByProfitAsc'
}

const PortfolioView: React.FC<BottomTabProps<'Tab_Portfolio'>> = ({ route, navigation }) => {
    const insets = useSafeAreaInsets()
    const windowHeight = Dimensions.get('window').height;
    const { width, height } = Dimensions.get('window'); 
    const isFullScreen = width >= 375 && height >= 812;
    const TabBarHeight = 55;
    const TabBar2Height = 55;
    var HeaderHeight = 250;
    const HeaderDefaultHeight = 250;
    const HeaderButtonHeight = 310;
    const SafeArea = Platform.select({
        ios: insets.top - insets.bottom,
        android: StatusBar.currentHeight,
    });

    const StatusBarArea = Platform.select({
        ios: 0,
        android: StatusBar.currentHeight,
    });

    const [t, i18n] = useTranslation();

    
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const scheme = useColorScheme();

    var isFocus = useIsFocused();

    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const symbols = useSelector((state: State) => state.trading.symbols);
    const portfolioCurrentTab = useSelector((state: State) => state.state.portfolioCurrentTab ?? PortfolioTabType.Position);
    // const onTouch = useSelector((state: State) => state.state.onTouch);
    const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

    // const [portfolioCurrentTab, SetPortfolioCurrentTab] = useState<string>(PortfolioTabType.Position)
    const [offset, setOffset] = useState<number>(0);

    const selectedSortingType = useSelector((state: State) => state.selection.selected?.portfolioSortingType);
    const [symbol, SetSymbol] = useState<string[]>([t('historyDealQuery.all')]);
    const [from, SetFrom] = useState<Date>(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 89));
    const [to, SetTo] = useState<Date>(new Date());
    const [defaultFrom, SetDefaultFrom] = useState<Date>(from)
    const [defaultTo, SetDefaultTo] = useState<Date>(to)
    const [isSyncScrollOffset, SetIsSyncScrollOffset] = useState<boolean>(false)

    const openPositionsKey: number[] = useSelector((state: State) => {
        if (isFocus && portfolioCurrentTab === PortfolioTabType.Position && state.trading.positions) {
            // console.debug('portfolioCurrentTab openPositionsKey', isFocus)
            var filterList = Object.values(state.trading.positions);
            if (symbol && !symbol.includes(t('historyDealQuery.all'))) {
                filterList = filterList.filter(x => symbol.includes(x.Symbol))
            }

            var sortList = filterList.sort((a, b) => b.OpenTime - a.OpenTime)
            if (selectedSortingType === SortingType.DateDesc) {
                //Default sorting
            } else if (selectedSortingType === SortingType.DateAsc) {
                sortList = filterList.sort((a, b) => a.OpenTime - b.OpenTime)
            } else {
                var profits = state.trading.profits
                if (selectedSortingType === SortingType.ProfitDesc) {
                    sortList = filterList.sort((a, b) => profits[b.Ticket] - profits[a.Ticket])
                } else if (selectedSortingType === SortingType.ProfitAsc) {
                    sortList = filterList.sort((a, b) => profits[a.Ticket] - profits[b.Ticket])
                }
            }

            return sortList.map(a => a.Ticket);
        } else {
            return undefined
        }
    }, shallowEqual);

    const pendingOrderKey: number[] = useSelector((state: State) => {
        if (isFocus && portfolioCurrentTab === PortfolioTabType.Order && state.trading.orders)
        {
            var filterList = Object.values(state.trading.orders);
            if (symbol && !symbol.includes(t('historyDealQuery.all'))) {
                filterList = filterList.filter(x => symbol.includes(x.Symbol))
            }

            var sortList = filterList.sort((a, b) => b.TimeSetup - a.TimeSetup)
            if (selectedSortingType === SortingType.DateDesc) {
                //Default sorting
            } else if (selectedSortingType === SortingType.DateAsc) {
                sortList = filterList.sort((a, b) => a.TimeSetup - b.TimeSetup)
            }

            return sortList.map(a => a.Ticket);
        } else {
            return undefined
        }
    }, shallowEqual);

    const historyDealKey = useSelector((state: State) => {
        if (isFocus && portfolioCurrentTab === PortfolioTabType.History) {
            return state.trading.historyDeal && Object.keys(state.trading.historyDeal)
        } else {
            return  undefined
        }
    }, shallowEqual);


    // useEffect(() => {
    //     if (ref && ref.current && route.params.defaultTab)
    //         ref.current.jumpToTab(route.params.defaultTab)
    // }, [route.params.defaultTab]);

    useEffect(() => {
        if (route.params.positionTicket)
            navigation.navigate('PositionDetails', { type: DetailsType.NEW_POSITION, ticketId: route.params.positionTicket })
    }, [route.params.positionTicket])

    const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();
    const { onPressExitDemo } = ActionUtils();

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowOrderConfirmDialog, SetOnTouch, SetIsShowMessageDialog, SetPortfolioCurrentTab } = bindActionCreators(StateActionCreators, dispatch);
    const { OpenCloseSelection, SetSelected } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
    const { SetHistoryDeal, AddHistoryDeal } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    //Sorting selection
    const [sortingTypeSelectionOptions, setSortingTypeSelectionOptions] = useState<string[]>([SortingType.DateDesc, SortingType.DateAsc, SortingType.ProfitDesc, SortingType.ProfitAsc]);
    const [sortingTypeOrderSelectionOptions, setSortingTypeOrderSelectionOptions] = useState<string[]>([SortingType.DateDesc, SortingType.DateAsc]);
    const [sortingType, setSortingType] = useState<string>(sortingTypeSelectionOptions[0]);

    const limit = 20;
    const access_token = useSelector((state: State) => state.account.account && state.account.account.AccessToken);
    const getHistory = async (offset: number, inSymbol?: string[], inFrom?: Date, inTo?: Date) => {
        var inSymbol = inSymbol ?? [];
        // if (!symbol.includes(t('historyDealQuery.all'))) {
        //     inSymbol = [...symbol];
        // }
        if (inSymbol.includes(t('historyDealQuery.all'))) {
            inSymbol = [];
        }

        console.debug('getHistory inSymbol', inSymbol)

        var sortType = 0;
        if (selectedSortingType === SortingType.DateDesc) {
            //Default sorting
        } else if (selectedSortingType === SortingType.DateAsc) {
            sortType = 1;
        } else if (selectedSortingType === SortingType.ProfitDesc) {
            sortType = 2;
        } else if (selectedSortingType === SortingType.ProfitAsc) {
            sortType = 3;
        }

        var queryFrom = inFrom ?? from;
        var queryTo = inTo ?? to;

        const response = await getDealHistory(access_token, login, Math.trunc(queryFrom.getTime() / 1000), Math.trunc(queryTo.getTime() / 1000), offset, limit, sortType, inSymbol.length > 0 ? inSymbol[0] : "");

        if (response.status == 0) {

            console.debug(response.data.map(x => x.Ticket))
            if (offset === 0)
                SetHistoryDeal(response.data);
            else
                AddHistoryDeal(response.data);
        }
        SetIsShowLoading(false);
    }

    useEffect(() => {
        if (selectedSortingType) {
            setSortingType(selectedSortingType);
            if (portfolioCurrentTab === PortfolioTabType.History) {
                setOffset(0);
                getHistory(0, symbol);
            }
        }
    }, [selectedSortingType, isFocus]);

    useEffect(() => {
        SetIsSyncScrollOffset(prev => !prev)
        SetSelected({
            portfolioSortingType: SortingType.DateDesc
        });
        SetSymbol([t('historyDealQuery.all')])
        if (portfolioCurrentTab === PortfolioTabType.History) {
            getHistory(0);
        }
    }, [portfolioCurrentTab]);

    useEffect(() => {
        // console.debug('symbol', symbol)
        let allEn = t('historyDealQuery.all', { returnObjects: true, lng: 'en' })
        let allTc = t('historyDealQuery.all', { returnObjects: true, lng: 'tc' })
        let allSc = t('historyDealQuery.all', { returnObjects: true, lng: 'sc' })
        if (symbol.includes(allEn) || symbol.includes(allTc) || symbol.includes(allSc)){
            // console.debug('symbol includes')
            SetSymbol([t('historyDealQuery.all')])
        }
    }, [i18n.language]);

    useEffect(() => {
        if (isDemoAccount && stateLoginLevel === 2) {
            HeaderHeight = HeaderButtonHeight
        } else {
            HeaderHeight = HeaderDefaultHeight
        }
    }, [isDemoAccount, stateLoginLevel]);

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    const onPressSorting = () => {
        OpenCloseSelection({
            selectedOption: sortingType,
            selectOptions: getTranslationDict(portfolioCurrentTab === PortfolioTabType.Order ? sortingTypeOrderSelectionOptions: sortingTypeSelectionOptions, "portfolio", t),
            isOpen: !isOpen,
            location: SelectionContextLocation.PortfolioSortingType
        })
    }

    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            if (symbol.SymbolName.indexOf(".") > 0)
                return symbol.DisplayCategory
        });

        return [...new Set(categories)];
    }, [symbols])

    useLayoutEffect(() => {
        var rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        if (isDemoAccount) {
            rightComponents = [
                <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.MainFont} />} />,
                <HeaderButton onPress={() => onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton />} />
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents.push(
                // <HeaderButton iconComponent={<TopBarNoticeNew color={colors.White} />} />
            )
        }

        navigation.setOptions({
            header: props => <ExpandableHeader
                isDemo={isDemoAccount}
                isShowBalance={false}
                rightComponents={rightComponents}
                enableBottomRadius={false}
                enableExpand={false} />,
        });
    }, [navigation, isDemoAccount, categories]);

    const onPressClosePosition = useCallback(
        (Ticket: number) => {
        navigation.navigate('ClosePosition', { ticketId: Ticket })
    }, [])

    const onPressCloseOrder = useCallback(
        async (Ticket: number, symbol: string, orderIsBuy: boolean, orderVolume: string) => {
        if (!isEnableTradeConfirmationDialog) {
            var result = await startDeletePendingOrder(Ticket);
            if (result.RtnCode === TradingResponseCode.RET_OK || result.RtnCode === TradingResponseCode.RET_CUST_OK) {
                orderSuccessCallback(symbol);
            } else {
                const componentView = (
                    <View>
                        <View style={styles.contentItem}>
                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                            <DashedLineView style={{paddingHorizontal: 10}} />
                            <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(symbol)}</Text>
                        </View>
                        <View style={styles.contentItem}>
                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                            <DashedLineView style={{paddingHorizontal: 10}} />
                            <Text style={[globalStyles.Body_Text_B]}>{productName(displayContractCode(symbol))}</Text>
                        </View>
                        <View style={styles.contentItem}>
                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                            <DashedLineView style={{paddingHorizontal: 10}} />
                            <Text style={[globalStyles.Body_Text_B, orderIsBuy ? {color: colors.Green} : {color: colors.Red}]}>{orderIsBuy ? t('trade.buy') : t('trade.sell')}</Text>
                        </View>
                        <View style={styles.contentItem}>
                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                            <DashedLineView style={{paddingHorizontal: 10}} />
                            <Text style={[globalStyles.Body_Text_B]}>{orderVolume} {t('confirmTrade.lots')}</Text>
                        </View>
                    </View>
                )

                if (result.RtnCode === -1) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.cancelOrderFailure'), message: ''})
                } else if (result.RtnCode === 132) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.132'), message: ''})
                    
                } 
                else if (result.RtnCode === 999) {
                    SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
                }
                else {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode)})
                }
            }
        }
        else {
            SetIsShowOrderConfirmDialog({
                isShowDialog:true,
                type: OrderActionType.CANCEL_ORDER,
                contractCode:'',
                isSelectBuy:false,
                volume:0,
                price:0,
                isPending:true,
                pendingOrderType:0,
                pendingOrderValidity:0,
                isProfit:false,
                stopLossPrice:0,
                limitProfitPrice:0,
                transactionId:Ticket,
                callback:orderSuccessCallback,
            })
        }
    }, [])

    const onPressEditPosition = useCallback(
        (Ticket: number) => {
        navigation.navigate('EditPosition', { ticketId: Ticket })
    }, [])

    const onPressEditOrder = useCallback(
        (Ticket: number) => {
        navigation.navigate('NewOrder', { symbolName: ' ', isBuy: false, transactionId: Ticket })
    }, [])

    const onPressOpenPositionDetails = useCallback(
        (Ticket: number) => {
        navigation.navigate('PositionDetails', { type: DetailsType.NEW_POSITION, ticketId: Ticket })
    }, [])

    const onPressPendingOrderDetails = useCallback(
        (Ticket: number) => {
        navigation.navigate('PositionDetails', { type: DetailsType.PENDING_ORDER, ticketId: Ticket })
    }, [])

    const onPressHistoryDetails = useCallback((Ticket: number) => {
        navigation.navigate('PositionDetails', { type: DetailsType.CLOSE_POSITION, ticketId: Ticket })
    }, []);

    const onPressSearch = () => {
        let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
        navigation.navigate('SearchProductView', { categories: searchCategories })
    }

    // const ref = React.useRef<CollapsibleRef>()

    const orderSuccessCallback = (contractCode: string) => {
        navigation.push('ContractDetailView', { contractCode: contractCode, defaultTab: 1, defaultPositionTab: 1, isEnterByOrder: true })
    }

    const onPressHistory = useCallback(() => {
        navigation.navigate('HistoryChartDetail')
    }, []);

    const onPressDeposit = useCallback(() => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.PORTFOLIO, param: { type: MIOType.Deposit }})
    }, [])

    const onPressWithdrawal = useCallback(() => {
        // navigation.push('MIO', { type: MIOType.Withdraw })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.PORTFOLIO, param: { type: MIOType.Withdraw }})
    }, [])

    const Header = () => {
        return (
            <PortfolioUserComponent 
                onPressHistory={onPressHistory} 
                onPressDeposit={onPressDeposit}
                onPressWithdrawal={onPressWithdrawal}
            />
        )
    }

    const onQuery1 = (symbolQuery: string[]) => {
        console.debug('onQuery1', symbolQuery)
        if (!shallowEqual(symbol, symbolQuery))
            SetSymbol(symbolQuery)
    }

    const onQuery2 = (fromQuery: Date, toQuery: Date, symbolQuery: string[]) => {
        console.debug('onQuery2')
        fromQuery.setHours(0, 0, 0, 0);
        toQuery.setHours(23, 59, 59, 0);
        SetFrom(fromQuery)
        SetTo(toQuery)
        setOffset(0)
        if (!shallowEqual(symbol, symbolQuery))
            SetSymbol(symbolQuery)
        getHistory(0, symbolQuery, fromQuery, toQuery)
    }

    const onPressShowAll = () => {
        if (portfolioCurrentTab === PortfolioTabType.Position) {
            navigation.navigate('HistoryQuery', { type: DetailsType.NEW_POSITION, inFrom: from, inTo: to, inCurrency: symbol, onQuery: onQuery1 })
        } else if (portfolioCurrentTab === PortfolioTabType.Order) {
            navigation.navigate('HistoryQuery', { type: DetailsType.PENDING_ORDER, inFrom: from, inTo: to, inCurrency: symbol, onQuery: onQuery1 })
        } else if (portfolioCurrentTab === PortfolioTabType.History) {
            navigation.navigate('HistoryQuery', { type: DetailsType.CLOSE_POSITION, inFrom: from, inTo: to, inCurrency: symbol, onQuery: onQuery2 })
        }
    }

    const renderFilter = () => {
        return (
            <View style={{backgroundColor: isDemoAccount ? colors.Brand1 : colors.Brand3}}>
                <View style={[styles.listContainer, { backgroundColor: colors.MainBg }]}>
                    <View style={[styles.listHeader, { backgroundColor: colors.MainBg }]}>
                        <View style={[styles.listHeaderItem, { alignItems: 'flex-start' }]}>
                            <MSmallButton title={t('portfolio.by') + ' ' + t('portfolio.' + sortingType)} type='White' textStyle={globalStyles.Note} onPress={onPressSorting} />
                        </View>
                        <View style={[styles.listHeaderItem, { alignItems: 'flex-end' }]}>
                            {/* <MSmallButton title={(symbol.includes(t('historyDealQuery.all')) && (ref.current?.getFocusedTab() !== PortfolioTabType.History || (ref.current?.getFocusedTab() === PortfolioTabType.History && (defaultFrom.getTime() === from.getTime() && defaultTo.getTime() === to.getTime())))) ? t('portfolio.showAll') : t('portfolio.showCustom')} type='Solid' onPress={onPressShowAll} /> */}
                            <PressableComponent style={styles.infoBarWrapper} onPress={onPressShowAll}>
                                <StackView spacing={8} style={{ alignItems: 'center' }}>
                                    <Text style={[globalStyles.Big_Text_B]}>{(symbol.includes(t('historyDealQuery.all')) && ( ((defaultFrom.getTime() === from.getTime() && (defaultTo.getFullYear() === to.getFullYear() && defaultTo.getMonth() === to.getMonth() && defaultTo.getDate() === to.getDate()))))) ? ( portfolioCurrentTab === PortfolioTabType.History? t('portfolio.show90') : t('portfolio.showAll')) : t('portfolio.showCustom')}</Text>
                                    <CircleArrow20px color={colors.White} backgroundColor={colors.Brand3} />
                                </StackView>
                            </PressableComponent>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    const renderItemOpenPosition = ({ item, index }) => {
        return (
            <OpenPositionItem
                isDetailsPage={false}
                item={item.toString()}
                onPressClose={onPressClosePosition}
                onPressEdit={onPressEditPosition}
                onPressDetails={() => onPressOpenPositionDetails(item)} />
        );
    };
    
    const renderItemOrder = ({item, index}) => {
        return (
            <PendingOrderItem
                isDetailsPage={false}
                item={item && item.toString()}
                onPressClose={onPressCloseOrder}
                onPressEdit={onPressEditOrder}
                onPressDetails={() => onPressPendingOrderDetails(item)} />
        );
    };

    const renderItemHistorical = ({item, index}) => {
        return (
            <HistoryPLItem
            item={item.toString()}
            onPressDetails={onPressHistoryDetails}/>
        );
    };
  
    const styles = StyleSheet.create({
        listContainer: {
            height: TabBar2Height,
            marginTop: 7,
            paddingTop: 15,
            paddingBottom: 15,
            borderTopLeftRadius: 12,
            borderTopRightRadius: 12,
        },
        listHeader: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: LAYOUT_PADDING_HORIZONTAL,
            paddingRight: 4
        },
        listHeaderItem: {
            flex: 0.5,
        },
        infoBarWrapper: {
            flexDirection: 'row',
            alignItems: 'center'
        },
        flatListContentContainerStyle: {
            paddingTop: (isDemoAccount && stateLoginLevel === 2 ? HeaderButtonHeight : HeaderHeight) + TabBarHeight + TabBar2Height, 
            minHeight: windowHeight + TabBarHeight + (Platform.OS === "android"  ? (TabBar2Height + SafeArea) : ( isFullScreen ? (20 - SafeArea) : ((TabBar2Height + SafeArea - 10))) ) + (isDemoAccount && stateLoginLevel === 2 ? 110 : 0)
        },
        tab: {
            backgroundColor: isDemoAccount ? colors.Brand1 : colors.Brand3
        },
        tabTitle: {
            color: isDemoAccount ? colors.DemoHeaderFont : colors.HeaderFont,
            // flex: 1, 
            textAlign: 'center', 
            marginHorizontal: 4, 
            fontFamily: 'Montserrat', 
            fontStyle: 'normal', 
            fontWeight: '600', 
            fontSize: 13
        },
        tabIndicatorStyle: {
            backgroundColor: colors.HeaderFont
        },
        contentItem: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: 10
        },
    })

    const onEndReached = async () => {
        if (portfolioCurrentTab === PortfolioTabType.History) {
            console.debug('offset', offset, historyDealKey!.length)
            var end = offset;
            if (historyDealKey!.length >= end) {
                SetIsShowLoading(true)
                let newOffset = end + limit;
                setOffset(newOffset)
                await getHistory(newOffset, symbol);
            }
        }
    }

    return (
        <HeaderScrollableTabView 
            header={
                <PortfolioUserComponent 
                    onPressHistory={onPressHistory} 
                    onPressDeposit={onPressDeposit}
                    onPressWithdrawal={onPressWithdrawal} />
            }
            routes={[
                {key: TabKey.Position, title: t('portfolio.openPositions')},
                {key: TabKey.Order, title: t('portfolio.pendingOrders')},
                {key: TabKey.History, title: t('portfolio.historicalPL')}
            ]}
            onTabPress={(key) => {
                SetIsSyncScrollOffset(prev => !prev)

                if (key === TabKey.Position) {
                    SetPortfolioCurrentTab(PortfolioTabType.Position)
                } else if (key === TabKey.Order) {
                    SetPortfolioCurrentTab(PortfolioTabType.Order)
                } else if (key === TabKey.History) {
                    SetPortfolioCurrentTab(PortfolioTabType.History)
                    var newToDate = new Date();
                    SetDefaultTo(newToDate)
                    SetTo(newToDate);
                } 
                setOffset(0)
            }}
            tabBarFilter={renderFilter()}
            dataSource={[
                {data: openPositionsKey, renderItem: renderItemOpenPosition},
                {data: pendingOrderKey, renderItem: renderItemOrder},
                {data: historyDealKey, renderItem: renderItemHistorical},
            ]}
            flatListProps={{
                contentContainerStyle: styles.flatListContentContainerStyle
            }}
            tabProps={{
                style: styles.tab,
                titleStyle: styles.tabTitle,
                indicatorStyle: styles.tabIndicatorStyle
            }}
            onEndReached={onEndReached}
            isSyncScrollOffset={isSyncScrollOffset}
        />
    )
}

export default PortfolioView