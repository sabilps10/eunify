import React, { useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { Pressable, StyleSheet, Text, useColorScheme, View, FlatList,  KeyboardAvoidingView } from 'react-native';
import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component'
import HeaderSearchBar from '../../components/header/headerSearchBar.component';
import { Props } from '../../routes/route';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import SearchProductItem from './searchProductItem';
import { useTranslation } from 'react-i18next';
import { SafeAreaView } from 'react-native-safe-area-context';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { debounce } from 'lodash'
import { addWatchList, removeWatchList } from '../../utils/api/tradingApi';
import ArrowRight from '../../../assets/svg/ArrowRight';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { categoryName, displayContractCode } from '../utils/CommonFunction';
import { THEME_TYPE } from '../../types/types';
import i18n from '../../locales/i18n';
import { SymbolContent } from '../../redux/trading/type';
import PressableComponent from '../utils/PressableComponent';

interface SearchResult {
    id: string,
    title: string
}

const SearchProductView: React.FC<Props<'SearchProductView'>> = ({ route, navigation }) => {

    const [t] = useTranslation()
    
    let popular: SearchResult = {id: 'Popular', title: 'Popular'}

    const params = route.params
    const filter = params.filter

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)
    const dispatch = useDispatch()

    const { SetMyList } = bindActionCreators(StateActionCreators, dispatch)
    const { SetIsShowLoading, SetIsShowToastDialog, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch)

    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1)
    const symbols = useSelector((state: State) => state.trading.symbols)
    const recommandList = useSelector((state: State) => state.trading.recommendList)
    const watchlist = useSelector((state: State) => state.state.myList ?? [])
    const positions = useSelector((state: State) => state.trading.positions)

    const [categories, setCategories] = useState<SearchResult[]>(params.categories ? [popular, ...params.categories] : null)
    const [searchText, setSearchText] = useState<string>('')
    const [flatListData, setFlatListData] = useState<SearchResult[]>([])

    const accessToken = useSelector((state: State) => state.account.account?.AccessToken);
    // const onTouch = useSelector((state: State) => state.state.onTouch);
    
    useLayoutEffect(() => {
        // console.debug('filter:' + filter)
        // console.debug('filter.byCategory:' + filter?.byCategory)
        // console.debug('filter.byKeywords:' + filter?.byKeywords)
        navigation.setOptions({
            header: () => categories ? categoriesHeaderComponent() : contractsHeaderComponent(filter)
        });
    }, [navigation, searchText, categories, filter]);

    useEffect(() => {
        if (categories == null) return
        setFlatListData(categories)
    }, [categories])

    useEffect(() => {
        console.debug('searchProductView, searchText')
        // console.debug('searchProductView, searchText, date:' + new Date() + ' searchText: ' + searchText)

        if (searchText.length === 0) {
            setFlatListData(categories)
            return
        }

        if (symbols == null) return
        let search = searchText.toUpperCase()
        // console.debug('searchProductView, search:' + search)

        // console.debug('searchProductView, Object.values(symbols):' + Object.values(symbols).flatMap((item) => item.SymbolName))

        // Filter all non region specific symbols.
        let filteredSymbols: SymbolContent[] = Object.values(symbols).filter((symbol) => symbol.SymbolName.indexOf('.') > 0)

        // Get Contracts name from English Locale in JSON.
        let productMapEn = t('product', { returnObjects: true, lng: 'en' })
        
        // Create a searching datas
        let searchingData: {id: string, title: string, nameEn: string, nameInCurrentLocale: string}[] = filteredSymbols.flatMap((symbol) => {
            let id = symbol.SymbolName
            let title = symbol.SymbolName.split('.')[0]
            let nameEn = productMapEn[title]
            let nameInCurrentLocale = t('product.' + title)

            return {id: id, title: title, nameEn: nameEn, nameInCurrentLocale: nameInCurrentLocale}
        })
        // Searching
        .filter((item) => {
            let isSearchInEnglish = search.match('[a-zA-Z]+')
            
            // console.debug('searchProductView, isSearchInEnglish:' + isSearchInEnglish)

            let isTitle = item.title.includes(search)
            let isNameEn = isSearchInEnglish == null || item.nameEn === undefined ? false : item.nameEn.toUpperCase().includes(search)
            let isNameInCurrentLocale = item.nameInCurrentLocale.includes(search)

            // console.debug('searchProductView, item.title:' + item.title)
            // console.debug('searchProductView, item.nameEn:' + item.nameEn)
            // console.debug('searchProductView, item.nameInCurrentLocale:' + item.nameInCurrentLocale)

            // console.debug('searchProductView, isTitle:' + isTitle)
            // console.debug('searchProductView, isNameEn:' + isNameEn)
            // console.debug('searchProductView, isNameInCurrentLocale:' + isNameInCurrentLocale)

            return isTitle || isNameEn || isNameInCurrentLocale
        })
        let data: SearchResult[] = searchingData.flatMap((item) => {
            return {id: item.id, title: item.title}
        })
        console.debug('data:' + JSON.stringify(data))

        setFlatListData(data)
    }, [symbols, searchText])

    useEffect(() => {
        console.debug('searchProductView, filter:' + filter)
        // console.debug('searchProductView, symbols:' + symbols)
        if (filter == null || symbols == null) return

        let filteredSymbols = Object.fromEntries(Object.entries(symbols).filter(([key, value]) => key.indexOf('.') > 0));
        var symbolContracts: SymbolContent[] = []

        if (filter === 'Popular') {
            if (recommandList == null) return
            // console.debug('searchProductView, recommandList:' + recommandList)

            let filteredSymbolsKey = Object.keys(filteredSymbols)
            // console.debug('searchPRoductView, filteredSymbolsKey:' + filteredSymbolsKey)

            symbolContracts = recommandList
                .flatMap((symbol) => {
                    let index = filteredSymbolsKey.findIndex((item) => item.includes(symbol))
                    return index == -1 ? null : filteredSymbols[filteredSymbolsKey[index]]
                })
                .filter((item) => item)
        }
        else {
            symbolContracts = Object.values(filteredSymbols)
                .filter((symbol => symbol.DisplayCategory === filter))
        }

        // console.debug('searchProductView, symbolContracts:' + JSON.stringify(symbolContracts))

        let data: SearchResult[] = symbolContracts.flatMap((symbol) => {
            // console.debug('searchProductView, symbol:' + symbol)
            // console.debug('searchProductView, symbol.SymbolName:' + symbol.SymbolName)

            let id = symbol.SymbolName
            let title = symbol.SymbolName.split('.')[0]

            return {id: id, title: title}
        })
        // console.debug('data:' + data)
        setFlatListData(data)
    }, [filter, symbols, recommandList])

    const searchDebounce = React.useRef(debounce(async (text) => { setSearchText(text)}, 500)).current

    const categoriesHeaderComponent = () => {
        return (
            <Header 
                style={{backgroundColor: colors.WrapperBg}}
                leftComponents={[
                    <HeaderButton iconComponent={<TopBarBack color={colors.MainFont} />} onPress={onPressBack} />
                ]}
                MiddleComponent={<HeaderSearchBar onChangeText={onChangeText} />}
            />
        )
    }

    const contractsHeaderComponent = (filter?: string) => {
        return (
            <Header 
                leftComponents={[
                    <HeaderButton title={categoryName(filter)} titleStyle={{...globalStyles.H4, color: colors.Brand2}} iconComponent={<TopBarBack color={colors.MainFont} />} onPress={onPressBack} />
                ]}
            />
        )
    }

    const onPressBack = () => { navigation.goBack() }

    const onChangeText = (value: string) => { searchDebounce(value) }

    const onPressCategoryItem = (id: string) => {
        // console.debug('onPressCategoryItem')
        // console.debug('onPressCategoryItem id:' + id)
        
        let category = flatListData?.find((item) => item.id === id)

        if (category == null) return
        navigation.push('SearchProductView', {filter: category.id})
    }

    const onPressContractItem = (item: string) => {
        console.debug('onPressContractItem')
        console.debug('onPressContractItem item:' + item)
        navigation.push('ContractDetailView', {contractCode: item, isEnterByOrder: true})
    }

    const onPressWatchlist = async (item: string) => {
        console.debug('item:' + item)
        console.debug('watchlist:' + watchlist)
        //SetIsShowLoading(true)
        const isLoggedin = !(stateLoginLevel === 1)
        const isAddedWatchlist = watchlist.find(obj => obj === item)

        var success = true

        // If Loggedin, call WS update.
        if (isLoggedin && accessToken) {
            // If is Remove Symbol from Watchlist
            if (isAddedWatchlist && positions) {
                var validate = Object.values(positions).filter(position => displayContractCode(position.Symbol) === item).length === 0

                if (!validate) {
                    SetIsShowToastDialog({
                        isShowDialog: true,
                        message: t('editWatchlist.unableRemove'),
                        isSuccess: false
                    })
                    return
                }
            }
            success = isAddedWatchlist ? await removeWatchList(accessToken, item) : await addWatchList(accessToken, item)
        }
        //SetIsShowLoading(false)
        if (!success) {
            console.debug(isAddedWatchlist ? 'Add' : 'Remove' + ' Watchlist ' + item + ' fail.')
            return
        }

        if (isAddedWatchlist) {
            SetIsShowToastDialog({
                isShowDialog: true,
                message: t('editWatchlist.removeSuccess'),
                isSuccess: true
            })
            watchlist.splice(watchlist.indexOf(item), 1)
        }
        else {
            SetIsShowToastDialog({
                isShowDialog: true,
                message: t('editWatchlist.addSuccess'),
                isSuccess: true
            })
            watchlist.push(item)
        }
        SetMyList([...new Set(watchlist)])
    }

    const renderCategoryItem = ({ item, index }: { item: {id: string, title: string}, index: number }) => {
        return (
            <PressableComponent 
                onPress={() => onPressCategoryItem(item.id)}
                style={[styles.renderCategoryItemContainer, { borderColor: colors.Grey0}]}>
                
                <Text style={[globalStyles.Big_Text_B]}>
                    {categoryName(item.title)}
                </Text>
                <View style={styles.arrow}>
                    <ArrowRight color={colors.Brand3} />
                </View>
            </PressableComponent>
        )
    }

    const renderContractItem = ({ item }: { item: {id: string, title: string} }) => {
        return (
            <SearchProductItem 
                item={item}
                onPress={onPressContractItem}
                onPressWatchlist={onPressWatchlist}/>
        )
    }

    const renderItem = ({ item, index }: { item: {id: string, title: string}, index: number }) => {
        if (searchText || filter)
            return renderContractItem({item})
        
        if (categories)
            return renderCategoryItem({item, index})

        return null
    }

    const ItemSeparatorComponent = (prop: any) => {
        return searchText.length === 0 ? null : <View style={styles.itemSeparator} />
    }

    const flatlistComponent = useMemo(() => {
        return (
            <FlatList
                data={flatListData}
                keyExtractor={(item, index) => item.id}
                renderItem={renderItem}
                ItemSeparatorComponent={ItemSeparatorComponent}
                showsHorizontalScrollIndicator={false} 
                // onTouchStart={()=>{SetOnTouch(!onTouch)}}
                />
        )
    }, [flatListData])

    return (
        <View 
            style={[styles.container, {backgroundColor: colors.WrapperBg}]}>
            <SafeAreaView 
                style={[styles.safeArea, {backgroundColor: colors.MainBg}, filter ? null : styles.contrainerRadius]} 
                edges={['left', 'right', 'bottom']}>
                <KeyboardAvoidingView 
                    style={{flex: 1}}
                    behavior='height'>
                    {flatlistComponent}
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        flex: 1,
    },
    safeArea: {
        flex: 1,
        paddingTop: 26
    },
    contrainerRadius: {
        borderTopLeftRadius: 12, 
        borderTopRightRadius: 12
    },
    renderCategoryItemContainer: {
        height: 50,
        paddingHorizontal: 16,
        paddingTop: 15,
        paddingBottom: 13,
        borderTopWidth: 1,
        borderTopColor: colors.WrapperBg,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    renderValuesItemContainer: {
        height: 42,
        paddingHorizontal: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    itemSeparator: {
        height: 18
    },
})

export default SearchProductView