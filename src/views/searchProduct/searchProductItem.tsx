import { useTheme } from "@react-navigation/native"
import React, { useEffect, useMemo, useState } from "react"
import { useTranslation } from "react-i18next"
import { Image, Pressable, StyleSheet, Text, useColorScheme, View } from "react-native"
import { useSelector } from "react-redux"
import FavAdd from "../../../assets/svg/FavAdd"
import FavAdded from "../../../assets/svg/FavAdded"
import { State } from "../../redux/root-reducer"
import { makeGlobalStyles } from "../../styles/styles"
import { displayContractCode, productName } from "../utils/CommonFunction"
import PressableComponent from "../utils/PressableComponent"


interface SearchProductItemParams {
    item: {id: string, title: string},
    onPress: (item: string) => void,
    onPressWatchlist: (item: string) => void,
}

const searchProductItem = (params: SearchProductItemParams) => {

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    const contract = displayContractCode(params.item.id);
    const watchlist = useSelector((state: State) => state.state.myList ?? [])

    const watchlistImage = useMemo(() => {
        let isAddedWatchlist = watchlist.find(obj => obj === contract)
        if (isAddedWatchlist)
            return <FavAdded color={colors.Brand3} />
        else
            return <FavAdd color={colors.Brand3} />
    }, [watchlist])

    return (
        <PressableComponent
            onPress={() => params.onPress(params.item.id)}
            style={styles.item}>
            <View style={styles.contractContainer}>
                <Text style={[globalStyles.Big_Text_B, styles.contractText, {color: colors.Brand2}]}>
                    {contract}
                </Text>
            </View>
            <View style={styles.contractContainer}>
                <Text style={[globalStyles.Note, styles.contractText, {color: colors.MainFont}]}>
                    {productName(contract)}
                </Text>
            </View>
            <PressableComponent 
                onPress={() => params.onPressWatchlist(contract)}
                style={styles.bookmarkContainer}>
                {watchlistImage}
            </PressableComponent>
        </PressableComponent>
    )
}

const styles = StyleSheet.create({
    item: {
        height: 42,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        flex: 1,
    },
    contractContainer: {
        flex: 1, 
        marginLeft: 16,
        alignSelf: 'center'
    },
    contractText: {
        textAlignVertical: 'center'
    },
    bookmarkContainer: {
        paddingHorizontal: 16, 
        justifyContent: 'center'
    },
})

export default searchProductItem