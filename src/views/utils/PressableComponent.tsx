import * as React from "react"
import { Pressable } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { State } from "../../redux/root-reducer";
import { StateActionCreators } from "../../redux/state/actionCreator";

const PressableComponent = (props) => {

    const dispatch = useDispatch();
    const { SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    // const onTouch = useSelector((state: State) => state.state.onTouch);

    return (
        <Pressable {...props} onPress={()=>{
            if (props && props.onPress) {
                props.onPress();
            }
            SetOnTouch()
        }}/>
    )
}

export default PressableComponent
