import { t } from 'i18next';
import moment from 'moment-timezone';
import { Dimensions } from 'react-native';
import { State } from '../../redux/root-reducer';
import { store } from '../../redux/store';
import * as RNLocalize from "react-native-localize";
const windowWidth = Dimensions.get('window').width;

export const getTranslationDict = (options: string[], translationKey: string, t: any) => {
  return options.map((e) => {
    let key = translationKey + '.' + e
    let value = t(key)
    let display = key === value ? e : value
    return { key: e, value: translationKey === '' ? t(e) : display }
  })
}

export const getDict = (options: string[]) => {
  return options.map((e) => {
    return { key: e, value: e }
  })
}

export function useSwipe(onSwipeLeft?: any, onSwipeRight?: any, rangeOffset = 4) {

  let firstTouch = 0
  
  // set user touch start position
  function onTouchStart(e: any) {
      firstTouch = e.nativeEvent.pageX
  }

  // when touch ends check for swipe directions
  function onTouchEnd(e: any){

      // get touch position and screen size
      const positionX = e.nativeEvent.pageX
      const range = windowWidth / rangeOffset

      // check if position is growing positively and has reached specified range
      if(positionX - firstTouch > range){
          onSwipeRight && onSwipeRight()
      }
      // check if position is growing negatively and has reached specified range
      else if(firstTouch - positionX > range){
          onSwipeLeft && onSwipeLeft()
      }
  }

  return {onTouchStart, onTouchEnd};
}

export const categoryName = (category: string) => {
  let key = 'contractCategory.' + category
  let value = t(key)

  return key === value ? category : value
}

export const productName = (symbol: string) => {
  let key = 'product.' + symbol
  let value = t(key)

  return key === value ? symbol : value
}

export const displayContractCode = (symbol: string) => {
  if (symbol) {
    return symbol.indexOf(".") > 0 ? symbol.split(".", 1).toString() : symbol;
  }
}

export const timestampToDateWithFormat = (timestamp: number, format: string = '') => {
  const state: State = store.getState()
  //const timeZone = state.state.setting.TimeZone ?? 'Hongkong'/*'America/Los_Angeles'*/
  const timeZone = RNLocalize.getTimeZone();
  
  return moment.tz(timestamp * 1000, timeZone).format(format)
}

export const isEmail = (str) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
  console.debug(str, reg.test(str))
  return reg.test(str);
}

export const validEmailCharacters = (str: string) => {
  if (str.length <= 0) {
      return false;
  }
  return (/^[@._a-zA-Z0-9-]+$/.test(str));
}

export const hasInValidCharacters = (str: string) => {
  if (str.length <= 0) {
      return false;
  }
  return !(/^[~`!@#$%^&*()_+=[\]\{}|;':",.\/<>?a-zA-Z0-9-]+$/.test(str));
}

export const validNumber = (str: string) => {
  if (str.length <= 0) {
      return true;
  }
  return (/^[0-9]+$/.test(str));
}