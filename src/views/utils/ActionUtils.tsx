import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import { useTranslation } from 'react-i18next';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { AccountUtils } from '../../utils/accountUtils';
import { setChartURL, setTradingURL } from '../../utils/api/apiSetter';
import { store } from '../../redux/store';
import { State } from '../../redux/root-reducer';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';

export const ActionUtils = () => {
    const [t, i18n] = useTranslation();
    const dispatch = useDispatch();

    const { SetAccountDisplayName, SetAccountDetail } = bindActionCreators(OpenPositionsActionCreators, dispatch)
    const { SetIsShowLoading, SetUserId, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
    
    const { doSwitchAccount, doConnectWebsocket, doCloseWebsocket, signOut, registerDemo, doLogin } = AccountUtils();

    return {
        onPressRegisterDemo : async(Route: Function) =>{
            SetSwitchingConnection(true)
            SetIsShowLoading(true);
            try
            {
                const state: State = store.getState();
                const region = state.state.setting?.UserRegion

                var result = await registerDemo(region)
                if (result) {
                    var data = await doSwitchAccount(result.Account, true, result.Entity)
                    if (data && data.success) {
                        await doCloseWebsocket(true);

                        Route()
                        
                        console.debug("register demo account: " + data.paths)
                        const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, true);
                        if (status === 101)
                        {
                            SetAccountDisplayName(data.payload.currentTrader);
                            setTradingURL(data.payload.tApiPath);
                            setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                        }
                        
                    }
                    else {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                    }
                }
            }
            finally
            {
                SetSwitchingConnection(false)
                SetIsShowLoading(false);
            }
        },
        onPressExitDemo : async (Route: Function) => {
            SetSwitchingConnection(true)
            SetIsShowLoading(true);

            const state: State = store.getState();
            const tradingAccountList = state.account.tradingAccountList;
            const region = state.state.setting?.UserRegion
            const setting = state.state.setting
            const stateLoginLevel = state.account.account?.LoginLevel ?? 1

            try{
                if (stateLoginLevel === 3 && tradingAccountList.length > 0) {
                    console.log("onPressExitDemo stateLoginLevel: " + stateLoginLevel + " " + tradingAccountList[0]?.Account)
                    var data = await doSwitchAccount(tradingAccountList[0]?.Account, tradingAccountList[0]?.isDemo, tradingAccountList[0]?.Entity)
                    if (data && data.success) {
                        await doCloseWebsocket(true);

                        Route();

                        const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, tradingAccountList[0]?.isDemo);
                        if (status === 101)
                        {
                            SetAccountDisplayName(data.payload.currentTrader);
                            setTradingURL(data.payload.tApiPath);
                            setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                        }
                    }
                    else {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                    }
                    
                } else {
                    var data = await doLogin(setting.PushyDeviceID ?? "", region, true, true)
                    if (data && data.success) {
                        await doCloseWebsocket(true);
                        SetAccountDetail(null)

                        Route();

                        const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                        if (status === 101)
                        {
                            SetAccountDisplayName(data.payload.currentTrader);
                            setTradingURL(data.payload.tApiPath);
                            setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                        }
                    }
                    else {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                    }
                }
            }
            finally
            {
                SetIsShowLoading(false);
                SetSwitchingConnection(false)
            }
        },
        onPressSwitchAccount: async(account: string, entity: string) => {
            SetSwitchingConnection(true)
            SetIsShowLoading(true);
            try
            {
                var data = await doSwitchAccount(account, false, entity)
                if (data && data.success) {
                    await doCloseWebsocket(true);
                    const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                    if (status === 101)
                    {
                        SetAccountDisplayName(data.payload.currentTrader);
                        setTradingURL(data.payload.tApiPath);
                        setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                    }
                }
                else {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                }
            }
            finally
            {
                SetSwitchingConnection(false)
                SetIsShowLoading(false);
            }
        }
    
    };
    
};
