import * as React from "react"
import { Pressable, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { State } from "../../redux/root-reducer";
import { StateActionCreators } from "../../redux/state/actionCreator";

const TouchableOpacityComponent = (props) => {

    const dispatch = useDispatch();
    const { SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    // const onTouch = useSelector((state: State) => state.state.onTouch);

    return (
        <TouchableOpacity {...props} onPress={()=>{
            if (props && props.onPress) {
                props.onPress();
            }
            SetOnTouch()
        }}/>
    )
}

export default TouchableOpacityComponent
