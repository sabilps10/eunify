import React, { useState, useEffect, useRef, ReactNode } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    Animated,
    PanResponder,
    Platform,
    StatusBar,
    ListRenderItem,
    StyleProp,
    ViewStyle,
    TextStyle,
} from 'react-native';
import {TabView, TabBar, Route} from 'react-native-tab-view';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { PortfolioTabType } from '../../redux/state/types';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const TabBarHeight = 55;
const TabBar2Height = 55;
var HeaderHeight = 250;
const HeaderDefaultHeight = 250;
const HeaderButtonHeight = 310;
const SafeStatusBar = Platform.select({
    ios: 44,
    android: StatusBar.currentHeight,
});
const tab1ItemSize = (windowWidth - 30) / 2;
const tab2ItemSize = (windowWidth - 40) / 3;

interface HeaderScrollableTabViewInterface {
    header: ReactNode,
    routes: Route[],
    onTabPress?: (key: string) => void
    tabBarFilter: ReactNode,
    dataSource: {data: any[], renderItem: ({item, index}) => void}[],
    flatListProps?: {
        contentContainerStyle?: StyleProp<ViewStyle>,
    },
    tabProps?: {
        style?: StyleProp<ViewStyle>,
        titleStyle?: StyleProp<TextStyle>,
        indicatorStyle?: StyleProp<TextStyle>,
    },
    onEndReached,
    isSyncScrollOffset: boolean
}

const HeaderScrollableTabView = (params: HeaderScrollableTabViewInterface) => {

    const { header, routes, onTabPress, tabBarFilter, dataSource, flatListProps, tabProps, onEndReached, isSyncScrollOffset } = params

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const portfolioCurrentTab = useSelector((state: State) => state.state.portfolioCurrentTab ?? PortfolioTabType.Position);

    useEffect(() => {
        if (isDemoAccount && stateLoginLevel === 2) {
            HeaderHeight = HeaderButtonHeight
        } else {
            HeaderHeight = HeaderDefaultHeight
        }
    }, [isDemoAccount, stateLoginLevel]);

    useEffect(() => {
        //console.debug('isSyncScrollOffset', isSyncScrollOffset)
        syncScrollOffset();
    }, [isSyncScrollOffset]);

    /**
     * stats
     */
    const [tabIndex, setIndex] = useState(0);
    const [canScroll, setCanScroll] = useState(true);
    var canSwitchTab = true;

    /**
     * ref
     */
    const scrollY = useRef(new Animated.Value(0)).current;
    const headerScrollY = useRef(new Animated.Value(0)).current;
    const listRefArr = useRef([]);
    const listOffset = useRef({});
    const isListGliding = useRef(false);
    const headerScrollStart = useRef(0);
    const _tabIndex = useRef(0);

    /**
     * PanResponder for header
     */
    const headerPanResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,
            onStartShouldSetPanResponder: (evt, gestureState) => {
                headerScrollY.stopAnimation();
                syncScrollOffset();
                return false;
            },

            onMoveShouldSetPanResponder: (evt, gestureState) => {
                headerScrollY.stopAnimation();
                return Math.abs(gestureState.dy) > 5;
            },

            onPanResponderRelease: (evt, gestureState) => {
                syncScrollOffset();
                if (Math.abs(gestureState.vy) < 0.2) {
                    return;
                }
                headerScrollY.setValue(scrollY._value);
                Animated.decay(headerScrollY, {
                    velocity: -gestureState.vy,
                    useNativeDriver: true,
                }).start(() => {
                    syncScrollOffset();
                });
            },
            onPanResponderMove: (evt, gestureState) => {
                listRefArr.current.forEach((item) => {
                    if (item.key !== routes[_tabIndex.current].key) {
                        return;
                    }
                    if (item.value) {
                        item.value.scrollToOffset({
                            offset: -gestureState.dy + headerScrollStart.current,
                            animated: false,
                        });
                    }
                });
            },
            onShouldBlockNativeResponder: () => true,
            onPanResponderGrant: (evt, gestureState) => {
                headerScrollStart.current = scrollY._value;
            },
        }),
    ).current;

    /**
     * PanResponder for list in tab scene
     */
    const listPanResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,
            onStartShouldSetPanResponder: (evt, gestureState) => false,
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                headerScrollY.stopAnimation();
                return false;
            },
            onShouldBlockNativeResponder: () => true,
            onPanResponderGrant: (evt, gestureState) => {
                headerScrollY.stopAnimation();
            },
        }),
    ).current;

    /**
     * effect
     */
    useEffect(() => {
        scrollY.addListener(({ value }) => {
            const curRoute = routes[tabIndex].key;
            listOffset.current[curRoute] = value;
        });

        headerScrollY.addListener(({ value }) => {
            listRefArr.current.forEach((item) => {
                if (item.key !== routes[tabIndex].key) {
                    return;
                }
                if (value > HeaderHeight || value < 0) {
                    headerScrollY.stopAnimation();
                    syncScrollOffset();
                }
                if (item.value && value <= HeaderHeight) {
                    item.value.scrollToOffset({
                        offset: value,
                        animated: false,
                    });
                }
            });
        });
        return () => {
            scrollY.removeAllListeners();
            headerScrollY.removeAllListeners();
        };
    }, [routes, tabIndex]);

    useEffect(() => {
        console.debug('HeaderScrollableTabView', portfolioCurrentTab)
        if (portfolioCurrentTab === PortfolioTabType.Position) {
            // if (_tabIndex.current !== 0) {
                setIndex(0)
            // }
        }
        else if (portfolioCurrentTab === PortfolioTabType.Order) {
            // if (_tabIndex.current !== 1) {
                setIndex(1)
            // }
        }
        else if (portfolioCurrentTab === PortfolioTabType.History) {
            // if (_tabIndex.current !== 2) {
                setIndex(2)
            // }
        }
    }, [portfolioCurrentTab]);

    /**
     *  helper functions
     */
    const syncScrollOffset = () => {
        var list = ['Order', 'History'];

        if (portfolioCurrentTab === PortfolioTabType.Position){
            list = ['Order', 'History'];
        } else if (portfolioCurrentTab === PortfolioTabType.Order){
            list = ['Position', 'History'];
        } else if (portfolioCurrentTab === PortfolioTabType.History){
            list = ['Position', 'Order'];
        }
        var paddingTop = (isDemoAccount && stateLoginLevel === 2 ? HeaderButtonHeight : HeaderHeight) + TabBarHeight + TabBar2Height

        listRefArr.current.forEach((item) => {
            if (list.includes(item.key)) {
                if (scrollY._value < paddingTop && scrollY._value >= 0) {
                    if (item.value) {
                        item.value.scrollToOffset({
                            offset: scrollY._value,
                            animated: false,
                        });
                        listOffset.current[item.key] = scrollY._value;
                    }
                } else if (scrollY._value >= paddingTop) {
                    if (
                        listOffset.current[item.key] < paddingTop ||
                        listOffset.current[item.key] == null
                    ) {
                        if (item.value) {
                            item.value.scrollToOffset({
                                offset: paddingTop,
                                animated: false,
                            });
                            listOffset.current[item.key] = paddingTop;
                        }
                    }
                }
            }
        });
    };

    const onMomentumScrollBegin = () => {
        isListGliding.current = true;
        canSwitchTab = false
    };

    const onMomentumScrollEnd = () => {
        isListGliding.current = false;
        syncScrollOffset();
        canSwitchTab = true
    };

    const onScrollEndDrag = () => {
        syncScrollOffset();
    };

    /**
     * render Helper
     */
    const renderHeader = () => {
        const y = scrollY.interpolate({
            inputRange: [0, HeaderHeight],
            outputRange: [0, -HeaderHeight],
            extrapolate: 'clamp',
        });
        return (
            <Animated.View
                {...headerPanResponder.panHandlers}
                style={[styles.header, { height: (isDemoAccount && stateLoginLevel === 2 ? HeaderButtonHeight : HeaderHeight), transform: [{ translateY: y }] }]}>
                {header}
            </Animated.View>
        );
    };

    const renderLabel = ({ route, focused }) => {
        return (
            <View style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
                <Text style={[styles.label, { opacity: focused ? 1 : 0.5 }, tabProps?.titleStyle, {alignSelf: 'center', justifyContent: 'center'}]}>
                    {route.title}
                </Text>
            </View>
        );
    };

    const renderScene = ({ route }) => {
        const focused = route.key === routes[tabIndex].key;
        var contentContainerStyle = flatListProps.contentContainerStyle
        return (
            <Animated.FlatList
                // scrollEnabled={canScroll}
                {...listPanResponder.panHandlers}
                ref={(ref) => {
                    if (ref) {
                        const found = listRefArr.current.find((e) => e.key === route.key);
                        if (!found) {
                            listRefArr.current.push({
                                key: route.key,
                                value: ref,
                            });
                        }
                    }
                }}
                scrollEventThrottle={16}
                onScroll={
                    focused
                        ? Animated.event(
                            [
                                {
                                    nativeEvent: { contentOffset: { y: scrollY } },
                                },
                            ],
                            { useNativeDriver: true },
                        )
                        : null
                }
                onMomentumScrollBegin={onMomentumScrollBegin}
                onScrollEndDrag={onScrollEndDrag}
                onMomentumScrollEnd={onMomentumScrollEnd}
                contentContainerStyle={contentContainerStyle}
                showsHorizontalScrollIndicator={false}
                data={dataSource[tabIndex].data}
                renderItem={dataSource[tabIndex].renderItem}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={onEndReached}
            />
        );
    };

    const renderTabBar = (props) => {
        const y = scrollY.interpolate({
            inputRange: [0, HeaderHeight],
            outputRange: [HeaderHeight, 0],
            extrapolate: 'clamp',
        });
        return (
            <Animated.View
                style={{
                    top: 0,
                    zIndex: 1,
                    position: 'absolute',
                    transform: [{ translateY: y }],
                    width: '100%',
                }}>
                <TabBar
                    {...props}
                    onTabPress={({ route, preventDefault }) => {
                        if (canSwitchTab) {
                            if (isListGliding.current) {
                                preventDefault();
                            }
                            onTabPress(route.key)
                        } else {
                            preventDefault();
                        }
                    }}
                    style={[styles.tab, tabProps?.style]}
                    renderLabel={renderLabel}
                    indicatorStyle={[styles.indicator, tabProps?.indicatorStyle]}
                />
                {tabBarFilter}
            </Animated.View>
        );
    };

    const renderTabView = () => {
        return (
            <TabView
                onSwipeStart={() => setCanScroll(false)}
                onSwipeEnd={() => setCanScroll(true)}
                onIndexChange={(id) => {
                    _tabIndex.current = id;
                    // setIndex(id);
                }}
                navigationState={{ index: tabIndex, routes }}
                renderScene={renderScene}
                renderTabBar={renderTabBar}
                initialLayout={{
                    height: 0,
                    width: windowWidth,
                }}
                swipeEnabled={false}
            />
        );
    };

    return (
        <View style={styles.container}>
            {renderTabView()}
            {renderHeader()}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: HeaderHeight,
        width: '100%',
        position: 'absolute',
    },
    label: { fontSize: 16, color: '#222' },
    tab: {
        elevation: 0,
        shadowOpacity: 0,
        backgroundColor: '#FFCC80',
        height: TabBarHeight,
    },
    indicator: { backgroundColor: '#222' },
});

export default React.memo(HeaderScrollableTabView);