import React, { useEffect, useState, useLayoutEffect, useMemo, ReactNode, useCallback } from 'react'
import { BottomTabProps } from '../../routes/route';
import { Text, View, Pressable, useColorScheme, StyleSheet, Linking, Dimensions } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';

import { useTranslation } from 'react-i18next';

import Header from '../../components/header/header.component';
import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import TextLabel from '../../components/forms/labels/textLabel.component';
import MeSettingButton from '../../components/me/meSettingButtons.component';
import BigButton from '../../components/buttons/bigButton.component';

import { AccountUtils } from '../../utils/accountUtils';
import MeAccountButton from '../../components/me/meAccountButtons.component';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { Auth } from 'aws-amplify';
import { logout, lv3Registration } from '../../utils/api/userApi';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { maskPhoneFull } from '../../utils/stringUtils';
import MediumButton from '../../components/buttons/mediumButton.component';
import ArrowRight from '../../../assets/svg/ArrowRight';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import ProfilePic from '../../../assets/svg/ProfilePic';
import MeSetting from '../../../assets/svg/MeSetting';
import MeMyReward from '../../../assets/svg/MeMyReward';
import MeDemoTrade from '../../../assets/svg/MeDemoTrade';
import MeBackCard from '../../../assets/svg/MeBackCard';
import MeLogo from '../../../assets/svg/MeLogo';
import Edit from '../../../assets/svg/Edit';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import { THEME_TYPE } from '../../types/types';
import { format } from 'react-string-format';
import { getApplicationName } from 'react-native-device-info';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  getIndividual,
  getPersonMessage,
} from '../../utils/api/apiAccountOpening';
import {AccountOpeningCreators} from '../../redux/accountOpening/actionCreator';
import {CognitoActionCreators} from '../../redux/cognito/actionCreator';
import {entityId} from '../../config/constants';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { setChartURL, setTradingURL } from '../../utils/api/apiSetter';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { ActionUtils } from '../utils/ActionUtils';
import { MessageDialogContent } from '../../redux/state/types'
import { aoEntry } from '../../utils/navigation';

const MeView: React.FC<BottomTabProps<'Tab_Me'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const dispatch = useDispatch();

    const loginLevel = useSelector((state: State) => state.account.account?.LoginLevel);
    const displayName = useSelector((state: State) => state.account.account?.DisplayName);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const demoTradingAccount = useSelector((state: State) => state.account.demoTradingAccount);
    const symbols = useSelector((state: State) => state.trading.symbols);
    const regionDataList = useSelector((state: State) => state.state.regionDataList)
    const mobileNumber = useSelector((state: State) => state.account.account?.MobileNumber)
    const resetDialog = useSelector((state: state) => state.accountOpening.resetDialog.resetDialogDeposit);
    const region = useSelector((state: State) => state.state.setting?.UserRegion);
    const username = useSelector((state: State) => state.cognitoTemp.username);
    const {currentToken} = useSelector((state: State) => state.cognito);

    const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)
    const { SetIsShowLoading, SetIsShowMessageDialog, SetIsManualLogout } = bindActionCreators(StateActionCreators, dispatch);
    const { SetEnterDemoTab, SetAccountInfo, SetTradingAccountListInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
  const {
    SetPersonalInformation,
    SetContactInformation,
    SetFinancialInformation,
    SetTradingExperience,
    SetIndividual,
    SetTradingAccount,
    SetSelectedTradingAccount,
    SetDepositDialog,
    SetResetDialogDeposit,
  } = bindActionCreators(AccountOpeningCreators, dispatch);
  const {SetAccountDetail} = bindActionCreators(
    OpenPositionsActionCreators,
    dispatch,
  );
  const {height} = Dimensions.get('window');
  const {SetCurrentToken} = bindActionCreators(CognitoActionCreators, dispatch);
  const {individual, selectedTradingAccount: AOTradingAccount} = useSelector(
    (state: State) => state.accountOpening,
  );
  const {Reset} = bindActionCreators(AccountOpeningCreators, dispatch);

    const theme = useSelector((state: State) => state.state.setting?.Theme);
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const styles = makeStyles(colors, height)
    const scheme = useColorScheme();

    const [isLockMyProfile, setIsLockMyProfile] = useState<boolean>(true);
    const [isLockMyReward, setIsLockMyReward] = useState<boolean>(true);
    const [isLockDemoTrade, setIsLockDemoTrade] = useState<boolean>(true);
    const [isLockNotification, setIsLockNotification] = useState<boolean>(true);
    const [isLockBankcard, setIsLockBankcard] = useState<boolean>(true);
    const [isLockMyFavorites, setIsLockMyFavorites] = useState<boolean>(true);
    const [phoneNumber, setPhoneNumber] = useState<string>('');
    // const [isShowLoading, SetIsShowLoading] = useState<boolean>(false);
    const [individualStatus, setIndividualStatus] = useState<string | null>(null);

    const { doSwitchAccount, doConnectWebsocket, doCloseWebsocket, signOut } = AccountUtils();
    const { onPressRegisterDemo, onPressSwitchAccount } = ActionUtils();

    useLayoutEffect(() => {
        let rightComponents: ReactNode[] = [
            <HeaderButton iconComponent={<TopBarSearch color={colors.White} />} onPress={onPressSearch} />,
        ]
        // if (loginLevel === 2 || loginLevel === 3) {
        //     rightComponents.push(<HeaderButton iconComponent={<TopBarNotice color={colors.White} onPress={} />})
        // }
        navigation.setOptions({
            header: () =>
                <Header
                    style={{backgroundColor: colors.Brand3}}
                    rightComponents={rightComponents}
                />
        });
      }, [navigation, loginLevel]);

    // useEffect(() => {
    //     const fetchData = async () => {
    //         Alert.alert('')
    //         try {
    //             const session = await Auth.currentSession();
    //             var payload: { [key: string]: any; } = session.getIdToken().payload;
    //             console.warn('payload', payload)
    //             var _phoneNumber = payload['phone_number'];
    //             setPhoneNumber(_phoneNumber)
    //         } catch (e) {
    //             console.error('e', e)
    //         }
    //     }
    //     fetchData().catch(console.error);
    // }, []);

    useFocusEffect(
        React.useCallback(() => {
            if (mobileNumber) {
                setPhoneNumber(mobileNumber)
            } else {
                const fetchData = async () => {
                    try {
                        const session = await Auth.currentSession();
                        var payload: { [key: string]: any; } = session.getIdToken().payload;
                        // console.warn('payload', payload)
                        var _phoneNumber = payload['phone_number'];
                        if (_phoneNumber) {
                            setPhoneNumber(_phoneNumber)
                        }
                    } catch (e) {
                        console.error('e', e)
                    }
                }
                fetchData().catch(console.error);
            }
        }, [mobileNumber])
    );

    useEffect(() => {
        if (loginLevel === 2){
            setIsLockMyProfile(false)
            setIsLockMyReward(false)
            setIsLockDemoTrade(false)
            setIsLockNotification(false)
            setIsLockBankcard(true)
            setIsLockMyFavorites(false)
        } else if (loginLevel === 3){
            setIsLockMyProfile(false)
            setIsLockMyReward(false)
            setIsLockDemoTrade(false)
            setIsLockNotification(false)
            setIsLockBankcard(false)
            setIsLockMyFavorites(false)
        } else {
            setIsLockMyProfile(true)
            setIsLockMyReward(true)
            setIsLockDemoTrade(true)
            setIsLockNotification(true)
            setIsLockBankcard(true)
            setIsLockMyFavorites(true)
        }

    }, [loginLevel]);

    useEffect(() => {
        setPhoneNumber(mobileNumber)
    }, [mobileNumber]);

    const onPressLogin = () => {
        navigation.navigate('CognitoLogin')
    }

    // const switchTradeAccount = async () => {
    //     if (demoTradingAccount) {
    //         const session = await Auth.currentSession();
    //         var token = session.getAccessToken().getJwtToken();
    //         var payload: { [key: string]: any; } = session.getIdToken().payload;
    //         var cognitoUserName = payload['cognito:username'];
    //         const data = await switchAccount(cognitoUserName, token, demoTradingAccount.Account!, demoTradingAccount.isDemo ? '0' : '1', demoTradingAccount.Entity);
    //         if (data) {
    //             SetAccountDisplayName(data.currentTrader);
    //             try {
    //             console.debug(`loginLevel: ${data.loginLevel}`);
    //             await startWebsocket(data.mtPath, data.accessToken, data.loginLevel);
    //             } catch (err) {
    //             console.debug(err)
    //             }
    //         }
    //     }
    // }
    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            return symbol.DisplayCategory
        });

        //console.debug(JSON.stringify(categories));

        return [...new Set(categories)];
    }, [symbols])

    const onPressDeposit = () => {
      navigation.push('TabDirectoryWebView', {
        page: TabDirectoryWebViewPage.MIO,
        focusTab: TabDirectoryWebViewTab.ME,
        param: {type: MIOType.Deposit},
      });
    };

    const onPressSearch =() => {
        let searchCategories = categories.flatMap((category) => { return { id: category, title: category } })
        navigation.navigate('SearchProductView', { categories: searchCategories })
    };



    const successRegisterDemoRoute = () => {
        navigation.navigate('TabDirectory', {screen: TabDirectoryTab.QUOTE})
    }

    const onPressToDemoTrade = async() => {
        if (demoTradingAccount) {
            SetSwitchingConnection(true)
            SetEnterDemoTab(TabDirectoryTab.ME)
            SetIsShowLoading(true);
            try
            {
                var data = await doSwitchAccount(demoTradingAccount.Account, demoTradingAccount.isDemo, demoTradingAccount.Entity)
                if (data && data.success) {
                    await doCloseWebsocket(true);
                    console.debug("demo account: " + data.paths)
                    const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, demoTradingAccount.isDemo);
                    if (status === 101)
                    {
                        SetAccountDisplayName(data.payload.currentTrader);
                        setTradingURL(data.payload.tApiPath);
                        setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                    }
                    navigation.navigate('TabDirectory')
                    
                } else {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                }
            }
            finally
            {
                SetSwitchingConnection(false)
                SetIsShowLoading(false);
            }
        }
        else {
            SetIsShowMessageDialog({isShowDialog: true, trackingContext: MessageDialogContent.TrackingContext.ActivateDemoTrade, title: t('demo.activateTitle'), message: format(t('demo.activateDesc'), getApplicationName()), callback: () => onPressRegisterDemo(successRegisterDemoRoute), isShowCancel: true, btnLabel: t('demo.activateButton')})
        }
    }

    const onPressBankCard = async() => {
        // navigation.push('MIO', { type: MIOType.BankManagement })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.ME, param: { type: MIOType.BankManagement }})
    }

    const signOutFunc = async() => {
        SetIsShowLoading(true);
        SetIsManualLogout(true)
        try {
            const session = await Auth.currentSession();
            if (session !== null) {
                var payload: { [key: string]: any; } = session.getIdToken().payload;
                var cognitoUserName = payload['cognito:username'];
                await logout(cognitoUserName)
            } 
        } catch (e) {}
        finally {await signOut()}
        SetIsShowLoading(false);
    }

    const onPressLogout = async() => {
        SetIsShowMessageDialog({isShowDialog: true, trackingContext: MessageDialogContent.TrackingContext.ConfirmLogout, title: t('me.logoutMsg'), message: '', btnLabel:t('common.yes'), callback:signOutFunc, isShowCancel: true})
    }

    const onPressOpenAccount = () => {
      aoEntry(individual, navigation);
    };

    const getIndividualData = async () => {
      const session = await Auth.currentSession();
      const sessionPayload = session.getIdToken().payload;
      

      console.debug(`[MeView getIndividualData] will await getIndividual()`)
     await getIndividual({
        token: currentToken,
        payload: {
          username: sessionPayload['cognito:username'],
        },
        onSuccess: async data => {
          if (data?.response !== null) {
            SetIndividual(data.response);
            setIndividualStatus(data.response.status);
            SetPersonalInformation({
              customerId: data?.response?.id,
              legalLastNameEn: data?.response?.legalLastNameEn,
              legalFirstAndMiddleNameEn:
                data?.response?.legalFirstAndMiddleNameEn,
              legalLastName: data?.response?.legalLastName,
              legalFirstAndMiddleName: data?.response?.legalFirstAndMiddleName,
              gender: data?.response?.gender,
              jurisdiction: data?.response?.jurisdiction,
              dateOfBirth: data?.response?.dateOfBirth,
              jurisdictionOfResidence: data?.response?.jurisdictionOfResidence,
              identificationType: data?.response?.identificationType,
              identificationNo: data?.response?.identificationNo,
              identificationExpiryDate:
                data?.response?.identificationExpiryDate,
              usCitizen: true,
              efsgRewardProgram: data?.response?.efsgRewardProgram,
            });
            SetContactInformation({
              customerId: data?.response?.id,
              addressLine1: data?.response?.addressLine1,
              addressLine2: data?.response?.addressLine2,
              city: data?.response?.city,
              province: data?.response?.province,
              postcode: data?.response?.postCode,
              jurisdictionOfResidence: data?.response?.jurisdictionOfResidence,
            });
            SetFinancialInformation({
              customerId: data?.response?.id,
              employmentStatus: data?.response.employmentStatus,
              industrial: data?.response.industrialOther ? `${t('accountOpening.purpose.OTHER')}: ${data?.response.industrialOther}` : data?.response.industrial,
              jobTitle: data?.response.jobTitleOther ? `${t('accountOpening.purpose.OTHER')}: ${data?.response.jobTitleOther}` :  data?.response.jobTitle,
              annualIncome: data?.response.annualIncome,
              liquidNetworth: data?.response.liquidNetworth,
              jurisdictionOfTaxResidence:
                data?.response.jurisdictionOfTaxResidence,
              sourceOfFunds: data?.response.sourceOfFundsOther ? `${t('accountOpening.purpose.OTHER')}: ${data?.response.sourceOfFundsOther}` :  data?.response.sourceOfFunds,
              haveTin: data?.response.haveTin,
              tinNumber: data?.response.tinNumber,
            });
            SetTradingExperience({
              customerId: data?.response?.id,
              fiveOrMorTransactionLastThreeYears:
                data?.response.fiveOrMorTransactionLastThreeYears,
              haveOtherTrade: data?.response.haveOtherTrade,
              tradingExperience: data?.response.yearsOfTrading,
              tradingFrequency: data?.response.frequencyOfTrading,
              accountOpeningPurpose: data?.response.accountOpeningPurposeOther ? `${t('accountOpening.purpose.OTHER')}: ${data?.response.accountOpeningPurposeOther}` : data?.response.purposeOfTrading,
            });
            await getPersonMessage({
              token: currentToken,
              payload: {uuid: data.response.id},
              onSuccess: async res => {
                if (res.result === true && res.response.accountId.length > 0) {
                  SetTradingAccount(res.response.accountId);
                  SetSelectedTradingAccount(res.response.accountId[0]);
                  console.debug(`[MeView getIndividualData] .onSuccess getPersonMessage() .onSuccess ${typeof Number(res.response.accountId[0])}`)
                  // SetAccountDetail({Login: Number(res.response.accountId[0])});
                  if (
                    data.response.status === 'PENDING_DEPOSIT' ||
                    'DEPOSIT_PENDING_APPROVAL' ||
                    'COMPLETED'
                  ) {
                    SetAccountInfo({
                      LoginLevel: 3,
                    });
                  }
                }
              },
              onFailed: error => {
                console.log(error);
              },
            });
            if (data?.response.status === "PENDING_DEPOSIT" && resetDialog === false){
              SetDepositDialog({
                url: '',
                isOpen: 'deposit',
                onPress: onPressDeposit,
              });
              SetResetDialogDeposit({resetDialogDeposit: true})
          }
          }
        },
        onFailed: () => {},
        setLoading: SetIsShowLoading,
      });
    }

    useEffect(() => {
      const fetchToken = async () => {
        const currentSession = await Auth.currentSession();
        const token = currentSession.getAccessToken().getJwtToken();
        SetCurrentToken(token);
      };
      fetchToken();
      // eslint-disable-next-line react-hooks/exhaustive-deps	
    }, [username, currentToken]);

    useFocusEffect(
      useCallback(() => {
      if (loginLevel !== 1 && username && currentToken) {
       getIndividualData();
      }
    },[username,currentToken,loginLevel,individual?.status,resetDialog]))

    const statusLv25 = [
      'AWAITING_RESPONSE',
      'PENDING_VERIFICATION',
      'PENDING_APPROVAL',
    ];

    const statusLv3 = ['PENDING_DEPOSIT', 'DEPOSIT_PENDING_APPROVAL', 'REJECTED', 'COMPLETED','Completed'];

    return (
        <View style={[styles.container, {backgroundColor: colors.Brand3}]}>
            <View style={[(individual && statusLv25.includes(individual?.status) ? styles.topContainerLv25 : ((individual && statusLv3.includes(individual?.status) || tradingAccountList && tradingAccountList.length > 0) ? styles.topContainerLv3 : styles.topContainer)), styles.topContainerShare ]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <ProfilePic style={{width: 22.5, height: 22.5}} color={colors.Brand2} backgroundColor={colors.Brand1} />
                    {
                        loginLevel === 2 || loginLevel === 3 ? 
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{ padding: 5 }} />
                                <Text style={[globalStyles.H4, {color: colors.FixedTitleFont}]}>{displayName}</Text>
                                <View style={{ padding: 5 }} />
                                <PressableComponent onPress={()=> {navigation.navigate('MeSettingUser')}}><Edit color={colors.FixedMainFont} /></PressableComponent>
                            </View>
                        :
                            null
                    }
                </View>
                {
                    loginLevel === 2 || loginLevel === 3 ?
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{ paddingHorizontal: 15 }} />
                            <Text style={{color: colors.FixedTitleFont}}>{'+'+maskPhoneFull(phoneNumber, regionDataList)}</Text>
                        </View>
                    : 
                        null
                }
                <View style={{marginTop: '3%'}}>
                  { tradingAccountList && tradingAccountList.length > 0 && (
                    <>
                      <Text style={[{marginBottom: 10}, globalStyles.H4]}>
                        {t('me.myAccounts')}
                      </Text>
                      {
                      tradingAccountList.map(item => {
                          return (
                              <View>
                                  <View style={{ padding: 5 }} />
                                  <MeAccountButton 
                                      statusComponent={
                                          individual?.status === 'REJECTED' ? (
                                              <View
                                                  style={{
                                                      backgroundColor: colors.Red,
                                                      borderRadius: 100,
                                                      marginRight: 10,
                                                      padding: 3,
                                                  }}>
                                                  <Icon
                                                      name="exclamation-thick"
                                                      color={colors.White}
                                                      style={{fontSize: 15}}
                                                  />
                                              </View>
                                          ) : null
                                      }
                                      accountName={item.Name} 
                                      tradeAccount={item.Account} 
                                      access={()=> {onPressSwitchAccount(item.Account, item.Entity)}} 
                                      isDefault={item.isDefault} />
                              </View>
                          )
                      })
                      }
                    </>
                  )}
                  {individual && individual?.status !== "DRAFT" && 
                  <View
                    style={{
                      marginTop: 10,
                      justifyContent: 'flex-end',
                      alignItems: 'flex-end',
                    }}>
                    <Pressable onPress={() => navigation.navigate('AccountStatus')}>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text>{t('accountOpening.me_account_status')}</Text>
                        <Icon
                          name={'chevron-right'}
                          color={colors.White}
                          style={{
                            fontSize: 25,
                          }}
                        />
                      </View>
                    </Pressable>
                  </View>}
                </View>
            

            </View>
            <View style={[(individual && statusLv25.includes(individual?.status) ? styles.bottomContainerLv25 : ((individual && statusLv3.includes(individual?.status) || tradingAccountList && tradingAccountList.length > 0) ? styles.bottomContainerLv3 : styles.bottomContainer)), styles.bottomContainerShare, {backgroundColor: colors.MainBg}]}>
                {
                    loginLevel === 3 || (individual && individual?.status !== 'DRAFT' && individual?.status !== 'REJECTED') ?
                        null 
                    :
                        <View style={{paddingTop: 25}}>
                            <PressableComponent 
                                onPress={()=>{loginLevel === 2 ? onPressOpenAccount() : navigation.navigate('CognitoSignUp')}} 
                                style={[styles.button, { backgroundColor: colors.Brand1, flexDirection: 'row' }]} >
                                <Text style={[globalStyles.ButtonText, { color: colors.FixedEnableBtnFont, alignSelf: 'center' }]}>
                                    {loginLevel === 2 ? t('me.openRealAccount') : t('me.signup')}
                                </Text>
                                <View style={styles.arrow}>
                                    <ArrowRight color={colors.MainFont} />
                                </View>
                            </PressableComponent>
                        </View>
                }
                
                <View style={[styles.settingContainer, {backgroundColor: colors.WrapperBg}]}>
                    <View style={styles.settingRow}>
                        <MeSettingButton 
                            btn1Title={t('me.setting')} 
                            btn1Func={()=>{navigation.navigate('MeSetting')}} 
                            source={<MeSetting color={colors.Brand3} />}
                        />
                        <MeSettingButton 
                            btn1Title={t('me.myReward')} 
                            btn1Func={()=>{navigation.navigate('MeMyRewardScreen')}} 
                            source={<MeMyReward color={isLockMyReward ? colors.Grey3 : colors.Brand3} />}
                            isLock={isLockMyReward} 
                        />
                        <MeSettingButton 
                            btn1Title={t('me.demoTrade')} 
                            btn1Func={onPressToDemoTrade} 
                            source={<MeDemoTrade color={isLockDemoTrade ? colors.Grey3 : colors.Brand3} />}
                            isLock={isLockDemoTrade} 
                        />
                    </View>
                    <View style={{ padding: 15 }} />
                    <View style={styles.settingRow}>
                        <MeSettingButton 
                            btn1Title={t('me.bankCard')} 
                            btn1Func={onPressBankCard} 
                            source={<MeBackCard color={isLockBankcard ? colors.Grey3 : colors.Brand3} />}
                            isLock={isLockBankcard} />
                        {/* <MeSettingButton btn1Title={t('me.help')} btn1Func={()=>{}} source={getIcon_setting_help(scheme)} soruceLock={getIcon_setting_help(scheme)}/> */}
                        <MeSettingButton 
                            btn1Title={t('me.aboutUs')} 
                            btn1Func={()=>{navigation.navigate('MeAboutUsScreen')}} 
                            source={<MeLogo color="white" />}
                        />
                    </View>
                </View>
            </View>
            <View style={[styles.bottomButtonContainer, {backgroundColor: colors.MainBg}]}>
                {
                    loginLevel === 2 || loginLevel === 3 ?
                        <MediumButton 
                            style={{width: '50%', alignSelf: 'center'}} 
                            title={t('me.logout')} 
                            type='Outline' 
                            onPress={onPressLogout} />
                    :
                        <BigButton
                            textStyle={{color: colors.FixedMainFont}}
                            type='Solid'
                            title={t('me.login')}
                            onPress={onPressLogin} />
                }
            </View>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE,height: number) => StyleSheet.create({
    container: {
        flex: 1
    },
    topContainer: {
        flex: 0.1,
    },
    topContainerLv25: {
        flex: height < 700 ? 0.25 : 0.2,
   },
    topContainerLv3: {
        flex: height < 700 ? 0.35 : 0.3,
    },
    topContainerShare: {
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },
    bottomContainer: {
        flex: 0.9,
    },
    bottomContainerLv25: {
        flex: height < 700 ? 0.75 : 0.8,
  },
    bottomContainerLv3: {
        flex: height < 700 ? 0.65 : 0.7,
    },
    bottomContainerShare: {
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        alignItems: 'center'
    },
    bottomButtonContainer: {
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        paddingBottom: 12
    },
    value: {
        width: 170,
        height: 148,
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 40,
        lineHeight: 48,
        textAlign: 'center'
    },
    button: {
        alignSelf: 'flex-start',
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 25,
        backgroundColor: colors.Brand1, 
        flexDirection: 'row', 
        alignItems: 'center',
    },
    settingContainer: {
        padding: 10,
        marginTop: 25,
        width: '100%',
        borderRadius: 12
    },
    settingRow: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center'
    },
    settingItem: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
})

export default MeView