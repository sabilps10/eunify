import React, {useState, useLayoutEffect, useEffect} from 'react';
import { StyleSheet, useColorScheme, View, Pressable, ScrollView } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import TextCounter from '../../components/forms/labels/textCounter.component';
import Header from '../../components/header/header.component';

import { Auth } from 'aws-amplify';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { isEmpty, maskEmail, maskPhone } from '../../utils/stringUtils';
import { AccountUtils } from '../../utils/accountUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resendCodeSecond } from '../../config/constants';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import InputDropDownList from '../../components/forms/inputs/inputDropdownList.component';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { InputType } from '../../types/InputTypes';
import { State } from '../../redux/root-reducer';
import { CountryOptionType } from '../../components/countryOptionView/countryOptionView';
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import TopBarBack from '../../../assets/svg/TopBarBack';
import HeaderButton from '../../components/header/headerButton.component';
import { allowSkipSMS } from '../../../build_config/codePush/src/config/constants';
import { format } from 'react-string-format';
import PressableComponent from '../utils/PressableComponent';
import { validNumber } from '../utils/CommonFunction';
import analytics from '@react-native-firebase/analytics'
import GAScreenName from '../../json/GAScreenName.json'

const Tab = createMaterialTopTabNavigator();

const enum VerificationStage {
    VerifyCodeEmail,
    NewMobileNumber,
    VerifyCodeNewPhoneNumber,
}

const MeChangeMobileNumber: React.FC<Props<'MeChangeMobileNumber'>> = ({ route, navigation }) => {
	
    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()
    
    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />} 
                    title={t('meSetting.changeMobileNumber.title')} 
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                    onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const regionDataList = useSelector((state: State) => state.state.regionDataList);

    const [verStage, setVerStage] = useState<VerificationStage>(VerificationStage.VerifyCodeEmail);

    const [displayEmail, setDisplayEmail] = useState<string>('');
    // const [emailCode, setEmailCode] = useState<string>('');
    const [emailCode, setEmailCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });
  
    // const selectionOptions = ["852", "853"];
    const [phonePrefix, SetPhonePrefix] = useState<string>(regionDataList[0].countryCode);
    // const [phoneNumber, SetPhoneNumber] = useState<string>('');
    const [phoneNumber, setPhoneNumber] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 20, isValid: false });
    const [isCheckPhone, setIsCheckPhone] = useState<boolean>(false);
    const [isPhoneCodeSend, setIsPhoneCodeSend] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetIsShowLoading, SetIsShowToastDialog } = bindActionCreators(StateActionCreators, dispatch);

    const { updateProfileMobile } = AccountUtils();

    const onPressBackBtn = () => { navigation.goBack() }

    const onSelectCountryCodeOption = (value: string) => {
        console.debug('onSelectCountryCodeOption', value)
        // formDispatch({ type: "UPDATE_FORM", data: { name: "phonePrefix", value: value } })
        SetPhonePrefix(value)
        setIsCheckPhone(!isCheckPhone)
      }

    const onPressShowAll = () => {
        // OpenCloseSelection({
        //     selectedOption: phonePrefix,
        //     selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
        //     isOpen: true,
        //     location: 'forgotPasswordPhonePrefix'
        // })
        navigation.navigate('CountryOptionView', {type: CountryOptionType.COUNTRY, onSelect: onSelectCountryCodeOption, selectedId: phonePrefix})
    }

    const resendEmailCode = async () => {
        try {
            if (isPhoneCodeSend || verStage === VerificationStage.VerifyCodeEmail) {
                const user = await Auth.currentAuthenticatedUser();
                await Auth.verifyUserAttribute(user, verStage === VerificationStage.VerifyCodeEmail ? 'email' : 'phone_number');
            } else {
                await verifyUserAttribute()
                setIsPhoneCodeSend(true)
            }
        } catch (error) {
          console.debug('error resending code: ', error);
        //   Alert.alert(error.toString())
        }
      };

    const callbackEmail = () => {
        resendEmailCode()
      };

    const toPhoneCode = async() => {
        SetIsShowLoading(true)
        if (phoneNumber.value.length > 0 && phoneNumber.value.length < 8) {
            setPhoneNumber({ ...phoneNumber, error: t("error.mobileLength8"), isValid: false})
            SetIsShowLoading(false)
            return
        }

        var cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;
        if (await userExist(cognitoPhoneNumber)) {
            setPhoneNumber({ ...phoneNumber, error: t("error.mobileInUse"), isValid: false})
            SetIsShowLoading(false)
            return
        } else {
            setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
        }

        setVerStage(VerificationStage.VerifyCodeNewPhoneNumber)
        // 06-004-07 Change Mobile Number
        setEmailCode({...emailCode, value: '', isValid: true, error: ''})
        SetIsShowLoading(false)
    }
    
    const verifyUserAttribute = async () => {
        const user = await Auth.currentAuthenticatedUser();
        const cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;        
        Auth.updateUserAttributes(user, {
            'phone_number': cognitoPhoneNumber
        }).then((res) => {
            console.debug('updateResult' + res + cognitoPhoneNumber)            
        }).catch((error) => {
            console.error(error) // hitting error here 
        })
    };

    const verifyUserAttributeConfirm = async (type: string) => {
        SetIsShowLoading(true)
        const user = await Auth.currentAuthenticatedUser();
        var verify = false;
        await Auth.verifyUserAttributeSubmit(user, type, emailCode.value).then((res) => {
            console.debug(res) 
            verify = true;
        }).catch((error) => {
            console.error(error) // hitting error here 
            // if (type === 'email') {
                setEmailCode({...emailCode, isValid: false, error: t('meSetting.changeMobileNumber.error.incorrectEmailCode')})
            // } 
        }).finally(async()=>{
            if (verify) {
                if (type === 'email') {
                    setVerStage(VerificationStage.NewMobileNumber)
                    // 06-004-06 Change Mobile Number
                    SetIsShowLoading(false)
                }
                else {
                    SetIsShowLoading(false);
                    SetIsShowToastDialog({
                        isShowDialog: true,
                        message: t('meSetting.changeMobileNumber.success'),
                        isSuccess: true
                    })
                    SetAccountInfo({
                        MobileNumber: '+' + phonePrefix + phoneNumber.value
                    })
                    SetIsShowLoading(true);
                    //Call EMP API
                    await updateProfileMobile(phoneNumber.value, phonePrefix)
                    onPressBackBtn()
                    SetIsShowLoading(false);
                }
            } else {
                SetIsShowLoading(false);
            }
            
        })
    };

    useEffect(()=> {
        const fetchData = async () => {
            const session = await Auth.currentSession();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var email = payload['email'];
            setDisplayEmail(maskEmail(email))
            resendEmailCode()
        }
        
        fetchData().catch(console.error);
       
    }, [])

    const userExist = async(user: string) => {
        return await Auth.signIn(user, '123').then(res => {
          return false;
        }).catch(error => {
          const code = error.code;
          console.debug('userExist ', user, error);
          switch (code) {
            case 'UserNotFoundException':
              return false;
            case 'NotAuthorizedException':
              return true;
            case 'PasswordResetRequiredException':
              return false;
            case 'UserNotConfirmedException':
              return false;
            default:
              return false;
          }
        });
      }

    //   useEffect(() => {
    //     const fetchData = async () => {
    //         if (verStage === 1) {
    //             if (phoneNumber.value.length > 0 && phoneNumber.value.length < 8) {
    //                 setPhoneNumber({ ...phoneNumber, error: t("error.mobileLength8"), isValid: false})
    //                 return
    //             }

    //             var cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;
    //             if (await userExist(cognitoPhoneNumber)) {
    //                 setPhoneNumber({ ...phoneNumber, error: t("error.mobileInUse"), isValid: false})
    //             } else {
    //                 setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
    //             }
    //         }
    //     }
        
    //     fetchData().catch(console.error);
    
    //   }, [isCheckPhone]);

    // iOS 14
    const allowTrack = useSelector((state: State) => state.state.allowTrack)
    // GA4 Tracking ScreenView
    useEffect(() => {
        if (allowTrack && verStage) {
            const getScreenClassFor = (stage: VerificationStage) => {
                switch (stage) {
                    case VerificationStage.VerifyCodeEmail: return 'MeChangeMobileNumber+AWSAmplifyAuthVerifyUserAttributeSubmitConfirmationCodeEmail'
                        break;
                    case VerificationStage.NewMobileNumber: return 'MeChangeMobileNumber+NewMobileNumber'
                        break;
                    case VerificationStage.VerifyCodeNewPhoneNumber: return 'MeChangeMobileNumber+AWSAmplifyAuthVerifyUserAttributeSubmitConfirmationCodeNewPhoneNumber'
                        break;
                }
            }
            let screenName: string, screenClass: string
            if ((screenClass = getScreenClassFor(verStage)) && (screenName = GAScreenName[screenClass]) && screenName.length !== 0 && screenName.trim() !== '') {
                analytics().logScreenView({
                    screen_name: screenName,
                    screen_class: screenClass
                })
                console.debug(`[MeChangeMobileNumber useEffect [verStage, allowTrack]] analytics().logScreenView(${JSON.stringify({screen_name: screenName, screen_class: screenClass})})`)
            }
        }
    }, [verStage, allowTrack])

    return (
        <ScrollView style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg, paddingBottom: insets.bottom}]}
        keyboardShouldPersistTaps='handled'>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
                {verStage === VerificationStage.VerifyCodeEmail && 
                    <View>
                        <Input
                            label={t('meSetting.changeMobileNumber.verificationMessage')}
                            value={emailCode}
                            onChangeText={(value) => {
                                if (validNumber(value)){
                                    setEmailCode({...emailCode, value})
                                }
                            }}
                            rightComponent={<TextCounter counter={resendCodeSecond} callback={callbackEmail}/>}
                            type='number'
                        />
                        <View style={{ padding: 5 }} />
                        <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), displayEmail)} labelStyle={globalStyles.Small_Note}/>

                        <View style={{ padding: 30 }} />
                        <ActionButton
                        title={t('common.next')}
                        onPress={()=> {verifyUserAttributeConfirm('email')}}
                        isEnable={!isEmpty(emailCode.value) && emailCode.value.length === 6}
                        />
                    </View>
                }
                {verStage === VerificationStage.NewMobileNumber && 
                    <View>
                        {/* <View>
                            <TextLabel label={t('meSetting.changeMobileNumber.newMobileNumber')}/>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Pressable onPress={onPressShowAll}>
                                <InputDropDownList
                                    value={phonePrefix}
                                    />
                                </Pressable>
                                <View style={{ width: 10 }} />
                                <Input
                                style={{ flex: 1 }}
                                placeholder=''
                                value={phoneNumber}
                                onChangeText={(value) => {SetPhoneNumber({...phoneNumber, value})}}
                                type='phone'
                                />
                            </View>
                        </View> */}
                        <TextLabel label={t('meSetting.changeMobileNumber.newMobileNumber')} />
                        <View style={{ flexDirection: 'row' }}>
                            <PressableComponent onPress={onPressShowAll}>
                                <InputDropDownList value={phonePrefix} />
                            </PressableComponent>
                            <View style={{ width: 10 }} />
                            <View style={{flex: 1}}>
                            <Input
                                style={{ flex: 1, justifyContent: 'center' }}
                                type='phone'
                                value={phoneNumber}
                                //onChangeText={(value) => { formDispatch({ type: "UPDATE_FORM", data: { name: "phoneNumber", value: value } }) }}
                                //onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: '+' + formState.phonePrefix.value + phoneNumber.value } })}
                                onChangeText={(value) => 
                                    {
                                        let newText = '';
                                        let numbers = '0123456789';
                    
                                        for (var i=0; i < value.length; i++) {
                                            if(numbers.indexOf(value[i]) > -1 ) {
                                                newText = newText + value[i];
                                            }
                                        }
                                        setPhoneNumber({ ...phoneNumber, value: newText, error: '' })
                                    }
                                }
                                onEndEditing={(e) => setIsCheckPhone(!isCheckPhone)}
                            />
                            </View>
                        </View>

                        <View style={{ padding: 30 }} />
                        <ActionButton
                        title={t('common.next')}
                        onPress={toPhoneCode}
                        isEnable={!isEmpty(phoneNumber.value)}
                        />    
                    </View>
                }
                {verStage === VerificationStage.VerifyCodeNewPhoneNumber && 
                    <View>
                        <Input
                            label={t('meSetting.changeMobileNumber.verificationMobile')}
                            value={emailCode}
                            onChangeText={(value) => {
                                if (validNumber(value)){
                                    setEmailCode({...emailCode, value})
                                }
                            }}
                            rightComponent={<TextCounter counter={-1} callback={callbackEmail}/>}
                            type='number'
                        />
                        <View style={{ padding: 5 }} />
                        <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), maskPhone('+' + phonePrefix, phoneNumber.value))} labelStyle={globalStyles.Small_Note}/>

                        <View style={{ padding: 30 }} />
                        <ActionButton
                        title={t('common.confirm')}
                        onPress={()=> {verifyUserAttributeConfirm('phone_number')}}
                        isEnable={!isEmpty(emailCode.value) && emailCode.value.length === 6}
                        />    
                    </View>
                }
               
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100
    },
});

export default MeChangeMobileNumber;