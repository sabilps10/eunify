import React, { useEffect, useLayoutEffect, useState } from 'react';
import { StyleSheet, useColorScheme } from 'react-native';
import Header from '../../components/header/header.component';
import { Props } from '../../routes/route';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';

import MeSettingListButton from '../../components/me/meSettingListButtons.component';

import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import InputSwitch from '../../components/forms/inputs/inputSwitch.component';

import { SafeAreaView } from 'react-native-safe-area-context';
import ActionSheetTick from '../../../assets/svg/ActionSheetTick';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';


const MeSettingNotification: React.FC<Props<'MeSettingNotification'>> = ({ route, navigation }) => {
	const [t, i18n] = useTranslation();

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />} 
                    title={t('meSetting.title') + ' - ' + t('meSetting.notification')} 
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                    onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    const dispatch = useDispatch();
    const { SetSetting } = bindActionCreators(StateActionCreators, dispatch);

    // const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[login] && state.state.setting.TradeSetting[login].IsEnableTradeConfirmationDialog) ?? false);
    
    const [isInApp, SetIsInApp] = useState<boolean>(true);
    const [isPhone, SetIsPhone] = useState<boolean>(true);
    const [isPhoneOrder, SetIsPhoneOrder] = useState<boolean>(true);
    const [isPhoneCommunity, SetIsPhoneCommunity] = useState<boolean>(true);
    const [isPhoneOther, SetIsPhoneOther] = useState<boolean>(true);

    useEffect(()=> {
        if (isPhoneOrder || isPhoneCommunity || isPhoneOther){
            SetIsPhone(true)
        } else {
            SetIsPhone(false)
        }
    }, [isPhoneOrder, isPhoneCommunity, isPhoneOther])

    const onPressPhone = () => { 
        var v = !isPhone
        SetIsPhone(v)
        if (v){
            SetIsPhoneOrder(true)
            SetIsPhoneCommunity(true)
            SetIsPhoneOther(true)
        } else {
            SetIsPhoneOrder(false)
            SetIsPhoneCommunity(false)
            SetIsPhoneOther(false)
        }
    }

    const onPressBackBtn = () => { navigation.goBack() }

    return (
        <SafeAreaView 
            edges={['bottom']}
            style={[styles.container, {backgroundColor: colors.MainBg}]}>
            <MeSettingListButton 
                leftLabel={t('meSetting.notiication.inApp')} 
                rightElement={<ActionSheetTick color={colors.Brand3} />}
            />
            <MeSettingListButton 
                isPrimary={true}
                leftLabel={t('meSetting.notiication.phone')} 
                rightElement={<InputSwitch isRequired={false} value={isPhone} onSwitch={onPressPhone}/>}
            />
            <MeSettingListButton 
                isSecondary={true}
                leftLabel={t('meSetting.notiication.order')} 
                rightElement={<InputSwitch isRequired={false} value={isPhoneOrder} onSwitch={() => {SetIsPhoneOrder(!isPhoneOrder)}}/>}
            />
            <MeSettingListButton 
                isSecondary={true}
                leftLabel={t('meSetting.notiication.community')} 
                rightElement={<InputSwitch isRequired={false} value={isPhoneCommunity} onSwitch={() => {SetIsPhoneCommunity(!isPhoneCommunity)}}/>}
            />
            <MeSettingListButton 
                isSecondary={true}
                leftLabel={t('meSetting.notiication.other')} 
                rightElement={<InputSwitch isRequired={false} value={isPhoneOther} onSwitch={() => {SetIsPhoneOther(!isPhoneOther)}}/>}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
export default MeSettingNotification