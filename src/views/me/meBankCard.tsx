import React, { useLayoutEffect } from "react"
import { SafeAreaView } from "react-native-safe-area-context"
import { Props } from "../../routes/route"
import StackView from "../../components/stackView.component"
import { Pressable, Text } from "react-native"
import { useTheme } from "@react-navigation/native"
import Header from "../../components/header/header.component"
import HeaderButton from "../../components/header/headerButton.component"
import TopBarBack from "../../../assets/svg/TopBarBack"
import { makeGlobalStyles } from "../../styles/styles"
import { useTranslation } from "react-i18next"
import { MIOType } from "../../components/MIO"
import PressableComponent from "../utils/PressableComponent"
import { TabDirectoryWebViewPage } from "../tabDirectory/tabDirectoryWebView"

const MeBankCard: React.FC<Props<'MeBankCard'>> = ({ route, navigation }) => {

    const [t] = useTranslation();

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header leftComponents={
                <HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />}
                    onPress={onPressBack} 
                    title={t('me.setting')} 
                    titleStyle={[globalStyles.H4, {color: colors.Brand2}]} />
            } />
        });
    }, [navigation]);

    const onPressBack = async() => {
        navigation.pop()
    }

    const onPressOpenMIO = async(type: MIOType) => {
        // navigation.push('MIO', { type: type })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.ME, param: { type: type }})
    }

    return (
        <SafeAreaView
            edges={['bottom']}>
            <StackView direction="column" spacing={10} style={{padding: 10}}>
                <PressableComponent
                    onPress={() => onPressOpenMIO(MIOType.BankManagement)}
                    style={{justifyContent: 'center', alignItems: 'center', height: 50, borderColor: colors.Brand3, borderWidth: 1}}>
                    <Text>Bank Record Management</Text>
                </PressableComponent>
                <PressableComponent
                    onPress={() => onPressOpenMIO(MIOType.Deposit)}
                    style={{justifyContent: 'center', alignItems: 'center', height: 50, borderColor: colors.Brand3, borderWidth: 1}}>
                    <Text>Deposit</Text>
                </PressableComponent>
                <PressableComponent
                    onPress={() => onPressOpenMIO(MIOType.Withdraw)}
                    style={{justifyContent: 'center', alignItems: 'center', height: 50, borderColor: colors.Brand3, borderWidth: 1}}>
                    <Text>Withdraw</Text>
                </PressableComponent>
            </StackView>
        </SafeAreaView>
    )
}

export default MeBankCard