import React, { useState, useLayoutEffect } from 'react';
import { StyleSheet, useColorScheme, View, ScrollView } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import Header from '../../components/header/header.component';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { isEmpty } from '../../utils/stringUtils';
import { AccountUtils } from '../../utils/accountUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { InputType } from '../../types/InputTypes';
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { State } from '../../redux/root-reducer';

const Tab = createMaterialTopTabNavigator();

const MeChangeUsername: React.FC<Props<'MeChangeUsername'>> = ({ route, navigation }) => {

	const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />} 
                    title={t('meSetting.changeUsername.title')} 
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                    onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const isEnableBiometrics = useSelector((state: State) => state.state.setting?.IsEnableBiometrics);
    const userId = useSelector((state: State) => state.state.userId);

    //const [newUsername, setNewUsername] = useState<string>('');
    const [username, setUserName] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 30, isValid: false });

    const [errorCode, SetErrorCode] = useState<string | undefined>(undefined);

    const dispatch = useDispatch();
    const { SetIsShowToastDialog, SetUserId, SetSetting } = bindActionCreators(StateActionCreators, dispatch);

    const { changeUserNickname } = AccountUtils();

    const onPressBackBtn = () => { navigation.goBack() }

    const onPressChangeNickName = async() => {
        var result = await changeUserNickname(username.value);
        if (result) {
            if (result.status === '0'){
                SetIsShowToastDialog({
                    isShowDialog: true,
                    message: t('meSetting.changeUsername.success'),
                    isSuccess: true
                })
                if (isEnableBiometrics === userId) {
                    SetUserId(username.value)
                    SetSetting({IsEnableBiometrics: username.value});
                }
                onPressBackBtn();
            } else {
                setUserName({
                    ...username,
                    error: t('meSetting.changeUsername.error.' + result.errorCode),
                    isValid: false
                })
                // SetErrorCode(t('meSetting.changeUsername.error.' + result.errorCode))
            }
        }
    }

    return (
        <ScrollView style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg, paddingBottom: insets.bottom }]}
        keyboardShouldPersistTaps='handled'>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
                <Input
                label={t('meSetting.changeUsername.newUsername')}
                placeholder=''
                value={username}
                onChangeText={(value) => setUserName({...username, value, error: ''})}
                />

                <View style={{ padding: 30 }} />
                <ActionButton
                title={t('common.confirm')}
                onPress={onPressChangeNickName}
                isEnable={!isEmpty(username.value)}
                />    
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100
    },
});

export default MeChangeUsername;