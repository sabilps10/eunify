import React, { useLayoutEffect, useState, useCallback } from 'react';
import { Pressable, StyleSheet, Text, View, useColorScheme } from 'react-native';
import Header from '../../components/header/header.component';
import { Props } from '../../routes/route';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { State } from '../../redux/root-reducer';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { Auth } from 'aws-amplify';
import { maskEmail, maskPhone } from '../../utils/stringUtils';
import ShowPassword from '../../../assets/svg/ShowPassword';
import HidePassword from '../../../assets/svg/HidePassword';
import { SafeAreaView } from 'react-native-safe-area-context';
import TopBarBack from '../../../assets/svg/TopBarBack';
import HeaderButton from '../../components/header/headerButton.component';
import Edit from '../../../assets/svg/Edit';
import ProfilePic from '../../../assets/svg/ProfilePic';
import PressableComponent from '../utils/PressableComponent';

const MeSettingUser: React.FC<Props<'MeSettingUser'>> = ({ route, navigation }) => {
	const [t, i18n] = useTranslation();

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />} 
                    title={t('meSetting.user.profile')} 
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                    onPress={onPressBackBtn} />}
                />
        });
    }, [navigation]);

    const dispatch = useDispatch();
    const { SetSetting } = bindActionCreators(StateActionCreators, dispatch);
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);

    const selectedPhonePrefix = useSelector((state: State) => state.selection.selected?.settingPhonePrefix);
    const displayName = useSelector((state: State) => state.account.account?.DisplayName);
    const updatedMobileNumber = useSelector((state: State) => state.account.account?.MobileNumber);

    const [email, setEmail] = useState<string>('');
    const [isShowEmail, setIsShowEmail] = useState<boolean>(false);
    const [phonePrefix, setPhonePrefix] = useState<string>('');
    const [phoneNumber, setPhoneNumber] = useState<string>('');
    const [isShowPhone, setIsShowPhone] = useState<boolean>(false);

    const onPressBackBtn = () => { navigation.goBack() }

    useFocusEffect(
        useCallback(() => {
            const fetchData = async () => {
                const session = await Auth.currentSession();
                var payload: { [key: string]: any; } = session.getIdToken().payload;
                var phoneNumber = payload['phone_number'];

                if (updatedMobileNumber !== undefined) {
                    phoneNumber = updatedMobileNumber;
                }
    
                const countryCodes = require('country-codes-list')
                const myCountryCodesObject = countryCodes.customList('countryCallingCode', '+{countryCallingCode}')
                const countryKeys = Object.keys(myCountryCodesObject)
                var _phonePrefix = '';
                var _phoneNumber = '';
                if (countryKeys.includes(phoneNumber.substring(1, 2))){
                    _phonePrefix = phoneNumber.substring(1, 2);
                    _phoneNumber = phoneNumber.substring(2, phoneNumber.length);
                }
                else if (countryKeys.includes(phoneNumber.substring(1, 3))){
                    _phonePrefix = phoneNumber.substring(1, 3);
                    _phoneNumber = phoneNumber.substring(3, phoneNumber.length);
                }
                else if (countryKeys.includes(phoneNumber.substring(1, 4))){
                    _phonePrefix = phoneNumber.substring(1, 4);
                    _phoneNumber = phoneNumber.substring(4, phoneNumber.length);
                }
                else if (countryKeys.includes(phoneNumber.substring(1, 5))){
                    _phonePrefix = phoneNumber.substring(1, 5);
                    _phoneNumber = phoneNumber.substring(5, phoneNumber.length);
                }
    
                setPhonePrefix('+' + _phonePrefix)
                setPhoneNumber(_phoneNumber)
    
                var email = payload['email'];
                setEmail(email)
            }
            
            fetchData().catch(console.error);
        }, [updatedMobileNumber])
    );

    return (
        <SafeAreaView 
            edges={['bottom']}
            style={[styles.container, {backgroundColor: colors.MainBg}]}>
            <View style={styles.imgContainer}>
                <ProfilePic color={colors.Brand2} backgroundColor={colors.Brand1} />
            </View>
            <View style={styles.formContainer}>
                <Text style={[globalStyles.Form_Title]}>{t('meSetting.user.userName')}</Text>
                <View style={{paddingTop: 5, paddingBottom: 10}}>
                    <Text style={[globalStyles.Big_Text_B]}>{displayName}</Text>
                    <PressableComponent style={{position:'absolute', alignContent: 'center', alignSelf: 'flex-end', justifyContent: 'center'}} onPress={()=>{navigation.navigate('MeChangeUsername')}}>
                        <Edit color={colors.Brand3} />
                    </PressableComponent>
                </View>
                
                <View style={{ padding: 5 }} />
                <Text style={[globalStyles.Form_Title]}>{t('meSetting.user.mobileNumber')}</Text>
                <View style={{paddingTop: 5, paddingBottom: 10}}>
                    <Text style={[globalStyles.Big_Text_B]}>{isShowPhone ? phonePrefix + '-' + phoneNumber :  maskPhone(phonePrefix, phoneNumber)}</Text>
                    <View style={{position:'absolute', alignContent: 'center', alignSelf: 'flex-end', justifyContent: 'center', flexDirection: 'row'}}>
                    <PressableComponent onPress={()=> {setIsShowPhone(!isShowPhone)}}>
                        {isShowPhone ? <ShowPassword color={colors.Brand3}/> : <HidePassword color={colors.Brand3}/>}
                    </PressableComponent>
                    <View style={{padding: 5}}/>
                    <PressableComponent onPress={()=>{navigation.navigate('MeChangeMobileNumber')}}>
                        <Edit color={colors.Brand3} />
                    </PressableComponent>
                    </View>
                </View>

                <View style={{ padding: 5 }} />
                <Text style={[globalStyles.Form_Title]}>{t('meSetting.user.emailAddress')}</Text>
                <View style={{paddingTop: 5, paddingBottom: 10}}>
                    <Text style={[globalStyles.Big_Text_B]}>{isShowEmail ? email : maskEmail(email)}</Text>
                    <View style={{position:'absolute', alignContent: 'center', alignSelf: 'flex-end', justifyContent: 'center', flexDirection: 'row'}}>
                    <PressableComponent onPress={()=> {setIsShowEmail(!isShowEmail)}}>
                        {isShowEmail ? <ShowPassword color={colors.Brand3}/> : <HidePassword color={colors.Brand3}/>} 
                    </PressableComponent>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgContainer: {
        flex: 0.3, 
        alignItems:'center', 
        justifyContent:'center'
    },
    formContainer: {
        flex: 0.7,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    }
})
export default MeSettingUser