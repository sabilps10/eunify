import React, { useCallback, useEffect, useLayoutEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View, useColorScheme, Platform, Linking, Alert, NativeModules } from 'react-native';
import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component';
import { Props } from '../../routes/route';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { State } from '../../redux/root-reducer';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import MeSettingListButton from '../../components/me/meSettingListButtons.component';
import DeviceInfo from 'react-native-device-info'
import * as RNLocalize from "react-native-localize";

import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import InputSwitch from '../../components/forms/inputs/inputSwitch.component';
import { getTranslationDict } from '../utils/CommonFunction';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { userDeviceList, getTimeZoneName } from '../../utils/api/userApi';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics';
import { useAppState } from '@react-native-community/hooks';
import { buildDate } from '../../config/constants';
import { CountryOptionType } from '../../components/countryOptionView/countryOptionView';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { DeviceInterface } from '../../redux/account/reducer';
import { registerCustomIconType } from '@rneui/base';
import { CognitoActionCreators } from '../../redux/cognito/actionCreator';
import { CognitoState } from '../../redux/cognito/types';
import { MessageDialogContent } from '../../redux/state/types';
import { SelectionContextLocation } from '../../redux/selection/type';

const MeSetting: React.FC<Props<'MeSetting'>> = ({ route, navigation }) => {

    const insets = useSafeAreaInsets()

    const [t, i18n] = useTranslation();

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header leftComponents={
                <HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />}
                    onPress={onPressBack} 
                    title={t('me.setting')} 
                    titleStyle={[globalStyles.H4, {color: colors.Brand2}]} />
            } />
        });
    }, [navigation, colors]);

    const dispatch = useDispatch();
    const { SetSetting, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetApprovedDevicesList } = bindActionCreators(AccountActionCreators, dispatch);   
    const { SetLoginCredentials } = bindActionCreators(CognitoActionCreators, dispatch); 

    const accountState = useSelector((state: State) => state.account.account);
    const isEnableBiometrics = useSelector((state: State) => state.state.setting?.IsEnableBiometrics);
    const userRegion = useSelector((state: State) => state.state.setting?.UserRegion);
    const userLanguage = useSelector((state: State) => state.state.setting?.UserLanguage);
    const theme = useSelector((state: State) => state.state.setting?.Theme);
    
    const selectedLanguage = useSelector((state: State) => state.selection.selected?.settingLanguage);
    const selectedTheme = useSelector((state: State) => state.selection.selected?.settingTheme);
    const deviceList = useSelector((state: State) => state.account.approvedDeviceList);

    const tradeSetting = useSelector((state: State) => state.state.setting!.TradeSetting);
    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const regionOption = useSelector((state: State) => state.state.regionDataList)
    const userId = useSelector((state: State) => state.state.userId);
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

    const [loginLevel, SetLoginLevel] = useState<number>(0);
    // const [biometricSetting, SetBiometricSetting] = useState<string>('');
    const [approvedDevices, SetApprovedDevices] = useState<string>('');
    const [notification, SetNotification] = useState<string>('');
    const [versionNumber, SetVersionNumber] = useState<string>(DeviceInfo.getVersion());
    const [region, SetRegion] = useState<string>(userRegion);
    
    const [isBiometric, SetIsBiometric] = useState<boolean>(isEnableBiometrics === userId);

    const languageOption = ['en', 'tc', 'sc'];
    const themeOption = ['light', 'dark'];

    const [displayDeviceList, SetDisplayDeviceList] = useState<string[]>([]);
    const [sensorType, setSensorType] = useState<string>();
    const appState = useAppState();

    useFocusEffect(
        React.useCallback(() => {
            const fetchData = async () => {
                var data2 = await userDeviceList() //Get Device List
                if (data2){
                    var tempDeviceList: DeviceInterface[] = [];
                    var list = data2.userDeviceList;
                    list.forEach(element => {
                        tempDeviceList.push(element)
                    });
                    SetApprovedDevicesList(tempDeviceList)
                }
            }
    
            fetchData().catch(console.error);
            
        }, [])
      );

    useFocusEffect(
        React.useCallback(() => {
            if (loginLevel === 3 && tradingAccountList){
                for (var i = 0 ; i < tradingAccountList.length ; i++){
                    var trader = tradingAccountList[i]
                    if ((trader && trader.Account) === login.toString()) {
                        // console.debug('trader.countryCode', trader.countryCode)
                        if (trader.countryCode) {
                            for (var j = 0 ; j < regionOption.length ; j++) {
                                var region = regionOption[j];
                                // console.debug('region.countrySymbol', region.countrySymbol, trader.countryCode)
                                if (region.countrySymbol === trader.countryCode) {
                                    SetRegion(region.regionCode)
                                }
                            }
                        }
                    }
                }
            } else {
                SetRegion(userRegion)
            }
        }, [loginLevel, userRegion])
    );

    useEffect(() => {    
        if (appState === 'active') {
            if (Platform.OS === "android") {
                setSensorType('biometrics');
            } 
            else {
                console.debug('useFocusEffect', useFocusEffect)
                const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })

                rnBiometrics.isSensorAvailable()
                    .then((resultObject) => {
                        
                        const { available, biometryType } = resultObject

                        console.debug('resultObject', resultObject)
                        if (!available)
                            setSensorType(undefined);

                        if (biometryType === BiometryTypes.TouchID) {
                            console.debug("TouchID is supported")
                            setSensorType('touch');
                        } else if (biometryType === BiometryTypes.FaceID) {
                            console.debug('FaceID is supported')
                            setSensorType('face');
                        } else if (biometryType === BiometryTypes.Biometrics) {
                            console.debug("Biometrics is supported")
                            setSensorType('biometrics');
                        }
                    })
            }
        }

    
      }, [appState]);

    useEffect(() => {
        if (selectedTheme)
        {
            SetSetting({
                Theme: selectedTheme
            })
        }
    }, [selectedTheme]);

    useEffect(() => {
        if (selectedLanguage)
        {
            SetSetting({
                UserLanguage: selectedLanguage
            })
            
            i18n.changeLanguage(selectedLanguage);
        }
    }, [selectedLanguage]);

    useEffect(() => {
        const fetchData = async () => {
            if (accountState) {
                // if (accountState.LoginLevel! === 3) {
                //     SetBiometricSetting(isEnableBiometrics ? 'Biometric' : t('meSetting.disabled'))
                // }

                SetLoginLevel(accountState.LoginLevel!)
            }
        }
        fetchData().catch(console.error);
    }, [accountState]);

    useEffect(() => {
        if (deviceList) {
            var names = deviceList.map(function(item) {
                return item ? item['deviceName'] : '';
            });
            SetDisplayDeviceList(names)
        }
    }, [deviceList]);

    useEffect(() => {
        SetIsBiometric(isEnableBiometrics === userId)
    }, [isEnableBiometrics, userId]);

    const onPressBack = () => { navigation.goBack() }

    const onSelectRegionOption = (value: string) => {
        SetSetting({
            UserRegion: value
        })
    }

    const showTimeZone = () => {
        const temp = new Date().toTimeString().split(" ")[1];
        if (temp.length === 8)
        return temp.slice(0, 6) + ":" + temp.slice(6)
    }

    const onPressCountry = () => {
        navigation.navigate('CountryOptionView', {type: CountryOptionType.REGION, onSelect: onSelectRegionOption})
    }

    const onPressLanguage = () => {
        OpenCloseSelection({
            selectedOption: userLanguage ?? "",
            selectOptions: getTranslationDict(languageOption, "language", t),
            isOpen: !isOpen,
            location: SelectionContextLocation.SettingLanguage
        })
    }

    const onPressDisplayMode = () => {
        OpenCloseSelection({
            selectedOption: theme ?? "",
            selectOptions: getTranslationDict(themeOption, "theme", t),
            isOpen: !isOpen,
            location: SelectionContextLocation.SettingTheme
        })
    }

    const onPressBiometric = async() => {
        console.debug('sensorType', sensorType)
        const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })

        if (isBiometric) {
            var rnBiometricsSuccess = false;

            SetIsShowMessageDialog({
                isShowDialog: true, 
                isShowCancel: true, 
                callback:()=>{SetSetting({IsEnableBiometrics: undefined})}, 
                title: t('meSetting.biometrics.disableTitle'), 
                message: '', 
                btnLabel: t('meSetting.biometrics.disableBtn')
            })
            return
        }
        
        var rnBiometricsSuccess = false;
        var enrolledBio = true;
        if (sensorType) {
            if (Platform.OS === "android") {
                const {BiometricNativeModule} = NativeModules;
                const result = await BiometricNativeModule.startBiometric(i18n.language,);
                console.debug('BiometricNativeModule result', result)
                if (result === -1) { //Not Registered
                    enrolledBio = false;
                    console.debug('enrolledBio', enrolledBio)
                } else if (result === 1) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                }
            } else {
                await rnBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
                .then((resultObject) => {
                const { success } = resultObject

                if (success) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                } else {
                    console.debug('user cancelled biometric prompt')
                }
                })
                .catch((e: Error) => {
                    // console.debug('biometrics failed', e.message)
                    // if (e.message.includes('No fingerprints enrolled')) {
                    //     enrolledBio = false
                    // } else if (e.message.includes('Too many attempts')) {
                    //     SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.tooManyAttempts'), message: ''})
                    // }
                    console.debug('biometrics failed', e)
                    if (e.message.includes('Too many attempts') || e.message.includes('嘗試次數過多') || e.message.includes('尝试次数过多')) {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.tooManyAttempts'), message: ''})
                    } else {
                        enrolledBio = false
                    }
                })
            }
        } 

        console.debug('sensorType', sensorType, enrolledBio)
        if (!sensorType || !enrolledBio){
            // Platform.OS === "android" ?
            // SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.notAvailable'), message: t('meSetting.biometrics.biometricDesc'), callback:()=>{Linking.sendIntent("android.settings.BIOMETRIC_ENROLL")}, isShowCancel: true, btnLabel: t('meSetting.biometrics.settingBtn')}) :
            // SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.notAvailable'), message: t('meSetting.biometrics.touchIDDesc'), callback:()=>{}, isShowCancel: true, btnLabel: t('meSetting.biometrics.settingBtn')})

            let iOSBiometric = sensorType == 'face' ? t('meSetting.biometrics.faceIDDesc') : t('meSetting.biometrics.touchIDDesc')

            let isAndroid = Platform.OS === "android"
            let isShowDialog = true
            let isShowCancel = true
            const OsVer = Platform.constants['Release'];
            const brand = DeviceInfo.getBrand();
            let callBack = isAndroid ? (() => (OsVer < 11 || (OsVer == 11 && brand.toLowerCase().includes('samsung') )) ? Linking.sendIntent("android.settings.SETTINGS") : Linking.sendIntent("android.settings.BIOMETRIC_ENROLL")) : () => Linking.openURL('App-prefs:PASSCODE')
            let title = t('meSetting.biometrics.notAvailable')
            let message = isAndroid ? t('meSetting.biometrics.biometricDesc') : iOSBiometric
            let btnLabel = t('meSetting.biometrics.settingBtn')

            SetIsShowMessageDialog({
                isShowDialog: isShowDialog, 
                trackingContext: MessageDialogContent.TrackingContext.EnableBiometricNotAvailable,
                isShowCancel: isShowCancel, 
                callback: callBack, 
                title: title, 
                message: message, 
                btnLabel: btnLabel
            })

        }

        console.debug('rnBiometricsSuccess ' + rnBiometricsSuccess)

        if (rnBiometricsSuccess) {
            SetSetting({IsEnableBiometrics: userId});
            SetLoginCredentials({
                username: cognitoTempState.username,
                password: cognitoTempState.password
            } as CognitoState)
        }

    }

    const onPressApprovedDevices = () => {
        navigation.push('DeviceList', { isFromAddDevice: false })
    }

    const onPressChangePassword = () => {
        navigation.navigate('MeSettingChangeCognitoPassword', { isChangePassword: true })
    }

    const onPressNotification = () => {
        navigation.navigate('MeSettingNotification')
    }

    const setTradeSetting = (isEnableTradeConfirmationDialog: boolean) => {
        var userTradeSetting = {
            IsEnableTradeConfirmationDialog: isEnableTradeConfirmationDialog
        };
        
        tradeSetting[cognitoTempState.username] = userTradeSetting;

        SetSetting({
            TradeSetting: tradeSetting
        })
    }

    const onPressConfirmation = () => {
        if (isEnableTradeConfirmationDialog) {
            SetIsShowMessageDialog({isShowDialog: true, title: t('confirmTrade.disclaimerTitle'), message: t('confirmTrade.disclaimerBody'), callback: () => {setTradeSetting(false)}, isShowCancel: true, btnLabel: t('confirmTrade.accept')})
        } else {
            setTradeSetting(true)
        }
    }

    const onPressTimeZone = () => {
        let isIOS = Platform.OS === 'ios'

        if (isIOS) {
            var url = 'App-prefs:General&path=DATE_AND_TIME'
            let canOpenURL = Linking.canOpenURL(url)
            url = canOpenURL ? url : 'app-settings:'
            Linking.openURL(url)
        } 
        else {
            Linking.sendIntent("android.settings.DATE_SETTINGS")
        }
    }

    return (
        <ScrollView
            style={[styles.container, { backgroundColor: colors.MainBg }]}
            contentContainerStyle={{ paddingBottom: insets.bottom }}
            keyboardShouldPersistTaps='handled'>
            <Text style={[globalStyles.H4, { color: colors.Brand2, paddingHorizontal: LAYOUT_PADDING_HORIZONTAL, paddingTop: 10, paddingBottom: 24 }]}>{t('meSetting.appSetting')}</Text>
            <MeSettingListButton isEnable={loginLevel !== 3} leftLabel={t('meSetting.country')} rightLabel={t(`region.${region}`)} func={onPressCountry} />
            <MeSettingListButton leftLabel={t('meSetting.language')} rightLabel={t(`language.${userLanguage}`)} func={onPressLanguage} />
            {/* <MeSettingListButton leftLabel={t('meSetting.displayMode')} rightLabel={t(`theme.${theme}`)} func={onPressDisplayMode} /> */}
            <MeSettingListButton leftLabel={t('meSetting.timeZone')} rightLabel={showTimeZone()} func={onPressTimeZone} />
            {
                loginLevel === 2 || loginLevel === 3 ?
                    <View>
                        <MeSettingListButton leftLabel={t('meSetting.password')} rightLabel='******' func={onPressChangePassword} />
                        {/* <MeSettingListButton leftLabel={t('meSetting.biometricSetting')} rightLabel={biometricSetting} func={onPressBiometric} /> */}
                        <MeSettingListButton leftLabel={t('meSetting.biometricSetting')} rightLabel='' rightElement={<InputSwitch label='' isRequired={false} value={isBiometric} onSwitch={onPressBiometric} />} />
                        <MeSettingListButton leftLabel={t('meSetting.approvedDevices')} rightLabel={displayDeviceList.join('\r\n')} func={onPressApprovedDevices} />
                        {/* <MeSettingListButton leftLabel={t('meSetting.notification')} rightLabel={notification} func={onPressNotification} /> Comment for trim */}
                    </View>
                : 
                    null
            }
            <MeSettingListButton leftLabel={t('meSetting.versionNumber')} rightLabel={versionNumber + '.' + buildDate} />
            {
                loginLevel === 3 ?
                    <View style={[styles.bottomContainerLv3, { backgroundColor: colors.MainBg }]}>
                        <Text style={[globalStyles.H4, { color: colors.Brand2, paddingHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>{t('meSetting.tradeSetting')}</Text>
                        <View style={{ padding: 5 }} />
                        <MeSettingListButton leftLabel={t('meSetting.tradeReview')} rightLabel='' rightElement={<InputSwitch label='' isRequired={false} value={isEnableTradeConfirmationDialog!} onSwitch={onPressConfirmation} />} />
                    </View>
                :
                    null
            }
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topContainer: {
        flex: 1,
        paddingTop: 10
    },
    topContainerLv3: {
        flex: 0.75,
        paddingTop: 10,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
    },
    bottomContainerLv3: {
        flex: 0.25,
        paddingTop: 25,
        marginTop: 10,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    }
})

export default MeSetting