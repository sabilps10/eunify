import React, { useState, useCallback, useEffect, useLayoutEffect } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Alert,
  KeyboardAvoidingView,
  BackHandler
} from 'react-native';

import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';
import { Auth, Hub } from 'aws-amplify';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import Header from '../../components/header/header.component';

import { maskEmail, maskPhone } from '../../utils/stringUtils';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles'
import { useFocusEffect, useTheme } from '@react-navigation/native';

import commonStyles from '../../styles/styles'
import { StateActionCreators } from '../../redux/state/actionCreator';
import { allowSkipSMS, resendCodeSecond } from '../../config/constants';
import { InputType } from '../../types/InputTypes';
import TextCounter from '../../components/forms/labels/textCounter.component';
import { completeSignUp, updateUserAttribute } from '../../utils/api/userApi';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import VerifyCodeTick from '../../../assets/svg/VerifyCodeTick';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { format } from 'react-string-format';
import { ConsoleLogger } from '@aws-amplify/core';
import { validNumber } from '../utils/CommonFunction';

const CognitoSignUpVerification: React.FC<Props<'CognitoSignUpVerification'>> = ({ route, navigation }) => {

  const [t, i18n] = useTranslation();
  const insets = useSafeAreaInsets()

  useFocusEffect(
      useCallback(() => {

          const backAction = async() => {
              await onPressBackBtn()
              return true;
          };

          const backHandler = BackHandler.addEventListener(
              "hardwareBackPress",
              backAction
          );

          return () => {
              backHandler.remove();
          }
      }, [])
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () =>
        <Header
          leftComponents={<HeaderButton 
            iconComponent={<TopBarBack color={colors.MainFont} />} 
            title={t('cognitoSignUpVerification.title')} 
            titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
            onPress={onPressBackBtn} />}
        />
    });
  }, [navigation]);

  const onPressBackBtn = async() => { 
    const routes = navigation.getState()?.routes;
    const prevRoute = routes[routes.length - 2];
    console.debug('prevRoute', prevRoute)
    if (prevRoute.name === 'CaptchaPage') {
        navigation.navigate('CognitoSignUp')
    } else {
        navigation.goBack()
    }
  }

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)

  const { username, email, phoneNumber, phone, phonePrefix } = route.params;

  const [isCompletedEmail, setIsCompletedEmail] = useState<boolean>(false);
  const [isCompletedPhone, setIsCompletedPhone] = useState<boolean>(false);

  const [emailCode, setEmailCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });
  const [phoneCode, setPhoneCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });

  const dispatch = useDispatch();
  const { SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);
  const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
  const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);

  const listener = ({ payload }) => {
    const { event } = payload;
    if (event === 'autoSignIn') {
      const user = payload.data;
      // console.debug('autoSignIn', user)
      setIsCompletedEmail(true)
      sendPhoneCode();
      SetIsShowLoading(false);
    } else if (event === 'autoSignIn_failure') {
      console.debug('autoSignIn_failure')
      const user = payload.data;
      console.debug('autoSignIn_failure', user)
      SetIsShowLoading(false);
    }
  }

  useEffect(() => {
    Hub.listen('auth', listener)

    return function cleanup() {
      Hub.remove('auth', listener)
    };
  }, []);

  useEffect(() => {
    if (isCompletedEmail) {
      setEmailCode({ ...emailCode, error: '' })
    }
    if (isCompletedPhone) {
      setPhoneCode({ ...phoneCode, error: '' })
    }
  }, [isCompletedEmail, isCompletedPhone]);

  const sendEmailCode = async () => {
    try {
      console.debug('sendEmailCode')
      await Auth.resendSignUp(username);
    } catch (error) {
      console.debug('error resending code: ', error);
      // Alert.alert(error.toString())
      SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.auth'), message: error.toString()})
    }
  };

  const sendPhoneCode = async () => {
    console.debug('sendPhoneCode')
    const user = await Auth.currentAuthenticatedUser();
    const updateResult = await Auth.updateUserAttributes(user, {
      'phone_number': phoneNumber
    });
    console.debug(`sendPhoneCode result ${updateResult}`)
  };

  const checkEmailCode = async (value: string) => {
    setEmailCode({ ...emailCode, value })
    if (value.length !== emailCode.maxLength) {
      return;
    }

    try {
      SetIsShowLoading(true);
      console.debug("before checkEmail")
      await Auth.confirmSignUp(username, value);
      console.debug("after checkEmail")
    } catch (error) {
      console.debug('error confirming sign up', error);
      if (error instanceof Error) {
        if (error.name === 'CodeMismatchException') {
          console.debug('confirmSignUp ', error)
          setEmailCode({ ...emailCode, error: t('error.incorrectCode'), value: '' })
          SetIsShowLoading(false);
        }
        else {
          console.error('confirmSignUp ', error)
        }
      }
    }
  }

  const checkPhoneCode = async (value: string) => {
    setPhoneCode({ ...phoneCode, value })
    if (value.length !== phoneCode.maxLength){
      return;
    }
    console.debug('checkPhoneCode')
    console.debug ("(((((((((((((("+value);

    const user = await Auth.currentAuthenticatedUser();
    var verify = false || allowSkipSMS;
    SetIsShowLoading(true);
    await Auth.verifyUserAttributeSubmit(user, 'phone_number', value).then((res) => {
      console.debug(res)
      verify = true;
    }).catch(async (error) => {
      console.error(error) // hitting error here 
      if (error instanceof Error) {
        if (error.name === 'InternalFailure' || error.name === 'AliasExistsException') {
          const session = await Auth.currentSession();
          var payload: { [key: string]: any; } = session.getIdToken().payload;
          var cognitoUserName = payload['cognito:username'];
          var data = await updateUserAttribute(cognitoUserName, 'phone_number', phoneNumber, true)
          console.debug('updateUserAttribute2' + JSON.stringify(data))
          if (data.status === '0') {
            verify = true
          }
        } else {
          setPhoneCode({ ...phoneCode, error: t('error.incorrectCode'), value: '' })
        }
      }
    }).finally(() => {
      if (verify) {
        setIsCompletedPhone(true)
        setPhoneCode({ ...phoneCode, error: '' })
      }
      SetIsShowLoading(false);
    })
  };

  const onPressNext = async () => {
    if (isCompletedPhone && isCompletedEmail) {
      try
      {
        SetIsShowLoading(true);
        const session = await Auth.currentSession();
        if (session !== null) {
          var payload: { [key: string]: any; } = session.getIdToken().payload;
          var cognitoUserName = payload['cognito:username'];

          SetAccountInfo({
              MobileNumber: '+' + phonePrefix + phone
          })
  
          const result = await completeSignUp(cognitoUserName, email, phone, phonePrefix)
          navigation.navigate("CognitoSignUpCompleted",{cognitoUserName, email, phone, phonePrefix})
        }
        SetIsShowLoading(false);
      }
      catch(err)
      {
        SetIsShowMessageDialog({isShowDialog: true, title: '', message: t('common.unknownError')})
      }
    }
  };

  const callbackEmail = () => {
    sendEmailCode()
  };

  const callbackPhone = () => {
    sendPhoneCode()
  };

  return (
    <KeyboardAvoidingView style={[globalStyles.container_mainBg, { flex: 1 }]}>
      <ScrollView
        contentContainerStyle={[commonStyles.ScrollViewContainerStyle, {paddingBottom: insets.bottom}]}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'>
        <View style={commonStyles.contentContainer}>
          <View style={{ height: 130 }} />
          <Input
            label={t('cognitoSignUpVerification.enterEmailCode')}
            value={emailCode}
            onChangeText={(value) => {
              if (validNumber(value)){
                checkEmailCode(value)
              }
            }}
            rightComponent={isCompletedEmail ? <VerifyCodeTick color={colors.White} backgroundColor={colors.Green} /> : <TextCounter counter={resendCodeSecond} callback={callbackEmail} />}
            editable={!isCompletedEmail}
            type='number'
          />
          <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), maskEmail(email))} labelStyle={globalStyles.Small_Note} />

          <View style={{ height: 20 }} />
          <Input
            label={t('cognitoSignUpVerification.enterPhoneCode')}
            value={phoneCode}
            onChangeText={(value) => {
              if (validNumber(value)){
                checkPhoneCode(value)
              }
            }}
            rightComponent={isCompletedPhone ? <VerifyCodeTick color={colors.White} backgroundColor={colors.Green} /> : isCompletedEmail ? <TextCounter counter={resendCodeSecond} callback={callbackPhone} /> : null}
            editable={isCompletedEmail && !isCompletedPhone}
            type='number'
          />
          <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), maskPhone(phonePrefix, phone))} labelStyle={[globalStyles.Small_Note, { color: !isCompletedEmail ? colors.Grey3 : colors.MainFont }]} />
        </View>

        <View style={{ height: 30}} />
        <View style={{paddingHorizontal: LAYOUT_PADDING_HORIZONTAL}}>
          <ActionButton
            title={t('common.next')}
            onPress={onPressNext}
            isEnable={(isCompletedEmail && isCompletedPhone)}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 100
  },
});

export default CognitoSignUpVerification;