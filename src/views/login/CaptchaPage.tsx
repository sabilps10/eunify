import React, { useState, useContext, useCallback, useMemo, useEffect, useLayoutEffect, useRef, useReducer } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
  Alert,
  Pressable,
  KeyboardAvoidingView,
  Text,
  Button
} from 'react-native';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';
import CaptchaComponent from '../../components/captcha/captcha.component';
import { SafeAreaView } from 'react-native-safe-area-context';


const CaptchaPage: React.FC<Props<'CaptchaPage'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();

  const { onSuccess } = route.params;

  return (
    <CaptchaComponent 
      onSuccess={onSuccess} 
      onClose={()=>{navigation.goBack()}}/>
  );
};

export default CaptchaPage;