import React, { useState, useEffect, useLayoutEffect, useReducer } from 'react';
import {
  View,
  Pressable,
  Text,
  ScrollView
} from 'react-native';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';
import { Auth } from 'aws-amplify';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import InputDropDownList from '../../components/forms/inputs/inputDropdownList.component';
import TextLabel from '../../components/forms/labels/textLabel.component';

import { isEmail, isEmpty } from '../../utils/stringUtils';
import PasswordMessageDialog from '../../components/alerts/passwordMessageDialog.component';
import PasswordEyeIcon from '../../components/forms/labels/passwordEyeIcon.component';
import Header from '../../components/header/header.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';

import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles';

import commonStyles from '../../styles/styles'
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';
import InputCheckBox from '../../components/forms/inputs/inputCheckBox.component';
import { useTheme } from '@react-navigation/native';

import uuid from 'react-native-uuid';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { InputType, SignUpForm } from '../../types/InputTypes';
import { CountryOptionType } from '../../components/countryOptionView/countryOptionView';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { hasInValidCharacters } from '../utils/CommonFunction';
import PressableComponent from '../utils/PressableComponent';

const CognitoSignUp: React.FC<Props<'CognitoSignUp'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();

  const insets = useSafeAreaInsets()

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () =>
        <Header
          leftComponents={<HeaderButton 
            iconComponent={<TopBarBack color={colors.MainFont} />} 
            title={t('cognitoSignUp.title')} 
            titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
            onPress={onPressBackBtn} />}
          RightComponent={<MSmallButton title={t('cognitoSignUp.toLogin')} onPress={() => { navigation.navigate('CognitoLogin', {backPage: 'CognitoSignUp'}) }} />}
        />
    });
  }, [navigation]);

  const onPressBackBtn = () => { navigation.goBack() }

  const [isAgree, SetIsAgree] = useState<boolean>(false);
  const [isGetInfo, SetIsGetInfo] = useState<boolean>(false);

  const [isPasswordVerify, setIsPasswordVerify] = useState<boolean>(false);

  const [isEnablePassword, SetIsEnablePassword] = useState<boolean>(false);
  const [isEnableConfirmPassword, SetIsEnableConfirmPassword] = useState<boolean>(false);
  const [isCheckPhone, setIsCheckPhone] = useState<boolean>(false);
  const [isCheckEmail, setIsCheckEmail] = useState<boolean>(false);

  const dispatch = useDispatch();
  const { SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);
  const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

  // const selectedPhonePrefix = useSelector((state: State) => state.selection.selected?.signUpPhonePrefix);
  const regionDataList = useSelector((state: State) => state.state.regionDataList);

  const [phoneNumber, setPhoneNumber] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 20, isValid: false });
  const [email, setEmail] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 100, isValid: false });

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const initialState: SignUpForm = {
    password: { value: "", error: "", mandatory: true, maxLength: 50, isValid: false },
    confirmPassword: { value: "", error: "", mandatory: true, maxLength: 50, isValid: false },
    // email: { value: "", error: "", mandatory: true, maxLength: 100, isValid: false },
    phonePrefix: { value: regionDataList[0].countryCode, error: "", mandatory: true, isValid: false },
    // phoneNumber: { value: "", error: "", mandatory: true, maxLength: 20, isValid: false },
    referralCode: { value: "", error: "", mandatory: false, isValid: true }
  }

  const formsReducer = (state: SignUpForm, action) => {
    const { name, value, otherValue } = action.data;
    let error = "";
    switch (action.type) {
      case "UPDATE_FORM":
        return {
          ...state,
          [name]: {
            ...state[name],
            value: value,
            error: error,
            isValid: error === "" ? true : false
          }
        }
      case "VALID_FORM":
        error = validateInput(name, value, otherValue, state[name].mandatory);
        return {
          ...state,
          [name]: {
            ...state[name],
            error: error,
            isValid: error === "" ? true : false
          }
        }
      case "UPDATE_AND_VALID_FORM":
        error = validateInput(name, value, otherValue, state[name].mandatory);
        return {
          ...state,
          [name]: {
            ...state[name],
            value: value,
            error: error,
            isValid: error === "" ? true : false
          }
        }
      default:
        return state;
    }
  }

  const validateInput = (name, value, otherValue, isMandatory) => {

    if (isMandatory && value.trim() === "") {
      return t("error.mandatory");
    }

    let error = "";
    switch (name) {
      case "phoneNumber":
        break;
      case "email":
        if (!isEmail(value)) {
          error = t("error.invalidEmailAddress");
        }
        if (userExist(value)) {
          error = t("error.invalidEmailAddress");
        }
        break;
      case "password":
        break;
      case "confirmPassword":
        if (value !== otherValue) {
          error = t("error.inconsistentPassword");
        }
        break;
      default:
        break
    }
    return error;
  }

  const [formState, formDispatch] = useReducer(formsReducer, initialState);


  // useEffect(() => {
  //   if (selectedPhonePrefix)
  //     formDispatch({ type: "UPDATE_FORM", data: { name: "phonePrefix", value: selectedPhonePrefix } })
  // }, [selectedPhonePrefix]);

  useEffect(() => {
    const fetchData = async () => {
      if (phoneNumber.value.length > 0 && phoneNumber.value.length < 8) {
        setPhoneNumber({ ...phoneNumber, error: t("error.mobileLength8"), isValid: false})
        return
      }

      var cognitoPhoneNumber = '+' + formState.phonePrefix.value + phoneNumber.value;
      if (await userExist(cognitoPhoneNumber)) {
        setPhoneNumber({ ...phoneNumber, error: t("error.mobileInUse"), isValid: false})
      } else {
      setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
      }
    }
    
    fetchData().catch(console.error);

  }, [isCheckPhone]);

  useEffect(() => {
    const fetchData = async () => {
      if (!email.value) {
        setEmail({ ...email, error: '', isValid: true })
      }
      else if (!isEmail(email.value)) {
        setEmail({...email, error: t("error.invalidEmailAddress"), isValid: false})
      } else {
        if (await userExist(email.value)) {
          setEmail({ ...email, error: t("error.emailInUse"), isValid: false})
        } else {
          setEmail({ ...email, error: '', isValid: true })
        }
      }
    }
    
    fetchData().catch(console.error);

  }, [isCheckEmail]);

  const onPressSignUp = async () => {

    // formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: '+' + formState.phonePrefix.value + phoneNumber.value } })
    // formDispatch({ type: "VALID_FORM", data: { name: "email", value: email.value } })
    if (await userExist(email.value)) {
      setEmail({ ...email, error: t("error.emailInUse"), isValid: false})
      return;
    }

    formDispatch({ type: "VALID_FORM", data: { name: "password", value: formState.password.value } })
    formDispatch({ type: "VALID_FORM", data: { name: "confirmPassword", value: formState.confirmPassword.value, otherValue: formState.password.value } })

    if (!isPasswordVerify || !phoneNumber.isValid || !email.isValid || !formState.password.isValid || !formState.confirmPassword.isValid) {
      return;
    }
    // Alert.alert(
    //   "Captcha",
    //   "This is Captcha",
    //   [
    //     {
    //       text: "Yes",
    //       onPress: async () => {
    //         await doSignUp();
    //       },
    //     },
    //     {
    //       text: "No",
    //     },
    //   ]
    // );
    navigation.navigate('CaptchaPage', { onSuccess: onCpatchaSuccess })

  };
  const userExist = async(user: string) => {
    return await Auth.signIn(user, '123').then(res => {
      return false;
    }).catch(error => {
      const code = error.code;
      console.debug('userExist ', user, error);
      switch (code) {
        case 'NotAuthorizedException':
          return true;
        case 'PasswordResetRequiredException':
          return true;
        default:
          return false;
      }
    });
  }

  const doSignUp = async () => {
    SetIsShowLoading(true);
    var cognitoPhoneNumber = '+' + formState.phonePrefix.value + phoneNumber.value;

    // const isEmailConfirmed = await userExist(formState.email.value)
    const isEmailConfirmed = false

    if (!isEmailConfirmed) {
      var cognitoUsername = await signUpCognito(email.value, formState.password.value)
      if (cognitoUsername) {
        navigation.navigate('CognitoSignUpVerification', { username: cognitoUsername, email: email.value, phoneNumber: cognitoPhoneNumber, phonePrefix: formState.phonePrefix.value, phone: phoneNumber.value })
      } else {
        SetIsShowMessageDialog({ isShowDialog: true, title: '', message: t('cognitoSignUp.signUpFailed') })
      }
    } else {
      SetIsShowMessageDialog({ isShowDialog: true, title: t('cognitoSignUp.signUpFailed'), message: t('cognitoSignUp.accountExisted'), callback: ()=>{navigation.navigate('CognitoSignUp')} })
    }
    SetIsShowLoading(false);
  }

  const signUpCognito = async (email, password): Promise<string | undefined> => {

    var cognitoUsername: string | undefined = uuid.v4().toString()

    try {

      const { user, userConfirmed, userSub } = await Auth.signUp({
        username: cognitoUsername,
        password,
        attributes: {
          email,
        },
        autoSignIn: {
          enabled: true,
        }
      });
      // console.debug('user', user);
      // console.debug('userConfirmed', userConfirmed);
      // console.debug('userSub', userSub);
      return cognitoUsername;
    } catch (error) {
      console.debug('error', error);
      if (error instanceof Error) {
        if (error.name === 'UsernameExistsException') {
          return await signUpCognito(email, password);
        }
      }
    }
  }

  const onSelectCountryCodeOption = (value: string) => {
    console.debug('onSelectCountryCodeOption', value)
    formDispatch({ type: "UPDATE_FORM", data: { name: "phonePrefix", value: value } })
    // setIsCheckPhone(!isCheckPhone)
  }

  const onPressShowAll = () => {
    // OpenCloseSelection({
    //   selectedOption: formState.phonePrefix.value,
    //   selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
    //   isOpen: true,
    //   location: 'signUpPhonePrefix'
    // })
    navigation.navigate('CountryOptionView', {type: CountryOptionType.COUNTRY, onSelect: onSelectCountryCodeOption, selectedId: formState.phonePrefix.value})
  }

  const updatePasswordVerify = (passwordVerfied: boolean) => {
    setIsPasswordVerify(passwordVerfied)
  };

  const callbackEnablePasswordVisibility = (passwordVerfied: boolean) => {
    SetIsEnablePassword(!isEnablePassword)
  };

  const callbackEnableConfirmPasswordVisibility = (passwordVerfied: boolean) => {
    SetIsEnableConfirmPassword(!isEnableConfirmPassword)
  };

  const onPressPrivacy = () => {
    navigation.navigate("PrivacyPolicy");
  }

  const onPressTerms = () => {
    navigation.navigate("TermsOfService");
  }

  const onCpatchaSuccess = async(data: any) => {
    console.debug('onCpatchaSuccess', data)
    await doSignUp();
  }

  const buttons: string[] = [t('cognitoSignUp.searchEngine'), t('cognitoSignUp.socialMedia'), t('cognitoSignUp.kol'), t('cognitoSignUp.onlineAdvertisement'), t('cognitoSignUp.email'), t('cognitoSignUp.website'),
  t('cognitoSignUp.offlineAdvertisement'), t('cognitoSignUp.event'), t('cognitoSignUp.branch'), t('cognitoSignUp.friend'), t('cognitoSignUp.sms'), t('cognitoSignUp.other')];
  const [selectButtons, SetSelectButtons] = useState<string[]>([]);

  const onSelectButton = (item: string) => {
    var list = [...selectButtons];
    console.debug('onSelectButton', item, list)
    const index = list.indexOf(item);
    if (index > -1) { // only splice array when item is found
      list.splice(index, 1); // 2nd parameter means remove one item only
    } else {
      list.push(item)
    }
    SetSelectButtons(list)
  }

  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: colors.MainBg}}
      edges={['bottom']}>
      <KeyboardAwareScrollView
        extraScrollHeight={40}
        style={{flex: 1}}
        contentContainerStyle={[commonStyles.ScrollViewContainerStyle, {paddingBottom: insets.bottom}]}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'>
        <ScrollView style={{flex: 1}}>
          <View style={commonStyles.contentContainer}>
            <View style={commonStyles.inputPadding} />
            <TextLabel labelStyle={{color: colors.MainFont}} label={t('cognitoSignUp.mobileNumber') + (phoneNumber.mandatory && ' *')} />
            <View style={{ flexDirection: 'row' }}>
              <PressableComponent onPress={onPressShowAll}>
                <InputDropDownList
                  style={{backgroundColor: colors.WrapperBg}}
                  textStyle={{color: colors.MainFont}}
                  value={formState.phonePrefix.value}
                />
              </PressableComponent>
              <View style={{ width: 10 }} />
              <View style={{flex: 1}}>
                <Input
                  inputStyle={{color: colors.MainFont}}
                  type='phone'
                  value={phoneNumber}
                  //onChangeText={(value) => { formDispatch({ type: "UPDATE_FORM", data: { name: "phoneNumber", value: value } }) }}
                  //onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: '+' + formState.phonePrefix.value + phoneNumber.value } })}
                  onChangeText={(value) => 
                    {
                      let newText = '';
                      let numbers = '0123456789';

                      for (var i=0; i < value.length; i++) {
                          if(numbers.indexOf(value[i]) > -1 ) {
                              newText = newText + value[i];
                          }
                      }
                      setPhoneNumber({ ...phoneNumber, value: newText })
                    }
                  }
                  onEndEditing={(e) => setIsCheckPhone(!isCheckPhone)}
                />
              </View>
            </View>

            <View style={commonStyles.inputPadding} />
            <Input
              inputStyle={{color: colors.MainFont}}
              label={t('cognitoSignUp.emailAddress')}
              type='email'
              value={email}
              onChangeText={(value) => setEmail({...email, value})}
              onEndEditing={(e) => setIsCheckEmail(!isCheckEmail)}
            />

            <View style={commonStyles.inputPadding} />
            <Input
              inputStyle={{color: colors.MainFont}}
              label={t('cognitoSignUp.password')}
              value={formState.password}
              type={isEnablePassword ? 'text' : 'password'}
              onChangeText={(value) => {
                  if (!hasInValidCharacters(value)) {
                    formDispatch({ type: "UPDATE_AND_VALID_FORM", data: { name: "password", value: value } })
                    if (formState.confirmPassword.value) {
                      formDispatch({ type: "UPDATE_AND_VALID_FORM", data: { name: "confirmPassword", value: formState.confirmPassword.value, otherValue: value } })
                    }
                  }
                }
              }
              onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "password", value: formState.password.value } })}
              rightComponent={<PasswordEyeIcon isEnable={isEnablePassword} callback={callbackEnablePasswordVisibility} />}
            />

            <View style={commonStyles.inputPadding} />
            <PasswordMessageDialog password={formState.password.value} callback={updatePasswordVerify} />

            <View style={commonStyles.inputPadding} />
            <Input
              inputStyle={{color: colors.MainFont}}
              label={t('cognitoSignUp.repeatPassword')}
              value={formState.confirmPassword}
              type={isEnableConfirmPassword ? 'text' : 'password'}
              onChangeText={(value) => formDispatch({ type: "UPDATE_AND_VALID_FORM", data: { name: "confirmPassword", value: value, otherValue: formState.password.value } })}
              onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "confirmPassword", value: formState.confirmPassword.value, otherValue: formState.password.value } })}
              rightComponent={<PasswordEyeIcon isEnable={isEnableConfirmPassword} callback={callbackEnableConfirmPasswordVisibility} />}
            />

            <View style={commonStyles.inputPadding} />
            {/* <Input
              placeholder=''
              label={t('cognitoSignUp.referralCode')}
              value={referralCode}
              onChangeText={(value) => SetReferralCode(value)}
            />
            <View style={{ padding: 10 }} />
            <Input
              placeholder=''
              label={t('cognitoSignUp.promoCode')}
              value={promoCode}
              onChangeText={(value) => SetPromoCode(value)}
            />

            <View style={{ padding: 10 }} />
            <TextLabel label={t('cognitoSignUp.howHear')} />
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start' }}>
              {
                buttons.map((item, index) =>
                (
                  <SelectionButton title={item} onPress={() => { onSelectButton(item) }} list={selectButtons} />
                ))
              }

            </View> */}

            <View style={{ padding: 5 }} />
            <InputCheckBox 
              labelComponent={
                <View>
                  <Text style={[globalStyles.Note]}>{t('cognitoSignUp.agree')}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <PressableComponent onPress={onPressPrivacy}><Text style={[globalStyles.Note, { color: colors.Brand3 }]}>{t('cognitoSignUp.privacyPolicy')}</Text></PressableComponent>
                  <Text style={[globalStyles.Note]}>{t('cognitoSignUp.and')}</Text>
                  <PressableComponent onPress={onPressTerms}><Text style={[globalStyles.Note, { color: colors.Brand3 }]}>{t('cognitoSignUp.terms')}</Text></PressableComponent>
                </View>
                </View>
              }
              textStyle={{ paddingEnd: 32 }} 
              value={isAgree} 
              onPress={() => { SetIsAgree(!isAgree) }} />

            <View style={{ padding: 5 }} />
            <InputCheckBox 
              label={t('cognitoSignUp.tick')} 
              textStyle={{ paddingEnd: 32, color: colors.MainFont }} 
              value={isGetInfo} 
              onPress={() => { SetIsGetInfo(!isGetInfo) }} />

            {/* <View style={[commonStyles.bottomContainer]}>
              <ActionButton
                title={t('cognitoSignUp.signUp')}
                onPress={onPressSignUp}
                isEnable={isAgree && phoneNumber.isValid && formState.email.isValid && formState.confirmPassword.isValid}
              />
            </View> */}
          </View>
        </ScrollView>
        <View style={[commonStyles.bottomContainer, {backgroundColor: colors.MainBg, flex: 0, paddingHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
          <ActionButton
            title={t('cognitoSignUp.signUp')}
            onPress={onPressSignUp}
            isEnable={isAgree && phoneNumber.isValid && !isEmpty(phoneNumber.value) && email.isValid && formState.confirmPassword.isValid && isPasswordVerify}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default CognitoSignUp;