import { Text } from '@rneui/base';
import React, { useLayoutEffect } from 'react';
import { StyleSheet, useColorScheme, View } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ForgetPasswordPhoneEmail from '../../components/login/forgetPasswordPhoneEmail';
import Header from '../../components/header/header.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { THEME_TYPE } from '../../types/types';

const Tab = createMaterialTopTabNavigator();

const CognitoForgetPassword: React.FC<Props<'CognitoForgetPassword'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
            leftComponents={<HeaderButton 
                iconComponent={<TopBarBack color={colors.MainFont} />} 
                title={t('cognitoForgetPassword.title')} 
                titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    const onPressBackBtn = () => { navigation.goBack() }

    return (
        <SafeAreaView 
            edges={['bottom']} 
            style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg }]}>
                <Tab.Navigator
                    screenOptions={{
                        tabBarStyle: {
                            backgroundColor: colors.MainBg,
                            marginHorizontal: 15,
                            elevation: 0,
                            marginBottom: 8
                        },
                        tabBarIndicatorStyle: {
                            backgroundColor: colors.Brand2,
                        },
                    }}>
                    {/* <Tab.Screen
                        name={t('cognitoForgetPassword.mobileNumber')}
                        component={ForgetPasswordPhoneEmail}
                        initialParams={{type: 'phone'}}
                        options={{
                            tabBarLabel: (props => <Text style={props.focused ? [styles.tabActiveHeader, { color: colors.Brand2 }] : [styles.tabHeader, { color: 'rgba(0, 0, 0, 0.25)' }]}>{t('cognitoForgetPassword.mobileNumber')}</Text>)
                        }} /> */}
                    <Tab.Screen
                        name={t('cognitoForgetPassword.emailAddress')}
                        component={ForgetPasswordPhoneEmail}
                        initialParams={{type: 'email'}}
                        options={{
                            tabBarLabel: (props => <Text style={props.focused ? [styles.tabActiveHeader, { color: colors.Brand2 }] : [styles.tabHeader, { color: 'rgba(0, 0, 0, 0.25)' }]}>{t('cognitoForgetPassword.emailAddress')}</Text>)
                        }} />
                </Tab.Navigator>
        </SafeAreaView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        paddingTop: 100
    },
    // info container
    infoContainer: {
        marginLeft: 16,
        marginRight: 11,
        marginTop: 9,
        marginBottom: 28,
        flexDirection: 'row',
        alignItems: 'center',
    },
    infoContainerColumn: {},
    infoContainerColumnItem: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 93,
        height: 18,
        justifyContent: 'space-between'
    },
    tabActiveHeader: {
        fontFamily: 'Montserrat',
        fontWeight: '600',
        fontSize: 13,
        lineHeight: 14.3,
    },
    tabHeader: {
        fontFamily: 'Montserrat',
        fontWeight: '600',
        fontSize: 13,
        lineHeight: 14.3,
    }
});

export default CognitoForgetPassword;