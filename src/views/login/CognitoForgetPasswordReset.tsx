import React, { useState, useLayoutEffect, useEffect } from 'react';
import { KeyboardAvoidingView, Platform, ScrollView, StyleSheet, useColorScheme, View } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import PasswordMessageDialog from '../../components/alerts/passwordMessageDialog.component';
import PasswordEyeIcon from '../../components/forms/labels/passwordEyeIcon.component';
import Header from '../../components/header/header.component';

import { Auth } from 'aws-amplify';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { isEmpty } from '../../utils/stringUtils';
import { InputType } from '../../types/InputTypes';
import { AccountUtils } from '../../utils/accountUtils';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { hasInValidCharacters } from '../utils/CommonFunction';
import { SetLoginCredentials } from '../../redux/cognito/action';
import { SetSetting } from '../../redux/state/action';

import { CognitoState } from '../../redux/cognito/types';
import { SetConnectionStatus } from '../../redux/connection/action';
import { Status } from '../../redux/connection/type';

const Tab = createMaterialTopTabNavigator();

const CognitoForgetPasswordReset: React.FC<Props<'CognitoForgetPasswordReset'>> = ({ route, navigation }) => {

  const [t, i18n] = useTranslation();
  const insets = useSafeAreaInsets()

  const { isChangePassword, username, displayUsername, emailCode, skipVerification } = route.params;
  const scheme = useColorScheme();
  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)

  // const [oldPassword, SetOldPassword] = useState<string>('');
  // const [password, SetPassword] = useState<string>('');
  // const [confirmPassword, SetConfirmPassword] = useState<string>('');

  const [oldPassword, SetOldPassword] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 50, isValid: false });
  const [password, SetPassword] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 50, isValid: false });
  const [confirmPassword, SetConfirmPassword] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 50, isValid: false });

  const [isPasswordVerify, setIsPasswordVerify] = useState<boolean>(false);

  const [isEnableOldPassword, SetIsEnableOldPassword] = useState<boolean>(false);
  const [isEnablePassword, SetIsEnablePassword] = useState<boolean>(false);
  const [isEnableConfirmPassword, SetIsEnableConfirmPassword] = useState<boolean>(false);

  const [errorOldPassword, SetErrorOldPassword] = useState<string | undefined>(undefined);
  const [errorPassword, SetErrorPassword] = useState<string | undefined>(undefined);
  const [errorRepeatPassword, SetErrorRepeatPassword] = useState<string | undefined>(undefined);

  const { updateProfilePassword } = AccountUtils();

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () => <Header
        leftComponents={<HeaderButton
          iconComponent={<TopBarBack color={colors.MainFont} />}
          title={isChangePassword ? ((skipVerification && skipVerification.isSkip) ? t('cognitoForgetPassword.titleReset') : t('meSetting.changePassword.title')) : t('cognitoForgetPassword.titleReset')}
          titleStyle={[globalStyles.H4, { color: colors.Brand2 }]}
          onPress={onPressBackBtn} />}
      />
    });
  }, [navigation]);

  useEffect(() => {
    if (oldPassword) {
      SetErrorOldPassword(undefined)
    }
    if (password) {
      SetErrorPassword(undefined)
    }
    if (confirmPassword) {
      SetErrorRepeatPassword(undefined)
    }
  }, [oldPassword, password, confirmPassword]);

  const dispatch = useDispatch();
  const { SetIsShowLoading, SetIsShowToastDialog, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

  const onPressBackBtn = () => { navigation.goBack() }

  const updatePasswordVerify = (passwordVerfied: boolean) => {
    setIsPasswordVerify(passwordVerfied)
  };

  const resetPassword = async () => {
    console.debug(isChangePassword,)
    if (isChangePassword && !(skipVerification && skipVerification.isSkip) && isEmpty(oldPassword.value)) {
      //Alert.alert(t('cognitoSignUp.errorPasswordEmpty'));
      //SetErrorPassword(t('cognitoSignUp.errorMandatory'))
      SetOldPassword({ ...oldPassword, error: t('error.mandatory') })
      return;
    } else {
      SetOldPassword({ ...oldPassword, error: '' })
    }

    if (isEmpty(password.value)) {
      // Alert.alert(t('cognitoSignUp.errorPasswordEmpty'));
      //SetErrorPassword(t('cognitoSignUp.errorMandatory'))
      SetPassword({ ...password, error: t('error.mandatory') })
      return;
    } else {
      SetPassword({ ...password, error: '' })
    }

    if (isEmpty(confirmPassword.value)) {
      // Alert.alert(t('cognitoSignUp.errorRepeatPasswordEmpty'));
      //SetErrorRepeatPassword(t('cognitoSignUp.errorMandatory'))
      SetConfirmPassword({ ...confirmPassword, error: t('error.mandatory') })
      return;
    } else {
      SetConfirmPassword({ ...confirmPassword, error: '' })
    }

    if (!isPasswordVerify) {
      // Alert.alert(t('cognitoSignUp.errorPasswordIncorrect'));
      return;
    }

    if (password.value !== confirmPassword.value) {
      // Alert.alert(t('cognitoSignUp.errorPasswordNotMatch'));
      //SetErrorRepeatPassword(t('cognitoSignUp.errorInconsistent'))
      SetConfirmPassword({ ...confirmPassword, error: t('error.inconsistentPassword') })
      return;
    } else {
      SetConfirmPassword({ ...confirmPassword, error: '' })
    }

    SetIsShowLoading(true)
    if (skipVerification && skipVerification.isSkip) {
      const user = await Auth.signIn(
        skipVerification.username,
        skipVerification.password
      );
      if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
        try {
          const newUser = await Auth.completeNewPassword(
            user, // the Cognito User Object
            password.value, // the new password
          )
          // at this time the user is logged in if no MFA required
          console.debug('completeNewPassword', newUser);
          SetIsShowToastDialog({
            isShowDialog: true,
            message: t('meSetting.changePassword.success'),
            isSuccess: true
          })


        //   SetLoginCredentials({
        //     username: "",
        //     password: ""
        // } as CognitoState)

        // SetSetting({
        //     IsSkipRegBiometric: false,
        //     IsEnableBiometrics: undefined
        // })

        // SetConnectionStatus(Status.ResetPassword)

          //navigation.navigate('CognitoLogin', { forceLogin: true })
          navigation.reset({
            index: 1,
            routes: [{name: 'TabDirectory'}, {name: 'CognitoLogin', params: {forceLogin: true}}]
          });
        }
        catch (e) {
          console.debug('completeNewPassword', e);
        };
      }
    }
    else if (isChangePassword) {
      try {
        const user = await Auth.currentAuthenticatedUser()
        const data = await Auth.changePassword(user, oldPassword.value, password.value);
        await updateProfilePassword(password.value)

        SetIsShowToastDialog({
          isShowDialog: true,
          message: t('meSetting.changePassword.success'),
          isSuccess: true
        })

        //navigation.navigate('CognitoLogin', { forceLogin: true })
        navigation.reset({
          index: 1,
          routes: [{name: 'TabDirectory'}, {name: 'CognitoLogin', params: {forceLogin: true}}]
        });
      }
      catch (err) {
        console.debug(err)
        if (err instanceof Error) {
          if (err.name === 'NotAuthorizedException') {
            SetOldPassword({ ...oldPassword, isValid: false, error: t('cognitoLogin.error.incorrectPassword') })
          } else {
            // Alert.alert(err.toString())
            SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.auth'), message: err.toString() })
          }
        }
      }
    } else {
      // await Auth.forgotPasswordSubmit(username, emailCode, password.value)
      //   .then(async data => {
      //     console.debug(data)
      //     // Alert.alert(data.toString())
      //     SetIsShowToastDialog({
      //       isShowDialog: true,
      //       message: t('cognitoForgetPassword.success'),
      //       isSuccess: true
      //     })

      //     //Call EMP Change Password
      //     await updateProfilePassword(password.value, username)

      //     navigation.navigate('CognitoLogin')
      //   })
      //   .catch(err => {
      //     console.debug(err)
      //     // Alert.alert(err.toString())
      //   });
      navigation.navigate('CognitoForgetPasswordVerification', { username: username, displayUsername: displayUsername, password: password.value })
    }
    SetIsShowLoading(false)
  }

  const callbackEnableOldPasswordVisibility = (passwordVerfied: boolean) => {
    SetIsEnableOldPassword(!isEnableOldPassword)
  };

  const callbackEnablePasswordVisibility = (passwordVerfied: boolean) => {
    SetIsEnablePassword(!isEnablePassword)
  };

  const callbackEnableConfirmPasswordVisibility = (passwordVerfied: boolean) => {
    SetIsEnableConfirmPassword(!isEnableConfirmPassword)
  };

  return (
    <SafeAreaView
      style={{ flex: 1, backgroundColor: colors.MainBg }}
      edges={['bottom']}>
      <KeyboardAwareScrollView
        extraScrollHeight={40}
        style={[globalStyles.container, styles.container]}
        keyboardShouldPersistTaps='handled'>
        <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
          {
            isChangePassword && !(skipVerification && skipVerification.isSkip) ?
              <View>
                <Input
                  label={t('cognitoForgetPassword.oldPassword')}
                  value={oldPassword}
                  onChangeText={(value) => {
                    if (!hasInValidCharacters(value)) {
                      SetOldPassword({ ...oldPassword, value, error: '' })
                    }
                  }
                  }
                  // onEndEditing={(e) => checkEmailCode()}
                  rightComponent={<PasswordEyeIcon isEnable={isEnableOldPassword} callback={callbackEnableOldPasswordVisibility} />}
                  type={isEnableOldPassword ? 'text' : 'password'}
                />
                <View style={{ padding: 10 }} />
              </View> : null
          }
          <Input
            label={t(isChangePassword ? 'cognitoForgetPassword.newPassword' : 'cognitoForgetPassword.password')}
            value={password}
            onChangeText={(value) => {
              if (!hasInValidCharacters(value)) {
                SetPassword({ ...password, value })
              }
            }
            }
            // onEndEditing={(e) => checkEmailCode()}
            rightComponent={<PasswordEyeIcon isEnable={isEnablePassword} callback={callbackEnablePasswordVisibility} />}
            type={isEnablePassword ? 'text' : 'password'}
          />
          <View style={{ padding: 10 }} />
          <PasswordMessageDialog password={password.value} callback={updatePasswordVerify} />
          <View style={{ padding: 10 }} />
          <Input
            label={t(isChangePassword ? 'meSetting.changePassword.confirmNewPassword' : 'cognitoForgetPassword.repeatPassword')}
            placeholder=''
            value={confirmPassword}
            onChangeText={(value) => {
              if (!hasInValidCharacters(value)) {
                SetConfirmPassword({ ...confirmPassword, value })
              }
            }
            }
            // onEndEditing={(e) => checkEmailCode()}
            rightComponent={<PasswordEyeIcon isEnable={isEnableConfirmPassword} callback={callbackEnableConfirmPasswordVisibility} />}
            type={isEnableConfirmPassword ? 'text' : 'password'}
          />
          <View style={{ padding: 30 }} />
          <ActionButton
            title={isChangePassword ? t('common.done') : t('common.next')}
            onPress={resetPassword}
            isEnable={(!isChangePassword || (skipVerification && skipVerification.isSkip) || !isEmpty(oldPassword.value)) && !isEmpty(password.value) && isPasswordVerify && !isEmpty(confirmPassword.value)}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 100
  },
});

export default CognitoForgetPasswordReset;