import React, {useEffect,useState} from 'react';
import {
  Linking,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';
import { Auth } from 'aws-amplify';

import TextLabel from '../../components/forms/labels/textLabel.component';
import TickMessage from '../../components/alerts/tickMessage.component'
import BulletMessage from '../../components/alerts/bulletMessage.component'

import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import ActionButton2 from '../../components/buttons/actionButton2.component';
import ActionButton from '../../components/buttons/actionButton.component';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { WebsocketUtils } from '../../utils/websocketUtils';
import { AccountUtils } from '../../utils/accountUtils';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { lv3Registration } from '../../utils/api/userApi';
import { setChartURL, setTradingURL } from '../../utils/api/apiSetter';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import {CognitoActionCreators} from '../../redux/cognito/actionCreator';
import {CognitoTempActionCreators} from '../../redux/cognitoTemp/actionCreator';


const CognitoSignUpCompleted: React.FC<Props<'CognitoSignUpCompleted'>> = ({ route, navigation }) => {

  const {email} = route?.params;
  const [t, i18n] = useTranslation();
  const insets = useSafeAreaInsets()

  const dispatch = useDispatch();
  const { SetIsShowMessageDialog, SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);
  const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)
  const isLogined = useSelector((state: State) => state.account.isLogined);

  const {SetCurrentToken} = bindActionCreators(CognitoActionCreators, dispatch);
  const {SetLoginTempCredentials} = bindActionCreators(
    CognitoTempActionCreators,
    dispatch,
  );

  const [notYetPressedButton, setNotYetPressedButton] = useState(true);

  const setting = useSelector((state: State) => state.state.setting);

  const { getClosestPath } = WebsocketUtils();
  const { doLogin, doConnectWebsocket, doCloseWebsocket } = AccountUtils();

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)

  const getLoginData = async () => {
    var pushyDeviceID = setting?.PushyDeviceID ?? '';
    var region: string = setting?.UserRegion!;
    if (isLogined) {
      var res = await doLogin(pushyDeviceID, region, true, true);
    } else {
      var res = await doLogin(pushyDeviceID, region, false, true);
    }
    return res;
  };

  const onPressExperience = async() => {

    if (!notYetPressedButton) {
      return;
    }
    setNotYetPressedButton(false);
    SetIsShowLoading(true)
    try {
      // var pushyDeviceID = setting?.PushyDeviceID ?? "";
      // var region: string = setting?.UserRegion!;
      // var data = await doLogin(pushyDeviceID, region, false, true)
      getLoginData().then(async(resData) => {
        if (resData && resData.success) {
          await doCloseWebsocket(true);
          const status = await doConnectWebsocket(resData.paths, true, resData.accessToken, resData.loginLevel, false);
          if (status === 101)
          {
              SetAccountDisplayName(resData.payload.currentTrader);
              setTradingURL(resData.payload.tApiPath);
              setChartURL(resData.payload.chartPath, resData.payload.chartWSPath);
          }
          // navigation.navigate('TabDirectory')
          navigation.navigate('Tab_WatchlistView');
      }
      else {
        SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: '' })
      }
      })
    } 
    catch(e) {

    } finally {
      SetIsShowLoading(false);
    }
    //navigation.navigate(TabDirectoryTab.QUOTE)
  }


  const onPressOpenAccount = async () => {
    if (!notYetPressedButton) {
      return;
    }
    setNotYetPressedButton(false);
    SetIsShowLoading(true);

    getLoginData().then(async(resData) => {
      if (resData && resData.success) {
        await doCloseWebsocket(true);
        const status = await doConnectWebsocket(
          resData.paths,
          true,
          resData.accessToken,
          resData.loginLevel,
          false,
        );
        if (status === 101) {
          SetAccountDisplayName(resData.payload.currentTrader);
          setTradingURL(resData.payload.tApiPath);
          setChartURL(resData.payload.chartPath, resData.payload.chartWSPath);
        }
  
        const currentSession = await Auth.currentSession();
        var payload: {[key: string]: any} = currentSession.getIdToken().payload;
        var token = currentSession.getAccessToken().getJwtToken();
  
        SetCurrentToken(token);
        SetLoginTempCredentials({
          username: email ?? payload['email'],
          password: '',
        });
        SetIsShowLoading(false);
  
        navigation.navigate('AccountQuestion', {
          email: email ?? payload['email'],
          token,
        });
        setNotYetPressedButton(true);
      } else {
        SetIsShowMessageDialog({
          isShowDialog: true,
          title: t('common.error.network'),
          message: '',
        });
      }
    })
  };

  return (
    <ScrollView style={[globalStyles.container, styles.container, { backgroundColor: colors.Brand3, paddingBottom: insets.bottom }]}
    keyboardShouldPersistTaps='handled'>
        <View style={[styles.upperContainer, { backgroundColor: colors.Brand3 }]}>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
            <TextLabel label={t('cognitoSignUpCompleted.congratulations')} labelStyle={[globalStyles.H2, { color: colors.TitleFont }]} />
            <TextLabel label={t('cognitoSignUpCompleted.nowYouCan')} labelStyle={[globalStyles.H4, { color: colors.TitleFont }]} />
          </View>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL, paddingTop: 24, paddingBottom: 27 }}>
            <View style={styles.row}>
              <TickMessage message={t('cognitoSignUpCompleted.checkQuotes')} isTick={true} />
              <TickMessage message={t('cognitoSignUpCompleted.exportMarket')} isTick={true} />
            </View>
            <View style={{ height: 10 }} />
            <View style={styles.row}>
              <TickMessage message={t('cognitoSignUpCompleted.demoTrade')} isTick={true} />
              <TickMessage message={t('cognitoSignUpCompleted.learnToTrade')} isTick={true} />
            </View>
            {/* <View style={{ height: 10 }} />
            <View style={styles.row}>
              <TickMessage message={t('cognitoSignUpCompleted.memberOffer')} isTick={false} />
              <TickMessage message={t('cognitoSignUpCompleted.exclusivePromotions')} isTick={true} />
            </View> */}
          </View>
        </View>
        <View style={[styles.container, { backgroundColor: colors.Brand3 }]}>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL, paddingBottom: 50 }}>
            <ActionButton title={t('cognitoSignUpCompleted.experienceNow')} onPress={onPressExperience} />
          </View>
          <View style={[styles.lowerContainer, { backgroundColor: colors.Brand1 }]}>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
              <TextLabel label={t('cognitoSignUpCompleted.openRealAccount')} labelStyle={[globalStyles.H4, { color: colors.FixedMainFont }]} />
            </View>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL, paddingTop: 15, paddingBottom: 27 }}>
              <View style={styles.row}>
                <BulletMessage message={t('cognitoSignUpCompleted.tradeLeverage')} />
                <BulletMessage message={t('cognitoSignUpCompleted.earnPoints')} />
              </View>
              <View style={{ height: 5 }} />
              <View style={styles.row}>
                <BulletMessage message={t('cognitoSignUpCompleted.expertSupport')} />
                <BulletMessage message={t('cognitoSignUpCompleted.exclusivePromotions')} />
              </View>
              {/* <View style={{ height: 5 }} />
              <View style={styles.row}>
                <BulletMessage message={t('cognitoSignUpCompleted.loyaltyPoints')} />
                <BulletMessage message={t('cognitoSignUpCompleted.giftRedemption')} />
              </View> */}
              <View style={{ height: 15 }} />
              <ActionButton2 
                label={t('cognitoSignUpCompleted.openAccount')} 
                labelStyle={[globalStyles.ButtonText, { color: colors.FixedMainFont, alignSelf: 'center' }]} 
                style={{ borderColor: colors.FixedMainFont }} 
                onPress={onPressOpenAccount}
                iconColor={colors.FixedMainFont} />
            </View>
          </View>
        </View>
        {/* <View style={[styles.bottomContainer, { backgroundColor: colors.White, marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
          <MSmallButton title={t('cognitoSignUpCompleted.toDashboard')} type='White' textStyle={[globalStyles.Note]} onPress={()=>{navigation.navigate('TabDirectory')}} />
          <View style={{ height: 10 }} />
          <BigButton title={t('cognitoSignUpCompleted.activateDemoTrade')} type='Outline' onPress={() => { console.debug('Big button Outline') }} />
          <View style={{ height: 10 }} />
          <BigButton title={t('cognitoSignUpCompleted.openRealAccount')} onPress={() => { console.debug('Big button Outline') }} />
        </View> */}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  upperContainer: {
    paddingTop: 80
  },
  lowerContainer: {
    borderRadius: 12,
    paddingTop: 30,
    marginHorizontal: LAYOUT_PADDING_HORIZONTAL
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 10
  },
  row: {
    flexDirection: 'row'
  }
});

export default CognitoSignUpCompleted;