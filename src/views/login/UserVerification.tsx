import React, {useState, useLayoutEffect, useEffect} from 'react';
import { StyleSheet, useColorScheme, View, Alert, Pressable, ScrollView } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { Text } from '@rneui/base';
import { UserAgent } from 'amazon-cognito-identity-js';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import TextCounter from '../../components/forms/labels/textCounter.component';
import Header from '../../components/header/header.component';

import { Auth } from 'aws-amplify';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import ErrorMessage from '../../components/alerts/errorMessage.component';
import { isEmpty, maskEmail, maskPhone } from '../../utils/stringUtils';
import { AccountUtils } from '../../utils/accountUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { allowSkipSMS, resendCodeSecond } from '../../config/constants';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { getTranslationDict, validNumber } from '../utils/CommonFunction';
import InputDropDownList from '../../components/forms/inputs/inputDropdownList.component';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { InputType } from '../../types/InputTypes';
import { State } from '../../redux/root-reducer';
import { CountryOptionType } from '../../components/countryOptionView/countryOptionView';
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { updateUserAttribute } from '../../utils/api/userApi';
import { format } from 'react-string-format';
import PressableComponent from '../utils/PressableComponent';

const Tab = createMaterialTopTabNavigator();

const UserVerification: React.FC<Props<'UserVerification'>> = ({ route, navigation }) => {
	
    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()
    
    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={<TopBarBack color={colors.MainFont} />}
                    title={t('userVerification.title')} 
                    titleStyle={{...globalStyles.H4, color: colors.Brand2}} 
                    onPress={onPressBack} />}
            />
        });
    }, [navigation]);

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const { type, callBack } = route.params;

    const regionDataList = useSelector((state: State) => state.state.regionDataList);

    const [verStage, setVerStage] = useState<number>(1);

    const [displayEmail, setDisplayEmail] = useState<string>('');
    // const [emailCode, setEmailCode] = useState<string>('');
    const [emailCode, setEmailCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });
  
    // const selectionOptions = ["852", "853"];
    const [phonePrefix, SetPhonePrefix] = useState<string>(regionDataList[0].countryCode);
    // const [phoneNumber, SetPhoneNumber] = useState<string>('');
    const [phoneNumber, setPhoneNumber] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 20, isValid: false });
    const [isCheckPhone, setIsCheckPhone] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetIsShowLoading, SetIsShowToastDialog } = bindActionCreators(StateActionCreators, dispatch);

    const { updateProfileMobile } = AccountUtils();

    const onPressBack = () => { navigation.goBack() }

    const onSelectCountryCodeOption = (value: string) => {
        console.debug('onSelectCountryCodeOption', value)
        // formDispatch({ type: "UPDATE_FORM", data: { name: "phonePrefix", value: value } })
        SetPhonePrefix(value)
        setIsCheckPhone(!isCheckPhone)
      }

    const onPressShowAll = () => {
        // OpenCloseSelection({
        //     selectedOption: phonePrefix,
        //     selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
        //     isOpen: true,
        //     location: 'forgotPasswordPhonePrefix'
        // })
        navigation.navigate('CountryOptionView', {type: CountryOptionType.COUNTRY, onSelect: onSelectCountryCodeOption, selectedId: phonePrefix})
    }

    const resendEmailCode = async () => {
        try {
            const user = await Auth.currentAuthenticatedUser();
            // await Auth.verifyUserAttribute(user, type);
            await verifyUserAttribute()
        } catch (error) {
          console.debug('error resending code: ', error);
        //   Alert.alert(error.toString())
        }
      };

    const callbackEmail = () => {
        resendEmailCode()
    };
    
    const verifyUserAttribute = async () => {
        const user = await Auth.currentAuthenticatedUser();
        const cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;        
        // Auth.updateUserAttributes(user, {
        //     'phone_number': cognitoPhoneNumber
        // }).then((res) => {
        //     console.debug('updateResult' + res + cognitoPhoneNumber) 
        //     setVerStage(2)
        //     setEmailCode({...emailCode, value: '', isValid: true, error: ''})
        // }).catch((error) => {
        //     console.error(error) // hitting error here 
        // })

        if (await userExist(cognitoPhoneNumber)) {
            setPhoneNumber({ ...phoneNumber, error: t("error.mobileInUse"), isValid: false})
            SetIsShowLoading(false)
            return
        } else {
            setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
        }
        
        const session = await Auth.currentSession();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        var cognitoUserName = payload['cognito:username'];
        var data = await updateUserAttribute(cognitoUserName, 'phone_number', cognitoPhoneNumber, false)
        console.debug('updateUserAttribute2' + JSON.stringify(data))
        if (data.status === '0') {
            console.debug('updateResult' + data.status + cognitoPhoneNumber) 
            setVerStage(2)
            setEmailCode({...emailCode, value: '', isValid: true, error: ''})
        }

    };

    const verifyUserAttributeConfirm = async (type: string) => {
        SetIsShowLoading(true)
        const user = await Auth.currentAuthenticatedUser();
        const cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;     
        var verify = false || allowSkipSMS;
        await Auth.verifyUserAttributeSubmit(user, type, emailCode.value).then((res) => {
            console.debug(res) 
            verify = true;
        }).catch(async(error) => {
            console.error(error) // hitting error here 
            if (error instanceof Error) {
                if (error.name === 'InternalFailure' || error.name === 'AliasExistsException' ) {
                    const session = await Auth.currentSession();
                    var payload: { [key: string]: any; } = session.getIdToken().payload;
                    var cognitoUserName = payload['cognito:username'];
                    var data = await updateUserAttribute(cognitoUserName, 'phone_number', cognitoPhoneNumber, true)
                    console.debug('updateUserAttribute2' + JSON.stringify(data))
                    if (data.status === '0') {
                    verify = true
                    }
                } else {
                    setEmailCode({...emailCode, isValid: false, error: t('userVerification.incorrectEmailCode')})
                }
            }
            
        }).finally(async()=>{
            if (verify) {
                if (type === 'email') {
                    setVerStage(1)
                }
                else {
                    console.debug('verifyUserAttributeConfirm 1')
                    //Call EMP API
                    await updateProfileMobile(phoneNumber.value, phonePrefix)
                    console.debug('verifyUserAttributeConfirm 2')
                    await callBack(phoneNumber.value, '+'+phonePrefix)
                    console.debug('verifyUserAttributeConfirm 3')
                }
            }
            SetIsShowLoading(false);
        })
    };

    const userExist = async(user: string) => {
        return await Auth.signIn(user, '123').then(res => {
          return false;
        }).catch(error => {
          const code = error.code;
          console.debug('userExist ', user, error);
          switch (code) {
            case 'UserNotFoundException':
              return false;
            case 'NotAuthorizedException':
              return true;
            case 'PasswordResetRequiredException':
              return false;
            case 'UserNotConfirmedException':
              return false;
            default:
              return false;
          }
        });
      }

    useEffect(() => {
        const fetchData = async () => {
            if (verStage === 1) {
                var cognitoPhoneNumber = '+' + phonePrefix + phoneNumber.value;
                if (await userExist(cognitoPhoneNumber)) {
                    setPhoneNumber({ ...phoneNumber, error: t("error.mobileInUse"), isValid: false})
                } else {
                    setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
                }
            }
            // setPhoneNumber({ ...phoneNumber, error: '', isValid: true })
        }
        
        fetchData().catch(console.error);
    
      }, [isCheckPhone]);

    return (
        <ScrollView style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg, paddingBottom: insets.bottom}]}
            keyboardShouldPersistTaps='handled'>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
                {verStage === 1 && 
                    <View>
                        <TextLabel label={t('userVerification.mobileNumber')} />
                        <View style={{ flexDirection: 'row' }}>
                            <PressableComponent onPress={onPressShowAll}>
                                <InputDropDownList value={phonePrefix} />
                            </PressableComponent>
                            <View style={{ width: 10 }} />
                            <View style={{flex: 1}}>
                            <Input
                                style={{ flex: 1, justifyContent: 'center' }}
                                type='phone'
                                value={phoneNumber}
                                //onChangeText={(value) => { formDispatch({ type: "UPDATE_FORM", data: { name: "phoneNumber", value: value } }) }}
                                //onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: '+' + formState.phonePrefix.value + phoneNumber.value } })}
                                onChangeText={(value) => setPhoneNumber({ ...phoneNumber, value })}
                                onEndEditing={(e) => setIsCheckPhone(!isCheckPhone)}
                            />
                            </View>
                        </View>

                        <View style={{ padding: 30 }} />
                        <ActionButton
                        title={t('common.next')}
                        onPress={verifyUserAttribute}
                        isEnable={!isEmpty(phoneNumber.value)}
                        />
                    </View>
                }
                {verStage === 2 && 
                    <View>
                        <Input
                            label={t('userVerification.verificationMobile')}
                            value={emailCode}
                            onChangeText={(value) => {
                                if (validNumber(value)){
                                    setEmailCode({...emailCode, value})
                                }
                            }}
                            rightComponent={<TextCounter counter={resendCodeSecond} callback={callbackEmail}/>}
                            type='number'
                        />
                        <View style={{ padding: 5 }} />
                        <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), maskPhone('+' + phonePrefix, phoneNumber.value))} labelStyle={globalStyles.Small_Note}/>

                        <View style={{ padding: 30 }} />
                        <ActionButton
                        title={t('common.confirm')}
                        onPress={()=> {verifyUserAttributeConfirm(type)}}
                        isEnable={!isEmpty(emailCode.value) && emailCode.value.length === 6}
                        />    
                    </View>
                }
               
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100
    },
});

export default UserVerification;