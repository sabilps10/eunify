import React, { useLayoutEffect } from 'react';
import {
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';

import { useTranslation } from 'react-i18next';

import { Props } from '../../routes/route';

import TextLabel from '../../components/forms/labels/textLabel.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import Header from '../../components/header/header.component';
import { SafeAreaView } from 'react-native-safe-area-context';
import { maskEmail } from '../../utils/stringUtils';
import ErrorLogin from '../../../assets/svg/ErrorLogin';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';


const ForceChangePassword: React.FC<Props<'ForceChangePassword'>> = ({ route, navigation }) => {
  const [t, i18n] = useTranslation();
  const { username, skipVerification } = route.params;

  useLayoutEffect(() => {
    navigation.setOptions({
        header: () => <Header 
            leftComponents={<HeaderButton 
                iconComponent={<TopBarBack color={colors.MainFont} />}
                onPress={onPressBack} />}
        />
    });
  }, [navigation]);

  const onPressBack = () => { navigation.goBack() }

  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors)
  const scheme = useColorScheme();

  return (
    <SafeAreaView 
      edges={['bottom']}
      style={[globalStyles.container, styles.container, { backgroundColor: colors.White }]}>
      <View style={[styles.container]}>
          <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
            <View style={styles.centerWarningContainer}>
                <ErrorLogin color={colors.Red} />
                <View style={{padding: 5}}/>
                <TextLabel label={t('forceChangePassword.title')} labelStyle={[globalStyles.H4, { color: colors.Brand2, textAlign: 'center', marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]} />
            </View>
          </View>
      </View>
      <View style={[styles.bottomContainer, { backgroundColor: colors.White, marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
        <ActionButton
          title={t('forceChangePassword.button')}
          onPress={()=> {
            if (skipVerification) {
              navigation.navigate('CognitoForgetPasswordReset', { isChangePassword: true, skipVerification: skipVerification })
            } else {
              // navigation.navigate('CognitoForgetPasswordVerification', {username: username, displayUsername: maskEmail(username)})
              navigation.navigate('CognitoForgetPasswordReset', { isChangePassword: false, skipVerification: undefined, username: username, displayUsername: maskEmail(username) })
            }
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  centerWarningContainer: { 
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: 10
  },
  row: {
    flexDirection: 'row'
  },
  companyLogo: {
    height: 40,
    width: 40
  },
  companyLogoView: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 100,
  },
});

export default ForceChangePassword;