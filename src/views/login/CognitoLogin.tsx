import React, { useLayoutEffect, useState, useEffect, useCallback } from 'react';
import { StyleSheet, useColorScheme, View, Pressable, Alert, Text, NativeModules, Platform, BackHandler } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import TextLabel from '../../components/forms/labels/textLabel.component';
import MediumButton from '../../components/buttons/mediumButton.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useFocusEffect, useTheme } from '@react-navigation/native';

import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics'

import commonStyles from '../../styles/styles';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../src/redux/root-reducer';
import { Auth } from 'aws-amplify';
import { AccountUtils } from '../../utils/accountUtils';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import TabBarView from '../../components/tabBarView.component';
import LoginWithPassword from './LoginWithPassword';
import { CognitoActionCreators } from '../../redux/cognito/actionCreator';
import { CognitoTempActionCreators } from '../../redux/cognitoTemp/actionCreator';
import { isEmpty, maskEmail, maskPhoneFull } from '../../utils/stringUtils';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { addPasswordCount, completeSignUp, logout } from '../../utils/api/userApi';
import MeLogo from '../../../assets/svg/MeLogo';
import MeLogo2 from '../../../assets/svg/MeLogo2';
import Fingerprint from '../../../assets/svg/Fingerprint';
import FaceId from '../../../assets/svg/FaceId';
import FaceIdFingerprint from '../../../assets/svg/FaceIdFingerprint';
import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { THEME_TYPE } from '../../types/types';
import { errorType } from '../../utils/errorUtil';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { setChartURL, setTradingURL } from '../../utils/api/apiSetter';
import { Status } from '../../redux/connection/type';
import { CognitoState } from '../../redux/cognito/types';

const Tab = createMaterialTopTabNavigator();

enum LoginType {
    Email = 0,
    Mobile = 1
}

const CognitoLogin: React.FC<Props<'CognitoLogin'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const insets = useSafeAreaInsets()
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const [sensorType, setSensorType] = useState<string>('');

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowMessageDialog, SetSetting, SetIsManualLogout } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLoginCredentials } = bindActionCreators(CognitoActionCreators, dispatch);
    const { SetLoginTempCredentials } = bindActionCreators(CognitoTempActionCreators, dispatch);
    // const { SetIsLogined } = bindActionCreators(AccountActionCreators, dispatch);

    const isEnableBiometrics = useSelector((state: State) => state.state.setting?.IsEnableBiometrics);
    // const [isBiometricsSaved, SetIsBiometricsSaved] = useState<boolean>(isEnableBiometrics ?? false); //TODO Get this from Redux
    const setting = useSelector((state: State) => state.state.setting);
    // const userId = useSelector((state: State) => state.state.userId);
    const cognitoState = useSelector((state: State) => state.cognito);
    const regionDataList = useSelector((state: State) => state.state.regionDataList);

    const [loginType, setLoginType] = useState(LoginType.Email)

    const [isBiometricsLogin, setIsBiometricsLogin] = useState<boolean>(isEnableBiometrics ? true : false);
    const [usernameError, setUsernameError] = useState<{ isShow: boolean, message: string }>({ isShow: false, message: '' });
    const [passwordError, setPasswordError] = useState<{ isShow: boolean, message: string }>({ isShow: false, message: '' });
    const [userId] = useState<string>(isEnableBiometrics);
    const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)

    const { doLogin, doConnectWebsocket, signOut, doCloseWebsocket } = AccountUtils();

    useFocusEffect(
        useCallback(() => {

            const backAction = async () => {
                onPressBack()
                return true;
            };

            const backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );

            return () => {
                backHandler.remove();
            }
        }, [])
    );

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header
                style={{ backgroundColor: colors.Brand3 }}
                leftComponents={<HeaderButton
                    title={t('cognitoLogin.login')}
                    iconComponent={<TopBarBack color={colors.White} />}
                    onPress={onPressBack}
                    titleStyle={{ ...globalStyles.H4, color: colors.White }} />}
            />
        });
    }, [navigation]);

    const onPressBack = () => {
        // navigation.goBack() 
        if (route.params) {
            if (route.params.backPage) {
                navigation.navigate(route.params.backPage, route.params.backPageParam);
                return
            }
        }

        navigation.navigate('TabDirectory', { screen: TabDirectoryTab.QUOTE });
    }

    useEffect(() => {

        const fetchData = async () => {
            console.debug('fetch route.params', route.params)
            SetSetting({ IsEnableBiometrics: undefined })
            try {
                const session = await Auth.currentSession();
                if (session !== null) {
                    var payload: { [key: string]: any; } = session.getIdToken().payload;
                    var cognitoUserName = payload['cognito:username'];
                    logout(cognitoUserName).then(() => {
                        signOut()
                    })
                } else {
                    signOut()
                }
            } catch (error) {
                signOut()
            }
            setIsBiometricsLogin(false);
        }

        console.debug('route.params 1', route.params)
        if (route.params) {
            console.debug('route.params 2', route.params.forceLogin)
            if (route.params.forceLogin) {
                console.debug('route.params forceLogin')
                fetchData().catch(console.error);
            }
        }

        if (Platform.OS === "android") {
            setSensorType('biometrics');
        }
        else {
            const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })

            rnBiometrics.isSensorAvailable()
                .then((resultObject) => {
                    const { available, biometryType } = resultObject

                    if (!available)
                        setSensorType(undefined);

                    if (biometryType === BiometryTypes.TouchID) {
                        console.debug("TouchID is supported")
                        setSensorType('touch');
                    } else if (biometryType === BiometryTypes.FaceID) {
                        console.debug('FaceID is supported')
                        setSensorType('face');
                    } else if (biometryType === BiometryTypes.Biometrics) {
                        console.debug("Biometrics is supported")
                        setSensorType('biometrics');
                    }
                })
        }
    }, [route.params])

    const onPressLoginWithPassword = () => { setIsBiometricsLogin(false) }

    const onPressBiometricLogin = async () => {
        SetIsShowLoading(true)
        console.debug('onPressBiometricLogin')
        if (!cognitoState.username || !cognitoState.password) {
            console.debug("Biometric Login not ready");
            setIsBiometricsLogin(false)
            SetIsShowLoading(false)
            return;
        }
        const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })
        var rnBiometricsSuccess = false;
        try {
            if (Platform.OS === "android") {
                const { BiometricNativeModule } = NativeModules;
                const result = await BiometricNativeModule.startBiometric(i18n.language,);
                // console.debug('BiometricNativeModule result', result)
                if (result === 1) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                } else {
                    SetIsShowLoading(false)
                }
            } else {
                const resultObject = await rnBiometrics.simplePrompt({ promptMessage: 'Confirm fingerprint' })
                const { success } = resultObject

                if (success) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                }
                else {
                    SetIsShowLoading(false)
                }
            }
        }
        catch (error) {
            console.error(`biometrics failed: ${error}`);
            SetIsShowLoading(false)
        }

        console.debug('rnBiometricsSuccess ' + rnBiometricsSuccess)

        if (rnBiometricsSuccess) {
            console.debug('onPressBiometricLogin success')

            try {
                const user = await Auth.signIn(
                    cognitoState.username,
                    cognitoState.password
                );
                const session = await Auth.currentSession();
                if (user && session) {
                    var pushyDeviceID = setting?.PushyDeviceID ?? "";
                    var region: string = setting?.UserRegion;
                    //var data = await loginLevel(pushyDeviceID, region, false)
                    var data = await doLogin(pushyDeviceID, region, false)
                    if (data && data.success) {
                        SetLoginTempCredentials({
                            username: cognitoState.username,
                            password: cognitoState.password
                        })
                        SetIsManualLogout(false);
                        var page = data.redirect
                        switch (page) {
                            case 'AddDevices':
                                const session = await Auth.currentSession();
                                var payload: { [key: string]: any; } = session.getIdToken().payload;
                                var phoneNumber = payload['phone_number'];

                                navigation.navigate('AddDevices', { displayPhoneNumber: maskPhoneFull(phoneNumber, regionDataList) })
                                break;
                            case 'TabDirectory':
                                await doCloseWebsocket(true);
                                await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                                // navigation.navigate('TabDirectory')
                                navigation.navigate('Tab_WatchlistView')
                                break;
                        }

                    }
                    // else {
                    //     SetIsShowMessageDialog({ isShowDialog: true, title: t('cognitoLogin.error.loginFailed'), message: t('cognitoLogin.error.biometricFailed'), callback: onPressLoginWithPassword })
                    // }
                    else {
                        var page = data.redirect
                        var param = data.redirectParam!;
                        switch (page) {
                            case 'UserVerification':
                                // { type: string, callBack: (phone: string, phonePrefix: string) => void }
                                navigation.navigate('UserVerification', {
                                    ...param, callBack: async (phone, phonePrefix) => {
                                        console.debug('callBack')
                                        onPasswordLogin(cognitoState.username, cognitoState.password)
                                    }
                                })
                                break;
                        }
                    }
                    SetIsShowLoading(false);
                }
            } catch (e) {
                SetIsShowLoading(false);
                SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.resetPassword'), message: t('common.error.resetPasswordMsg'), btnLabel: t('common.confirm'), 
                callback: ()=> {
                    SetLoginCredentials({
                        username: "",
                        password: ""
                    } as CognitoState);
                    SetSetting({
                        IsSkipRegBiometric: false,
                        IsEnableBiometrics: undefined
                    })
                    onPressLoginWithPassword()
                } })
            }

        } else {
            SetIsShowMessageDialog({ isShowDialog: true, title: t('cognitoLogin.error.loginFailed'), message: t('cognitoLogin.error.biometricFailed'), callback: onPressLoginWithPassword })
        }
    }

    const renderSensorIcon = (sensorType) => {
        switch (sensorType) {
            case "touch":
                return <Fingerprint color={colors.TitleFont} />
            case "face":
                return <FaceId color={colors.TitleFont} />
            case "biometrics":
                return <FaceIdFingerprint color={colors.TitleFont} />
            default:
                return <></>
        }
    }

    const renderSensorText = (sensorType) => {
        switch (sensorType) {
            case "touch":
                return <TextLabel label={t('cognitoLogin.loginTouch')} labelStyle={[globalStyles.H4, { color: colors.TitleFont }]} />
            case "face":
                return <TextLabel label={t('cognitoLogin.loginFace')} labelStyle={[globalStyles.H4, { color: colors.TitleFont, textAlign: 'center' }]} />
            case "biometrics":
                return <TextLabel label={t('cognitoLogin.loginBiometric')} labelStyle={[globalStyles.H4, { color: colors.TitleFont }]} />
            default:
                return <></>
        }
    }

    const onPressTabBarItem = (index: number) => {
        setLoginType(index)
    }

    const onPressForgotPassword = () => {
        navigation.navigate('CognitoForgetPassword');
    }

    const completedVerification = async (username, password, accessToken, phone, phonePrefix) => {
        console.debug('completedVerification 1')
        try {
            const session = await Auth.currentSession();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var cognitoUserName = payload['cognito:username'];
            var email = payload['email'];


            const data = await completeSignUp(cognitoUserName, email, phone, phonePrefix)

            const user = await Auth.signIn(
                username,
                password
            );

            if (data) {
                // console.debug('data', data)
                // if (data.status === '0') {
                await onPasswordLogin(username, password);
                // }
            }
        } catch (e) {
            console.debug(e.toString())
        }
    }

    const onPasswordLogin = async (username, password) => {
        setPasswordError({ isShow: false, message: '' })
        setUsernameError({ isShow: false, message: '' })
        SetIsShowLoading(true);
        try {
            // console.debug(`signIn ${username}, ${password}`);
            const user = await Auth.signIn(
                username,
                password
            );
            // console.debug('Login user', user);
            // const session = await Auth.currentSession();
            if (user !== null) {
                console.debug('Login Success');
                // console.debug(JSON.stringify(user));

                var pushyDeviceID = setting?.PushyDeviceID ?? "";
                var region: string = setting?.UserRegion;
                //var data = await loginLevel(pushyDeviceID, region, false)
                if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
                    navigation.navigate('ForceChangePassword', { username: username, skipVerification: { isSkip: true, username: username, password: password } })
                } else {
                    var data = await doLogin(pushyDeviceID, region, false, undefined, isEmpty(cognitoState.username) ? false : (cognitoState.username !== username))
                    if (data) {
                        SetLoginTempCredentials({
                            username: username,
                            password: password
                        })
                        SetIsManualLogout(false);

                        if (data.success) {
                            var page = data.redirect
                            var param = data.redirectParam!;

                            switch (page) {
                                case 'AddDevices':
                                    const session = await Auth.currentSession();
                                    var payload: { [key: string]: any; } = session.getIdToken().payload;
                                    var phoneNumber = payload['phone_number'];

                                    navigation.navigate('AddDevices', { displayPhoneNumber: maskPhoneFull(phoneNumber, regionDataList) })
                                    break;
                                case 'EnableBiometrics':
                                case 'TabDirectory':
                                    await doCloseWebsocket(true);
                                    const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                                    if (status === 101) {
                                        SetAccountDisplayName(data.payload.currentTrader);
                                        setTradingURL(data.payload.tApiPath);
                                        setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                                    }
                                    if (page === 'TabDirectory') {
                                        navigation.navigate('Tab_WatchlistView')
                                    } else {
                                        navigation.navigate(page)
                                    }
                                    break;
                            }
                        } else {
                            var page = data.redirect
                            var param = data.redirectParam!;
                            switch (page) {
                                case 'UserVerification':
                                    // { type: string, callBack: (phone: string, phonePrefix: string) => void }
                                    navigation.navigate('UserVerification', {
                                        ...param, callBack: async (phone, phonePrefix) => {
                                            console.debug('callBack')
                                            await completedVerification(username, password, data.accessToken, phone, phonePrefix)
                                        }
                                    })
                                    break;
                            }
                        }
                    } else {
                        SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: '' })
                    }
                }
            }
        } catch (error) {
            console.debug('error signing in: ', error, typeof (error));
            // SetIsShowMessageDialog({isShowDialog: true, title: '', message: t('common.unknownError')})
            if (error instanceof Error) {
                console.debug(error.name)
                if (error.name === 'NotAuthorizedException') {
                    var data2 = await addPasswordCount(username)
                    console.debug('data2', data2)
                    if (data2) {
                        if (data2.errorCode !== 0) {
                            if (data2.errorCode === errorType.AUTH) {
                                SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.auth'), message: t('common.error.code') + data2.errorCode })
                            } else if (data2.errorCode === errorType.NETWORK) {
                                SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data2.errorCode })
                            } else if (data2.errorCode === errorType.LOCK) {
                                SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.lockTitle'), message: t('common.error.lock') })
                            }
                        } else {
                            setPasswordError({ isShow: true, message: t('cognitoLogin.error.incorrectPassword') })
                        }
                    } else {
                        setPasswordError({ isShow: true, message: t('cognitoLogin.error.incorrectPassword') })
                    }
                } else if (error.name === 'UserNotFoundException') {
                    setUsernameError({ isShow: true, message: loginType === LoginType.Email ? t('cognitoLogin.error.invalidEmail') : t('cognitoLogin.error.invalidMobile') })
                } else if (error.name === 'PasswordResetRequiredException') {
                    console.debug(error.name)
                    // navigation.navigate('CognitoForgetPasswordVerification', {username: username, displayUsername: maskEmail(username)})
                    navigation.navigate('ForceChangePassword', { username: username })
                }
                else {
                    SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: '' })
                }
            } else {
                if (error.includes('No current user')) {
                    navigation.navigate('ForceChangePassword', { username: username })
                }
            }

        } finally {
            SetIsShowLoading(false);
        }
    }

    const onSignUp = () => {
        navigation.navigate("CognitoSignUp")
    }

    return (
        <KeyboardAwareScrollView
            extraScrollHeight={40}
            style={{ backgroundColor: colors.Brand3 }}
            contentContainerStyle={[commonStyles.ScrollViewContainerStyle, { paddingBottom: insets.bottom }]}
            showsVerticalScrollIndicator={false}
        // keyboardShouldPersistTaps='handled'
        >
            <View style={styles.container}>
                <View style={styles.companyLogoView}>
                    <MeLogo2 style={styles.meLogo} color={colors.Black} backgroundColor={'#c4c4c4'} />
                </View>
                <View style={[commonStyles.contentContainer, { flex: 0 }]}>
                    <TextLabel label={t('cognitoLogin.welcome')} labelStyle={[globalStyles.H1, { color: colors.TitleFont }]} />
                </View>
                {
                    isBiometricsLogin && sensorType ?
                        <View style={commonStyles.contentContainer}>
                            <View>
                                <TextLabel label={userId} labelStyle={{ color: colors.TitleFont }} />
                                <View style={{ padding: 20 }} />
                                <PressableComponent
                                    onPress={onPressBiometricLogin}>
                                    <View style={styles.biometricsLoginLogoView}>
                                        {
                                            renderSensorIcon(sensorType)
                                        }
                                    </View>
                                    <View style={{ padding: 5 }} />
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        {
                                            renderSensorText(sensorType)
                                        }
                                    </View>
                                </PressableComponent>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <MediumButton title={t('cognitoLogin.loginWithPassword')} type='White' onPress={onPressLoginWithPassword} />
                            </View>
                        </View>
                        :
                        <View style={[commonStyles.contentContainer]}>
                            <TabBarView
                                style={{ height: 36 }}
                                labelTitles={[t('cognitoLogin.tab.emailAddress')]}
                                labelColor={colors.FixedTitleFont}
                                indicatorColor={colors.FixedIndicator}
                                indicatorHeight={2}
                                onPressTabBarItem={onPressTabBarItem} />
                            {
                                loginType === LoginType.Email ?
                                    <LoginWithPassword type='email' onPressForgotPassword={onPressForgotPassword} onLogin={onPasswordLogin} onSignup={onSignUp} usernameError={usernameError} passwordError={passwordError} />
                                    :
                                    <LoginWithPassword type='mobile' onPressForgotPassword={onPressForgotPassword} onLogin={onPasswordLogin} onSignup={onSignUp} usernameError={usernameError} passwordError={passwordError} />
                            }
                        </View>
                }
            </View>
        </KeyboardAwareScrollView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        paddingTop: 5,
        flex: 1
    },
    containerBiometrics: {
        flex: 1
    },
    bottomContainerBiometrics: {
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 8,
        marginBottom: 8
    },
    tabActiveHeader: {
        fontFamily: 'Montserrat',
        fontWeight: '600',
        fontSize: 13,
        lineHeight: 14.3,
    },
    tabHeader: {
        fontFamily: 'Montserrat',
        fontWeight: '600',
        fontSize: 13,
        lineHeight: 14.3,
    },
    biometricsLoginLogo: {
        height: 81,
        width: 81
    },
    biometricsLoginLogoView: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: 130,
    },
    companyLogo: {
        height: 40,
        width: 40
    },
    companyLogoView: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: 130,
    },
    meLogo: {
        height: 40,
        width: 40,
    },
})

export default CognitoLogin;