import React, { useState, useContext, useRef, useEffect, useReducer, useCallback } from 'react';
import { StyleSheet, Text, View, Pressable } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useFocusEffect, useTheme } from '@react-navigation/native';

import Input from '../../components/forms/inputs/input.component';
import BigButton from '../../components/buttons/bigButton.component';

import PasswordEyeIcon from '../../components/forms/labels/passwordEyeIcon.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import InputDropDownList from '../../components/forms/inputs/inputDropdownList.component';

import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';

import { State } from '../../redux/root-reducer';

import commonStyles from '../../styles/styles'
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { getTranslationDict, hasInValidCharacters, isEmail } from '../utils/CommonFunction';
import { PasswordLoginForm } from '../../types/InputTypes';
import { TouchableOpacity } from 'react-native-gesture-handler';
import PressableComponent from '../utils/PressableComponent';
import TouchableOpacityComponent from '../utils/TouchableOpacityComponent';

interface LoginWithPasswordInterface {
    type: string
    onLogin: (username, password) => void
    onSignup: () => void,
    onPressForgotPassword: () => void,
    usernameError?: {isShow: boolean, message: string},
    passwordError?: {isShow: boolean, message: string}
}

const LoginWithPassword = (props: LoginWithPasswordInterface) => {

    const [t, i18n] = useTranslation();
    // const selectionOptions = ["852", "853"];

    const { colors } = useTheme();
    const [isEnablePassword, SetIsEnablePassword] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);

    // const selectedPhonePrefix = useSelector((state: State) => state.selection.selected?.loginPhonePrefix);
    const regionDataList = useSelector((state: State) => state.state.regionDataList);

    const initialState: PasswordLoginForm = {
        password: { value: "", error: "", mandatory: true, maxLength: 50, isValid: false },
        email: { value: "", error: "", mandatory: true, maxLength: 100, isValid: false },
        // password: { value: "P@ssw0rd!", error: "", mandatory: true, maxLength: 50, isValid: false },
        // email: { value: "efsg.test1005@gmail.com", error: "", mandatory: true, maxLength: 100, isValid: false },
        phonePrefix: { value: regionDataList[0].countryCode, error: "", mandatory: true, isValid: true },
        phoneNumber: { value: "", error: "", mandatory: true, maxLength: 20, isValid: false },
        type: props.type
    }

    const formsReducer = (state: PasswordLoginForm, action) => {
        const { name, value, otherValue } = action.data;
        let error = "";
        switch (action.type) {
            case "RESET_FORM":
                return initialState;
            case "UPDATE_FORM":
                return {
                    ...state,
                    [name]: {
                        ...state[name],
                        value: value,
                        error: error,
                        isValid: error === "" ? true: false
                    }
                }
            case "VALID_FORM":
                error = validateInput(name, value, otherValue, state[name].mandatory);
                return {
                    ...state,
                    [name]: {
                        ...state[name],
                        error: error,
                        isValid: error === "" ? true: false
                    }
                }
            case "VALID_FORM_COGNITO":
                switch (name) {
                    case "email":
                    case "phoneNumber":
                        error = props.usernameError.isShow ? props.usernameError.message : ''
                    break;
                    case "password":
                        error = props.passwordError.isShow ? props.passwordError.message : ''
                    break;
                }
                return {
                    ...state,
                    [name]: {
                        ...state[name],
                        error: error,
                        isValid: error === "" ? true: false
                    }
                }
            default:
                return state;
        }
    }

    const validateInput = (name, value, otherValue, isMandatory) => {

        if (isMandatory && value.trim() === "") {
            return t("error.mandatory");
        }

        let error = "";
        switch (name) {
            case "phoneNumber":

                break;
            case "email":
                if (!isEmail(value)) {
                    error = t("error.invalidEmailAddress");
                }
                break;
            case "password":
                break;
            case "confirmPassword":
                if (value !== otherValue) {
                    error = t("error.inconsistentPassword");
                }
                break;
            default:
                break
        }
        return error;
    }

    const [formState, formDispatch] = useReducer(formsReducer, initialState);

    // useEffect(() => {
    //     if (selectedPhonePrefix)
    //         formDispatch({ type: "UPDATE_FORM", data: { name: "phonePrefix", value: selectedPhonePrefix } })
    // }, [selectedPhonePrefix])

    useFocusEffect(
        useCallback(() => {
            formDispatch({ type: "RESET_FORM", data: {} })
        }, [])
    );


    useEffect(()=> {
        if (props.usernameError && props.usernameError.isShow) {
            formDispatch({ type: "VALID_FORM_COGNITO", data: { name: "email" } })
        }
        if (props.passwordError && props.passwordError.isShow) {
            formDispatch({ type: "VALID_FORM_COGNITO", data: { name: "password" } })
        }
    }, [props.usernameError, props.passwordError])

    const callbackEnablePasswordVisibility = (passwordVerfied: boolean) => {
        SetIsEnablePassword(!isEnablePassword)
    };

    const onPressLogin = () => {
        if (props.type === "email") {
            formDispatch({ type: "VALID_FORM", data: { name: "email", value: formState.email.value } })
            formDispatch({ type: "VALID_FORM", data: { name: "password", value: formState.password.value } })

            const emailError = validateInput("email", formState.email.value, "", formState.email.mandatory);
            const passwordError = validateInput("password", formState.password.value, "", formState.password.mandatory);

            if (emailError !== "" || passwordError !== "")
                return;

            props.onLogin(formState.email.value, formState.password.value);
        }
        else if (props.type === "mobile") {
            formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: formState.phoneNumber.value } })
            formDispatch({ type: "VALID_FORM", data: { name: "password", value: formState.password.value } })

            const phoneError = validateInput("phoneNumber", formState.phoneNumber.value, "", formState.phoneNumber.mandatory);
            const passwordError = validateInput("password", formState.password.value, "", formState.password.mandatory);
            
            if (phoneError !== "" || passwordError !== "")
                return;

            props.onLogin("+" + formState.phonePrefix.value + formState.phoneNumber.value, formState.password.value)
        }
    }

    const onPressForgotPassword = () => {
        props.onPressForgotPassword();
    }

    const onPressSignUp = () => {
        props.onSignup();
    }

    const onPressShowAll = () => {
        // console.debug("onPressAll");
        // OpenCloseSelection({
        //     selectedOption: formState.phonePrefix.value,
        //     selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
        //     isOpen: true,
        //     location: 'loginPhonePrefix'
        // })

        //navigation.navigate('CountryOptionView', {type: CountryOptionType.COUNTRY, onSelect: onSelectCountryCodeOption})
    }

    return (
        <View style={[commonStyles.container, { backgroundColor: colors.Brand3 }]}>
            {
                props.type === 'email' ?
                    <>
                        <View style={commonStyles.inputPadding} />
                        <TextLabel label={t('cognitoSignUp.emailAddress')} labelStyle={{ color: colors.FixedTitleFont }} />
                        <Input
                            style={{backgroundColor: colors.FixedInputBg}}
                            inputStyle={{color: colors.FixedMainFont}}
                            type='email'
                            value={formState.email}
                            onChangeText={(value) => formDispatch({ type: "UPDATE_FORM", data: { name: "email", value: value } })}
                            onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "email", value: formState.email.value } })}
                        />
                    </>
                :
                    <>
                        <View style={commonStyles.inputPadding} />
                        <TextLabel label={t('cognitoSignUp.mobileNumber')} labelStyle={{ color: colors.FixedTitleFont }} />
                        <View style={{ flexDirection: 'row' }}>
                            <PressableComponent onPress={onPressShowAll}>
                                <InputDropDownList
                                    value={formState.phonePrefix.value}
                                />
                            </PressableComponent>
                            <View style={{ width: 10 }} />
                            <Input
                                style={{backgroundColor: colors.FixedInputBg}}
                                inputStyle={{color: colors.FixedMainFont}}
                                value={formState.phoneNumber}
                                onChangeText={(value) => formDispatch({ type: "UPDATE_FORM", data: { name: "phoneNumber", value: value } })}
                                onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "phoneNumber", value: formState.phoneNumber.value } })}
                            />
                        </View>
                    </>
            }
            <View style={commonStyles.inputPadding} />
            <TextLabel label={t('cognitoSignUp.password')} labelStyle={{ color: colors.FixedTitleFont }} />
            <Input
                style={{backgroundColor: colors.FixedInputBg}}
                inputStyle={{color: colors.FixedMainFont}}
                value={formState.password}
                type={isEnablePassword ? 'text' : 'password'}
                onChangeText={(value) => {
                        if (!hasInValidCharacters(value)) {
                            formDispatch({ type: "UPDATE_FORM", data: { name: "password", value: value } })
                        }
                    }
                }
                onEndEditing={(e) => formDispatch({ type: "VALID_FORM", data: { name: "password", value: formState.password.value } })}
                rightComponent={<PasswordEyeIcon iconColor={colors.FixedMainFont} isEnable={isEnablePassword} callback={callbackEnablePasswordVisibility} />}
            />

            <View style={{ padding: 5 }} />
            <View style={{flex:0, alignContent: 'flex-end', alignItems: 'flex-end'}}>
                <TouchableOpacityComponent onPress={onPressForgotPassword} >
                    <TextLabel label={t('cognitoLogin.toForgotPassword')} labelStyle={{ color: colors.FixedTitleFont, textAlign: 'right'}} />
                </TouchableOpacityComponent>
            </View>
            <View style={[commonStyles.bottomContainer]}>
                <MSmallButton title={t('cognitoLogin.notMemberToSignUp')} type="White" onPress={onPressSignUp} />
                <View style={{ padding: 7 }} />
                <BigButton
                    type='Solid'
                    title={t('cognitoLogin.login')}
                    onPress={onPressLogin}
                    style={{borderWidth: 1, borderColor: colors.White}}
                    textStyle={{color: colors.FixedEnableBtnFont}}
                />
                <View style={{padding: 4}}/>
            </View>

        </View>
    )
}

export default LoginWithPassword