import React, { useState, useLayoutEffect, useEffect } from 'react';
import { StyleSheet, View, Platform, Linking, NativeModules, BackHandler } from 'react-native';
import { Props } from '../../routes/route';
import { State } from '../../redux/root-reducer';

import Header from '../../components/header/header.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import ReactNativeBiometrics, { BiometryTypes } from 'react-native-biometrics'

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useSelector, useDispatch } from 'react-redux';
import IconTouch from '../../../assets/icon/IconTouch';
import IconFaceId from '../../../assets/icon/IconFaceId';
import IconBiometrics from '../../../assets/icon/IconBiometrics';
import { useAppState } from '@react-native-community/hooks';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { format } from 'react-string-format';
import { getApplicationName } from 'react-native-device-info';
import DeviceInfo from 'react-native-device-info'
import { CognitoActionCreators } from '../../redux/cognito/actionCreator';
import { MessageDialogContent } from '../../redux/state/types';

const EnableBiometrics: React.FC<Props<'EnableBiometrics'>> = ({ route, navigation }) => {

	const [t, i18n] = useTranslation();

    const isSetting = null;

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [sensorType, setSensorType] = useState<string>();

    const dispatch = useDispatch();
    const { SetSetting, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLoginCredentials } = bindActionCreators(CognitoActionCreators, dispatch);

    const isEnableBiometrics = useSelector((state: State) => state.state.setting?.IsEnableBiometrics);
    const userId = useSelector((state: State) => state.state.userId);
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);

    const appState = useAppState();

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
                leftComponents={<HeaderButton 
                    iconComponent={isSetting ? <TopBarBack color={colors.MainFont} /> : null} 
                    title={(isSetting ? t('meSetting.title') + ' - ' : '') + t('biometricsLogin.title')} 
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                    onPress={isSetting ? onPressBackBtn : null} />}
                />
        });
    }, [navigation]);

    useEffect(() => {
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            () => { return true;}
        );

        return () => {
            backHandler.remove();
        }
    }, []);

    useEffect(() => {    
        if (appState === 'active') {
            if (Platform.OS === "android") {
                setSensorType('biometrics');
            } 
            else {
                const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })

                rnBiometrics.isSensorAvailable()
                    .then((resultObject) => {
                        
                        const { available, biometryType } = resultObject

                        console.debug('resultObject', resultObject)
                        if (!available)
                            setSensorType(undefined);

                        if (biometryType === BiometryTypes.TouchID) {
                            console.debug("TouchID is supported")
                            setSensorType('touch');
                        } else if (biometryType === BiometryTypes.FaceID) {
                            console.debug('FaceID is supported')
                            setSensorType('face');
                        } else if (biometryType === BiometryTypes.Biometrics) {
                            console.debug("Biometrics is supported")
                            setSensorType('biometrics');
                        }
                    })
            }
        }

    
      }, [appState]);

    const onPressBackBtn = () => { navigation.goBack() }

    const onPressSkip = () => { 
        
        SetSetting({
            IsSkipRegBiometric: true
        })
        //navigation.navigate('TabDirectory')
        navigation.navigate('Tab_Me')
    }

    const onPressEnable = async() => { 
        const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })
        var rnBiometricsSuccess = false;
        var enrolledBio = true;
        if (sensorType) {
            if (Platform.OS === "android") {
                const {BiometricNativeModule} = NativeModules;
                const result = await BiometricNativeModule.startBiometric(i18n.language,);
                console.debug('BiometricNativeModule result', result)
                if (result === -1) { //Not Registered
                    enrolledBio = false;
                    console.debug('enrolledBio', enrolledBio)
                } else if (result === 1) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                }
            } else {
                await rnBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
                .then((resultObject) => {
                const { success } = resultObject

                if (success) {
                    console.debug('successful biometrics provided')
                    rnBiometricsSuccess = true;
                } else {
                    console.debug('user cancelled biometric prompt')
                }
                })
                .catch((e: Error) => {
                    // console.debug('biometrics failed', e.message)
                    // if (e.message.includes('No fingerprints enrolled')) {
                    //     enrolledBio = false
                    // } else if (e.message.includes('Too many attempts')) {
                    //     SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.tooManyAttempts'), message: ''})
                    // }
                    console.debug('biometrics failed', e)
                    if (e.message.includes('Too many attempts') || e.message.includes('嘗試次數過多') || e.message.includes('尝试次数过多')) {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.tooManyAttempts'), message: ''})
                    } else {
                        enrolledBio = false
                    }
                })
            }
        } 
        
        if (!sensorType || !enrolledBio){
            // Platform.OS === "android" ?
            // SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.notAvailable'), message: t('meSetting.biometrics.biometricDesc'), callback:()=>{Linking.sendIntent("android.settings.BIOMETRIC_ENROLL")}, isShowCancel: true, btnLabel: t('meSetting.biometrics.settingBtn')}) :
            // SetIsShowMessageDialog({isShowDialog: true, title: t('meSetting.biometrics.notAvailable'), message: t('meSetting.biometrics.touchIDDesc'), callback:()=>{}, isShowCancel: true, btnLabel: t('meSetting.biometrics.settingBtn')})

            let iOSBiometric = sensorType == 'face' ? t('meSetting.biometrics.faceIDDesc') : t('meSetting.biometrics.touchIDDesc')

            let isAndroid = Platform.OS === "android"
            let isShowDialog = true
            let isShowCancel = true
            const OsVer = Platform.constants['Release'];
            const brand = DeviceInfo.getBrand();
            let callBack = isAndroid ? (() => (OsVer < 11 || (OsVer == 11 && brand.toLowerCase().includes('samsung') )) ? Linking.sendIntent("android.settings.SETTINGS") : Linking.sendIntent("android.settings.BIOMETRIC_ENROLL")) : () => Linking.openURL('App-prefs:PASSCODE')
            let title = t('meSetting.biometrics.notAvailable')
            let message = isAndroid ? t('meSetting.biometrics.biometricDesc') : iOSBiometric
            let btnLabel = t('meSetting.biometrics.settingBtn')

            SetIsShowMessageDialog({
                isShowDialog: isShowDialog, 
                trackingContext: MessageDialogContent.TrackingContext.EnableBiometricNotAvailable,
                isShowCancel: isShowCancel, 
                callback: callBack, 
                title: title, 
                message: message, 
                btnLabel: btnLabel
            })

        }

        console.debug('rnBiometricsSuccess ' + rnBiometricsSuccess)

        if (rnBiometricsSuccess) {
            SetSetting({IsEnableBiometrics: userId});
            SetLoginCredentials({
                username: cognitoTempState.username,
                password: cognitoTempState.password
            })
            if (isSetting) {
                onPressBackBtn()
            } else {
                // navigation.navigate('TabDirectory')
                navigation.navigate('Tab_WatchlistView')
            }
            // navigation.navigate('AddDevices')
        } 
    }

    const renderSensorIcon = (sensorType) => {
        switch (sensorType) {
            case "touch":
                return <IconTouch color={colors.biometricsIcon} backgroundColor={colors.White} />
            case "face":
                return <IconFaceId color={colors.biometricsIcon} backgroundColor={colors.White} />
            case "biometrics":
            default:
                return <IconBiometrics color={colors.biometricsIcon} backgroundColor={colors.White} />
        }
    }

    const renderSensorText = (sensorType) => {
        switch (sensorType) {
            case "touch":
                return <TextLabel label={t('biometricsLogin.titleTouch')} labelStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
            case "face":
                return <TextLabel label={t('biometricsLogin.titleFace')} labelStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
            case "biometrics":
            default:
                return <TextLabel label={t('biometricsLogin.titleBiometric')} labelStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
        }
    }

    return (
        <SafeAreaView style={[globalStyles.container_mainBg, styles.container]}>
            <View style={[styles.body, {marginHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
                <View style={styles.biometricsLoginLogoView}>
                    {
                        renderSensorIcon(sensorType)
                    }
                </View>
                <View style={{ padding: 20 }} />
                {
                    renderSensorText(sensorType)
                }
                <View style={{ padding: 5 }} />
                <TextLabel label={format(t('biometricsLogin.desc'), getApplicationName(), getApplicationName())}/>
            </View>
            {
                <View style={[styles.bottomContainer, {marginHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
                    {
                        isSetting ? null : <MSmallButton title={t('biometricsLogin.skipNow')} type='White' textStyle={globalStyles.Note} onPress={onPressSkip} />
                    }
                    <ActionButton
                        title={t('biometricsLogin.enable')}
                        onPress={onPressEnable}
                    />  
                </View>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100
    },
    body: {
        flex: 1
    },
    bottomContainer: {
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 8,
    },
    biometricsLoginLogo: {
        height: 81,
        width: 81
    },
    biometricsLoginLogoView: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: 130,
    }
});

export default EnableBiometrics;