import React, { useState, useLayoutEffect, useEffect } from 'react';
import { StyleSheet, useColorScheme, View, Alert, ScrollView } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import TextCounter from '../../components/forms/labels/textCounter.component';
import Header from '../../components/header/header.component';

import { Auth } from 'aws-amplify';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { resendCodeSecond } from '../../config/constants';
import { isEmpty } from '../../utils/stringUtils';
import { InputType } from '../../types/InputTypes';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { format } from 'react-string-format';
import { validNumber } from '../utils/CommonFunction';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { AccountUtils } from '../../utils/accountUtils';
// import { SetLoginCredentials } from '../../redux/cognito/action';
import { CognitoState } from '../../redux/cognito/types';
// import { SetSetting } from '../../redux/state/action';
import { Status } from '../../redux/connection/type';
import { SetConnectionStatus } from '../../redux/connection/action';
import { CognitoActionCreators } from '../../redux/cognito/actionCreator';
import { State } from '../../redux/root-reducer';

const Tab = createMaterialTopTabNavigator();

const CognitoForgetPasswordVerification: React.FC<Props<'CognitoForgetPasswordVerification'>> = ({ route, navigation }) => {

    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header
                leftComponents={<HeaderButton
                    iconComponent={<TopBarBack color={colors.MainFont} />}
                    title={t('cognitoForgetPassword.titleVerification')}
                    titleStyle={[globalStyles.H4, { color: colors.Brand2 }]}
                    onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    const { username, displayUsername, password } = route.params;
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [emailCode, setEmailCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowToastDialog, SetIsShowMessageDialog, SetSetting } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLoginCredentials } = bindActionCreators(CognitoActionCreators, dispatch); 

    const cognitoState = useSelector((state: State) => state.cognito);

    const { updateProfilePassword } = AccountUtils();

    const onPressBackBtn = () => { navigation.goBack() }

    useEffect(() => {
        resendForgotPassword();
    }, [])

    const resendForgotPassword = async () => {
        Auth.forgotPassword(username)
            .then(data => {
                console.debug(data);
            }
            )
            .catch(err => console.debug(err));
    };
    const toResetPassword = async () => {
        console.debug('toResetPassword')
        // navigation.navigate('CognitoForgetPasswordReset', { isChangePassword: false, username: username, emailCode: emailCode.value })
        await Auth.forgotPasswordSubmit(username, emailCode.value, password)
            .then(async data => {
                console.debug(data)
                // Alert.alert(data.toString())

                if (cognitoState.username === username) {
                    SetLoginCredentials({
                        username: "",
                        password: ""
                    } as CognitoState)

                    SetSetting({
                        IsSkipRegBiometric: false,
                        IsEnableBiometrics: undefined
                    })
                }

                // SetConnectionStatus(Status.ResetPassword)

                SetIsShowToastDialog({
                    isShowDialog: true,
                    message: t('cognitoForgetPassword.success'),
                    isSuccess: true
                })

                //Call EMP Change Password
                await updateProfilePassword(password, username)

                navigation.navigate('CognitoLogin')
            })
            .catch(err => {
                console.debug(err)
                if (err instanceof Error) {
                    if (err.name === 'CodeMismatchException') {
                        setEmailCode({ ...emailCode, error: t('error.incorrectCode'), value: '' })
                    }
                }

                // Alert.alert(err.toString())
            });
    }

    const callbackEmail = (callbackFunction: boolean) => {
        resendForgotPassword();
    };

    return (
        <ScrollView style={[globalStyles.container, styles.container, { paddingBottom: insets.bottom, backgroundColor: colors.MainBg }]}
            keyboardShouldPersistTaps='handled'>
            <View style={{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }}>
                <Input
                    label={t('cognitoSignUpVerification.enterEmailCode')}
                    placeholder=''
                    value={emailCode}
                    onChangeText={(value) => {
                        if (validNumber(value)) {
                            setEmailCode({ ...emailCode, value, error: '' })
                        }
                    }}
                    rightComponent={<TextCounter counter={resendCodeSecond} callback={callbackEmail} />}
                    type='number'
                />
                <View style={{ padding: 5 }} />
                <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), displayUsername)} labelStyle={[globalStyles.Small_Note, { color: colors.MainFont }]} />

                <View style={{ padding: 30 }} />
                <ActionButton
                    title={t('common.submit')}
                    onPress={toResetPassword}
                    isEnable={!isEmpty(emailCode.value) && emailCode.value.length === 6}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100
    },
});

export default CognitoForgetPasswordVerification;