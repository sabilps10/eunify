import React, { useState, useLayoutEffect, useEffect } from 'react';
import { StyleSheet, useColorScheme, View, KeyboardAvoidingView, ScrollView } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Header from '../../components/header/header.component';
import TextLabel from '../../components/forms/labels/textLabel.component';

import Input from '../../components/forms/inputs/input.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { useTranslation } from 'react-i18next';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { Auth } from 'aws-amplify';
import { addUserDevice } from '../../utils/api/userApi';
import TextCounter from '../../components/forms/labels/textCounter.component';
import { allowSkipSMS, resendCodeSecond } from '../../config/constants';

import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { AccountUtils } from '../../utils/accountUtils';
import { isEmpty } from '../../utils/stringUtils';
import { InputType } from '../../types/InputTypes';
import commonStyles from '../../styles/styles'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { format } from 'react-string-format';
import { validNumber } from '../utils/CommonFunction';
import { setChartURL, setTradingURL,  } from '../../utils/api/apiSetter';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';

const Tab = createMaterialTopTabNavigator();

const AddDevices: React.FC<Props<'AddDevices'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const {displayPhoneNumber} = route.params;

    const [phoneNumber, SetPhoneNumber] = useState<string>('');
    // const [phoneCode, SetPhoneCode] = useState<string>('');
    const [errorPhone, SetErrorPhone] = useState<string | undefined>(undefined);

    const [phoneCode, setPhoneCode] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 6, isValid: false });

    const setting = useSelector((state: State) => state.state.setting);
    const cognitoState = useSelector((state: State) => state.cognito);
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    
    const dispatch = useDispatch();
    const { SetIsShowAddDevice, SetIsShowLoading, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)

    const { doLogin, doConnectWebsocket, doCloseWebsocket } = AccountUtils();

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
            leftComponents={<HeaderButton 
                iconComponent={<TopBarBack color={colors.MainFont} />} 
                title={t('addDevices.title')} 
                titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    useEffect(()=> {
        const fetchData = async () => {
            await verifyUserAttribute();
        }
        
        fetchData().catch(console.error);
       
    }, [])

    useEffect(() => {
        if (phoneCode.value.length === 6)
            checkPhoneCode()
    }, [phoneCode.value])

    const verifyUserAttribute = async () => {
        console.debug('verifyUserAttribute') 
        const user = await Auth.currentAuthenticatedUser();
        if (user) {
            SetPhoneNumber(user.attributes.phone_number)
        }
        const updateResult = await Auth.verifyUserAttribute(user, 'phone_number')
        console.debug('updateResult' + updateResult) 
    };

    const callbackPhone = () => {
        verifyUserAttribute()
    };

    const onPressAdd = async() => {
        try {
            SetIsShowLoading(true)
            const user = await Auth.currentAuthenticatedUser();
            var verify = false || allowSkipSMS;
            await Auth.verifyUserAttributeSubmit(user, 'phone_number', phoneCode.value).then((res) => {
                console.debug('verifyUserAttributeSubmit', res)
                verify = true; 
            }).catch((error) => {
                console.error('error', error)
                setPhoneCode({ ...phoneCode, error: t('error.incorrectCode'), value: '' })
            })

            if (!verify) return
            const session = await Auth.currentSession();
            var token = session.getAccessToken().getJwtToken();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var cognitoUserName = payload['cognito:username'];
            const data = await addUserDevice(cognitoUserName, token)
            console.debug('addUserDevice', data)
            if (data) {
                if (data.status === '0') {
                    SetIsShowAddDevice(true)
                    let startDologin = async () => {
                        SetIsShowLoading(true)
                        try {
                            var region: string = setting?.UserRegion;
                            var pushyDeviceID = setting?.PushyDeviceID ?? '';
                            // var data2 = await loginLevel(pushyDeviceID, region, true)
                            var data2 = await doLogin(pushyDeviceID, region, true, undefined, isEmpty(cognitoState.username) ? false : (cognitoState.username !== cognitoTempState.username))
                            if (data2 && data2.success) {

                                var page = data2.redirect
                                switch (page) {
                                    case 'AddDevices':
                                        navigation.navigate('AddDevices')
                                        break;
                                    case 'EnableBiometrics':
                                    case 'TabDirectory':
                                        await doCloseWebsocket(true);
                                        const status = await doConnectWebsocket(data2.paths, true, data2.accessToken, data2.loginLevel, false);
                                        if (status === 101)
                                        {
                                            SetAccountDisplayName(data2.payload.currentTrader);
                                            setTradingURL(data2.payload.tApiPath);
                                            setChartURL(data2.payload.chartPath, data2.payload.chartWSPath);
                                        }
                                        navigation.navigate(page)
                                        break;
                                }
                            }
                            else {
                                SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: '' })
                            }
                        }
                        finally
                        {
                            SetIsShowLoading(false)
                        }
                    }
                    setTimeout(startDologin, 2000);
                } else if (data.status === '1') {
                    navigation.navigate('DeviceList', { isFromAddDevice: true })
                }
            }
        } catch (error) {
            console.debug(error)
        }
        finally {
            SetIsShowLoading(false)
        }
    }

    const onPressBackBtn = () => { navigation.goBack() }

    const formsReducer = (state: PasswordLoginForm, action) => {
        const { name, value, otherValue } = action.data;
        let error = "";
        switch (action.type) {
            case "UPDATE_FORM":
                return {
                    ...state,
                    [name]: {
                        ...state[name],
                        value: value,
                        error: error,
                        isValid: error === "" ? true: false
                    }
                }
            case "VALID_FORM":
                error = validateInput(name, value, otherValue, state[name].mandatory);
                return {
                    ...state,
                    [name]: {
                        ...state[name],
                        error: error,
                        isValid: error === "" ? true: false
                    }
                }
            default:
                return state;
        }
    }

    const checkPhoneCode = async () => {
        console.debug('checkPhoneCode')
        if (phoneCode.value.length !== phoneCode.maxLength)
            return;
        await onPressAdd();
    };
    
    return (
        <KeyboardAvoidingView style={globalStyles.container_mainBg}>
        <ScrollView style={{paddingBottom: insets.bottom}}
        keyboardShouldPersistTaps='handled'>
            <View style={commonStyles.contentContainer}>
            <View style={{ height: 130 }} />
            <Input
                label={t('cognitoSignUpVerification.enterPhoneCode')}
                placeholder=''
                value={phoneCode}
                onChangeText={(value) => {
                    if (validNumber(value)){
                        setPhoneCode({ ...phoneCode, value })
                    }
                }}
                // onEndEditing={(e) => checkPhoneCode()}
                rightComponent={<TextCounter counter={resendCodeSecond} callback={callbackPhone} />}
                type='number' />
            <TextLabel label={format(t('cognitoSignUpVerification.enterCodeRemark'), displayPhoneNumber)} labelStyle={globalStyles.Small_Note} />
            </View>

            <View style={{ height: 30 }} />
            <View style={commonStyles.contentContainer}>
                <ActionButton
                    title={t('addDevices.add')}
                    onPress={() => onPressAdd() }
                    isEnable={!isEmpty(phoneCode.value) && phoneCode.value.length === 6}
                />
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 100,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },
    body: {
        flex: 1
    }
});

export default AddDevices;