import React, { useState, useLayoutEffect, useEffect } from 'react';
import { StyleSheet, useColorScheme, View, Pressable, Text } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Header from '../../components/header/header.component';
import TextLabel from '../../components/forms/labels/textLabel.component';

import { useTranslation } from 'react-i18next';
import { LAYOUT_PADDING_HORIZONTAL, makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { logout, removeUserDevice, userDeviceList } from '../../utils/api/userApi';
import { Auth } from 'aws-amplify';
import { formatDate, formatDateYYYY, isEmpty } from '../../utils/stringUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '../../redux/root-reducer';
import { AccountUtils } from '../../utils/accountUtils';
import { setChartURL, setTradingURL, sleep } from '../../utils/api/apiSetter';
import { getUniqueId } from 'react-native-device-info';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { SafeAreaView } from 'react-native-safe-area-context';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';
import Remove from '../../../assets/svg/Remove';
import VerifyCodeTick from '../../../assets/svg/VerifyCodeTick';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import PressableComponent from '../utils/PressableComponent';
import { dialogDisplaySecond } from '../../config/constants';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';

const Tab = createMaterialTopTabNavigator();

interface DeviceInterface {
    deviceId: string,
    deviceName: string,
    createTime: number,
    lastLoginTime: number
}

const DeviceList: React.FC<Props<'DeviceList'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const { isFromAddDevice } = route.params;

    //const [deviceList, SetDeviceList] = useState<(DeviceInterface|undefined)[]>([]);
    const [updateList, SetUpdateList] = useState<boolean>(false);
    const [isExceeded, SetIsExceeded] = useState<boolean>(false);
    const [maxDevice, SetMaxDevice] = useState<number>(0);
    const [thisDeviceId, SetThisDeviceId] = useState<string>('');
    
    const dispatch = useDispatch();
    const { SetIsShowAddDevice, SetIsShowLoading, SetIsShowMessageDialog, SetSetting } = bindActionCreators(StateActionCreators, dispatch);
    const { SetApprovedDevicesList } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)

    const setting = useSelector((state: State) => state.state.setting);
    const deviceList = useSelector((state: State) => state.account.approvedDeviceList);

    const cognitoState = useSelector((state: State) => state.cognito);
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);

    const { doLogin, doConnectWebsocket, signOut, doCloseWebsocket } = AccountUtils();
    
    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header 
            leftComponents={<HeaderButton 
                iconComponent={<TopBarBack color={colors.MainFont} />} 
                title={t('device.title')} 
                titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} 
                onPress={onPressBackBtn} />}
            />
        });
    }, [navigation]);

    useEffect(()=>{
        const fetchData = async () => {
            var _deviceId = await getUniqueId(); 
            SetThisDeviceId(_deviceId)
        }
        
        fetchData().catch(console.error);
    }, [])

    useEffect(()=>{
        const fetchData = async () => {
            const date = new Date();
            const offset = date.getTimezoneOffset();
            console.debug(offset);

            var data = await userDeviceList()
            if (data){
                var tempDeviceList: (DeviceInterface|undefined)[] = [];
                var list = data.userDeviceList;
                list.forEach(element => {
                    var device : DeviceInterface = {
                        deviceId: element.deviceId,
                        deviceName: element.deviceName,
                        createTime: element.createTime,
                        lastLoginTime: element.lastLoginTime,
                    }
                    tempDeviceList.push(device)
                });
                for (var i = list.length; i < Number(data.maxDevice) ; i++ ){
                    tempDeviceList.push(undefined)
                }
                // SetDeviceList(tempDeviceList)
                SetApprovedDevicesList(tempDeviceList)
                SetMaxDevice(Number(data.maxDevice))
                SetIsExceeded(Number(data.maxDevice) <= list.length)
            }
        }
        
        fetchData().catch(console.error);
    }, [updateList])

    const onPressBackBtn = () => { 
        if (isFromAddDevice) {
            navigation.goBack() 
        } else {
            navigation.navigate('MeSetting')
        }
    }

    const onPressRemoveDeviceDialog = async(deviceId: string) => {
        SetIsShowMessageDialog({ isShowDialog: true, title: t('device.removeDeviceTitle'), message: '', callback: async()=>{onPressRemoveDevice(deviceId)}, isShowCancel: true, btnLabel: t('device.removeDeviceBtn') })
    }

    const onPressRemoveDevice = async(deviceId: string) => {
        console.debug('onPressRemoveDevice', deviceId)
        const session = await Auth.currentSession();
        var token = session.getAccessToken().getJwtToken();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        var cognitoUserName = payload['cognito:username'];
        if (isFromAddDevice) { //removeUserDevice & replace
            var data = await removeUserDevice(cognitoUserName, token, deviceId, deviceId === thisDeviceId ? false : true);
            SetIsShowAddDevice(true)
            await sleep(dialogDisplaySecond * 1000)
            SetIsShowLoading(true)
            if (data) {
                if (data.status === '0') {
                    // SetUpdateList(!updateList)
                    var region: string = setting?.UserRegion;
                    var pushyDeviceID = setting?.PushyDeviceID ?? '';
                    // var data2 = await loginLevel(pushyDeviceID, region, false)
                    var data2 = await doLogin(pushyDeviceID, region, true, undefined, isEmpty(cognitoState.username) ? false : (cognitoState.username !== cognitoTempState.username))
                    console.debug('data2', data2)
                    if (data2) {
                        if (data2.success) {
                            var page = data2.redirect
                            switch (page) {
                                case 'AddDevices':
                                    navigation.navigate('AddDevices')
                                    break;
                                case 'EnableBiometrics':
                                case 'TabDirectory':
                                    await doCloseWebsocket(true);
                                    const status = await doConnectWebsocket(data2.paths, true, data2.accessToken, data2.loginLevel, false);
                                    if (status === 101)
                                    {
                                        SetAccountDisplayName(data2.payload.currentTrader);
                                        setTradingURL(data2.payload.tApiPath);
                                        setChartURL(data2.payload.chartPath, data2.payload.chartWSPath);
                                    }
                                    navigation.navigate(page)
                                    break;
                            }
                        }
                    }
                }
            }
            SetIsShowLoading(false)
        } else {
            SetIsShowLoading(true)
            var data = await removeUserDevice(cognitoUserName, token, deviceId);
            if (data) {
                if (data.status === '0') {
                    SetUpdateList(!updateList)
                }
            }

            if (deviceId === thisDeviceId) {
                SetSetting({IsEnableBiometrics: undefined})
                Auth.currentSession().then(async(session) =>  {
                    var payload: { [key: string]: any; } = session.getIdToken().payload;
                    var cognitoUserName = payload['cognito:username'];
                    await logout(cognitoUserName).then(async ()=>{
                    })
                }).finally(async () =>{
                    await signOut()
                    navigation.navigate(TabDirectoryTab.ME)
                })
                
            }
            SetIsShowLoading(false)
        }
    }

    return (
        <SafeAreaView 
            edges={['bottom']}
            style={[globalStyles.container, styles.container, { backgroundColor: colors.MainBg }]}>
            <TextLabel label={ isExceeded ? t('device.exceededAlert') : t('device.addUpTo').replace('%1', maxDevice.toString())}/>
            {
                deviceList.map((item, index) => 
                (
                  <View style={[styles.list, {backgroundColor: colors.WrapperBg}]} key={index}>
                    { 
                        item ? 
                            <View>
                                <View style={styles.listIconLeft}>
                                    <VerifyCodeTick color={colors.White} backgroundColor={item.deviceId === thisDeviceId ? colors.Green : colors.Grey2} />
                                </View>
                                <View style={{paddingStart: 35}}>
                                    <View style={{padding: 5}}/>
                                    <Text style={[globalStyles.Body_Text_B, { color: colors.MainFont }]}>{item.deviceName}</Text>
                                    {/* <View style={{padding: 1}}/> */}
                                    <Text style={[globalStyles.Body_Text, { color: colors.MainFont }]}>{t('device.registerAt') + formatDateYYYY(new Date(item.createTime))}</Text>
                                    {/* <View style={{padding: 1}}/> */}
                                    <Text style={[globalStyles.Body_Text, { color: colors.MainFont }]}>{t('device.lastActive') + formatDate(new Date(item.lastLoginTime))}</Text>
                                </View>
                                <View style={styles.listIcon}><PressableComponent onPress={()=> {onPressRemoveDeviceDialog(item.deviceId)}}><Remove color={colors.Brand3} /></PressableComponent></View>
                                <View style={{padding: 5}}/>
                            </View> 
                        : 
                            null
                    }
                  </View>
                ))
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL
    },
    body: {
        flex: 1
    },
    list: {
        marginBottom: 20,
        borderRadius: 5,
        paddingHorizontal: 10,
        height: 90,
    },
    listIconLeft:{
        position: 'absolute', 
        alignSelf:'flex-start', 
        height: '100%', 
        justifyContent: 'center',
        paddingEnd: 10
    },
    listIcon:{
        position: 'absolute', 
        alignSelf:'flex-end', 
        height: '100%', 
        justifyContent: 'center',
        paddingEnd: 10
    }
});

export default DeviceList;