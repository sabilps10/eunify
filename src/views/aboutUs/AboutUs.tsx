import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  useColorScheme,
  KeyboardAvoidingView,
  Pressable,
  StyleSheet,
  Linking,
} from 'react-native';
import React, {useLayoutEffect} from 'react';
import {create} from 'twrnc';
import {useTranslation} from 'react-i18next';

//UI
//import TextLabel from '../../components/labels/TextLabel';
import TextLabel from '../../components/forms/labels/textLabel.component';
//import BigButton from '../../components/buttons/BigButton';
import BigButton from '../../components/buttons/bigButton.component';
//import MSmallButton from '../../components/buttons/mSmallButton';
import MSmallButton from '../../components/buttons/mSmallButton.component';
import commonStyles, {makeGlobalStyles} from '../../styles/styles';
import {useTheme} from '@react-navigation/native';

import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

import AntDesignIcons from 'react-native-vector-icons/AntDesign';

import {getIcon_companyLogo, getIcon_whatsapp} from '../../../assets/constants';

//import {reward_whatsappUrl, entityForFebVersion} from '../../config/Constants';
import {reward_whatsappUrl, entityId as entityForFebVersion} from '../../config/constants';

const AboutUs = ({route, navigation}) => {
  const tw = create(require(`../../tailwind.config.js`));

  const [t, i18n] = useTranslation();
  const {colors} = useTheme();

  const globalStyles = makeGlobalStyles(colors);
  const scheme = useColorScheme();

  // useLayoutEffect(() => {
  //   navigation.setOptions({
  //     headerLeft: () => (
  //       <TouchableOpacity
  //         onPress={() => navigation.goBack()}
  //         style={{paddingRight: 10}}>
  //         <MaterialCommunityIcons
  //           name="chevron-left"
  //           size={24}
  //           color={colors.primary}
  //         />
  //       </TouchableOpacity>
  //     ),
  //     headerTitle: () => (
  //       <Text style={[globalStyles.H4, {color: colors.secondaryBtn}]}>
  //         {getEntityTranslation('title')}
  //       </Text>
  //     ),
  //     headerRight: () => null,
  //   });
  // }, [navigation, colors]);
    useLayoutEffect(() => {
      navigation.setOptions({
          header: () => <Header leftComponents={
              <HeaderButton 
                  iconComponent={<TopBarBack color={colors.MainFont} />}
                  onPress={onPressBack} 
                  title={t('aboutus.title')} 
                  titleStyle={[globalStyles.H4, {color: colors.Brand2}]} />
          } />
      });
  }, [navigation]);

  const gotoWeb = url => {
    //Whatsapp
    Linking.openURL(url);
  };

  const onPressBack = () => { navigation.goBack() }


  const getEntityTranslation = k => {
    if (entityForFebVersion === 'EIE') {
      return t('aboutus.' + k);
    } else {
      return t('aboutus.emxpro_' + k);
    }
  };

  return (
    <KeyboardAvoidingView style={{flex: 1}}>
      <ScrollView
        contentContainerStyle={commonStyles.ScrollViewContainerStyle}
        showsVerticalScrollIndicator={false}>
        <View style={[commonStyles.contentContainer, tw`px-3 pb-8`]}>
          <View style={tw`py-3`}>
            <Text style={[globalStyles.H4, tw`text-brand2`]}>{getEntityTranslation('subtitle_app')}</Text>
          </View>
          <Text style={[globalStyles.Body_Text]}>{getEntityTranslation('text_app')}</Text>
          { entityForFebVersion === 'EIE' &&
          <View
            style={tw`content-center w-full items-center justify-center py-3`}>
            <Image
              source={require('../../../assets/images/aboutus.png')}
              style={tw`rounded-lg max-w-full`}
            />
          </View>
          }
          <View style={tw`pt-6 pb-3`}>
            <Text style={[globalStyles.H4, tw`text-brand2`]}>{getEntityTranslation('subtitle1')}</Text>
          </View>
          <View style={tw`pb-6`}>
            <Text style={[globalStyles.Body_Text]}>{getEntityTranslation('text1')}</Text>
            <Text style={[globalStyles.Body_Text]}>{getEntityTranslation('text3')}</Text>
          </View>
          {
          entityForFebVersion === 'EIE' ?
          <View
            style={[
              tw`p-4 rounded-xl flex flex-row w-full h-auto justify-around`,
              {backgroundColor: colors.contentBackground},
            ]}>
            <View style={tw`p-4 max-w-[70%] justify-center items-center`}>
            <Text style={[globalStyles.Body_Text_B]}>{t('myreward.whatsappmessage')}</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                gotoWeb(reward_whatsappUrl + t('aboutus.whatsapp_msg'));
              }}>
              <View
                style={[
                  tw`w-[70px] h-[70px] rounded-full justify-center items-center`,
                  globalStyles.Background,
                ]}>
                <Image source={getIcon_whatsapp(scheme)} />
              </View>
            </TouchableOpacity>
          </View>
          :
          <View
            style={[
              tw`p-4 rounded-xl flex flex-row w-full h-auto justify-around`,
              {backgroundColor: colors.contentBackground},
            ]}>
            <TouchableOpacity
              onPress={() => {
                gotoWeb('tel:+864008422808');
              }}>
              <View
                style={[
                  tw`w-[70px] h-[70px] rounded-full justify-center items-center`,
                  globalStyles.Background,
                ]}>
                <AntDesignIcons style={{fontSize: 28}} color="#00B1D2" name="phone" />
                <Text style={[globalStyles.Body_Text_B]}>{getEntityTranslation('phone')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                gotoWeb('mailto:cs@emxpro.com');
              }}>
              <View
                style={[
                  tw`w-[70px] h-[70px] rounded-full justify-center items-center`,
                  globalStyles.Background,
                ]}>
                <AntDesignIcons style={{fontSize: 28}} color="#00B1D2" name="mail" />
                <Text style={[globalStyles.Body_Text_B]}>{getEntityTranslation('email')}</Text>
              </View>
            </TouchableOpacity>
          </View>
          }
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
  },
});

export default AboutUs;
