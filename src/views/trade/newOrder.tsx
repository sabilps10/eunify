import { Text } from '@rneui/base';
import React, { useLayoutEffect, useState, useEffect, useMemo } from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, Pressable, ScrollView, StyleSheet, useColorScheme, View } from 'react-native';
import { Props } from '../../routes/route';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import HeaderButton from '../../components/header/headerButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import BuySellTradeButton from '../../components/buttons/buySellTradeButton.component';
import OrderSlider from '../../components/orderSlider.component';
import InputSwitch from '../../components/forms/inputs/inputSwitch.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useIsFocused, useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';

import { useSelector } from 'react-redux';
import { State } from '../../../src/redux/root-reducer';

import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { OrderActionType, PendingOrderType, PendingOrderBuySellType, ProfitStopLossOrderType, OrderDirectionType } from '../../utils/orderUtils'
import { ActionUtils } from '../utils/ActionUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import DashedLineView from '../../components/dashedLineView.component';
import { WebsocketUtils } from '../../utils/websocketUtils';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import OrderConfirmDialog from '../../components/dialogs/orderConfirmDialog';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import ContractValueComponent from './component/contractValue.component';
import MarginValueComponent from './component/marginValue.component';
import { displayContractCode, productName } from '../utils/CommonFunction';
import PendingOrderComponent from './component/pendingOrder.component';
import StopLossTakeProfitComponent from './component/stopLossTakeProfit.component';
import ContractChangeComponent from './component/contractChange.component';
import { addWatchList } from '../../utils/api/tradingApi';
import { store } from '../../redux/store';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import TopBarMoney from '../../../assets/svg/TopBarMoney';
import TopBarBack from '../../../assets/svg/TopBarBack';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { useHeaderHeight } from '@react-navigation/elements';
import { TradingResponseCode } from '../../utils/errorUtil';


const NewOrder: React.FC<Props<'NewOrder'>> = ({ route, navigation }) => {

    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()
    const headerHeight = useHeaderHeight();

    const { symbolName, isBuy, transactionId } = route.params;

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, SetContractName] = useState<string>(symbolName);
    const [contractDisplayName, SetContractDisplayName] = useState<string>(symbolName);
    const [contractDisplayCode, SetContractDisplayCode] = useState<string>(symbolName);

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    const contractSymbols = useSelector((state: State) => state.trading.symbols && state.trading.symbols[contractName]);

    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const region = useSelector((state: State) => state.state.setting?.UserRegion)
    const setting = useSelector((state: State) => state.state.setting)
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const symbols = useSelector((state: State) => state.trading.symbols);
    const enterDemoTab = useSelector((state: State) => state.account.enterDemoTab ?? TabDirectoryTab.QUOTE);

    const [isEditOrder, SetIsEditOrder] = useState<boolean>(transactionId !== undefined);
    const order = useSelector((state: State) => isEditOrder ? state.trading.orders![transactionId!] : undefined);
    const currentTradeSL = useSelector((state: State) => state.trading.currentTradeSL);
    const currentTradeTP = useSelector((state: State) => state.trading.currentTradeTP);
    
    const [isProgressOrder, setIsProgressOrder] = useState<boolean>(false);
    
    const [contractMaxVolume, SetContractMaxVolume] = useState<number>(100);
    const [contractMinVolume, SetContractMinVolume] = useState<number>(0);
    const [contractStepVolume, SetContractStepVolume] = useState<number>(1);
    const [contractSize, SetContractSize] = useState<number>(10);
    const [contractVolumeDP, SetContractVolumeDP] = useState<number>(2);

    const [currentLot, setCurrentLot] = useState<number>(1);
    const [currentLotUpdate, SetCurrentLotUpdate] = useState<boolean>(false);

    const [sellPrice, SetSellPrice] = useState<number>(0);
    const [buyPrice, SetBuyPrice] = useState<number>(0);
    const [isSelectBuy, SetIsSelectBuy] = useState<boolean>(isBuy);
    const [isPending, SetIsPending] = useState<boolean>(false);
    const [isProfit, SetIsProfit] = useState<boolean>(false);
    // const [isEnableConfirmation, SetIsEnableConfirmation] = useState<boolean>(true);
    const [orderValidity, SetOrderValidity] = useState<number>(1); //0:GTC, 1:today
    const [profitType, SetProfitType] = useState<number>(ProfitStopLossOrderType.PRICE); //0:price, 1:pips, 2:percentage
    const [pendingOrderPrice, SetPendingOrderPrice] = useState<number>(0);
    const [pendingOrderType, SetPendingOrderType] = useState<number>(PendingOrderType.LIMIT); //0:limit, 1:stop

    const [marginMode, setMarginMode] = useState<number>(0)

    // const [stopLossPrice, SetStopLossPrice] = useState<number>(-1);
    // const [stopLossPriceRef, SetStopLossPriceRef] = useState<number>(-1); //Reference price for non-price type
    const [stopLossPriceUpdate, SetStopLossPriceUpdate] = useState<boolean>(false);

    // const [limitProfitPrice, SetLimitProfitPrice] = useState<number>(-1);
    // const [limitProfitPriceRef, SetLimitProfitPriceRef] = useState<number>(-1); //Reference price for non-price type
    const [limitProfitPriceUpdate, SetLimitProfitPriceUpdate] = useState<boolean>(false);

    const [lotSizeError, setLotSizeError] = useState<{isShow: boolean, message: string}>({isShow: false, message: ''});
    const [isPendingOrderShowError, SetIsPendingOrderShowError] = useState<boolean>(false);
    const [isSLOrderShowError, SetIsSLOrderShowError] = useState<boolean>(false);
    const [isTPOrderShowError, SetIsTPOrderShowError] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowOrderConfirmDialog, SetMyList, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLastOrderId, SetCurrentTradeSL, SetCurrentTradeTP } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

    const { onPressExitDemo } = ActionUtils();
    // const [isDemoState, SetIsDemoState]= useState<boolean>(isDemoAccount);

    // const multiSwitchButton1Buy = [
    //     {
    //         title: t('trade.buyLimit'),
    //         function: () => { SetPendingOrderType(PendingOrderType.LIMIT) }
    //     },
    //     {
    //         title: t('trade.buyStop'),
    //         function: () => { SetPendingOrderType(PendingOrderType.STOP) }
    //     }
    // ]
    // const multiSwitchButton1Sell = [
    //     {
    //         title: t('trade.sellLimit'),
    //         function: () => { SetPendingOrderType(PendingOrderType.LIMIT) }
    //     },
    //     {
    //         title: t('trade.sellStop'),
    //         function: () => { SetPendingOrderType(PendingOrderType.STOP) }
    //     }
    // ]

    // const multiSwitchButton2 = [
    //     {
    //         title: t('trade.today'),
    //         function: () => { SetOrderValidity(1) }
    //     },
    //     {
    //         title: t('trade.gtc'),
    //         function: () => { SetOrderValidity(0) }
    //     }
    // ]

    // const multiSwitchButton3 = [
    //     {
    //         title: t('trade.price'),
    //         function: () => { SetProfitType(ProfitStopLossOrderType.PRICE) }
    //     },
    //     {
    //         title: t('trade.point'),
    //         function: () => { SetProfitType(ProfitStopLossOrderType.PIPS) }
    //     },
    //     {
    //         title: t('trade.percentage'),
    //         function: () => { SetProfitType(ProfitStopLossOrderType.PERCENTAGE) }
    //     }
    // ]

    const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();

    useEffect(() => { 
        SetIsSelectBuy(isBuy)
    }, [isBuy])

    useEffect(() => {
        SetIsEditOrder(transactionId !== undefined)
        SetProfitType(ProfitStopLossOrderType.PRICE)

        console.debug('isEditOrder', transactionId)
        console.debug('order', order)
        if (isEditOrder && order){
            SetIsPending(true)
            SetContractName(order.Symbol)
            SetIsSelectBuy(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.BUY_STOP)
            SetPendingOrderType(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.SELL_LIMIT ? 0 : 1)
            SetPendingOrderPrice(order.PriceOrder)
            SetOrderValidity(order.TimeExpiration === 0 ? 0 : 1)
            setCurrentLot(order.RemainVolume)

            var pofit = order.TP !== 0 || order.SL !== 0;
            SetIsProfit(pofit)
            if (pofit) {
                // SetStopLossPrice(order.SL === 0 ? -1 : order.SL)
                SetStopLossPriceUpdate(true)
                // SetLimitProfitPrice(order.TP === 0 ? -1 : order.TP)
                SetLimitProfitPriceUpdate(true)

                SetCurrentTradeSL({
                    ...currentTradeSL,
                    StopLossPrice: order.SL === 0 ? undefined : order.SL
                })

                SetCurrentTradeTP({
                    ...currentTradeTP,
                    TakeProfitPrice: order.TP === 0 ? -1 : order.TP
                })
            }
        }
    }, [transactionId, isEditOrder, order])

    //Reset SL TP price to N/A
    useEffect(()=>{
        //console.debug('useEffect 4')
        // SetStopLossPrice(-1)
        // SetLimitProfitPrice(-1)
        SetIsSLOrderShowError(false)
        SetIsTPOrderShowError(false)
        
        SetCurrentTradeSL({
            ...currentTradeSL,
            StopLossPrice: undefined
        })
        SetCurrentTradeTP({
            ...currentTradeTP,
            TakeProfitPrice: -1
        })

        if (profitType === ProfitStopLossOrderType.PRICE) {
            if (isEditOrder && order) {
                // SetStopLossPrice(order.SL === 0 ? -1 : order.SL)
                // SetLimitProfitPrice(order.TP === 0 ? -1 : order.TP)

                SetCurrentTradeSL({
                    ...currentTradeSL,
                    StopLossPrice: order.SL === 0 ? undefined : order.SL
                })
                SetCurrentTradeTP({
                    ...currentTradeTP,
                    TakeProfitPrice: order.TP === 0 ? -1 : order.TP
                })
            } else {
                SetCurrentTradeSL({
                    ...currentTradeSL,
                    StopLossPrice: undefined,
                    StopLossPriceRef: -1
                })
                SetCurrentTradeTP({
                    ...currentTradeTP,
                    TakeProfitPrice: -1,
                    TakeProfitPriceRef: -1
                })
            }
        }
        else if (profitType === ProfitStopLossOrderType.PIPS) {
            // SetStopLossHintSign('≥')
            // SetLimitProfitHintSign('≥')
            // var slHintPrice = contractSymbols!.StopsLevel
            // var tpHintPrice = contractSymbols!.StopsLevel
            // SetStopLossHintPrice(slHintPrice)
            // SetLimitProfitHintPrice(tpHintPrice)
            // SetStopLossPredictPL(0)
            // SetLimitProfitPredictPL(0)
            // SetStopLossPriceRef(-1)
            // SetLimitProfitPriceRef(-1)

            SetCurrentTradeSL({
                ...currentTradeSL,
                StopLossPrice: undefined,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                ...currentTradeTP,
                TakeProfitPrice: -1,
                TakeProfitPriceRef: -1
            })
        } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
            // SetStopLossHintSign('≥')
            // SetLimitProfitHintSign('≥')
            // var slHintPrice = percentageOrderSpread
            // var tpHintPrice = percentageOrderSpread
            // SetStopLossHintPrice(slHintPrice)
            // SetLimitProfitHintPrice(tpHintPrice)
            // SetStopLossPredictPL(0)
            // SetLimitProfitPredictPL(0)
            // SetStopLossPriceRef(-1)
            // SetLimitProfitPriceRef(-1)

            SetCurrentTradeSL({
                ...currentTradeSL,
                StopLossPrice: undefined,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                ...currentTradeTP,
                TakeProfitPrice: -1,
                TakeProfitPriceRef: -1
            })
        }
    }, [profitType])

    //Update symbol setting
    useEffect(() => {
        console.debug('useEffect 8', contractSymbols, contractName)
        if (contractSymbols) {
            setMarginMode(contractSymbols.MarginMode)
            SetContractDisplayName(contractSymbols.SymbolName)
            SetContractDisplayCode(contractSymbols.SymbolName)
            SetContractSize(contractSymbols.ContractSize)
            SetContractMaxVolume(contractSymbols.VolumeMax)
            SetContractMinVolume(contractSymbols.VolumeMin)
            SetContractStepVolume(contractSymbols.VolumeStep)
            var vDP = contractSymbols.VolumeStep.toString().split('.');
            var volumeDP = 0
            if (vDP.length > 1){
                volumeDP = vDP[1].length;
            }
            SetContractVolumeDP(volumeDP)
            SetCurrentLotUpdate(!currentLotUpdate)
        }

        return () => {

        }
    }, [contractSymbols, contractName])

    //Update contract price
    // useEffect(() => {
    //     // console.debug('useEffect 9', contractPrice, contractName)
    //     if (contractPrice) {
    //         SetSellPrice(contractPrice.Bid)
    //         SetBuyPrice(contractPrice.Ask)
    //         SetContractClosePrice(contractPrice.Close)
    //         var priceDiff = contractPrice.Bid - contractPrice.Close;
    //         var sign = priceDiff > 0 ? '+' : '';
    //         var priceDiffPercent = priceDiff / contractPrice.Close * 100;
    //         var signPercent = priceDiffPercent > 0 ? '+' : '';
    //         SetChangePercent(sign + priceDiff.toFixed(contractDP) + ' (' + signPercent + priceDiffPercent.toFixed(priceChangePercentageDP) + '%)')
    //         SetChangePercentColor(priceDiff > 0 ? colors.Up : colors.Down)
    //     }
    // }, [contractPrice, contractDP])

    //Update pending order price DP
    // useEffect(() => {
    //     SetPendingOrderPriceUpdate(!pendingOrderPriceUpdate)
    // }, [contractDP])

    //Update Lot Size error message
    useEffect(() => {
        //console.debug('useEffect 10')
        if (currentLot > contractMaxVolume || currentLot < contractMinVolume || currentLot <= 0) {
            setLotSizeError({isShow: true, message: t('trade.error.invalidLot')})
        } else {
            setLotSizeError({isShow: false, message: ''})
        }
    }, [currentLot])

    // //Update Pending order price error message
    // useEffect(() => {
    //     if (isPending) {
    //         //console.debug('useEffect 11')
    //         // //console.debug('useEffect pendingOrderPrice', pendingOrderPrice)
    //         if (checkPendingOrderPrice()){
    //             SetIsPendingOrderShowError(true)
    //         } else {
    //             SetIsPendingOrderShowError(false)
    //         }
    //     }
    // }, [pendingOrderPrice])

    //Update StopLoss predict PL
    // useEffect(() => {
    //     if (isProfit) {
    //         // console.debug('useEffect 12')
    //         if (contractPrice) {
    //             if (profitType === ProfitStopLossOrderType.PRICE) {
    //                 if (isPending){
    //                     if (isSelectBuy){ 
    //                         SetStopLossPredictPL((stopLossPrice - pendingOrderPrice ) * contractSize * currentLot)
    //                     } else { 
    //                         SetStopLossPredictPL((pendingOrderPrice - stopLossPrice ) * contractSize * currentLot)
    //                     }
    //                 } else {
    //                     var bid = contractPrice.Bid;
    //                     var ask = contractPrice.Ask;
    //                     if (isSelectBuy){ 
    //                         SetStopLossPredictPL((stopLossPrice - ask) * contractSize * currentLot)
    //                     } else { 
    //                         SetStopLossPredictPL((bid - stopLossPrice ) * contractSize * currentLot)
    //                     }
    //                 }
    //             } else if (profitType === ProfitStopLossOrderType.PIPS) {
    //                 var refPrice = 0;
    //                 var bid = contractPrice.Bid;
    //                 var ask = contractPrice.Ask;
    //                 var spread = contractSymbols!.Spread;
    //                 var point = contractSymbols!.Point;
    //                 if (isProfit && stopLossPrice > 0) {
    //                     if (isPending){
    //                         if (isSelectBuy) {
    //                             refPrice = pendingOrderPrice - (spread + stopLossPrice) * point
    //                         } else {
    //                             refPrice = pendingOrderPrice + (spread + stopLossPrice) * point
    //                         }
    //                     }
    //                     else {
    //                         if (isSelectBuy) {
    //                             refPrice = bid - stopLossPrice * point;
    //                         } else {
    //                             refPrice = ask + stopLossPrice * point;
    //                         }
    //                     }
    //                     SetStopLossPriceRef(refPrice);
    //                 }else {
    //                     SetStopLossPriceRef(-1);
    //                 }
                    
    //                 SetStopLossPredictPL((-stopLossPrice - spread) * point * contractSize * currentLot)
    //             } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //                 var refPrice = 0;
    //                 var bid = contractPrice.Bid;
    //                 var ask = contractPrice.Ask;
    //                 var pl = requiredMargin * stopLossPrice/100 * -1 ;
    //                 var point = contractSymbols!.Point;
    //                 if (isProfit && stopLossPrice > 0) {
    //                     if (isPending) {
    //                         if (isSelectBuy) {
    //                             refPrice = (pendingOrderPrice + (pl / (contractSize * currentLot)))
    //                         } else {
    //                             refPrice = (pendingOrderPrice - (pl / (contractSize * currentLot)))
    //                         }
    //                     } else {
    //                         if (isSelectBuy) {
    //                             refPrice = (bid + (pl / (contractSize * currentLot)))
    //                         } else {
    //                             refPrice = (ask - (pl / (contractSize * currentLot)))
    //                         }
    //                     }
    //                     console.debug('refPrice', refPrice)
    //                     SetStopLossPriceRef(refPrice);
    //                 }
    //                 SetStopLossPredictPL(pl)
    //             }
    //         }
    //     }
    // }, [stopLossPrice, contractPrice, pendingOrderPrice])

    // //Update TakeProfit predict PL
    // useEffect(() => {
    //     if (isProfit) {
    //         //console.debug('useEffect 13')
    //         if (contractPrice) {
    //             if (profitType === ProfitStopLossOrderType.PRICE) {
    //                 if (isPending){
    //                     if (isSelectBuy){ 
    //                         SetLimitProfitPredictPL((limitProfitPrice - pendingOrderPrice) * contractSize * currentLot)
    //                     } else { 
    //                         SetLimitProfitPredictPL((pendingOrderPrice- limitProfitPrice) * contractSize * currentLot)
    //                     }
    //                 } else {
    //                     var bid = contractPrice.Bid;
    //                     var ask = contractPrice.Ask;
    //                     if (isSelectBuy){ 
    //                         SetLimitProfitPredictPL((limitProfitPrice - ask) * contractSize * currentLot)
    //                     } else { 
    //                         SetLimitProfitPredictPL((bid - limitProfitPrice) * contractSize * currentLot)
    //                     }
    //                 }
    //             }
    //             else if (profitType === ProfitStopLossOrderType.PIPS) {
    //                 var refPrice = 0;
    //                 var bid = contractPrice.Bid;
    //                 var ask = contractPrice.Ask;
    //                 var spread = contractSymbols!.Spread;
    //                 var point = contractSymbols!.Point;
    //                 if (isProfit && limitProfitPrice > 0) {
    //                     if (isPending){
    //                         if (isSelectBuy) {
    //                             refPrice = pendingOrderPrice - (spread - limitProfitPrice) * point
    //                         } else {
    //                             refPrice = pendingOrderPrice + (spread - limitProfitPrice) * point
    //                         }
    //                     }
    //                     else {
    //                         if (isSelectBuy) {
    //                             refPrice = bid + limitProfitPrice * point;
    //                         } else {
    //                             refPrice = ask - limitProfitPrice * point;
    //                         }
    //                     }
    //                     SetLimitProfitPriceRef(refPrice);
    //                 } else {
    //                     SetLimitProfitPriceRef(-1);
    //                 }
                    
    //                 SetLimitProfitPredictPL((limitProfitPrice - spread) * point * contractSize * currentLot)
    //             } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //                 var refPrice = 0;
    //                 var bid = contractPrice.Bid;
    //                 var ask = contractPrice.Ask;
    //                 var pl = requiredMargin * limitProfitPrice/100
    //                 var point = contractSymbols!.Point;
    //                 if (isProfit && limitProfitPrice > 0) {
    //                     if (isPending) {
    //                         if (isSelectBuy) {
    //                             refPrice = (pendingOrderPrice + (pl / (contractSize * currentLot)))
    //                         } else {
    //                             refPrice = (pendingOrderPrice - (pl / (contractSize * currentLot)))
    //                         }
    //                     } else {
    //                         if (isSelectBuy) {
    //                             refPrice = (bid + (pl / (contractSize * currentLot)))
    //                         } else {
    //                             refPrice = (ask - (pl / (contractSize * currentLot)))
    //                         }
    //                     }
    //                     console.debug('refPrice', refPrice)
    //                     SetLimitProfitPriceRef(refPrice);
    //                 }
    //                 SetLimitProfitPredictPL(pl)
    //             }
    //         }
    //     }
    // }, [limitProfitPrice, contractPrice, pendingOrderPrice])

    // const checkPendingOrderPrice = () => {
    //     if (contractSymbols && contractPrice) {
    //         var stopLevel = contractSymbols.StopsLevel;
    //         var spread = contractSymbols?.Spread;
    //         var point = contractSymbols?.Point;
    //         // console.debug('spread point', spread, point)
    //         if (pendingOrderType === PendingOrderType.LIMIT) {
    //             if (isSelectBuy){ //BuyLimit
    //                 return !(pendingOrderPrice < contractPrice.Ask - (stopLevel * point))
    //             } else { //SellLimit
    //                 return !(pendingOrderPrice > contractPrice.Bid + (stopLevel * point))
    //             }
    //         } else {
    //             if (isSelectBuy){ //BuyStop
    //                 return !(pendingOrderPrice > contractPrice.Ask + (stopLevel * point))
    //             } else { //SellStop
    //                 return !(pendingOrderPrice < contractPrice.Bid - (stopLevel * point))
    //             }
    //         }
    //     } else {
    //         return false;
    //     }
    // }

    const onCancelStopLoss = () => {
        console.debug('onCancelStopLoss')

        SetCurrentTradeSL({
            ...currentTradeSL,
            StopLossPrice: undefined,
            StopLossPriceRef: -1
        })

        // SetStopLossPrice(-1); 
        SetStopLossPriceUpdate(!stopLossPriceUpdate); 
        SetIsSLOrderShowError(false) 
        // SetStopLossPriceRef(-1)
    }

    const onCancelLimitProfit = () => { 
        console.debug('onCancelLimitProfit')

        SetCurrentTradeTP({
            ...currentTradeTP,
            TakeProfitPrice: -1,
            TakeProfitPriceRef: -1
        })

        // SetLimitProfitPrice(-1); 
        SetLimitProfitPriceUpdate(!limitProfitPriceUpdate);
        SetIsTPOrderShowError(false)
        // SetLimitProfitPriceRef(-1)
    }

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    useLayoutEffect(() => {
        var rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.MainFont} />} />
        ]

        if (isDemoAccount) {
            rightComponents = [
                <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.MainFont} />} />,
                <HeaderButton onPress={() => onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton />} />
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents = [
                <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.MainFont} />} />
            ]
        }

        navigation.setOptions({
            header: props => <ExpandableHeader
                isDemo={isDemoAccount}
                rightComponents={rightComponents}
                leftComponents={<HeaderButton onPress={onPressBackBtn} iconComponent={<TopBarBack color={isDemoAccount ? colors.MainFont : colors.TitleFont} />} />}
            />
        });
    }, [navigation]);

    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            if (symbol.SymbolName.indexOf(".") > 0)
                return symbol.DisplayCategory
        });

        return [...new Set(categories)];
    }, [symbols])

    const onPressDeposit = async () => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.QUOTE, param: { type: MIOType.Deposit }})
    }

    const onPressSearch = () => {
        let searchCategories = categories.flatMap((category) => {return {id: category, title: category}})
        navigation.navigate('SearchProductView', {categories: searchCategories})
    }

    const onPressBackBtn = () => { navigation.goBack() }

    const onPressBuySellBtn = (type: number) => {
        SetIsSelectBuy(type === 1);
    }

    const orderSuccessCallback = async (contractCode: string) => {
        if (!isPending)
        {
            const state: State = store.getState();
            const accessToken = state.account.account?.AccessToken;
            if (!accessToken) return;
            const symbol = displayContractCode(contractCode)
            const success = await addWatchList(accessToken, symbol);

            console.debug("orderSuccessCallback: " + success);
            if (success)
            {
                const watchlist = state.state.myList;
                const isAddedWatchlist = watchlist.find(obj => obj === symbol)
                if (!isAddedWatchlist)
                {
                    watchlist.push(symbol)
                    SetMyList([...new Set(watchlist)])
                }
            }
            
        }
        let params = {contractCode: contractCode, defaultTab: 1, defaultPositionTab: isPending? 1: 0, isEnterByOrder: true}
        navigation.popToTop()
        navigation.push('ContractDetailView', params)
        console.debug(`[NewOrder orderSuccessCallback] navigation.push ContractDetailView ${params}`)
    }

    const [keyboardHeight, setKeyboardHeight] = useState(0);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            (event) => {
                setKeyboardHeight(event.endCoordinates.height);
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardHeight(0);
            }
        );
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    return (
        <View style={{ flex: 1, backgroundColor: colors.MainBg }}>
            <KeyboardAvoidingView
                style={{ flex: 1, backgroundColor: colors.WrapperBg }}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={headerHeight}>
                <View style={[styles.innerContainer, { marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
                    {
                        isEditOrder ?
                            <View>
                                <TextLabel label={t('trade.editOrder')} labelStyle={[globalStyles.H4]} />
                                <View style={{ padding: 5 }} />
                            </View>
                            :
                            null
                    }
                    <Text style={[globalStyles.Note, { color: colors.MainFont }]}>{productName(displayContractCode(contractDisplayName))}</Text>
                    <View style={[styles.row]}>
                        <TextLabel label={displayContractCode(contractDisplayCode)} labelStyle={[globalStyles.H3, { color: colors.Brand2 }]} />
                        <View style={{ padding: 10 }} />
                        {/* <View style={{ justifyContent: 'center', flex: 1 }}>
                                    <TextLabel label={changePercent} labelStyle={[globalStyles.Body_Text_B, { color: changePercentColor }]} />
                                </View> */}
                        <ContractChangeComponent symbol={contractName} />
                    </View>
                    <View style={{ padding: 5 }} />
                    {
                        isEditOrder ?
                            <View style={[styles.row]}>
                                <BuySellTradeButton title={t('trade.sell')} contract={contractName} isBuy={false} onPress={() => { }} isSelected={!isSelectBuy} />
                                <View style={{ padding: 10 }} />
                                <BuySellTradeButton title={t('trade.buy')} contract={contractName} isBuy={true} onPress={() => { }} isSelected={isSelectBuy} />
                            </View>
                            :
                            <View style={[styles.row]}>
                                <BuySellTradeButton title={t('trade.sell')} contract={contractName} isBuy={false} onPress={() => onPressBuySellBtn(0)} isSelected={!isSelectBuy} />
                                <View style={{ padding: 10 }} />
                                <BuySellTradeButton title={t('trade.buy')} contract={contractName} isBuy={true} onPress={() => onPressBuySellBtn(1)} isSelected={isSelectBuy} />
                            </View>
                    }
                    <View style={{ padding: 10 }} />
                    {
                        isEditOrder ?
                            <View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('trade.direction')}</Text>
                                    {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                    <Text style={[globalStyles.Body_Text_B, isSelectBuy ? { color: colors.Green } : { color: colors.Red }]}>{isSelectBuy ? t('trade.buy') : t('trade.sell')}</Text>
                                </View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('trade.volume')}</Text>
                                    {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                    <Text style={[globalStyles.Body_Text_B]}>{currentLot.toFixed(contractVolumeDP)} {t('confirmTrade.lots')}</Text>
                                </View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('trade.transactionId')}</Text>
                                    {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                    <Text style={[globalStyles.Body_Text_B]}>{transactionId}</Text>
                                </View>
                            </View>
                            :
                            null
                    }
                </View>

                <ScrollView  style={[styles.innerContainer2, { flex: 1, backgroundColor: colors.MainBg }]}>
                    <View style={[{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL }]}>
                        {
                            !isEditOrder ?
                                <View>
                                    <OrderSlider
                                        maxValue={contractMaxVolume}
                                        minValue={contractMinVolume}
                                        stepValue={contractStepVolume}
                                        contractSize={contractSize}
                                        currentLot={currentLot}
                                        onChangeValue={setCurrentLot}
                                        isShowError={lotSizeError.isShow}
                                        errorMessage={lotSizeError.message}
                                        dp={contractVolumeDP}
                                        currentLotUpdate={currentLotUpdate}
                                    />
                                    <View style={{ padding: 20 }} />
                                </View>
                                :
                                null
                        }

                        <View style={{ padding: 10 }} />

                        <ContractValueComponent symbol={contractName} currentLot={currentLot} isSelectBuy={isSelectBuy} isPending={isPending} pendingOrderPrice={pendingOrderPrice} />
                        <MarginValueComponent
                            symbol={contractName}
                            currentLot={currentLot}
                            isSelectBuy={isSelectBuy}
                            isPending={isPending}
                            price={pendingOrderPrice}
                            isRequiredMargin={false}
                            marginMode={marginMode} />

                        <View style={[styles.line, { borderColor: colors.Grey3 }]} />
                        <View style={{ padding: 5 }} />
                        <InputSwitch isEnableSwitch={!isEditOrder} label={t('trade.pendingOrder')} isRequired={false} value={isPending} onSwitch={() => { !isEditOrder ? SetIsPending(!isPending) : null }} textStyle={globalStyles.Big_Text_B} />
                        {
                            isPending ?
                                <PendingOrderComponent
                                    contract={symbolName}
                                    isSelectBuy={isSelectBuy}
                                    isEditOrder={isEditOrder}
                                    pendingOrderPrice={pendingOrderPrice}
                                    setPendingOrderPrice={SetPendingOrderPrice}
                                    pendingOrderType={pendingOrderType}
                                    setPendingOrderType={SetPendingOrderType}
                                    orderValidity={orderValidity}
                                    setOrderValidity={SetOrderValidity}
                                    isPendingOrderShowError={isPendingOrderShowError}
                                    setIsPendingOrderShowError={SetIsPendingOrderShowError}
                                    transactionId={transactionId}
                                />
                                :
                                null
                        }
                        <View style={[styles.line, { borderColor: colors.Grey3 }]} />
                        <View style={{ padding: 5 }} />
                        <InputSwitch label={t('trade.profitStopLoss')} isRequired={false} value={isProfit} onSwitch={() => { SetIsProfit(!isProfit) }} textStyle={globalStyles.Big_Text_B} />
                        {
                            isProfit ?
                                <View>
                                    <StopLossTakeProfitComponent
                                        contract={symbolName}
                                        isPending={isPending}
                                        pendingOrderType={pendingOrderType}
                                        pendingOrderPrice={pendingOrderPrice}
                                        currentLot={currentLot}
                                        isSelectBuy={isSelectBuy}
                                        isEditPosition={false}
                                        isEditOrder={isEditOrder}
                                        profitType={profitType}
                                        setProfitType={SetProfitType}

                                        // stopLossPrice = {stopLossPrice}
                                        // setStopLossPrice = {SetStopLossPrice}
                                        onCancelStopLoss={onCancelStopLoss}
                                        // stopLossPriceRef = {stopLossPriceRef}
                                        // setStopLossPriceRef = {SetStopLossPriceRef}
                                        isSLOrderShowError={isSLOrderShowError}
                                        setIsSLOrderShowError={SetIsSLOrderShowError}

                                        // limitProfitPrice = {limitProfitPrice}
                                        // setLimitProfitPrice = {SetLimitProfitPrice}
                                        onCancelLimitProfit={onCancelLimitProfit}
                                        // limitProfitPriceRef = {limitProfitPriceRef}
                                        // setLimitProfitPriceRef = {SetLimitProfitPriceRef}
                                        isTPOrderShowError={isTPOrderShowError}
                                        setIsTPOrderShowError={SetIsTPOrderShowError}
                                        transactionId={transactionId}
                                    />
                                </View>
                                :
                                null
                        }
                        <View style={[styles.line, { borderColor: colors.Grey3 }]} />
                    </View>
                    <View style={{ marginBottom: keyboardHeight / 2 }}></View>
                </ScrollView>
            </KeyboardAvoidingView>
            
            <>
                <View style={[{ backgroundColor: colors.MainBg }]}>
                    <View style={[{ marginHorizontal: LAYOUT_PADDING_HORIZONTAL, alignItems: 'center', marginTop: 5 }]}>
                        {isEnableTradeConfirmationDialog ?
                            null :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[globalStyles.Note, {textAlign: 'center'}]}>{t('trade.confirmationWarning')} <Text onPress={() => { navigation.navigate('MeSetting') }} style={[globalStyles.Note, { color: colors.Brand3, alignContent: 'center', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>{t('trade.confirmationChange')}</Text></Text>
                                {/* <TextLabel label={t('trade.confirmationWarning')} labelStyle={[globalStyles.Note]} />
                                <View style={{ padding: 2 }} />
                                <PressableComponent onPress={() => { navigation.navigate('MeSetting') }}><TextLabel label={t('trade.confirmationChange')} labelStyle={[globalStyles.Note, { color: colors.Brand3 }]} /></PressableComponent> */}
                            </View>
                        }
                        <ActionButton
                            title={isEditOrder ? t('trade.editOrder') : (isSelectBuy ? t('trade.buy') : t('trade.sell'))}
                            isEnable={(!isProgressOrder || isEnableTradeConfirmationDialog) && !lotSizeError.isShow && !(isPending && isPendingOrderShowError) && !(isProfit && (isSLOrderShowError || isTPOrderShowError))}
                            onPress={async () => {
                                if (contractSymbols.isTrading === 0 || contractSymbols.TradeMode === 0) {
                                    SetIsShowMessageDialog({ isShowDialog: true, title: t('common.alert'), message: t('watchlist.nonTradableContract') })
                                    return
                                }
                                var result = undefined;
                                var price = isPending || isEditOrder ? pendingOrderPrice : (isSelectBuy ? buyPrice : sellPrice);
                                if (!isEnableTradeConfirmationDialog) {
                                    setIsProgressOrder(true)
                                    SetIsShowLoading(true);
                                    if (isEditOrder) {
                                        result = await startEditOrder(transactionId!, price, isProfit && currentTradeTP.TakeProfitPrice > 0 ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef) : 0, isProfit && currentTradeSL.StopLossPrice ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef) : 0)
                                    } else {
                                        if (isPending) {
                                            var orderType = 2;
                                            if (pendingOrderType === PendingOrderType.LIMIT) {
                                                if (isSelectBuy) { //BuyLimit
                                                    orderType = PendingOrderBuySellType.BUY_LIMIT
                                                } else { //SellLimit
                                                    orderType = PendingOrderBuySellType.SELL_LIMIT
                                                }
                                            } else {
                                                if (isSelectBuy) { //BuyStop
                                                    orderType = PendingOrderBuySellType.BUY_STOP
                                                } else { //SellStop
                                                    orderType = PendingOrderBuySellType.SELL_STOP
                                                }
                                            }
                                            result = await startNewPendingOrder(contractName, orderType, currentLot, price, isProfit && currentTradeTP.TakeProfitPrice > 0 ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef) : 0, isProfit && currentTradeSL.StopLossPrice ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef) : 0, orderValidity!);
                                        }
                                        else {
                                            result = await startNewOrder(contractName, isSelectBuy ? OrderDirectionType.BUY : OrderDirectionType.SELL, currentLot, price, isProfit && currentTradeTP.TakeProfitPrice > 0 ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef) : 0, isProfit && currentTradeSL.StopLossPrice ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef) : 0);
                                        }
                                    }
                                    if (result.RtnCode === TradingResponseCode.RET_OK || (result.RtnCode === TradingResponseCode.RET_CUST_OK)) {
                                        if (result.ID && (isEditOrder ? OrderActionType.EDIT_ORDER : OrderActionType.NEW_ORDER)) {
                                            SetLastOrderId(result.ID.toString());
                                        }
                                        await orderSuccessCallback(contractName);
                                    } else {
                                        const componentView = (
                                            <View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(contractName)}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{productName(displayContractCode(contractName))}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                                    <Text style={[globalStyles.Body_Text_B, isBuy ? { color: colors.Green } : { color: colors.Red }]}>{isBuy ? t('trade.buy') : t('trade.sell')}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                                                    <DashedLineView style={{ paddingHorizontal: 10 }} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{currentLot.toFixed(2)} {t('confirmTrade.lots')}</Text>
                                                </View>
                                            </View>
                                        )

                                        if (result.RtnCode === -1) {
                                            SetIsShowMessageDialog({ isShowDialog: true, title: t('trade.error.orderFailure'), message: '' })
                                        } else if (result.RtnCode === 132) {
                                            SetIsShowMessageDialog({ isShowDialog: true, title: t('trade.error.132'), message: '' })
                                        } 
                                        else if (result.RtnCode === 999) {
                                            SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
                                        }
                                        else {
                                            SetIsShowMessageDialog({ isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode) })
                                        }
                                    }
                                    SetIsShowLoading(false);
                                    setIsProgressOrder(false)
                                } else {
                                    // SetIsShowOrderDialog(true)
                                    SetIsShowOrderConfirmDialog({
                                        isShowDialog: true,
                                        type: isEditOrder ? OrderActionType.EDIT_ORDER : OrderActionType.NEW_ORDER,
                                        contractCode: contractName,
                                        isSelectBuy: isSelectBuy,
                                        volume: currentLot,
                                        price: isPending || isEditOrder ? pendingOrderPrice : 0,
                                        isPending: isPending,
                                        pendingOrderType: pendingOrderType,
                                        pendingOrderValidity: orderValidity,
                                        isProfit: isProfit,
                                        stopLossPrice: profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef,
                                        limitProfitPrice: profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef,
                                        transactionId: isEditOrder ? transactionId : undefined,
                                        callback: orderSuccessCallback,
                                        profitType: { type: profitType, slValue: currentTradeSL.StopLossPrice, tpValue: currentTradeTP.TakeProfitPrice }
                                    })
                                }
                            }}
                        />
                    </View>
                </View>
                <View style={{ marginBottom: insets.bottom }}></View>
            </>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        paddingTop: 15
    },
    innerContainer2: {
        flex: 1,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    row: {
        flexDirection: 'row',
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    line: {
        borderWidth: 0.5,
        borderRadius: 1,
        marginTop: 10
    }
});

export default NewOrder;