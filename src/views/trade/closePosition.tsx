import { Text } from '@rneui/base';
import React, { useLayoutEffect, useState, useEffect, useMemo, useCallback } from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, Pressable, StyleSheet, useColorScheme, View } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import HeaderButton from '../../components/header/headerButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import BuySellTradeButton from '../../components/buttons/buySellTradeButton.component';
import OrderSlider from '../../components/orderSlider.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useFocusEffect, useIsFocused, useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';

import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AccountActionCreators } from '../../redux/account/actionCreator';

import {toCurrencyDisplay} from '../../utils/stringUtils'

import { OrderActionType, OrderDirectionType } from '../../utils/orderUtils'
import { StateActionCreators } from '../../redux/state/actionCreator';
import { priceChangePercentageDP } from '../../config/constants';
import DashedLineView from '../../components/dashedLineView.component';
import { WebsocketUtils } from '../../utils/websocketUtils';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import OrderConfirmDialog from '../../components/dialogs/orderConfirmDialog';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { displayContractCode, productName } from '../utils/CommonFunction';
import ContractValueComponent from './component/contractValue.component';
import ContractChangeComponent from './component/contractChange.component';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import TopBarBack from '../../../assets/svg/TopBarBack';
import TopBarMoney from '../../../assets/svg/TopBarMoney';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { store } from '../../redux/store';
import { ActionUtils } from '../utils/ActionUtils';
import PLValueComponent from './component/plValue.component';
import { useHeaderHeight } from '@react-navigation/elements';
import { TradingResponseCode } from '../../utils/errorUtil';

const ClosePosition: React.FC<Props<'ClosePosition'>> = ({ route, navigation }) => {
    // const {symbolName, isBuy} = route.params;
    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()
    const isFocused = useIsFocused();
    console.debug("ClosePosition: " + isFocused);
    const headerHeight = useHeaderHeight();

    const { ticketId } = route.params;

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [symbolName, SetSymbolName] = useState<string>('');

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    const contractSymbols = useSelector((state: State) => state.trading.symbols && state.trading.symbols[symbolName]);
    // const contractPrice = useSelector((state: State) => {
    //     if (state.trading.prices && isFocused)
    //         return state.trading.prices[symbolName]
    // });
    const contractPrice = useSelector((state: State) => undefined);
    const position = useSelector((state: State) => state.trading.positions && state.trading.positions[ticketId]);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const region = useSelector((state: State) => state.state.setting?.UserRegion)
    const setting = useSelector((state: State) => state.state.setting)
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const symbols = useSelector((state: State) => state.trading.symbols);
    const enterDemoTab = useSelector((state: State) => state.account.enterDemoTab ?? TabDirectoryTab.QUOTE);

    const [isProgressOrder, setIsProgressOrder] = useState<boolean>(false);

    const [contractName, SetContractName] = useState<string>(' ');
    const [contractDisplayName, SetContractDisplayName] = useState<string>(symbolName);
    const [contractDP, SetContractDP] = useState<number>(2);
    const [contractMaxVolume, SetContractMaxVolume] = useState<number>(100); 
    const [contractMinVolume, SetContractMinVolume] = useState<number>(0);
    const [contractStepVolume, SetContractStepVolume] = useState<number>(1);  
    const [contractSize, SetContractSize] = useState<number>(10); 
    const [contractVolumeDP, SetContractVolumeDP] = useState<number>(2);

    const [currentLot, setCurrentLot] = useState<number>(1);
    const [currentLotUpdate, SetCurrentLotUpdate] = useState<boolean>(false);

    const [changePercent, SetChangePercent] = useState<string>('');
    const [changePercentColor, SetChangePercentColor] = useState<string>(colors.Up);
    const [sellPrice, SetSellPrice] = useState<number>(0);
    const [buyPrice, SetBuyPrice] = useState<number>(0);
    const [isSelectBuy, SetIsSelectBuy] = useState<boolean>(false);
    // const [amountCurrency, SetAmountCurrency] = useState<string>('USD');
    // const [amount, SetAmount] = useState<number>(198650);

    const [lotSizeError, setLotSizeError] = useState<{isShow: boolean, message: string}>({isShow: false, message: ''});

    const [isShowOrderDialog, SetIsShowOrderDialog] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetIsShowLoading, SetIsShowOrderConfirmDialog, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLastOrderId } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

    const { onPressExitDemo } = ActionUtils();

    // const [isDemoState, SetIsDemoState]= useState<boolean>(isDemoAccount);
    
    const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();

    // useEffect(() => {
    //     console.debug('positions', position)
    //     if (position) {
    //         SetSymbolName(position.Symbol)
    //         SetIsSelectBuy(position.Type === OrderDirectionType.BUY)
    //         setCurrentLot(position.Volume)
    //         SetCurrentLotUpdate(!currentLotUpdate)
    //     }
    // }, [])
    useFocusEffect(
        useCallback(() => {
            if (position) {
                SetSymbolName(position.Symbol)
                SetIsSelectBuy(position.Type === OrderDirectionType.BUY)
                setCurrentLot(position.Volume)
                SetCurrentLotUpdate(!currentLotUpdate)
            }
        }, [position])
    )

    useEffect(() => {
        // console.debug('constractSymbols', contractSymbols)
        if (contractSymbols) {
            // console.debug('constractSymbols', contractSymbols.SymbolName + '/' + contractSymbols.Currency)
            SetContractDisplayName(contractSymbols.SymbolName)
            SetContractName(contractSymbols.SymbolName)
            SetContractDP(contractSymbols.Digits)
            SetContractSize(contractSymbols.ContractSize)
            SetContractMinVolume(contractSymbols.VolumeMin)
            SetContractStepVolume(contractSymbols.VolumeStep)
            var vDP = contractSymbols.VolumeStep.toString().split('.');
            var volumeDP = 0
            if (vDP.length > 1){
                volumeDP = vDP[1].length;
            }
            SetContractVolumeDP(volumeDP)
            SetContractMaxVolume(position.Volume)
            SetCurrentLotUpdate(!currentLotUpdate)
        }

        return () => {

        }
    }, [contractSymbols, symbolName])

    useEffect(() => {
        // console.debug('contractPrice', contractPrice)
        if (contractPrice) {
            SetSellPrice(contractPrice.Bid)
            SetBuyPrice(contractPrice.Ask)
            var priceDiff = contractPrice.Bid - contractPrice.Close;
            var sign = priceDiff > 0 ? '+' : '';
            var priceDiffPercent = priceDiff/contractPrice.Close * 100;
            var signPercent = priceDiffPercent > 0 ? '+' : '';
            SetChangePercent(sign + priceDiff.toFixed(contractDP) + ' (' + signPercent + priceDiffPercent.toFixed(priceChangePercentageDP) + '%)') 
            SetChangePercentColor(priceDiff > 0 ? colors.Up : colors.Down)
        }
    }, [contractPrice])

    useEffect(() => {
        if (currentLot > contractMaxVolume || currentLot < contractMinVolume || currentLot <= 0) {
            setLotSizeError({isShow: true, message: t('trade.error.invalidLot')})
        } else {
            setLotSizeError({isShow: false, message: ''})
        }
    }, [currentLot, contractMaxVolume])

    // const updateAmount = () => {
    //     if (contractSymbols && contractPrice) {
    //         var amountValue = (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) * contractSymbols.ContractSize * currentLot;
    //         SetAmount(amountValue);
    //     }
    // }

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    useLayoutEffect(() => {
        var rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        if (isDemoAccount) {
            rightComponents = [
                <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.MainFont} />} />,
                <HeaderButton onPress={() => onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton/>}/>
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents = [
                <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.White} />} />
            ]
        }

        navigation.setOptions({
            header: props => <ExpandableHeader
                isDemo={isDemoAccount}
                rightComponents={rightComponents}
                leftComponents={<HeaderButton onPress={onPressBackBtn} iconComponent={<TopBarBack color={isDemoAccount ? colors.MainFont : colors.White} />} />}
            />
        });
    }, [navigation]);

    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            if (symbol.SymbolName.indexOf(".") > 0)
                return symbol.DisplayCategory
        });

        return [...new Set(categories)];
    }, [symbols])

    const onPressDeposit = async () => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.QUOTE, param: { type: MIOType.Deposit }})
    }

    const onPressSearch = () => {
        let searchCategories = categories.flatMap((category) => {return {id: category, title: category}})
        navigation.navigate('SearchProductView', {categories: searchCategories})
    }

    const onPressBackBtn = () => { navigation.goBack() }

    const orderSuccessCallback = (contractCode: string) => {
        let params = {contractCode: contractCode, defaultTab: 1, isEnterByOrder: true}
        navigation.popToTop()
        navigation.push('ContractDetailView', params)
    }

    const [keyboardHeight, setKeyboardHeight] = useState(0);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            (event) => {
                setKeyboardHeight(event.endCoordinates.height);
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardHeight(0);
            }
        );
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    return (
        <View style={{flex: 1, backgroundColor: colors.MainBg}}>
            <KeyboardAvoidingView
                style={{flex: 1, backgroundColor: colors.WrapperBg}}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={headerHeight}>
                {/* <OrderConfirmDialog
                    isShow={isShowOrderDialog}
                    setShow={SetIsShowOrderDialog}
                    type={OrderActionType.CLOSE_POSITION}
                    contractCode={symbolName}
                    isSelectBuy={isSelectBuy} 
                    volume={currentLot} 
                    price={0}
                    isPending={false}
                    isProfit={false}
                    stopLossPrice={0}
                    limitProfitPrice={0}
                    transactionId={ticketId}
                    callback={orderSuccessCallback}
                /> */}
                <ScrollView style={{backgroundColor: colors.MainBg}}>
                    <View style={{backgroundColor: colors.WrapperBg}}>
                        <View style={[styles.innerContainer, {marginHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
                            <TextLabel label={t('trade.closePosition')} labelStyle={[globalStyles.H4]}/>
                            <View style={{ padding: 5 }} />
                            <Text style={[globalStyles.Note, {color: colors.MainFont}]}>{productName(displayContractCode(contractDisplayName))}</Text>
                            <View style={[styles.row]}>
                                {/* <Text style={[globalStyles.Form_Title, globalStyles.H3, {color: colors.Brand2}]}>{contractName}</Text> */}
                                <TextLabel label={displayContractCode(contractName)} labelStyle={[globalStyles.H3, {color: colors.Brand2}]}/>
                                <View style={{ padding: 10 }} />
                                {/* <View style={{ justifyContent: 'center', flex: 1}}>
                                    <TextLabel label={changePercent} labelStyle={[globalStyles.Body_Text_B, {color: changePercentColor}]}/>
                                </View> */}
                                <ContractChangeComponent symbol={contractName}/>
                            </View>
                            <View style={{ padding: 5 }} />
                            <View style={[styles.row]}>
                                <BuySellTradeButton title={t('trade.sell')} contract={contractName} isBuy={false} isSelected={isSelectBuy}/>
                                <View style={{ padding: 10 }} />
                                <BuySellTradeButton title={t('trade.buy')} contract={contractName} isBuy={true} isSelected={!isSelectBuy}/>
                            </View>
                            <View style={{ padding: 10 }} />
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.direction')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B, isSelectBuy ? {color: colors.Green} : {color: colors.Red}]}>{isSelectBuy ? t('trade.buy') : t('trade.sell')}</Text>
                            </View>
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.openPrice')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{position ? position.OpenPrice : '0'}</Text>
                            </View>
                            <ContractValueComponent 
                                symbol={contractName} 
                                currentLot={currentLot} 
                                // Liquidate IS opposite of the Open Position
                                // Open Position is SELL, Liquidate is BUY vice versa.
                                isSelectBuy={!isSelectBuy} 
                                isPending={false} 
                                pendingOrderPrice={0}/>
                            {position &&
                                <PLValueComponent
                                    symbol={contractName} 
                                    isSelectBuy={isSelectBuy} 
                                    openPrice={position.OpenPrice}
                                    ticket={position.Ticket}
                                    lotRatio={currentLot/position.Volume}/>
                            }
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.transactionId')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{ticketId}</Text>
                            </View>
                        </View>
                    </View>
                    
                    <View style={[styles.innerContainer2, {backgroundColor: colors.MainBg}]}>
                        <View style={[ {marginHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
                            <OrderSlider
                                maxValue={contractMaxVolume}
                                minValue={contractMinVolume}
                                stepValue={contractStepVolume}
                                contractSize={contractSize}
                                currentLot={currentLot}
                                onChangeValue={setCurrentLot}
                                isShowError={lotSizeError.isShow}
                                errorMessage={lotSizeError.message}
                                optionButtonsType={'all'}
                                dp={contractVolumeDP}
                                currentLotUpdate={currentLotUpdate}
                                />
                            <View style={{ padding: 20 }} />
                            {/* <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.contractValue')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{amountCurrency} {toCurrencyDisplay(amount)}</Text>
                            </View> */}
                        </View>
                    </View>
                    <View style={{marginBottom: keyboardHeight / 2}}></View>
                </ScrollView>

            </KeyboardAvoidingView>
            <>
                <View style={[{backgroundColor: colors.MainBg}]}>
                    <View style={[ {marginHorizontal: LAYOUT_PADDING_HORIZONTAL, alignItems: 'center', marginTop: 5}]}>
                        {
                            isEnableTradeConfirmationDialog ? 
                                null 
                            : 
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={[globalStyles.Note, {textAlign: 'center'}]}>{t('trade.confirmationWarning')} <Text onPress={() => { navigation.navigate('MeSetting') }} style={[globalStyles.Note, { color: colors.Brand3, alignContent: 'center', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>{t('trade.confirmationChange')}</Text></Text>
                                    {/* <TextLabel label={t('trade.confirmationWarning')} labelStyle={[globalStyles.Note]} />
                                    <View style={{ padding: 2 }} />
                                    <PressableComponent onPress={() => { navigation.navigate('MeSetting') }}><TextLabel label={t('trade.confirmationChange')} labelStyle={[globalStyles.Note, { color: colors.Brand3 }]} /></PressableComponent> */}
                                </View>
                        }
                        <ActionButton
                            title={t('trade.closePosition')}
                            isEnable={(!isProgressOrder || isEnableTradeConfirmationDialog) && !lotSizeError.isShow}
                            onPress={async()=>{
                                if (!isEnableTradeConfirmationDialog) {
                                    setIsProgressOrder(true)
                                    SetIsShowLoading(true);
                                    var result = await startLiqOrder(contractName, isSelectBuy ? OrderDirectionType.SELL : OrderDirectionType.BUY, currentLot, isSelectBuy ? buyPrice : sellPrice, ticketId!);
                                    if (result.RtnCode === TradingResponseCode.RET_OK || result.RtnCode === TradingResponseCode.RET_CUST_OK) {
                                        if (result.ID) {
                                            SetLastOrderId(result.ID.toString());
                                        }
                                        await orderSuccessCallback(contractName);
                                    } else {
                                        const componentView = (
                                            <View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(contractName)}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{productName(displayContractCode(contractName))}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B, isSelectBuy ? {color: colors.Red} : {color: colors.Green}]}>{isSelectBuy ? t('trade.sell') : t('trade.buy')}</Text>
                                                </View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{currentLot.toFixed(2)} {t('confirmTrade.lots')}</Text>
                                                </View>
                                            </View>
                                        )

                                        if (result.RtnCode === -1) {
                                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.closePositionFailure'), message: ''})
                                        } else if (result.RtnCode === 132) {
                                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.132'), message: ''})
                                        } 
                                        else if (result.RtnCode === 999) {
                                            SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
                                        }
                                        else {
                                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode)})
                                        }
                                    }
                                    SetIsShowLoading(false);
                                    setIsProgressOrder(false)
                                } else {
                                    SetIsShowOrderConfirmDialog({
                                        isShowDialog:true,
                                        type:OrderActionType.CLOSE_POSITION,
                                        contractCode:symbolName,
                                        isSelectBuy:isSelectBuy,
                                        volume:currentLot,
                                        price:0,
                                        isPending:false,
                                        isProfit:false,
                                        stopLossPrice:0,
                                        limitProfitPrice:0,
                                        transactionId:ticketId,
                                        callback:orderSuccessCallback
                                    })
                                }
                            }}
                        />
                    </View>
                </View>
                <View style={{marginBottom: insets.bottom}}></View>
            </>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        paddingTop: 15
    },
    innerContainer2: {
        flex: 1,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    row: {
        flexDirection: 'row',
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    line: {
        borderWidth: 0.5,
        borderRadius: 1,
        marginTop: 10
    }
});

export default ClosePosition;