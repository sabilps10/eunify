import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, TextStyle, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';

interface ComponentInterface {
    symbol: string,
    currentLot: number,
    isSelectBuy: boolean,
    isPending: boolean,
    pendingOrderPrice?: number,
    textStyle?: TextStyle
}

const ContractValueComponent = ({
    symbol = null,
    currentLot = 0,
    isSelectBuy = false,
    isPending = false,
    pendingOrderPrice,
    textStyle = {}
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const contractSymbols = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[symbol]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[symbol]! : undefined);
    const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);

    const [valueCurrency, SetValueCurrency] = useState<string>('');
    const [amount, SetAmount] = useState<number>(0);

    useEffect(() => {
        if (contractSymbols == null) return
        console.debug('contractValue, contractSymbols.CalcMode:' + contractSymbols.CalcMode)
        // is Forex
        if (contractSymbols.CalcMode == 0) {
            let currency = contractSymbols.SymbolName.substring(3, 6)
            SetValueCurrency(currency)    
            return
        }
        SetValueCurrency(contractSymbols.Currency)
    }, [contractSymbols])

    useEffect(()=>{
        updateAmount();
    }, [contractSymbols, contractPrice, currentLot, isSelectBuy, isPending, pendingOrderPrice])

    const updateAmount = () => {
        if (contractSymbols && contractPrice) {
            var price = isPending ? pendingOrderPrice : (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) 
            var amountValue = price * contractSymbols.ContractSize * currentLot;
            SetAmount(amountValue);
        }
    }

    return (
        <View style={styles.contentItem}>
            <Text style={[globalStyles.Body_Text, textStyle]}>{t('trade.contractValue')}</Text>
            {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
            <DashedLineView style={{paddingHorizontal: 10}} />
            <Text style={[globalStyles.Body_Text_B, textStyle]}>{valueCurrency} {toCurrencyDisplay(amount)}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default ContractValueComponent