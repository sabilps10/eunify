import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { PendingOrderBuySellType, PendingOrderType } from '../../../utils/orderUtils';
import TextLabel from '../../../components/forms/labels/textLabel.component';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';
import PendingOrderPriceComponent from './pendingOrderPrice.component';

interface ComponentInterface {
    contract: string,
    isSelectBuy: boolean,
    isEditOrder: boolean,
    pendingOrderPrice: number,
    setPendingOrderPrice: Function,
    pendingOrderType: number,
    setPendingOrderType: Function,
    orderValidity: number,
    setOrderValidity: Function,
    isPendingOrderShowError: boolean,
    setIsPendingOrderShowError: Function,

    transactionId?: number
}

const PendingOrderComponent = ({
    contract = '',
    isSelectBuy = false,
    isEditOrder = false,
    pendingOrderPrice = 0,
    setPendingOrderPrice,
    pendingOrderType = 0,
    setPendingOrderType,
    orderValidity = 0,
    setOrderValidity,
    isPendingOrderShowError = false,
    setIsPendingOrderShowError,
    transactionId = 0
}: ComponentInterface) => {
    console.debug("PendingOrder")
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, setContractName] = useState<string>(contract);
    const order = useSelector((state: State) => isEditOrder ? state.trading.orders![transactionId!] : undefined);

    useEffect(() => { 
        if (isEditOrder && order){
            setPendingOrderType(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.SELL_LIMIT ? 0 : 1)
        }
    }, [])

    const multiSwitchButton1Buy = [
        {
            title: t('trade.buyLimit'),
            function: () => { setPendingOrderType(PendingOrderType.LIMIT) }
        },
        {
            title: t('trade.buyStop'),
            function: () => { setPendingOrderType(PendingOrderType.STOP) }
        }
    ]

    const multiSwitchButton1Sell = [
        {
            title: t('trade.sellLimit'),
            function: () => { setPendingOrderType(PendingOrderType.LIMIT) }
        },
        {
            title: t('trade.sellStop'),
            function: () => { setPendingOrderType(PendingOrderType.STOP) }
        }
    ]

    const multiSwitchButton2 = [
        {
            title: t('trade.today'),
            function: () => { setOrderValidity(1) }
        },
        {
            title: t('trade.gtc'),
            function: () => { setOrderValidity(0) }
        }
    ]

    return (
        <View>
            <PendingOrderPriceComponent
                contract = {contract}
                isSelectBuy = {isSelectBuy}
                isEditOrder = {isEditOrder}
                pendingOrderPrice={pendingOrderPrice} 
                setPendingOrderPrice={setPendingOrderPrice}
                pendingOrderType = {pendingOrderType}
                setPendingOrderType = {setPendingOrderType}
                setOrderValidity = {setOrderValidity}
                isPendingOrderShowError = {isPendingOrderShowError}
                setIsPendingOrderShowError = {setIsPendingOrderShowError}
                transactionId = {transactionId}
            />
            <TextLabel label={t('trade.orderType')} labelStyle={[globalStyles.Form_Title]} />
            <MultiSwitchButton enable={!isEditOrder} buttons={isSelectBuy ? multiSwitchButton1Buy : multiSwitchButton1Sell} selectedIndex={pendingOrderType}/>
            <View style={{ padding: 10 }} />
            <TextLabel label={t('trade.validity')} labelStyle={[globalStyles.Form_Title]} />
            <MultiSwitchButton enable={!isEditOrder} buttons={multiSwitchButton2}  selectedIndex={orderValidity === 1 ? 0 : 1}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default PendingOrderComponent