import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, TextStyle, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { shallowEqual, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { ForexType, getConvertPrice, getEstMargin, CalculateMarginInterface, calculateMargin, CalcMode } from '../../../utils/orderUtils';
import { RotationGestureHandler } from 'react-native-gesture-handler';
import { PriceQuoteContent, SymbolContent, SymbolParam } from '../../../redux/trading/type';
import { priceChangePercentageDP } from '../../../../build_config/QA-EMXPro/src/config/constants';
import { store } from '../../../redux/store';

interface ComponentInterface {
    symbol: string,
    currentLot: number,
    isSelectBuy: boolean,
    isPending: boolean,
    price?: number,
    marginRate?: number,
    textStyle?: TextStyle,
    isRequiredMargin?: boolean,
    marginMode?: number,
}

const MarginValueComponent = ({
    symbol = null,
    currentLot = 0,
    isSelectBuy = false,
    isPending = false,
    price = 0,
    marginRate = 0,
    textStyle = {},
    isRequiredMargin = false,
    marginMode = null,
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const contractSymbol = useSelector((state: State) => state.trading.symbols[symbol])
    const currentContractPrice = useSelector((state: State) => state.trading.prices[symbol]);

    const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);
    const depositCurrency = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.DepositCurrency);


    const [marginCurrency, SetMarginCurrency] = useState<string>('');
    const [requiredMargin, SetRequiredMargin] = useState<number>(0);

    
    useEffect(() => {
        if (depositCurrency == null) return
        SetMarginCurrency(depositCurrency)
    }, [depositCurrency])

    useEffect(()=>{
        updateRequireMargin();
    }, [currentLot, isSelectBuy, isPending, price, accountLeverage, contractSymbol, currentContractPrice])


    const updateRequireMargin = () => {
        if (contractSymbol == null) return

        // is estimate margin (New Order)
        if (!isRequiredMargin) {
            // Default margin rate is 1 means no need calculate exchange rate.
            marginRate = 1

            if (currentContractPrice) {
                price = isPending ? price : (isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid)
            }

            let isFroex = contractSymbol.MarginMode === CalcMode.FOREX
            let baseCurrency = symbol.substring(0, 3)
            let quoteCurrency = symbol.substring(3, 6)
            let isBaseInMargin = baseCurrency === contractSymbol.MarginCurrency
            let isQuoteInDeposit = quoteCurrency === depositCurrency
            let isIndirectQuote = isFroex && isBaseInMargin && isQuoteInDeposit
            let needCalculateExchangeRate = !(contractSymbol.MarginCurrency === depositCurrency)

            // Indirect Quote (Base currency = Non USD, XXX/USD)
            if (isIndirectQuote) {
                marginRate = price
            }
            else {
                // When contract's margin currency NOT equal to deposit currency
                if (needCalculateExchangeRate) {
                    let averagePrice = 0
                    // console.debug('marginValue, line 89 MarginConversionCurrency2:' + contractSymbol.MarginConversionCurrency2)
                    const state: State = store.getState()
                    let exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency2]
                    if (exchangeContractPrice) {
                        let bid = exchangeContractPrice.Bid
                        let ask = exchangeContractPrice.Ask
                        averagePrice = (bid + ask) / 2
                    }
                    else {
                        // console.debug('marginValue, line 99 MarginConversionCurrency1:' + contractSymbol.MarginConversionCurrency1)
                        exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency1]

                        if (exchangeContractPrice) {
                            let bid = exchangeContractPrice.Bid
                            let ask = exchangeContractPrice.Ask
                            averagePrice = 1 / ((bid + ask) / 2)
                        }
                    }
                    // console.debug('marginValue, averagePrice:' + averagePrice)
                    marginRate = averagePrice
                }
            }

            // Need exchange but can't found coresponding contract to calculate the exchange rate in deposit currency.
            if ((isIndirectQuote || (!isIndirectQuote && needCalculateExchangeRate)) && marginRate == 0) {
                SetRequiredMargin(null)
                return
            }
        }

        let isFutures = contractSymbol.MarginMode == CalcMode.FUTURES
        let isUseMaintenanceMargin = isFutures && isRequiredMargin
        let margin = isUseMaintenanceMargin ? contractSymbol.MaintenanceMargin : contractSymbol.InitialMargin

        let calculateMarginParams: CalculateMarginInterface = {
            symbol: symbol,
            marginMode: contractSymbol.MarginMode,
            isFixedMargin: !(contractSymbol.InitialMargin === 0),
            margin: margin,
            contractSize: contractSymbol.ContractSize,
            percentage: contractSymbol.Percentage,
            tickSize: contractSymbol.TickSize,
            tickValue: contractSymbol.TickValue,
            marginCurrency: contractSymbol.MarginCurrency,
            accountLeverage: accountLeverage,
            lots: currentLot,
            price: price,
            marginRate: marginRate,
        }
        let marginValue = calculateMargin(calculateMarginParams)
        SetRequiredMargin(marginValue)
    }

    return (
        <View style={styles.contentItem}>
            <Text style={[globalStyles.Body_Text, textStyle]}>{isRequiredMargin ? t('positionDetails.initialMargin') : t('trade.estimatedMargin')}</Text>
            {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
            <DashedLineView style={{paddingHorizontal: 10}} />
            <Text style={[globalStyles.Body_Text_B, textStyle]}>{marginCurrency} {requiredMargin == null ? '-' : toCurrencyDisplay(requiredMargin)}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default React.memo(MarginValueComponent)