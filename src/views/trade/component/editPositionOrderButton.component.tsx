import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';

import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { OrderActionType, PendingOrderBuySellType, PendingOrderType, ProfitStopLossOrderType } from '../../../utils/orderUtils';
import TextLabel from '../../../components/forms/labels/textLabel.component';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';
import PendingOrderPriceComponent from './pendingOrderPrice.component';
import ActionButton from '../../../components/buttons/actionButton.component';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../../redux/state/actionCreator';
import { WebsocketUtils } from '../../../utils/websocketUtils';
import { OpenPositionsActionCreators } from '../../../redux/trading/actionCreator';
import DashedLineView from '../../../components/dashedLineView.component';
import { displayContractCode, productName } from '../../utils/CommonFunction';
import { TradingResponseCode } from '../../../utils/errorUtil';

interface ComponentInterface {
    isEnable: boolean,
    isEnableTradeConfirmationDialog: boolean,
    setIsProgressOrder: React.Dispatch<React.SetStateAction<boolean>>,
    orderSuccessCallback: Function,
    order: {
        ticketId: number,
        symbolName: string,
        isSelectBuy: boolean,
        currentLot: number,
        openPrice: number,
        profitType: ProfitStopLossOrderType
    }
}

const EditPositionButtonComponent = ({
    isEnable,
    isEnableTradeConfirmationDialog,
    setIsProgressOrder,
    orderSuccessCallback,
    order
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const currentTradeSL = useSelector((state: State) => state.trading.currentTradeSL);
    const currentTradeTP = useSelector((state: State) => state.trading.currentTradeTP);

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowOrderConfirmDialog, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLastOrderId } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const { startEditPosition } = WebsocketUtils();


    return (

        <ActionButton
            title={t('trade.editPosition')}
            isEnable={isEnable}
            onPress={async()=>{
                if (!isEnableTradeConfirmationDialog) {
                    setIsProgressOrder(true)
                    SetIsShowLoading(true);
                    var result = await startEditPosition(order.ticketId!, currentTradeTP.TakeProfitPrice > 0 ? (order.profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef) : 0, currentTradeSL.StopLossPrice ? (order.profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef) : 0)
                    
                    if (result.RtnCode === TradingResponseCode.RET_OK || result.RtnCode === TradingResponseCode.RET_CUST_OK ) {
                        SetLastOrderId(order.ticketId.toString());
                        await orderSuccessCallback(order.symbolName);
                    } else {
                        const componentView = (
                            <View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(order.symbolName)}</Text>
                                </View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{productName(displayContractCode(order.symbolName))}</Text>
                                </View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B, order.isSelectBuy ? {color: colors.Green} : {color: colors.Red}]}>{order.isSelectBuy ? t('trade.buy') : t('trade.sell')}</Text>
                                </View>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{order.currentLot.toFixed(2)} {t('confirmTrade.lots')}</Text>
                                </View>
                            </View>
                        )
                        
                        if (result.RtnCode === -1) {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.editPositionFailure'), message: ''})
                        } else if (result.RtnCode === 132) {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.132'), message: ''})
                        } 
                        else if (result.RtnCode === 999) {
                            SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
                        }
                        else {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode)})
                        }
                    }

                    SetIsShowLoading(false);
                    setIsProgressOrder(false)
                } else {
                    // SetIsShowOrderDialog(true)
                    SetIsShowOrderConfirmDialog({
                        isShowDialog:true,
                        type:OrderActionType.EDIT_POSITION,
                        contractCode:order.symbolName,
                        isSelectBuy:order.isSelectBuy,
                        volume:order.currentLot,
                        price:order.openPrice,
                        isPending:false,
                        isProfit:true,
                        stopLossPrice:0,
                        limitProfitPrice:0,
                        transactionId:order.ticketId,
                        callback:orderSuccessCallback,
                        profitType:{type: order.profitType}//{type: profitType, slValue: currentTradeSL.StopLossPrice, tpValue: currentTradeTP.TakeProfitPrice}
                    })
                }
            }}
        />  
    )
}

const styles = StyleSheet.create({
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
})

export default EditPositionButtonComponent