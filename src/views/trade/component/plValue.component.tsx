import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, TextStyle, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { priceChangePercentageDP } from '../../../config/constants';

interface ComponentInterface {
    symbol: string,
    isSelectBuy: boolean,
    openPrice?: number,
    textStyle?: TextStyle,
    ticket: number,
    lotRatio: number
}

const PLValueComponent = ({
    symbol = null,
    isSelectBuy = false,
    openPrice = 0,
    textStyle = {},
    ticket,
    lotRatio
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const contractSymbols = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[symbol]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[symbol]! : undefined);
    const profit = useSelector((state: State) => state.trading.profits ? state.trading.profits[ticket]! : undefined);

    const [pl, SetPL] = useState<number>(0);

    useEffect(() => {
        SetPL(Number((profit*lotRatio).toFixed(2)))
    }, [profit, lotRatio])

    const PL = () => 
    {
        var referencePrice = 0;
        if (contractPrice) {
            referencePrice = isSelectBuy ? contractPrice.Bid : contractPrice.Ask;
        }
        const displayProfit = pl > 0 ? '+' + toCurrencyDisplay(pl) : toCurrencyDisplay(pl);
        const change = (isSelectBuy ? (referencePrice - openPrice) : (openPrice - referencePrice)) / openPrice * 100;
        const displayChange = '(' + (change > 0 ? '+' + change.toFixed(priceChangePercentageDP) : change.toFixed(priceChangePercentageDP)) + '%)';
        return (<Text
        style={[
            globalStyles.Body_Text_B,
            { color: pl > 0 ? colors.Green : colors.Red },
        ]}>
        {displayProfit + ' ' + displayChange}
        </Text>)
    }

    return (
        <View style={styles.contentItem}>
            <Text style={[globalStyles.Body_Text, textStyle]}>{t('trade.pl')}</Text>
            {/* <View style={[styles.dottedLine, { borderColor: colors.Grey3 }]} /> */}
            <DashedLineView style={{paddingHorizontal: 10}} />
            <PL/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default PLValueComponent