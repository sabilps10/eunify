import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { ProfitStopLossOrderType } from '../../../utils/orderUtils';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';
import InputArithmomether from '../../../components/forms/inputs/inputArithmomether.component';
import ErrorMessage from '../../../components/alerts/errorMessage.component';
import { percentageOrderSpread } from '../../../config/constants';
import StopLossComponent from './stopLoss.component';
import TakeProfitComponent from './takeProfit.component';

interface ComponentInterface {
    contract: string,

    isPending: boolean,
    pendingOrderType: number,
    pendingOrderPrice: number,
    
    currentLot: number,
    isSelectBuy: boolean,
    isEditPosition: boolean,
    isEditOrder: boolean,
    profitType: number,
    setProfitType: Function,

    // stopLossPrice: number,
    // setStopLossPrice: Function,
    onCancelStopLoss: Function,
    // stopLossPriceRef: number,
    // setStopLossPriceRef: Function,
    isSLOrderShowError: boolean,
    setIsSLOrderShowError: Function

    // limitProfitPrice: number,
    // setLimitProfitPrice: Function
    onCancelLimitProfit: Function,
    // limitProfitPriceRef: number,
    // setLimitProfitPriceRef: Function,
    isTPOrderShowError: boolean,
    setIsTPOrderShowError: Function,

    transactionId?: number
}

const StopLossTakeProfitComponent = ({
    contract = '',
    isPending = false,
    pendingOrderType = 0,
    pendingOrderPrice = 0,
    currentLot = 0,
    isSelectBuy = false,
    isEditPosition = false,
    isEditOrder = false,
    profitType = 0,
    setProfitType,
    // stopLossPrice = 0,
    // setStopLossPrice,
    onCancelStopLoss,
    // setStopLossPriceRef,
    isSLOrderShowError = false,
    setIsSLOrderShowError,
    // limitProfitPrice = 0,
    // setLimitProfitPrice,
    onCancelLimitProfit,
    // setLimitProfitPriceRef,
    isTPOrderShowError = false,
    setIsTPOrderShowError,
    transactionId = 0
}: ComponentInterface) => {

    // console.debug("StopLossTakeProfitComponent")
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, setContractName] = useState<string>(contract);

    const contractSymbol = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[contractName]! : undefined);

    const [contractStepPrice, setContractStepPrice] = useState<number>(1);
    const [contractDP, setContractDP] = useState<number>(6);
    const [contractSize, setContractSize] = useState<number>(0);
    const [requiredMargin, SetRequiredMargin] = useState<number>(0);

    const multiSwitchButton3 = [
        {
            title: t('trade.price'),
            function: () => { setProfitType(ProfitStopLossOrderType.PRICE) }
        },
        {
            title: t('trade.point'),
            function: () => { setProfitType(ProfitStopLossOrderType.PIPS) }
        },
        {
            title: t('trade.percentage'),
            function: () => { setProfitType(ProfitStopLossOrderType.PERCENTAGE) }
        }
    ]

    useEffect(() => {
        if (contractSymbol) {
            setContractStepPrice(contractSymbol.Point)
            setContractDP(contractSymbol.Digits)
            setContractSize(contractSymbol.ContractSize)
        }
    }, [contractSymbol, contractName])

    return (
        <View>
            <View style={{ padding: 10 }} />
            {/* <MultiSwitchButton 
                buttons={multiSwitchButton3} 
                selectedIndex={ profitType === ProfitStopLossOrderType.PRICE ? 0 : ( profitType === ProfitStopLossOrderType.PIPS ? 1 : 2) } />
            <View style={{ padding: 10 }} /> */}
            <StopLossComponent
                contract = {contract}
                isPending = {isPending}
                pendingOrderType = {pendingOrderType}
                pendingOrderPrice = {pendingOrderPrice}
                currentLot = {currentLot}
                isSelectBuy = {isSelectBuy}
                isEditPosition = {isEditPosition}
                isEditOrder = {isEditOrder}
                profitType = {profitType}
                setProfitType = {setProfitType}
                // stopLossPrice = {stopLossPrice}
                // setStopLossPrice = {setStopLossPrice}
                onCancelStopLoss = {onCancelStopLoss}
                // setStopLossPriceRef = {setStopLossPriceRef}
                isSLOrderShowError = {isSLOrderShowError}
                setIsSLOrderShowError = {setIsSLOrderShowError}
                transactionId = {transactionId}
            />
            <TakeProfitComponent
                contract = {contract}
                isPending = {isPending}
                pendingOrderType = {pendingOrderType}
                pendingOrderPrice = {pendingOrderPrice}
                currentLot = {currentLot}
                isSelectBuy = {isSelectBuy}
                isEditPosition = {isEditPosition}
                isEditOrder = {isEditOrder}
                profitType = {profitType}
                setProfitType = {setProfitType}
                // limitProfitPrice = {limitProfitPrice}
                // setLimitProfitPrice = {setLimitProfitPrice}
                onCancelLimitProfit = {onCancelLimitProfit}
                // setLimitProfitPriceRef = {setLimitProfitPriceRef}
                isTPOrderShowError = {isTPOrderShowError}
                setIsTPOrderShowError = {setIsTPOrderShowError}
                transactionId = {transactionId}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default StopLossTakeProfitComponent