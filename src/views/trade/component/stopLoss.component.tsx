import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { CalcMode, calculateMargin, CalculateMarginInterface, getConvertPrice, getEstMargin, getPLConvertPrice, ProfitStopLossOrderType } from '../../../utils/orderUtils';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';
import InputArithmomether from '../../../components/forms/inputs/inputArithmomether.component';
import ErrorMessage from '../../../components/alerts/errorMessage.component';
import { percentageOrderSpread } from '../../../config/constants';
import { bindActionCreators } from 'redux';
import { OpenPositionsActionCreators } from '../../../redux/trading/actionCreator';
import { OpenPositionContent } from '../../../redux/trading/type';
import { store } from '../../../redux/store';

interface ComponentInterface {
    contract: string,

    isPending: boolean,
    pendingOrderType: number,
    pendingOrderPrice: number,
    
    currentLot: number,
    isSelectBuy: boolean,
    isEditPosition: boolean,
    isEditOrder: boolean,
    profitType: number,
    setProfitType: Function,

    // stopLossPrice: number,
    // setStopLossPrice: Function,
    onCancelStopLoss: Function,
    // setStopLossPriceRef: Function,
    isSLOrderShowError: boolean,
    setIsSLOrderShowError: Function,

    transactionId?: number
}

const StopLossComponent = ({
    contract = '',
    isPending = false,
    pendingOrderType = 0,
    pendingOrderPrice = 0,
    currentLot = 0,
    isSelectBuy = false,
    isEditPosition = false,
    isEditOrder = false,
    profitType = 0,
    setProfitType,
    // stopLossPrice = 0,
    // setStopLossPrice,
    onCancelStopLoss,
    // setStopLossPriceRef,
    isSLOrderShowError = false,
    setIsSLOrderShowError,
    transactionId = 0
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, setContractName] = useState<string>(contract);

    const contractSymbol = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[contractName]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[contractName]! : undefined);
    const marginProfitConvertPrice = useSelector((state: State) => 
    {
        // console.debug('_contractPricesMargin1', contractSymbol.ProfitConversionCurrency1, contractSymbol.ProfitConversionCurrency2)

        var convertPrice = 1;

        if (contractSymbol) {

            const _contractPricesSelf = state.trading.prices[contractSymbol.SymbolName];
            const _contractPricesMargin1 = state.trading.prices[contractSymbol.ProfitConversionCurrency1];
            const _contractPricesMargin2 = state.trading.prices[contractSymbol.ProfitConversionCurrency2];

            const first = contractSymbol.SymbolName.substring(0, 3);
            const second = contractSymbol.SymbolName.substring(3, 6);
            const group = contractSymbol.SymbolName.substring(6, contractSymbol.SymbolName.length);
            
            if (first === 'USD'){
                //Direct Quote
                convertPrice = (isSelectBuy ? _contractPricesSelf.Bid : _contractPricesSelf.Ask)
            } else if (second === 'USD'){
                //Indriect Quote
            } else {
                const quote1 = _contractPricesMargin1;
                const quote2 = _contractPricesMargin2;
                if (quote1) {
                    //Cross Indriect Quote
                    // console.debug('updateRequireMargin Cross Indriect Quote')
                    // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
                    convertPrice = (isSelectBuy ? quote1.Bid : quote1.Ask)
                } else if (quote2) {
                    //Cross Driect Quote
                    // console.debug('updateRequireMargin Cross Driect Quote')
                    convertPrice = 1/(isSelectBuy ? quote2.Bid : quote2.Ask)
                }
            }
        }

        // console.debug('state marginProfitConvertPrice', convertPrice)
        return convertPrice;
    });
    const plConvertPrice = useSelector((state: State) => 
    {
        var convertPrice = 1;

        if (contractSymbol) {
            const _contractPricesSelf = state.trading.prices[contractSymbol.SymbolName];

            const first = contractSymbol.SymbolName.substring(0, 3);
            const second = contractSymbol.SymbolName.substring(3, 6);
            const group = contractSymbol.SymbolName.substring(6, contractSymbol.SymbolName.length);
            
            if (first === 'USD' && contractSymbol.MarginMode === CalcMode.FOREX){
                //Direct Quote
                convertPrice = 1/ state.trading.currentTradeSL.StopLossPrice
            } else if (second === 'USD' && contractSymbol.MarginMode === CalcMode.FOREX){
                //Indriect Quote
            } else {
                const directQuote = state.trading.prices[contractSymbol.ProfitConversionCurrency1];
                const inDirectQuote = state.trading.prices[contractSymbol.ProfitConversionCurrency2];
                if (inDirectQuote) {
                    //Cross Indriect Quote
                    // console.debug('updateRequireMargin Cross Indriect Quote')
                    // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
                    convertPrice = isSelectBuy ? inDirectQuote.Bid : inDirectQuote.Ask
                } else if (directQuote) {
                    //Cross Driect Quote
                    // console.debug('updateRequireMargin Cross Driect Quote')
                    convertPrice = 1/ (isSelectBuy ? directQuote.Bid : directQuote.Ask)
                }
            }
        }

        // console.debug('state plConvertPrice', convertPrice)
        return convertPrice;
    });
    const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);
    // const currentTrade = useSelector((state: State) => state.trading.currentTrade);
    const currentTradeSL = useSelector((state: State) => state.trading.currentTradeSL);
    
    const [contractStepPrice, setContractStepPrice] = useState<number>(1);
    const [contractDP, setContractDP] = useState<number>(6);
    const [contractSize, setContractSize] = useState<number>(0);
    const [requiredMargin, SetRequiredMargin] = useState<number>(0);

    // const [priceRef, setPriceRef] = useState<number>(-1);
    const [stopLossPriceUpdate, setStopLossPriceUpdate] = useState<boolean>(false);
    const [stopLossHintSign, SetStopLossHintSign] = useState<string>('>');
    const [stopLossHintPrice, SetStopLossHintPrice] = useState<number>(0);
    const [stopLossPredictPL, SetStopLossPredictPL] = useState<number>(-0.050);

    const [isCompleteEditOrderLoad, SetIsCompleteEditOrderLoad] = useState<boolean>(false);

    const position = useSelector((state: State) => isEditPosition ? state.trading.positions![transactionId!] : undefined);
    const profit = useSelector((state: State) => isEditPosition ? state.trading.profits![transactionId!] : undefined);
    const order = useSelector((state: State) => isEditOrder ? state.trading.orders![transactionId!] : undefined);

    const depositCurrency = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.DepositCurrency);
    const currentContractPrice = useSelector((state: State) => {
        if (contractSymbol) {
            return state.trading.prices[contractSymbol.SymbolName]
        }
    });
    const needCalculateExchangeRate = useSelector((state: State) => {
        if (contractSymbol) {
            return !(contractSymbol.MarginCurrency === depositCurrency)
        } else {
            return false;
        }
    }, shallowEqual);
    const estimatedMarginRate = useSelector((state: State) => {
        var marginRate = 1

        if (contractSymbol) {
            let isFroex = contractSymbol.MarginMode === CalcMode.FOREX
            let baseCurrency = contractSymbol.SymbolName.substring(0, 3)
            let quoteCurrency = contractSymbol.SymbolName.substring(3, 6)
            let isBaseInMargin = baseCurrency === contractSymbol.MarginCurrency
            let isQuoteInDeposit = quoteCurrency === depositCurrency
            let isIndirectQuote = isFroex && isBaseInMargin && isQuoteInDeposit

            if (isIndirectQuote) {
                if (isPending) {
                    marginRate = pendingOrderPrice
                } else if (isEditPosition && position) {
                    marginRate = position.OpenPrice
                } else if (isEditOrder && order) {
                    marginRate = order.PriceOrder
                } else {
                    marginRate = isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid
                }
            }
            else {
                // When contract's margin currency NOT equal to deposit currency
                if (needCalculateExchangeRate) {
                    // Getting Symbol's region
                    let symbolSuffix = ''

                    // Only Forex need find corresponding group
                    if (isFroex) {
                        let index = contractSymbol.SymbolName.indexOf('.')
                        symbolSuffix = index == -1 ? '' : contractSymbol.SymbolName.substring(index, contractSymbol.SymbolName.length)
                    }

                    let averagePrice = 0
                    // let exchangeRateSymbolName = contractSymbol.MarginCurrency + depositCurrency + symbolSuffix
                    //console.debug('marginValue, line 89 exchangeRateSymbolName:' + exchangeRateSymbolName, contractSymbol.MarginConversionCurrency2)
                    const state: State = store.getState();
                    let exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency2] //contractPrices[exchangeRateSymbolName]
                    if (exchangeContractPrice) {
                        let bid = exchangeContractPrice.Bid
                        let ask = exchangeContractPrice.Ask
                        averagePrice = (bid + ask) / 2
                    }
                    else {
                        // exchangeRateSymbolName = depositCurrency + contractSymbol.MarginCurrency + symbolSuffix
                         //console.debug('marginValue, line 99 exchangeRateSymbolName:' + exchangeRateSymbolName, contractSymbol.MarginConversionCurrency1)
                        exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency1] //contractPrices[exchangeRateSymbolName]

                        if (exchangeContractPrice) {
                            let bid = exchangeContractPrice.Bid
                            let ask = exchangeContractPrice.Ask
                            averagePrice = 1 / ((bid + ask) / 2)
                        }
                    }
                    //console.debug('marginValue, averagePrice:' + averagePrice)
                    marginRate = averagePrice
                }
            }

        }
        return marginRate
    }, shallowEqual);

    const [positionObj, setPositionObj] = useState<OpenPositionContent>(undefined);

    const dispatch = useDispatch();
    const { SetCurrentTradeSL } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const multiSwitchButton3 = [
        {
            title: t('trade.price'),
            function: () => { setProfitType(ProfitStopLossOrderType.PRICE) }
        },
        {
            title: t('trade.point'),
            function: () => { setProfitType(ProfitStopLossOrderType.PIPS) }
        },
        {
            title: t('trade.percentage'),
            function: () => { setProfitType(ProfitStopLossOrderType.PERCENTAGE) }
        }
    ]

    useEffect(() => { 
        if (isEditPosition && position){
            setPositionObj(position)
            setContractName(position.Symbol)

            var profit = position.SL !== 0;
            console.debug('profit', profit)
            if (profit) {
                // setStopLossPrice(position.SL === 0 ? -1 : position.SL)
                SetCurrentTradeSL({
                    ...currentTradeSL,
                    StopLossPrice: position.SL === 0 ? undefined : position.SL
                })

                setStopLossPriceUpdate(!stopLossPriceUpdate)
            }
        }
        if (isEditOrder && order){
            setContractName(order.Symbol)

            var profit = order.SL !== 0;
            console.debug('profit', profit)
            if (profit) {
                // setStopLossPrice(order.SL === 0 ? -1 : order.SL)
                SetCurrentTradeSL({
                    ...currentTradeSL,
                    StopLossPrice: order.SL === 0 ? undefined : order.SL
                })

                setStopLossPriceUpdate(!stopLossPriceUpdate)
            }
        }
    }, [profitType])

    useEffect(() => {
        if (contractSymbol) {
            setContractStepPrice(contractSymbol.Point)
            setContractDP(contractSymbol.Digits)
            setContractSize(contractSymbol.ContractSize)
            setStopLossPriceUpdate(!stopLossPriceUpdate)
        }
    }, [contractSymbol])


    //Reset SL TP price to N/A
    useEffect(()=>{
        // console.debug('useEffect 4')
        if ((!isEditOrder || (isEditOrder && isCompleteEditOrderLoad)) && !isEditPosition) {
            // setStopLossPrice(-1)
            // setIsSLOrderShowError(false)

            if (profitType === ProfitStopLossOrderType.PIPS) {
                SetStopLossHintSign('≥')

                var bsPoint = contractSymbol!.StopsLevel //Y
                SetStopLossHintPrice(Number(bsPoint.toFixed(0)))

            } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
                SetStopLossHintSign('≥')
                var slHintPrice = percentageOrderSpread
                // var tpHintPrice = percentageOrderSpread
                SetStopLossHintPrice(slHintPrice)
                // SetStopLossPredictPL(0)
                // setStopLossPriceRef(-1)
            }
        }
    }, [isSelectBuy, isPending, pendingOrderType, profitType])

    // //Update Percentage Hint
    // useEffect(()=>{
    //     if ((!isEditOrder || (isEditOrder && isCompleteEditOrderLoad))) {
    //         if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //             SetStopLossHintSign('≥')
    //             var slHintPrice = percentageOrderSpread
                
    //             var price = isPending ? pendingOrderPrice : (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) 
    //             var pointValue = contractSymbol.Point * contractSymbol.ContractSize * plConvertPrice;
    //             // var marginValue =  1;
    //             // if (isEditPosition && position && contractSymbol.MarginMode !== 1) {
    //             //     //Required Margin
    //             //     var marginDivider = contractSymbol.MarginDivider;
    //             //     var marginRate = position.MarginRate;
            
    //             //     marginValue = ((position.Volume * contractSymbol.ContractSize) / accountLeverage)/marginDivider * marginRate / plConvertPrice
    //             // } else {
    //             //     marginValue =  getEstMargin(contractSymbol, currentLot, price, accountLeverage, convertPrice);
    //             // }
    //             var marginValue = 1;

    //             let calculateMarginParams: CalculateMarginInterface = {
    //                 symbol: contractSymbol.SymbolName,
    //                 marginMode: contractSymbol.MarginMode,
    //                 isFixedMargin: !(contractSymbol.InitialMargin === 0),
    //                 margin: isEditPosition ? contractSymbol.Maintenance : contractSymbol.InitialMargin,
    //                 contractSize: contractSymbol.ContractSize,
    //                 percentage: contractSymbol.Percentage,
    //                 tickSize: contractSymbol.TickSize,
    //                 tickValue: contractSymbol.TickValue,
    //                 marginCurrency: contractSymbol.MarginCurrency,
    //                 accountLeverage: accountLeverage,
    //                 lots: currentLot,
    //                 price: isEditPosition ? position.OpenPrice : (isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid),
    //                 marginRate: isEditPosition ? position.MarginRate : estimatedMarginRate,
    //             }
    //             marginValue = calculateMargin(calculateMarginParams)

    //             var slPointValue = (contractSymbol.StopsLevel * pointValue * currentLot)/marginValue;
    //             // console.debug('slPointValue', slPointValue)
    //             SetStopLossHintPrice(slPointValue > slHintPrice ? slPointValue : slHintPrice)
    //         }
    //     }
    // }, [isSelectBuy, isPending, profitType, contractSymbol, contractPrice, currentLot])

    //Update st/tp reference price (Market Order)
    useEffect(() => {
        if (contractPrice && contractSymbol) {
            //console.debug('useEffect 5')
            var point = contractSymbol.Point;
            var stopsLevel = contractSymbol.StopsLevel;

            if (!isPending && profitType === ProfitStopLossOrderType.PRICE){
            // if (profitType === ProfitStopLossOrderType.PRICE){
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                if (isSelectBuy){ 
                    SetStopLossHintSign('≤')
                    var slHintPrice = bid - (stopsLevel * point)
                    SetStopLossHintPrice(slHintPrice)
                } else { 
                    SetStopLossHintSign('≥')
                    var slHintPrice = ask + (stopsLevel * point)
                    SetStopLossHintPrice(slHintPrice)
                }
            }

            //Update hint points value
            if (profitType === ProfitStopLossOrderType.PIPS) {
                SetStopLossHintSign('≥')
                // if (positionObj) {
                //     var point = contractSymbol.Point;
                //     var stopsLevel = contractSymbol.StopsLevel;
                //     var bid = contractPrice.Bid;
                //     var ask = contractPrice.Ask;
                //     var bsPoint = contractSymbol!.StopsLevel * point //Y

                //     var refPrice = 0
                //     if (isSelectBuy) {
                //         refPrice = positionObj.OpenPrice - (bid - bsPoint)
                //     } else {
                //         refPrice = (ask + bsPoint) - positionObj.OpenPrice
                //     }
                //     var hint = Number((refPrice/point).toFixed(0));
                //     SetStopLossHintPrice(hint)
                // } else {
                //     SetStopLossHintPrice(contractSymbol!.StopsLevel)
                // }

                var bsPoint = contractSymbol!.StopsLevel //Y
                SetStopLossHintPrice(Number(bsPoint.toFixed(0)))
            }
            else if (isEditPosition && profitType === ProfitStopLossOrderType.PERCENTAGE) {
                SetStopLossHintSign('≥')
                // if (positionObj) {
                //     var price = isPending ? pendingOrderPrice : (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) 
                //     // var convertPrice = getConvertPrice(isSelectBuy, contractSymbol, contractPrices, isEditPosition, positionObj ? positionObj.OpenPrice : 0);
                //     var marginValue =  getEstMargin(contractSymbol, currentLot, price, accountLeverage, convertPrice);
                //     // var plConvertPrice = getPLConvertPrice(isSelectBuy, contractSymbol, contractPrices)
                //     var refPrice = 0;
                //     var bid = contractPrice.Bid;
                //     var ask = contractPrice.Ask;
                //     var point = contractSymbol!.Point;

                //     var refPrice = 0
                //     if (isSelectBuy) {
                //         refPrice = bid - (stopsLevel * point)
                //     } else {
                //         refPrice = ask + (stopsLevel * point)
                //     }

                //     var minPLDiff = ((refPrice-positionObj.OpenPrice) * contractSize * currentLot) * plConvertPrice;
                //     var hint = Number(Math.ceil(Math.abs(minPLDiff/marginValue*100)).toFixed(0));
                //     if (hint > percentageOrderSpread) {
                //         SetStopLossHintPrice(hint)
                //     }   
                //     else {
                //         SetStopLossHintPrice(percentageOrderSpread)
                //     }
                // } else {
                //     SetStopLossHintPrice(percentageOrderSpread)
                // }       
                SetStopLossHintPrice(percentageOrderSpread)   
            }
        }
    }, [isSelectBuy, pendingOrderPrice, contractPrice, profitType, contractSymbol])

    //Update st/tp reference price (Pending Order)
    useEffect(() => {
        //console.debug('useEffect 6')
        if (contractPrice && contractSymbol) {
            var spread = contractPrice.Ask - contractPrice.Bid;
            var point = contractSymbol.Point;
            var stopsLevel = contractSymbol.StopsLevel;

            if (isPending && profitType === ProfitStopLossOrderType.PRICE){
                if (isSelectBuy){ 
                    SetStopLossHintSign('≤')
                    var slHintPrice = pendingOrderPrice - (stopsLevel * point)
                    SetStopLossHintPrice(slHintPrice)
                } else { 
                    SetStopLossHintSign('≥')
                    var slHintPrice = pendingOrderPrice + (stopsLevel * point)
                    SetStopLossHintPrice(slHintPrice)
                }
            }
        }
    }, [isSelectBuy, pendingOrderPrice, isPending, profitType, contractSymbol])

    //checkSLTPPrice (Market Order)
    useEffect(()=>{
        if (!isPending) {
            //console.debug('useEffect 2')
            checkSLTPPrice(); 
        }
    }, [contractPrice, currentTradeSL.StopLossPrice, currentTradeSL.StopLossPriceRef, stopLossHintPrice])

    //checkSLTPPrice (Pending Order)
    useEffect(()=>{
        if (isPending){
            // console.debug('useEffect 3')
            checkSLTPPrice(); 
        }
    }, [pendingOrderPrice, currentTradeSL.StopLossPrice, stopLossHintPrice])

    //Update StopLoss predict PL
    useEffect(() => {
        if (contractPrice) {
            // var plConvertPrice = getPLConvertPrice(isSelectBuy, contractSymbol, contractPrices)
            if (profitType === ProfitStopLossOrderType.PRICE) {
                if (isPending){
                    if (isSelectBuy){ 
                        SetStopLossPredictPL((currentTradeSL.StopLossPrice - pendingOrderPrice ) * contractSize * currentLot * plConvertPrice)
                    } else { 
                        SetStopLossPredictPL((pendingOrderPrice - currentTradeSL.StopLossPrice ) * contractSize * currentLot * plConvertPrice)
                    }
                } else {
                    
                    if (isEditPosition && positionObj) {
                        if (isSelectBuy){ 
                            SetStopLossPredictPL((currentTradeSL.StopLossPrice - positionObj.OpenPrice) * contractSize * currentLot * plConvertPrice)
                        } else { 
                            SetStopLossPredictPL((positionObj.OpenPrice - currentTradeSL.StopLossPrice ) * contractSize * currentLot * plConvertPrice)
                        }
                    } else {
                        var bid = contractPrice.Bid;
                        var ask = contractPrice.Ask;
                        if (isSelectBuy){ 
                            SetStopLossPredictPL((currentTradeSL.StopLossPrice - ask) * contractSize * currentLot * plConvertPrice)
                        } else { 
                            SetStopLossPredictPL((bid - currentTradeSL.StopLossPrice ) * contractSize * currentLot * plConvertPrice)
                        }
                    }
                }
            } else if (profitType === ProfitStopLossOrderType.PIPS) {
                var refPrice = 0;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                var spread = contractSymbol!.Spread;
                var point = contractSymbol!.Point;
                if (currentTradeSL.StopLossPrice) {
                    if (isPending){
                        if (isSelectBuy) {
                            refPrice = pendingOrderPrice - (currentTradeSL.StopLossPrice * point)
                        } else {
                            refPrice = pendingOrderPrice + (currentTradeSL.StopLossPrice * point)
                        }
                    }
                    else {
                        if (isEditPosition && positionObj) {
                            if (isSelectBuy) {
                                // refPrice = positionObj.OpenPrice - currentTradeSL.StopLossPrice * point;
                                refPrice = bid - currentTradeSL.StopLossPrice * point;
                            } else {
                                // refPrice = positionObj.OpenPrice + currentTradeSL.StopLossPrice * point;
                                refPrice = ask + currentTradeSL.StopLossPrice * point;
                            }
                        } else {
                            if (isSelectBuy) {
                                refPrice = bid - currentTradeSL.StopLossPrice * point;
                            } else {
                                refPrice = ask + currentTradeSL.StopLossPrice * point;
                            }
                        }
                    }
                    // setStopLossPriceRef(refPrice);
                    // setPriceRef(refPrice)
                    SetCurrentTradeSL({
                        ...currentTradeSL,
                        StopLossPriceRef: Number(refPrice.toFixed(contractDP))
                    })
                }else {
                    // setStopLossPriceRef(-1);
                    // setPriceRef(-1)
                    SetCurrentTradeSL({
                        ...currentTradeSL,
                        StopLossPriceRef: -1
                    })
                }
                
                var orderPrice = 0;
                if (isPending) {
                    orderPrice = pendingOrderPrice
                } else if (isEditPosition && positionObj) {
                    orderPrice = positionObj.OpenPrice
                } else {
                    orderPrice = isSelectBuy ? ask : bid
                }

                if (isSelectBuy){ 
                    SetStopLossPredictPL((refPrice - orderPrice) * contractSize * currentLot * plConvertPrice)
                } else { 
                    SetStopLossPredictPL((orderPrice - refPrice ) * contractSize * currentLot * plConvertPrice)
                }
            } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
                var marginValue = 1;

                let calculateMarginParams: CalculateMarginInterface = {
                    symbol: contractSymbol.SymbolName,
                    marginMode: contractSymbol.MarginMode,
                    isFixedMargin: !(contractSymbol.InitialMargin === 0),
                    margin: isEditPosition ? contractSymbol.Maintenance : contractSymbol.InitialMargin,
                    contractSize: contractSymbol.ContractSize,
                    percentage: contractSymbol.Percentage,
                    tickSize: contractSymbol.TickSize,
                    tickValue: contractSymbol.TickValue,
                    marginCurrency: contractSymbol.MarginCurrency,
                    accountLeverage: accountLeverage,
                    lots: currentLot,
                    price: isEditPosition ? position.OpenPrice : (isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid),
                    marginRate: isEditPosition ? position.MarginRate : estimatedMarginRate,
                }
                marginValue = calculateMargin(calculateMarginParams)

                // //Check Point validation 2
                // var pointValue = contractSymbol.Point * contractSymbol.ContractSize * plConvertPrice;
                // var slPointValue = (contractSymbol.StopsLevel * pointValue * currentLot)/marginValue;
                // // console.debug('slPointValue ', slPointValue)
                // if (slPointValue > percentageOrderSpread) {
                //     SetStopLossHintPrice(slPointValue)
                // }

                var refPrice = 0;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                var pl = marginValue * currentTradeSL.StopLossPrice/100 * -1 ;

                // console.debug('sl1 marginValue', marginValue)
                // console.debug('sl2 pl', pl)
                // console.debug('sl3 marginProfitConvertPrice', marginProfitConvertPrice)
                var point = contractSymbol!.Point;
                if (currentTradeSL.StopLossPrice) {
                    if (isPending) {
                        if (isSelectBuy) {
                            refPrice = (pendingOrderPrice + (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                        } else {
                            refPrice = (pendingOrderPrice - (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                        }
                    } else {
                        if (isEditPosition && positionObj) {
                            if (isSelectBuy) {
                                refPrice = (bid + (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            } else {
                                refPrice = (ask - (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            }
                        } else {
                            if (isSelectBuy) {
                                refPrice = (ask + ((pl*marginProfitConvertPrice) / (contractSize * currentLot)))
                            } else {
                                refPrice = (bid - ((pl*marginProfitConvertPrice) / (contractSize * currentLot)))
                            }
                        }
                    }
                    // console.debug('refPrice SL', refPrice)
                    // setStopLossPriceRef(refPrice);
                    SetCurrentTradeSL({
                        ...currentTradeSL,
                        StopLossPriceRef: Number(refPrice.toFixed(contractDP))
                    })
                }
                
                if (isEditPosition) {
                    // console.debug('isEditPosition', pl, profit)
                    pl += profit
                }
                SetStopLossPredictPL(pl)
            }
        }
    }, [currentTradeSL.StopLossPrice, contractPrice, pendingOrderPrice])

    const checkSLTPPrice = () => {
        if (!isEditOrder || (isEditOrder && isCompleteEditOrderLoad)) {
            var _stopLossPrice = currentTradeSL.StopLossPrice ? Number(currentTradeSL.StopLossPrice.toFixed(contractDP)) : -1
            var _stopLossHintPrice = Number(stopLossHintPrice.toFixed(contractDP))
        
            var condition1 = true;
            var condition2 = true;

            if (isSelectBuy){ 
                if (currentTradeSL.StopLossPrice) {
                    var isError = profitType === ProfitStopLossOrderType.PRICE ? _stopLossPrice > _stopLossHintPrice : _stopLossPrice < _stopLossHintPrice;
                    if (isError) {
                        condition1 = false;
                    } else {
                        condition1 = true;
                    }
                } else {
                    condition1 = true;
                }
            } else { 
                if (currentTradeSL.StopLossPrice) {
                    var isError = _stopLossPrice < _stopLossHintPrice;
                    if (isError) {
                        condition1 = false;
                    } else {
                        condition1 = true;
                    }
                } else {
                    condition1 = true;
                }
            }

            //Edit position 2nd condition
            if (isEditPosition && (profitType === ProfitStopLossOrderType.PERCENTAGE)) {
                var point = contractSymbol.Point;
                var stopsLevel = contractSymbol.StopsLevel;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;

                if (currentTradeSL.StopLossPrice && currentTradeSL.StopLossPriceRef > 0) {
                    if (isSelectBuy) {
                        var price = bid - (stopsLevel * point)
                        price = Number(price.toFixed(contractDP))
                        // console.debug('<=sl price ', priceRef, price)
                        if (currentTradeSL.StopLossPriceRef > price) {
                            condition2 = false;
                        } else {
                            condition2 = true;
                        }
                    } else {
                        var price = ask + (stopsLevel * point)
                        price = Number(price.toFixed(contractDP))
                        // console.debug('>=sl price ', priceRef, price)
                        if (currentTradeSL.StopLossPriceRef < price) {
                            condition2 = false;
                        } else {
                            condition2 = true;
                        }
                    }
                } else {
                    condition2 = true;
                }
            }
            setIsSLOrderShowError(!condition1 || !condition2)
        } else if (isEditOrder){
            SetIsCompleteEditOrderLoad(true)
        }
    }

    return (
        <View>
            <InputArithmomether
                label={t('trade.stopLoss') + ' (' + stopLossHintSign + stopLossHintPrice.toFixed(profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0) + (profitType === ProfitStopLossOrderType.PERCENTAGE ? '%' : '') + ')'}
                isRequired={false}
                note={t('trade.predictPL') + ' ' + (currentTradeSL.StopLossPrice ? toCurrencyDisplay(stopLossPredictPL) : '0.00')}
                note2={profitType === ProfitStopLossOrderType.PRICE ? undefined : t('trade.refSLPrice') + ' ' + (currentTradeSL.StopLossPriceRef > 0 ? currentTradeSL.StopLossPriceRef.toFixed(contractDP) : t('positionDetails.na'))}
                value={currentTradeSL.StopLossPrice}
                valueUpdate={stopLossPriceUpdate}
                valuePerStep={profitType === ProfitStopLossOrderType.PRICE ? contractStepPrice : 1}
                onChangeValue={(value: number) => {
                    // setStopLossPrice(value)
                    SetCurrentTradeSL({
                        ...currentTradeSL,
                        StopLossPrice: value
                    })
                }}
                onCancel={()=> {onCancelStopLoss()}}
                dp={profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0} 
                referencePrice={stopLossHintPrice}
                isError={isSLOrderShowError}
                profitType={profitType}
                allowNegative={true}
                />
            {
                isSLOrderShowError ? 
                    <View style={{alignSelf: 'flex-end'}}>
                        <ErrorMessage message={t('trade.error.invalidSL')} />
                    </View>
                : 
                    <View style={{ padding: 10 }} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default StopLossComponent