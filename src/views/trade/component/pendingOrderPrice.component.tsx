import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { PendingOrderBuySellType, PendingOrderType } from '../../../utils/orderUtils';
import Arithmometer from '../../../components/arithmometer.component';
import TextLabel from '../../../components/forms/labels/textLabel.component';
import ErrorMessage from '../../../components/alerts/errorMessage.component';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';

interface ComponentInterface {
    contract: string,
    isSelectBuy: boolean,
    isEditOrder: boolean,
    pendingOrderPrice: number,
    setPendingOrderPrice: Function,
    pendingOrderType: number,
    setPendingOrderType: Function,
    setOrderValidity: Function,
    isPendingOrderShowError: boolean,
    setIsPendingOrderShowError: Function,

    transactionId?: number
}

const PendingOrderPriceComponent = ({
    contract = '',
    isSelectBuy = false,
    isEditOrder = false,
    pendingOrderPrice = 0,
    setPendingOrderPrice,
    pendingOrderType = 0,
    setPendingOrderType,
    setOrderValidity,
    isPendingOrderShowError = false,
    setIsPendingOrderShowError,
    transactionId = 0
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, setContractName] = useState<string>(contract);
    const [contractStepPrice, setContractStepPrice] = useState<number>(1);
    const [contractDP, setContractDP] = useState<number>(6);
    const [priceUpdate, setPriceUpdate] = useState<boolean>(false);

    const [nextValidPrice, setNextValidPrice] = useState<number>(1);

    const contractSymbol = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[contractName]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[contractName]! : undefined);
    const order = useSelector((state: State) => isEditOrder ? state.trading.orders![transactionId!] : undefined);

    useEffect(() => { 
        if (isEditOrder && order){
            setContractName(order.Symbol)
            //setIsSelectBuy(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.BUY_STOP)
            setPendingOrderType(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.SELL_LIMIT ? 0 : 1)
            setPendingOrderPrice(order.PriceOrder)
            setOrderValidity(order.TimeExpiration === 0 ? 0 : 1)
        }
    }, [])

    useEffect(() => {
        if (contractSymbol) {
            setContractStepPrice(contractSymbol.Point)
            setContractDP(contractSymbol.Digits)
            setPriceUpdate(!priceUpdate)
        }
    }, [contractSymbol, contractName])

    //Update Pending order price error message
    useEffect(() => {
        if (checkPendingOrderPrice()){
            if (!isPendingOrderShowError)
            {
                setIsPendingOrderShowError(true)
            }
        } else {
            if (isPendingOrderShowError)
                setIsPendingOrderShowError(false)
        }
    }, [pendingOrderPrice, contractPrice])

    //Update pending order price
    useEffect(() => {
        console.debug('component', pendingOrderType, isSelectBuy)
        if (contractPrice && contractSymbol) {
            var bid = contractPrice.Bid;
            var ask = contractPrice.Ask;
            // var spread = contractSymbols.Spread;
            var point = contractSymbol.Point;
            var stopsLevel = contractSymbol.StopsLevel;

            if (!isEditOrder) {
                if (pendingOrderType === PendingOrderType.LIMIT) {
                    if (isSelectBuy){ //BuyLimit
                        setPendingOrderPrice(ask - ((stopsLevel + 1) * point))
                    } else { //SellLimit
                        setPendingOrderPrice(bid + ((stopsLevel + 1) * point))
                    }
                } else {
                    if (isSelectBuy){ //BuyStop
                        setPendingOrderPrice(ask + ((stopsLevel + 1) * point))
                    } else { //SellStop
                        setPendingOrderPrice(bid - ((stopsLevel + 1) * point))
                    }
                }
                setPriceUpdate(!priceUpdate)
            }
        }
    }, [pendingOrderType, isSelectBuy])

    const checkPendingOrderPrice = () => {
        if (contractSymbol && contractPrice) {
            var stopLevel = contractSymbol.StopsLevel;
            var spread = contractSymbol?.Spread;
            var point = contractSymbol?.Point;
            // console.debug('spread point', spread, point)
            if (pendingOrderType === PendingOrderType.LIMIT) {
                if (isSelectBuy){ //BuyLimit
                    var price = contractPrice.Ask - (stopLevel * point);
                    setNextValidPrice(price - point)
                    return !(pendingOrderPrice < price)
                } else { //SellLimit
                    var price = contractPrice.Bid + (stopLevel * point);
                    setNextValidPrice(price + point)
                    return !(pendingOrderPrice > price)
                }
            } else {
                if (isSelectBuy){ //BuyStop
                    var price = contractPrice.Ask + (stopLevel * point);
                    setNextValidPrice(price + point)
                    return !(pendingOrderPrice > price)
                } else { //SellStop
                    var price = contractPrice.Bid - (stopLevel * point);
                    setNextValidPrice(price - point)
                    return !(pendingOrderPrice < price)
                }
            }
        } else {
            return false;
        }
    }

    return (
        <View>
            <Arithmometer
                value={pendingOrderPrice}
                valueUpdate={priceUpdate}
                valuePerStep={contractStepPrice}
                nextValidPrice={nextValidPrice}
                onChangeValue={(value: number) => {
                    setPendingOrderPrice(value)
                }}
                style={{ paddingHorizontal: 16 }}
                minimunValue={0}
                maximunValue={999999}
                headerComponent={<TextLabel label={isSelectBuy ? t('trade.buyAt') : t('trade.sellAt')} labelStyle={[globalStyles.Small_Note]} />}
                valueStyle={[globalStyles.H2, isPendingOrderShowError ? {color: colors.Red} : {}]}
                dp={contractDP}
            />
            {
                isPendingOrderShowError ? 
                <View style={{alignSelf: 'center'}}>
                    <ErrorMessage message={t('trade.error.invalidOrderPrice')} />
                </View>
                : null
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default PendingOrderPriceComponent