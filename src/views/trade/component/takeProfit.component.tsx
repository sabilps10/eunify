import React, { ReactNode, useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import { CalcMode, calculateMargin, CalculateMarginInterface, getConvertPrice, getEstMargin, getPLConvertPrice, ProfitStopLossOrderType } from '../../../utils/orderUtils';
import MultiSwitchButton from '../../../components/buttons/multiSwitchButton.component';
import InputArithmomether from '../../../components/forms/inputs/inputArithmomether.component';
import ErrorMessage from '../../../components/alerts/errorMessage.component';
import { percentageOrderSpread } from '../../../config/constants';
import { bindActionCreators } from 'redux';
import { OpenPositionsActionCreators } from '../../../redux/trading/actionCreator';
import { OpenPositionContent } from '../../../redux/trading/type';
import { store } from '../../../redux/store';

interface ComponentInterface {
    contract: string,

    isPending: boolean,
    pendingOrderType: number,
    pendingOrderPrice: number,
    
    currentLot: number,
    isSelectBuy: boolean,
    isEditPosition: boolean,
    isEditOrder: boolean,
    profitType: number,
    setProfitType: Function,

    // limitProfitPrice: number,
    // setLimitProfitPrice: Function
    onCancelLimitProfit: Function,
    // setLimitProfitPriceRef: Function,
    isTPOrderShowError: boolean,
    setIsTPOrderShowError: Function,

    transactionId?: number
}

const TakeProfitComponent = ({
    contract = '',
    isPending = false,
    pendingOrderType = 0,
    pendingOrderPrice = 0,
    currentLot = 0,
    isSelectBuy = false,
    isEditPosition = false,
    isEditOrder = false,
    profitType = 0,
    setProfitType,
    // limitProfitPrice = 0,
    // setLimitProfitPrice,
    onCancelLimitProfit,
    // setLimitProfitPriceRef,
    isTPOrderShowError = false,
    setIsTPOrderShowError,
    transactionId = 0
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [contractName, setContractName] = useState<string>(contract);

    const contractSymbol = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[contractName]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[contractName]! : undefined);
    const marginProfitConvertPrice = useSelector((state: State) => 
    {
        // console.debug('_contractPricesMargin1', contractSymbol.ProfitConversionCurrency1, contractSymbol.ProfitConversionCurrency2)

        var convertPrice = 1;

        if (contractSymbol) {

            const _contractPricesSelf = state.trading.prices[contractSymbol.SymbolName];
            const _contractPricesMargin1 = state.trading.prices[contractSymbol.ProfitConversionCurrency1];
            const _contractPricesMargin2 = state.trading.prices[contractSymbol.ProfitConversionCurrency2];

            const first = contractSymbol.SymbolName.substring(0, 3);
            const second = contractSymbol.SymbolName.substring(3, 6);
            const group = contractSymbol.SymbolName.substring(6, contractSymbol.SymbolName.length);
            
            if (first === 'USD'){
                //Direct Quote
                convertPrice = (isSelectBuy ? _contractPricesSelf.Bid : _contractPricesSelf.Ask)
            } else if (second === 'USD'){
                //Indriect Quote
            } else {
                const quote1 = _contractPricesMargin1;
                const quote2 = _contractPricesMargin2;
                if (quote1) {
                    //Cross Indriect Quote
                    // console.debug('updateRequireMargin Cross Indriect Quote')
                    // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
                    convertPrice = (isSelectBuy ? quote1.Bid : quote1.Ask)
                } else if (quote2) {
                    //Cross Driect Quote
                    // console.debug('updateRequireMargin Cross Driect Quote')
                    convertPrice = 1/(isSelectBuy ? quote2.Bid : quote2.Ask)
                }
            }
        }

        // console.debug('state marginProfitConvertPrice', convertPrice)
        return convertPrice;
    });
    const plConvertPrice = useSelector((state: State) => 
    {
        var convertPrice = 1;

        if (contractSymbol) {
            const _contractPricesSelf = state.trading.prices[contractSymbol.SymbolName];

            const first = contractSymbol.SymbolName.substring(0, 3);
            const second = contractSymbol.SymbolName.substring(3, 6);
            const group = contractSymbol.SymbolName.substring(6, contractSymbol.SymbolName.length);
            
            if (first === 'USD' && contractSymbol.MarginMode === CalcMode.FOREX){
                //Direct Quote
                convertPrice = 1/ state.trading.currentTradeTP.TakeProfitPrice
            } else if (second === 'USD' && contractSymbol.MarginMode === CalcMode.FOREX){
                //Indriect Quote
            } else {
                const directQuote = state.trading.prices[contractSymbol.ProfitConversionCurrency1];
                const inDirectQuote = state.trading.prices[contractSymbol.ProfitConversionCurrency2];

                if (inDirectQuote) {
                    //Cross Indriect Quote
                    // console.debug('updateRequireMargin Cross Indriect Quote')
                    // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
                    convertPrice = isSelectBuy ? inDirectQuote.Bid : inDirectQuote.Ask
                } else if (directQuote) {
                    //Cross Driect Quote
                    // console.debug('updateRequireMargin Cross Driect Quote')
                    convertPrice = 1/ (isSelectBuy ? directQuote.Bid : directQuote.Ask)
                }
            }
        }

        // console.debug('state plConvertPrice', convertPrice)
        return convertPrice;
    });
    const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);
    const currentTradeTP = useSelector((state: State) => state.trading.currentTradeTP);
    
    const [contractStepPrice, setContractStepPrice] = useState<number>(1);
    const [contractDP, setContractDP] = useState<number>(6);
    const [contractSize, setContractSize] = useState<number>(0);
    const [requiredMargin, SetRequiredMargin] = useState<number>(0);

    // const [priceRef, setPriceRef] = useState<number>(-1);
    const [limitProfitPriceUpdate, setLimitProfitPriceUpdate] = useState<boolean>(false);
    const [limitProfitHintSign, SetLimitProfitHintSign] = useState<string>('>');
    const [limitProfitHintPrice, SetLimitProfitHintPrice] = useState<number>(0);
    const [limitProfitPredictPL, SetLimitProfitPredictPL] = useState<number>(0.050);

    const [isCompleteEditOrderLoad, SetIsCompleteEditOrderLoad] = useState<boolean>(false);

    const position = useSelector((state: State) => isEditPosition ? state.trading.positions![transactionId!] : undefined);
    const profit = useSelector((state: State) => isEditPosition ? state.trading.profits![transactionId!] : undefined);
    const order = useSelector((state: State) => isEditOrder ? state.trading.orders![transactionId!] : undefined);

    const depositCurrency = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.DepositCurrency);
    const currentContractPrice = useSelector((state: State) => {
        if (contractSymbol) {
            return state.trading.prices[contractSymbol.SymbolName]
        }
    });
    const needCalculateExchangeRate = useSelector((state: State) => {
        if (contractSymbol) {
            return !(contractSymbol.MarginCurrency === depositCurrency)
        } else {
            return false;
        }
    }, shallowEqual);
    const estimatedMarginRate = useSelector((state: State) => {
        var marginRate = 1

        if (contractSymbol) {
            let isFroex = contractSymbol.MarginMode === CalcMode.FOREX
            let baseCurrency = contractSymbol.SymbolName.substring(0, 3)
            let quoteCurrency = contractSymbol.SymbolName.substring(3, 6)
            let isBaseInMargin = baseCurrency === contractSymbol.MarginCurrency
            let isQuoteInDeposit = quoteCurrency === depositCurrency
            let isIndirectQuote = isFroex && isBaseInMargin && isQuoteInDeposit

            if (isIndirectQuote) {
                if (isPending) {
                    marginRate = pendingOrderPrice
                } else if (isEditPosition && position) {
                    marginRate = position.OpenPrice
                } else if (isEditOrder && order) {
                    marginRate = order.PriceOrder
                } else {
                    marginRate = isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid
                }
            }
            else {
                // When contract's margin currency NOT equal to deposit currency
                if (needCalculateExchangeRate) {
                    // Getting Symbol's region
                    let symbolSuffix = ''

                    // Only Forex need find corresponding group
                    if (isFroex) {
                        let index = contractSymbol.SymbolName.indexOf('.')
                        symbolSuffix = index == -1 ? '' : contractSymbol.SymbolName.substring(index, contractSymbol.SymbolName.length)
                    }

                    let averagePrice = 0
                    // let exchangeRateSymbolName = contractSymbol.MarginCurrency + depositCurrency + symbolSuffix
                    //console.debug('marginValue, line 89 exchangeRateSymbolName:' + exchangeRateSymbolName, contractSymbol.MarginConversionCurrency2)
                    const state: State = store.getState();
                    let exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency2] //contractPrices[exchangeRateSymbolName]
                    if (exchangeContractPrice) {
                        let bid = exchangeContractPrice.Bid
                        let ask = exchangeContractPrice.Ask
                        averagePrice = (bid + ask) / 2
                    }
                    else {
                        // exchangeRateSymbolName = depositCurrency + contractSymbol.MarginCurrency + symbolSuffix
                         //console.debug('marginValue, line 99 exchangeRateSymbolName:' + exchangeRateSymbolName, contractSymbol.MarginConversionCurrency1)
                        exchangeContractPrice = state.trading.prices[contractSymbol.MarginConversionCurrency1] //contractPrices[exchangeRateSymbolName]

                        if (exchangeContractPrice) {
                            let bid = exchangeContractPrice.Bid
                            let ask = exchangeContractPrice.Ask
                            averagePrice = 1 / ((bid + ask) / 2)
                        }
                    }
                    //console.debug('marginValue, averagePrice:' + averagePrice)
                    marginRate = averagePrice
                }
            }

        }
        return marginRate
    }, shallowEqual);
    
    const [positionObj, setPositionObj] = useState<OpenPositionContent>(undefined);
    
    const dispatch = useDispatch();
    const { SetCurrentTradeTP } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const multiSwitchButton3 = [
        {
            title: t('trade.price'),
            function: () => { setProfitType(ProfitStopLossOrderType.PRICE) }
        },
        {
            title: t('trade.point'),
            function: () => { setProfitType(ProfitStopLossOrderType.PIPS) }
        },
        {
            title: t('trade.percentage'),
            function: () => { setProfitType(ProfitStopLossOrderType.PERCENTAGE) }
        }
    ]

    useEffect(() => {
        if (isEditPosition && position){
            setPositionObj(position)
            setContractName(position.Symbol)

            var profit = position.TP !== 0;
            if (profit) {
                // setLimitProfitPrice(position.TP === 0 ? -1 : position.TP)
                SetCurrentTradeTP({
                    ...currentTradeTP,
                    TakeProfitPrice: position.TP === 0 ? -1 : position.TP
                })


                setLimitProfitPriceUpdate(!limitProfitPriceUpdate)
            }
        } 
        if (isEditOrder && order){
            setContractName(order.Symbol)

            var profit = order.TP !== 0;
            if (profit) {
                // setLimitProfitPrice(order.TP === 0 ? -1 : order.TP)
                SetCurrentTradeTP({
                    ...currentTradeTP,
                    TakeProfitPrice: order.TP === 0 ? -1 : order.TP
                })

                setLimitProfitPriceUpdate(!limitProfitPriceUpdate)
            }
        }
    }, [profitType])

    useEffect(() => {
        if (contractSymbol) {
            setContractStepPrice(contractSymbol.Point)
            setContractDP(contractSymbol.Digits)
            setContractSize(contractSymbol.ContractSize)
            setLimitProfitPriceUpdate(!limitProfitPriceUpdate)
        }
    }, [contractSymbol])


    //Reset SL TP price to N/A
    useEffect(()=>{
        //console.debug('useEffect 4')
        if ((!isEditOrder || (isEditOrder && isCompleteEditOrderLoad)) && !isEditPosition) {
            // setLimitProfitPrice(-1)
            // setIsTPOrderShowError(false)

            if (profitType === ProfitStopLossOrderType.PIPS) {
                SetLimitProfitHintSign('≥')
                
                var bsPoint = contractSymbol!.StopsLevel //Y
                SetLimitProfitHintPrice(Number(bsPoint.toFixed(0)))

            } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
                SetLimitProfitHintSign('≥')
                // var slHintPrice = percentageOrderSpread
                var tpHintPrice = percentageOrderSpread
                SetLimitProfitHintPrice(tpHintPrice)
                // SetLimitProfitPredictPL(0)
                // setLimitProfitPriceRef(-1)
            }
        }
    }, [isSelectBuy, isPending, pendingOrderType, profitType])

    // //Update Percentage Hint
    // useEffect(()=>{
    //     if ((!isEditOrder || (isEditOrder && isCompleteEditOrderLoad)) && !isEditPosition) {
    //         if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //             SetLimitProfitHintPrice('≥')
    //             var tpHintPrice = percentageOrderSpread
                
    //             var price = isPending ? pendingOrderPrice : (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) 
    //             var pointValue = contractSymbol.Point * contractSymbol.ContractSize * plConvertPrice;
    //             // var marginValue =  1;
    //             // if (isEditPosition && position && contractSymbol.MarginMode !== 1) {
    //             //     //Required Margin
    //             //     var marginDivider = contractSymbol.MarginDivider;
    //             //     var marginRate = position.MarginRate;
            
    //             //     marginValue = ((position.Volume * contractSymbol.ContractSize) / accountLeverage)/marginDivider * marginRate / plConvertPrice
    //             // } else {
    //             //     marginValue =  getEstMargin(contractSymbol, currentLot, price, accountLeverage, convertPrice);
    //             // }
    //             var marginValue = 1;

    //             let calculateMarginParams: CalculateMarginInterface = {
    //                 symbol: contractSymbol.SymbolName,
    //                 marginMode: contractSymbol.MarginMode,
    //                 isFixedMargin: !(contractSymbol.InitialMargin === 0),
    //                 margin: isEditPosition ? contractSymbol.Maintenance : contractSymbol.InitialMargin,
    //                 contractSize: contractSymbol.ContractSize,
    //                 percentage: contractSymbol.Percentage,
    //                 tickSize: contractSymbol.TickSize,
    //                 tickValue: contractSymbol.TickValue,
    //                 marginCurrency: contractSymbol.MarginCurrency,
    //                 accountLeverage: accountLeverage,
    //                 lots: currentLot,
    //                 price: isEditPosition ? position.OpenPrice : (isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid),
    //                 marginRate: isEditPosition ? position.MarginRate : estimatedMarginRate,
    //             }
    //             marginValue = calculateMargin(calculateMarginParams)

    //             var tpPointValue = (contractSymbol.StopsLevel * pointValue * currentLot)/marginValue;
    //             // console.debug('tpPointValue', tpPointValue)
    //             SetLimitProfitHintPrice(tpPointValue > tpHintPrice ? tpPointValue : tpHintPrice)
    //         }
    //     }
    // }, [isSelectBuy, isPending, profitType, contractSymbol, contractPrice, currentLot])

    //Update st/tp reference price (Market Order)
    useEffect(() => {
        if (contractPrice && contractSymbol) {
            //console.debug('useEffect 5')
            var point = contractSymbol.Point;
            var stopsLevel = contractSymbol.StopsLevel;

            if (!isPending && profitType === ProfitStopLossOrderType.PRICE){
            // if (profitType === ProfitStopLossOrderType.PRICE){
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                if (isSelectBuy){ 
                    SetLimitProfitHintSign('≥')
                    var tpHintPrice = bid + (stopsLevel * point)
                    SetLimitProfitHintPrice(tpHintPrice)
                } else { 
                    SetLimitProfitHintSign('≤')
                    var tpHintPrice = ask - (stopsLevel * point)
                    SetLimitProfitHintPrice(tpHintPrice)
                }
            }
            
            //Update hint points value
            else if (isEditPosition && profitType === ProfitStopLossOrderType.PRICE) {
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                if (isSelectBuy){ 
                    SetLimitProfitHintSign('≥')
                    var tpHintPrice = bid + (stopsLevel * point)
                    var plDiff = positionObj ? (tpHintPrice-positionObj.OpenPrice) : 0;
                    if (plDiff < 0) {
                        SetLimitProfitHintPrice(positionObj.OpenPrice)
                    } else {
                        SetLimitProfitHintPrice(tpHintPrice)
                    }
                } else { 
                    SetLimitProfitHintSign('≤')
                    var tpHintPrice = ask - (stopsLevel * point)
                    var plDiff = positionObj ? (positionObj.OpenPrice - tpHintPrice) : 0;
                    if (plDiff < 0) {
                        SetLimitProfitHintPrice(positionObj.OpenPrice)
                    } else {
                        SetLimitProfitHintPrice(tpHintPrice)
                    }
                }
            }
            else if (profitType === ProfitStopLossOrderType.PIPS) {
                SetLimitProfitHintSign('≥')
                // if (positionObj) {
                //     var point = contractSymbol.Point;
                //     var stopsLevel = contractSymbol.StopsLevel;
                //     var bid = contractPrice.Bid;
                //     var ask = contractPrice.Ask;
                //     var bsPoint = contractSymbol!.StopsLevel * point //Y

                //     var refPrice = 0
                //     if (isSelectBuy) {
                //         refPrice = (bid + bsPoint) - positionObj.OpenPrice
                //     } else {
                //         refPrice = positionObj.OpenPrice - (ask - bsPoint )
                //     }
                //     var hint = Number((refPrice/point).toFixed(0));
                //     SetLimitProfitHintPrice(hint > 0 ? hint : 0)
                // } else {
                //     SetLimitProfitHintPrice(contractSymbol!.StopsLevel)
                // }

                var bsPoint = contractSymbol!.StopsLevel //Y
                SetLimitProfitHintPrice(Number(bsPoint.toFixed(0)))
            }
            else if (isEditPosition && profitType === ProfitStopLossOrderType.PERCENTAGE) {
                SetLimitProfitHintSign('≥')
                // if (positionObj) {
                //     var price = isPending ? pendingOrderPrice : (isSelectBuy ? contractPrice.Ask : contractPrice.Bid) 
                //     // var convertPrice = getConvertPrice(isSelectBuy, contractSymbol, contractPrices, isEditPosition, positionObj ? positionObj.OpenPrice : 0);
                //     var marginValue =  getEstMargin(contractSymbol, currentLot, price, accountLeverage, convertPrice);
                //     // var plConvertPrice = getPLConvertPrice(isSelectBuy, contractSymbol, contractPrices)
                //     var refPrice = 0;
                //     var bid = contractPrice.Bid;
                //     var ask = contractPrice.Ask;
                //     var point = contractSymbol!.Point;

                //     var refPrice = 0
                //     if (isSelectBuy) {
                //         refPrice = bid + (stopsLevel * point)
                //     } else {
                //         refPrice = ask - (stopsLevel * point)
                //     }

                //     var minPLDiff = ((refPrice-positionObj.OpenPrice) * contractSize * currentLot) * plConvertPrice ;
                //     var hint = Number(Math.ceil(Math.abs(minPLDiff/marginValue*100)).toFixed(0));
                //     if (hint > percentageOrderSpread) {
                //         SetLimitProfitHintPrice(hint)
                //     }  
                //     else {
                //         SetLimitProfitHintPrice(percentageOrderSpread)
                //     }   
                // } else {
                //     SetLimitProfitHintPrice(percentageOrderSpread)
                // }    
                SetLimitProfitHintPrice(percentageOrderSpread)
            }
        }
    }, [isSelectBuy, pendingOrderPrice, contractPrice, profitType, contractSymbol])

    //Update st/tp reference price (Pending Order)
    useEffect(() => {
        //console.debug('useEffect 6')
        if (contractPrice && contractSymbol) {
            var spread = contractPrice.Ask - contractPrice.Bid;
            var point = contractSymbol.Point;
            var stopsLevel = contractSymbol.StopsLevel;

            if (isPending && profitType === ProfitStopLossOrderType.PRICE){
                if (isSelectBuy){ 
                    SetLimitProfitHintSign('≥')
                    var tpHintPrice = pendingOrderPrice + (stopsLevel * point)
                    SetLimitProfitHintPrice(tpHintPrice)
                } else { 
                    SetLimitProfitHintSign('≤')
                    var tpHintPrice = pendingOrderPrice - (stopsLevel * point)
                    SetLimitProfitHintPrice(tpHintPrice)
                }
            }
        }
    }, [isSelectBuy, pendingOrderPrice, isPending, profitType, contractSymbol])

    //checkSLTPPrice (Market Order)
    useEffect(()=>{
        if (!isPending) {
            //console.debug('useEffect 2')
            checkSLTPPrice(); 
        }
    }, [contractPrice, currentTradeTP.TakeProfitPrice, currentTradeTP.TakeProfitPriceRef, limitProfitHintPrice, limitProfitPredictPL])

    //checkSLTPPrice (Pending Order)
    useEffect(()=>{
        if (isPending){
            //console.debug('useEffect 3')
            checkSLTPPrice(); 
        }
    }, [pendingOrderPrice, currentTradeTP.TakeProfitPrice, limitProfitHintPrice, limitProfitPredictPL])

    //Update TakeProfit predict PL
    useEffect(() => {
        //console.debug('useEffect 13')
        if (contractPrice) {
            // var plConvertPrice = getPLConvertPrice(isSelectBuy, contractSymbol, contractPrices)
            if (profitType === ProfitStopLossOrderType.PRICE) {
                var pl = 0;
                if (isPending){
                    if (isSelectBuy){ 
                        pl = (currentTradeTP.TakeProfitPrice - pendingOrderPrice) * contractSize * currentLot * plConvertPrice;
                        SetLimitProfitPredictPL(pl)
                    } else { 
                        pl = (pendingOrderPrice- currentTradeTP.TakeProfitPrice) * contractSize * currentLot * plConvertPrice;
                        SetLimitProfitPredictPL(pl)
                    }
                } else {
                    if (isEditPosition && positionObj) {
                        if (isSelectBuy){ 
                            pl = (currentTradeTP.TakeProfitPrice - positionObj.OpenPrice) * contractSize * currentLot * plConvertPrice;
                            SetLimitProfitPredictPL(pl)
                        } else { 
                            pl = (positionObj.OpenPrice - currentTradeTP.TakeProfitPrice) * contractSize * currentLot * plConvertPrice;
                            SetLimitProfitPredictPL(pl)
                        }
                    } else {
                        var bid = contractPrice.Bid;
                        var ask = contractPrice.Ask;
                        if (isSelectBuy){ 
                            pl = (currentTradeTP.TakeProfitPrice - ask) * contractSize * currentLot * plConvertPrice;
                            SetLimitProfitPredictPL(pl)
                        } else { 
                            pl = (bid - currentTradeTP.TakeProfitPrice) * contractSize * currentLot * plConvertPrice;
                            SetLimitProfitPredictPL(pl)
                        }
                    }
                }
            }
            else if (profitType === ProfitStopLossOrderType.PIPS) {
                var refPrice = 0;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                var spread = contractSymbol!.Spread;
                var point = contractSymbol!.Point;
                if (currentTradeTP.TakeProfitPrice >= 0) {
                    if (isPending){
                        if (isSelectBuy) {
                            refPrice = pendingOrderPrice + (currentTradeTP.TakeProfitPrice * point)
                        } else {
                            refPrice = pendingOrderPrice - (currentTradeTP.TakeProfitPrice * point)
                        }
                    }
                    else {
                        if (isEditPosition && positionObj) {
                            if (isSelectBuy) {
                                // refPrice = positionObj.OpenPrice + currentTradeTP.TakeProfitPrice * point;
                                refPrice = bid + currentTradeTP.TakeProfitPrice * point;
                            } else {
                                // refPrice = positionObj.OpenPrice - currentTradeTP.TakeProfitPrice * point;
                                refPrice = ask - currentTradeTP.TakeProfitPrice * point;
                            }
                        } else {

                            if (isSelectBuy) {
                                refPrice = bid + currentTradeTP.TakeProfitPrice * point;
                            } else {
                                refPrice = ask - currentTradeTP.TakeProfitPrice * point;
                            }
                        }
                    }
                    // console.debug('tp refPrice', refPrice)
                    // setLimitProfitPriceRef(refPrice);
                    // setPriceRef(refPrice);

                    SetCurrentTradeTP({
                        ...currentTradeTP,
                        TakeProfitPriceRef: Number(refPrice.toFixed(contractDP))
                    })
                } else {
                    // setLimitProfitPriceRef(-1);
                    // setPriceRef(-1);
                    SetCurrentTradeTP({
                        ...currentTradeTP,
                        TakeProfitPriceRef: -1
                    })
                }
                
                var orderPrice = 0;
                if (isPending) {
                    orderPrice = pendingOrderPrice
                } else if (isEditPosition && positionObj) {
                    orderPrice = positionObj.OpenPrice
                } else {
                    orderPrice = isSelectBuy ? ask : bid
                }

                if (isSelectBuy){ 
                    SetLimitProfitPredictPL((refPrice - orderPrice) * contractSize * currentLot * plConvertPrice)
                } else { 
                    SetLimitProfitPredictPL((orderPrice - refPrice) * contractSize * currentLot * plConvertPrice)
                }
            } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
                var marginValue = 1;

                let calculateMarginParams: CalculateMarginInterface = {
                    symbol: contractSymbol.SymbolName,
                    marginMode: contractSymbol.MarginMode,
                    isFixedMargin: !(contractSymbol.InitialMargin === 0),
                    margin: isEditPosition ? contractSymbol.Maintenance : contractSymbol.InitialMargin,
                    contractSize: contractSymbol.ContractSize,
                    percentage: contractSymbol.Percentage,
                    tickSize: contractSymbol.TickSize,
                    tickValue: contractSymbol.TickValue,
                    marginCurrency: contractSymbol.MarginCurrency,
                    accountLeverage: accountLeverage,
                    lots: currentLot,
                    price: isEditPosition ? position.OpenPrice : (isSelectBuy ? currentContractPrice.Ask : currentContractPrice.Bid),
                    marginRate: isEditPosition ? position.MarginRate : estimatedMarginRate,
                }
                marginValue = calculateMargin(calculateMarginParams)

                // //Check Point validation 2
                // var pointValue = contractSymbol.Point * contractSymbol.ContractSize * plConvertPrice;
                // var tpPointValue = (contractSymbol.StopsLevel * pointValue * currentLot)/marginValue;
                // // console.debug('tpPointValue ', tpPointValue)
                // if (tpPointValue > percentageOrderSpread) {
                //     SetLimitProfitHintPrice(tpPointValue)
                // }

                var refPrice = 0;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;
                var pl = marginValue * currentTradeTP.TakeProfitPrice/100

                // console.debug('tp1 marginValue', marginValue)
                // console.debug('tp2 pl', pl)
                // console.debug('tp3 marginProfitConvertPrice', marginProfitConvertPrice)
                var point = contractSymbol!.Point;
                if (currentTradeTP.TakeProfitPrice >= 0) {
                    if (isPending) {
                        if (isSelectBuy) {
                            refPrice = (pendingOrderPrice + (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                        } else {
                            refPrice = (pendingOrderPrice - (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                        }
                    } else {
                        if (isEditPosition && positionObj) {
                            if (isSelectBuy) {
                                refPrice = (bid + (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            } else {
                                refPrice = (ask - (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            }
                        } else {
                            if (isSelectBuy) {
                                refPrice = (ask + (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            } else {
                                refPrice = (bid - (pl*marginProfitConvertPrice / (contractSize * currentLot)))
                            }
                        }
                    }
                    // console.debug('refPrice TP', refPrice)
                    // setLimitProfitPriceRef(refPrice);
                    SetCurrentTradeTP({
                        ...currentTradeTP,
                        TakeProfitPriceRef: Number(refPrice.toFixed(contractDP))
                    })
                }

                if (isEditPosition) {
                    // console.debug('isEditPosition', pl, profit)
                    pl += profit
                }
                SetLimitProfitPredictPL(pl)
            }
        }
    }, [currentTradeTP.TakeProfitPrice, contractPrice, pendingOrderPrice])

    const checkSLTPPrice = () => {
        if (!isEditOrder || (isEditOrder && isCompleteEditOrderLoad)) {
            var _limitProfitPrice = currentTradeTP.TakeProfitPrice ? Number(currentTradeTP.TakeProfitPrice.toFixed(contractDP)) : -1
            var _limitProfitHintPrice = Number(limitProfitHintPrice.toFixed(contractDP))

            var condition1 = true;
            var condition2 = true;
            var condition3 = true;

            if (isSelectBuy){ 
                if (currentTradeTP.TakeProfitPrice > 0) {
                    var isError = _limitProfitPrice < _limitProfitHintPrice;
                    if (isError) {
                        condition1 = false;
                    } else {
                        condition1 = true;
                    }
                } else {
                    condition1 = true;
                }
            } else { 
                if (currentTradeTP.TakeProfitPrice > 0) {
                    var isError = profitType === ProfitStopLossOrderType.PRICE ? _limitProfitPrice > _limitProfitHintPrice : _limitProfitPrice < _limitProfitHintPrice;
                    if (isError) {
                        condition1 = false;
                    } else {
                        condition1 = true;
                    }
                } else {
                    condition1 = true;
                }
            }

            //Edit position 2nd condition
            if (isEditPosition && (profitType === ProfitStopLossOrderType.PERCENTAGE)) {
                var point = contractSymbol.Point;
                var stopsLevel = contractSymbol.StopsLevel;
                var bid = contractPrice.Bid;
                var ask = contractPrice.Ask;

                //console.debug('currentTrade', currentTrade)

                if (currentTradeTP.TakeProfitPrice > 0 && currentTradeTP.TakeProfitPriceRef > 0) {
                    if (isSelectBuy) {
                        var price = bid + (stopsLevel * point)
                        price = Number(price.toFixed(contractDP))
                        if (currentTradeTP.TakeProfitPriceRef < price) {
                            condition2 = false;
                        } else {
                            condition2 = true;
                        }
                    } else {
                        var price = ask - (stopsLevel * point)
                        price = Number(price.toFixed(contractDP))
                        // console.debug('currentTradeTP.TakeProfitPriceRef > price', currentTradeTP.TakeProfitPriceRef > price, currentTradeTP.TakeProfitPriceRef, price)
                        if (currentTradeTP.TakeProfitPriceRef > price) {
                            condition2 = false;
                        } else {
                            condition2 = true;
                        }
                    }
                } else {
                    condition2 = true;
                }
            }

            // if (currentTradeTP.TakeProfitPrice > 0 && currentTradeTP.TakeProfitPriceRef > 0 && limitProfitPredictPL < 0) {
            //     condition3 = false;
            // } else {
            //     condition3 = true;
            // }
            setIsTPOrderShowError(!condition1 || !condition2)
        }
        else if (isEditOrder){
            SetIsCompleteEditOrderLoad(true)
        }

    }

    return (
        <View>
            <InputArithmomether
                label={t('trade.limitProfit') + ' (' + limitProfitHintSign + limitProfitHintPrice.toFixed(profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0) + (profitType === ProfitStopLossOrderType.PERCENTAGE ? '%' : '') + ')'}
                isRequired={false}
                note={t('trade.predictPL') + ' ' + (currentTradeTP.TakeProfitPrice > 0 ? toCurrencyDisplay(limitProfitPredictPL) : '0.00')}
                note2={profitType === ProfitStopLossOrderType.PRICE ? undefined : t('trade.refTPPrice') + ' ' + (currentTradeTP.TakeProfitPriceRef > 0 ? currentTradeTP.TakeProfitPriceRef.toFixed(contractDP) : t('positionDetails.na'))}
                value={currentTradeTP.TakeProfitPrice}
                valueUpdate={limitProfitPriceUpdate}
                valuePerStep={profitType === ProfitStopLossOrderType.PRICE ? contractStepPrice : 1}
                onChangeValue={(value: number) => {
                    // setLimitProfitPrice(value)
                    SetCurrentTradeTP({
                        ...currentTradeTP,
                        TakeProfitPrice: value
                    })
                }}
                onCancel={()=> {onCancelLimitProfit()}}
                dp={profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0}
                referencePrice={limitProfitHintPrice}
                isError={isTPOrderShowError}
                profitType={profitType}
                />
            {
                isTPOrderShowError ? 
                <View style={{alignSelf: 'flex-end'}}>
                    <ErrorMessage message={t('trade.error.invalidTP')} />
                </View>
                : <View style={{ padding: 10 }} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default TakeProfitComponent