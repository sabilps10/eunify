import React, { ReactNode, useEffect, useState } from 'react'
import { ColorValue, StyleSheet, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import DashedLineView from '../../../components/dashedLineView.component';

import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { State } from '../../../redux/root-reducer';
import { toCurrencyDisplay } from '../../../utils/stringUtils';
import TextLabel from '../../../components/forms/labels/textLabel.component';
import { priceChangePercentageDP } from '../../../config/constants';

interface ComponentInterface {
    symbol: string
}

const ContractChangeComponent = ({
    symbol = null
}: ComponentInterface) => {
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [changePercent, SetChangePercent] = useState<string>('');
    const [changePercentColor, SetChangePercentColor] = useState<ColorValue>(colors.Up);

    const contractSymbols = useSelector((state: State) => state.trading.symbols ? state.trading.symbols[symbol]! : undefined);
    const contractPrice = useSelector((state: State) => state.trading.prices ? state.trading.prices[symbol]! : undefined);

    //Update contract price
    useEffect(() => {
        // console.debug('useEffect 9', contractPrice, contractName)
        if (contractPrice && contractSymbols) {
            var priceDiff = contractPrice.Bid - contractPrice.Close;
            var sign = priceDiff > 0 ? '+' : '';
            var priceDiffPercent = priceDiff / contractPrice.Close * 100;
            var signPercent = priceDiffPercent > 0 ? '+' : '';
            SetChangePercent(sign + priceDiff.toFixed(contractSymbols.Digits) + ' (' + signPercent + priceDiffPercent.toFixed(priceChangePercentageDP) + '%)')
            SetChangePercentColor(priceDiff > 0 ? colors.Up : colors.Down)
        }
    }, [contractSymbols, contractPrice])

    return (
        <View>
            <View style={{ justifyContent: 'center', flex: 1 }}>
                <TextLabel label={changePercent} labelStyle={[globalStyles.Body_Text_B, { color: changePercentColor }]} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
})

export default React.memo(ContractChangeComponent)