import { Text } from '@rneui/base';
import React, { Fragment, useLayoutEffect, useState, useEffect, useMemo, useRef } from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, Pressable, StyleSheet, useColorScheme, View } from 'react-native';
import { Props } from '../../routes/route';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ForgetPasswordPhoneEmail from '../../components/login/forgetPasswordPhoneEmail';
import Header from '../../components/header/header.component';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import HeaderButton from '../../components/header/headerButton.component';
import TextLabel from '../../components/forms/labels/textLabel.component';
import BuySellTradeButton from '../../components/buttons/buySellTradeButton.component';
import OrderSlider from '../../components/orderSlider.component';
import InputSwitch from '../../components/forms/inputs/inputSwitch.component';
import ActionButton from '../../components/buttons/actionButton.component';

import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';

import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';

import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AccountActionCreators } from '../../redux/account/actionCreator';

import {toCurrencyDisplay} from '../../utils/stringUtils'

import { getConvertPrice, getEstMargin, OrderActionType, OrderDirectionType, ProfitStopLossOrderType } from '../../utils/orderUtils'
import { AccountUtils } from '../../utils/accountUtils';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { percentageOrderSpread, priceChangePercentageDP } from '../../config/constants';
import DashedLineView from '../../components/dashedLineView.component';
import ErrorMessage from '../../components/alerts/errorMessage.component';
import { WebsocketUtils } from '../../utils/websocketUtils';
import ExitDemoButton from '../../components/buttons/exitDemo.component';
import OrderConfirmDialog from '../../components/dialogs/orderConfirmDialog';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { TabDirectoryTab } from '../tabDirectory/tabDirectory';
import { displayContractCode, productName } from '../utils/CommonFunction';
import StopLossTakeProfitComponent from './component/stopLossTakeProfit.component';
import ContractChangeComponent from './component/contractChange.component';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import TopBarSearch from '../../../assets/svg/TopbarSearch';
import TopBarBack from '../../../assets/svg/TopBarBack';
import TopBarMoney from '../../../assets/svg/TopBarMoney';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { MIOType } from '../../components/MIO';
import { OpenPositionContent, OpenPositionParam } from '../../redux/trading/type';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PressableComponent from '../utils/PressableComponent';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import EditPositionButtonComponent from './component/editPositionOrderButton.component';
import { ActionUtils } from '../utils/ActionUtils';
import { store } from '../../redux/store';
import { useHeaderHeight } from '@react-navigation/elements';

const EditPosition: React.FC<Props<'EditPosition'>> = ({ route, navigation }) => {
    console.debug('EditPosition')
    // const {symbolName, isBuy} = route.params;
    const [t, i18n] = useTranslation();
    const insets = useSafeAreaInsets()
    const headerHeight = useHeaderHeight();

    const { ticketId } = route.params;

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const [symbolName, SetSymbolName] = useState<string>('');

    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);
    const isEnableTradeConfirmationDialog = useSelector((state: State) => (state.state.setting.TradeSetting[cognitoTempState.username] && state.state.setting.TradeSetting[cognitoTempState.username].IsEnableTradeConfirmationDialog) ?? true);
    const contractSymbols = useSelector((state: State) => state.trading.symbols? state.trading.symbols[symbolName]! : undefined);
    // const contractPrice = useSelector((state: State) => state.trading.prices? state.trading.prices[symbolName]! : undefined);
    // const contractPrice = useSelector((state: State) => undefined);
    // const contractPrices = useSelector((state: State) => profitType === ProfitStopLossOrderType.PERCENTAGE && state.trading.prices);
    const position = useSelector((state: State) => state.trading.positions? state.trading.positions[ticketId]! : undefined);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const region = useSelector((state: State) => state.state.setting?.UserRegion)
    const setting = useSelector((state: State) => state.state.setting)
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const symbols = useSelector((state: State) => state.trading.symbols);
    // const accountLeverage = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Leverage);
    const enterDemoTab = useSelector((state: State) => state.account.enterDemoTab ?? TabDirectoryTab.QUOTE);
    // const currentTradeSL = useSelector((state: State) => state.trading.currentTradeSL);
    // const currentTradeTP = useSelector((state: State) => state.trading.currentTradeTP);
    
    const [isProgressOrder, setIsProgressOrder] = useState<boolean>(false);

    const [contractName, SetContractName] = useState<string>(' ');
    const [contractDisplayName, SetContractDisplayName] = useState<string>(symbolName);
    const [contractDP, SetContractDP] = useState<number>(2);
    const [contractStepPrice, SetContractStepPrice] = useState<number>(1);
    const [contractStepVolume, SetContractStepVolume] = useState<number>(1);  
    const [contractSize, SetContractSize] = useState<number>(10);
    const [contractVolumeDP, SetContractVolumeDP] = useState<number>(2);

    const [currentLot, SetCurrentLot] = useState<number>(1);
    const [positionObj, setPositionObj] = useState<OpenPositionContent>(undefined);

    const [changePercent, SetChangePercent] = useState<string>('+0.039 (+0.20%)');
    const [changePercentColor, SetChangePercentColor] = useState<string>(colors.Up);
    const [sellPrice, SetSellPrice] = useState<number>(0);
    const [buyPrice, SetBuyPrice] = useState<number>(0);
    const [isSelectBuy, SetIsSelectBuy] = useState<boolean>(false);
    const [amountCurrency, SetAmountCurrency] = useState<string>('USD');
    const [amount, SetAmount] = useState<number>(198650);

    const [profitType, SetProfitType] = useState<number>(ProfitStopLossOrderType.PRICE); //0:price, 1:pips, 2:percentage

    // const [stopLossPrice, SetStopLossPrice] = useState<number>(-1);
    // const [stopLossPriceRef, SetStopLossPriceRef] = useState<number>(-1); //Reference price for non-price type
    const [stopLossPriceUpdate, SetStopLossPriceUpdate] = useState<boolean>(false);
    const [stopLossHintSign, SetStopLossHintSign] = useState<string>('>');
    const [stopLossHintPrice, SetStopLossHintPrice] = useState<number>(0);
    const [stopLossPredictPL, SetStopLossPredictPL] = useState<number>(-0.050);
    // const [limitProfitPrice, SetLimitProfitPrice] = useState<number>(-1);
    // const [limitProfitPriceRef, SetLimitProfitPriceRef] = useState<number>(-1); //Reference price for non-price type
    const [limitProfitPriceUpdate, SetLimitProfitPriceUpdate] = useState<boolean>(false);
    const [limitProfitHintSign, SetLimitProfitHintSign] = useState<string>('>');
    const [limitProfitHintPrice, SetLimitProfitHintPrice] = useState<number>(0);
    const [limitProfitPredictPL, SetLimitProfitPredictPL] = useState<number>(0.050);

    const [isSLOrderShowError, SetIsSLOrderShowError] = useState<boolean>(false);
    const [isTPOrderShowError, SetIsTPOrderShowError] = useState<boolean>(false);

    const [isShowOrderDialog, SetIsShowOrderDialog] = useState<boolean>(false);

    const { onPressExitDemo } = ActionUtils();
    // const [isDemoState, SetIsDemoState]= useState<boolean>(isDemoAccount);

    const dispatch = useDispatch();
    const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetIsShowLoading, SetIsShowOrderConfirmDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLastOrderId, SetCurrentTradeSL, SetCurrentTradeTP } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

    const multiSwitchButton3 = [
        {
            title: t('trade.price'),
            function: () => { SetProfitType(ProfitStopLossOrderType.PRICE) }
        },
        {
            title: t('trade.point'),
            function: () => { SetProfitType(ProfitStopLossOrderType.PIPS) }
        },
        {
            title: t('trade.percentage'),
            function: () => { SetProfitType(ProfitStopLossOrderType.PERCENTAGE) }
        }
    ]

    const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();

    useEffect(() => {
        console.debug('positions', position)
        // SetProfitType(ProfitStopLossOrderType.PRICE)
        if (position) {
            SetSymbolName(position.Symbol)
            SetIsSelectBuy(position.Type === OrderDirectionType.BUY)
            SetCurrentLot(position.Volume)
            // SetStopLossPrice(position.SL === 0 ? -1 : position.SL)
            // SetLimitProfitPrice(position.TP === 0 ? -1 : position.TP)
            SetCurrentTradeSL({
                // ...currentTradeSL,
                StopLossPrice: position.SL === 0 ? undefined : position.SL,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                // ...currentTradeTP,
                TakeProfitPrice: position.TP === 0 ? -1 : position.TP,
                TakeProfitPriceRef: -1
            })
            SetStopLossPriceUpdate(!stopLossPriceUpdate)
            SetLimitProfitPriceUpdate(!limitProfitPriceUpdate)
            setPositionObj(position)
        }
    }, [contractDP, contractVolumeDP, position])

    useEffect(() => {
        console.debug('constractSymbols', contractSymbols)
        if (contractSymbols) {
            console.debug('constractSymbols', contractSymbols.SymbolName)
            SetContractDisplayName(contractSymbols.SymbolName)
            SetContractName(contractSymbols.SymbolName)
            SetContractDP(contractSymbols.Digits)
            SetContractSize(contractSymbols.ContractSize)
            SetContractStepVolume(contractSymbols.VolumeStep)
            SetContractStepPrice(contractSymbols.Point)

            var vDP = contractSymbols.VolumeStep.toString().split('.');
            var volumeDP = 0
            if (vDP.length > 1){
                volumeDP = vDP[1].length;
            }
            SetContractVolumeDP(volumeDP)

            SetStopLossPriceUpdate(!stopLossPriceUpdate)
            SetLimitProfitPriceUpdate(!limitProfitPriceUpdate)
        }

        return () => {

        }
    }, [contractSymbols, symbolName])

    // //checkSLTPPrice (Market Order)
    // useEffect(()=>{
    //     checkSLTPPrice(); 
    // }, [contractPrice, stopLossPrice, limitProfitPrice])

    // //Update st/tp reference price (Market Order)
    // useEffect(() => {
    //     if (contractPrice && contractSymbols) {
    //         //console.debug('useEffect 5')
    //         var point = contractSymbols.Point;
    //         var stopsLevel = contractSymbols.StopsLevel;

    //         if (profitType === ProfitStopLossOrderType.PRICE){
    //             var bid = contractPrice.Bid;
    //             var ask = contractPrice.Ask;
    //             if (isSelectBuy){ 
    //                 SetStopLossHintSign('≤')
    //                 SetLimitProfitHintSign('≥')
    //                 var slHintPrice = bid - (stopsLevel * point)
    //                 var tpHintPrice = bid + (stopsLevel * point)
    //                 SetStopLossHintPrice(slHintPrice)
    //                 SetLimitProfitHintPrice(tpHintPrice)
    //             } else { 
    //                 SetStopLossHintSign('≥')
    //                 SetLimitProfitHintSign('≤')
    //                 var slHintPrice = ask + (stopsLevel * point)
    //                 var tpHintPrice = ask - (stopsLevel * point)
    //                 SetStopLossHintPrice(slHintPrice)
    //                 SetLimitProfitHintPrice(tpHintPrice)
    //             }
    //         }
    //     }
    // }, [isSelectBuy, contractPrice, profitType])

    //Update symbol setting
    useEffect(() => {
        //console.debug('useEffect 8')
        if (contractSymbols) {
            SetContractName(contractSymbols.SymbolName)
            SetContractDP(contractSymbols.Digits)
            SetContractSize(contractSymbols.ContractSize)
            // SetContractMaxVolume(contractSymbols.VolumeMax)
            // SetContractMinVolume(contractSymbols.VolumeMin)
            SetContractStepVolume(contractSymbols.VolumeStep)
            var vDP = contractSymbols.VolumeStep.toString().split('.');
            var volumeDP = 0
            if (vDP.length > 1){
                volumeDP = vDP[1].length;
            }
            SetContractVolumeDP(volumeDP)
            SetContractStepPrice(contractSymbols.Point)
            // SetContractSpread(contractSymbols.Spread)
            // SetCurrentLotUpdate(!currentLotUpdate)
        }

        return () => {

        }
    }, [contractSymbols])

    // //Update contract price
    // useEffect(() => {
    //     //console.debug('useEffect 9')
    //     // console.debug('contractPrice', contractPrice)
    //     if (contractPrice) {
    //         SetSellPrice(contractPrice.Bid)
    //         SetBuyPrice(contractPrice.Ask)
    //         var priceDiff = contractPrice.Bid - contractPrice.Close;
    //         var sign = priceDiff > 0 ? '+' : '';
    //         var priceDiffPercent = priceDiff / contractPrice.Close * 100;
    //         var signPercent = priceDiffPercent > 0 ? '+' : '';
    //         SetChangePercent(sign + priceDiff.toFixed(contractDP) + ' (' + signPercent + priceDiffPercent.toFixed(priceChangePercentageDP) + '%)')
    //         SetChangePercentColor(priceDiff > 0 ? colors.Up : colors.Down)
    //     }
    // }, [contractPrice])

    // //Update StopLoss predict PL
    // useEffect(() => {
    //     if (contractPrice) {
    //         if (profitType === ProfitStopLossOrderType.PRICE) {
    //             // var bid = contractPrice.Bid;
    //             // var ask = contractPrice.Ask;
    //             // if (isSelectBuy){ 
    //             //     SetStopLossPredictPL((stopLossPrice - position!.OpenPrice!) * contractSize * currentLot)
    //             // } else { 
    //             //     SetStopLossPredictPL((position!.OpenPrice! - stopLossPrice ) * contractSize * currentLot)
    //             // }
    //         } else if (profitType === ProfitStopLossOrderType.PIPS) {
    //             var refPrice = 0;
    //             var bid = contractPrice.Bid;
    //             var ask = contractPrice.Ask;
    //             var spread = contractSymbols!.Spread;
    //             var point = contractSymbols!.Point;
    //             if (currentTradeSL.StopLossPrice) {
    //                 if (isSelectBuy) {
    //                     refPrice = bid - currentTradeSL.StopLossPrice * point;
    //                 } else {
    //                     refPrice = ask + currentTradeSL.StopLossPrice * point;
    //                 }
    //                 // SetStopLossPriceRef(refPrice);
    //                 SetCurrentTradeSL({
    //                     ...currentTradeSL,
    //                     StopLossPriceRef: refPrice
    //                 })
    //             }else {
    //                 // SetStopLossPriceRef(-1);
    //                 SetCurrentTradeSL({
    //                     ...currentTradeSL,
    //                     StopLossPriceRef: -1
    //                 })
    //             }
                
    //             SetStopLossPredictPL((-currentTradeSL.StopLossPrice - spread) * point * contractSize * currentLot)
    //         } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //             var refPrice = 0;
    //             var bid = contractPrice.Bid;
    //             var ask = contractPrice.Ask;
    //             // var requiredMargin = position!.OpenPrice! * contractSymbols!.ContractSize * currentLot * initialMargin/100;
    //             var requiredMargin = getEstMargin(contractSymbols, position.Volume, position!.OpenPrice!, accountLeverage, 1);
    //             var pl = requiredMargin * currentTradeSL.StopLossPrice/100 * -1 ;
    //             var spread = contractSymbols!.Spread;
    //             var point = contractSymbols!.Point;
    //             if (currentTradeSL.StopLossPrice) {
    //                 if (isSelectBuy) {
                        
    //                     refPrice = (position!.OpenPrice! - (spread * point) + (pl / (contractSize * currentLot)))
    //                 } else {
    //                     refPrice = (position!.OpenPrice! + (spread * point) - (pl / (contractSize * currentLot)))
    //                 }
    //                 // SetStopLossPriceRef(refPrice);
    //                 SetCurrentTradeSL({
    //                     ...currentTradeSL,
    //                     StopLossPriceRef: refPrice
    //                 })
    //             }
    //             SetStopLossPredictPL(pl)
    //         }
    //     }
    // }, [currentTradeSL.StopLossPrice, contractPrice])

    // //Update TakeProfit predict PL
    // useEffect(() => {
    //     if (contractPrice) {
    //         if (profitType === ProfitStopLossOrderType.PRICE) {
    //             // var bid = contractPrice.Bid;
    //             // var ask = contractPrice.Ask;
    //             // if (isSelectBuy){ 
    //             //     SetLimitProfitPredictPL((limitProfitPrice - ask) * contractSize * currentLot)
    //             // } else { 
    //             //     SetLimitProfitPredictPL((bid - limitProfitPrice) * contractSize * currentLot)
    //             // }
    //         }
    //         else if (profitType === ProfitStopLossOrderType.PIPS) {
    //             var refPrice = 0;
    //             var bid = contractPrice.Bid;
    //             var ask = contractPrice.Ask;
    //             var spread = contractSymbols!.Spread;
    //             var point = contractSymbols!.Point;
    //             if (currentTradeTP.TakeProfitPrice > 0) {
    //                 if (isSelectBuy) {
    //                     refPrice = bid + currentTradeTP.TakeProfitPrice * point;
    //                 } else {
    //                     refPrice = ask - currentTradeTP.TakeProfitPrice * point;
    //                 }
    //                 // SetLimitProfitPriceRef(refPrice);
    //                 SetCurrentTradeTP({
    //                     ...currentTradeTP,
    //                     TakeProfitPriceRef: refPrice
    //                 })
    //             } else {
    //                 // SetLimitProfitPriceRef(-1);
    //                 SetCurrentTradeTP({
    //                     ...currentTradeTP,
    //                     TakeProfitPriceRef: -1
    //                 })
    //             }
                
    //             SetLimitProfitPredictPL((currentTradeTP.TakeProfitPrice - spread) * point * contractSize * currentLot)
    //         } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
    //             var refPrice = 0;
    //             var bid = contractPrice.Bid;
    //             var ask = contractPrice.Ask;
    //             // var requiredMargin = position!.OpenPrice! * contractSymbols!.ContractSize * currentLot * initialMargin/100;
    //             var convertPrice = getConvertPrice(position.Type === 0, contractSymbols, contractPrices);
    //             var requiredMargin = getEstMargin(contractSymbols, position.Volume, position!.OpenPrice!, accountLeverage, convertPrice);
    //             var pl = requiredMargin * currentTradeTP.TakeProfitPrice/100
    //             var spread = contractSymbols!.Spread;
    //             var point = contractSymbols!.Point;
    //             if (currentTradeTP.TakeProfitPrice > 0) {
    //                 if (isSelectBuy) {
    //                     refPrice = (position!.OpenPrice! - (spread * point) + (pl / (contractSize * currentLot)))
    //                 } else {
    //                     refPrice = (position!.OpenPrice! + (spread * point) - (pl / (contractSize * currentLot)))
    //                 }
    //                 // SetLimitProfitPriceRef(refPrice);
    //                 SetCurrentTradeTP({
    //                     ...currentTradeTP,
    //                     TakeProfitPriceRef: refPrice
    //                 })
    //             }
    //             SetLimitProfitPredictPL(pl)
    //         }
    //     }
    // }, [currentTradeTP.TakeProfitPrice, contractPrice])

    //Reset SL TP price to N/A
    useEffect(()=>{
        if (!position) {
            SetCurrentTradeSL({
                // ...currentTradeSL,
                StopLossPrice: undefined,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                // ...currentTradeTP,
                TakeProfitPrice: -1,
                TakeProfitPriceRef: -1
            })
            return;
        }

        //console.debug('useEffect 4')
        // SetStopLossPrice(-1)
        // SetLimitProfitPrice(-1)
        SetIsSLOrderShowError(false)
        SetIsTPOrderShowError(false)
        if (profitType === ProfitStopLossOrderType.PRICE) {
            // SetStopLossPrice(position.SL === 0 ? -1 : position.SL)
            // SetLimitProfitPrice(position.TP === 0 ? -1 : position.TP)
            SetCurrentTradeSL({
                // ...currentTradeSL,
                StopLossPrice: position.SL === 0 ? undefined : position.SL,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                // ...currentTradeTP,
                TakeProfitPrice: position.TP === 0 ? -1 : position.TP,
                TakeProfitPriceRef: -1
            })
        }
        else if (profitType === ProfitStopLossOrderType.PIPS) {
            SetStopLossHintSign('≥')
            SetLimitProfitHintSign('≥')
            var slHintPrice = contractSymbols!.StopsLevel
            var tpHintPrice = contractSymbols!.StopsLevel
            SetStopLossHintPrice(slHintPrice)
            SetLimitProfitHintPrice(tpHintPrice)
            SetStopLossPredictPL(0)
            SetLimitProfitPredictPL(0)
            // SetStopLossPriceRef(-1)
            // SetLimitProfitPriceRef(-1)
            SetCurrentTradeSL({
                // ...currentTradeSL,
                StopLossPrice: undefined,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                // ...currentTradeTP,
                TakeProfitPrice: -1,
                TakeProfitPriceRef: -1
            })
        } else if (profitType === ProfitStopLossOrderType.PERCENTAGE) {
            SetStopLossHintSign('≥')
            SetLimitProfitHintSign('≥')
            var slHintPrice = percentageOrderSpread
            var tpHintPrice = percentageOrderSpread
            SetStopLossHintPrice(slHintPrice)
            SetLimitProfitHintPrice(tpHintPrice)
            SetStopLossPredictPL(0)
            SetLimitProfitPredictPL(0)
            // SetStopLossPriceRef(-1)
            // SetLimitProfitPriceRef(-1)
            SetCurrentTradeSL({
                // ...currentTradeSL,
                StopLossPrice: undefined,
                StopLossPriceRef: -1
            })
            SetCurrentTradeTP({
                // ...currentTradeTP,
                TakeProfitPrice: -1,
                TakeProfitPriceRef: -1
            })
        }
    }, [profitType, position])

    // const checkSLTPPrice = () => {
    //     if (isSelectBuy){ 
    //         if (stopLossPrice > 0) {
    //             var isError = profitType === ProfitStopLossOrderType.PRICE ? stopLossPrice > stopLossHintPrice : stopLossPrice < stopLossHintPrice;
    //             if (isError) {
    //                 SetIsSLOrderShowError(true)
    //             } else {
    //                 SetIsSLOrderShowError(false)
    //             }
    //         } else {
    //             SetIsSLOrderShowError(false)
    //         }
    //         if (limitProfitPrice > 0) {
    //             var isError = limitProfitPrice < limitProfitHintPrice;
    //             if (isError) {
    //                 SetIsTPOrderShowError(true)
    //             } else {
    //                 SetIsTPOrderShowError(false)
    //             }
    //         } else {
    //             SetIsTPOrderShowError(false)
    //         }
    //     } else { 
    //         if (stopLossPrice > 0) {
    //             var isError = stopLossPrice < stopLossHintPrice;
    //             if (isError) {
    //                 SetIsSLOrderShowError(true)
    //             } else {
    //                 SetIsSLOrderShowError(false)
    //             }
    //         } else {
    //             SetIsSLOrderShowError(false)
    //         }
    //         if (limitProfitPrice > 0) {
    //             var isError = profitType === ProfitStopLossOrderType.PRICE ? limitProfitPrice > limitProfitHintPrice : limitProfitPrice < limitProfitHintPrice;
    //             if (isError) {
    //                 SetIsTPOrderShowError(true)
    //             } else {
    //                 SetIsTPOrderShowError(false)
    //             }
    //         } else {
    //             SetIsTPOrderShowError(false)
    //         }
    //     }
    // }

    const onCancelStopLoss = () => {
        // SetStopLossPrice(-1); 
        SetStopLossPriceUpdate(!stopLossPriceUpdate); 
        SetIsSLOrderShowError(false) 
        // SetStopLossPriceRef(-1)

        SetCurrentTradeSL({
            // ...currentTradeSL,
            StopLossPrice: undefined,
            StopLossPriceRef: -1
        })
    }

    const onCancelLimitProfit = () => { 
        // SetLimitProfitPrice(-1); 
        SetLimitProfitPriceUpdate(!limitProfitPriceUpdate); 
        SetIsTPOrderShowError(false) 
        // SetLimitProfitPriceRef(-1)

        SetCurrentTradeTP({
            // ...currentTradeTP,
            TakeProfitPrice: -1,
            TakeProfitPriceRef: -1
        })
    }

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    useLayoutEffect(() => {
        var rightComponents = [
            <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        if (isDemoAccount) {
            rightComponents = [
                <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.MainFont} />} />,
                <HeaderButton onPress={() => onPressExitDemo(exitDemoSuccessRoute)} component={<ExitDemoButton/>} />
            ]
        } else if (stateLoginLevel === 3) {
            rightComponents = [
                <HeaderButton onPress={onPressDeposit} iconComponent={<TopBarMoney color={colors.White} />} />
            ]
        }

        navigation.setOptions({
            header: props => <ExpandableHeader
                isDemo={isDemoAccount}
                rightComponents={rightComponents}
                leftComponents={<HeaderButton onPress={onPressBackBtn} iconComponent={<TopBarBack color={isDemoAccount ? colors.MainFont : colors.White} />} />}
            />
        });
    }, [navigation]);

    const categories = useMemo(() => {
        if (!symbols) return [];

        var categories: string[] = Object.values(symbols).filter(symbol => symbol.SymbolName.indexOf(".") > 0).flatMap((symbol) => {
            if (symbol.SymbolName.indexOf(".") > 0)
                return symbol.DisplayCategory
        });

        return [...new Set(categories)];
    }, [symbols])


    const onPressDeposit = async () => {
        // navigation.push('MIO', { type: MIOType.Deposit })
        navigation.push('TabDirectoryWebView', {page: TabDirectoryWebViewPage.MIO, focusTab: TabDirectoryWebViewTab.QUOTE, param: { type: MIOType.Deposit }})
    }

    const onPressSearch = () => {
        let searchCategories = categories.flatMap((category) => {return {id: category, title: category}})
        navigation.navigate('SearchProductView', {categories: searchCategories})
    }

    const onPressBackBtn = () => { navigation.goBack() }

    const orderSuccessCallback = (contractCode: string) => {
        let params = {contractCode: contractCode, defaultTab: 1, isEnterByOrder: true}
        navigation.popToTop()
        navigation.push('ContractDetailView', params)
    }

    const [keyboardHeight, setKeyboardHeight] = useState(0);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            (event) => {
                setKeyboardHeight(event.endCoordinates.height);
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardHeight(0);
            }
        );
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    return (
        <View style={[globalStyles.container, styles.container, {backgroundColor: colors.MainBg}]}>
            <KeyboardAvoidingView
                style={{ flex: 1}}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={headerHeight}>
                <ScrollView>
                    <View style={{backgroundColor: colors.WrapperBg}}>
                        <View style={[styles.innerContainer, {marginHorizontal: LAYOUT_PADDING_HORIZONTAL}]}>
                            <TextLabel label={t('trade.editPosition')} labelStyle={[globalStyles.H4]}/>
                            <View style={{ padding: 5 }} />
                            <Text style={[globalStyles.Note, {color: colors.MainFont}]}>{productName(displayContractCode(contractDisplayName))}</Text>
                            <View style={[styles.row]}>
                                {/* <Text style={[globalStyles.Form_Title, globalStyles.H3, {color: colors.Brand2}]}>{contractName}</Text> */}
                                <TextLabel label={displayContractCode(contractName)} labelStyle={[globalStyles.H3, {color: colors.Brand2}]}/>
                                <View style={{ padding: 10 }} />
                                {/* <View style={{ justifyContent: 'center', flex: 1}}>
                                    <TextLabel label={changePercent} labelStyle={[globalStyles.Body_Text_B, {color: changePercentColor}]}/>
                                </View> */}
                                <ContractChangeComponent symbol={contractName}/>
                            </View>
                            <View style={{ padding: 5 }} />
                            <View style={[styles.row]}>
                                <BuySellTradeButton title={t('trade.sell')} contract={contractName} isBuy={false} isSelected={!isSelectBuy}/>
                                <View style={{ padding: 10 }} />
                                <BuySellTradeButton title={t('trade.buy')} contract={contractName} isBuy={true} isSelected={isSelectBuy}/>
                            </View>
                            <View style={{ padding: 10 }} />
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.direction')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B, isSelectBuy ? {color: colors.Green} : {color: colors.Red}]}>{isSelectBuy ? t('trade.buy') : t('trade.sell')}</Text>
                            </View>
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.volume')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{currentLot.toFixed(contractVolumeDP)} {t('confirmTrade.lots')}</Text>
                            </View>
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.openPrice')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{(positionObj ? positionObj.OpenPrice : 0).toFixed(contractDP)}</Text>
                            </View>
                            <View style={styles.contentItem}>
                                <Text style={[globalStyles.Body_Text]}>{t('trade.transactionId')}</Text>
                                <DashedLineView style={{paddingHorizontal: 10}} />
                                <Text style={[globalStyles.Body_Text_B]}>{ticketId}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={[globalStyles.container, styles.innerContainer2]}>
                        <View style={[ {marginHorizontal: LAYOUT_PADDING_HORIZONTAL, paddingTop: 30}]}>
                            <TextLabel label={t('trade.tpslSetting')} labelStyle={[globalStyles.H4, {color: colors.Brand2}]}/>
                            {/* <View>
                                <View style={{ padding: 10 }} />
                                <MultiSwitchButton buttons={multiSwitchButton3} />
                                <View style={{ padding: 10 }} />
                                <InputArithmomether
                                    label={t('trade.stopLoss') + ' (' + stopLossHintSign + stopLossHintPrice.toFixed(profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0) + (profitType === ProfitStopLossOrderType.PERCENTAGE ? '%' : '') + ')'}
                                    isRequired={false}
                                    note={t('trade.predictPL') + ' ' + (stopLossPrice > 0 ? toCurrencyDisplay(stopLossPredictPL) : '0.00')}
                                    value={stopLossPrice}
                                    valueUpdate={stopLossPriceUpdate}
                                    valuePerStep={profitType === ProfitStopLossOrderType.PRICE ? contractStepPrice : 1}
                                    onChangeValue={(value: number) => {
                                        SetStopLossPrice(value)
                                    }}
                                    onCancel={onCancelStopLoss}
                                    dp={profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0} 
                                    referencePrice={stopLossHintPrice}
                                    isError={isSLOrderShowError}
                                    profitType={profitType}
                                    />
                                {
                                    isSLOrderShowError ? 
                                    <View style={{alignSelf: 'flex-end'}}>
                                        <ErrorMessage message={t('trade.error.invalidSL')} />
                                    </View>
                                    : <View style={{ padding: 10 }} />
                                }
                                <InputArithmomether
                                    label={t('trade.limitProfit') + ' (' + limitProfitHintSign + limitProfitHintPrice.toFixed(profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0) + (profitType === ProfitStopLossOrderType.PERCENTAGE ? '%' : '') + ')'}
                                    isRequired={false}
                                    note={t('trade.predictPL') + ' ' + (limitProfitPrice > 0 ? toCurrencyDisplay(limitProfitPredictPL) : '0.00')}
                                    value={limitProfitPrice}
                                    valueUpdate={limitProfitPriceUpdate}
                                    valuePerStep={profitType === ProfitStopLossOrderType.PRICE ? contractStepPrice : 1}
                                    onChangeValue={(value: number) => {
                                        SetLimitProfitPrice(value)
                                    }}
                                    onCancel={onCancelLimitProfit}
                                    dp={profitType === ProfitStopLossOrderType.PRICE ? contractDP : 0}
                                    referencePrice={limitProfitHintPrice}
                                    isError={isTPOrderShowError}
                                    profitType={profitType}
                                    />
                                {
                                    isTPOrderShowError ? 
                                    <View style={{alignSelf: 'flex-end'}}>
                                        <ErrorMessage message={t('trade.error.invalidTP')} />
                                    </View>
                                    : <View style={{ padding: 10 }} />
                                }
                            </View> */}
                            <StopLossTakeProfitComponent
                                contract = {symbolName}
                                isPending = {false}
                                pendingOrderType = {0}
                                pendingOrderPrice = {0}
                                currentLot = {currentLot}
                                isSelectBuy = {isSelectBuy}
                                isEditPosition = {true}
                                isEditOrder = {false}
                                profitType = {profitType}
                                setProfitType = {SetProfitType}

                                // stopLossPrice = {stopLossPrice}
                                // setStopLossPrice = {SetStopLossPrice}
                                onCancelStopLoss = {onCancelStopLoss}
                                // stopLossPriceRef = {stopLossPriceRef}
                                // setStopLossPriceRef = {SetStopLossPriceRef}
                                isSLOrderShowError = {isSLOrderShowError}
                                setIsSLOrderShowError = {SetIsSLOrderShowError}

                                // limitProfitPrice = {limitProfitPrice}
                                // setLimitProfitPrice = {SetLimitProfitPrice}
                                onCancelLimitProfit = {onCancelLimitProfit}
                                // limitProfitPriceRef = {limitProfitPriceRef}
                                // setLimitProfitPriceRef = {SetLimitProfitPriceRef}
                                isTPOrderShowError = {isTPOrderShowError}
                                setIsTPOrderShowError = {SetIsTPOrderShowError}
                                transactionId = {ticketId}
                            />
                        </View>
                    </View>
                    <View style={{ marginBottom: keyboardHeight / 2 }}></View>
                </ScrollView>
                
            </KeyboardAvoidingView>
        
            <View style={{ backgroundColor: colors.MainBg, paddingBottom: insets.bottom }}>
                <View style={[ {marginHorizontal: LAYOUT_PADDING_HORIZONTAL, alignItems: 'center', marginTop: 5}]}>
                    {/* <TextLabel label={t('trade.editPositionWarning').replace('%1', contractSymbols ? contractSymbols.StopsLevel.toString() : '0')} labelStyle={[globalStyles.Note]}/> */}
                    {
                        isEnableTradeConfirmationDialog ? 
                            null 
                        : 
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[globalStyles.Note, {textAlign: 'center'}]}>{t('trade.confirmationWarning')} <Text onPress={() => { navigation.navigate('MeSetting') }} style={[globalStyles.Note, { color: colors.Brand3, alignContent: 'center', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }]}>{t('trade.confirmationChange')}</Text></Text>
                                {/* <TextLabel label={t('trade.confirmationWarning')} labelStyle={[globalStyles.Note]} />
                                <View style={{ padding: 2 }} />
                                <PressableComponent onPress={() => { navigation.navigate('MeSetting') }}><TextLabel label={t('trade.confirmationChange')} labelStyle={[globalStyles.Note, { color: colors.Brand3 }]} /></PressableComponent> */}
                            </View>
                        }
                        
                        {/* <ActionButton
                            title={t('trade.editPosition')}
                            isEnable={(!isProgressOrder || isEnableTradeConfirmationDialog) && !isSLOrderShowError && !isTPOrderShowError}
                            onPress={async()=>{
                                if (!isEnableTradeConfirmationDialog) {
                                    setIsProgressOrder(true)
                                    SetIsShowLoading(true);
                                    var result = await startEditPosition(ticketId!, currentTradeTP.TakeProfitPrice > 0 ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeTP.TakeProfitPrice : currentTradeTP.TakeProfitPriceRef) : 0, currentTradeSL.StopLossPrice ? (profitType === ProfitStopLossOrderType.PRICE ? currentTradeSL.StopLossPrice : currentTradeSL.StopLossPriceRef) : 0)
                                    
                                    if (result) {
                                        SetLastOrderId(ticketId.toString());
                                        await orderSuccessCallback(symbolName);
                                    }

                                    SetIsShowLoading(false);
                                    setIsProgressOrder(false)
                                } else {
                                    // SetIsShowOrderDialog(true)
                                    SetIsShowOrderConfirmDialog({
                                        isShowDialog:true,
                                        type:OrderActionType.EDIT_POSITION,
                                        contractCode:symbolName,
                                        isSelectBuy:isSelectBuy,
                                        volume:currentLot,
                                        price:positionObj.OpenPrice,
                                        isPending:false,
                                        isProfit:true,
                                        stopLossPrice:0,
                                        limitProfitPrice:0,
                                        transactionId:ticketId,
                                        callback:orderSuccessCallback,
                                        profitType:{type: profitType}//{type: profitType, slValue: currentTradeSL.StopLossPrice, tpValue: currentTradeTP.TakeProfitPrice}
                                    })
                                }
                            }}
                        />   */}
                        
                        <EditPositionButtonComponent
                            isEnable={(!isProgressOrder || isEnableTradeConfirmationDialog) && !isSLOrderShowError && !isTPOrderShowError}
                            isEnableTradeConfirmationDialog={isEnableTradeConfirmationDialog}
                            setIsProgressOrder={setIsProgressOrder}
                            orderSuccessCallback={orderSuccessCallback}
                            order={{
                                ticketId: ticketId,
                                symbolName: symbolName,
                                isSelectBuy: isSelectBuy,
                                currentLot: currentLot,
                                openPrice: positionObj && positionObj.OpenPrice,
                                profitType: profitType
                            }}
                        />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        paddingTop: 15
    },
    innerContainer2: {
        flex: 1,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    row: {
        flexDirection: 'row',
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    line: {
        borderWidth: 0.5,
        borderRadius: 1,
        marginTop: 10
    }
});

export default EditPosition;