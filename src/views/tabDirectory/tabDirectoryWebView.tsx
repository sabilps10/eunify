import React, { Fragment, useCallback, useLayoutEffect, useRef, useState } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Text, StyleSheet, useColorScheme, Image, BackHandler, Alert } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { Props } from '../../routes/route';

import { makeGlobalStyles } from '../../styles/styles'
import { useTranslation } from 'react-i18next';

import { useFocusEffect } from '@react-navigation/native';
import { useEffect } from 'react';
import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { bindActionCreators } from 'redux';
import LoadingDialog from '../../components/dialogs/loadingDialogs';
import AddDeviceDialog from '../../components/dialogs/addedDeviceDialogs';
import { dialogDisplaySecond, reward_login_loyaltyportal } from '../../config/constants';
import { PermissionAccessType } from '../permission/permissionAccess';
import { AccountUtils } from '../../utils/accountUtils';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import ToastDialog from '../../components/dialogs/toastDialogs';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import BottomMenuHome from '../../../assets/svg/BottomMenuHome';
import BottomMenuPortfolio from '../../../assets/svg/BottomMenuPortfolio';
import BottomMenuMe from '../../../assets/svg/BottomMenuMe';
import BottomMenuWatchlist from '../../../assets/svg/BottomMenuWatchlist';
import { Auth } from 'aws-amplify';
import { logout } from '../../utils/api/userApi';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { PortfolioTabType } from '../../redux/state/types';
import ExpandableHeader from '../../components/header/expandableHeader.component';
import WebView from 'react-native-webview';
import MIO, { MIOType } from '../../components/MIO';
import WatchlistView from '../watchlist/watchlist';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

export enum TabDirectoryWebViewPage {
    'MIO' = 'MIO',
    'RewardLoginLoyaltyPortal' = 'RewardLoginLoyaltyPortal'
}

export enum TabDirectoryWebViewTab {
    'QUOTE' = 'Tab_WatchlistView2',
    'PORTFOLIO' = 'Tab_Portfolio2',
    'ME' = 'Tab_Me2',
}

const TabDirectoryWebView: React.FC<Props<'TabDirectoryWebView'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const { page, focusTab, param } = route.params;

    const inset = useSafeAreaInsets()

    const dispatch = useDispatch();
    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    // const onTouch = useSelector((state: State) => state.state.onTouch)

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const Tab = createBottomTabNavigator();

    const { SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);    

    useLayoutEffect(() => {
        var leftComponents = [
            <HeaderButton onPress={onPressBack} iconComponent={<TopBarBack color={colors.White} />} />
        ]
        var rightComponents = [
            // <HeaderButton onPress={onPressSearch} iconComponent={<TopBarSearch color={colors.White} />} />
        ]

        navigation.setOptions({
            header: () => <ExpandableHeader
                isDemo={isDemoAccount}
                leftComponents={leftComponents}
                rightComponents={rightComponents}
            />,
        });
    }, [navigation, isDemoAccount])

    useFocusEffect(
        useCallback(() => {
            console.debug("Tab Directory");

            const backAction = async() => {
                onPressBack()
                return true;
            };

            const backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );

            return () => {
                backHandler.remove();
            }
        }, [])
    );

    const onPressBack = () => {
        navigation.goBack()
    }

    const WebLoyaltyPortal = ({ route, navigation }) => <WebView source={{uri: reward_login_loyaltyportal}} />

    const Component = page === TabDirectoryWebViewPage.MIO ? MIO : page === TabDirectoryWebViewPage.RewardLoginLoyaltyPortal ? WebLoyaltyPortal : WatchlistView;

    const routeNameForTabDummyContainer = page === TabDirectoryWebViewPage.MIO ? 'Tab_DummyContainerMIO' : page === TabDirectoryWebViewPage.RewardLoginLoyaltyPortal ? 'Tab_DummyContainerRewardLoginLoyaltyPortal' : null

    return (
        <Fragment>
            <Tab.Navigator
                screenOptions={{
                    headerShown: false ?? true,
                    tabBarStyle: {backgroundColor: colors.MainMenuBg, height: 55 + inset.bottom},
                    tabBarHideOnKeyboard: true
                }}
                initialRouteName = {routeNameForTabDummyContainer}
                >
                <Tab.Screen
                    name={focusTab == TabDirectoryWebViewTab.QUOTE ? routeNameForTabDummyContainer : "Tab_WatchlistView2"}
                    initialParams={param}
                    component={Component}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuWatchlist color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.watchlist')}</Text>
                     }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                            navigation.navigate("TabDirectory", {
                                screen: "Tab_WatchlistView"
                            })
                        },
                    }}/>
                <Tab.Screen 
                    name={focusTab == TabDirectoryWebViewTab.PORTFOLIO ? routeNameForTabDummyContainer : "Tab_Portfolio2"}
                    initialParams={param}
                    component={Component}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuPortfolio color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.portfolio')}</Text>
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                            navigation.navigate("TabDirectory", {
                                screen: "Tab_Portfolio",
                                params: {defaultTab: PortfolioTabType.Position}
                            })
                        },
                    }}/>
                <Tab.Screen
                    name={focusTab == TabDirectoryWebViewTab.ME ? routeNameForTabDummyContainer : "Tab_Me2"}
                    initialParams={param}
                    component={Component}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuMe color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.me')}</Text>
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                            navigation.navigate("TabDirectory", {
                                screen: "Tab_Me"
                            })
                        },
                    }}/>
            </Tab.Navigator>
        </Fragment>
    )
}

const styles = StyleSheet.create({
    tabIconWrapper: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default TabDirectoryWebView