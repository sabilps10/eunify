import React, { Fragment, useCallback, useLayoutEffect, useRef, useState } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Text, StyleSheet, useColorScheme, Image, BackHandler, Alert } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { Props } from '../../routes/route';

import WatchlistView from '../watchlist/watchlist';
import PortfolioView from '../portfolio/portfolio';
import MeView from '../me/me';

import { makeGlobalStyles } from '../../styles/styles'
import { useTranslation } from 'react-i18next';

import { useFocusEffect } from '@react-navigation/native';
import { useEffect } from 'react';
import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { bindActionCreators } from 'redux';
import LoadingDialog from '../../components/dialogs/loadingDialogs';
import AddDeviceDialog from '../../components/dialogs/addedDeviceDialogs';
import { dialogDisplaySecond } from '../../config/constants';
import { PermissionAccessType } from '../permission/permissionAccess';
import { AccountUtils } from '../../utils/accountUtils';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import ToastDialog from '../../components/dialogs/toastDialogs';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import BottomMenuHome from '../../../assets/svg/BottomMenuHome';
import BottomMenuPortfolio from '../../../assets/svg/BottomMenuPortfolio';
import BottomMenuMe from '../../../assets/svg/BottomMenuMe';
import BottomMenuWatchlist from '../../../assets/svg/BottomMenuWatchlist';
import { Auth } from 'aws-amplify';
import { logout } from '../../utils/api/userApi';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { PortfolioTabType } from '../../redux/state/types';
import { ActionUtils } from '../utils/ActionUtils';
import { store } from '../../redux/store';
import DashedLineView from '../../components/dashedLineView.component';
import { displayContractCode, productName } from '../utils/CommonFunction';
import { isEmpty } from '../../utils/stringUtils';

export enum TabDirectoryTab {
    'HOME' = 'Tab_Home',
    'QUOTE' = 'Tab_WatchlistView',
    'COMM' = 'Tab_Community',
    'PORTFOLIO' = 'Tab_Portfolio',
    'ME' = 'Tab_Me',
}

export enum TransactionType {
    TT_ORDER_MK_OPEN = 66,
    TT_ORDER_IE_OPEN = 64,
    TT_ORDER_PENDING_OPEN = 67,
    TT_ORDER_MK_CLOSE = 70,
    TT_ORDER_IE_CLOSE = 68,
    TT_ORDER_MODIFY = 71,
    TT_ORDER_DELETE = 72
}

const TabDirectory: React.FC<Props<'TabDirectory'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const inset = useSafeAreaInsets()

    const dispatch = useDispatch();
    const isDemoAccount = useSelector((state: State) => state.account.isDemoAccount);
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const demoTradingAccount = useSelector((state: State) => state.account.demoTradingAccount);
    const isSwitchingConnection = useSelector((state: State) => state.connection.isSwitchingConnection);
    const rejectMessage = useSelector((state: State) => state.trading.rejectMessage);

    const region = useSelector((state: State) => state.state.setting?.UserRegion)
    const isFirstTime = useSelector((state: State) => state.state.setting?.IsFirstTime)
  
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const Tab = createBottomTabNavigator();

    const { SetIsShowLoading, SetIsShowMessageDialog, SetOnTouch } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);
    const { signOut } = AccountUtils();
    const { onPressExitDemo } = ActionUtils();

    useEffect(() => {
        if (isFirstTime && region === undefined) {
            navigation.navigate('Welcome')
        }
    }, [region, isFirstTime])

    useLayoutEffect(() => {
        navigation.setOptions({
            headerShown: false
        });
    }, [navigation]);

    const exitApp = async () => {
        SetIsShowLoading(true);
        try {
            const session = await Auth.currentSession();
            if (session !== null) {
                var payload: { [key: string]: any; } = session.getIdToken().payload;
                var cognitoUserName = payload['cognito:username'];
                await logout(cognitoUserName)
            }
        } catch (e){}
        finally{await signOut();}
        SetIsShowLoading(false);
        BackHandler.exitApp()
    }

    const exitDemoSuccessRoute = () => {
        const state: State = store.getState();
        const enterDemoTab = state.account.enterDemoTab ?? TabDirectoryTab.QUOTE
        navigation.navigate("TabDirectory", {screen: enterDemoTab})
    }

    useFocusEffect(
        useCallback(() => {
            console.debug("Tab Directory");

            const backAction = async() => {
                if (isDemoAccount) {
                    await onPressExitDemo(exitDemoSuccessRoute)
                } else {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.exitApp'), message: '', callback: exitApp, isShowCancel: true})
                }
                return true;
            };

            const backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                backAction
            );

            return () => {
                backHandler.remove();
            }
        }, [isDemoAccount])
    );

    const openRejectMessageDialog = (message) => {
        const state: State = store.getState();
        var nVolumeDP = 0
        const vDP = state.trading.symbols[message.Symbol] && state.trading.symbols[message.Symbol].VolumeStep.toString().split('.');
        if (vDP.length > 1){
            nVolumeDP = vDP[1].length;
        }
        let product = productName(displayContractCode(message.Symbol))
        let messageTitle = "";
        if (message.TransactionType === TransactionType.TT_ORDER_PENDING_OPEN)
            messageTitle = t('trade.error.rejectedPendingOrder')
        else if (message.TransactionType === TransactionType.TT_ORDER_MODIFY && (message.Type === 0 || message.Type === 1))
            messageTitle = t('trade.error.rejectedEditPosition')
        else if (message.TransactionType === TransactionType.TT_ORDER_MODIFY)
            messageTitle = t('trade.error.rejectedEditPendingOrder')
        else if (message.TransactionType === TransactionType.TT_ORDER_DELETE)
            messageTitle = t('trade.error.rejectedDeletePendingOrder')            
        else
            messageTitle = t('trade.error.rejectedOrder')

        console.log("messageTitle: " + messageTitle)
        const componentView = (
            <View>
                <View style={styles.contentItem}>
                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                    <DashedLineView style={{paddingHorizontal: 10}} />
                    <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(message.Symbol)}</Text>
                </View>
                {
                    isEmpty(product) ? null :
                    <View style={styles.contentItem}>
                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.product')}</Text>
                        <DashedLineView style={{paddingHorizontal: 10}} />
                        <Text style={[globalStyles.Body_Text_B]}>{product}</Text>
                    </View>
                }

                <View style={styles.contentItem}>
                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                    <DashedLineView style={{paddingHorizontal: 10}} />
                    <Text style={[globalStyles.Body_Text_B, message.Type === 0 ? {color: colors.Green} : {color: colors.Red}]}>{(message.Type === 0 || message.Type === 2 || message.Type === 4)? t('trade.buy') : t('trade.sell')}</Text>
                </View>
                <View style={styles.contentItem}>
                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.transactionId')}</Text>
                    <DashedLineView style={{paddingHorizontal: 10}} />
                    <Text style={[globalStyles.Body_Text_B]}>{message.Ticket === 0 ? "-" : message.Ticket}</Text>
                </View>
                {
                    message.TransactionType !== TransactionType.TT_ORDER_MODIFY && message.TransactionType !== TransactionType.TT_ORDER_DELETE ?
                    <View style={styles.contentItem}>
                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                        <DashedLineView style={{paddingHorizontal: 10}} />
                        <Text style={[globalStyles.Body_Text_B]}>{message.Volume.toFixed(nVolumeDP)} {t('confirmTrade.lots')}</Text>
                    </View>:
                    <></>
                }
                
            </View>
        )
        SetIsShowMessageDialog({ isShowDialog: true, title: messageTitle, titleStyle: { color: colors.Red }, message: '', messageStyle: { color: colors.Red }, isShowBtn: false, isShowCancel: true, detailComponent: componentView })
    }

    useEffect(() => {
        if (rejectMessage) {
            console.debug("rejectMessage: " + JSON.stringify(rejectMessage))
            openRejectMessageDialog(rejectMessage)
        }
    }, [rejectMessage])

    return (
        <Fragment>
            <Tab.Navigator
                screenOptions={{
                    headerShown: true,
                    tabBarStyle: { backgroundColor: colors.MainMenuBg, height: 55 + inset.bottom }
                }}
            // initialRouteName = {route.params && route.params.defaultTab?route.params.defaultTab : 'Home'}
            >
                <Tab.Screen
                    name="Tab_WatchlistView"
                    component={WatchlistView}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuWatchlist color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.watchlist')}</Text>
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                        },
                    }}/>
                <Tab.Screen name="Tab_Portfolio"
                    component={PortfolioView}
                    initialParams={{defaultTab: PortfolioTabType.Position}}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuPortfolio color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.portfolio')}</Text>
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                            if (stateLoginLevel === 1) {
                                e.preventDefault();
                                navigation.navigate('PermissionAccess', {type: PermissionAccessType.member_only})
                            } else if (stateLoginLevel === 2) {
                                if (demoTradingAccount) {
                                    if (!isDemoAccount) {
                                        // e.preventDefault();
                                        // SetIsShowLoading(true);
                                        // // await switchAccount(demoTradingAccount.Account, demoTradingAccount.isDemo, demoTradingAccount.Entity)
                                        // var data = await doSwitchAccount(demoTradingAccount.Account, demoTradingAccount.isDemo, demoTradingAccount.Entity)
                                        // if (data && data.success) {
                                        //     await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, demoTradingAccount.isDemo);
                                        // }
                                        // navigation.navigate('TabDirectory', {screen: TabDirectoryTab.PORTFOLIO})
                                        // SetIsShowLoading(false);
                                        e.preventDefault();
                                        navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_has_demo})
                                    }
                                } else {
                                    e.preventDefault();
                                    navigation.navigate('PermissionAccess', {type: PermissionAccessType.lv2_no_demo})
                                }
                            }
                        },
                    }}/>
                {
                (!isDemoAccount || isSwitchingConnection) &&
                <Tab.Screen name="Tab_Me"
                    component={MeView}
                    options={{
                        tabBarIcon: (props) => <View style={styles.tabIconWrapper}><BottomMenuMe color={props.focused ? colors.MainMenuSelectedFont : colors.MainMenuImg} /></View>,
                        tabBarLabel: (props) => <Text style={[globalStyles.Mobile_Menu, { color: props.focused ? colors.MainMenuSelectedFont : colors.MainMenuFont }]}>{t('tabNavigation.me')}</Text>
                    }}
                    listeners={{
                        tabPress: async e => {
                            SetOnTouch()
                        },
                    }}/>
                }
            </Tab.Navigator>
        </Fragment>
    )
}

const styles = StyleSheet.create({
    tabIconWrapper: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
});

export default TabDirectory