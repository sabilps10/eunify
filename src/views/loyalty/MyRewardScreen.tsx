import React, {
  useState,
  createElement,
  useLayoutEffect,
  useEffect,
  useCallback
} from 'react';
import {
  Text,
  Image,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
  Pressable,
  useColorScheme,
  FlatList,
  Linking,
  ImageBackground,
  StyleSheet
} from 'react-native';


//Lang
import { getLanguage } from '../../locales/i18n';

import { create } from 'twrnc';

import { Auth } from 'aws-amplify';

//UI
//UI
//import TextLabel from '../../components/labels/TextLabel';
import TextLabel from '../../components/forms/labels/textLabel.component';
//import BigButton from '../../components/buttons/BigButton';
import BigButton from '../../components/buttons/bigButton.component';
//import MSmallButton from '../../components/buttons/mSmallButton';
import commonStyles, { makeGlobalStyles } from '../../styles/styles';

import { useFocusEffect, useTheme } from '@react-navigation/native';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import PressableComponent from '../utils/PressableComponent';
import StackView from '../../components/stackView.component'
import ArrowRight from '../../../assets/svg/ArrowRight';

import Header from '../../components/header/header.component';
import HeaderButton from '../../components/header/headerButton.component';
import TopBarBack from '../../../assets/svg/TopBarBack';

//Redux
import { useDispatch, useSelector } from 'react-redux';


import { State } from '../../../src/redux/root-reducer';

//Translate
import { useTranslation } from 'react-i18next';

//Util
//import { getTheme } from "../../utils/ThemeUtil";
import { commify, nullToEmpty, isNullString } from '../../utils/stringUtils';
import HeaderLv3Blue from '../../components/header/headerLv3Blue';
import HeaderDarkArrowButton from '../../components/header/headerDarkArrowButton';

import {
  getImage_loyaltybackground,
  getIcon_whatsapp,
  getIcon_next_tight
} from '../../../assets/constants';

import {
  host_loyaltyApi,
  loyalty_getExistingAccount,
  host_rewardApi,
  reward_pageSize,
  reward_defaultPointStart,
  reward_defaultPointEnd,
  reward_whatsappUrl,
  reward_contactus_emxpro,
  reward_efsgphone,
  reward_efsgemail,
  entityId as entityForFebVersion, //FebVersion of EIEHK or EMXPRO
  reward_signup_eiehk,
  emxproLiveChat,
  reward_login_loyaltyportal,
} from '../../config/constants';
import { Platform } from 'react-native';
import { TabDirectoryWebViewPage, TabDirectoryWebViewTab } from '../tabDirectory/tabDirectoryWebView';
import { aoEntry } from '../../utils/navigation';

interface loyaltyAccountInf {
  errorReasonCode?: string,
}

function MyRewardScreen({ route, navigation }) {
  const { t } = useTranslation();
  const currentLang = getLanguage();

  const tw = create(require(`../../tailwind.config`));
  const { colors } = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const scheme = useColorScheme();

  const {individual} = useSelector((state: State) => state.accountOpening);

  const [rewardList, setRewardList] = useState([]);
  const [loyaltyAccount, setLoyaltyAccount] = useState<loyaltyAccountInf>({});
  const [loyaltyAccountResponse, setLoyaltyAccountResponse] = useState<any>({});

  const loginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
  const dispatch = useDispatch();
  const { SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);


  //DEBUG
  //let localMsg = 'Start Debug...';
  //const [debugMsg, setDebugMsg] = useState(localMsg);

  //Constant
  const MEM_RETURNCODE_LV2 = 'E001'; //Account not found

  //Obtain default entity defined in config file.
  const loyaltyMemberCRMUserAccountQueryParamsEntity = entityForFebVersion == 'EIE' ? 'EIEHK' : 'XPro';
  const rewardListQueryParamsEntity = entityForFebVersion == 'EIE' ? 'EIEHK' : 'EMXpro (SVG)';


  //User email is obtained from cognito during login process
  const accountEmail = "efsg.test1005@gmail.com";
  // let loyaltyMembership = {};

  //fetch api
  // useEffect(() => {
  //   try {
  //     //TODO: Passing in email and entity; from user profile during login
  //     loyaltyMembership = getExistingLoyalAccountJson(
  //       accountEmail,
  //       accountEntity,
  //     );
  //   } catch (e) {
  //     console.error(e);
  //   }
  //   //Loyalty points
  // }, []);

  useFocusEffect(
    useCallback(() => {
      const fetchData = async () => {
        SetIsShowLoading(true);
        getExistingLoyalAccountJson(loyaltyMemberCRMUserAccountQueryParamsEntity,)
      }
      fetchData().catch(console.error);
    }, [])
  );



  // useLayoutEffect(() => {
  //   const props = {
  //     func: onPressBackBtn,
  //     title: t('myreward.title'),
  //   };
  //   navigation.setOptions(getNavOption(loyaltyAccount.errorReasonCode, props));
  // }, [navigation, nullToEmpty(loyaltyAccount.errorReasonCode)]);

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () => <Header leftComponents={
        <HeaderButton
          iconComponent={<TopBarBack color={colors.MainFont} />}
          onPress={onPressBack}
          title={t('myreward.title')}
          titleStyle={[globalStyles.H4, { color: colors.Brand2 }]} />
      } />
    });
  }, [navigation]);

  //Get option
  // const getNavOption = (code, props) => {
  //   console.debug('Code = ' + code);
  //   if (
  //     code === MEM_RETURNCODE_LV2 ||
  //     code === MEM_RETURNCODE_LV3_NO_MEMBERSHIP
  //   ) {
  //     return {
  //       headerShown: true,
  //       header: () => <HeaderDarkArrowButton {...props} />,
  //     };
  //   } else {
  //     return {
  //       headerShown: true,
  //       header: () => <HeaderLv3Blue {...props} />,
  //     };
  //   }
  // };

  const gotoWeb = url => {
    if (url === 'email') {
      let emailLink = `mailto:${reward_efsgemail}`;
      // Create email link query
      const query = `{
        subject: 'Reward point enquiry',
        body: 'I would like to check my reward points.',
      }`;
      if (query.length) {
        emailLink += `?${query}`;
      }
      Linking.openURL(emailLink);
    } else if (url === 'phone') {
      Linking.openURL(`tel:${reward_efsgphone}`);
    } else {
      //Whatsapp
      Linking.openURL(url);
    }
  };


  const onPressBack = () => { navigation.goBack() }

  const langConversionForLoyalty = (lang: string) => {
    switch (lang) {
      case 'en':
        return 'english';
        break;
      case 'tc':
        return 'tchinese';
        break;
      case 'sc':
        return 'chinese';
        break;
      default:
        return 'english';
    }
  };

  const onClickGoToLoyalty = () => {
    if (loginLevel ===  3) {
      if (entityForFebVersion === 'EIE') {
        gotoWeb(reward_whatsappUrl + t('myreward.whatsappmessage_intext2'));
      } else {
        Linking.openURL(emxproLiveChat);
      }
    } else if (loginLevel === 2) {
      aoEntry(individual, navigation);

    }
    //navigation.navigate('Level3-Ekyc', {});
  };

  const getExistingLoyalAccountJson = async (entity: string) => {

    const session = await Auth.currentSession();
    var token = session.getAccessToken().getJwtToken();
    var payload: { [key: string]: any; } = session.getIdToken().payload;
    const email = payload['email'];


    const urlParam = encodeURI('?email=' + email + '&entity=' + entity);
    const url = host_loyaltyApi + loyalty_getExistingAccount + urlParam;


    console.debug('getExistingLoyalAccountJson url:' + url + "-----" + token);

    let returnJson: any = {};
    if (!isNullString(email) && !isNullString(entity)) {
      try {

        fetch(url, {
          method: 'GET',
          headers: {
            Authorization: token,
          },
        })
        .catch(networkError => console.error(`networkError ${networkError}`))
          .then(response => response.ok ? response.json() : Promise.reject(`http error status ${response.status}`))
          .then(

            json => {
              returnJson = json;
            })
          .catch(error => {
            console.error('getExistingLoyalAccountJson fetch response handle error catch: ' + error);
          })
          .finally(() => {
            SetIsShowLoading(false);
            console.debug("returnJson =====" + returnJson);
            console.debug(returnJson);
            getRewardList(
              rewardListQueryParamsEntity,
              langConversionForLoyalty(currentLang),
            );
            setLoyaltyAccount(returnJson);
            const { response, errorResasonCode }: { response: any; errorResasonCode: string } = returnJson;
            if (
              response !== undefined &&
              errorResasonCode === undefined
            ) {
              //Save Member Profile
              setLoyaltyAccountResponse(response);
              //Get reward list
              // getRewardList(
              //   accountEntity,
              //   langConversionForLoyalty(currentLang),
              // );
            }
            return returnJson;
          });
      } catch (e) {
        //localMsg += 'error' + e;
        //setDebugMsg(localMsg);
        console.error('getExistingLoyalAccountJson try catch: ' + e);
        return returnJson;
      }
    }
  };

  const getRewardList = (entity: string, lang: string) => {
    //localMsg += '.../n/ngetTestData2';
    const queryParam =
      '"loyaltyPointStart":' +
      reward_defaultPointStart +
      ',"loyaltyPointEnd":' +
      reward_defaultPointEnd +
      ',"language":"' +
      lang +
      '","entityList":["' +
      entity +
      '"]';


    const url =
      host_rewardApi +
      '/reward?queryParam={' +
      queryParam +
      '}&pageNo=0&' +
      reward_pageSize +
      '&orderBy=lastmodifieddate&isAsc=false';

    fetch(url, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(json => {
        if (json.result) {
          if (json.result.recordList === undefined) {
            console.debug(
              'Reward list undefined:' + JSON.stringify(json.result.recordList),
            );
            console.debug('Result not found.');
          } else {
            //          console.debug("Reward result found:"+ JSON.stringify(json.result));
            console.debug(
              'Reward list found:' + JSON.stringify(json.result.recordList),
            );
            setRewardList(json.result.recordList);
          }
        }
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        SetIsShowLoading(false);
      });
  };

  //Display single item
  const Item = ({ i }) => (
    <View
      style={tw`p-4 flex flex-col max-w-[150px] justify-between items-center`}>
      <View style={tw`bg-white rounded-lg`}>
        <Image
          source={{ uri: i.reward_item__c.image_path__c }}
          style={[{ width: 120, height: 120 }, tw`rounded-lg`]}
        />
      </View>
      <View style={tw`h-[110px] pt-2`}>
        <Text style={[globalStyles.Body_Text_B, tw`text-brand2  text-center`]}>
          {i.reward_item__c.name}
        </Text>
      </View>
      <View style={tw`flex-1`}>
        <Text style={[globalStyles.Body_Text]}>
          {commify(i.required_loyalty_point__c) +
            ' ' +
            t('myreward.point_unit')}
        </Text>
      </View>
    </View>
  );

  const renderItem = ({ item }) => <Item i={item} />;
  const keyExtractor = item => item.id;

  //Show reward list
  const Display = () => {
    let content;
    //Debuggin
    //loyaltyAccount.errorReasonCode = 'E002';
    if (loyaltyAccount && loyaltyAccount.errorReasonCode) {
      content = Display_Lv2;
      // content = Display_MyReward;
    } else if (Object.keys(loyaltyAccount).length != 0) {
      content = Display_MyReward;
    } else {
      content = Display_empty;
    }
    return (
      <ScrollView
        contentContainerStyle={commonStyles.ScrollViewContainerStyle}
        style={tw`pb-6`}>
        {content()}
      </ScrollView>
    );
  };

  //Get related translation based on the app entity (EIEHK or EMXPro)
  const entityTranlation = translation => {
    let a = translation + '_' + entityForFebVersion.toLowerCase();
    return t(a);
  };

  const Display_Lv2 = () => {
    let articleHeader =
      entityForFebVersion !== 'EIE'
        ? t('myreward.emxpro_loyalty_text')
        : t('myreward.loyalty_text');
    let articleContent =
      entityForFebVersion !== 'EIE'
        ? t('myreward.emxpro_loyalty_text2')
        : t('myreward.loyalty_text2');



    return (
      <View style={tw`flex flex-col`}>

        <View style={tw`w-full h-auto bg-slate-200`}>
          <ImageBackground
            style={tw`w-full h-[200px]`}
            source={getImage_loyaltybackground(scheme)}
            resizeMode="cover">
            <View
              style={[
                tw`rounded-t-3xl w-full h-[30px] absolute bottom-0`,
                globalStyles.Background,
              ]}></View>
          </ImageBackground>
        </View>
        <ScrollView style={tw`px-4 py-2 flex flex-col`}>
          <Text style={[globalStyles.Body_Text_B]}>{articleHeader}</Text>
          <Text style={[globalStyles.Body_Text_B]}></Text>
          <Text style={[globalStyles.Body_Text_B]}>{Platform.OS === "android" ? articleContent.replace(/\n\r/g, currentLang === 'en' ? '\n\n' : '\n') : articleContent}</Text>

          {loyaltyAccount.errorReasonCode === MEM_RETURNCODE_LV2? (
            <View style={tw`pt-4 pb-6 px-8`}>
              <BigButton
                title={entityTranlation('myreward.button_signup')}
                onPress={onClickGoToLoyalty}
              />

            </View>
          ) : null}
        </ScrollView>
      </View>
    );
  };

  //Display for registered loyalty member
  const Display_MyReward = () => {
    return (
      <View style={[tw`flex flex-col h-full`]}>
        <View style={tw`flex flex-col flex-grow justify-center`}>
          <View style={tw`p-3  bg-brand3`}>
            <View style={tw`pl-4 flex flex-row items-baseline`}>
              {loyaltyAccountResponse ? (
                <Text style={[globalStyles.H1, tw`text-white text-[40px]`]}>
                  {loyaltyAccountResponse.totalPointBalance
                  }
                </Text>
              ) : (
                <View />
              )}
              <Text style={[globalStyles.Body_Text, tw`text-white pl-1`]}>
                {t('myreward.point_unit')}
              </Text>
            </View>
            <View style={tw`pl-4 pt-2 pb-6`}>
              {loyaltyAccountResponse ? (
                <View style={[tw`bg-brand2 p-2 self-start`, { borderRadius: 5 }]}>
                  <Text style={[globalStyles.Small_Note_B, tw`text-white`]}>
                    {loyaltyAccountResponse?.tier?.toUpperCase() +
                      ' '}
                  </Text>
                </View>
              ) : (
                <View />
              )}
            </View>
          </View>
          <View style={tw`w-full bg-brand3`}>
            <View
              style={[
                tw`rounded-t-xl w-full h-[30px]`,
                globalStyles.Background,
              ]}>
              <Text> </Text>
            </View>
          </View>
          <View style={tw`flex flex-col rounded-t-xl flex-grow p-4 pt-4`}>
            <View style={tw`py-3`}>
              <Text style={[globalStyles.H4, tw`text-brand2`]}>
                {t('myreward.rewards_subtitle')}
              </Text>
            </View>
            <View style={{ height: 300 }}>
              <FlatList
                horizontal
                data={rewardList}
                renderItem={renderItem}
                keyExtractor={keyExtractor}
              />
            </View>
            <View style={tw`py-2`}>
              <BigButton
                type="Outline"
                title={t('myreward.btn_redeem_rewards')}
                onPress={() => navigation.push('TabDirectoryWebView', {
                  page: TabDirectoryWebViewPage.RewardLoginLoyaltyPortal,
                  focusTab: TabDirectoryWebViewTab.ME,
                  param: {},
                })}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };


  const Display_empty = () => {
    return (
      <View style={[tw`flex flex-col h-full`]}>
        <View style={tw`flex flex-col flex-grow justify-center`}>

        </View>
      </View>
    );
  };
  return <Display />;
}



export default MyRewardScreen;
