import { CognitoTempActionTypes, CognitoTempAction, CognitoTempState } from "./types";
import { Dispatch } from "redux";


export const SetLoginTempCredentials = (value: CognitoTempState) => {
    
    return (dispatch: Dispatch<CognitoTempAction>) => {
        console.debug('SetLoginTempCredentials')
        dispatch({
            type: CognitoTempActionTypes.SET_TEMP_LOGIN,
            payload: value
        })
    }
}