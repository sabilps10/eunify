import { CognitoTempActionTypes, CognitoTempAction, CognitoTempState } from "./types"

const INITIAL_STATE: CognitoTempState = {
    username: "",
    password: ""
}

const LoginCredentialsReducerTemp = (state = INITIAL_STATE, action: CognitoTempAction) => {
    switch (action.type) {
        case CognitoTempActionTypes.SET_TEMP_LOGIN:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}

export default LoginCredentialsReducerTemp;