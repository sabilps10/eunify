// Store username and password using redux-persist-sensitive-storage
// Ref to https://reactnative.dev/docs/security#secure-storage
export type CognitoTempState = {
    username: string,
    password: string
}

interface SetLoginTempCredentialsInterface {
    type: CognitoTempActionTypes.SET_TEMP_LOGIN,
    payload: CognitoTempState
}

export type CognitoTempAction = SetLoginTempCredentialsInterface;

export enum CognitoTempActionTypes {
    SET_TEMP_LOGIN = 'SET_TEMP_LOGIN'
}