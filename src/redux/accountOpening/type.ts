export type AccountQuestionType = {
  jurisdictionOfResidence: string;
  hasHKBankAccount: boolean | string;
  tradeProducts: string[];
  tradingAccountType?: string;
};

export type PersonalInformationType = {
  customerId?: string;
  legalLastNameEn: string;
  legalFirstAndMiddleNameEn: string;
  legalLastName: string;
  legalFirstAndMiddleName: string;
  gender: string;
  jurisdictionOfResidence: string;
  dateOfBirth: Date | string;
  jurisdiction: string;
  identificationType: string;
  identificationNo: string;
  identificationExpiryDate: Date | string;
  usCitizen: boolean;
  efsgRewardProgram: boolean;
};

export type ContactInformation = {
  customerId?: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  province: string;
  postcode: string;
  jurisdictionOfResidence: string;
};

export type FinancialInformation = {
  customerId?: string;
  employmentStatus: string;
  industrial: string;
  jobTitle: string;
  annualIncome: number | string;
  liquidNetworth: number | string;
  jurisdictionOfTaxResidence: string;
  sourceOfFunds: string;
  haveTin: boolean;
  tinNumber: string | null;
};

export type TradingExperience = {
  customerId?: string;
  fiveOrMorTransactionLastThreeYears: string | boolean | null;
  haveOtherTrade: string | boolean | null;
  tradingExperience: string | number | null;
  tradingFrequency: string | null;
  accountOpeningPurpose: string | null;
};

export type Promotions = {
  promocode: string;
  title: string;
};

export type INITIAL_STATE_TYPE = {
  accountQuestion: AccountQuestionType | null;
  personalInformation: PersonalInformationType | null;
  contactInformation: ContactInformation | null;
  financialInformation: FinancialInformation | null;
  tradingExperience: TradingExperience | null;
  promotions: Promotions | null;
  individual: any;
  tradingAccount: string[] | [];
  selectedTradingAccount: string | null;
  depositDialog: {
    isOpen: string | null;
    onClose: () => void;
    onPress: () => void;
    url: string | null;
  } | null;
  resetDialog: {
    resetDialogDeposit: boolean;
  };
};

export type ResponseAccountOpening = {
  response: {
    id: string;
    entity: string;
    clientType: string;
    clientTypeLabel: string | null;
    sumsubLink: string | null;
    email: string | null;
    mobileCountryCode: string;
    mobile: string | null;
    legalEntityName: string | null;
    identificationNo: string | null;
    identificationType: string | null;
    dateOfBirth: string | null;
    accountOpeningStatus: string | null;
    status: string | null;
    statusLabel: string | null;
    subStatus: string | null;
    createdBy: string | null;
    createdDate: Date | string | null;
    lastModifiedBy: string | null;
    lastModifiedDate: Date | string | null;
    refCode: string | null;
    username: string | null;
    tradeAccountNo: string | null;
    upperIbAcc: string | null;
    promoCode: string | null;
    suspendReason: string | null;
    rejectReason: string | null;
    legalLastNameEn: string | null;
    legalFirstAndMiddleNameEn: string | null;
    legalLastName: string | null;
    legalFirstAndMiddleName: string | null;
    gender: string | null;
    jurisdictionOfResidence: string | null;
    jurisdictionOfTaxResidence: string | null;
    jurisdiction: string | null;
    identificationExpiryDate: string | null;
    usCitizen: boolean;
    efsgRewardProgram: boolean;
    loyalty: boolean;
    addressLine1: string | null;
    addressLine2: string | null;
    city: string | null;
    province: string | null;
    postcode: string | null;
    employmentStatus: string | null;
    industrial: string | null;
    jobTitle: string | null;
    annualIncome: string | null;
    liquidNetworth: string | null;
    sourceOfFunds: string | null;
    countryOfTaxResidence: string | null;
    taxpayerIdentificationNumber: string | null;
    haveTin: boolean | null;
    fiveOrMorTransactionLastThreeYears: string | null;
    yearsOfTrading: string | null;
    frequencyOfTrading: string | null;
    purposeOfTrading: string | null;
    expiredDate: string | null;
    hasHKBankAccount: boolean;
    tradeProduct: string | string[] | [];
    tradingAccountType: string | null;
  };
};

enum AccountOpeningActionType {
  SET_ACCOUNT_QUESTION = 'SET_ACCOUNT_QUESTION',
  SET_PERSONAL_INFORMATION = 'SET_PERSONAL_INFORMATION',
  SET_CONTACT_INFORMATION = 'SET_CONTACT_INFORMATION',
  SET_FINANCIAL_INFORMATION = 'SET_FINANCIAL_INFORMATION',
  SET_TRADING_EXPERIENCE = 'SET_TRADING_EXPERIENCE',
  SET_PROMOTIONS = 'SET_PROMOTIONS',
  SET_INDIVIDUAL = 'SET_INDIVIDUAL',
  SET_TRADING_ACCOUNT = 'SET_TRADING_ACCOUNT',
  SET_SELECTED_TRADING_ACCOUNT = 'SET_SELECTED_TRADING_ACCOUNT',
  SET_DIALOG_DEPOSIT = 'SET_DIALOG_DEPOSIT',
  SET_RESET_DIALOG_DEPOSIT = 'SET_RESET_DIALOG_DEPOSIT',
  RESET = 'RESET',
}

export default AccountOpeningActionType;

type setAccountQuestion = {
  type: AccountOpeningActionType.SET_ACCOUNT_QUESTION;
  payload: AccountQuestionType;
};

type setPersonalInformation = {
  type: AccountOpeningActionType.SET_PERSONAL_INFORMATION;
  payload: PersonalInformationType;
};

type setContactInformation = {
  type: AccountOpeningActionType.SET_CONTACT_INFORMATION;
  payload: ContactInformation;
};

type setFinancialInformation = {
  type: AccountOpeningActionType.SET_FINANCIAL_INFORMATION;
  payload: FinancialInformation;
};

type setTradingExperience = {
  type: AccountOpeningActionType.SET_TRADING_EXPERIENCE;
  payload: TradingExperience;
};

type setPromotions = {
  type: AccountOpeningActionType.SET_PROMOTIONS;
  payload: Promotions;
};

type setIndividual = {
  type: AccountOpeningActionType.SET_INDIVIDUAL;
  payload: any;
};

type Reset = {
  type: AccountOpeningActionType.RESET;
};

type SetTradingAccount = {
  type: AccountOpeningActionType.SET_TRADING_ACCOUNT;
  payload: string[] | [];
};

type SetSelectedTradingAccount = {
  type: AccountOpeningActionType.SET_SELECTED_TRADING_ACCOUNT;
  payload: string;
};

type SetDialogDeposit = {
  type: AccountOpeningActionType.SET_DIALOG_DEPOSIT;
  payload: {
    isOpen: string | null;
    onClose: () => void;
    onPress: () => void;
    url: string | null;
  };
};

type SetResetDialogDeposit = {
  type: AccountOpeningActionType.SET_RESET_DIALOG_DEPOSIT;
  payload: {
    resetDialogDeposit: boolean | null;
  };
};

export type AccountOpeningAction =
  | setAccountQuestion
  | setPersonalInformation
  | setContactInformation
  | setFinancialInformation
  | setTradingExperience
  | setPromotions
  | Reset
  | setIndividual
  | SetTradingAccount
  | SetSelectedTradingAccount
  | SetDialogDeposit
  | SetResetDialogDeposit;
