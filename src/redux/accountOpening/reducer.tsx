/* eslint-disable @typescript-eslint/no-unused-vars */
import AccountOpeningActionType, {
  AccountOpeningAction,
  INITIAL_STATE_TYPE,
} from './type';

const INITIAL_STATE: INITIAL_STATE_TYPE = {
  accountQuestion: {
    jurisdictionOfResidence: '',
    hasHKBankAccount: '',
    tradeProducts: [],
    tradingAccountType: '',
  },
  personalInformation: {
    customerId: '',
    legalLastNameEn: '',
    legalFirstAndMiddleNameEn: '',
    legalLastName: '',
    legalFirstAndMiddleName: '',
    gender: null,
    jurisdictionOfResidence: '',
    dateOfBirth: null,
    jurisdiction: '',
    identificationType: '',
    identificationNo: '',
    identificationExpiryDate: null,
    usCitizen: true,
    efsgRewardProgram: true,
  },
  contactInformation: {
    customerId: '',
    addressLine1: '',
    addressLine2: '',
    city: '',
    province: '',
    postcode: '',
    jurisdictionOfResidence: '',
  },
  financialInformation: {
    customerId: '',
    employmentStatus: '',
    industrial: '',
    jobTitle: '',
    annualIncome: '',
    liquidNetworth: '',
    jurisdictionOfTaxResidence: '',
    sourceOfFunds: '',
    haveTin: true,
    tinNumber: '',
  },
  tradingExperience: {
    customerId: '',
    fiveOrMorTransactionLastThreeYears: null,
    haveOtherTrade: null,
    tradingExperience: null,
    tradingFrequency: '',
    accountOpeningPurpose: '',
  },
  promotions: {
    promocode: '',
    title: '',
  },
  individual: null,
  tradingAccount: [],
  selectedTradingAccount: null,
  depositDialog: {
    isOpen: null,
    onClose: () => {},
    onPress: () => {},
    url: null,
  },
  resetDialog: {
    resetDialogDeposit: false,
  },
};

const AccouuntOpeningReducer = (
  state = INITIAL_STATE,
  action: AccountOpeningAction,
) => {
  switch (action.type) {
    case AccountOpeningActionType.SET_ACCOUNT_QUESTION:
      return {
        ...state,
        accountQuestion: action.payload,
      };
    case AccountOpeningActionType.SET_PERSONAL_INFORMATION:
      return {
        ...state,
        personalInformation: action.payload,
      };
    case AccountOpeningActionType.SET_CONTACT_INFORMATION:
      return {
        ...state,
        contactInformation: action.payload,
      };
    case AccountOpeningActionType.SET_FINANCIAL_INFORMATION:
      return {
        ...state,
        financialInformation: action.payload,
      };
    case AccountOpeningActionType.SET_TRADING_EXPERIENCE:
      return {
        ...state,
        tradingExperience: action.payload,
      };
    case AccountOpeningActionType.SET_PROMOTIONS:
      return {
        ...state,
        promotions: action.payload,
      };
    case AccountOpeningActionType.SET_INDIVIDUAL:
      return {
        ...state,
        individual: action.payload,
      };
    case AccountOpeningActionType.RESET:
      return {
        accountQuestion: null,
        personalInformation: null,
        contactInformation: null,
        financialInformation: null,
        tradingExperience: null,
        promotions: null,
        individual: null,
        tradingAccount: [],
        selectedTradingAccount: null,
        depositDialog: {
          isOpen: null,
          onClose: () => {},
          onPress: () => {},
          url: null,
        },
      };
    case AccountOpeningActionType.SET_TRADING_ACCOUNT:
      return {
        ...state,
        tradingAccount: action.payload,
      };
    case AccountOpeningActionType.SET_SELECTED_TRADING_ACCOUNT:
      return {
        ...state,
        selectedTradingAccount: action.payload,
      };
    case AccountOpeningActionType.SET_DIALOG_DEPOSIT:
      return {
        ...state,
        depositDialog: action.payload,
      };
    case AccountOpeningActionType.SET_RESET_DIALOG_DEPOSIT:
      return {
        ...state,
        resetDialog: action.payload,
      };

    default:
      return state;
  }
};

export default AccouuntOpeningReducer;
