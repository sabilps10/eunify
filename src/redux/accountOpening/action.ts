/* eslint-disable @typescript-eslint/no-unused-vars */
import reactotron from 'reactotron-react-native';
import AccountOpeningActionType, {
  AccountOpeningAction,
  AccountQuestionType,
  ContactInformation,
  FinancialInformation,
  INITIAL_STATE_TYPE,
  PersonalInformationType,
  Promotions,
  TradingExperience,
} from './type';
import {Dispatch} from 'react';

export const SetAccountQuestion = (value: AccountQuestionType) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_ACCOUNT_QUESTION,
      payload: value,
    });
  };
};

export const SetPersonalInformation = (value: PersonalInformationType) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_PERSONAL_INFORMATION,
      payload: value,
    });
  };
};

export const SetContactInformation = (value: ContactInformation) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_CONTACT_INFORMATION,
      payload: value,
    });
  };
};

export const SetFinancialInformation = (value: FinancialInformation) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_FINANCIAL_INFORMATION,
      payload: value,
    });
  };
};

export const SetTradingExperience = (value: TradingExperience) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_TRADING_EXPERIENCE,
      payload: value,
    });
  };
};

export const SetPromotions = (value: Promotions) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_PROMOTIONS,
      payload: value,
    });
  };
};

export const SetIndividual = (val: any) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_INDIVIDUAL,
      payload: val,
    });
  };
};

export const SetTradingAccount = (val: string[] | []) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_TRADING_ACCOUNT,
      payload: val,
    });
  };
};

export const SetSelectedTradingAccount = (val: string) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_SELECTED_TRADING_ACCOUNT,
      payload: val,
    });
  };
};

export const SetDepositDialog = (val: any) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_DIALOG_DEPOSIT,
      payload: val,
    });
  };
};

export const SetResetDialogDeposit = (val: any) => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.SET_RESET_DIALOG_DEPOSIT,
      payload: val,
    });
  };
};

export const Reset = () => {
  return (dispatch: Dispatch<AccountOpeningAction>) => {
    dispatch({
      type: AccountOpeningActionType.RESET,
    });
  };
};
