import SelectionActionType, {SelectionState, SelectionAction, SelectedState} from "./type";

export type SELECTION_STATE_TYPE = {
    //data
    selection: SelectionState,
    selected: SelectedState | null
}

const INITIAL_STATE: SELECTION_STATE_TYPE = {
    selection: {
        selectedOption: "",
        selectOptions: [],
        isOpen: false,
        location: null
    },
    selected: null
}

const SelectionReducer = (state = INITIAL_STATE, action: SelectionAction) => {
    switch(action.type) {
        case SelectionActionType.OPEN_CLOSE_SELECTION:
            console.debug("OpenClose");
            return {
                ...state,
                selection: {
                    ...state.selection,
                    ...action.payload
                }
            }
        case SelectionActionType.UPDATE_SELECTED:
            return {
                ...state,
                selected: {
                    ...state.selected,
                    ...action.payload
                }
            }
        default:
            return state;
    }
}

export default SelectionReducer;