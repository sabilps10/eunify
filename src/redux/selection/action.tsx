import SelectionActionType, {SelectionState, SelectionAction, SelectedState} from "./type";
import { Dispatch } from "redux";


export const OpenCloseSelection = (value: SelectionState) => {
    console.debug(`OpenCloseSelection: ${value.isOpen}, ${value.location}`);
    return (dispatch: Dispatch<SelectionAction>) => {
        dispatch({
            type: SelectionActionType.OPEN_CLOSE_SELECTION,
            payload: value
        })
    }
};

export const SetSelected = (value: SelectedState) => {
    return (dispatch: Dispatch<SelectionAction>) => {
        dispatch({
            type: SelectionActionType.UPDATE_SELECTED,
            payload: value
        })
    }
};