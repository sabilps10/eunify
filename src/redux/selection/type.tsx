export type SelectionState = {
    selectOptions: {key: string, value: string}[],
    selectedOption: string,
    isOpen: boolean,
    location?: SelectionContextLocation
}

export enum SelectionContextLocation {
    WatchlistCategory = "watchlistCategory",
    WatchlistPriceChange = "watchlistPriceChange",
    LoginPhonePrefix = "loginPhonePrefix",
    ForgotPasswordPhonePrefix = "forgotPasswordPhonePrefix",
    SignUpPhonePrefix = "signUpPhonePrefix",
    SettingLanguage = "settingLanguage",
    SettingTheme = "settingTheme",
    SettingPhonePrefix = "settingPhonePrefix",
    CurrentTradeAccount = "currentTradeAccount",
    ChartType = "chartType",
    ChartInterval = "chartInterval",
    PortfolioSortingType = "portfolioSortingType",
}

export type SelectedState = {
    [SelectionContextLocation.WatchlistCategory]?: string,
    [SelectionContextLocation.WatchlistPriceChange]?: string,
    [SelectionContextLocation.LoginPhonePrefix]?: string,
    [SelectionContextLocation.ForgotPasswordPhonePrefix]?: string,
    [SelectionContextLocation.SignUpPhonePrefix]?: string,
    [SelectionContextLocation.SettingLanguage]?: string,
    [SelectionContextLocation.SettingTheme]?: string,
    [SelectionContextLocation.SettingPhonePrefix]?: string,
    [SelectionContextLocation.CurrentTradeAccount]?: string,
    [SelectionContextLocation.ChartType]?: string,
    [SelectionContextLocation.ChartInterval]?: string,
    [SelectionContextLocation.PortfolioSortingType]?: string
}

enum SelectionActionType {
    OPEN_CLOSE_SELECTION = 'OPEN_CLOSE_SELECTION',
    UPDATE_SELECTED = 'UPDATE_SELECTED'
}

export default SelectionActionType;

interface OpenCloseSelectionInterface {
    type: SelectionActionType.OPEN_CLOSE_SELECTION,
    payload: SelectionState
}

interface UpdateSelectedInterface {
    type: SelectionActionType.UPDATE_SELECTED,
    payload: SelectedState
}

export type SelectionAction = OpenCloseSelectionInterface | UpdateSelectedInterface