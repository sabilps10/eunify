import ConnectionActionType, {Status, ConnectionAction} from "./type";
import { Dispatch } from "redux";


export const SetConnectionStatus = (value: Status) => {
    return (dispatch: Dispatch<ConnectionAction>) => {
        console.debug(`SetConnectionStatus: ${value}`);
        dispatch({
            type: ConnectionActionType.SET_CONNECTION_STATUS,
            payload: value
        })
    }
};

export const SetSwitchingConnection = (value: boolean) => {
    return (dispatch: Dispatch<ConnectionAction>) => {
        console.debug(`SetSwitchingConnection: ${value}`);
        dispatch({
            type: ConnectionActionType.SET_SWITCHING_CONNECTION,
            payload: value
        })
    }
};