import ConnectionActionType, {ConnectionState, ConnectionAction, Status} from "./type";

const INITIAL_STATE: ConnectionState = {
    webSocketStatus: Status.Disconneted,
    isSwitchingConnection: false
}

const ConnectionReducer = (state = INITIAL_STATE, action: ConnectionAction) => {
    switch(action.type) {
        case ConnectionActionType.SET_CONNECTION_STATUS:
            return {
                ...state,
                webSocketStatus: action.payload
            }
        case ConnectionActionType.SET_SWITCHING_CONNECTION:
            return {
                ...state,
                isSwitchingConnection: action.payload
            }
        default:
            return state;
    }
}

export default ConnectionReducer;