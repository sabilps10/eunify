export enum Status {
    Connected = "connected",
    Disconneted = "disconnected",
    ManualDisconnected = "manualDisconnected",
    DuplicateLogin = "duplicateLogin",
    ResetPassword = "resetPassword"
}

export type ConnectionState = {
    webSocketStatus: Status,
    isSwitchingConnection: boolean
}

enum ConnectionActionType {
    SET_CONNECTION_STATUS = 'SET_CONNECTION_STATUS',
    SET_T_API_PATH = 'SET_T_API_PATH',
    SET_SWITCHING_CONNECTION = 'SET_SWITCHING_CONNECTION'
}

export default ConnectionActionType;

interface SetConnectionStatusInterface {
    type: ConnectionActionType.SET_CONNECTION_STATUS,
    payload: Status
}

interface SetSwitchingConnectionInterface {
    type: ConnectionActionType.SET_SWITCHING_CONNECTION,
    payload: boolean
}


export type ConnectionAction = SetConnectionStatusInterface | SetSwitchingConnectionInterface;
