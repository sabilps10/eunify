import { combineReducers } from 'redux';

import AccountReducer from './account/reducer';
import ConnectionReducer from './connection/reducer';
import TradingReducer from './trading/reducer';
import StateReducer from './state/reducer';
import SelectionReducer from './selection/reducer';
import LoginCredentialsReducer from './cognito/reducer';
import LoginCredentialsReducerTemp from './cognitoTemp/reducer';
import AccouuntOpeningReducer from './accountOpening/reducer';
import BottomSheetReducer from './BottomSheet/reducer';

import { createMigrate, persistReducer } from "redux-persist";
import createSensitiveStorage from "redux-persist-sensitive-storage";
import AsyncStorage from '@react-native-async-storage/async-storage';

const sensitiveStorage = createSensitiveStorage({
    keychainService: "myKeychain",
    sharedPreferencesName: "mySharedPrefs"
});

const sensitiveStorageTemp = createSensitiveStorage({
    keychainService: "myKeychainTemp",
    sharedPreferencesName: "mySharedPrefsTemp"
});

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['cognito', 'state'],
    version: 1,
    migrate: (state) => {
        state = {
            ...state,
            state: {
                ...state.state,
                testPersist: 'test',
                regionList: [],
                isShowProgress: {
                    isShowDialog: false,
                    progress: 0,
                    total: 0
                },
                isShowOrderConfirmDialog: {
                    isShowDialog: false,
                    type: '', 
                    contractCode: '', 
                    isSelectBuy: false, 
                    volume: 0, 
                    price: 0,  
                    isPending: false, 
                    isProfit: false, 
                    stopLossPrice: 0, 
                    limitProfitPrice: 0, 
                }
            },
            trade: {
                ...state.trade,
                currentTrade: {
                    StopLossPrice: -1,
                    StopLossPriceRef: -1,
                    TakeProfitPrice: -1,
                    TakeProfitPriceRef: -1
                }
            },
            cognitoTemp: {
                username: '',
                password: ''
            }
        }
        return Promise.resolve(state)
    }
}

const securePersistConfig = {
    key: "secure",
    storage: sensitiveStorage
  };

const securePersistConfigTemp = {
    key: "secureTemp",
    storage: sensitiveStorageTemp
  };

const appReducer = combineReducers({
    account: AccountReducer,
    connection: ConnectionReducer,
    trading: TradingReducer,
    state: StateReducer,
    selection: SelectionReducer,
    cognito: persistReducer(securePersistConfig, LoginCredentialsReducer),
    cognitoTemp: persistReducer(securePersistConfigTemp, LoginCredentialsReducerTemp),
    accountOpening: AccouuntOpeningReducer,
    bottomSheet: BottomSheetReducer,
});

const rootReducer = (state: any, action: any) => {
    if (action.type === 'RESET') {
        console.debug("rootReducer: Reset");
        return appReducer({
            ...state,
            account: {
                ...state.account,
                isDemoAccount: false
            },
            connection: undefined,
            trading: undefined,
            selection: undefined,
            state: {
                ...state.state,
                isShowLoading: false
            }
        }, action)
    }

    return appReducer(state, action)
}

export const persistedReducer = persistReducer<any, any>(persistConfig, rootReducer);

export type State = ReturnType<typeof rootReducer>;

export default rootReducer;