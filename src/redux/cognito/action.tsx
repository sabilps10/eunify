import { CognitoActionTypes, CognitoAction, CognitoState } from "./types";
import { Dispatch } from "redux";


export const SetLoginCredentials = (value: CognitoState) => {
    
    return (dispatch: Dispatch<CognitoAction>) => {
        console.debug('SetLoginCredentials')
        dispatch({
            type: CognitoActionTypes.SET_LOGIN,
            payload: value
        })
    }
}

export const SetCurrentToken = (value: string) => {
    
    return (dispatch: Dispatch<CognitoAction>) => {
        console.debug('SetCurrentToken')
        dispatch({
            type: CognitoActionTypes.SET_CURRENT_TOKEN,
            payload: value
        })
    }
}