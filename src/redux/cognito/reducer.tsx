import { CognitoActionTypes, CognitoAction, CognitoState } from "./types"

const INITIAL_STATE: CognitoState = {
    username: "",
    password: "",
    currentToken: "",
    cognito_username: '',
}

const LoginCredentialsReducer = (state = INITIAL_STATE, action: CognitoAction) => {
    switch (action.type) {
        case CognitoActionTypes.SET_LOGIN:
            return {
                ...state,
                ...action.payload
            }
        case CognitoActionTypes.SET_CURRENT_TOKEN:
            return {
                ...state,
                currentToken: action.payload
            }
        default:
            return state
    }
}

export default LoginCredentialsReducer;