// Store username and password using redux-persist-sensitive-storage
// Ref to https://reactnative.dev/docs/security#secure-storage
export type CognitoState = {
    username: string,
    password: string,
    currentToken: string
}

interface SetLoginCredentialsInterface {
    type: CognitoActionTypes.SET_LOGIN,
    payload: CognitoState,
    cognito_username: string;
}

interface SetCurrentTokenInterface {
    type: CognitoActionTypes.SET_CURRENT_TOKEN,
    payload: string
}

export type CognitoAction = SetLoginCredentialsInterface | SetCurrentTokenInterface;

export enum CognitoActionTypes {
    SET_LOGIN = 'SET_LOGIN',
    SET_CURRENT_TOKEN = 'SET_CURRENT_TOKEN'
}