export type OpenPositionsState = {
    positions: OpenPositionParam | null,
    profits: ProfitParam | null,
    profitsType: ProfitParam | null,
    symbols: SymbolParam | null,
    prices: PriceQuoteParam | null,
    orders: OrderParam | null,
    recommendList: (string)[],
    accountDetail: AccountDetailContent | null,
    accountDisplayName: string,
    historyDeal: HistoryDealParam | null,
    tradeSentiment: TradeSentimentParam | null,
    liveStat: LiveStatParam,
    lastOrderId: string | undefined,
    historyTransaction: HistoryTransactionParam | null,
    currentTradeSL: CurrentTradeSLContent,
    currentTradeTP: CurrentTradeTPContent,
    rejectMessage: ManualApprovalOrderRejectMessage | null
}

export type OpenPositionParam = {
    [ticket: number]: OpenPositionContent
}

export type ProfitParam = {
    [ticket: number]: number
}

export type OrderParam = {
    [ticket: number]: OrderContent
}

export type SymbolParam = {
    [symbol: string]: SymbolContent
}

export type PriceQuoteParam = {
    [symbol: string]: PriceQuoteContent
}

export type HistoryDealParam = {   
    [key: number]: HistoryDealContent
}

export type HistoryTransactionParam = {   
    [key: number]: HistoryTransactionContent
}

export type TradeSentimentParam = {   
    [symbol: string]: TraderSentimentContent
}

export type LiveStatParam = {   
    [symbol: string]: LiveStatContent[]
}

export type SymbolContent = {
    SymbolName: string;
    Path: string;
    SecurityTypeName: string;
    DisplayCategory: string;
    Point: number;
    Digits: number;
    TickSize: number;
    ContractSize: number;
    VolumeMin: number;
    VolumeMax: number;
    VolumeStep: number;
    CalcMode: number;
    MarginMode: number;
    StopsLevel: number;
    Type: number;
    TradingSessions?: TradingSessionsEntity[] | null;
    isTrading: number;
    SpreadDiff: number;
    Spread: number;
    SpreadBalance: number;
    Currency: string;
    MarginCurrency: string;
    SwapLong: number;
    SwapShort: number;
    TickValue: number;
    InitialMargin: number;
    MaintenanceMargin: number;
    Percentage: number;
    MarginDivider: number;
    TradeMode: number;
    ProfitConversionCurrency1: string;
    ProfitConversionCurrency2: string;
    MarginConversionCurrency1: string;
    MarginConversionCurrency2: string;
    SwapType: number
}

export type TradingSessionsEntity = {
    trade: {
        Open: number;
        Close: number;
    }[]
}


export type PriceQuoteContent = {
    Symbol: string;
    Bid: number;
    Ask: number;
    Time: number;
    Close: number;
}

export type OpenPositionContent = {
    Login: number;
    Ticket: number;
    Symbol: string;
    Volume: number;
    Type: number;
    OpenPrice: number;
    OpenTime: number;
    CurrentPrice: number;
    SL: number,
    TP: number,
    Profit: number;
    PositionID: number;
    Swap: number;
    Taxes: number;
    Commission: number;
    MarginRate: number;
}

export type OrderContent = {
    Login: number;
    Ticket: number;
    Symbol: string;
    Type: number;
    InitialVolume: number;
    RemainVolume: number;
    PriceOrder: number;
    SL: number,
    TP: number,
    // CurrentPrice: number;
    TimeSetup: number;
    TimeExpiration: number;
    TimeDone: number;
    State: number;
}

export type AccountDetailContent = {
    ConnID: string;
    Login: number;
    Group: string;
    Enable: boolean;
    TradeEnable: boolean;
    Leverage: number;
    Credit: number;
    Balance: number;
    MarginFree: number;
    Margin: number;
    Profit: number;
    Equity: number;
    MarginLevel: number;
    DepositCurrency: string;
    LiqMargin: number;
    MarginStopout: number;
    MarginType: number;
}

export type HistoryDealContent = {
    Symbol: string;
    Ticket: number;
    Type: number;
    OpenTime: number;
    CloseTime: number;
    Volume: number;
    Commission: number;
    Taxes: number;
    OpenPrice: number;
    ClosedPrice: number;
    Swaps: number;
    Profit: number;
    TP: number;
    SL: number;
}

export type HistoryTransactionContent = {
    Symbol: string;
    Ticket: number;
    Type: number;
    CloseTime: number;
    Swaps: number;
    Profit: number;
}

export type CurrentTradeSLContent = {
    StopLossPrice: number | undefined,
    StopLossPriceRef: number
}

export type CurrentTradeTPContent = {
    TakeProfitPrice: number,
    TakeProfitPriceRef: number
}

export type TraderSentimentContent = {
    Symbol: string,
    BuyPercentage: number;
    SellPercentage: number;
}

export type LiveStatContent = {
    Symbol: string,
    Type: number;
    PriceData: PriceData[];
}

export type PriceData = {
    Symbol: string, 
    High: number, 
    Low: number, 
    Time: number, 
    Open: number, 
    Close: number
}

export type ManualApprovalOrderRejectMessage = {
    Login: number,
    Symbol: string,
    Ticket: number,
    Type: number,
    Volume: number
}

enum OpenPositionsActionType {
    UPDATE_OPEN_POSITION_PL = "UPDATE_OPEN_POSITION_PL",
    SET_OPEN_POSITION = "SET_OPEN_POSITION",
    ADD_OPEN_POSITION = "ADD_OPEN_POSITION",
    DELETE_OPEN_POSITION = "DELETE_OPEN_POSITION",
    SET_SYMBOL_INFO = 'SET_SYMBOL_INFO',
    SET_PRICEQUOTE_INFO = 'SET_PRICEQUOTE_INFO',
    SET_INITPRICEQUOTE_INFO = 'SET_INITPRICEQUOTE_INFO',
    SET_ORDER= "SET_ORDER",
    ADD_ORDER = "ADD_ORDER",
    DELETE_ORDER = "DELETE_ORDER",
    SET_RECOMMENDLIST = "SET_RECOMMENDLIST",
    SET_WATCHLIST = "SET_WATCHLIST",
    SET_ACCOUNT_DETAIL = 'SET_ACCOUNT_DETAIL',
    SET_ACCOUNT_DISPLAY_NAME = 'SET_ACCOUNT_DISPLAY_NAME',
    SET_HISTORY_DEAL = 'SET_HISTORY_DEAL',
    ADD_HISTORY_DEAL = 'ADD_HISTORY_DEAL',
    SET_TRADER_SENTIMENT = 'SET_TRADER_SENTIMENT',
    SET_LIVE_STAT = 'SET_LIVE_STAT',
    SET_LAST_ORDER_ID = 'SET_LAST_ORDER_ID',
    SET_HISTORY_TRANSACTION = 'SET_HISTORY_TRANSACTION',
    ADD_HISTORY_TRANSACTION = 'ADD_HISTORY_TRANSACTION',
    SET_CURRENT_TRADE_SL = 'SET_CURRENT_TRADE_SL',
    SET_CURRENT_TRADE_TP = 'SET_CURRENT_TRADE_TP',
    SET_SYMBOLS_UPDATE = 'SET_SYMBOLS_UPDATE',
    SET_MANUAL_APPROVAL_ORDER_REJECT = 'SET_MANUAL_APPROVAL_ORDER_REJECT',
    UPDATE_ACCOUNT_DETAIL_INIT = 'UPDATE_ACCOUNT_DETAIL_INIT'
}

export default OpenPositionsActionType;

interface SetOpenPositionsInterface {
    type: OpenPositionsActionType.SET_OPEN_POSITION,
    payload: OpenPositionContent[]
}

interface AddOpenPositionsInterface {
    type: OpenPositionsActionType.ADD_OPEN_POSITION,
    payload: OpenPositionContent
}

interface DeleteOpenPositionsInterface {
    type: OpenPositionsActionType.DELETE_OPEN_POSITION,
    payload: number
}

interface UpdateOpenPositionsInterface {
    type: OpenPositionsActionType.UPDATE_OPEN_POSITION_PL,
    payload: {
        symbol: string,
        content: PriceQuoteContent
    }
}

interface SetSymbolInfoInterface {
    type: OpenPositionsActionType.SET_SYMBOL_INFO,
    payload: SymbolContent[]
}

interface SetPriceQuoteInterface {
    type: OpenPositionsActionType.SET_PRICEQUOTE_INFO,
    payload: {
        symbol: string,
        content: PriceQuoteContent
    }
}

interface SetInitPriceQuoteInterface {
    type: OpenPositionsActionType.SET_INITPRICEQUOTE_INFO,
    payload: {
        symbol: string,
        content: PriceQuoteContent
    }
}

interface SetOrdersInterface {
    type: OpenPositionsActionType.SET_ORDER,
    payload: OrderContent[]
}

interface AddOrdersInterface {
    type: OpenPositionsActionType.ADD_ORDER,
    payload: OrderContent
}

interface DeleteOrdersInterface {
    type: OpenPositionsActionType.DELETE_ORDER,
    payload: number
}

interface SetRecommendListInterface {
    type: OpenPositionsActionType.SET_RECOMMENDLIST,
    payload: string[]
}


interface SetAccountDetailInterface {
    type: OpenPositionsActionType.SET_ACCOUNT_DETAIL,
    payload: AccountDetailContent
}

interface SetAccountDisplayNameInterface {
    type: OpenPositionsActionType.SET_ACCOUNT_DISPLAY_NAME,
    payload: string
}

interface SetHistoryDealInterface {
    type: OpenPositionsActionType.SET_HISTORY_DEAL,
    payload: HistoryDealContent[]
}

interface AddHistoryDealInterface {
    type: OpenPositionsActionType.ADD_HISTORY_DEAL,
    payload: HistoryDealContent[]
}

interface SetTraderSentiment {
    type: OpenPositionsActionType.SET_TRADER_SENTIMENT,
    payload: TraderSentimentContent
}

interface SetLiveStat {
    type: OpenPositionsActionType.SET_LIVE_STAT,
    payload: LiveStatContent
}

interface SetLastOrderIdInterface {
    type: OpenPositionsActionType.SET_LAST_ORDER_ID,
    payload: string
}

interface SetHistoryTranasctionInterface {
    type: OpenPositionsActionType.SET_HISTORY_TRANSACTION,
    payload: HistoryTransactionContent[]
}

interface AddHistoryTransactionInterface {
    type: OpenPositionsActionType.ADD_HISTORY_TRANSACTION,
    payload: HistoryTransactionContent[]
}

interface SetCurrentTradeSLInterface {
    type: OpenPositionsActionType.SET_CURRENT_TRADE_SL,
    payload: CurrentTradeSLContent
}

interface SetCurrentTradeTPInterface {
    type: OpenPositionsActionType.SET_CURRENT_TRADE_TP,
    payload: CurrentTradeTPContent
}

interface SetSymbolsUpdateInterface {
    type: OpenPositionsActionType.SET_SYMBOLS_UPDATE,
    payload: SymbolContent[]
}

interface SetManualOrderApprovalInterface {
    type: OpenPositionsActionType.SET_MANUAL_APPROVAL_ORDER_REJECT,
    payload: ManualApprovalOrderRejectMessage
}

interface UpdateAccountDetailInitInterface {
    type: OpenPositionsActionType.UPDATE_ACCOUNT_DETAIL_INIT
}

export type OpenPositionsAction = SetOpenPositionsInterface | UpdateOpenPositionsInterface
    | AddOpenPositionsInterface | DeleteOpenPositionsInterface | SetSymbolInfoInterface
    | SetPriceQuoteInterface | SetOrdersInterface | AddOrdersInterface | DeleteOrdersInterface
    | SetRecommendListInterface | SetAccountDetailInterface | SetAccountDisplayNameInterface | SetHistoryDealInterface
    | SetTraderSentiment | SetLiveStat | SetLastOrderIdInterface | AddHistoryDealInterface | SetHistoryTranasctionInterface 
    | AddHistoryTransactionInterface | SetSymbolsUpdateInterface | SetCurrentTradeSLInterface | SetCurrentTradeTPInterface 
    | SetInitPriceQuoteInterface | SetManualOrderApprovalInterface | UpdateAccountDetailInitInterface;
