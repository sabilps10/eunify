import OpenPositionsActionType, { OpenPositionsState, OpenPositionsAction, OpenPositionContent, SymbolContent, PriceQuoteContent, PriceQuoteParam, OrderContent, AccountDetailContent, OpenPositionParam, HistoryDealContent, ProfitParam, HistoryTransactionContent } from "./type";

const INITIAL_STATE: OpenPositionsState = {
    positions: null,
    profits: null,
    profitsType: null,
    symbols: null,
    prices: null,
    orders: null,
    recommendList: [],
    accountDetail: null,
    accountDisplayName: '',
    historyDeal: null,
    tradeSentiment: null,
    liveStat: {},
    lastOrderId: undefined,
    historyTransaction: null,
    currentTradeSL: {
        StopLossPrice: -1,
        StopLossPriceRef: -1
    },
    currentTradeTP: {
        TakeProfitPrice: -1,
        TakeProfitPriceRef: -1
    },
    rejectMessage: null
}

function calculatePL(position: OpenPositionContent, symbol: SymbolContent, prices: PriceQuoteParam, profit: number, depositCurrency: string): number {
    if (position.Type !== 0 && position.Type !== 1) {
        return profit;
    }
    else if (position.Type === 0 && prices[position.Symbol].Bid === position.CurrentPrice)
    {
        return profit;
    }
    else if (position.Type === 1 && prices[position.Symbol].Ask === position.CurrentPrice)
    {
        return profit;
    }
    
    const group = symbol.SymbolName.indexOf('.') > 0 ? symbol.SymbolName.substring(symbol.SymbolName.indexOf('.'), symbol.SymbolName.length) : "";

    if (symbol.CalcMode == 0) {
        // Forex
        // Buy deals, Normalize(Close price  * Contract size * Volume in lots) — Normalize(Open price * Contract size * Volume in lots)
        // Sell deal, Normalize(Open price  * Contract size * Volume in lots) — Normalize(Close price * Contract size * Volume in lots)
        if (symbol.SymbolName.length >= 6) {
            const first = symbol.SymbolName.substring(0, 3);
            const second = symbol.SymbolName.substring(3, 6);
            if (position.Type === 0) {
                if (depositCurrency === second) {
                    return (prices[position.Symbol].Bid - position.OpenPrice) * symbol.ContractSize * position.Volume;
                }
                else if (depositCurrency === first) {
                    return (prices[position.Symbol].Bid - position.OpenPrice) / prices[position.Symbol].Bid * symbol.ContractSize * position.Volume;
                }
                else {
                    const tempSymbol1 = depositCurrency + second + group;
                    const tempSymbol2 = second + depositCurrency + group;

                    if (prices[tempSymbol1] && prices[tempSymbol1].Bid) {
                        return (prices[position.Symbol].Bid - position.OpenPrice) / prices[tempSymbol1].Bid * symbol.ContractSize * position.Volume;
                    }
                    else if (prices[tempSymbol2] && prices[tempSymbol2].Bid) {
                        return (prices[position.Symbol].Bid - position.OpenPrice) * prices[tempSymbol2].Bid * symbol.ContractSize * position.Volume;
                    }
                }
            }
            else {
                if (depositCurrency === second) {
                    return (position.OpenPrice - prices[position.Symbol].Ask) * symbol.ContractSize * position.Volume;
                }
                else if (depositCurrency === first) {
                    return (position.OpenPrice - prices[position.Symbol].Ask) / prices[position.Symbol].Ask * symbol.ContractSize * position.Volume;
                }
                else {
                    const tempSymbol1 = depositCurrency + second + group;
                    const tempSymbol2 = second + depositCurrency + group;

                    if (prices[tempSymbol1] && prices[tempSymbol1].Ask) {
                        return (position.OpenPrice - prices[position.Symbol].Ask) / prices[tempSymbol1].Ask * symbol.ContractSize * position.Volume;
                    }
                    else if (prices[tempSymbol2] && prices[tempSymbol2].Ask) {
                        return (position.OpenPrice - prices[position.Symbol].Ask) * prices[tempSymbol2].Ask * symbol.ContractSize * position.Volume;
                    }
                }
            }
        }
    } 
    else if (symbol.CalcMode == 1) {
        if (position.Type == 0) {
            let profit = (prices[position.Symbol].Bid - position.OpenPrice) * symbol.ContractSize * position.Volume;
            if (depositCurrency === symbol.Currency) {
                return profit;
            }
            else {
                const tempSymbol1 = depositCurrency + symbol.Currency;
                const tempSymbol2 = symbol.Currency + depositCurrency;
                if (prices[tempSymbol1] && prices[tempSymbol1].Bid) {
                    return profit / prices[tempSymbol1].Bid;
                }
                else if (prices[tempSymbol2] && prices[tempSymbol2].Bid) {
                    return profit * prices[tempSymbol2].Bid;
                }
            }
        } else {
            let profit = (position.OpenPrice - prices[position.Symbol].Ask) * symbol.ContractSize * position.Volume;
            if (depositCurrency === symbol.Currency) {
                return profit;
            }
            else {
                const tempSymbol1 = depositCurrency + symbol.Currency;
                const tempSymbol2 = symbol.Currency + depositCurrency;
                if (prices[tempSymbol1] && prices[tempSymbol1].Bid) {
                    return profit / prices[tempSymbol1].Bid;
                }
                else if (prices[tempSymbol2] && prices[tempSymbol2].Bid) {
                    return profit * prices[tempSymbol2].Bid;
                }
            }
        }
    }
    else if (symbol.CalcMode == 2) {
        if (position.Type == 0) {
            let profit = (prices[position.Symbol].Bid - position.OpenPrice) * symbol.TickValue / symbol.TickSize * position.Volume;
            if (depositCurrency === symbol.Currency) {
                return profit;
            }
            else {
                const tempSymbol1 = depositCurrency + symbol.Currency;
                const tempSymbol2 = symbol.Currency + depositCurrency;
                if (prices[tempSymbol1] && prices[tempSymbol1].Bid) {
                    return profit / prices[tempSymbol1].Bid;
                }
                else if (prices[tempSymbol2] && prices[tempSymbol2].Bid) {
                    return profit * prices[tempSymbol2].Bid;
                }
            }
        } else {
            let profit = (position.OpenPrice - prices[position.Symbol].Ask) * symbol.TickValue / symbol.TickSize * position.Volume;
            if (depositCurrency === symbol.Currency) {
                return profit;
            }
            else {
                const tempSymbol1 = depositCurrency + symbol.Currency;
                const tempSymbol2 = symbol.Currency + depositCurrency;
                if (prices[tempSymbol1] && prices[tempSymbol1].Bid) {
                    return profit / prices[tempSymbol1].Bid;
                }
                else if (prices[tempSymbol2] && prices[tempSymbol2].Bid) {
                    return profit * prices[tempSymbol2].Bid;
                }
            }
        }
    }
    return profit;
}


const OpenPositionsReducer = (state = INITIAL_STATE, action: OpenPositionsAction) => {

    switch (action.type) {

        case OpenPositionsActionType.UPDATE_ACCOUNT_DETAIL_INIT:
            let accountDetailState1 = state.accountDetail;
            if (state.accountDetail != null && state.positions != null) {
                const profit = Object.values(state.positions).reduce((previousValue, current) => previousValue + (state.profits[current.Ticket] ?? 0) + current.Commission + current.Taxes + current.Swap, 0);
                const equity = state.accountDetail.Balance + profit+ state.accountDetail.Credit;
                accountDetailState1 = {
                    ...state.accountDetail,
                    Profit: profit,
                    Equity: equity,
                    MarginFree: equity - state.accountDetail.Margin,
                    LiqMargin: state.accountDetail.Margin > 0 ? (state.accountDetail.MarginType === 0 ? state.accountDetail.Margin * state.accountDetail.MarginStopout/100 : state.accountDetail.MarginStopout) : 0,
                    MarginLevel: state.accountDetail.Margin !== 0 ? equity / state.accountDetail.Margin * 100 : 0
                }
            }

            return {
                ...state,
                accountDetail: accountDetailState1
            }

        case OpenPositionsActionType.UPDATE_OPEN_POSITION_PL:
            var updatedProfits: { [ticket: number]: number } = {};
            var updatedProfitsType: { [ticket: number]: number } = {};

            if (state.positions != null) {
                Object.values(state.positions).forEach(position => {
                    if (position.Symbol === action.payload.symbol) {
                        // let profit = position.Profit;
                        let profit = position.Profit;

                        if (state.profits && state.profits[position.Ticket])
                        {
                            profit = state.profits[position.Ticket]
                            if (state.symbols && state.symbols[position.Symbol] && state.prices) {
                                var depositCurrency = state.accountDetail?.DepositCurrency ?? 'USD'
                                profit = Math.round(calculatePL(position, state.symbols[position.Symbol], state.prices, profit, depositCurrency) * 100) / 100;
                            }
                        }

                        updatedProfits = {
                            ...updatedProfits,
                            [position.Ticket]: profit
                        }
                        updatedProfitsType = {
                            ...updatedProfitsType,
                            [position.Ticket]: profit > 0 ? 1 : (profit < 0 ? -1 : 0)
                        }
                    }
                    //console.debug(`positions: ${x.Ticket}, Symbol: ${x.Symbol}, Profit: ${x.Profit}, Swap: ${x.Swap}, Taxes: ${x.Taxes}, Commission: ${x.Commission}`);
                });
            }

            let accountDetailState = state.accountDetail;
            if (state.accountDetail != null && state.positions != null && Object.keys(updatedProfits).length > 0) {
                const profit = Object.values(state.positions).reduce((previousValue, current) => previousValue + (updatedProfits[current.Ticket] ?? (state.profits[current.Ticket] ?? 0)) + current.Commission + current.Taxes + current.Swap, 0);
                const equity = state.accountDetail.Balance + profit+ state.accountDetail.Credit;
                accountDetailState = {
                    ...state.accountDetail,
                    Profit: profit,
                    Equity: equity,
                    MarginFree: equity - state.accountDetail.Margin,
                    LiqMargin: state.accountDetail.Margin > 0 ? (state.accountDetail.MarginType === 0 ? state.accountDetail.Margin * state.accountDetail.MarginStopout/100 : state.accountDetail.MarginStopout) : 0,
                    MarginLevel: state.accountDetail.Margin !== 0 ? equity / state.accountDetail.Margin * 100 : 0
                }
            }

            return {
                ...state,
                profits: {
                    ...state.profits,
                    ...updatedProfits
                },
                profitsType: {
                    ...state.profitsType,
                    ...updatedProfitsType
                },
                accountDetail: accountDetailState
            }


        case OpenPositionsActionType.ADD_OPEN_POSITION:
            console.debug("ADD_OPEN_POSITION")
            return {
                ...state,
                positions: {
                    ...state.positions,
                    [action.payload.Ticket]: action.payload
                }
            }

        case OpenPositionsActionType.DELETE_OPEN_POSITION:
            console.debug("DELETE_OPEN_POSITION")
            if (state.positions != null)
                delete state.positions[action.payload];

            return {
                ...state,
                positions: state.positions
            }

        case OpenPositionsActionType.SET_OPEN_POSITION:
            console.debug("SET_OPEN_POSITION")
            let tempPositions: { [key: number]: OpenPositionContent } = {};
            let tempProfits: { [key: number]: number } = {};
            let tempProfitsType: { [key: number]: number } = {};

            action.payload.forEach(x => {
                tempPositions[x.Ticket] = x;
                tempProfits[x.Ticket] = x.Profit;
                tempProfitsType[x.Ticket] = 0;
            });
            return {
                ...state,
                positions: tempPositions,
                profits: tempProfits,
                profitsType: tempProfitsType
            }
        case OpenPositionsActionType.SET_SYMBOL_INFO:
            console.debug("SET_SYMBOL_INFO");
            let tempSymbols: { [key: string]: SymbolContent } = {};

            action.payload.forEach(x => {
                tempSymbols[x.SymbolName] = x
            });

            return {
                ...state,
                symbols: tempSymbols
            }
        case OpenPositionsActionType.SET_PRICEQUOTE_INFO:
        case OpenPositionsActionType.SET_INITPRICEQUOTE_INFO:
            if (action.type === OpenPositionsActionType.SET_INITPRICEQUOTE_INFO)
            {
                if (state.prices && state.prices[action.payload.symbol] && state.prices[action.payload.symbol].Time >= action.payload.content.Time)
                {
                    return state;
                }
            }

            const symbol : SymbolContent = state.symbols[action.payload.symbol]
            if (symbol)
            {
                action.payload.content.Bid = action.payload.content.Bid - (symbol.Point * Math.floor((symbol.SpreadDiff) / 2))
                action.payload.content.Ask = action.payload.content.Ask + (symbol.Point * Math.floor((symbol.SpreadDiff + 1) / 2))
            }

            return {
                ...state,
                prices: {
                    ...state.prices,
                    [action.payload.symbol]: action.payload.content
                }
            }
        case OpenPositionsActionType.SET_ORDER:
            // return {
            //     ...state,
            //     orders: 
            //     {
            //         ...state.orders,
            //         [action.payload.Ticket] : action.payload
            //     }
            // }
            console.debug('SET_ORDER ', action.payload)
            let tempOrder: { [key: number]: OrderContent } = {};
            action.payload.forEach(x => {
                tempOrder[x.Ticket] = x;
            });
            return {
                ...state,
                orders: tempOrder
            }
            
        case OpenPositionsActionType.DELETE_ORDER:
            if (state.orders != null)
                delete state.orders[action.payload];

            return {
                ...state,
                orders: {
                    ...state.orders
                }
            }

        case OpenPositionsActionType.ADD_ORDER:
            console.debug('ADD_ORDER ', action.payload)
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [action.payload.Ticket]: action.payload
                }
            }
        case OpenPositionsActionType.SET_RECOMMENDLIST:
            return {
                ...state,
                recommendList: action.payload
            }
        case OpenPositionsActionType.SET_ACCOUNT_DETAIL:
            console.debug(`AccountReducer Set Account Detail: ${JSON.stringify(action.payload)}`);
            if (action.payload && state.positions != null) {
                const profit = Object.values(state.positions).reduce((previousValue, current) => previousValue + (state.profits[current.Ticket] ?? 0) + current.Commission + current.Taxes + current.Swap, 0);
                const equity = action.payload?.Balance + profit+ action.payload?.Credit;
                action.payload = {
                    ...action.payload,
                    Profit: profit,
                    Equity: equity,
                    MarginFree: equity - action.payload?.Margin,
                    LiqMargin: action.payload?.Margin > 0 ? (action.payload?.MarginType === 0 ? action.payload?.Margin * action.payload?.MarginStopout/100 : action.payload?.MarginStopout) : 0,
                    MarginLevel: action.payload?.Margin !== 0 ? equity / action.payload?.Margin * 100 : 0
                }
            }
            // action.payload = {
            //     ...action.payload,
            //     Login: Number(action.payload?.Login)
            // }
            return {
                ...state,
                accountDetail: action.payload
            }
        case OpenPositionsActionType.SET_ACCOUNT_DISPLAY_NAME:
            return {
                ...state,
                accountDisplayName: action.payload
            }
        case OpenPositionsActionType.SET_HISTORY_DEAL:
            let tempHistoryDeal: { [key: number]: HistoryDealContent } = {};

            for(var i = 0; i < action.payload.length; i++)
            {
                tempHistoryDeal[i] = action.payload[i]
            }
            //console.debug("SET_HISTORY_DEAL: " + Object.keys(tempHistoryDeal))
            return {
                ...state,
                historyDeal: tempHistoryDeal
            }

        case OpenPositionsActionType.ADD_HISTORY_DEAL:
            let tempHistoryDeal1: { [key: number]: HistoryDealContent } = {};

            let previousLength = Object.keys(state.historyDeal).length;
            for(var i = 0; i < action.payload.length; i++)
            {
                tempHistoryDeal1[previousLength + i] = action.payload[i]
            }
            return {
                ...state,
                historyDeal: {
                    ...state.historyDeal,
                    ...tempHistoryDeal1
                }
            }
        case OpenPositionsActionType.SET_HISTORY_TRANSACTION:
                let tempHistoryTrans: { [key: number]: HistoryTransactionContent } = {};
    
                for(var i = 0; i < action.payload.length; i++)
                {
                    tempHistoryTrans[i] = action.payload[i]
                }
                return {
                    ...state,
                    historyTransaction: tempHistoryTrans
                }
        case OpenPositionsActionType.ADD_HISTORY_TRANSACTION:
                let tempHistoryTrans1: { [key: number]: HistoryTransactionContent } = {};
    
                let previousTransLength = Object.keys(state.historyTransaction).length;
                for(var i = 0; i < action.payload.length; i++)
                {
                    tempHistoryTrans1[previousTransLength + i] = action.payload[i]
                }
                return {
                    ...state,
                    historyTransaction: {
                        ...state.historyTransaction,
                        ...tempHistoryTrans1
                    }
                }
        case OpenPositionsActionType.SET_TRADER_SENTIMENT:
            return {
                ...state,
                tradeSentiment: {
                    [action.payload.Symbol]: action.payload
                }
            }
        case OpenPositionsActionType.SET_LIVE_STAT:
            var live = [action.payload]
            if (state.liveStat && state.liveStat[action.payload.Symbol]) {
                live = [...state.liveStat[action.payload.Symbol], action.payload]
            }
            return {
                ...state,
                liveStat: {
                    [action.payload.Symbol]: live
                }
            }
        case OpenPositionsActionType.SET_LAST_ORDER_ID:
            return {
                ...state,
                lastOrderId: action.payload
            }
        case OpenPositionsActionType.SET_CURRENT_TRADE_SL:
            return {
                ...state,
                currentTradeSL: action.payload
            }
        case OpenPositionsActionType.SET_CURRENT_TRADE_TP:
            return {
                ...state,
                currentTradeTP: action.payload
            }
        case OpenPositionsActionType.SET_SYMBOLS_UPDATE:
            let updatedSymbols = Object.fromEntries(action.payload.map(x => [x.SymbolName, x]));

            return {
                ...state,
                symbol: Object.assign(state.symbols, updatedSymbols)
            }
        case OpenPositionsActionType.SET_MANUAL_APPROVAL_ORDER_REJECT:
            return {
                ...state,
                rejectMessage: action.payload
            }

        default:
            return state;
    }
}

export default OpenPositionsReducer;
