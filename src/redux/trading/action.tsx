import OpenPositionsActionType, {OpenPositionContent, PriceQuoteContent, SymbolContent, OpenPositionsAction, OrderContent, AccountDetailContent, HistoryDealContent, TraderSentimentContent, LiveStatContent, HistoryTransactionContent, CurrentTradeSLContent, CurrentTradeTPContent, ManualApprovalOrderRejectMessage} from "./type";
import { Dispatch } from "redux";


export const SetOpenPositions = (content: OpenPositionContent[]) => {
    console.debug("SET_OPEN_POSITION");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_OPEN_POSITION,
            payload: content
        })
    }
};

export const AddOpenPositions = (content: OpenPositionContent) => {
    console.debug("AddOpenPositions");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.ADD_OPEN_POSITION,
            payload: content
        })
    }
};

export const DeleteOpenPositions = (content: number) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.DELETE_OPEN_POSITION,
            payload: content
        })
    }
};

export const UpdateOpenPositionsPL = (symbol: string, content: PriceQuoteContent) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.UPDATE_OPEN_POSITION_PL,
            payload: {
                symbol: symbol,
                content: content
            }
        })
    }
};

export const SetSymbolInfo = (content: SymbolContent[]) => {
    console.debug("SetSymbolInfo");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_SYMBOL_INFO,
            payload: content
        })
    }
};

export const SetPriceQuoteInfo = (symbol: string, content: PriceQuoteContent) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_PRICEQUOTE_INFO,
            payload: {
                symbol: symbol,
                content: content
            }
        })
    }
};

export const SetInitPriceQuoteInfo = (symbol: string, content: PriceQuoteContent) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_INITPRICEQUOTE_INFO,
            payload: {
                symbol: symbol,
                content: content
            }
        })
    }
};

export const SetOrders = (content: OrderContent[]) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_ORDER,
            payload: content
        })
    }
};

export const DeleteOrders = (content: number) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.DELETE_ORDER,
            payload: content
        })
    }
};

export const AddOrders = (content: OrderContent) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.ADD_ORDER,
            payload: content
        })
    }
};

export const SetAccountDetail = (content: AccountDetailContent) => {
    console.debug(`[redux trading action SetAccountDetail] ${content}`)
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_ACCOUNT_DETAIL,
            payload: content
        })
    }
};

export const SetRecommendList = (content: string[]) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_RECOMMENDLIST,
            payload: content
        })
    }
};

export const SetAccountDisplayName = (content: string) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_ACCOUNT_DISPLAY_NAME,
            payload: content
        })
    }
};


export const SetHistoryDeal = (content: HistoryDealContent[]) => {
    console.debug("SET_HISTORY_DEAL");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_HISTORY_DEAL,
            payload: content
        })
    }
};

export const AddHistoryDeal = (content: HistoryDealContent[]) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.ADD_HISTORY_DEAL,
            payload: content
        })
    }
};

export const SetHistoryTransaction = (content: HistoryTransactionContent[]) => {
    console.debug("SET_HISTORY_TRANS");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_HISTORY_TRANSACTION,
            payload: content
        })
    }
};

export const AddHistoryTransaction = (content: HistoryTransactionContent[]) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.ADD_HISTORY_TRANSACTION,
            payload: content
        })
    }
};

export const SetTraderSentiment = (content: TraderSentimentContent) => {
    console.debug("SetTraderSentiment");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_TRADER_SENTIMENT,
            payload: content
        })
    }
};

export const SetLiveStat = (content: LiveStatContent) => {
    console.debug("SetLiveStat");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_LIVE_STAT,
            payload: content
        })
    }
};

export const SetLastOrderId = (content: string | undefined) => {
    console.debug("SetLastOrderId");
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_LAST_ORDER_ID,
            payload: content
        })
    }
};

export const SetCurrentTradeSL = (content: CurrentTradeSLContent) => {
    // console.debug("SetCurrentTradeSL", content);
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_CURRENT_TRADE_SL,
            payload: content
        })
    }
};

export const SetCurrentTradeTP = (content: CurrentTradeTPContent) => {
    // console.debug("SetCurrentTradeTP", content);
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_CURRENT_TRADE_TP,
            payload: content
        })
    }
};

export const SetSymbolsUpdate = (content: SymbolContent[]) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_SYMBOLS_UPDATE,
            payload: content
        })
    }
}

export const SetManualApprovalOrderRejecteUpdate = (content: ManualApprovalOrderRejectMessage) => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.SET_MANUAL_APPROVAL_ORDER_REJECT,
            payload: content
        })
    }
}

export const UpdateAccountDetailInit = () => {
    return (dispatch: Dispatch<OpenPositionsAction>) => {
        dispatch({
            type: OpenPositionsActionType.UPDATE_ACCOUNT_DETAIL_INIT
        })
    }
}