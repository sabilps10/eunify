// eslint-disable-next-line @typescript-eslint/no-unused-vars
import BottomSheetType, {BottomSheetAction, INITIAL_STATE_TYPE} from './type';

const INITIAL_STATE: INITIAL_STATE_TYPE = {
  isOpen: false,
  options: [],
  label: '',
  selected: '',
  onSelect: null,
};

const BottomSheetReducer = (
  state = INITIAL_STATE,
  action: BottomSheetAction,
) => {
  switch (action.type) {
    case BottomSheetType.OPEN_BOTTOM_SHEET:
      return action.payload;
    case BottomSheetType.CLOSE_BOTTOM_SHEET:
      return {
        isOpen: false,
        options: [],
        label: '',
        selected: '',
        onSelect: null,
      };
    case BottomSheetType.SET_SELECTED:
      return {
        ...state,
        selected: action.payload,
      };
    default:
      return state;
  }
};

export default BottomSheetReducer;
