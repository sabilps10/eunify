export type INITIAL_STATE_TYPE = {
  isOpen: boolean;
  options: any[];
  label: string;
  selected: string;
  onSelect: (val: any) => void | null;
};

enum BottomSheetType {
  OPEN_BOTTOM_SHEET = 'OPEN_BOTTOM_SHEET',
  CLOSE_BOTTOM_SHEET = 'CLOSE_BOTTOM_SHEET',
  SET_SELECTED = 'SET_SELECTED',
}

export default BottomSheetType;

type OpenBottomSheet = {
  type: BottomSheetType.OPEN_BOTTOM_SHEET;
  payload: INITIAL_STATE_TYPE;
};

type CloseBottomSheet = {
  type: BottomSheetType.CLOSE_BOTTOM_SHEET;
};

type SetSelected = {
  type: BottomSheetType.SET_SELECTED;
  payload: string;
};

export type BottomSheetAction =
  | OpenBottomSheet
  | CloseBottomSheet
  | SetSelected;
