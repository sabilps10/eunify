/* eslint-disable @typescript-eslint/no-unused-vars */
import BottomSheetType, {BottomSheetAction, INITIAL_STATE_TYPE} from './type';
import {Dispatch} from 'redux';

export const OpenBottomSheet = (value: INITIAL_STATE_TYPE) => {
  return (dispatch: Dispatch<BottomSheetAction>) => {
    dispatch({
      type: BottomSheetType.OPEN_BOTTOM_SHEET,
      payload: value,
    });
  };
};

export const CloseBottomSheet = () => {
  return (dispatch: Dispatch<BottomSheetAction>) => {
    dispatch({
      type: BottomSheetType.CLOSE_BOTTOM_SHEET,
    });
  };
};

export const SetSelected = (value: string) => {
  return (dispatch: Dispatch<BottomSheetAction>) => {
    dispatch({
      type: BottomSheetType.SET_SELECTED,
      payload: value,
    });
  };
};
