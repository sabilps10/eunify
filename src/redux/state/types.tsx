import { ReactElement } from "react"
import { TextStyle } from "react-native"
import { OrderActionType } from "../../utils/orderUtils"

export type SettingState = {
    userId?: string,                    // Cognito user uuid
    displayName?: string,               // Display user name
    setting: SettingContent,
    isShowLoading: boolean,
    isShowProgress: ProgressDialogContent,
    isShowAddDevice: boolean,
    isShowMessageDialog: MessageDialogContent,
    isShowToastDialog: ToastDialogContent,
    isShowOrderConfirmDialog: OrderConfirmDialogContent,
    myList: string[] | null,
    regionDataList: RegionContent[],
    countryList: CountryContent[],
    chartSetting: ChartSettingParam | null,  
    portfolioCurrentTab: string,
    onTouch: boolean,
    isManualLogout: boolean,
    allowTrack: boolean,
    ga4UserPropertyUUID?: string
}

export type ChartSettingParam = {
    [symbol: string]: string
}

export type SettingContent = {
    UserLanguage?: string,
    IsIOSLocaleFixed?: boolean,
    IsAndroidLocaleFixed?: boolean,
    UserRegion?: string,
    PushyDeviceID?: string,
    TradeSetting?: {[key: string]: TradeSetting},
    Theme?: string,
    IsEnableBiometrics?: string
    IsFirstTime?: boolean,
    TimeZone?: string,
    IsSkipRegBiometric?: boolean
}

export type TradeSetting = {
    IsEnableTradeConfirmationDialog: boolean
}

export type ProgressDialogContent = {
    isShowDialog: boolean,
    progress: number,
    total: number
}

export type MessageDialogContent = {
    isShowDialog: boolean,
    title: string,
    titleStyle?: TextStyle,
    message: string,
    messageStyle?: TextStyle,
    linkMessage?: string,
    linkMessageCallBack?: Function,
    isShowBtn?: boolean,
    isShowCancel?: boolean,
    cancelCallback?: Function,
    callback?: Function,
    btnLabel?: string
    alternateStyle?: boolean,
    detailComponent?: ReactElement<any>,
    trackingContext?: MessageDialogContent.TrackingContext
}

export namespace MessageDialogContent {
    export enum TrackingContext {
        ActivateDemoTrade = "MessageDialog+ActivateDemoTrade",
        ConfirmLogout = "MessageDialog+ConfirmLogout",
        EnableBiometricNotAvailable = "MessageDialog+EnableBiometricNotAvailable"
    }
}

export type ToastDialogContent = {
    isShowDialog: boolean,
    message: string,
    isSuccess?: boolean
}

export type RegionContent = {
    regionCode: string,
    countryCode: string,
    countrySymbol: string
}

export type CountryContent = {
  id: string;
  nameEn: string;
  nameSc: string;
  nameTc: string;
  value: string;
  code: string;
  needEdd: string | boolean | null;
};

interface ProfitTypeInterface {
    type: number,
    // slValue: number,
    // tpValue: number
}

export type OrderConfirmDialogContent = {
    isShowDialog: boolean,
    type: OrderActionType,
    contractCode: string,
    isSelectBuy: boolean,
    volume: number,
    price: number,
    isPending: boolean,
    pendingOrderType?: number,
    pendingOrderValidity?: number,
    isProfit: boolean,
    stopLossPrice: number, 
    limitProfitPrice: number, 
    transactionId?: number,
    callback?: Function,
    profitType?: ProfitTypeInterface | undefined
}

interface SetSettingInterface {
    type: StateActionTypes.SET_SETTING,
    payload: SettingContent
}

interface SetIsShowLoadingInterface {
    type: StateActionTypes.SET_IS_SHOW_LOADING,
    payload: boolean
}

interface SetIsShowProgressInterface {
    type: StateActionTypes.SET_IS_SHOW_PROGRESS,
    payload: ProgressDialogContent
}

interface SetIsShowAddDeviceInterface {
    type: StateActionTypes.SET_IS_SHOW_ADD_DEVICE,
    payload: boolean
}

interface SetIsShowMessageDialogInterface {
    type: StateActionTypes.SET_IS_SHOW_MESSAGE_DIALOG,
    payload: MessageDialogContent
}

interface SetIsShowToastDialogInterface {
    type: StateActionTypes.SET_IS_SHOW_TOAST_DIALOG,
    payload: ToastDialogContent
}

interface SetIsShowOrderConfirmDialogInterface {
    type: StateActionTypes.SET_IS_SHOW_ORDER_CONFIRM_DIALOG,
    payload: OrderConfirmDialogContent
}

interface SetMyListInterface {
    type: StateActionTypes.SET_MYLIST,
    payload: string[]
}

interface SetUserIdInterface {
    type: StateActionTypes.SET_USER_ID,
    payload: string
}

interface SetRegionListInterface {
    type: StateActionTypes.SET_REGION_LIST,
    payload: RegionContent[]
}

interface SetCountryListInterface {
  type: StateActionTypes.SET_COUNTRY_LIST;
  payload: CountryContent[];
}

interface SetChartSettinghInterface {
    type: StateActionTypes.SET_CHART_SETTING,
    payload: {
        symbol: string,
        content: string
    }
}

interface SetPortfolioCurrentTabInterface {
    type: StateActionTypes.SET_PORTFOLIO_CURRENT_TAB,
    payload: string
}

interface SetOnTouchInterface {
    type: StateActionTypes.SET_ONTOUCH
}

interface SetIsManualLogoutInterface {
    type: StateActionTypes.SET_IS_MANUAL_LOGOUT,
    payload: boolean
}

interface SetAllowTrackInterface {
    type: StateActionTypes.SET_ALLOW_TRACK,
    payload: boolean
}

interface SetGA4UserPropertyUUIDInterface {
    type: StateActionTypes.SET_GA4_USER_PROPERTY_UUID,
    payload: string
}

export type StateAction = SetSettingInterface | SetIsShowLoadingInterface | SetIsShowAddDeviceInterface | SetMyListInterface | SetIsShowMessageDialogInterface | SetIsShowToastDialogInterface | SetUserIdInterface | SetRegionListInterface | SetIsShowProgressInterface | SetIsShowOrderConfirmDialogInterface | SetChartSettinghInterface | SetPortfolioCurrentTabInterface | SetOnTouchInterface | SetIsManualLogoutInterface | SetAllowTrackInterface | SetGA4UserPropertyUUIDInterface | SetCountryListInterface;

export enum StateActionTypes {
    SET_SETTING = 'SET_SETTING',
    SET_IS_SHOW_LOADING = 'SET_IS_SHOW_LOADING',
    SET_IS_SHOW_PROGRESS = 'SET_IS_SHOW_PROGRESS',
    SET_IS_SHOW_ADD_DEVICE = 'SET_IS_SHOW_ADD_DEVICE',
    SET_IS_SHOW_MESSAGE_DIALOG = 'SET_IS_SHOW_MESSAGE_DIALOG',
    SET_IS_SHOW_TOAST_DIALOG = 'SET_IS_SHOW_TOAST_DIALOG',
    SET_IS_SHOW_ORDER_CONFIRM_DIALOG = 'SET_IS_SHOW_ORDER_CONFIRM_DIALOG',
    SET_CURRENT_TAB = 'SET_CURRENT_TAB',
    SET_MYLIST = 'SET_MYLIST',
    SET_USER_ID = 'SET_USER_ID',
    SET_REGION_LIST = 'SET_REGION_LIST',
    SET_COUNTRY_LIST = 'SET_COUNTRY_LIST',
    SET_CHART_SETTING = 'SET_CHART_SETTING',
    SET_PORTFOLIO_CURRENT_TAB = 'SET_PORTFOLIO_CURRENT_TAB',
    SET_ONTOUCH = 'SET_ONTOUCH',
    SET_IS_MANUAL_LOGOUT = 'SET_IS_MANUAL_LOGOUT',
    SET_ALLOW_TRACK = 'SET_ALLOW_TRACK',
    SET_GA4_USER_PROPERTY_UUID = 'SET_GA4_USER_PROPERTY_UUID'
}

export enum TabType {
    DASHBOARD = 'DASHBOARD',
    WATCH_LIST = 'WATCH_LIST',
    COMMUNITY = 'COMMUNITY',
    PORTFOLIO = 'PORTFOLIO',
    ME = 'ME'
}

export enum PortfolioTabType {
    Position = 'Tab_openPositions',
    Order = 'Tab_limitOrders',
    History = 'Tab_historicalPL'
}