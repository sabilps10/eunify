import { StateActionTypes, StateAction, SettingState, TabType, PortfolioTabType } from "./types"

const INITIAL_STATE: SettingState = {
    setting: {
        UserLanguage: undefined,
        TradeSetting: {},
        Theme: 'light',
        IsEnableBiometrics: undefined,
        IsFirstTime: true,
        IsSkipRegBiometric: false
    },
    isShowLoading: false,
    isShowProgress: {
        isShowDialog: false,
        progress: 0,
        total: 0
    },
    isShowAddDevice: false,
    isShowMessageDialog: {
        isShowDialog: false,
        title: '',
        message: ''
    },
    isShowToastDialog: {
        isShowDialog: false,
        message: '',
        isSuccess: true
    },
    isShowOrderConfirmDialog: {
        isShowDialog: false,
        type: null, 
        contractCode: '', 
        isSelectBuy: false, 
        volume: 0, 
        price: 0,  
        isPending: false, 
        isProfit: false, 
        stopLossPrice: 0, 
        limitProfitPrice: 0, 
    },
    myList: null,
    regionDataList: [],
    countryList: [],
    chartSetting: undefined,
    portfolioCurrentTab: PortfolioTabType.Position,
    onTouch: false,
    isManualLogout: false,
    ga4UserPropertyUUID: ''
}

const StateReducer = (state = INITIAL_STATE, action: StateAction) => {
    switch (action.type) {
        case StateActionTypes.SET_SETTING:
            return {
                ...state,
                setting: {
                    ...state.setting,
                    ...action.payload
                }
            }
        case StateActionTypes.SET_IS_SHOW_LOADING:
            return {
                ...state,
                isShowLoading: action.payload
            }
        case StateActionTypes.SET_IS_SHOW_PROGRESS:
            return {
                ...state,
                isShowProgress: action.payload
            }
        case StateActionTypes.SET_IS_SHOW_ADD_DEVICE:
            return {
                ...state,
                isShowAddDevice: action.payload
            }
        case StateActionTypes.SET_IS_SHOW_MESSAGE_DIALOG:
            return {
                ...state,
                isShowMessageDialog: action.payload
            }
        case StateActionTypes.SET_IS_SHOW_TOAST_DIALOG:
            return {
                ...state,
                isShowToastDialog: action.payload
            }
        case StateActionTypes.SET_IS_SHOW_ORDER_CONFIRM_DIALOG:
            return {
                ...state,
                isShowOrderConfirmDialog: action.payload
            }
        case StateActionTypes.SET_MYLIST:
            return {
                ...state,
                myList: action.payload
            }
        case StateActionTypes.SET_USER_ID:
            return {
                ...state,
                userId: action.payload
            }
        case StateActionTypes.SET_REGION_LIST:
            return {
                ...state,
                regionDataList: action.payload
            }
        case StateActionTypes.SET_COUNTRY_LIST:
            return {
                ...state,
                countryList: action.payload
            }
        case StateActionTypes.SET_CHART_SETTING:
            return {
                ...state,
                chartSetting: {
                    ...state.chartSetting,
                    [action.payload.symbol]: action.payload.content
                }
            }
        case StateActionTypes.SET_PORTFOLIO_CURRENT_TAB:
            return {
                ...state,
                portfolioCurrentTab: action.payload
            }
        case StateActionTypes.SET_ONTOUCH:
            return {
                ...state,
                onTouch: !state.onTouch
            }
        case StateActionTypes.SET_IS_MANUAL_LOGOUT:
            return {
                ...state,
                isManualLogout: action.payload
            }
        case StateActionTypes.SET_ALLOW_TRACK:
            return {
                ...state,
                allowTrack: action.payload
            }
        case StateActionTypes.SET_GA4_USER_PROPERTY_UUID:
            return {
                ...state,
                ga4UserPropertyUUID: action.payload
            }    
        default:
            return state
    }
}

export default StateReducer;