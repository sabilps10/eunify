import { StateActionTypes, StateAction, SettingState, SettingContent, TabType, MessageDialogContent, ToastDialogContent, ProgressDialogContent, OrderConfirmDialogContent, RegionContent, ChartSettingParam, CountryContent } from "./types";
import { Dispatch } from "redux";


export const SetSetting = (value: SettingContent) => {
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_SETTING,
            payload: value
        })
    }
}

export const SetIsShowLoading = (value: boolean) => {
    console.debug('SetIsShowLoading: ' + value)
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_LOADING,
            payload: value
        })
    }
}

export const SetIsShowProgress = (value: ProgressDialogContent) => {
    console.debug('SetIsShowProgress')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_PROGRESS,
            payload: value
        })
    }
}

export const SetIsShowAddDevice = (value: boolean) => {
    console.debug('SetIsShowAddDevice')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_ADD_DEVICE,
            payload: value
        })
    }
}

export const SetIsShowMessageDialog = (value: MessageDialogContent) => {
    console.debug('setIsShowMessageDialog: ' + JSON.stringify(value))
    return (dispatch: Dispatch<StateAction>) => {
        if (value.isShowDialog) {
            //console.debug('setIsShowMessageDialog set false')
            dispatch({
                type: StateActionTypes.SET_IS_SHOW_MESSAGE_DIALOG,
                payload: { isShowDialog: false, title: '', message: '' }
            })
        }
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_MESSAGE_DIALOG,
            payload: value
        })
    }
}

export const SetIsShowToastDialog = (value: ToastDialogContent) => {
    console.debug('SetIsShowToastDialog')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_TOAST_DIALOG,
            payload: value
        })
    }
}

export const SetIsShowOrderConfirmDialog = (value: OrderConfirmDialogContent) => {
    console.debug('SetIsShowOrderConfirmDialog')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_SHOW_ORDER_CONFIRM_DIALOG,
            payload: value
        })
    }
}

export const SetMyList = (content: string[]) => {
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_MYLIST,
            payload: content
        })
    }
};

export const SetUserId = (value: string) => {
    console.debug('SetUserId')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_USER_ID,
            payload: value
        })
    }
}

export const SetRegionList = (value: RegionContent[]) => {
    console.debug('SetRegionList')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_REGION_LIST,
            payload: value
        })
    }
}

export const SetChartSetting = (symbol: string, content: string) => {
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_CHART_SETTING,
            payload: {
                symbol: symbol,
                content: content
            }
        })
    }
}

export const SetPortfolioCurrentTab = (value: string) => {
    console.debug('SetPortfolioCurrentTab')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_PORTFOLIO_CURRENT_TAB,
            payload: value
        })
    }
}

export const SetOnTouch = () => {
    console.debug('SetOnTouch')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_ONTOUCH
        })
    }
}

export const SetIsManualLogout = (value: boolean) => {
    console.debug('SetIsManualLogout')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_IS_MANUAL_LOGOUT,
            payload: value
        })
    }
}

export const SetAllowTrack = (value: boolean) => {
    console.debug('SetAllowTrack')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_ALLOW_TRACK,
            payload: value
        })
    }
}

export const SetGA4UserPropertyUUID = (value: string) => {
    console.debug('SetGA4UserPropertyUUID')
    return (dispatch: Dispatch<StateAction>) => {
        dispatch({
            type: StateActionTypes.SET_GA4_USER_PROPERTY_UUID,
            payload: value
        })
    }
}

export const SetCountryList = (value: CountryContent[]) => {
  console.debug('SetCountryList');
  return (dispatch: Dispatch<StateAction>) => {
    dispatch({
      type: StateActionTypes.SET_COUNTRY_LIST,
      payload: value,
    });
  };
};