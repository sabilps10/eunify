import AccountActionType, {AccountState, AccountAction, TradingAccount, MIO} from "./type";
import { Dispatch } from "redux";
import { BottomTabParamList } from "../../routes/route";
import { DeviceInterface } from "./reducer";


export const SetAccountInfo = (value: AccountState) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetAccountInfo");
        dispatch({
            type: AccountActionType.SET_ACCOUNT_INFO,
            payload: value
        })
    }
};

export const SetTradingAccountListInfo = (value: TradingAccount[]) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetTradingAccountListInfo");
        dispatch({
            type: AccountActionType.SET_TRADING_ACCOUNT_LIST,
            payload: value
        })
    }
};

export const SetDemoTradingAccountInfo = (value: TradingAccount) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetDemoTradingAccountInfo");
        dispatch({
            type: AccountActionType.SET_DEMO_TRADING_ACCOUNT,
            payload: value
        })
    }
};

export const SetIsDemoAccount = (value: boolean) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetIsDemoAccount");
        dispatch({
            type: AccountActionType.SET_IS_DEMO,
            payload: value
        })
    }
};

export const SetEnterDemoTab = (value: keyof BottomTabParamList) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetEnterDemoTab");
        dispatch({
            type: AccountActionType.SET_ENTER_DEMO_TAB,
            payload: value
        })
    }
};

export const Reset = () => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("RESET");
        dispatch({
            type: AccountActionType.RESET,
        })
    }
};

export const ResetAccount = () => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("RESET_Account");
        dispatch({
            type: AccountActionType.RESET_ACCOUNT,
        })
    }
};

export const SetConnectedAccount = (value: string | undefined) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetConnectedAccount");
        dispatch({
            type: AccountActionType.SET_CONNECTED_ACCOUNT,
            payload: value
        })
    }
};

export const SetApprovedDevicesList = (value: (DeviceInterface | undefined)[]) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetApprovedDevicesList");
        dispatch({
            type: AccountActionType.SET_APPROVED_DEVICE_LIST,
            payload: value
        })
    }
};

export const SetIsLogined = (value: boolean) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetIsLogined");
        dispatch({
            type: AccountActionType.SET_IS_LOGINED,
            payload: value
        })
    }
};

export const SetMIO = (value: MIO) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetMIO");
        dispatch({
            type: AccountActionType.SET_MIO,
            payload: value
        })
    }
};

export const SetIsHeaderExpanded = (value: boolean) => {
    return (dispatch: Dispatch<AccountAction>) => {
        console.debug("SetIsHeaderExpanded");
        dispatch({
            type: AccountActionType.SET_IS_HEADER_EXPANDED,
            payload: value
        })
    }
};