import { StringMap } from "i18next";
import { BottomTabParamList, RootStackParamList } from "../../routes/route";
import { TabDirectoryTab } from "../../views/tabDirectory/tabDirectory";
import AccountActionType, {AccountState, AccountAction, TradingAccount, MIO} from "./type";

export type INITIAL_STATE_TYPE = {
    //data
    account: AccountState | null,
    isDemoAccount: boolean,
    enterDemoTab: keyof BottomTabParamList,
    demoTradingAccount: TradingAccount | null,
    tradingAccountList: TradingAccount[],
    connectedTradeAccount: string | undefined,
    approvedDeviceList: (DeviceInterface|undefined)[],
    isLogined: boolean,
    mio: MIO,
    isHeaderExpanded: boolean
}

const INITIAL_STATE: INITIAL_STATE_TYPE = {
    account: null,
    isDemoAccount: false,
    enterDemoTab: TabDirectoryTab.QUOTE,
    demoTradingAccount: null,
    tradingAccountList: [],
    connectedTradeAccount: undefined,
    approvedDeviceList: [],
    isLogined: false,
    mio: undefined,
    isHeaderExpanded: false
}

export interface DeviceInterface {
    deviceId: string,
    deviceName: string,
    createTime: number,
    lastLoginTime: number
}


const AccountReducer = (state = INITIAL_STATE, action: AccountAction) => {
    switch(action.type) {
        case AccountActionType.SET_ACCOUNT_INFO:
            // console.debug(`AccountReducer Set Account Info: ${action.payload.Path}`);
            // console.debug(`AccountReducer Set Account Info: ${action.payload.AccessToken}`);
            return {
                ...state,
                account: {
                    ...state.account,
                    ...action.payload
                }
            }
        case AccountActionType.SET_TRADING_ACCOUNT_LIST:
            return {
                ...state,
                tradingAccountList: action.payload
            }
        case AccountActionType.SET_DEMO_TRADING_ACCOUNT:
            return {
                ...state,
                demoTradingAccount: action.payload
            }
        case AccountActionType.SET_IS_DEMO:
            return {
                ...state,
                isDemoAccount: action.payload
            }
        case AccountActionType.SET_ENTER_DEMO_TAB:
            return {
                ...state,
                enterDemoTab: action.payload
            }
        case AccountActionType.RESET_ACCOUNT:
            return {
                ...state,
                account: null
            }
        case AccountActionType.SET_CONNECTED_ACCOUNT:
            return {
                ...state,
                connectedTradeAccount: action.payload
            }
        case AccountActionType.SET_APPROVED_DEVICE_LIST:
            return {
                ...state,
                approvedDeviceList: action.payload
            }
        case AccountActionType.SET_IS_LOGINED:
            return {
                ...state,
                isLogined: action.payload
            }
        case AccountActionType.SET_MIO:
            return {
                ...state,
                mio: action.payload
            }
        case AccountActionType.SET_IS_HEADER_EXPANDED:
            return {
                ...state,
                isHeaderExpanded: action.payload
            }
        default:
            return state;
    }
}

export default AccountReducer;