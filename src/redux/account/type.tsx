import { BottomTabParamList } from "../../routes/route";
import { DeviceInterface } from "./reducer";

export type AccountState = {
    AccessToken?: string,
    Path?: string,
    LoginLevel?: number,
    DisplayName?: string,
    MobileNumber?: string, //For change Mobile Number display
    DisplayHeaderName?: string
}

enum AccountActionType {
    SET_ACCOUNT_INFO = 'SET_ACCOUNT_INFO',
    SET_TRADING_ACCOUNT_LIST = 'SET_TRADING_ACCOUNT_LIST',
    SET_DEMO_TRADING_ACCOUNT = 'SET_DEMO_TRADING_ACCOUNT',
    SET_IS_DEMO = 'SET_IS_DEMO',
    SET_ENTER_DEMO_TAB = 'SET_ENTER_DEMO_TAB',
    RESET = 'RESET',
    RESET_ACCOUNT = 'RESET_ACCOUNT',
    SET_CONNECTED_ACCOUNT = 'SET_CONNECTED_ACCOUNT',
    SET_APPROVED_DEVICE_LIST = 'SET_APPROVED_DEVICE_LIST',
    SET_IS_LOGINED = 'SET_IS_LOGINED',
    SET_MIO = 'SET_MIO',
    SET_IS_HEADER_EXPANDED = 'SET_IS_HEADER_EXPANDED'
}

export default AccountActionType;

interface SetAccountInfoInterface {
    type: AccountActionType.SET_ACCOUNT_INFO,
    payload: AccountState
}

interface SetTradingAccountInterface {
    type: AccountActionType.SET_TRADING_ACCOUNT_LIST,
    payload: TradingAccount[]
}

interface SetDemoTradingAccountInterface {
    type: AccountActionType.SET_DEMO_TRADING_ACCOUNT,
    payload: TradingAccount
}

interface SetIsDemoAccountInterface {
    type: AccountActionType.SET_IS_DEMO,
    payload: boolean
}

interface SetEnterDemoTabInterface {
    type: AccountActionType.SET_ENTER_DEMO_TAB,
    payload: keyof BottomTabParamList
}

interface ResetStateInterface {
    type: AccountActionType.RESET
}

interface ResetAccountStateInterface {
    type: AccountActionType.RESET_ACCOUNT
}

interface SetConnectedAccountInterface {
    type: AccountActionType.SET_CONNECTED_ACCOUNT,
    payload: string | undefined
}

interface SetApprovedDeviceListInterface {
    type: AccountActionType.SET_APPROVED_DEVICE_LIST,
    payload: (DeviceInterface|undefined)[]
}

interface SetIsLoginedInterface {
    type: AccountActionType.SET_IS_LOGINED,
    payload: boolean
}

interface SetMIOInterface {
    type: AccountActionType.SET_MIO,
    payload: MIO
}

interface SetIsHeaderExpandedInterface {
    type: AccountActionType.SET_IS_HEADER_EXPANDED,
    payload: boolean
}

export type TradingAccount = {
    Name: string,
    Account: string,
    Entity: string,
    isDemo: boolean,
    isDefault: boolean,
    userId: string,
    countryCode: string
}

export type MIO = {
    entityId: string,
    authDomain: string,
    clientId: string,
    clientSecret: string,
    webDomain: string,
    userTokenDomain: string,
    entityCode: string
}



export type AccountAction = SetAccountInfoInterface | SetTradingAccountInterface | ResetStateInterface | SetIsDemoAccountInterface | SetDemoTradingAccountInterface | ResetAccountStateInterface | SetEnterDemoTabInterface | SetConnectedAccountInterface | SetApprovedDeviceListInterface | SetIsLoginedInterface | SetMIOInterface | SetIsHeaderExpandedInterface