import { configureStore } from '@reduxjs/toolkit'
import { persistStore, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import rootReducer, { persistedReducer } from './root-reducer';
import AsyncStorage from '@react-native-async-storage/async-storage';

// const persistConfig = {
//     key: 'root',
//     storage: AsyncStorage,
//     whitelist: ['account','connection','trading','state']
// }

// const persistedReducer = persistReducer<any, any>(persistConfig, rootReducer);

export const store = configureStore(
    {
        reducer: persistedReducer,
        middleware: [thunk]
    }
);

export const persistor = persistStore(store);