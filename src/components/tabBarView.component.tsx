import { useTheme } from '@react-navigation/native'
import React, { Fragment, useEffect, useState } from 'react'
import { ColorValue, Pressable, StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'
import { makeGlobalStyles } from '../styles/styles'
import PressableComponent from '../views/utils/PressableComponent'
import StackView from './stackView.component'

interface TaBarItemInterface {
    title: string,
    indicatorHeight: number,
    titleColor: ColorValue,
    indicatorColor: ColorValue,
    index: number,
    selectedIndex: number,
    onPress: (index: number) => void,
}

const TabBarItem = (config: TaBarItemInterface) => {

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    
    const [selected, setSelected] = useState<boolean>(config.index === config.selectedIndex)

    useEffect(() => {
        setSelected(config.index === config.selectedIndex)
    }, [config.selectedIndex])

    return (
        <View
            style={{flexDirection: 'column', flex: 1}}>
            <PressableComponent
                style={[tabBarItemStyle.container]}
                onPress={() => config.onPress(config.index)}>
                <Text
                    style={[tabBarItemStyle.title, { color: selected ? config.titleColor : '#00000040' }]}>
                    {config.title}
                </Text>
            </PressableComponent>
            <View
                style={[tabBarItemStyle.indicator, { height: config.indicatorHeight, backgroundColor: selected ? config.indicatorColor : 'transparent' }]} />
        </View>
    )
}

const tabBarItemStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 13,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    indicator: {
        width: '100%',
    },
})


interface TabViewInterface {
    style?: StyleProp<ViewStyle>,
    labelTitles: string[],
    labelColor: ColorValue,
    indicatorColor?: ColorValue,
    indicatorHeight?: number,
    onPressTabBarItem: Function,
    selectedIndex?: number
}

const TabBarView = (config: TabViewInterface) => {

    const [selectedIndex, setSelectIndex] = useState<number>(config.selectedIndex ?? 0)

    useEffect(() => {
        console.debug('useEffect, selectedIndex:' + selectedIndex)
    }, [selectedIndex])

    const onPress = (index: number) => {
        console.debug('TabBarView onPress index:' + index)
        setSelectIndex(index)
        config.onPressTabBarItem(index)
    }

    return (
        <View 
            style={[tabBarViewStyle.container, config.style]}>
        {
            config.labelTitles.map((title, index) => {
                return (
                    <TabBarItem 
                        key={index.toString()}
                        title={title}
                        indicatorHeight={config.indicatorHeight}
                        indicatorColor={config.indicatorColor}
                        titleColor={config.labelColor}
                        index={index}
                        selectedIndex={selectedIndex}
                        onPress={() => onPress(index)} />
                )
            })
        }
        </View>
    )
}

const tabBarViewStyle = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
})

export default TabBarView