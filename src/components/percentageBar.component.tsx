import React from 'react';
import { StyleSheet, Text, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../styles/styles';

interface PercentageBarInterface {
    leftTitle: string,
    leftPercentage: number,
    leftColor?: string,
    rightTitle: string,
    rightPercentage: number,
    rightColor?: string,
    style?: ViewStyle
}

const PercentageBar = ({
    leftTitle,
    leftPercentage,
    leftColor = '',
    rightTitle,
    rightPercentage,
    rightColor = '',
    style={}
}: PercentageBarInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <View style={styles.nameWrapper}>
                <Text style={[globalStyles.Note_B, { color: leftColor === '' ? colors.Down : leftColor }]}>{leftPercentage}% {leftTitle}</Text>
                <Text style={[globalStyles.Note_B, { color: rightColor === '' ? colors.Up : rightColor }]}>{rightPercentage}% {rightTitle}</Text>
            </View>
            <View style={{ height: 2 }} />
            <View style={[styles.percentageBarContainer, { backgroundColor: rightColor === '' ? colors.Up : rightColor }]}>
                <View style={[styles.leftContainer, { backgroundColor: leftColor === '' ? colors.Down : leftColor, width: `${leftPercentage}%` }]} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 48
    },
    nameWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    percentageBarContainer: {
        width: '100%',
        height: 8,
        borderRadius: 4,
    },
    leftContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        borderRadius: 4,
    }
})

export default PercentageBar