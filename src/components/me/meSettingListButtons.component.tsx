import React from 'react';
import { Pressable, StyleSheet, Text, View,Image, ImageSourcePropType, useColorScheme} from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';

import { LAYOUT_PADDING_HORIZONTAL } from '../../styles/styles';
import { color } from 'react-native-reanimated';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonsInterface {
    leftLabel: string,
    rightLabel?: string,
    rightElement?: Element,
    func?: Function,
    isPrimary?: boolean,
    isSecondary?: boolean,
    isEnable?: boolean
}

const MeSettingListButton = ({ leftLabel, rightLabel, func, rightElement, isPrimary, isSecondary, isEnable = true }: ActionButtonsInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const scheme = useColorScheme();

    const onPressBtn1Func = () => {
        if (func && isEnable) {
            func();
        }
    }

    return (
        <PressableComponent onPress={onPressBtn1Func} style={[styles.settingItem, isSecondary ? {} : {borderTopColor: colors.WrapperBg, borderTopWidth: 1, backgroundColor: colors.White}]}>
            <View style={{flexDirection: 'row', paddingHorizontal: LAYOUT_PADDING_HORIZONTAL}}>
                <Text style={[isPrimary ? globalStyles.Big_Text_B : globalStyles.Body_Text, {flex: 0.5, paddingStart: isSecondary ? 16 : 0}, !isEnable ? { opacity: 0.3 } : null]}>{leftLabel}</Text>
                {
                    rightElement ? <View style={{flex: 0.5, alignItems:'flex-end'}}>{rightElement}</View>
                    : <Text style={[globalStyles.Body_Text_B, {flex: 0.5, textAlign: 'right'}, !isEnable ? { opacity: 0.3 } : null]}>{rightLabel}</Text>
                }
                
            </View>
        </PressableComponent>
    )
}

const styles = StyleSheet.create({
    settingItem: { 
        paddingVertical: 15, 
    }
});

export default MeSettingListButton