import React from 'react';
import { Pressable, StyleSheet, Text, View, useColorScheme} from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import { State } from '../../redux/root-reducer';
import { useSelector } from 'react-redux';
import { t } from 'i18next';
import VerifyCodeTick from '../../../assets/svg/VerifyCodeTick';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonsInterface {
    statusComponent?: React.ReactNode,
    accountName: string,
    tradeAccount: string,
    access: Function,
    isDefault: boolean
}

const MeAccountButton = ({ statusIcon, accountName, access, tradeAccount, isDefault = false }: ActionButtonsInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const scheme = useColorScheme();

    const login = useSelector((state: State) => state.trading.accountDetail?.Login);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);

    return (
        <PressableComponent onPress={()=> {login && tradeAccount !== login.toString() ? access() : undefined}} >
            <View style={[styles.container, login && tradeAccount === login.toString() ? {backgroundColor: colors.FixedMainBg} : {backgroundColor: '#00000012'}]}>
                {
                statusIcon ??
                <VerifyCodeTick 
                    backgroundColor={login && tradeAccount === login.toString() ? colors.Green : '#9D9D9DAA'} 
                    color={login && tradeAccount === login.toString() ? colors.White : colors.Grey2} />
                }
                <View style={{padding: 5}}/>
                <Text style={[globalStyles.Body_Text_B, {flex: 1, alignSelf: 'center', color: login && tradeAccount === login.toString() ? colors.FixedMainFont : colors.FixedTitleFont}]}>{accountName + ' ' + tradeAccount}</Text>
                {
                    isDefault && tradingAccountList.length > 1 ?
                    <Text style={[globalStyles.Default, styles.default, {backgroundColor: colors.Red}]}>{t('me.default')}</Text>
                    : 
                    null
                }
            </View>
        </PressableComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderRadius: 6,
        flexDirection: 'row'
    },
    default: {
        borderRadius: 6,
        padding: 2,
        paddingVertical: 5,
        marginLeft: 10
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
});

export default MeAccountButton