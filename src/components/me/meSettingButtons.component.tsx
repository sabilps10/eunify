import React, { ReactNode } from 'react';
import { Pressable, StyleSheet, Text, View,Image, useColorScheme, Dimensions} from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import Lock from '../../../assets/svg/Lock';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonsInterface {
    btn1Title: string,
    btn1Func: Function,
    source: ReactNode,
    isLock?: boolean
}

const MeSettingButton = ({ btn1Title, btn1Func, source, isLock = false }: ActionButtonsInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const scheme = useColorScheme();

    const onPressBtn1Func = () => btn1Func();

    return (
        <View style={styles.settingItem}>
            {
                isLock ? <View style={styles.lock}><Lock color={colors.Grey3} /></View> : null
            }
            <PressableComponent disabled={isLock} onPress={onPressBtn1Func} style={styles.icon}>
                {source}
                <View style={{ padding: 4 }} />
                <Text style={[globalStyles.Note_120, isLock ? {color: colors.Grey3} : {} ]}>{btn1Title}</Text>
            </PressableComponent>
        </View>
    )
}

const styles = StyleSheet.create({
    settingItem: {
        flex: 1,
        maxWidth: Dimensions.get('window').width/3
    },
    icon: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    lock: {
        position:'absolute',
        left: 15
    }
});

export default MeSettingButton