import React, {} from 'react';
import { Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTranslation } from 'react-i18next';
import Remove from '../../../assets/svg/Remove';
import PressableComponent from '../../views/utils/PressableComponent';

interface LoadingDialogInterface {
    isShow: boolean,
    setShow: Function,
    title: string,
    message: string 
}

const MessageConfirmDialog = ({isShow, setShow, title, message}: LoadingDialogInterface) => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const onPressBackBtn = () => {setShow(false)}

    const toggleBackdrop = () => {
        // SetIsShowLoading(false);
        // setShow(false)
    }

    return (
        <View style={[styles.container]}>
            <Dialog isVisible={isShow} style={{backgroundColor: colors.White}} overlayStyle={[styles.overlayStyle]} onBackdropPress={toggleBackdrop}>
                <View style={styles.dialog}>
                    <Text style={[globalStyles.H3, {color: colors.Grey4}]}>{title}</Text>
                    <Text style={[globalStyles.Note, {color: colors.Text}]}>{message}</Text>
                    <View style={{padding: 10}}/>
                    <View style={styles.bottomButton}>
                        <PressableComponent onPress={onPressBackBtn}>
                            <Remove color={colors.MainFont} />
                        </PressableComponent>
                    </View>
                </View>
            </Dialog>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    overlayStyle:{
        width: '80%',
        borderRadius: 12
    },
    dialog: {
        backgroundColor: '#FFFFFF'
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    bottomButton: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default MessageConfirmDialog