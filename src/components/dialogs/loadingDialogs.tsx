import React, {useState, useEffect, Dispatch, SetStateAction} from 'react';
import { Image, StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import { t } from 'i18next';
import TextLabel from '../forms/labels/textLabel.component';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
} from 'react-native-indicators';

interface LoadingDialogInterface {
    message?: string,
    style?: ViewStyle,
    isEnableDisableByToggleBackdrop?: boolean,
    autoDisableTimeout?: number
}

const LoadingDialog = ({ message = '', style = {}, isEnableDisableByToggleBackdrop = false, autoDisableTimeout = 30000 }: LoadingDialogInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();
    
    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowProgress } = bindActionCreators(StateActionCreators, dispatch);
    
    const isShowLoading = useSelector((state: State) => state.state.isShowLoading);
    const isShowProgress = useSelector((state: State) => state.state.isShowProgress.isShowDialog);
    const progress = useSelector((state: State) => state.state.isShowProgress.progress);
    const total = useSelector((state: State) => state.state.isShowProgress.total);

    useEffect(() => {
        // console.debug('isShowLoading', isShowLoading)
        // if (autoDisableTimeout > 0){
        //     setTimeout(() => {
        //         SetIsShowLoading(false);
        //     }, autoDisableTimeout)
        // }
    }, [isShowLoading]);

    const toggleBackdrop = () => {
        if (isEnableDisableByToggleBackdrop) {
            SetIsShowLoading(false);
            SetIsShowProgress({ isShowDialog: false, progress: 0, total: 0 });
        }
    }

    return (
        <View style={[styles.container, style]}>
            <Dialog isVisible={isShowLoading || isShowProgress} backdropStyle={{backgroundColor:'#00000000'}} style={{backgroundColor: isShowLoading ? '#00000000' : colors.Grey2}} overlayStyle={[styles.overlayStyle, {backgroundColor: isShowLoading ? '#00000000' : colors.Grey2, height: isShowLoading? 0:150, width: isShowLoading? 0:150}]} onBackdropPress={toggleBackdrop}>
                {/* <Dialog.Loading loadingProps={{color: colors.White}} /> */}
                <UIActivityIndicator color={isShowLoading ? colors.Grey3 : colors.White} size={60}/>
                {
                    isShowProgress && 
                    <View>
                        <TextLabel label={t('codePush.downloading') + (progress/total*100).toFixed(0) + '%'} labelStyle={[globalStyles.Body_Text_B, {color: colors.White}]} />
                    </View>
                }
            </Dialog>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    overlayStyle:{
        height: 150, 
        width: 150, 
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000000',
        borderColor: '#00000000'
    }
})

export default React.memo(LoadingDialog)