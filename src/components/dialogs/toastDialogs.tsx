import React, { useEffect, useState } from 'react';
import { StyleSheet, useColorScheme, View } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useTheme } from '@react-navigation/native';
import TextLabel from '../forms/labels/textLabel.component';
import { makeGlobalStyles } from '../../styles/styles';
import SuccessCheck from '../../../assets/svg/SuccessCheck';

interface LoadingDialogInterface {
    autoDisableTimeout?: number
}

const ToastDialog = ({ autoDisableTimeout = 1000 }: LoadingDialogInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    const dispatch = useDispatch();
    const { SetIsShowToastDialog } = bindActionCreators(StateActionCreators, dispatch);
    
    const isShowDialog = useSelector((state: State) => state.state.isShowToastDialog && state.state.isShowToastDialog.isShowDialog);
    const message = useSelector((state: State) => state.state.isShowToastDialog && state.state.isShowToastDialog.message);

    const [timer, setTimer] = useState<NodeJS.Timeout>();

    useEffect(() => {
        console.debug('autoDisableTimeout', autoDisableTimeout)
        if (autoDisableTimeout > 0){
            clearTimeout(timer)
            setTimer(setTimeout(() => {
                SetIsShowToastDialog({
                    isShowDialog: false,
                    message: '',
                    isSuccess: true
                });
            }, autoDisableTimeout))
        }
    }, [isShowDialog]);

    return (
        <View style={[styles.container]}>
            <Dialog isVisible={isShowDialog} backdropStyle={{backgroundColor:'#00000000'}} style={{backgroundColor: colors.Grey3}} overlayStyle={[styles.overlayStyle, {backgroundColor: colors.Grey3}]}>
                <SuccessCheck style={{height: 22.5, width: 22.5}} color={colors.TitleFont} />
                <View style={{padding: 5}}/>
                <TextLabel label={message} labelStyle={[globalStyles.Body_Text_B, {color: colors.White, padding: 5, textAlign: 'center'}]} />
            </Dialog>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    overlayStyle:{
        height: 150, 
        width: 150, 
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    }
})

export default ToastDialog