import React, { useEffect } from 'react';
import { Pressable, ScrollView, StyleSheet, Text, useColorScheme, View } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTranslation } from 'react-i18next';
import ActionButton from '../buttons/actionButton.component';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { isEmpty } from '../../utils/stringUtils';
import Remove from '../../../assets/svg/Remove';
import { THEME_TYPE } from '../../types/types';
import PressableComponent from '../../views/utils/PressableComponent';
import { MessageDialogContent } from '../../redux/state/types'
import GAScreenName from '../../json/GAScreenName.json'
import analytics from '@react-native-firebase/analytics'

const MessageDialog = () => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const onPressBackBtn = () => {
        if (callback) {
            callback();
        }
        onPressCancel();
    }

    const onPressCancel = () => {
        SetIsShowMessageDialog({
            isShowDialog: false, 
            title: '', 
            message: '',
            isShowCancel: undefined,
            isShowBtn: undefined,
            callback: undefined,
            btnLabel: undefined
        })
    }

    const toggleBackdrop = () => {
        // SetIsShowLoading(false);
        // setShow(false)
    }

    const dispatch = useDispatch();
    const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

    const isShowDialog = useSelector((state: State) => state.state.isShowMessageDialog.isShowDialog);
    const title = useSelector((state: State) => state.state.isShowMessageDialog.title);
    const titleStyle = useSelector((state: State) => state.state.isShowMessageDialog.titleStyle);
    const message = useSelector((state: State) => state.state.isShowMessageDialog.message);
    const messageStyle = useSelector((state: State) => state.state.isShowMessageDialog.messageStyle);
    const callback = useSelector((state: State) => state.state.isShowMessageDialog.callback);
    const isShowBtn = useSelector((state: State) => state.state.isShowMessageDialog.isShowBtn);
    const isShowCancel = useSelector((state: State) => state.state.isShowMessageDialog.isShowCancel);
    const btnLabel = useSelector((state: State) => state.state.isShowMessageDialog.btnLabel);
    const linkMessage = useSelector((state: State) => state.state.isShowMessageDialog.linkMessage);
    const linkMessageCallBack = useSelector((state: State) => state.state.isShowMessageDialog.linkMessageCallBack);
    const alternateStyle = useSelector((state: State) => state.state.isShowMessageDialog.alternateStyle);
    const cancelCallback = useSelector((state: State) => state.state.isShowMessageDialog.cancelCallback);
    const detailComponent = useSelector((state: State) => state.state.isShowMessageDialog.detailComponent);
    const trackingContext = useSelector((state: State) => state.state.isShowMessageDialog.trackingContext)

    // iOS 14 Analytics Tracking permission authorization request
    const allowTrack = useSelector((state: State) => state.state.allowTrack)
    // GA4 Tracking ScreeView
    useEffect(() => {
        if (allowTrack && isShowDialog) {
            const getScreenClassFor = (trackingContext: MessageDialogContent.TrackingContext) => {
                return trackingContext
            }
            let screenName: string, screenClass: string
            if ((screenClass = getScreenClassFor(trackingContext)) && (screenName = GAScreenName[screenClass]) && screenName.length !== 0 && screenName.trim() !== '') {
                analytics().logScreenView({
                    screen_name: screenName,
                    screen_class: screenClass
                })
                console.debug(`[MessageDialog useEffect [isShowDialog, trackingContext, allowTrack]] analytics().logScreenView(${JSON.stringify({screen_name: screenName, screen_class: screenClass})})`)
            }
        }
    }, [isShowDialog, trackingContext, allowTrack])

    return (
        <View style={[styles.container]}>
            <Dialog isVisible={isShowDialog} overlayStyle={[styles.overlayStyle]} onBackdropPress={toggleBackdrop}>
                <View>
                    {
                        alternateStyle ?
                            <Text style={[globalStyles.ButtonText, {color: colors.DialogTitleFont}, titleStyle]}>{title}</Text>
                        :
                            <Text style={[globalStyles.H2, {color: colors.DialogTitleFont}, titleStyle]}>{title}</Text>
                    }   
                    
                    <View style={{padding: 7}}/>
                    {
                        !isEmpty(message) &&
                        <ScrollView style={{maxHeight: 300}}>
                            <View style={{padding: 8}}/>
                            <Text style={[globalStyles.Note, {color: colors.MainFont, lineHeight: 19}, messageStyle]}>{message}</Text>
                            <View style={{padding: 15}}/>
                        </ScrollView>
                    }
                    {
                        !isEmpty(linkMessage) &&
                        <PressableComponent onPress={()=> linkMessageCallBack()}>

                            <Text style={[globalStyles.Note, {color: colors.MainFont, lineHeight: 19}]}>{linkMessage}</Text>
                            <View style={{padding: 15}}/>
                        </PressableComponent>
                    }
                    {
                        detailComponent &&
                        <View>
                            {detailComponent}
                        </View>
                    }
                    
                    <View style={styles.bottomButton}>
                        {
                            (isShowBtn === undefined || isShowBtn) && (!alternateStyle) &&
                            <ActionButton title={btnLabel ?? t('common.ok')} onPress={onPressBackBtn}/>
                        }
                        
                        {
                            isShowCancel || alternateStyle?
                                <PressableComponent onPress={() => {
                                        onPressCancel()
                                        if (cancelCallback)
                                            cancelCallback()
                                    }}>
                                    <Remove style={{height: 32, width: 32}} color={colors.MainFont} />
                                </PressableComponent>
                            : 
                                null
                        }
                    </View>
                </View>
            </Dialog>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
    },
    overlayStyle:{
        width: '80%',
        borderRadius: 12,
        backgroundColor: colors.MainBg,
    },
    dialog: {
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    bottomButton: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default React.memo(MessageDialog)