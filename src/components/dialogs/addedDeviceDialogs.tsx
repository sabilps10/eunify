import React, { useEffect } from 'react';
import { StyleSheet, useColorScheme, View, ViewStyle } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useTheme } from '@react-navigation/native';
import TextLabel from '../forms/labels/textLabel.component';
import { makeGlobalStyles } from '../../styles/styles';
import SuccessCheck from '../../../assets/svg/SuccessCheck';

import { useTranslation } from 'react-i18next';

interface LoadingDialogInterface {
    message?: string,
    style?: ViewStyle,
    isEnableDisableByToggleBackdrop?: boolean,
    autoDisableTimeout?: number
}

const AddDeviceDialog = ({ style = {}, isEnableDisableByToggleBackdrop = false, autoDisableTimeout = 1000 }: LoadingDialogInterface) => {    
    const [t, i18n] = useTranslation();

    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    const dispatch = useDispatch();
    const { SetIsShowAddDevice } = bindActionCreators(StateActionCreators, dispatch);
    
    const isShowAddDevice = useSelector((state: State) => state.state.isShowAddDevice);

    useEffect(() => {
        console.debug('AddDeviceDialog', isShowAddDevice)
        if (autoDisableTimeout > 0){
            setTimeout(() => {
                console.debug('AddDeviceDialog setTimeout')
                SetIsShowAddDevice(false);
            }, autoDisableTimeout)
        }
    }, [isShowAddDevice]);

    const toggleBackdrop = () => {
        if (isEnableDisableByToggleBackdrop) {
            SetIsShowAddDevice(false);
        }
    }

    return (
        <View style={[styles.container, style]}>
            <Dialog isVisible={isShowAddDevice} style={{backgroundColor: colors.Grey3}} overlayStyle={[styles.overlayStyle, {backgroundColor: colors.Grey3}]} onBackdropPress={toggleBackdrop}>
                <SuccessCheck style={{height: 22.5, width: 22.5}} color={colors.White} backgroundColor={colors.White} />
                <View style={{padding: 5}}/>
                <TextLabel label={t('addDevices.added')} labelStyle={[globalStyles.Body_Text_B, {color: colors.White}]} />
            </Dialog>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    overlayStyle:{
        height: 150, 
        width: 150, 
        padding: 0,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    }
})

export default AddDeviceDialog