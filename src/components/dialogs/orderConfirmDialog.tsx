import React, { useState, useEffect } from 'react';
import { ColorValue, Pressable, ScrollView, StyleSheet, Text, useColorScheme, View } from 'react-native';

import { Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useTheme } from '@react-navigation/native';
import TextLabel from '../forms/labels/textLabel.component';
import { makeGlobalStyles } from '../../styles/styles';
import { useTranslation } from 'react-i18next';
import { getRejectedOrderComponent, OrderActionType, OrderDirectionType, PendingOrderBuySellType, PendingOrderType, ProfitStopLossOrderType } from '../../utils/orderUtils';
import { isEmpty, toCurrencyDisplay } from '../../utils/stringUtils';
import { priceChangePercentageDP } from '../../config/constants';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { WebsocketUtils } from '../../utils/websocketUtils';
import ActionButton from '../buttons/actionButton.component';
import DashedLineView from '../dashedLineView.component';
import InputCheckBox from '../forms/inputs/inputCheckBox.component';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { displayContractCode, productName } from '../../views/utils/CommonFunction';
import MarginValueComponent from '../../views/trade/component/marginValue.component';
import ContractValueComponent from '../../views/trade/component/contractValue.component';
import Remove from '../../../assets/svg/Remove'
import { THEME_TYPE } from '../../types/types';
import PressableComponent from '../../views/utils/PressableComponent';
import { TradingResponseCode } from '../../utils/errorUtil';
import analytics from '@react-native-firebase/analytics'
import GAScreenName from '../../json/GAScreenName.json'

interface ProfitTypeInterface {
    type: number,
    // slValue: number,
    // tpValue: number
}

interface LoadingDialogInterface {
    isShow?: boolean,
    setShow?: Function,
    type?: string, 
    contractCode?: string, 
    isSelectBuy?: boolean, 
    volume?: number, 
    price?: number,  
    isPending?: boolean, 
    pendingOrderType?: number, 
    penderOrderValidity?: number, 
    isProfit?: boolean, 
    stopLossPrice?: number, 
    limitProfitPrice?: number, 
    transactionId?: number,
    callback?: Function,
    profitType?: ProfitTypeInterface | undefined
}

const OrderConfirmDialog = ({
        // isShow=false, 
        // setShow, 
        // type='', 
        // contractCode='', 
        // isSelectBuy=false, 
        // volume=0, 
        // price=0, 
        // isPending=false, 
        // pendingOrderType=0, 
        // penderOrderValidity=0, 
        // isProfit=false, 
        // stopLossPrice=0, 
        // limitProfitPrice=0, 
        // transactionId=0, 
        // callback = Function, 
        // profitType=undefined
    }: LoadingDialogInterface) => {
    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const profitType = useSelector((state: State) => state.state.isShowOrderConfirmDialog.profitType);
    const isShowDialog = useSelector((state: State) => state.state.isShowOrderConfirmDialog.isShowDialog);
    const type = useSelector((state: State) => state.state.isShowOrderConfirmDialog.type);
    const contractCode = useSelector((state: State) => state.state.isShowOrderConfirmDialog.contractCode);
    const isSelectBuy = useSelector((state: State) => state.state.isShowOrderConfirmDialog.isSelectBuy);
    const volume = useSelector((state: State) => state.state.isShowOrderConfirmDialog.volume);
    const price = useSelector((state: State) => state.state.isShowOrderConfirmDialog.price);
    const isPending = useSelector((state: State) => state.state.isShowOrderConfirmDialog.isPending);
    const pendingOrderType = useSelector((state: State) => state.state.isShowOrderConfirmDialog.pendingOrderType);
    const penderOrderValidity = useSelector((state: State) => state.state.isShowOrderConfirmDialog.pendingOrderValidity);
    const isProfit = useSelector((state: State) => state.state.isShowOrderConfirmDialog.isProfit);
    // const stopLossPrice = useSelector((state: State) => state.state.isShowOrderConfirmDialog.stopLossPrice);
    const stopLossPrice = useSelector((state: State) => profitType && profitType.type === ProfitStopLossOrderType.PRICE ? state.trading.currentTradeSL.StopLossPrice : state.trading.currentTradeSL.StopLossPriceRef)
    const stopLossPricePoint = useSelector((state: State) => state.trading.currentTradeSL.StopLossPrice)
    // const limitProfitPrice = useSelector((state: State) => state.state.isShowOrderConfirmDialog.limitProfitPrice);
    const limitProfitPrice = useSelector((state: State) => profitType && profitType.type === ProfitStopLossOrderType.PRICE ? state.trading.currentTradeTP.TakeProfitPrice : state.trading.currentTradeTP.TakeProfitPriceRef)
    const limitProfitPricePoint = useSelector((state: State) => state.trading.currentTradeTP.TakeProfitPrice)
    const transactionId = useSelector((state: State) => state.state.isShowOrderConfirmDialog.transactionId);
    const callback = useSelector((state: State) => state.state.isShowOrderConfirmDialog.callback);
    const tradeSetting = useSelector((state: State) => state.state.setting!.TradeSetting);
    const login = useSelector((state: State) => state.trading.accountDetail && state.trading.accountDetail.Login); 
    const loginLevel = useSelector((state: State) => state.account.account?.LoginLevel);   
    const allowTrack = useSelector((state: State) => state.state.allowTrack)
    
    const [orderInstrument, SetOrderInstrument] = useState<string>(contractCode);
    const [orderContractCode, SetOrderContractCode] = useState<string>(contractCode);
    const [orderIsBuy, SetOrderIsBuy] = useState<boolean>(isSelectBuy);
    const [orderVolume, SetOrderVolume] = useState<number>(volume);
    const [orderVolumeDP, SetOrderVolumeDP] = useState<number>(0);
    const [orderPriceDP, SetOrderPriceDP] = useState<number>(0);
    const [orderPrice, SetOrderPrice] = useState<number>(0);
    const [orderPendingOrderType, SetOrderPendingOrderType] = useState<number>(pendingOrderType);

    const contractSymbols = useSelector((state: State) => state.trading.symbols && state.trading.symbols[orderContractCode]);
    const contractPrice = useSelector((state: State) => {
        if (isShowDialog && state.trading.prices)
            return state.trading.prices[orderContractCode]
    });
    const order = useSelector((state: State) => state.trading.orders && (type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER ) && state.trading.orders[transactionId!]);
    const symbol = useSelector((state: State) => state.trading.symbols && type === OrderActionType.CANCEL_ORDER && order && state.trading.symbols[order.Symbol]);
    const position = useSelector((state: State) => state.trading.positions && (type === OrderActionType.CLOSE_POSITION || type === OrderActionType.EDIT_POSITION) && state.trading.positions[transactionId!]);
    const positionProfit = useSelector((state: State) => {
        if (isShowDialog && state.trading.profits && (type === OrderActionType.CLOSE_POSITION || type === OrderActionType.EDIT_POSITION) && transactionId)
            return state.trading.profits[transactionId]
    });
    const cognitoTempState = useSelector((state: State) => state.cognitoTemp);

    const [floatingPnL, setFloatingPnL] = useState<number>(0)
    const [sellPrice, SetSellPrice] = useState<number>(0);
    const [buyPrice, SetBuyPrice] = useState<number>(0);
    const [pl, SetPL] = useState<string>('');
    const [plColor, SetPLColor] = useState<ColorValue>(colors.Up);
    const [enableButton, setEnableButton] = useState<boolean>(true);

    // useLayoutEffect(() => {
    //     navigation.setOptions({
    //         header: props => null
    //     });
    // }, [navigation]);
    useEffect(() => {
        console.debug('isShowDialog', isShowDialog)
    }, [isShowDialog])


    // Non-Close position
    useEffect(() => {
        let isClosePosition = type === OrderActionType.CLOSE_POSITION
        if (isClosePosition) return
        
        setFloatingPnL(positionProfit)
    }, [positionProfit])

    // Close position
    useEffect(() => {
        let isClosePosition = type === OrderActionType.CLOSE_POSITION
        if (!isClosePosition) return

        let pnl = (volume/position.Volume) * positionProfit

        setFloatingPnL(pnl)
    }, [contractPrice])

    useEffect(() => {
        if (isShowDialog) {
            SetOrderInstrument(contractCode)
            SetOrderContractCode(contractCode)
            SetOrderIsBuy(isSelectBuy)
            SetOrderVolume(volume)

            if (type === OrderActionType.EDIT_ORDER) {
                SetOrderPendingOrderType(pendingOrderType)
            }
        }
    }, [isShowDialog, contractCode, isSelectBuy, volume, pendingOrderType])

    useEffect(() => {
        if (isShowDialog && (type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER)) {
            if (order) {
                SetOrderContractCode(order.Symbol)
                SetOrderIsBuy(order.Type === PendingOrderBuySellType.BUY_LIMIT || order.Type === PendingOrderBuySellType.BUY_STOP)

                if (type === OrderActionType.CANCEL_ORDER){ 
                    SetOrderPrice(order.PriceOrder)
                    SetOrderVolume(order.RemainVolume)
                    SetOrderPendingOrderType(order.Type)
                } else {
                    SetOrderPrice(price)
                }
            }
        }
    }, [order, isShowDialog])

    useEffect(()=>{
        if (isShowDialog && (type === OrderActionType.CLOSE_POSITION || type === OrderActionType.EDIT_POSITION) && position && contractPrice) {
            var displayPL = floatingPnL > 0 ? '+' + toCurrencyDisplay(floatingPnL) : toCurrencyDisplay(floatingPnL);
            var closePrice = position.Type === 0 ? contractPrice.Bid! : contractPrice.Ask!
            const change = (position.Type === 0 ? (closePrice - position.OpenPrice) : (position.OpenPrice - closePrice)) / position.OpenPrice * 100;
            var displayChange = '(' + (change > 0 ? '+' + change.toFixed(priceChangePercentageDP) : change.toFixed(priceChangePercentageDP)) + '%)';
            SetPL(displayPL + ' ' +displayChange)
            SetPLColor(change > 0 ? colors.Up : colors.Down)
            if (type === OrderActionType.EDIT_POSITION) {
                SetOrderPrice(position.OpenPrice)
            }
        }
    }, [position, floatingPnL, isShowDialog])

    useEffect(() => {
        if (isShowDialog) {
            if (contractSymbols) {
                SetOrderInstrument(contractSymbols.SymbolName)
                var vDP = contractSymbols.VolumeStep.toString().split('.');
                var nVolumeDP = 0
                if (vDP.length > 1){
                    nVolumeDP = vDP[1].length;
                }
                SetOrderVolumeDP(nVolumeDP)
                SetOrderPriceDP(contractSymbols.Digits)
            }
        }
    }, [contractSymbols, isShowDialog])

    useEffect(() => {
        if (isShowDialog && (type === OrderActionType.CLOSE_POSITION || type === OrderActionType.CANCEL_ORDER || type === OrderActionType.EDIT_POSITION)) {
            if (contractPrice) {
                SetSellPrice(contractPrice.Bid)
                SetBuyPrice(contractPrice.Ask)
                // SetPL();
                // SetPLColor();
            }
        } 
        if ((type === OrderActionType.NEW_ORDER || type === OrderActionType.CLOSE_POSITION ) && contractPrice) {
            if (isPending)
                SetOrderPrice(price)
            else
                SetOrderPrice(isSelectBuy? contractPrice.Ask : contractPrice.Bid)
        }
    }, [contractPrice, isShowDialog])

    // GA4 Tracking ScreeView
    useEffect(() => {
        // allowTrack?
        if (allowTrack && isShowDialog) {
            const getScreenClassFor = (actionType: OrderActionType) => {
                switch (actionType) {
                    case OrderActionType.NEW_ORDER: return "OrderConfirmDialog+NewOrderReview"
                        break;
                    case OrderActionType.EDIT_ORDER: return "OrderConfirmDialog+UpdatePendingOrderReview"
                        break;
                    case OrderActionType.CANCEL_ORDER: return "OrderConfirmDialog+DeletePendingOrderReview"
                        break;
                }
            }
            let screenName: string, screenClass: string
            if ((screenClass = getScreenClassFor(type)) && (screenName = GAScreenName[screenClass]) && screenName.length !== 0 && screenName.trim() !== '') {
                analytics().logScreenView({
                    screen_name: screenName,
                    screen_class: screenClass
                })
                console.debug(`[OrderConfirmDialog useEffect [isShowDialog, type, allowTrack]] analytics().logScreenView(${JSON.stringify({screen_name: screenName, screen_class: screenClass})})`)
            }
        }
    }, [isShowDialog, type, allowTrack])

    const [isChecked, SetIsChecked] = useState<boolean>(false)
    const [isShowDisclaimer, SetIsShowDisclaimer] = useState<boolean>(false)

    const dispatch = useDispatch();
    const { SetAccountInfo } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetSetting, SetIsShowLoading, SetIsShowOrderConfirmDialog, SetIsShowToastDialog, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { SetLastOrderId } = bindActionCreators(OpenPositionsActionCreators, dispatch);

    const { startNewOrder, startNewPendingOrder, startLiqOrder, startDeletePendingOrder, startEditPosition, startEditOrder } = WebsocketUtils();

    // const onPressBackBtn = () => { navigation.goBack() }
    const onPressBackBtn = () => {
        SetIsShowOrderConfirmDialog({
            isShowDialog: false,
            type: null, 
            contractCode: '', 
            isSelectBuy: false, 
            volume: 0, 
            price: 0,  
            isPending: false, 
            isProfit: false, 
            stopLossPrice: 0, 
            limitProfitPrice: 0, 
        })
    }
    const onPressOrder = async() => { 
        SetIsShowOrderConfirmDialog({
            isShowDialog: false,
            type: null, 
            contractCode: '', 
            isSelectBuy: false, 
            volume: 0, 
            price: 0,  
            isPending: false, 
            isProfit: false, 
            stopLossPrice: 0, 
            limitProfitPrice: 0, 
        })

        if ((type === OrderActionType.EDIT_POSITION || type === OrderActionType.CLOSE_POSITION) && position === undefined) {
            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.noPosition')})
        } else if ((type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER) && order === undefined) {
            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.noOrder')})
        } else {
            setEnableButton(false)
            try
            {
                var result = undefined;
                if (type === OrderActionType.NEW_ORDER) {
                    if (isPending) {
                        var orderType = 2;
                        if (pendingOrderType === PendingOrderType.LIMIT) {
                            if (orderIsBuy){ //BuyLimit
                                orderType = PendingOrderBuySellType.BUY_LIMIT
                            } else { //SellLimit
                                orderType = PendingOrderBuySellType.SELL_LIMIT
                            }
                        } else {
                            if (orderIsBuy){ //BuyStop
                                orderType = PendingOrderBuySellType.BUY_STOP
                            } else { //SellStop
                                orderType = PendingOrderBuySellType.SELL_STOP
                            }
                        }
                        result = await startNewPendingOrder(contractCode, orderType, orderVolume, orderPrice, isProfit && limitProfitPrice > 0 ? limitProfitPrice : 0, isProfit && stopLossPrice > 0 ? stopLossPrice : 0, penderOrderValidity!);
                    }
                    else {
                        result = await startNewOrder(contractCode, orderIsBuy ? OrderDirectionType.BUY : OrderDirectionType.SELL, orderVolume, orderPrice, isProfit && limitProfitPrice > 0 ? limitProfitPrice : 0, isProfit && stopLossPrice > 0 ? stopLossPrice : 0);
                    }
                } else if (type === OrderActionType.CLOSE_POSITION) {
                    result = await startLiqOrder(contractCode, orderIsBuy ? OrderDirectionType.SELL : OrderDirectionType.BUY, orderVolume, orderPrice, transactionId!);
                } else if (type === OrderActionType.CANCEL_ORDER) {
                    result = await startDeletePendingOrder(transactionId!);
                } else if (type === OrderActionType.EDIT_POSITION) {
                    result = await startEditPosition(transactionId!, limitProfitPrice > 0 ? limitProfitPrice : 0, stopLossPrice > 0 ? stopLossPrice : 0)
                } else if (type === OrderActionType.EDIT_ORDER) {
                    result = await startEditOrder(transactionId!, orderPrice, isProfit && limitProfitPrice > 0 ? limitProfitPrice : 0, isProfit && stopLossPrice > 0 ? stopLossPrice : 0)
                }
    
                //After successfully made order
                if (isChecked) {
                    var userTradeSetting = {
                        IsEnableTradeConfirmationDialog: false
                    };
                    
                    tradeSetting[cognitoTempState.username] = userTradeSetting;
            
                    SetSetting({
                        TradeSetting: tradeSetting
                    })
                }
    
                if (result) {
                    if (result.RtnCode === TradingResponseCode.RET_OK || result.RtnCode === TradingResponseCode.RET_CUST_OK) {
                        if (result.ID && (type === OrderActionType.NEW_ORDER)) {
                            SetLastOrderId(result.ID.toString());
                        } else if (type === OrderActionType.EDIT_POSITION || type === OrderActionType.EDIT_ORDER){
                            SetLastOrderId(transactionId.toString()); 
                            if (type === OrderActionType.EDIT_ORDER) {
                                SetIsShowToastDialog({
                                    isShowDialog: true,
                                    message: t('confirmTrade.orderModified'),
                                    isSuccess: true
                                })
                            }
                        } 
                        else if (type === OrderActionType.CANCEL_ORDER) {
                            SetIsShowToastDialog({
                                isShowDialog: true,
                                message: t('confirmTrade.orderCancel'),
                                isSuccess: true
                            })
                        } else if (type === OrderActionType.CLOSE_POSITION) {
                            SetIsShowToastDialog({
                                isShowDialog: true,
                                message: t('confirmTrade.positionClosed'),
                                isSuccess: true
                            })
                        }
                        if (callback) {
                            console.debug(`[OrderConfirmDialog onPressOrder] try catch websocket start...Order with result ${JSON.stringify(result)} will await callback with ${orderContractCode}`)
                            await callback(orderContractCode);
                        }
                    } else {
                        console.log("error Code: " + result.RtnCode)
                        //SetIsShowLoading(false) 
                        var errMsg = '';
                        if (result.RtnCode === -1) {
                            if (type === OrderActionType.NEW_ORDER) {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.orderFailure'), message: ''})
                            } else if (type === OrderActionType.CANCEL_ORDER) {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.cancelOrderFailure'), message: ''})
                            } else if (type === OrderActionType.CLOSE_POSITION) {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.closePositionFailure'), message: ''})
                            } else if (type === OrderActionType.EDIT_POSITION) {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.editPositionFailure'), message: ''})
                            } else if (type === OrderActionType.EDIT_ORDER) {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.modifyOrderFailure'), message: ''})
                            } else {
                                SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.orderFailure'), message: ''})
                            }
                        } else if (result.RtnCode === 132) {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.132'), message: ''})
                        } 
                        else if (result.RtnCode === TradingResponseCode.RET_TIMEOUT) {
                            SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.timeoutTitle'), message: t('trade.error.timeoutContent')})
                        }
                        else if (result.RtnCode === TradingResponseCode.RET_NETWORK) {
                            SetIsShowMessageDialog({isShowDialog: true, title:t('trade.error.networkTitle'), message: t('trade.error.networkContent')})
                        }
                        else {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.failed'), message: t('trade.error.' + result.RtnCode)})
    
                            // SetIsShowMessageDialog({isShowDialog: true, 
                            //     title: t('trade.error.rejectedOrder'), titleStyle: {color: colors.Red, paddingTop: 20}, 
                            //     message: t('trade.error.' + result.RtnCode), messageStyle: {color: colors.Red},
                            //     component: componentView,
                            //     isShowBtn: false, isShowCancel: true})
                        }
                    }
                }
            }
            finally {
                setEnableButton(true);
            }
        }
    }

    const buttonTitle = () => {
        switch (type) {
            case OrderActionType.NEW_ORDER:
                return orderIsBuy ? t('trade.buy') : t('trade.sell')
            case OrderActionType.CLOSE_POSITION:
                return t('trade.closePosition')
            case OrderActionType.CANCEL_ORDER:
                return t('trade.cancelOrder')
            case OrderActionType.EDIT_POSITION:
                return t('trade.editPosition')
                case OrderActionType.EDIT_ORDER:
                return t('trade.editOrder')
            default:
                return orderIsBuy ? t('trade.buy') : t('trade.sell')
        }
    }

    const orderTypeDisplay = () => {
        if (type === OrderActionType.NEW_ORDER || type === OrderActionType.EDIT_POSITION){
            if (isPending) {
                if (pendingOrderType === PendingOrderType.LIMIT && orderIsBuy) {
                    return t('confirmTrade.type.buyLimit');
                } else if (pendingOrderType === PendingOrderType.LIMIT && !orderIsBuy) {
                    return t('confirmTrade.type.sellLimit');
                } else if (pendingOrderType === PendingOrderType.STOP && orderIsBuy) {
                    return t('confirmTrade.type.buyStop');
                } else if (pendingOrderType === PendingOrderType.STOP && !orderIsBuy) {
                    return t('confirmTrade.type.sellStop');
                }
            }
            else {
                return t('confirmTrade.type.marketOrder');
            }
        } else if (type === OrderActionType.CLOSE_POSITION) {
            return t('confirmTrade.type.marketOrder');
        } else if (type === OrderActionType.CANCEL_ORDER || type === OrderActionType.EDIT_ORDER){
            if ((orderPendingOrderType === PendingOrderType.LIMIT && orderIsBuy ) || orderPendingOrderType === PendingOrderBuySellType.BUY_LIMIT) {
                return t('confirmTrade.type.buyLimit');
            } else if ((orderPendingOrderType === PendingOrderType.LIMIT && !orderIsBuy ) || orderPendingOrderType === PendingOrderBuySellType.SELL_LIMIT) {
                return t('confirmTrade.type.sellLimit');
            } else if ((orderPendingOrderType === PendingOrderType.STOP && orderIsBuy ) || orderPendingOrderType === PendingOrderBuySellType.BUY_STOP) {
                return t('confirmTrade.type.buyStop');
            } else if ((orderPendingOrderType === PendingOrderType.STOP && !orderIsBuy ) || orderPendingOrderType === PendingOrderBuySellType.SELL_STOP) {
                return t('confirmTrade.type.sellStop');
            }
        }
        return '';
    }

    const toggleBackdrop = () => {
        // SetIsShowLoading(false);
        // setShow(false)
    }

    var product = productName(displayContractCode(orderInstrument));
    // if (isEmpty(product)) {
    //     product = displayContractCode(orderInstrument);
    // } 

    return (
        <View style={[styles.container]}>
            {/* {console.debug('orderConfirmDialog')} */}
            <Dialog isVisible={isShowDialog} overlayStyle={styles.overlayStyle} onBackdropPress={toggleBackdrop}>
                <View>
                    { 
                        isShowDisclaimer ? 
                            <View>
                                <TextLabel label={t('confirmTrade.disclaimerTitle')} labelStyle={globalStyles.H2}/>
                                <ScrollView style={{maxHeight: 300}}>
                                    <View style={{padding: 8}}/>
                                    <TextLabel label={t('confirmTrade.disclaimerBody')}/>
                                    <View style={{padding: 15}}/>
                                </ScrollView>
                                <View style={{ padding: 5 }} />
                                <View style={styles.bottomButton}>
                                    <ActionButton
                                        title={t('confirmTrade.accept')}
                                        onPress={()=>{
                                            SetIsChecked(true)
                                            SetIsShowDisclaimer(false)
                                        }}
                                    />  
                                    <View style={{ padding: 10 }} />
                                    <PressableComponent onPress={()=>SetIsShowDisclaimer(false)}>
                                        <Remove style={{height: 32, width: 32}} color={colors.MainFont} />
                                    </PressableComponent>
                                </View>
                            </View> 
                        :
                            <View>
                                <TextLabel label={t('confirmTrade.title')} labelStyle={globalStyles.H2}/>
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productCode')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{displayContractCode(orderInstrument)}</Text>
                                </View>
                                {
                                    isEmpty(product)?
                                    null:
                                    <View style={styles.contentItem}>
                                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.productName')}</Text>
                                        <DashedLineView style={{paddingHorizontal: 10}} />
                                        <Text style={[globalStyles.Body_Text_B]}>{product}</Text>
                                    </View>
                                }
                                {
                                    (type === OrderActionType.NEW_ORDER && isPending) || type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER ?
                                    <View style={styles.contentItem}>
                                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.orderType')}</Text>
                                        <DashedLineView style={{paddingHorizontal: 10}} />
                                        <Text style={[globalStyles.Body_Text_B]}>{orderTypeDisplay()}</Text>
                                    </View> : null
                                }
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.direction')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B, orderIsBuy ? {color: colors.Green} : {color: colors.Red}]}>{orderIsBuy ? t('trade.buy') : t('trade.sell')}</Text>
                                </View>
                                {
                                    ((type === OrderActionType.NEW_ORDER && isPending) || type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER) ?
                                    <View style={styles.contentItem}>
                                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.validity')}</Text>
                                        <DashedLineView style={{paddingHorizontal: 10}} />
                                        <Text style={[globalStyles.Body_Text_B]}>{penderOrderValidity === 1 ? t('trade.today') : t('trade.gtc') }</Text>
                                    </View> : null
                                }
                                {
                                    type === OrderActionType.CLOSE_POSITION || type === OrderActionType.EDIT_POSITION || type === OrderActionType.CANCEL_ORDER || type === OrderActionType.EDIT_ORDER ?
                                    <View style={styles.contentItem}>
                                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.transactionId')}</Text>
                                        <DashedLineView style={{paddingHorizontal: 10}} />
                                        <Text style={[globalStyles.Body_Text_B]}>{transactionId}</Text>
                                    </View> : null
                                }
                                <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.volume')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{orderVolume.toFixed(orderVolumeDP)} {t('confirmTrade.lots')}</Text>
                                </View>
                                {/* <View style={styles.contentItem}>
                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.contractValue')}</Text>
                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                    <Text style={[globalStyles.Body_Text_B]}>{amountCurrency} {toCurrencyDisplay(amount)}</Text>
                                </View> */}
                                <ContractValueComponent symbol={orderContractCode} currentLot={orderVolume} isSelectBuy={type === OrderActionType.CLOSE_POSITION ? position && position.Type === 1 : orderIsBuy} isPending={isPending || type === OrderActionType.EDIT_POSITION} pendingOrderPrice={orderPrice}/>
                                {
                                    type === OrderActionType.CLOSE_POSITION || type === OrderActionType.EDIT_POSITION ?
                                    <View style={styles.contentItem}>
                                        <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.openPrice')}</Text>
                                        <DashedLineView style={{paddingHorizontal: 10}} />
                                        <Text style={[globalStyles.Body_Text_B]}>{(position ? position.OpenPrice : 0).toFixed(orderPriceDP)}</Text>
                                    </View> : null
                                }
                                {
                                    type === OrderActionType.NEW_ORDER || type === OrderActionType.EDIT_POSITION || type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER? 
                                    <View>
                                        {
                                            type === OrderActionType.NEW_ORDER || type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER? 
                                            <View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{type === OrderActionType.EDIT_ORDER || type === OrderActionType.CANCEL_ORDER? t('confirmTrade.targetPrice') : t('confirmTrade.Price')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{orderPrice.toFixed(orderPriceDP)}</Text>
                                                </View>
                                                {/* <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.estimatedMargin')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{amountCurrency} {toCurrencyDisplay(requiredMargin)}</Text>
                                                </View> */}
                                                <MarginValueComponent symbol={orderContractCode} currentLot={orderVolume} isSelectBuy={orderIsBuy} isPending={isPending} price={orderPrice} isRequiredMargin={false} marginMode={contractSymbols && contractSymbols.MarginMode} />
                                            </View> : null
                                        } 
                                        {
                                            type === OrderActionType.EDIT_POSITION ? 
                                            <View>
                                                <View style={styles.contentItem}>
                                                    <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.currentPrice')}</Text>
                                                    <DashedLineView style={{paddingHorizontal: 10}} />
                                                    <Text style={[globalStyles.Body_Text_B]}>{orderIsBuy ? sellPrice.toFixed(orderPriceDP) : buyPrice.toFixed(orderPriceDP)}</Text>
                                                </View>
                                            </View> : null
                                        }
                                        
                                        <View style={styles.contentItem}>
                                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.stopLossPrice')}</Text>
                                            <DashedLineView style={{paddingHorizontal: 10}} />
                                            <Text style={[globalStyles.Body_Text_B]}>{type === OrderActionType.CANCEL_ORDER && order ? (order.SL > 0 ? order.SL.toFixed(symbol.Digits) : t('positionDetails.na')) : (isProfit && stopLossPrice > 0 ? (stopLossPrice.toFixed(orderPriceDP)): t('positionDetails.na'))}</Text>
                                        </View>
                                        {
                                            profitType && profitType.type !== ProfitStopLossOrderType.PRICE && stopLossPricePoint > 0?
                                            <View style={styles.contentItem}>
                                                <Text style={[globalStyles.Body_Text_B, {width: '100%', textAlign: 'right'}]}>({stopLossPricePoint}{profitType.type === ProfitStopLossOrderType.PIPS ? t('confirmTrade.points') : '%'})</Text>
                                            </View> : null
                                        }
                                        <View style={styles.contentItem}>
                                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.takeProfitPrice')}</Text>
                                            <DashedLineView style={{paddingHorizontal: 10}} />
                                            <Text style={[globalStyles.Body_Text_B]}>{type === OrderActionType.CANCEL_ORDER && order ? (order.TP > 0 ? order.TP.toFixed(symbol.Digits) : t('positionDetails.na')) : (isProfit && limitProfitPrice > 0 ? limitProfitPrice.toFixed(orderPriceDP): t('positionDetails.na'))}</Text>
                                        </View>
                                        {
                                            profitType && profitType.type !== ProfitStopLossOrderType.PRICE && limitProfitPricePoint > 0?
                                            <View style={styles.contentItem}>
                                                <Text style={[globalStyles.Body_Text_B, {width: '100%', textAlign: 'right'}]}>({limitProfitPricePoint}{profitType.type === ProfitStopLossOrderType.PIPS ? t('confirmTrade.points') : '%'})</Text>
                                            </View> : null
                                        }
                                    </View>
                                    : 
                                    null
                                }
                                {
                                    type === OrderActionType.CLOSE_POSITION ?
                                    <View>
                                        <View style={styles.contentItem}>
                                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.currentPrice')}</Text>
                                            <DashedLineView style={{paddingHorizontal: 10}} />
                                            <Text style={[globalStyles.Body_Text_B]}>{position && position.Type === 1 ? buyPrice.toFixed(orderPriceDP) : sellPrice.toFixed(orderPriceDP)}</Text>
                                        </View>
                                    </View>
                                    : null
                                }
                                {
                                    type === OrderActionType.CLOSE_POSITION?
                                    <View>
                                        <View style={styles.contentItem}>
                                            <Text style={[globalStyles.Body_Text]}>{t('confirmTrade.pl')}</Text>
                                            <DashedLineView style={{paddingHorizontal: 10}} />
                                            <Text style={[globalStyles.Body_Text_B, {color: plColor}]}>{pl}</Text>
                                        </View>
                                    </View>
                                    : null
                                }

                                {
                                loginLevel === 3 &&
                                <View style={styles.contentItem}>
                                    <InputCheckBox label={t('confirmTrade.dontShow')} value={isChecked} onPress={() => { 
                                        isChecked ? SetIsChecked(false) : SetIsShowDisclaimer(true)
                                    }} textStyle={globalStyles.Note}/>
                                </View>
                                }

                                <View style={{ padding: 5 }} />
                                <View style={styles.bottomButton}>
                                    <ActionButton
                                        title={buttonTitle()}
                                        onPress={onPressOrder}
                                        isEnable={enableButton}
                                    />  
                                    <View style={{ padding: 10 }} />
                                    <PressableComponent onPress={onPressBackBtn}>
                                        <Remove style={{height: 32, width: 32}} color={colors.MainFont} />
                                    </PressableComponent>
                                </View>
                            </View>
                    }
                </View>
            </Dialog>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
    },
    overlayStyle:{
        width: '80%',
        borderRadius: 12,
        backgroundColor: colors.MainBg,
    },
    dialog: {
        backgroundColor: '#FFFFFF'
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    },
    bottomButton: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default React.memo(OrderConfirmDialog)