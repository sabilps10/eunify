import React from 'react'
import { Pressable, StyleSheet, View } from 'react-native'
import PressableComponent from '../views/utils/PressableComponent'

interface GrayOverlayInterface {
    touchable?: boolean,
    onPress?: () => void
}

const GrayOverlay = ({ touchable = false, onPress = () => { } }: GrayOverlayInterface) => {

    return (
        
        <View
            pointerEvents={touchable ? "auto" : "none"}
            style={[styles.container, { opacity: 0.5 }]}
        >
            <PressableComponent style={{ flex: 1 }} onPress={() => onPress()} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: '#000',
        zIndex: 9
    },
});

export default GrayOverlay