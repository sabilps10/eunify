import React, { useState, useEffect, Dispatch, SetStateAction, useRef } from 'react';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { Alert, Image, Pressable, StyleSheet, Text, ToastAndroid, useColorScheme, View, Platform, Linking } from 'react-native';
import {
    PORTRAIT,
    OrientationType,
    useOrientationChange,
} from 'react-native-orientation-locker';
import Orientation from 'react-native-orientation-locker';

import { WebView } from 'react-native-webview'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import * as RNLocalize from "react-native-localize";
import { store } from '../../redux/store';
import { Status } from '../../redux/connection/type';
import { chartAPI, chartWSPath } from '../../utils/api/apiSetter';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import DeviceInfo from 'react-native-device-info'
import { applauncherBase } from '../../config/constants';

export enum IntervalType {
    ONE_MIN = '1m',
    FIVE_MIN = '5m',
    FIFTTEEN_MIN = '15m',
    THIRTY_MIN = '30m',
    ONE_HOUR = '1h',
    FOUR_HOUR = '4h',
    ONE_DAY = 'D',
    ONE_WEEK = "W"
}

interface ChartComponentInterface {
    contract: string,
    interval: string,
    orientation: OrientationType
}

const ChartComponent = ({ contract, interval = IntervalType.ONE_DAY, orientation }: ChartComponentInterface) => {

    const { colors } = useTheme();
    const [t, i18n] = useTranslation();

    const access_token = useSelector((state: State) => state.account.account?.AccessToken);
    const connectionStatus = useSelector((state: State) => state.connection.webSocketStatus);
    const appTheme = useSelector((state: State) => state.state.setting.Theme)
    const savedWidgetContent = useSelector((state: State) => state.state.chartSetting && state.state.chartSetting[contract]);

    const dispatch = useDispatch();
    const { SetChartSetting } = bindActionCreators(StateActionCreators, dispatch);

    let webref = useRef<WebView>();
    let theme = appTheme === 'dark' ? 'Dark' : 'Light'

    let widget = '';
    if (orientation === PORTRAIT) {
        widget = ` 
            var html = createHeaderButton('5m', '5m', function(event) {
                changeResolution("5m");
            }, { align: "left", useTradingViewStyle: true });
            
            var html = createHeaderButton('1h', '1h', function(event) {
                changeResolution("1h");
            }, { align: "left", useTradingViewStyle: true });
            
            var html = createHeaderButton('4h', '4h', function(event) {
                changeResolution("4h");
            }, { align: "left", useTradingViewStyle: true });             

            tvWidget.createDropdown(
            {
                title: '1m',
                tooltip: 'header-toolbar-resolution-dropdown',
                items: [
                    {
                        title: '${t('chart.chartInterval.1m')}',
                        onSelect: () => {
                            changeResolution("1m");
                        },
                    },
                    {
                        title: '${t('chart.chartInterval.15m')}',
                        onSelect: () => {
                            changeResolution("15m");
                        },
                    },
                    {
                        title: '${t('chart.chartInterval.30m')}',
                        onSelect: () => {
                            changeResolution("30m");
                        },
                    },
                    {
                        title: '${t('chart.chartInterval.D')}',
                        onSelect: () => {
                            changeResolution("D");
                        },
                    },
                    {
                        title: '${t('chart.chartInterval.W')}',
                        onSelect: () => {
                            changeResolution("W")
                        },
                    }
                ],
                icon: '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="7"><path id="arrow_down" d="M216,213.286,220.375,209,216,204.714l.875-1.714L223,209l-6.125,6Z" transform="translate(215 -216) rotate(90)" fill="currentColor"/></svg>',
            });
                    
            const dropDownElement = innerDoc.querySelector('[title=header-toolbar-resolution-dropdown]');
            dropDownElement.parentNode.parentNode.style.marginLeft = "0px";
            dropDownElement.childNodes[0].insertAdjacentElement('beforebegin', dropDownElement.childNodes[1]);
            if (selected_resolution === "1m" || selected_resolution === "15m" || selected_resolution === "30m" || selected_resolution === "D" || selected_resolution === "W")
            {
                dropDownElement.childNodes[0].innerHTML = selected_resolution;
                dropDownElement.childNodes[0].style.color = "${colors.Brand3.toString()}";
                dropDownElement.childNodes[1].innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="7"><path id="arrow_down" d="M216,213.286,220.375,209,216,204.714l.875-1.714L223,209l-6.125,6Z" transform="translate(215 -216) rotate(90)" fill="${colors.Brand3.toString()}"/></svg>';
            }

            var html = createHeaderButton('<svg id="chart_fullscreen" xmlns="http://www.w3.org/2000/svg" width="28" height="28"><rect id="Rectangle_936" data-name="Rectangle 936" width="28" height="28" fill="none"/><g id="Group_591" data-name="Group 591" transform="translate(-429.553 -208.007)"><path id="Path_1304" data-name="Path 1304" d="M441.845,216.007v-1h-5.292V220.3h1V216.77l3.875,3.876.763-.763-3.876-3.876Z" fill="currentColor"/><path id="Path_1305" data-name="Path 1305" d="M447.427,215.007v1h3.53l-3.876,3.876.763.763,3.876-3.876V220.3h1v-5.292Z" fill="currentColor"/><path id="Path_1306" data-name="Path 1306" d="M451.72,229.412l-3.876-3.876-.763.763,3.875,3.876h-3.529v1h5.293v-5.292h-1Z" fill="currentColor"/><path id="Path_1307" data-name="Path 1307" d="M441.428,225.536l-3.875,3.875v-3.529h-1v5.292h5.292v-1h-3.529l3.875-3.876Z" fill="currentColor"/></g></svg>', 'fullscreen', function(event) {
                const response = { type: "fullscreen" }
                window.ReactNativeWebView.postMessage(JSON.stringify(response));
            }, { align: "right", useTradingViewStyle: true });

            const changeResolution = function(resolution) {    
                selected_resolution = resolution;
                var elements = innerDoc.querySelectorAll('[data-name=header-toolbar-resolution]');
                elements.forEach((elem) => {
                    if (elem.innerHTML === selected_resolution)
                    {
                        elem.style.color = "${colors.Brand3.toString()}";
                    }
                    else
                    {
                        elem.style.color = null;
                    }
                }); 
                
                if (selected_resolution === "1m" || selected_resolution === "15m" || selected_resolution === "30m" || selected_resolution === "D" || selected_resolution === "W")
                {
                    dropDownElement.childNodes[0].innerHTML = selected_resolution;
                    dropDownElement.childNodes[0].style.color = "${colors.Brand3.toString()}";
                    dropDownElement.childNodes[1].innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="7"><path id="arrow_down" d="M216,213.286,220.375,209,216,204.714l.875-1.714L223,209l-6.125,6Z" transform="translate(215 -216) rotate(90)" fill="${colors.Brand3.toString()}"/></svg>';
                }
                else
                {
                    dropDownElement.childNodes[0].style.color = null;
                    dropDownElement.childNodes[1].innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="7"><path id="arrow_down" d="M216,213.286,220.375,209,216,204.714l.875-1.714L223,209l-6.125,6Z" transform="translate(215 -216) rotate(90)" fill="currentColor"/></svg>';
                }
                tvWidget.chart().setResolution(convertResoultion(selected_resolution), function() {
                });
            } 

        `
    }
    else
    {
        widget = `
            var html = createHeaderButton('1m', '1m', function(event) {
                changeResolution("1m");
            }, { align: "left", useTradingViewStyle: true });

            var html = createHeaderButton('5m', '5m', function(event) {
                changeResolution("5m");
            }, { align: "left", useTradingViewStyle: true });

            var html = createHeaderButton('15m', '15m', function(event) {
                changeResolution("15m");
            }, { align: "left", useTradingViewStyle: true });

            var html = createHeaderButton('30m', '30m', function(event) {
                changeResolution("30m");
            }, { align: "left", useTradingViewStyle: true });
            
            var html = createHeaderButton('1h', '1h', function(event) {
                changeResolution("1h");
            }, { align: "left", useTradingViewStyle: true });
            
            var html = createHeaderButton('4h', '4h', function(event) {
                changeResolution("4h");
            }, { align: "left", useTradingViewStyle: true });    

            var html = createHeaderButton('D', 'D', function(event) {
                changeResolution("D");
            }, { align: "left", useTradingViewStyle: true });    

            var html = createHeaderButton('W', 'W', function(event) {
                changeResolution("W");
            }, { align: "left", useTradingViewStyle: true });    

            var html = createHeaderButton('<svg id="chart_exitfullscreen" xmlns="http://www.w3.org/2000/svg" width="28" height="28"><rect id="Rectangle_935" data-name="Rectangle 935" width="28" height="28" fill="none"/><g id="Group_590" data-name="Group 590" transform="translate(-458.053 -208.007)"><path id="Path_1300" data-name="Path 1300" d="M465.4,219.646v1h5.292v-5.291h-1v3.528l-3.875-3.876-.763.763,3.876,3.876Z" fill="currentColor"/><path id="Path_1301" data-name="Path 1301" d="M480.874,220.646v-1h-3.53l3.876-3.876-.763-.763-3.876,3.877v-3.529h-1v5.291Z" fill="currentColor"/><path id="Path_1302" data-name="Path 1302" d="M476.581,227.3l3.876,3.876.763-.763-3.875-3.876h3.529v-1h-5.293v5.292h1Z" fill="currentColor"/><path id="Path_1303" data-name="Path 1303" d="M465.815,231.174l3.875-3.875v3.529h1v-5.292H465.4v1h3.529l-3.875,3.876Z" fill="currentColor"/></g></svg>', 'exitfullscreen', function(event) {
                const response = { type: "exitfullscreen" }
                window.ReactNativeWebView.postMessage(JSON.stringify(response));
            }, { align: "right", useTradingViewStyle: true });

            const changeResolution = function(resolution) {    
                selected_resolution = resolution;
                var elements = innerDoc.querySelectorAll('[data-name=header-toolbar-resolution]');
                elements.forEach((elem) => {
                    if (elem.innerHTML === selected_resolution)
                    {
                        elem.style.color = "${colors.Brand3.toString()}";
                    }
                    else
                    {
                        elem.style.color = null;
                    }
                }); 
                tvWidget.chart().setResolution(convertResoultion(selected_resolution), function() {
                });
            } 
        `
    }
	
	const convertFromResolution = (resolution) => {
        if (resolution === "1")
            return IntervalType.ONE_MIN;
        else if (resolution === "5")
            return IntervalType.FIVE_MIN;
        else if (resolution === "15")
            return IntervalType.FIFTTEEN_MIN;
        else if (resolution === "30")
            return IntervalType.THIRTY_MIN;
        else if (resolution === "60")
            return IntervalType.ONE_HOUR;
        else if (resolution === "240")
            return IntervalType.FOUR_HOUR;
        else if (resolution === "1D")
            return IntervalType.ONE_DAY;
        else if (resolution === "1W")
            return IntervalType.ONE_WEEK;
        else
            return IntervalType.ONE_DAY;
    }
    
    const state: State = store.getState();
    const digits = state.trading.symbols[contract]? state.trading.symbols[contract].Digits : 3; 
    const language = state.state.setting.UserLanguage === 'tc' ? 'zh_TW' : state.state.setting.UserLanguage === 'sc' ? 'zh' : 'en';
    let saved;
    if (savedWidgetContent)
    {
        saved = JSON.parse(savedWidgetContent)
        console.debug(savedWidgetContent)
        const resolution = saved.charts[0]?.panes[0]?.sources[0]?.state?.interval
        console.debug("resolution: " + resolution)
        interval = resolution
        //if (resolution)
        //    interval = convertFromResolution(resolution)
            
    }
    
    console.debug(`${orientation}, ${RNLocalize.getTimeZone()}, ${contract}, ${Math.pow(10, digits)}, ${interval}, ${theme}, ${chartAPI}, ${access_token}`)

    const jsToInject = `
        initOnReady("${orientation}", "${contract}", ${Math.pow(10, digits)}, "${interval}", "${theme}", "${chartAPI}", "${chartWSPath}", "${access_token}", "${RNLocalize.getTimeZone()}", ${JSON.stringify(saved)}, "${language}");
        
        function convertResoultion(interval)
        {
            if (interval === "1m")
                return "1";
            else if (interval === "5m")
                return "5";
            else if (interval === "15m")
                return "15";
            else if (interval === "30m")
                return "30";
            else if (interval === "1h")
                return "1h";
            else if (interval === "4h")
                return "4h";
            else if (interval === "D")
                return "1D";
            else if (interval === "W")
                return "1W"
            else
                return null;
        }
        tvWidget.onChartReady(function() {
            var chartContainer = document.getElementById('tv_chart_container');
            var iframe = chartContainer.firstChild;
            var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
            let selected_resolution = "${convertFromResolution(interval)}";

            var legendElement = innerDoc.querySelector('[data-name=legend-series-item]');
            if(legendElement)
                legendElement.firstChild.remove()

            tvWidget.chart().onIntervalChanged().subscribe(
                null,
                function(interval) {
                    const response = { type: "onAutoSave" }
                    window.ReactNativeWebView.postMessage(JSON.stringify(response));
                }
            );
            tvWidget.subscribe("drawing_event", function() {
                const response = { type: "onAutoSave" }
                window.ReactNativeWebView.postMessage(JSON.stringify(response));
            });
            tvWidget.subscribe("study_event", function() {
                const response = { type: "onAutoSave" }
                window.ReactNativeWebView.postMessage(JSON.stringify(response));
            });
            tvWidget.subscribe("onAutoSaveNeeded", function() {
                const response = { type: "onAutoSave" }
                window.ReactNativeWebView.postMessage(JSON.stringify(response));
            });
            tvWidget.headerReady().then(function () {
                function createHeaderButton(text, title, clickHandler, options) {
                    var button = tvWidget.createButton(options);
                    
                    if (options.align === "left")
                    {
                        button.parentNode.parentNode.style.marginLeft = "0px";	
                    }
                    button.style.paddingLeft = "5px";	
                    button.style.paddingRight = "5px";	
                    button.setAttribute("data-name", "header-toolbar-resolution");
                    button.setAttribute('title', title);
                    button.innerHTML = text;
                    if (text === selected_resolution)
                    {
                        button.style.color = "${colors.Brand3.toString()}";
                    }
                    button.addEventListener('click', clickHandler);
                }

                ${widget}
                
                var chartTypeElem = innerDoc.querySelector('#header-toolbar-chart-styles')
                                  || innerDoc.querySelector('[data-name=header-toolbar-chart-styles]');
                                  
                var indicatorElem = innerDoc.querySelector('#header-toolbar-indicators')
                          || innerDoc.querySelector('[data-name=open-indicators-dialog]');
                          
                indicatorElem.lastChild.childNodes[1].remove();
                
                var elemTo = innerDoc.querySelector('#header-toolbar-properties')
                                  || innerDoc.querySelector('[data-name=header-toolbar-properties]');
                                  
                elemTo.parentNode.insertAdjacentElement('beforebegin', chartTypeElem.parentNode);
                elemTo.parentNode.insertAdjacentElement('beforebegin', indicatorElem.parentNode);
                
         
            });
            const response = { type: "onChartReady" }
            window.ReactNativeWebView.postMessage(JSON.stringify(response));
        });
        true; // note: this is required, or you'll sometimes get silent failures 
            // (https://github.com/react-native-webview/react-native-webview/blob/master/docs/Guide.md)
    `;

    const onPressSaveChart = () => {
        const script = `
                tvWidget.save(function(data) {
                    const response = { type: "save", data: data }
                    window.ReactNativeWebView.postMessage(JSON.stringify(response));
                });
                true;
            `;
        
        webref.current?.injectJavaScript(script);
        
    }

    const onDisconnect = () => {
        setTimeout(() => {
            if (webref.current)
            {
                webref.current.injectJavaScript(jsToInject);
            }
        }, 2000);
    }

    const onPressFullScreen = () => {
        console.debug("onPressChangeFullScreen");
        Orientation.lockToLandscapeLeft(); //this will lock the view to Landscape
    }

    const onPressExitFullScreen = () => {
        Orientation.lockToPortrait(); //this will lock the view to Portrait
    }

    let chartUrl = '';
    if (Platform.OS === 'android')
    {
        chartUrl = `file:///android_asset/index.html`;
        const systemVersion = parseInt(DeviceInfo.getSystemVersion().split(".")[0]);
        if (systemVersion < 10)
            chartUrl = applauncherBase + 'tradingview/index.html';
    }
    else
    {
        chartUrl = `charting/index.html`;
    }

    console.debug("chart render: " + chartUrl);

    return (
        <>
            {
                connectionStatus === Status.Connected && access_token?
                    Platform.OS === "android" ?
                        <WebView
                            nestedScrollEnabled={true}
                            scrollEnabled={false}
                            ref={(r) => (webref.current = r)}
                            style={{ flex: 1 }}
                            source={{ uri: chartUrl }}
                            allowFileAccessFromFileURLs={true}
                            domStorageEnabled={true}
                            allowFileAccess={true}
                            allowUniversalAccessFromFileURLs={true}
                            originWhitelist={['file://*', 'https://*', 'http://*']}
                            injectedJavaScript={jsToInject}
                            onShouldStartLoadWithRequest={(e) => {
                                console.debug("LoadRequest: " + e.url)
                                if (e.url.includes("blank")) {
                                    webref.current?.goBack();
                                    return false;
                                }
                                return true;
                            }}
                            onMessage={(event) => {
                                const dataPayload = JSON.parse(event.nativeEvent.data)
                                if (dataPayload.type === 'Console') {
                                    console.info(`[Console] ${JSON.stringify(dataPayload.data)}`);
                                }
                                else if (dataPayload.type === "save") {
                                    SetChartSetting(contract, JSON.stringify(dataPayload.data));
                                }
                                else if (dataPayload.type === "onChartReady") {
                                    console.debug("onChartReady")
                                }
                                else if (dataPayload.type === "onAutoSave") {
                                    onPressSaveChart();
                                    console.debug("onAutoSave");
                                }
                                else if (dataPayload.type === "fullscreen") {
                                    onPressFullScreen();
                                }
                                else if (dataPayload.type === "exitfullscreen") {
                                    onPressExitFullScreen();
                                }
                                else if (dataPayload.type === "disconnect") {
                                    console.debug("disconnect");
                                    onDisconnect();
                                }
                            }}
                        /> 
                    :
                        <WebView
                            scrollEnabled={false}
                            ref={(r) => (webref.current = r)}
                            style={{ flex: 1 }}
                            source={{ uri: chartUrl }}
                            allowFileAccessFromFileURLs={true}
                            originWhitelist={['file://*', 'https://*', 'http://*']}
                            injectedJavaScript={jsToInject}
                            onShouldStartLoadWithRequest={(e) => {
                                let url = e.url
                                console.debug('url:' + url)
                                
                                if (url.includes("blank")) {
                                    webref.current?.goBack()
                                    return false
                                }
                                if (url.includes('www.tradingview.com')) {
                                    Linking.openURL(url)
                                    return false
                                }
                                return true
                            }}
                            onMessage={(event) => {
                                const dataPayload = JSON.parse(event.nativeEvent.data)
                                if (dataPayload.type === 'Console') {
                                    console.info(`[Console] ${JSON.stringify(dataPayload.data)}`);
                                }
                                else if (dataPayload.type === "save") {
                                    SetChartSetting(contract, JSON.stringify(dataPayload.data));
                                }
                                else if (dataPayload.type === "onChartReady") {
                                    console.debug("onChartReady")
                                }
                                else if (dataPayload.type === "onAutoSave") {
                                    onPressSaveChart();
                                    console.debug("onAutoSave");
                                }
                                else if (dataPayload.type === "fullscreen") {
                                    onPressFullScreen();
                                }
                                else if (dataPayload.type === "exitfullscreen") {
                                    onPressExitFullScreen();
                                }
                                else if (dataPayload.type === "disconnect") {
                                    console.debug("disconnect");
                                    onDisconnect();
                                }
                            }}
                        />
                : 
                    <></>
            }

        </>

    )
}

export default React.memo(ChartComponent)