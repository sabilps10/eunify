import React, {Fragment, useRef, useEffect, useCallback, useState} from 'react';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {StyleSheet, TextStyle, View, ViewStyle} from 'react-native';
import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetScrollView,
} from '@gorhom/bottom-sheet';
import {useTheme} from '@react-navigation/native';
import SelectionItem from './SelectionItem.component';

import {Text} from 'react-native-paper';
import {makeGlobalStyles} from '../../styles/styles';
import {useDispatch, useSelector} from 'react-redux';
import {bindActionCreators} from 'redux';
import {BottomSheetCreators} from '../../redux/BottomSheet/actionCreators';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../../src/redux/root-reducer';

interface BottomSheetDropdownProps {
  itemStyle?: ViewStyle;
  itemTextStyle?: TextStyle;
}

const BottomSheetDropdown = ({
  itemStyle = {},
  itemTextStyle = {},
}: BottomSheetDropdownProps) => {
  const {label, isOpen, options, selected, onSelect} = useSelector(
    (state: State) => state.bottomSheet,
  );
  const dispatch = useDispatch();
  const {CloseBottomSheet, SetSelected} = bindActionCreators(
    BottomSheetCreators,
    dispatch,
  );
  const selectionRef = useRef<BottomSheet>(null);

  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const [snapIndex, setSnapIndex] = useState<0 | -1>(-1);

  useEffect(() => {
    if (selectionRef.current) {
      if (isOpen) {
        selectionRef.current.snapToIndex(0);
        setTimeout(() => {
          setSnapIndex(0);
        }, 1000);
      } else if (!isOpen) {
        selectionRef.current.close();
        setSnapIndex(-1);
      }
    }
  }, [isOpen]);

  const handleSheetChange = useCallback(index => {
    if (index === -1) {
      CloseBottomSheet();
      setSnapIndex(-1);
      selectionRef.current.close();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderBackdrop = useCallback(
    props => (
      <BottomSheetBackdrop
        {...props}
        appearsOnIndex={0}
        disappearsOnIndex={-1}
        pressBehavior={snapIndex === 0 ? 'close' : 'none'}
      />
    ),
    [snapIndex],
  );

  const handleSelect = (val: any) => {
    SetSelected(val);
    onSelect(val);
    CloseBottomSheet();
  };

  return (
    <Fragment>
      <BottomSheet
        handleIndicatorStyle={[
          styles.bottomSheetHorizontalLine,
          {backgroundColor: colors.LabelBtnFont},
        ]}
        ref={selectionRef}
        snapPoints={[300]}
        index={-1}
        backdropComponent={renderBackdrop}
        enablePanDownToClose={true}
        onChange={handleSheetChange}>
        <View style={{height: 10}} />
        <Text
          style={[
            styles.title,
            globalStyles.H4,
            {color: colors.LabelBtnSelectedBg},
          ]}>
          {label}
        </Text>
        <BottomSheetScrollView
          style={[
            styles.bottomSheetContainer,
            {backgroundColor: colors.MainBg},
          ]}>
          {options.map(item => (
            <SelectionItem
              key={item.key}
              item={item}
              isSelected={selected === item.key}
              onSelect={handleSelect}
              style={itemStyle}
              textStyle={itemTextStyle}
            />
          ))}
        </BottomSheetScrollView>
      </BottomSheet>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  title: {
    paddingHorizontal: 16,
    paddingVertical: 10,
  },
  bottomSheetContainer: {
    width: '100%',
    height: 300,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  bottomSheetHorizontalLine: {
    width: 68,
    height: 4.53,
    top: 8,
    borderRadius: 2,
    alignSelf: 'center',
  },
});

export default React.memo(BottomSheetDropdown);
