import React, { Fragment, useMemo, useState, useRef, useEffect, useCallback } from 'react';
import { BackHandler, FlatList, ScrollView, StyleSheet, TextStyle, View, ViewStyle } from 'react-native';
import BottomSheet, { BottomSheetBackdrop, BottomSheetScrollView, BottomSheetView } from "@gorhom/bottom-sheet";
import GrayOverlay from '../grayOverlay';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import SelectionItem from './SelectionItem.component';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../src/redux/root-reducer';
import { bindActionCreators } from 'redux';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { AccountUtils } from '../../utils/accountUtils';
import { Text } from 'react-native-paper';
import { makeGlobalStyles } from '../../styles/styles';
import { useTranslation } from 'react-i18next';
import { ConnectionActionCreators } from '../../redux/connection/actionCreator';
import { ActionUtils } from '../../views/utils/ActionUtils';
import analytics from '@react-native-firebase/analytics'
import GAScreenName from '../../json/GAScreenName.json'
import { SelectionContextLocation } from '../../redux/selection/type';

interface BottomSelectionInterface {
    snapPoints?: number[];
    itemStyle?: ViewStyle;
    itemTextStyle?: TextStyle;
    grayOverlay?: boolean;
}

const BottomSelection = (props: BottomSelectionInterface) => {

    const [t, i18n] = useTranslation();

    

    const selection = useSelector((state: State) => state.selection.selection);
    const dispatch = useDispatch();
    const { OpenCloseSelection, SetSelected } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);
    const { SetSwitchingConnection } = bindActionCreators(ConnectionActionCreators, dispatch);

    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);

    const { onPressSwitchAccount } = ActionUtils()

    const selectionRef = useRef<BottomSheet>(null);
    const {
        snapPoints = [300],
        itemStyle = {},
        itemTextStyle = {},
        grayOverlay = true,
    } = props;
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [title, setTitle] = useState<string>('')
    const [isOpen, setIsOpen] = useState<boolean>(false)

    useEffect(() => {
        // console.debug('selection.isOpen', selection.isOpen)
        if (selectionRef.current) {
            if (selection.isOpen && !isOpen) {
                setIsOpen(true)
                // console.debug('current.snapToIndex(0)', selectionRef.current)
                selectionRef.current.snapToIndex(0);
            } else if (!selection.isOpen) {
                setIsOpen(false)
                // console.debug('current.close()')
                selectionRef.current.close();
            }
        }
    }, [selection.isOpen, selectionRef.current]);

    useEffect(() => {
        if (!selection.location) return
        setTitle(t('selection.title.' + selection.location))
    }, [selection.location])

    // iOS 14 Analytics Tracking permission authorization request
    const allowTrack = useSelector((state: State) => state.state.allowTrack)
    // GA4 Tracking ScreeView
    useEffect(() => {
        // allowTrack?
        if (allowTrack && selection.isOpen) {
            const getScreenClassFor = (location: SelectionContextLocation) => {
                switch (location) {
                    case SelectionContextLocation.WatchlistCategory:
                    case SelectionContextLocation.SettingLanguage: return `BottomSelection+${location}`
                        break;
                }
            }
            let screenName: string, screenClass: string
            if ((screenClass = getScreenClassFor(selection.location)) && (screenName = GAScreenName[screenClass]) && screenName.length !== 0 && screenName.trim() !== '') {
                analytics().logScreenView({
                    screen_name: screenName,
                    screen_class: screenClass
                })
                console.debug(`[BottomSelection useEffect [selection.isOpen, selection.location, allowTrack]] analytics().logScreenView(${JSON.stringify({screen_name: screenName, screen_class: screenClass})})`)
            }
        }
    }, [selection.isOpen, selection.location, allowTrack])

    const onSelect = async (selected: string) => {
        SetSelected({
            [selection.location]: selected,
        });
        switch (selection.location) {
            case SelectionContextLocation.CurrentTradeAccount:
                var enitity = 'HK';
                for (var i = 0; i < tradingAccountList.length; i++) {
                    if (tradingAccountList[i].Account === selected) {
                        enitity = tradingAccountList[i].Entity
                        continue;
                    }
                }

                await onPressSwitchAccount(selected.toString(), enitity);
                break;
            default:
                break;
        }
        selectionRef.current.close();
    };

    const handleSheetChange = useCallback((index) => {
        if (index == -1)
            onBottomSheetClose();
    }, []);


    const onBottomSheetClose = () => {
        OpenCloseSelection({
            selectedOption: "",
            selectOptions: [],
            isOpen: false,
            location: null
        })
    };

    const renderBackdrop = useCallback(
        props => (
            isOpen ?
            <BottomSheetBackdrop
                {...props}
                appearsOnIndex={0}
                disappearsOnIndex={-1}
            />
            : null
        ), [isOpen]
    );

    return (
        <Fragment>
            <BottomSheet
                handleIndicatorStyle={[styles.bottomSheetHorizontalLine, {backgroundColor: colors.LabelBtnFont}]}
                ref={selectionRef}
                snapPoints={snapPoints}
                index={-1}
                backdropComponent={renderBackdrop}
                enablePanDownToClose={true}
                onChange={handleSheetChange}
            >
                <View style={{ height: 10 }} />
                <Text style={[styles.title, globalStyles.H4, { color: colors.LabelBtnSelectedBg }]}>{title}</Text>
                <BottomSheetScrollView
                    style={[styles.bottomSheetContainer, { backgroundColor: colors.MainBg }]}>
                    
                    {selection.selectOptions.map((item) => {
                        // console.debug(item.key)
                        return (
                            <SelectionItem
                                key={item.key}
                                item={item}
                                isSelected={selection.selectedOption === item.key}
                                onSelect={onSelect}
                                style={itemStyle}
                                textStyle={itemTextStyle}
                            />
                        );
                    })}
                </BottomSheetScrollView>
            </BottomSheet>
        </Fragment>
    );
};

const styles = StyleSheet.create({
    title: {
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    bottomSheetContainer: {
        width: '100%',
        height: 300,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    bottomSheetHorizontalLine: {
        width: 68,
        height: 4.53,
        top: 8,
        borderRadius: 2,
        alignSelf: 'center',
    },
});

export default React.memo(BottomSelection);
