import React from 'react';
import { Pressable, StyleSheet, Text, TextStyle, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTranslation } from 'react-i18next';
import { displayContractCode } from '../../views/utils/CommonFunction';
import ActionSheetTick from '../../../assets/svg/ActionSheetTick';
import PressableComponent from '../../views/utils/PressableComponent';


interface SelectionItemInterface {
    item: {key: string, value: string},
    isSelected: boolean,
    onSelect: (id: string) => void,
    style?: ViewStyle,
    textStyle?: TextStyle,
    isContract?: boolean
}


const SelectionItem = ({ item, isSelected = false, onSelect, style = {}, textStyle = {}, isContract = false}: SelectionItemInterface) => {
    const scheme = useColorScheme();
    const [t, i18n] = useTranslation();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPress = () => onSelect(item.key)
    
    return (
        <PressableComponent style={[styles.container, { backgroundColor: colors.MainBg }, style]} onPress={onPress}>
            <Text style={[
                isSelected ? globalStyles.Big_Text_B : globalStyles.Big_Text,
                isSelected ? { color: colors.TabSelectedFont } : { color: colors.MainFont },
                textStyle
            ]}>{isContract ? displayContractCode(item.value) : item.value}</Text>
            {
                isSelected  &&
                <View style={styles.selectedIconWrapper}>
                    <ActionSheetTick color={colors.Brand3} />
                </View>
            }
        </PressableComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16
    },
    selectedIconWrapper: {
        width: 24,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default SelectionItem
