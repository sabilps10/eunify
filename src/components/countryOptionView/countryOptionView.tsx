import { useTheme } from "@react-navigation/native"
import React, { useEffect, useLayoutEffect, useState } from "react"
import { Image, Pressable, StyleSheet, Text, useColorScheme, View } from "react-native"
import { useSelector } from "react-redux"
import { getIcon_back_24 } from "../../../assets/constants"
import { State } from "../../redux/root-reducer"
import { Props } from "../../routes/route"
import { makeGlobalStyles } from "../../styles/styles"
import Header from "../header/header.component"
import HeaderButton from "../header/headerButton.component"
import HeaderSearchBar from "../header/headerSearchBar.component"
import AlphabetList from "react-native-flatlist-alphabet"
import { SafeAreaView } from "react-native-safe-area-context"
import { useTranslation } from "react-i18next"
import en from "../../locales/en.json"
import { THEME_TYPE } from "../../types/types"
import { debounce } from "lodash"
import CountryOptionViewItem from "./countryOptionViewItem"
import TopBarBack from "../../../assets/svg/TopBarBack"

export enum CountryOptionType {
    REGION = 'REGION',
    COUNTRY = 'COUNTRY'
}

const CountryOptionView: React.FC<Props<'CountryOptionView'>> = ({ route, navigation }) => {

    const [t] = useTranslation()

    const { colors } = useTheme()
    const scheme = useColorScheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    const { type, onSelect } = route.params

    const regionOption = useSelector((state: State) => state.state.regionDataList)
    const selectedId = useSelector((state: State) => type === CountryOptionType.REGION ? state.state.setting.UserRegion ?? route.params.selectedId : route.params.selectedId)
    const [regionOptionMapping, setRegionOptionMapping] = useState<{id: string, index: string, value: string, countryValue: string, englishValue: string}[]>([])
    const [data, setData] = useState<{value: string, key: string, regionCode: string, countryCode: string}[]>()
    const [searchText, setSearchText] = useState<string>('')
    
    const searchDebounce = React.useRef(debounce(async (text) => { setSearchText(text)}, 500)).current

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => <Header
                style={{backgroundColor: colors.WrapperBg}}
                leftComponents={<HeaderButton 
                    onPress={onPressBack} 
                    iconComponent={<TopBarBack color={colors.MainFont} />} />}
                MiddleComponent={<HeaderSearchBar onChangeText={onChangeText} />}
                />
        })
    }, [navigation])

    useEffect(() => {
        console.debug('selectedId:' + selectedId)
    }, [selectedId])

    useEffect(() => {
        console.debug('searchText:' + searchText)

        var regionOptionMapping = regionOption
        // Create a mapping of 
        // `id`, current locale name
        // `index`, en locale name
        // `value`, region id
        .map((region) => {
            // return {id: region, index: en.region[region], value: t('region.' + region)}
            return {id: region.regionCode, index: en.region[region.regionCode], value: t('region.' + region.regionCode), countryValue: region.countryCode, englishValue: en.region[region.regionCode]}
        })
        if (searchText) {
            regionOptionMapping = regionOptionMapping.filter((item) => item.value.toUpperCase().includes(searchText.toUpperCase()) || item.englishValue.toUpperCase().includes(searchText.toUpperCase()))
        }
        setRegionOptionMapping(regionOptionMapping)
    }, [regionOption, searchText])

    useEffect(() => {
        let data: {value: string, key: string, regionCode: string, countryCode: string, displayValue: string}[] = regionOptionMapping
            // Sort by en locale name
            .sort((a, b) => {
                let aIndex = a.index
                let bIndex = b.index

                if (aIndex < bIndex) return -1
                if (aIndex > bIndex) return 1
                return 0
            })
            // Create a mapping of 
            // `key`, en locale name 
            // `value`, current locale name
            .map((item) => {
                return {value: item.englishValue, key: item.index, regionCode: item.id, countryCode: item.countryValue, displayValue: item.value}
            })
        // console.debug('data:' + JSON.stringify(data))
        setData(data)
    }, [regionOptionMapping])

    const onPressBack = () => {
        navigation.goBack()
    }

    const onChangeText = (value: string) => { searchDebounce(value) }

    const onPressItem = (item: {key: string, value: string, countryCode: string}) => {
        console.debug('item:' + JSON.stringify(item))
        let index = regionOptionMapping.findIndex((mapItem) => {
            return mapItem.index === item.key
        })
        if (index === -1) {
            console.debug('item not found...')
            return
        }
        console.debug('regionOptionMapping[index]:' + JSON.stringify(regionOptionMapping[index]))
        onSelect(type === CountryOptionType.REGION ? regionOptionMapping[index].id : regionOptionMapping[index].countryValue)
        navigation.goBack()
    }

    const renderCustomItem = (item: {key: string, value: string, regionCode: string, countryCode: string, displayValue: string}) => (
        <CountryOptionViewItem 
            item={item}
            onPressItem={onPressItem}
            value={type === CountryOptionType.REGION ? item.displayValue : item.displayValue + ' (+' + item.countryCode + ')'}
            isSelected={selectedId === (type === CountryOptionType.REGION ? item.regionCode : item.countryCode)} />
    )

    return (
        <SafeAreaView 
            style={styles.safeAreaView}
            edges={['bottom']}>
            <AlphabetList 
                style={[styles.alphabetlist]} 
                containerStyle={styles.indexContainerStyle} 
                letterItemStyle={{...globalStyles.Big_Text, ...styles.indexLetterStyle}} 
                indexLetterColor={colors.SectionTitleFont.toString()}
                data={data ?? []} 
                renderItem={renderCustomItem} 
                renderSectionHeader={() => null} 
                getItemHeight={() => 53} 
                sectionHeaderHeight={0}/>
        </SafeAreaView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: colors.WrapperBg,
    },
    alphabetlist: {
        flex: 1, 
        backgroundColor: colors.MainBg, 
        borderTopLeftRadius: 12,
    },
    contentContainerStyle: {
        paddingTop: 26,
    },
    indexContainerStyle: {
        backgroundColor: colors.WrapperBg, 
        width: 21,
        position: 'absolute',
        right: 0,
    },
    indexLetterStyle: {
        fontSize: 13,
        marginTop: 5,
        marginBottom: 5,
        height: 18,
    },
})

export default CountryOptionView