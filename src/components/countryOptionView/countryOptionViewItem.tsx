import { useTheme } from "@react-navigation/native"
import React from "react"
import { Pressable, StyleSheet, Text, useColorScheme, View } from "react-native"
import ActionSheetTick from "../../../assets/svg/ActionSheetTick"
import { makeGlobalStyles } from "../../styles/styles"
import { THEME_TYPE } from "../../types/types"
import PressableComponent from "../../views/utils/PressableComponent"
import StackView from "../stackView.component"

interface CountryOptionViewItemParams {
    item: {key: string, value: string, countryCode: string}
    onPressItem: (item: {key: string, value: string, countryCode: string}) => void
    value: string,
    isSelected: boolean,
}

const CountryOptionViewItem = (params: CountryOptionViewItemParams) => {

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const styles = makeStyles(colors)

    // useEffect(() => {
    //     console.debug('params.item.countryCode:' + params.item.countryCode + ',params.item.value:' + params.item.value + ',item.key:' + params.item.key)
    // }, [params.item.key])

    return (
        <StackView direction="column">
            <PressableComponent 
                style={styles.item}
                onPress={() => params.onPressItem(params.item)}>
                <StackView 
                    spacing={5} 
                    style={{alignItems: 'center', justifyContent: 'space-between', paddingRight: 21}}>
                <Text style={[globalStyles.Big_Text, {color: colors.Text}]}>
                    {params.value}
                </Text>
                {params.isSelected ? <ActionSheetTick color={colors.Brand3} /> : null}
                </StackView>
            </PressableComponent>
            <View style={styles.itemSeparator} />
        </StackView>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    item: {
        height: 52, 
        justifyContent: 'center', 
        marginHorizontal: 16,
    },
    itemSeparator: {
        height: 1,
        backgroundColor: colors.WrapperBg,
    },
})

export default CountryOptionViewItem