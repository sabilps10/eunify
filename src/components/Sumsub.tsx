import React, {useEffect, useState} from 'react';
import {Button, Text, View} from 'react-native';
// import Button from '../components/buttons/Button';

import axios from 'axios';
import {bindActionCreators} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import {StateActionCreators} from '../redux/state/actionCreator';

import SNSMobileSDK from '@sumsub/react-native-mobilesdk-module';
import {getSumsubToken} from '../utils/sumsub';
import {entityId, sumsub_url} from '../config/constants';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../redux/root-reducer';
import {getIndividual} from '../utils/api/apiAccountOpening';
import {Auth} from 'aws-amplify';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Sumsub = ({navigation}) => {
  const [isFinished, setIsFinished] = useState<boolean>(false);
  const [accessToken, setAccessToken] = useState<string>('');
  const dispatch = useDispatch();

  const customerId = useSelector(
    (state: State) => state.accountOpening.contactInformation.customerId,
  );

  const {currentToken, username} = useSelector((state: State) => state.cognito);

  const {SetIsShowLoading} = bindActionCreators(StateActionCreators, dispatch);

  const acceptedStatus = [
    'Pending',
    'TemporarilyDeclined',
    'FinallyRejected',
    'Approved',
  ];

  const launchSNSMobileSDK = async () => {
    // From your backend get an access token for the applicant to be verified.
    // The token must be generated with `levelName` and `userId`,
    // where `levelName` is the name of a level configured in your dashboard.
    //
    // The sdk will work in the production or in the sandbox environment
    // depend on which one the `accessToken` has been generated on.
    let snsMobileSDK = SNSMobileSDK.init(accessToken, () => {
      // this is a token expiration handler, will be called if the provided token is invalid or got expired
      // call your backend to fetch a new access token (this is just an example)
      // const data: any = getSumsubToken(cognitoUsername, entityId);
      // const {token} = data;
      // return token;
      const res: any = axios({
        url: sumsub_url,
        method: 'POST',
        data: {
          customerId,
          kycType: entityId === 'EIE' ? 'aoeiehk' : 'aoxpro',
        },
      });
      return res.data.token;
    })
      .withHandlers({
        // Optional callbacks you can use to get notified of the corresponding events
        onStatusChanged: event => {
          console.log(event, '<< on status change');
          if (acceptedStatus.includes(event.newStatus)) {
            navigation.navigate('AccountPersonalInformation');
          }
        },
        onEvent: event => {
          console.log(event, '<< on event');
          if (event.eventType != null) {
            if (
              event.eventType === 'StepCompleted' &&
              event.payload.idDocSetType === 'PROOF_OF_RESIDENCE'
            ) {
              //This is the end of EKYC
              snsMobileSDK.dismiss();
              setIsFinished(true);
              // navigation.navigate('AccountPersonalInformation');
            }
          }
        },
      })
      .withDebug(true)
      .withLocale('en') // Optional, for cases when you need to override the system locale
      .build();

    snsMobileSDK
      .launch()
      .then(result => {
        console.log(result, '<< result');
        if (acceptedStatus.includes(result.status)) {
          navigation.navigate('AccountPersonalInformation');
        }
      })
      .catch(err => {
        console.log('SumSub SDK Error: ' + JSON.stringify(err));
      });
  };

  useEffect(() => {
    const fetchToken = async () => {
      SetIsShowLoading(true);
      const data: any = await getSumsubToken(customerId, entityId);
      const {token} = data;
      setAccessToken(token);
      SetIsShowLoading(false);
    };
    if (customerId) {
      fetchToken();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customerId]);

  useEffect(() => {
    if (isFinished) {
      SetIsShowLoading(true);
      const interval = setInterval(async () => {
        const session = await Auth.currentSession();
        const sessionPayload = session.getIdToken().payload;

        getIndividual({
          token: currentToken,
          payload: {username: sessionPayload['cognito:username']},
          onSuccess: data => {
            if (data?.response?.aoOcrIdDoc !== null) {
              SetIsShowLoading(false);
              clearInterval(interval);
              navigation.navigate('AccountPersonalInformation');
            }
          },
          onFailed: () => {},
        });
      }, 2000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFinished]);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '30%',
      }}>
      {isFinished ? (
        <Text>Loading...</Text>
      ) : (
        <Button
          onPress={launchSNSMobileSDK}
          title="Launch SumSub SDK"
          disabled={!accessToken}
        />
      )}
    </View>
  );
};

export default Sumsub;
