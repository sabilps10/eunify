import React, { Fragment, useEffect, useState } from 'react';
import { StyleSheet, View, useColorScheme, ViewStyle, Text, TouchableOpacity, TextStyle, TextInput } from 'react-native';
import { makeGlobalStyles } from '../styles/styles';
import { useTheme } from '@react-navigation/native';
import ValueOrVolumeButton from './buttons/valueOrVolumeButton.component';
import Plus from '../../assets/svg/Plus';
import Minus from '../../assets/svg/Minus';
import PressableComponent from '../views/utils/PressableComponent';
import TouchableOpacityComponent from '../views/utils/TouchableOpacityComponent';

interface ArithmometerInterface {
    value: number,
    valueUpdate: boolean,
    valuePerStep: number,
    minimunValue: number,
    maximunValue: number,
    nextValidPrice?: number,
    onChangeValue: (value: number) => void,
    title?: string,
    buttons?: { label: string, value: number }[],
    style?: ViewStyle,
    valueStyle?: TextStyle,
    titleStyle?: TextStyle,
    headerComponent?: React.ReactNode,
    remark?: string,
    remarkStyle?: TextStyle,
    arithmometerStyle?: ViewStyle,
    dp?: number,
    isShowError?: boolean
}

const Arithmometer = ({
    value,
    valueUpdate = false,
    valuePerStep,
    minimunValue,
    maximunValue,
    nextValidPrice,
    onChangeValue,
    title = '',
    buttons = [],
    style = {},
    valueStyle = {},
    titleStyle = {},
    headerComponent = null,
    remark = '',
    remarkStyle = {},
    arithmometerStyle = {},
    dp = 1,
    isShowError = false
}: ArithmometerInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [displayValue, SetDisplayValue] = useState<string>('');

    const [longPressTimer, SetTongPressTimer] = useState<NodeJS.Timer>(null);
    const [longPressMinus, SetLongPressMinus] = useState<{ isLongPress: boolean, longPressCount: number }>({ isLongPress: false, longPressCount: 0 });
    const [longPressPlus, SetLongPressPlus] = useState<{ isLongPress: boolean, longPressCount: number }>({ isLongPress: false, longPressCount: 0 });

    useEffect(() => {
        onChangeText(value.toString(), false)
    }, [valueUpdate])

    const updateCount = (isPlus: boolean) => {
        SetTongPressTimer(setInterval(() => {
            if (isPlus) {
                SetLongPressPlus(prev => (
                    { ...prev, longPressCount: prev.longPressCount + 1 }
                ))
            } else {
                SetLongPressMinus(prev => (
                    { ...prev, longPressCount: prev.longPressCount + 1 }
                ))
            }
        }, 200))
    }

    useEffect(() => {
        if (longPressMinus.longPressCount > 0) {
            onPressDecrease();
        }
        if (longPressPlus.longPressCount > 0) {
            onPressIncrease();
        }
    }, [longPressMinus.longPressCount, longPressPlus.longPressCount])

    useEffect(() => {
        if (longPressPlus.isLongPress || longPressMinus.isLongPress) {
            updateCount(longPressPlus.isLongPress)
        } else if (!longPressPlus.isLongPress) {
            SetLongPressPlus({ isLongPress: false, longPressCount: 0 })
            clearInterval(longPressTimer)
        } else if (!longPressMinus.isLongPress) {
            SetLongPressMinus({ isLongPress: false, longPressCount: 0 })
            clearInterval(longPressTimer)
        }
    }, [longPressMinus.isLongPress, longPressPlus.isLongPress])

    const onPressIncrease = () => {
        if (value === 0 && nextValidPrice !== undefined) {
            changeValue(nextValidPrice)
        } else {
            var newValue = (value * Math.pow(10, dp) + valuePerStep * Math.pow(10, dp)) / Math.pow(10, dp)
            newValue = Number(newValue.toFixed(dp))
            if (newValue < maximunValue) {
                changeValue(newValue)
            } else {
                changeValue(maximunValue)
            }
        }
    }
    const onPressDecrease = () => {
        var newValue = (value * Math.pow(10, dp) - valuePerStep * Math.pow(10, dp)) / Math.pow(10, dp)
        newValue = Number(newValue.toFixed(dp))
        if (newValue > minimunValue) {
            changeValue(newValue)
        } else {
            changeValue(minimunValue)
        }
    }

    const onChangeText = (newValue: string, inputByKeyboard: boolean) => {
        console.debug('onChangeText dp', dp)
        const numValue = Number(newValue);
        if (Number.isNaN(numValue)) {
            SetDisplayValue(value.toFixed(dp))
        } else {
            if (!inputByKeyboard) {
                onChangeValue(Math.round(numValue * Math.pow(10, dp)) / Math.pow(10, dp))
                SetDisplayValue(numValue.toFixed(dp))
            } else {
                var valueArray = newValue.split('.');
                if (valueArray.length <= 1) {
                    onChangeValue(numValue)
                    SetDisplayValue(newValue)
                } else {
                    var decimalPlace = valueArray[1];
                    if (decimalPlace.length <= dp) {
                        onChangeValue(numValue)
                        SetDisplayValue(newValue)
                    }
                }
            }
        }
    }

    const changeValue = (newValue: number) => {

        onChangeValue(newValue)
        SetDisplayValue(newValue.toFixed(dp))
    }

    return (
        <View style={[styles.container, style]}>
            {
                headerComponent &&
                <Fragment>
                    {headerComponent}
                </Fragment>
            }
            <View style={[styles.arithmometerContainer, arithmometerStyle]}>
                <TouchableOpacityComponent onPress={onPressDecrease} onLongPress={() => SetLongPressMinus({ ...longPressPlus, isLongPress: true })} onPressOut={() => SetLongPressMinus({ ...longPressPlus, isLongPress: false })}>
                    <Minus color={colors.MainFont} />
                </TouchableOpacityComponent>
                <TextInput
                    value={displayValue}
                    style={[styles.value, isShowError ? { color: colors.Red } : { color: colors.MainFont }, valueStyle]}
                    onChangeText={(v) => { onChangeText(v, true) }}
                    keyboardType='numeric' />
                <TouchableOpacityComponent onPress={onPressIncrease} onLongPress={() => SetLongPressPlus({ ...longPressPlus, isLongPress: true })} onPressOut={() => SetLongPressPlus({ ...longPressPlus, isLongPress: false })}>
                    <Plus color={colors.MainFont} />
                </TouchableOpacityComponent>
            </View>
            {
                title !== '' &&
                <Fragment>
                    <View style={{ height: 8 }} />
                    <Text style={[globalStyles.Big_Text, styles.title, titleStyle]}>{title}</Text>
                </Fragment>
            }
            {
                buttons.length !== 0 &&
                <Fragment>
                    <View style={{ height: 8 }} />
                    <View style={styles.buttonsContainer}>
                        {
                            buttons.map((item, index) => {
                                return <ValueOrVolumeButton
                                    key={item.label}
                                    label={item.label}
                                    value={item.value}
                                    onPress={(value) => {
                                            changeValue(value)
                                    }}
                                    style={{
                                        marginLeft: index === 0 ? 0 : 6
                                    }}
                                    isEnable={item.value >= minimunValue}
                                    />
                            })
                        }
                    </View>
                </Fragment>
            }
            {
                remark !== '' &&
                <Fragment>
                    <View style={{ height: 8 }} />
                    <Text style={[globalStyles.Small_Note, remarkStyle]}>{remark}</Text>
                </Fragment>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    arithmometerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    value: {
        flex: 1,
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 40,
        textAlign: 'center'
    },
    title: {
        alignSelf: 'center'
    },
    buttonsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default Arithmometer