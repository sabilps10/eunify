import React, { Fragment } from 'react'
import { StyleSheet, TouchableOpacity, ViewStyle } from 'react-native'
import { useTheme } from '@react-navigation/native';
import TouchableOpacityComponent from '../../../views/utils/TouchableOpacityComponent';

interface RadioButtonInterface {
    isChecked: boolean,
    onPress?: (value: boolean) => void,
    checkedStyle?: ViewStyle,
    unCheckedStyle?: ViewStyle
}

const RadioButton = ({ isChecked, onPress = () => { }, checkedStyle = {}, unCheckedStyle = {} }: RadioButtonInterface) => {
    const { colors } = useTheme();

    const onPressFunction = () => onPress(!isChecked)

    return (
        <Fragment>
            {
                isChecked ?
                    <TouchableOpacityComponent onPress={onPressFunction} style={[styles.container, styles.checkedContainer, { borderColor: '#ADE4EE', backgroundColor: colors.Brand3 }, checkedStyle]} /> :
                    <TouchableOpacityComponent onPress={onPressFunction} style={[styles.container, { backgroundColor: colors.Grey1 }, unCheckedStyle]} />
            }
        </Fragment>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 28,
        height: 28,
        borderRadius: 100,
    },
    checkedContainer: {
        borderWidth: 4
    },
})

export default RadioButton