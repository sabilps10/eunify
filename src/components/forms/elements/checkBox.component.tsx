import React from 'react'
import { StyleSheet, TouchableOpacity, useColorScheme, View, ViewStyle } from 'react-native'
import { useTheme } from '@react-navigation/native';
import CheckListTick from '../../../../assets/svg/CheckListTick';
import TouchableOpacityComponent from '../../../views/utils/TouchableOpacityComponent';

interface CheckBoxInterface {
    isChecked: boolean,
    onPress: (value: boolean) => void,
    style?: ViewStyle
}

const CheckBox = ({ isChecked, onPress, style = {} }: CheckBoxInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();

    const onPressFunction = () => onPress(!isChecked)

    return (
        <TouchableOpacityComponent style={[styles.touchContainer]} onPress={onPressFunction} >
            <View style={[styles.container, { backgroundColor: isChecked ? colors.Brand3 : colors.Grey2 }, style]}>
                {isChecked && <CheckListTick color={colors.White} />}
            </View>
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    touchContainer: {
        width: 35,
        height: 35,
        justifyContent: 'center',
        // alignItems: 'center'
    },
    container: {
        width: 22,
        height: 22,
        borderRadius: 5,
    }
})

export default CheckBox