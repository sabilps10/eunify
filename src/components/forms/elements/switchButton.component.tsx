import React from 'react';
import { StyleSheet, Switch } from 'react-native';
import { useTheme } from '@react-navigation/native';

interface SwitchInterface {
    isEnabled: boolean,
    onValueChange: ((value: boolean) => void | Promise<void>) | null | undefined,

}

const SwitchButton = ({ isEnabled, onValueChange }: SwitchInterface) => {
    const { colors } = useTheme();

    return (
        <Switch
            trackColor={{ false: colors.Grey2, true: colors.Brand3 }}
            thumbColor={isEnabled ? '#ADE4EE' : "#E6E6E6"}
            onValueChange={onValueChange}
            value={isEnabled}
        />
    )
}

export default SwitchButton