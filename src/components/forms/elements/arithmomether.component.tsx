import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle, TextInput } from 'react-native';
import { makeGlobalStyles } from '../../../styles/styles';
import { useTheme } from '@react-navigation/native';
import { ProfitStopLossOrderType } from '../../../utils/orderUtils';
import Minus from '../../../../assets/svg/Minus';
import Plus from '../../../../assets/svg/Plus';
import TouchableOpacityComponent from '../../../views/utils/TouchableOpacityComponent';

interface ArithmometherInterface {
    value: number,
    valueUpdate: boolean,
    valuePerStep: number,
    onChangeValue: (value: number) => void,
    style?: ViewStyle,
    textStyle?: TextStyle,
    dp?: number,
    referencePrice?: number,
    isError?: boolean,
    profitType?: ProfitStopLossOrderType,
    allowNegative?: boolean
}

const Arithmomether = ({
    value,
    valueUpdate,
    valuePerStep,
    onChangeValue,
    style,
    textStyle,
    dp = 2,
    referencePrice = 0,
    isError = false,
    profitType,
    allowNegative = false
}: ArithmometherInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [displayValue, SetDisplayValue] = useState<string>('');

    const [longPressTimer, SetTongPressTimer] = useState<NodeJS.Timer>(null);
    const [longPressMinus, SetLongPressMinus] = useState<{isLongPress:boolean, longPressCount: number}>({isLongPress:false, longPressCount: 0});
    const [longPressPlus, SetLongPressPlus] = useState<{isLongPress:boolean, longPressCount: number}>({isLongPress:false, longPressCount: 0});

    useEffect(()=> {
        onChangeText(value ? value.toString() : undefined, false)
    }, [valueUpdate])

    const updateCount = (isPlus: boolean) => {
        SetTongPressTimer(setInterval(() => {
            if (isPlus) {
                SetLongPressPlus(prev => (
                    {...prev, longPressCount: prev.longPressCount + 1}
                ))
            } else {
                SetLongPressMinus(prev => (
                    {...prev, longPressCount: prev.longPressCount + 1}
                ))
            }
        }, 200))
    }

    useEffect(() => {
        if (longPressMinus.longPressCount > 0) {
            onPressDecrease();
        }
        if (longPressPlus.longPressCount > 0) {
            onPressIncrease();
        }
    }, [longPressMinus.longPressCount, longPressPlus.longPressCount])

    useEffect(()=> {
        if (longPressPlus.isLongPress || longPressMinus.isLongPress) {
            updateCount(longPressPlus.isLongPress)
        } else if (!longPressPlus.isLongPress) {
            SetLongPressPlus({isLongPress:false, longPressCount: 0})
            clearInterval(longPressTimer)
        } else if (!longPressMinus.isLongPress) {
            SetLongPressMinus({isLongPress:false, longPressCount: 0})
            clearInterval(longPressTimer)
        } 
    }, [longPressMinus.isLongPress, longPressPlus.isLongPress])

    const onPressIncrease = () => {
        var newValue = (value * Math.pow(10, dp) + valuePerStep * Math.pow(10, dp)) / Math.pow(10, dp)
        if ((value < 0 && !allowNegative) || value === undefined) {
            newValue = referencePrice
        }
            
        changeValue(newValue)
    }
    const onPressDecrease = () => {
        var newValue = (value * Math.pow(10, dp) - valuePerStep * Math.pow(10, dp)) / Math.pow(10, dp)
        if (allowNegative) {
            if (Number.isNaN(newValue)){
                changeValue(referencePrice)
            } else {
                changeValue(newValue)
            }
        } else {
            if (value < 0) {
                newValue = referencePrice
            }
            if (newValue >= 0) {
                changeValue(newValue)
            }
        }
    }

    const onChangeText = (newValue: string, inputByKeyboard: boolean) => {
        if (newValue) {
            const numValue = Number(newValue);
            if (Number.isNaN(numValue)){
                if (value < 0){
                    onChangeValue(0)
                    SetDisplayValue('')
                } else {
                    SetDisplayValue(value ? value.toFixed(dp): undefined)
                }
            } else {
                if (!inputByKeyboard) {
                    onChangeValue(Math.round(numValue * Math.pow(10, dp)) / Math.pow(10, dp))
                    SetDisplayValue(numValue.toFixed(dp))
                } else {
                    var valueArray = newValue.split('.');
                    if (valueArray.length <= 1) {
                        onChangeValue(numValue)
                        SetDisplayValue(newValue)
                    } else {
                        var decimalPlace = valueArray[1];
                        if (decimalPlace.length <= dp){
                            onChangeValue(numValue)
                            SetDisplayValue(newValue)
                        }
                    }
                }
            }
        } else {
            onChangeValue(undefined)
            SetDisplayValue('')
        }
    }

    const changeValue = (newValue: number) => {
        onChangeValue(newValue)
        SetDisplayValue(newValue.toFixed(dp))
    }

    return (
        <View style={[styles.container, style]}>
            <TouchableOpacityComponent onPress={onPressDecrease} onLongPress={()=>SetLongPressMinus({...longPressPlus, isLongPress: true})} onPressOut={()=>SetLongPressMinus({...longPressPlus, isLongPress: false})}>
                <Minus color={colors.Brand3} />
            </TouchableOpacityComponent>
            <View style={{ width: 6 }} />
            <View style={styles.valueWrapper}>
                <TextInput 
                    value={(value === undefined || (value < 0) && (!allowNegative || !value))? '' : displayValue} 
                    style={[globalStyles.Body_Text, textStyle, isError ? {color: colors.Red} : {}]} 
                    onChangeText={(v) => {onChangeText(v, true)}} 
                    keyboardType='numeric' />
                <Text style={isError ? {color: colors.Red} : {}}>
                    {profitType === ProfitStopLossOrderType.PERCENTAGE && value >= 0 ? '%' : ''}
                </Text>           
            </View>
            <View style={{ width: 6 }} />
            <TouchableOpacityComponent onPress={onPressIncrease} onLongPress={()=>SetLongPressPlus({...longPressPlus, isLongPress: true})} onPressOut={()=>SetLongPressPlus({...longPressPlus, isLongPress: false})}>
                <Plus color={colors.Brand3} />
            </TouchableOpacityComponent>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    valueWrapper: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    button: {
        width: 18,
        height: 18
    }
})

export default Arithmomether;