import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  TouchableOpacity,
  Text,
  View,
  Pressable,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../../styles/styles';
import Header from '../../header/header.component';
import HeaderButton from '../../header/headerButton.component';
import ActionSheetTick from '../../../../assets/svg/ActionSheetTick';
import InputText from './InputText';
import CircleArrow25px from '../../../../assets/svg/CircleArrow25px';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTranslation} from 'react-i18next';
import tw from '../../../utils/tailwind';

interface DropdownPageProps {
  navigation: any;
  route: any;
}

type RouteParams = {
  label: string;
  items: any[];
  onSelect: (val: string) => void;
  selected: string;
};

const DropdownPage: React.FC<DropdownPageProps> = ({navigation, route}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const {label, items, onSelect, selected}: RouteParams = route.params;

  const [pageSelected, setPageSelected] = useState<string>(selected ?? '');
  const [otherValue, setOtherValue] = useState<string>('');
  const [error, setError] = useState<boolean>(false);

  const onPressBack = () => {
    navigation.goBack();
  };

  const handleSelect = (val: string) => {
    onSelect(val);
    setPageSelected(val);
    if (val !== 'OTHER') {
      navigation.goBack();
    }
  };

  const handleOther = () => {
    if (otherValue) {
      onSelect(`${t('accountOpening.purpose.OTHER')}: ${otherValue}`);
      navigation.goBack();
    } else {
      setError(true);
    }
  };

  useEffect(() => {
    if (selected?.split(':')[0] === t('accountOpening.purpose.OTHER')) {
      setPageSelected('OTHER');
      setOtherValue(selected?.split(':')[1].trim());
    }
  }, [selected]);

  useEffect(() => {
    navigation.setOptions({
      header: () => (
        <Header
          leftComponents={
            <HeaderButton
              iconComponent={
                <Icon
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{fontSize: 28, marginRight: 5}}
                  color={colors.MainFont}
                  name="chevron-left"
                  onPress={onPressBack}
                />
              }
              onPress={onPressBack}
              title={label}
              titleStyle={[globalStyles.H4, {color: colors.Brand2}]}
            />
          }
          style={{
            backgroundColor: colors.White,
            borderBottomWidth: 1,
            borderBottomColor: colors.Grey0,
            borderStyle: 'solid',
          }}
        />
      ),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  return (
    <KeyboardAvoidingView
      style={{flex: 1, backgroundColor: colors.White}}
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : null}
      enabled>
      <ScrollView style={{flex: 1}}>
        {items.map((el, idx) => (
          <>
            <TouchableOpacity
              key={el.value}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 16,
                ...(idx !== 0 && {
                  borderTopWidth: 1,
                  borderTopColor: colors.Grey0,
                  borderStyle: 'solid',
                }),
              }}
              onPress={() => handleSelect(el.value)}>
              <Text
                style={[
                  globalStyles.Big_Text,
                  {
                    color:
                      pageSelected === el.value
                        ? colors.Brand2
                        : colors.MainFont,
                  },
                ]}>
                {el.label}
              </Text>
              {pageSelected === el.value && (
                <ActionSheetTick color={colors.Brand3} />
              )}
            </TouchableOpacity>
            {el.value === 'OTHER' && pageSelected === 'OTHER' && (
              <>
                <View
                  style={{
                    paddingHorizontal: 16,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: Platform.OS === 'android' ? 30 : 0,
                  }}>
                  <InputText
                    name="other"
                    placeholder={t('accountOpening.please_specify')}
                    value={otherValue}
                    onChange={val => setOtherValue(val)}
                    wrapperStyle={{width: '90%', marginRight: 10}}
                  />
                  <Pressable onPress={handleOther}>
                    <CircleArrow25px
                      color={colors.White}
                      backgroundColor={colors.Brand3}
                    />
                  </Pressable>
                </View>
                {el.value === 'OTHER' && pageSelected === 'OTHER' && error && (
                  <Text
                    style={[
                      tw`mt-1`,
                      globalStyles.Small_Note,
                      {color: colors.Red, paddingHorizontal: 16},
                    ]}>
                    {t('error.mandatory')}
                  </Text>
                )}
              </>
            )}
          </>
        ))}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default DropdownPage;
