import React, {useState} from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';
import DatePickerNative from 'react-native-date-picker';
import {useTheme} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import Tooltip from 'rn-tooltip';
import {makeGlobalStyles} from '../../../styles/styles';
import tw from '../../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import {State} from '../../../redux/root-reducer';

interface DatePickerProps {
  label: string;
  name: string;
  value: any;
  onChange: (val: any, name: string) => void;
  required?: boolean;
  errors?: string[];
  wrapperStyle?: any;
  labelStyle?: any;
  format?: string;
  disabled?: boolean;
  tooltip?: string | null;
  onFocus?: () => void;
}

const DatePicker: React.FC<DatePickerProps> = ({
  label,
  name,
  value,
  onChange,
  required = false,
  errors,
  wrapperStyle,
  labelStyle,
  format = 'YYYY-MM-DD',
  disabled = false,
  tooltip = null,
  onFocus,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const currentDate = new Date();
  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const [isOpen, setIsOpen] = useState<boolean>(false);
  return (
    <View style={wrapperStyle}>
      <View style={tw`flex flex-row`}>
        <Text style={[tw`text-black mb-1`, labelStyle]}>{`${label} ${
          required ? '*' : ''
        }`}</Text>
        {tooltip && (
          <Tooltip
            actionType="press"
            popover={<Text style={tw`text-white`}>{tooltip}</Text>}
            withOverlay={false}
            height="auto"
            width={'50%'}
            // backgroundColor={colors.fixedBackground}
          >
            <Icon name="exclamationcircleo" size={18} style={tw`ml-[10px]`} />
          </Tooltip>
        )}
      </View>
      <Pressable onPress={onFocus}>
        <TouchableOpacity
          disabled={disabled}
          activeOpacity={0.8}
          onPress={() => setIsOpen(true)}
          style={styles.root}>
          <Text
            style={[
              globalStyles.Big_Text,
              {color: value && !disabled ? colors.MainFont : colors.Grey3},
            ]}>
            {value ? moment(value).format(format) : ''}
          </Text>
          <Icon name="calendar" size={18} />
        </TouchableOpacity>
      </Pressable>
      <DatePickerNative
        modal
        open={isOpen}
        onCancel={() => setIsOpen(false)}
        date={value ?? currentDate}
        onConfirm={val => {
          onChange(val, name);
          setIsOpen(false);
        }}
        onCancel={() => {
          onChange(null, name);
          setIsOpen(false);
        }}
        mode="date"
        title={t('historyDealQuery.dateTitle')}
        confirmText={t('historyDealQuery.dateConfirm')}
        cancelText={t('historyDealQuery.dateCancel')}
        locale={lang === 'sc' ? 'zh-Hans' : lang === 'tc' ? 'zh-Hant' : 'en'}
      />
      {errors?.includes(name) && (
        <Text style={[tw`mt-1`, globalStyles.Small_Note, {color: colors.Red}]}>
          {t('error.mandatory')}
        </Text>
      )}
      {name === 'dateOfBirth' && errors?.includes('ageValidation') && (
        <Text style={[tw`mt-1`, globalStyles.Small_Note, {color: colors.Red}]}>
          {t('accountOpening.personal_information.dob_tooltip')}
        </Text>
      )}
      {name === 'identificationExpiryDate' &&
        errors?.includes('expValidation') && (
          <Text
            style={[tw`mt-1`, globalStyles.Small_Note, {color: colors.Red}]}>
            {t('accountOpening.personal_information.expiry_validation')}
          </Text>
        )}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#eeee',
    borderRadius: 5,
    paddingHorizontal: 16,
    paddingVertical: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

export default DatePicker;
