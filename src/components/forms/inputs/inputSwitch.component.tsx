import React from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';
import SwitchButton from '../elements/switchButton.component';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';

interface InputSwitchInterface {
    label?: string,
    value: boolean,
    onSwitch: (value: boolean) => void,
    isRequired?: boolean,
    style?: ViewStyle,
    textStyle?: TextStyle
    isEnableSwitch?: boolean
}

const InputSwitch = ({
    label,
    value,
    onSwitch,
    isRequired = false,
    style = {},
    textStyle = {},
    isEnableSwitch = true
}: InputSwitchInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <Text style={[globalStyles.Form_Title, textStyle]}>{label} {isRequired && '*'}</Text>
            {
                isEnableSwitch && <SwitchButton isEnabled={value} onValueChange={onSwitch} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
})

export default InputSwitch;