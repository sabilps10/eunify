import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../../styles/styles';
import tw from '../../../utils/tailwind';
import {useTranslation} from 'react-i18next';

interface PickerProps {
  label: string;
  name: string;
  value?: any;
  options: {value: any; label: string; disabled?: boolean}[];
  onPick: (value: any, name: string) => void;
  multiplePick?: boolean;
  required?: boolean;
  errors?: string[];
  wrapperStyle?: any;
}

const Picker: React.FC<PickerProps> = ({
  label,
  name,
  value = '',
  options,
  multiplePick = false,
  onPick,
  required = false,
  errors,
  wrapperStyle,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const [picked, setPicked] = useState<any[]>([]);

  const handlePick = (val: any) => {
    if (multiplePick) {
      if (picked.includes(val)) {
        onPick(
          picked.filter((el: any) => el !== val),
          name,
        );
      } else {
        onPick([...picked, val], name);
      }
    } else {
      setPicked([val]);
      onPick(val, name);
    }
  };

  useEffect(() => {
    if (Array.isArray(value)) {
      setPicked(value);
    } else {
      setPicked([value]);
    }
  }, [value]);

  return (
    <View style={wrapperStyle}>
      <Text style={[tw`mb-1`, {color: colors.MainFont}]}>{`${label} ${
        required ? '*' : ''
      }`}</Text>
      <View style={[tw`flex flex-row flex-wrap justify-start`]}>
        {options.map((el: any) => (
          <TouchableOpacity
            disabled={el.disabled ?? false}
            activeOpacity={0.8}
            onPress={() => handlePick(el.value)}
            key={el.value}>
            <View
              style={[
                styles.root,
                picked?.includes(el.value)
                  ? styles.wrapperPicked
                  : el.disabled
                  ? styles.wrapperDisabled
                  : styles.wrapper,
              ]}>
              <Text
                style={[
                  globalStyles.Big_Text_B,
                  // eslint-disable-next-line react-native/no-inline-styles
                  {
                    color: picked.includes(el.value)
                      ? 'white'
                      : el.disabled
                      ? colors.MainFont
                      : colors.Grey3,
                  },
                ]}>
                {el.label}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
      {errors?.includes(name) && (
        <Text style={[globalStyles.Small_Note, {color: colors.Red}]}>
          {t('error.mandatory')}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 8,
    paddingVertical: 2,
    borderRadius: 5,
    marginRight: 15,
    marginBottom: 10,
  },
  wrapper: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#9D9D9D',
  },
  wrapperDisabled: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#9D9D9D',
    backgroundColor: '#9D9D9D',
  },
  wrapperPicked: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#00B1D2',
    backgroundColor: '#00B1D2',
  },
});

export default Picker;
