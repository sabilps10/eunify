import React, { Fragment } from 'react';
import { Pressable, StyleSheet, Text, TextStyle, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../../styles/styles';
import { useTheme } from '@react-navigation/native';

import Arithmomether from '../elements/arithmomether.component';
import { ProfitStopLossOrderType } from '../../../utils/orderUtils';
import Remove from '../../../../assets/svg/Remove';
import PressableComponent from '../../../views/utils/PressableComponent';

interface InputArithmometherInterface {
    label: string,
    note?: string | null,
    note2?: string | null,
    isRequired?: boolean,
    value: number,
    valueUpdate: boolean,
    valuePerStep: number,
    onChangeValue: (value: number) => void,
    onCancel: () => void,
    style?: ViewStyle,
    labelStyle?: TextStyle,
    noteStyle?: TextStyle,
    dp?: number,
    referencePrice?: number,
    isError?: boolean,
    profitType?: ProfitStopLossOrderType,
    allowNegative?: boolean
}

const InputArithmomether = ({
    label,
    note,
    note2,
    isRequired = false,
    value,
    valueUpdate,
    valuePerStep,
    onChangeValue,
    onCancel,
    style = {},
    labelStyle = {},
    noteStyle = {},
    dp = 2,
    referencePrice = 0,
    isError = false,
    profitType,
    allowNegative = false
}: InputArithmometherInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <View style={styles.leftWrapper}>
                <Text style={[globalStyles.Form_Title, labelStyle]}>{label} {isRequired && '*'}</Text>
                {
                    note &&
                    <Fragment>
                        <View style={{ height: 2 }} />
                        <Text style={[globalStyles.Small_Note, noteStyle]}>{note}</Text>
                    </Fragment>
                }
                {
                    note2 &&
                    <Fragment>
                        <View style={{ height: 2 }} />
                        <Text style={[globalStyles.Small_Note, noteStyle, {color: colors.Grey3}]}>{note2}</Text>
                    </Fragment>
                }
            </View>
            <View style={styles.rightWrapper}>
                <Arithmomether
                    textStyle={styles.inputText}
                    value={value}
                    valueUpdate={valueUpdate}
                    valuePerStep={valuePerStep}
                    onChangeValue={onChangeValue}
                    dp={dp}
                    referencePrice={referencePrice}
                    isError={isError}
                    profitType={profitType}
                    allowNegative={allowNegative}/>
                <View style={{ width: 16 }} />
                <PressableComponent style={styles.cancelCircleIconWrapper} onPress={onCancel}>
                    <Remove style={{width: 18, height: 18}} color={colors.MainFont} />
                </PressableComponent>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        maxWidth: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    leftWrapper: {
        flex: 0.5,
        justifyContent: 'flex-start'
    },
    rightWrapper: {
        flex: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: '100%',
    },
    inputText: {
        flex: 1, 
        textAlign: 'center', 
        height: '100%', 
    },
    cancelCircleIconWrapper: {
        width: 18,
        height: 18,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelCircleIcon: {
        width: 18,
        height: 18,
    }
})

export default InputArithmomether