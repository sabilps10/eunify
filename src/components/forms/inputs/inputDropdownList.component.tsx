import React, { Fragment, useState } from 'react';
import { StyleSheet, Text, TextStyle, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import ArrowRight from '../../../../assets/svg/ArrowRight';

interface InputDropDownListInterface {
    label?: string | null,
    value: string,
    style?: ViewStyle,
    textStyle?: TextStyle,
    labelStyle?: TextStyle,
    isRequired?: boolean
}

const InputDropDownList = ({
    label = null,
    value,
    style = {},
    textStyle = {},
    labelStyle = {},
    isRequired = false
}: InputDropDownListInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [dropDownPickerOpened, setDropDownPickerOpened] = useState<boolean>(false);

    return (
        <Fragment>
            {
                label &&
                <View>
                    <Text style={[globalStyles.Form_Title, labelStyle]}>{label}{isRequired && ' *'}</Text>
                    <View style={{ height: 8 }} />
                </View>
            }
            <View style={[styles.container, { backgroundColor: colors.WrapperBg }, style]}>
                <Text style={[globalStyles.Big_Text, textStyle]}>{value}</Text>
                <View style={styles.arrow}>
                    <ArrowRight color={colors.Brand3} />
                </View>
            </View>
        </Fragment>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingTop: 13,
        paddingBottom: 9,
        paddingLeft: 16,
        paddingRight: 10,
        borderWidth: 0
    },
    dropDownContainer: {
        borderWidth: 0,
        zIndex: 9
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
})

export default InputDropDownList