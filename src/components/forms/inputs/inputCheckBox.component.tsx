import React, { Component, ReactComponentElement } from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';
import CheckBox from '../elements/checkBox.component';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';

interface InputCheckBoxInterface {
    label?: string,
    labelComponent?: any,
    value: boolean,
    onPress: (value: boolean) => void,
    isRequired?: boolean,
    style?: ViewStyle,
    textStyle?: TextStyle
}

const InputCheckBox = ({
    label = '',
    labelComponent,
    value,
    onPress,
    isRequired = false,
    style = {},
    textStyle = {}
}: InputCheckBoxInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <View style={{alignSelf: 'flex-start'}}>
                <CheckBox isChecked={value} onPress={onPress} />
            </View>
            <View style={{ width: 4}} />
            {
                labelComponent?? <Text style={[globalStyles.Note, textStyle]}>{label} {isRequired && '*'}</Text>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    }
})

export default InputCheckBox
