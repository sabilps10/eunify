import React from 'react';
import {
  Platform,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {makeGlobalStyles} from '../../../styles/styles';
import tw from '../../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import {debounce} from 'lodash';

interface InputTextProps {
  name: string;
  label?: string;
  placeholder?: string;
  required?: boolean;
  value?: string;
  onChange: (val: string, name: string) => void;
  errors?: string[];
  errorMessage?: string;
  editAble?: boolean;
  labelStyle?: any;
  inputStyle?: any;
  wrapperStyle?: any;
  onFocus?: () => void;
  type?:
    | 'letterOnlyInput'
    | 'letterNoSpace'
    | 'numberOnlyInput'
    | 'letterNoDigit';
  maxLength?: number;
  ref?: any;
  onDebounce?: (val: string, name: string) => void;
}

const InputText: React.FC<InputTextProps> = ({
  label,
  name,
  placeholder,
  required = false,
  value,
  onChange,
  errors,
  editAble = true,
  labelStyle,
  inputStyle,
  wrapperStyle,
  onFocus,
  type,
  errorMessage,
  maxLength = 50,
  ref,
  onDebounce,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);

  const handleChange = (val: string) => {
    let valueInput = val;

    if (/^\s/.test(val)) {
      valueInput = val.replace(' ', '');
    }

    if (type === 'numberOnlyInput') {
      valueInput = val.replace(/[^0-9]/gi, '');
    }
    if (type === 'letterOnlyInput') {
      valueInput = /^\s/.test(val)
        ? val.replace(' ', '')
        : val.replace(
            /[`~!@#$%^&*()_|+\-=?;:'",.<>¥￥€£•"'\{\}\[\]\\\/]/gi,
            '',
          );
    }
    if (type === 'letterNoSpace') {
      valueInput = /^\s/.test(val)
        ? val.replace(' ', '')
        : val.replace(
            /[`~!@#$%^&*()_|+\-=?;:'",.<>¥￥€£•"' \{\}\[\]\\\/]/gi,
            '',
          );
    }
    if (type === 'letterNoDigit') {
      valueInput = /^\s/.test(val)
        ? val.replace(' ', '')
        : val.replace(
            /[`~0-9!@#$%^&*()_|+\-=?;:'",.<>¥￥€£•"'\{\}\[\]\\\/]/gi,
            '',
          );
    }

    onChange(valueInput, name);
    onDebounce && onDebounce(valueInput, name);
  };

  return (
    <View style={wrapperStyle}>
      {label && (
        <Text style={[tw`text-black mb-1`, labelStyle]}>{`${label} ${
          required ? '*' : ''
        }`}</Text>
      )}
      <Pressable
        onPress={() => {
          Platform.OS === 'android' && onFocus && onFocus();
        }}>
        <TextInput
          value={value}
          onChangeText={(val: string) => handleChange(val)}
          placeholder={placeholder}
          style={[
            styles.root,
            inputStyle,
            globalStyles.Big_Text,
            {color: editAble ? colors.MainFont : colors.Grey3},
          ]}
          editable={editAble}
          onPressIn={() => {
            Platform.OS === 'ios' && onFocus && onFocus();
          }}
          maxLength={maxLength}
          ref={ref}
        />
      </Pressable>

      {errors?.includes(name) && (
        <Text style={[tw`mt-1`, globalStyles.Small_Note, {color: colors.Red}]}>
          {errorMessage ?? t('error.mandatory')}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#eeee',
    borderRadius: 5,
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

export default InputText;