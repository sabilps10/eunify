import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Pressable,
} from 'react-native';
import {useTheme, useNavigation} from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {makeGlobalStyles} from '../../../styles/styles';
import tw from '../../../utils/tailwind';
import {useTranslation} from 'react-i18next';
import {CountryOptionType} from '../../countryOptionView/countryOptionView';
import {useDispatch, useSelector} from 'react-redux';
import {bindActionCreators} from 'redux';
import {BottomSheetCreators} from '../../../redux/BottomSheet/actionCreators';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../../redux/root-reducer';
import reactotron from 'reactotron-react-native';

interface DropdownProps {
  label: string;
  name: string;
  placeholder?: string;
  value?: any;
  options: {
    value: string | number;
    label: string;
    labelTc?: string;
    labelSc?: string;
  }[];
  onSelect: (value: string | number, name: string) => void;
  errors?: string[];
  required?: boolean;
  wrapperStyle?: any;
  disabled?: boolean;
  type?: 'bottomsheet' | 'page' | 'country' | 'other';
  onFocus?: () => void;
}

const Dropdown: React.FC<DropdownProps> = ({
  label,
  placeholder,
  name,
  value,
  options,
  onSelect,
  errors,
  required = false,
  wrapperStyle,
  disabled = false,
  type = 'bottomsheet',
  onFocus,
}) => {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const navigation: any = useNavigation();
  const dispatch = useDispatch();

  const countryOption = useSelector((state: State) => state.state.countryList);
  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const {OpenBottomSheet} = bindActionCreators(BottomSheetCreators, dispatch);

  const [selected, setSelected] = useState<string | number | null>(null);

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpenDropdown = () => {
    if (type === 'bottomsheet') {
      OpenBottomSheet({
        isOpen: true,
        selected: selected as string,
        options: options.map(el => ({
          key: el.value as string,
          value: el.label,
        })),
        onSelect: handleSelect,
        label,
      });
    } else if (type === 'country') {
      navigation.navigate('CountryDropdownPage', {
        type: CountryOptionType.REGION,
        onSelect: handleSelect,
        selectedId: '',
      });
    } else if (type === 'page') {
      navigation.navigate('DropdownPage', {
        label,
        items: options,
        onSelect: handleSelect,
        selected,
      });
    } else {
      setIsOpen(true);
    }
  };

  const handleSelect = (val: any) => {
    setSelected(val);
    onSelect(val, name);
    setIsOpen(false);
  };

  const getSelectedLabel = (value: any) => {
    if (type === 'country') {
      return (
        countryOption.find(el => el.value === value)[
          lang === 'en' ? 'nameEn' : lang === 'tc' ? 'nameTc' : 'nameSc'
        ] ?? value
      );
    }
    return options.find(el => el.value === value)?.label ?? value;
  };

  useEffect(() => {
    if (value) {
      setSelected(value);
    }
  }, [value]);

  return (
    <View style={wrapperStyle}>
      <Text style={tw`text-black mb-1`}>{`${label} ${
        required ? '*' : ''
      }`}</Text>
      <Pressable onPress={onFocus}>
        <TouchableOpacity
          disabled={disabled}
          onPress={handleOpenDropdown}
          activeOpacity={0.8}
          style={[styles.container, {backgroundColor: colors.Grey0}]}>
          <Text
            style={[
              globalStyles.Big_Text,
              {color: selected && !disabled ? colors.MainFont : colors.Grey3},
            ]}>
            {selected
              ? getSelectedLabel(selected)
              : placeholder
              ? placeholder
              : t('accountOpening.please_select')}
          </Text>
          <MaterialCommunityIcons
            name={isOpen ? 'chevron-down' : 'chevron-right'}
            size={24}
            color={colors.fixedBackground}
          />
        </TouchableOpacity>
      </Pressable>
      {errors?.includes(name) && (
        <Text style={[tw`mt-1`, globalStyles.Small_Note, {color: colors.Red}]}>
          {t('error.mandatory')}
        </Text>
      )}
      {isOpen && (
        <View style={[styles.dropdown, {backgroundColor: colors.Grey0}]}>
          <ScrollView keyboardShouldPersistTaps="handled">
            {options.map((el: any) => (
              <TouchableOpacity
                key={el.value}
                style={{paddingTop: 4, paddingBottom: 4}}
                onPress={() => handleSelect(el.value)}>
                <Text
                  style={{
                    color:
                      selected === el.value
                        ? colors.fixedBackground
                        : colors.MainFont,
                  }}>
                  {el.label}
                </Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 5,
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 16,
    paddingRight: 16,
    borderWidth: 0,
  },
  dropdown: {
    flexDirection: 'column',
    marginTop: 5,
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 18,
    paddingRight: 18,
    borderRadius: 5,
    maxHeight: 150,
  },
});

export default Dropdown;
