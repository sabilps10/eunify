import {useTheme} from '@react-navigation/native';
import React, {useEffect, useLayoutEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {State} from '../../../redux/root-reducer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {Props} from '../../../routes/route';
import {makeGlobalStyles} from '../../../styles/styles';
import Header from '../../header/header.component';
import HeaderButton from '../../header/headerButton.component';
import HeaderSearchBar from '../../header/headerSearchBar.component';
import AlphabetList from 'react-native-flatlist-alphabet';
import {SafeAreaView} from 'react-native-safe-area-context';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {THEME_TYPE} from '../../../types/types';
import {debounce} from 'lodash';
import CountryOptionViewItem from '../../countryOptionView/countryOptionViewItem';
import TopBarBack from '../../../../assets/svg/TopBarBack';

export enum CountryOptionType {
  REGION = 'REGION',
  COUNTRY = 'COUNTRY',
}

const CountryDropdownPage: React.FC<Props<'CountryOptionView'>> = ({
  route,
  navigation,
}) => {
  const {colors} = useTheme();
  const globalStyles = makeGlobalStyles(colors);
  const styles = makeStyles(colors);

  const lang = useSelector((state: State) => state.state.setting.UserLanguage);

  const {onSelect} = route.params;

  const countryOption = useSelector((state: State) => state.state.countryList);

  const selectedId = route.params.selectedId;

  const [data, setData] =
    useState<
      {value: string; key: string; regionCode: string; countryCode: string}[]
    >();
  const [searchText, setSearchText] = useState<string>('');

  const searchDebounce = React.useRef(
    debounce(async text => {
      setSearchText(text);
    }, 500),
  ).current;

  useLayoutEffect(() => {
    navigation.setOptions({
      header: () => (
        <Header
          style={{backgroundColor: colors.WrapperBg}}
          leftComponents={
            <HeaderButton
              onPress={onPressBack}
              iconComponent={<TopBarBack color={colors.MainFont} />}
            />
          }
          MiddleComponent={<HeaderSearchBar onChangeText={onChangeText} />}
        />
      ),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [navigation]);

  useEffect(() => {
    var country: any = countryOption.map(item => {
      return {
        value: item.nameEn,
        key: item.value,
        displayValue:
          lang === 'en'
            ? item.nameEn
            : lang === 'tc'
            ? item.nameTc
            : item.nameSc,
      };
    });
    if (searchText) {
      country = country.filter(
        item =>
          item.value.toUpperCase().includes(searchText.toUpperCase()) ||
          item.displayValue.toUpperCase().includes(searchText.toUpperCase()),
      );
    }
    setData(country);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countryOption, searchText]);

  useEffect(() => {
    let data: any = countryOption.map(item => {
      return {
        value: item.nameEn,
        key: item.value,
        displayValue:
          lang === 'en'
            ? item.nameEn
            : lang === 'tc'
            ? item.nameTc
            : item.nameSc,
      };
    });
    setData(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countryOption]);

  const onPressBack = () => {
    navigation.goBack();
  };

  const onChangeText = (value: string) => {
    searchDebounce(value);
  };

  const onPressItem = (item: {
    key: string;
    value: string;
    countryCode: string;
  }) => {
    onSelect(item.key);
    navigation.goBack();
  };

  const renderCustomItem = (item: {
    key: string;
    value: string;
    regionCode: string;
    countryCode: string;
    displayValue: string;
  }) => (
    <CountryOptionViewItem
      item={item}
      onPressItem={onPressItem}
      value={item.displayValue}
      isSelected={selectedId === item.value}
    />
  );

  return (
    <SafeAreaView style={styles.safeAreaView} edges={['bottom']}>
      <AlphabetList
        style={[styles.alphabetlist]}
        containerStyle={styles.indexContainerStyle}
        letterItemStyle={{...globalStyles.Big_Text, ...styles.indexLetterStyle}}
        indexLetterColor={colors.SectionTitleFont.toString()}
        data={data ?? []}
        renderItem={renderCustomItem}
        renderSectionHeader={() => null}
        getItemHeight={() => 53}
        sectionHeaderHeight={0}
      />
    </SafeAreaView>
  );
};

const makeStyles = (colors: THEME_TYPE) =>
  StyleSheet.create({
    safeAreaView: {
      flex: 1,
      backgroundColor: colors.WrapperBg,
    },
    alphabetlist: {
      flex: 1,
      backgroundColor: colors.MainBg,
      borderTopLeftRadius: 12,
    },
    contentContainerStyle: {
      paddingTop: 26,
    },
    indexContainerStyle: {
      backgroundColor: colors.WrapperBg,
      width: 21,
      position: 'absolute',
      right: 0,
    },
    indexLetterStyle: {
      fontSize: 13,
      marginTop: 5,
      marginBottom: 1,
      height: 18,
    },
  });

export default CountryDropdownPage;
