import React, { ReactNode, useState } from 'react'
import { LayoutChangeEvent, NativeSyntheticEvent, StyleProp, StyleSheet, Text, TextInput, TextInputEndEditingEventData, TextStyle, View, ViewStyle } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import { InputType } from '../../../types/InputTypes';
import ErrorMessage from '../../alerts/errorMessage.component';
import { hasInValidCharacters, validEmailCharacters } from '../../../views/utils/CommonFunction';

interface InputInterface {
    label?: string | null,
    value: InputType,
    onChangeText: (value: string) => void,
    onEndEditing?: (e: NativeSyntheticEvent<TextInputEndEditingEventData>) => void,
    type?: 'text' | 'password' | 'email' | 'phone' | 'number',
    placeholder?: string,
    style?: StyleProp<ViewStyle>,
    inputStyle?: StyleProp<TextStyle>,
    placeholderTextColor?: string,
    rightComponent?: ReactNode,
    editable?: boolean
}

const Input = ({
    label = null,
    value,
    onChangeText,
    onEndEditing,
    type = 'text',
    style = null,
    inputStyle = null,
    placeholder = '',
    placeholderTextColor = '',
    rightComponent = null,
    editable = true
}: InputInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const secureTextEntry = type === 'password';
    const keyboardType = type === 'email' ? 'email-address' : type === 'phone' || type === 'number' ? 'phone-pad' : 'default';

    return (
        <View>
            {
                label &&
                <View>
                    <Text style={[globalStyles.Form_Title]}>{label}{value.mandatory && ' *'}</Text>
                    <View style={{ height: 8 }} />
                </View>
            }
            <View style={[styles.inputSection, style ? style : { backgroundColor: colors.WrapperBg }]}>
                <TextInput
                    placeholder={placeholder}
                    placeholderTextColor={placeholderTextColor === '' ? colors.Grey3 : placeholderTextColor}
                    value={value.value}
                    secureTextEntry={secureTextEntry}
                    onChangeText={(value) => {
                        if (value && type === 'email' && !validEmailCharacters(value)) {
                            return;
                        }
                        onChangeText(value)
                    }}
                    // onChangeText={onChangeText}
                    onEndEditing={onEndEditing}
                    keyboardType={keyboardType}
                    style={[,
                        globalStyles.Form_Infield,
                        styles.textInput,
                        inputStyle ? inputStyle : { backgroundColor: colors.WrapperBg },
                    ]}
                    editable={editable}
                    maxLength={value.maxLength}
                    caretHidden = {false}
                />
                {
                    rightComponent &&
                    <View style={{ paddingEnd: 16 }}>
                        {rightComponent}
                    </View>
                }
            </View>
            {
                value.error !== "" &&
                <View style={{paddingVertical: 5}}>
                    <ErrorMessage message={value.error} />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    inputSection: {
        flex: 1,
        borderRadius: 5,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        flex: 1,
        height: 50,
        borderRadius: 5,
        paddingHorizontal: 18,
        paddingVertical: 12,
    },
})

export default Input