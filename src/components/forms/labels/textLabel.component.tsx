import React, { ReactNode, useEffect, useState } from 'react'
import { LayoutChangeEvent, StyleSheet, Text, TextInput, TextStyle, View, ViewStyle, Animated, StyleProp } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';

interface TextLabelInterface {
    label?: string | null,
    isRequired?: boolean,
    labelStyle?: StyleProp<TextStyle>,
    enableFadeIn?: boolean
}

const TextLabel = ({
    label = null,
    isRequired = false,
    labelStyle = undefined,
    enableFadeIn = false
}: TextLabelInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)

    const fadeIn = new Animated.Value(0);
    useEffect(() => {
        Animated.timing(fadeIn, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true
            }).start();
    }, [label])

    return (
        <View style={[styles.container]}>
            {
                <View>
                    <Animated.Text style={[globalStyles.Form_Title, labelStyle, enableFadeIn ? {opacity: fadeIn} : null]}>{label}{isRequired && ' *'}</Animated.Text>
                    <View style={{ height: 8 }} />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
    },
    textInput: {
        borderRadius: 5,
        height: 50,
        paddingHorizontal: 18,
        paddingVertical: 12
    },
    rightComponentContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default TextLabel