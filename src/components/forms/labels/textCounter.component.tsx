import React, { ReactNode, useState, useEffect } from 'react'
import { Image, Pressable, LayoutChangeEvent, StyleSheet, Text, TextInput, TextStyle, View, ViewStyle, useColorScheme } from 'react-native'
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../../styles/styles';
import SmallButton from '../../buttons/smallButton.component';
import { useTranslation } from 'react-i18next';
import { resendCodeSecond } from '../../../config/constants';

interface definitionInterface{
    (callbackFunction:boolean):void;
}

interface TextLabelInterface {
    counter?: number,
    callback: definitionInterface,
    enable?: boolean
}

const TextCounter = ({
    counter = resendCodeSecond,
    callback,
    enable = true
}: TextLabelInterface) => {
    const { colors } = useTheme();
    const [t, i18n] = useTranslation();
    const globalStyles = makeGlobalStyles(colors)
    const scheme = useColorScheme();

    const [displayCounter, SetDisplayCounter] = useState<number>(enable ? counter : -1);

    useEffect(() => {
        var interval = setInterval(() => {
            var nCounter = displayCounter - 1;
            if (nCounter >= 0) {
                SetDisplayCounter(nCounter)
            } else {
                SetDisplayCounter(nCounter)
                clearInterval(interval);
            }
        }, 1000);

        return () => {
            clearInterval(interval);
        }
    }, [displayCounter]);

    useEffect(() => {
        console.debug('textCounter enable', enable)
        if (enable) {
            SetDisplayCounter(counter)
        }
    }, [enable]);

    useEffect(() => {
        var interval = setInterval(() => {
            var nCounter = displayCounter - 1;
            if (nCounter >= 0) {
                SetDisplayCounter(nCounter)
            } else {
                SetDisplayCounter(nCounter)
                clearInterval(interval);
            }
        }, 1000);

        return () => {
            clearInterval(interval);
        }
    }, [displayCounter]);

    const doCallback = () => {
        callback(true);
        SetDisplayCounter(resendCodeSecond)
    } 

    return (
        <View>
            {
                <View>
                    {
                        displayCounter >= 0 ?
                        <Text style={[globalStyles.Form_Title, {color: colors.MainFont}]}>{displayCounter}s</Text>
                        :
                        <SmallButton textStyle={globalStyles.Form_Title} title={t('cognitoSignUpVerification.getCode')} onPress={doCallback} />
                    }
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //height: 100,
        paddingEnd: 16
    },
    textInput: {
        borderRadius: 5,
        height: 50,
        paddingHorizontal: 18,
        paddingVertical: 12
    },
    rightComponentContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectButton: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
})

export default TextCounter