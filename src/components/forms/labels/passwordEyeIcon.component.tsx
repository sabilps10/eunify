import React, { useEffect } from 'react'
import { ColorValue, TouchableOpacity, View } from 'react-native'
import { useTheme } from '@react-navigation/native';
import ShowPassword from '../../../../assets/svg/ShowPassword';
import HidePassword from '../../../../assets/svg/HidePassword';
import TouchableOpacityComponent from '../../../views/utils/TouchableOpacityComponent';


interface definitionInterface{
    (callbackFunction:boolean):void;
}

interface TextLabelInterface {
    iconColor?: ColorValue,
    isEnable: boolean,
    callback: definitionInterface
}

const PasswordEyeIcon = ({
    iconColor,
    isEnable = false,
    callback
}: TextLabelInterface) => {
    const { colors } = useTheme();

    useEffect(() => {
    }, []);

    const doCallback = () => {
        callback(true);
    } 

    return (
        <View>
            <TouchableOpacityComponent onPress={doCallback}>
                {
                    isEnable ?
                    <ShowPassword color={iconColor ? iconColor : colors.MainFont} />
                    :
                    <HidePassword color={iconColor ? iconColor : colors.MainFont} />
                }
            </TouchableOpacityComponent>
        </View>
    )
}

export default PasswordEyeIcon