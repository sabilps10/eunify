import React, { useState, useEffect, Dispatch, SetStateAction, useRef } from 'react';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { Alert, Image, Pressable, StyleSheet, Text, ToastAndroid, useColorScheme, View, Platform, Dimensions } from 'react-native';
import {
    PORTRAIT,
    OrientationType,
    useOrientationChange,
} from 'react-native-orientation-locker';
import Orientation from 'react-native-orientation-locker';

import { WebView } from 'react-native-webview'
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { captchaID } from '../../config/constants';
import * as RNLocalize from "react-native-localize";

interface ComponentInterface {
    onSuccess: Function,
    onClose: Function
}

const CaptchaComponent = ({onSuccess, onClose}: ComponentInterface) => {

    const { colors } = useTheme();
    const [t, i18n] = useTranslation();

    const [webref, setWebref] = useState<any>(undefined);
    const [status, setStatus] = useState<any>(undefined);

    const userLanguage = useSelector((state: State) => state.state.setting?.UserLanguage?? 'en');

    useEffect(()=> {
        console.debug('CaptchaWebview status', status)
        if(status === 'ready') {
          webref.injectJavaScript(`window.captchaObj.showCaptcha()`)
        }else{
        //   const interval = setInterval(()=>{
        //     if(this.status === 'ready' && this.props.show){
        //       this.webref.injectJavaScript(`window.captchaObj.showCaptcha()`)
        //       clearInterval(interval);
        //     }
        //   },10)
        }
        // console.debug('initOnReady data:'+JSON.stringify({"captchaId":captchaID,"protocol":"https://"}))
    }, [status])

    let sources = Platform.OS === 'android' ? 'file:///android_asset/Static.bundle/demo.html' : 'captcha/Static.bundle/demo.html'
    var language = userLanguage === 'en' ? 'eng' : (userLanguage === 'tc' ? 'zho-hk' : 'zho')
    const jsToInject = `initOnReady('?data='+JSON.stringify({"lang":"`+language+`","captchaId":"`+captchaID+`","protocol":"https://"}));`
    console.log('jsToInject', jsToInject)
    //页面加载完成触发
    const webViewOnLoad = (syntheticEvent: { nativeEvent: any; }) => {
      const {nativeEvent} = syntheticEvent;
      const curl = nativeEvent.url;
      //根据url地址判断刚才已经完成什么操作
      const jmurl = decodeURIComponent(curl);
    }


    var {
        height: deviceHeight,
        width: deviceWidth
    } = Dimensions.get('window');

    return (
      <View style={{flex: 1}}>
        <WebView
          ref={(r) => (setWebref(r))}
          allowFileAccess={true}
          source={{uri: sources}}
          onLoad={webViewOnLoad}
          allowFileAccessFromFileURLs={true}
          domStorageEnabled={true}
          allowUniversalAccessFromFileURLs={true}
          originWhitelist={['*']}
          injectedJavaScript={jsToInject}
          automaticallyAdjustContentInsets={false}
          onMessage={(e: {nativeEvent: {data?: any}}) => {
            //Alert.alert('Message received from JS:', e.nativeEvent.data);
            const type = JSON.parse(e.nativeEvent.data).type;
            const data = JSON.parse(e.nativeEvent.data).data;
            switch (type) {
              case 'ready':
                // Alert.alert(this.props.show)
                setStatus('ready')
                // console.debug(this.props.show,this.status)
                break;
              case 'fail':
                //this.props.onFail(data)
                break;
              case 'error':
                //this.props.onError(data)
                break;
              case 'result':
                onSuccess(data)
                break;
              case 'close':
                onClose()
                break;
            }
          }}
          mixedContentMode="compatibility"
          allowingReadAccessToURL="*"
          useWebKit={true}
        />
      </View>
    )
}

export default React.memo(CaptchaComponent)