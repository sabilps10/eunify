/*
Reference to iOS UIStackView
https://developer.apple.com/documentation/uikit/uistackview
*/
import React, { Children, Component, Fragment, FunctionComponent, ReactNode } from "react"
import { FlexStyle, StyleProp, StyleSheet, View, ViewStyle } from "react-native"


interface StackViewParams {
    style?: StyleProp<ViewStyle>,
    direction?: 'row' | 'column',
    spacing?: number
}

const stackView: FunctionComponent<StackViewParams> = (params) => {

    const style = params.style
    const direction = params.direction ?? 'row'
    const spacing = params.spacing ?? 0

    const items = () => {
        if (spacing === 0) return params.children
        if (!params.children) return null
        
        return Children.toArray(params.children).flatMap((child, index) => {
            let key = index.toString()
            let rappedChild = <Fragment key={key}>{child}</Fragment>

            if (index === 0) return rappedChild
            let spaceKey = (index+0.1).toString()
            let spaceView = <View key={spaceKey} style={direction === 'row' ? {width: spacing} : {height: spacing}} />

            return [spaceView, rappedChild]
        }).flat()
    }

    return (
        <View
            style={[styles.stackView, style, {flexDirection: direction}]}>
            { items() }
        </View>
    )
}

const styles = StyleSheet.create({
    stackView: {
        flexDirection: 'column',
    },
})

export default stackView