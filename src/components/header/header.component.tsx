import React, { Fragment, ReactNode, useEffect, useMemo } from 'react';
import { StyleSheet, Text, View, ViewStyle } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTheme } from '@react-navigation/native';
import StackView from '../stackView.component'

interface HeaderInterface {
    LeftComponent?: ReactNode,
    leftComponents?: ReactNode | ReactNode[],
    MiddleComponent?: ReactNode,
    RightComponent?: ReactNode,
    rightComponents?: ReactNode | ReactNode[],
    style?: ViewStyle
}

const Header = ({
    LeftComponent = null,
    leftComponents = [],
    MiddleComponent = null,
    RightComponent = null,
    rightComponents = [],
    style = {}
}: HeaderInterface) => {
    const { colors } = useTheme();

    const leftActionsComponent = useMemo(() => {
        if (!leftComponents) return null
        let components = Array.isArray(leftComponents) ? leftComponents : [leftComponents]

        if (components.length == 0) return null
        return (
            <StackView spacing={10}>
                {components}
            </StackView>
        )
    }, [leftComponents])

    const rightActionsComponent = useMemo(() => {
        if (!rightComponents) return null
        let components = Array.isArray(rightComponents) ? rightComponents : [rightComponents]

        if (components.length == 0) return null
        return (
            <StackView spacing={10}>
                {components}
            </StackView>
        )
    }, [rightComponents])

    return (
        <SafeAreaView
            style={[styles.safeAreaView, { backgroundColor: colors.MainBg }, style]}
            edges={['top', 'left', 'right']}
        >
            <View style={styles.container}>
                {LeftComponent ? LeftComponent : null}
                <View style={styles.leftComponents}>
                    {leftActionsComponent}
                </View>
                <View style={{flex: 1}}>
                    {MiddleComponent ? MiddleComponent : null}
                </View>
                {RightComponent ? RightComponent : null}
                <View style={styles.rightComponents}>
                    {rightActionsComponent}
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safeAreaView: {
        width: '100%',
    },
    container: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftComponents: {
        marginLeft: 16,
        height: '100%',
        flexDirection: 'row'
    },
    rightComponents: {
        marginRight: 16,
        height: '100%',
        flexDirection: 'row'
    },
})

export default Header;