import { useTheme } from "@react-navigation/native"
import React, { ReactNode } from "react"
import { Image, ImageSourcePropType, Pressable, StyleProp, Text, TextStyle, View } from "react-native"
import { makeGlobalStyles } from "../../styles/styles"
import PressableComponent from "../../views/utils/PressableComponent"

interface HeaderButtonInterface {
    onPress?: () => void,
    iconComponent?: ReactNode
    source?: ImageSourcePropType
    title?: string,
    titleStyle?: StyleProp<TextStyle>,
    component?: ReactNode
}

const HeaderButton = (params: HeaderButtonInterface) => {

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    const titleComponent = (title?: string, style?: StyleProp<TextStyle>) => {
        if (!title) return null
        
        return (
            <View
                style={{paddingHorizontal: 5}}>
                <Text
                    style={[style]}>
                    {title}
                </Text>
            </View> 
        )
    }

    return (
        <PressableComponent
            style={{justifyContent: 'center', flexDirection: 'row', alignItems: 'center'}} 
            onPress={params.onPress}>
            { params.iconComponent ? params.iconComponent : null }
            { params.source ? <Image source={params.source} /> : null }
            { titleComponent(params.title, params.titleStyle) }
            { params.component ? params.component : null }
        </PressableComponent>
    )
}

export default HeaderButton
