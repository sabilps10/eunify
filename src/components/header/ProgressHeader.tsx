import React from 'react';
import {View} from 'react-native';
import tw from '../../utils/tailwind';

const ProgressHeader = props => {
  const Stage = props => {
    const isOn = +props.currentStep === props.idx;
    const isPrevious = +props.currentStep < props.idx;

    if (isOn) {
      return (
        <View
          style={[
            tw`mx-0.5 w-[30px] h-[6px] bg-brand1`,
            {borderRadius: 5},
          ]}></View>
      );
    } else if (isPrevious) {
      return (
        <View
          style={[
            tw`mx-0.5 w-[30px] h-[6px] bg-black/10`,
            {borderRadius: 5},
          ]}></View>
      );
    } else {
      return (
        <View
          style={[
            tw`mx-0.5 w-[30px] h-[6px] bg-white`,
            {borderRadius: 5},
          ]}></View>
      );
    }
  };

  return (
    <View
      style={[
        tw`flex flex-row h-10 w-full justify-center items-center bg-brand3`,
      ]}>
      <Stage currentStep={props.currentStep} idx={1} />
      <Stage currentStep={props.currentStep} idx={2} />
      {props.entity !== 'EMX' && (
        <>
          <Stage currentStep={props.currentStep} idx={3} />
          <Stage currentStep={props.currentStep} idx={4} />
        </>
      )}
    </View>
  );
};

export default ProgressHeader;
