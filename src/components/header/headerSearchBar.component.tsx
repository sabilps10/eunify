import React, { useState } from 'react';
import { StyleSheet, TextInput, TextStyle, useColorScheme, View } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import TopBarSearch from '../../../assets/svg/TopbarSearch';

interface HeaderSearchBarInterface {
    keyword?: string,
    onChangeText?: (text: string) => void,
    style?: TextStyle,
    editable?: boolean,
}

const HeaderSearchBar = (params: HeaderSearchBarInterface) => {
    
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors)
    const [text, setText] = useState<string>(params.keyword ?? '')
    const scheme = useColorScheme();

    const onChange = (text: string) => {
        setText(text)
        params.onChangeText(text)
    }

    return (
        <View style={[styles.container, { backgroundColor: colors.MainBg }]}>
            <TextInput
                style={[globalStyles.Form_Infield, styles.textInput, params.style]}
                maxLength={100}
                numberOfLines={1}
                value={text}
                onChangeText={onChange}
                returnKeyType='search'
                editable={params.editable ?? true}
            />
            <TopBarSearch color={colors.MainFont} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 32,
        borderRadius: 15,
        backgroundColor: '#ffff00',
        marginHorizontal: 18,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        flex: 1,
        paddingVertical: 0,
        height: '100%',
    }
})

export default HeaderSearchBar