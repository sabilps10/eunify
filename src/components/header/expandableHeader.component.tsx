import React, { memo, ReactNode, useEffect, useMemo, useState } from 'react'
import { StyleSheet, View, useColorScheme, Text, Image, Pressable, Linking } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context'
import { State } from '../../redux/root-reducer';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation, useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import StackView from '../stackView.component'
import { toCurrencyDisplay } from '../../utils/stringUtils'
import { useTranslation } from 'react-i18next';
import ActionButton2 from '../buttons/actionButton2.component';
import { bindActionCreators } from 'redux';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { getDict } from '../../views/utils/CommonFunction';
import { THEME_TYPE } from '../../types/types';
import { ScreenNavigationProp } from '../../routes/route';
import ArrowDown from '../../../assets/svg/ArrowDown';
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import ArrowUp from '../../../assets/svg/ArrowUp';
import TopbarSwitchAcc from '../../../assets/svg/TopbarSwitchAcc';
import { lv3Registration } from '../../utils/api/userApi';
import PressableComponent from '../../views/utils/PressableComponent';
import { AccountActionCreators } from '../../redux/account/actionCreator';
import { SelectionContextLocation } from '../../redux/selection/type';
import { aoEntry } from '../../utils/navigation';

interface ExpandableHeaderInterface {
    leftComponents?: ReactNode | ReactNode[],
    rightComponents?: ReactNode | ReactNode[],
    isDemo?: boolean,
    enableBottomRadius?: boolean,
    enableExpand?: boolean,
    grayOverlay?: ReactNode,
    isShowBalance?: boolean
}

const ExpandableHeader = ({
    leftComponents = [],
    rightComponents = [],
    isDemo = false,
    enableBottomRadius = true,
    enableExpand = true,
    isShowBalance = true
}: ExpandableHeaderInterface) => {

    const [t, i18n] = useTranslation();

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const styles = makeStyles(colors)

    const loginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const loginTrader = useSelector((state: State) => state.trading.accountDetail?.Login);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const headerExpanded = useSelector((state: State) => state.account.isHeaderExpanded);

    const {individual} = useSelector((state: State) => state.accountOpening);

    const login = useSelector((state: State) => state.trading.accountDetail?.Login?.toString() ?? '-')
    // const balance = useSelector((state: State) => state.trading.accountDetail?.Balance ?? 0.0)

    const displayHeaderName = useSelector((state: State) => state.account.account?.DisplayHeaderName ?? '');
    
    const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

    const navigation = useNavigation<ScreenNavigationProp<'TabDirectory'>>();

    // const [headerExpanded, setHeaderExpanded] = useState<boolean>(false);

    const dispatch = useDispatch();
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetIsHeaderExpanded } = bindActionCreators(AccountActionCreators, dispatch);

    const headerTextColor = isDemo ? colors.DemoHeaderFont : colors.HeaderFont

    // useEffect(()=>{
    //     if (loginTrader !== currentTradeAccount) {
    //         var enitity = 'HK';
    //         for (var i = 0 ; i < tradingAccountList.length; i++){
    //             if (tradingAccountList[i].Account === currentTradeAccount) {
    //                 enitity = tradingAccountList[i].Entity
    //                 continue;
    //             }
    //         }

    //         const fetchData = async () => {
    //             SetIsShowLoading(true);
    //             await switchAccount(currentTradeAccount!.toString(), false, enitity)
    //             // navigation.navigate('TabDirectory')
    //             SetIsShowLoading(false);
    //         }
            
    //         fetchData().catch(console.error);
    //     }
        
    // }, [currentTradeAccount])

    useEffect(() => {
        let isLevel3 = loginLevel === 3

        if (!isLevel3)
            SetIsHeaderExpanded(false)
    }, [loginLevel, isDemo])

    const onPressExpand = () => 
    {
        console.debug('onPressExpand')
        if (enableExpand)
            SetIsHeaderExpanded(!headerExpanded);
    }

    const onPressSwitch = () => {
        const selectionOptions: string[] = [];
        for (var i = 0 ; i < tradingAccountList.length; i++){
            selectionOptions.push(tradingAccountList[i].Account);
        }
        
        OpenCloseSelection({
            selectedOption: loginTrader!.toString(),
            selectOptions: getDict(selectionOptions),
            isOpen: !isOpen,
            location: SelectionContextLocation.CurrentTradeAccount
        })
    }

    const onPressGuest = () => {
        navigation.push('CognitoSignUp')
    }

    const onPressRegisterRealTradingAccount = () => {
        aoEntry(individual, navigation);
    };

    const guestTitleComponent = () => {
        return (
            <PressableComponent onPress={onPressGuest}>
                <StackView spacing={8} style={styles.guestTitle}>
                    <Text style={[globalStyles.Big_Text_B, styles.guestTitleText]}>
                        {t('header.guest.signUp')}
                    </Text>
                    <CircleArrow20px color={colors.Brand3} backgroundColor={colors.White} />
                </StackView>
            </PressableComponent>
        )
    }

    const Level2TitleComponent = () => {
        return (
            <PressableComponent onPress={onPressRegisterRealTradingAccount}>
                <StackView spacing={8} style={styles.level2Title}>
                    <Text style={[globalStyles.Big_Text_B, {color: colors.HeaderFont}]}>
                        {t('header.guest.registerRealTradingAccount')}
                    </Text>
                    <CircleArrow20px color={colors.Brand3} backgroundColor={colors.White} />
                </StackView>
            </PressableComponent>
        )
    }

    const TradeAccountTitleComponent = memo((props: any) => {

        const equity = useSelector((state: State) => state.trading.accountDetail?.Equity ?? 0.0)

        return (
            <PressableComponent 
                style={[styles.tradeAccountTitle, {top: isShowBalance ? 5 : 0, 
                justifyContent: !isShowBalance ? 'center': 'flex-start',
                alignItems: 'center',
                position: !isShowBalance ? 'absolute': 'relative'
                }]}
                onPress={onPressExpand}>
                <View style={[styles.textWrapper]}>
                    <View style={{flexDirection: 'row'}}>
                        {
                            isDemo || (tradingAccountList && tradingAccountList.length <= 1) ? 
                                null
                            :
                                <PressableComponent onPress={onPressSwitch}>
                                    <TopbarSwitchAcc color={colors.White} />
                                </PressableComponent>
                        }
                        <View style={{padding: 2}}/>
                        <Text style={[globalStyles.Note_B, { fontWeight: '600', fontSize: 12, lineHeight: 19.2 }, { color: headerTextColor }]}>
                            { isDemo ? 'DEMO_' + displayHeaderName + '-' + props.login : displayHeaderName + '-' + props.login }
                        </Text>
                    </View>
                    {
                        isShowBalance ? 
                            <Text style={[globalStyles.Note_120, { fontWeight: '500', fontSize: 12, lineHeight: 14.4 }, { color: headerTextColor }]}>
                                {/* USD {toCurrencyDisplay(balance)} */}
                                USD {toCurrencyDisplay(equity)}
                            </Text>
                        : 
                            null
                    }
                </View>
                {
                    enableExpand && !props.headerExpanded &&
                    <View style={styles.expandIcon}>
                        <ArrowDown color={colors.White} />
                    </View>
                }
            </PressableComponent>
        )
    })

    const titleComponent = () => {
        var titlecomponent: ReactNode | null = guestTitleComponent()

        switch (loginLevel) {
            case 2: titlecomponent = isDemo ? <TradeAccountTitleComponent login={login} headerExpanded={headerExpanded}/> : <Level2TitleComponent/>; break;
            case 3: titlecomponent = <TradeAccountTitleComponent login={login} headerExpanded={headerExpanded}/>; break;
            default: break
        }
        return (
            titlecomponent
        )
    }
    const leftActionsComponent = useMemo(() => {
        if (!leftComponents) return null

        let component = Array.isArray(leftComponents) ? leftComponents : [leftComponents]
        if (component.length === 0) return
        return (
            <View style={[styles.leftComponents]}>
                <StackView 
                    direction='row' 
                    spacing={10}>
                    {component}
                </StackView>
            </View>
        )
    }, [leftComponents])

    const rightActionsComponent = useMemo(() => {
        if (!rightComponents) return null

        let component = Array.isArray(rightComponents) ? rightComponents : [rightComponents]
        if (component.length === 0) return
        return (
            <View style={[styles.rightComponents]}>
                <StackView 
                    direction='row'
                    spacing={10}>
                    {component}
                </StackView>
            </View>
        )
    }, [rightComponents])

    const ExpandComponent = () => {

        const profit = useSelector((state: State) => state.trading.accountDetail?.Profit ?? 0.0)
        const liqMargin = useSelector((state: State) => state.trading.accountDetail?.LiqMargin ?? 0.0)
        const margin = useSelector((state: State) => state.trading.accountDetail?.Margin ?? 0.0)
        const marginFree = useSelector((state: State) => state.trading.accountDetail?.MarginFree ?? 0.0)
        return (
            <PressableComponent onPress={onPressExpand}>
                <View style={styles.expandContainer}>
                    <StackView style={{width: '100%'}} spacing={10}>
                        <View style={styles.expandContainerItem}>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.HeaderDetailFont }]}>
                                {t('header.floatingPL')}
                            </Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.HeaderDetailFont }]}>
                                {(profit > 0 ? '+':'') + toCurrencyDisplay(profit)}
                            </Text>
                        </View>
                        <View style={styles.expandContainerItem}>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.HeaderDetailFont }]}>
                                {t('header.available')}
                            </Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.HeaderDetailFont }]}>
                                {toCurrencyDisplay(marginFree)}
                            </Text>
                        </View>
                    </StackView>
                    <StackView style={{width: '100%'}} spacing={10}>
                        <View style={styles.expandContainerItem}>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.HeaderDetailFont }]}>
                                {t('header.margin')}
                            </Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.HeaderDetailFont }]}>
                                {toCurrencyDisplay(margin)}
                            </Text>
                        </View>
                        <View style={styles.expandContainerItem}>
                            <Text style={[globalStyles.Small_Note_B, { color: colors.HeaderDetailFont }]}>
                                {t('header.liqMargin')}
                            </Text>
                            <Text style={[globalStyles.Small_Note, { color: colors.HeaderDetailFont }]}>
                                {toCurrencyDisplay(liqMargin)}
                            </Text>
                        </View>
                    </StackView>
                    {
                        loginLevel === 2 && 
                        <View style={{paddingVertical: 10}}>
                            <ActionButton2 
                                label={t('header.guest.registerRealTradingAccount')} 
                                labelStyle={[globalStyles.ButtonText, {alignSelf: 'center'}]} 
                                onPress={onPressRegisterRealTradingAccount} />
                        </View>
                    }
                    <View style={styles.collapseIcon}>
                        <ArrowUp color={colors.White} />
                    </View>
                </View>
            </PressableComponent>
        )
    }

    return (
        <View style={{backgroundColor: colors.WrapperBg}}>
            <SafeAreaView 
                style={[enableBottomRadius && { borderBottomLeftRadius: 12, borderBottomRightRadius: 12 }, { backgroundColor: isDemo ? colors.Brand1 : colors.Brand3 }]}
                edges={['top', 'left', 'right']}>
                <View style={styles.container}>
                    <View style={styles.topWrapper}>
                        {leftActionsComponent}
                        {titleComponent()}
                        {rightActionsComponent}
                    </View>
                    {
                        enableExpand && headerExpanded?
                        <ExpandComponent/>
                        :
                        <></>
                    }
                </View>
            </SafeAreaView>
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center'
    },
    topWrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        height: 55
    },
    leftComponent: {
        position: 'absolute',
        left: 16
    },
    leftComponents: {
        position: 'absolute',
        left: 16,
        height: '100%',
        flexDirection: 'row'
    },
    rightComponent: {
        position: 'absolute',
        right: 12
    },
    rightComponents: {
        position: 'absolute',
        right: 16,
        height: '100%',
        flexDirection: 'row'
    },
    guestTitle: {
        alignItems: 'center',
        flex: 1,
    },
    guestTitleText: {
        color: colors.HeaderFont,
    },
    level2Title: {
        alignItems: 'center',
        flex: 1,
    },
    tradeAccountTitle: {
        height: '100%',
        // justifyContent: 'center',
        // alignItems: 'center',
        // position: 'absolute',   
        top: 0
    },
    textWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    expandIcon: {
        height: 24,
        bottom: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    collapseIcon: {
        marginTop: 3.5,
        marginBottom: 4.5,
        alignSelf: 'center',
    },
    refreshIcon: {
        position: 'absolute',
        top: 5,
        right: -22,
    },
    expandContainer: {
        width: '100%',
        flexDirection: 'column',
        paddingLeft: 20,
        paddingRight: 20,
    },
    expandContainerRow: {
        flexDirection: 'row',
        width: '100%',
    },
    expandContainerItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
    },
    expandContainerItemSpace: {
        width: 10,
    },
});
export default ExpandableHeader
