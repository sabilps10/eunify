import React, {Fragment, ReactNode, useEffect} from 'react';
import {StyleSheet, Text, View, ViewStyle, useColorScheme, Pressable} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '@react-navigation/native';
import {create} from 'twrnc';
import {makeGlobalStyles} from '../../styles/styles';

interface HeaderInterface {
  LeftComponent?: ReactNode;
  leftComponents?: ReactNode[];
  MiddleComponent?: ReactNode;
  RightComponent?: ReactNode;
  rightComponents?: ReactNode[];
  style?: ViewStyle;
  func?: Function;
  title?: string;
}

const HeaderDarkArrowButton = (props: HeaderInterface) => {
  const {colors} = useTheme();
  const tw = create(require(`../../tailwind.config`));

  const globalStyles = makeGlobalStyles(colors);
  const scheme = useColorScheme();
  return (
    <View style={[tw`flex flex-row items-center bg-[#07D3DC]`]}>
      <SafeAreaView
        //style={styles.header}
        edges={['top', 'left', 'right']}>
        <View style={[tw`flex flex-row items-center bg-[#07D3DC]`]}>
          <View style={tw`p-3`}>
            <Pressable onPress={()=> props.func}>
            <Icon
              //size={28}
              style={{fontSize: 28}}
              color="#444444"
              name="chevron-left"
            />
            </Pressable>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};


export default HeaderDarkArrowButton;
