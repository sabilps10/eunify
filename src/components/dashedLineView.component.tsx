import { useTheme } from "@react-navigation/native"
import React from "react"
import { StyleProp, StyleSheet, View, ViewStyle } from "react-native"
import { THEME_TYPE } from "../types/types"

interface DashedLineViewInterface {
    style?: StyleProp<ViewStyle>,
    dashedLineStyle?: StyleProp<ViewStyle>,
}

const DashedLineView = (config: DashedLineViewInterface) => {

    const { colors } = useTheme()
    const styles = makeStyles(colors)

    return (
        <View style={[styles.container, config.style]} >
            <View style={[styles.dashedLine, config.dashedLineStyle]} />
        </View>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        flex: 1,
        flexGrow: 1,
        height: 1,
        overflow: 'hidden',
    },
    dashedLine: {
        height: 1,
        borderWidth: 1,
        borderColor: colors.DashedLine,
        borderStyle: 'dashed'
    },
})

export default DashedLineView