import { useTheme } from "@react-navigation/native";
import React, { useEffect, useLayoutEffect, useState } from "react"
import { useTranslation } from "react-i18next";
import WebView from "react-native-webview";
import TopBarBack from "../../assets/svg/TopBarBack";
import { Props } from "../routes/route"
import { makeGlobalStyles } from "../styles/styles";
import Header from "./header/header.component";
import HeaderButton from "./header/headerButton.component";
import { useSafeAreaInsets } from "react-native-safe-area-context"
import { getMIOToken, OAuth2Token, UserInfoToken, checkIsMIDepositReady } from "../utils/api/apiMIO";
import { MIOAuthDomain, MIOClientId, MIOClientSecret, MIOUserTokenDomain, MIOWebDomain } from "../config/constants";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../redux/root-reducer";
import { Auth } from "aws-amplify";
import { Linking, Platform, View } from "react-native";

import { SetIsShowLoading } from "../redux/state/action";
import { getPersonMessage } from '../utils/api/apiAccountOpening';
import { bindActionCreators } from 'redux';
import { OpenPositionsActionCreators } from '../redux/trading/actionCreator';
import gifToBase64DataURI from '../../assets/images/loading_animation.base64_gif'
import { AccountOpeningCreators } from "../redux/accountOpening/actionCreator";

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export enum MIOType {
    BankManagement,
    Deposit,
    Withdraw,
}

// <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
const INJECTEDJAVASCRIPT = `const metaViewportNotZoomToInput = document.createElement('meta'); metaViewportNotZoomToInput.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1'); metaViewportNotZoomToInput.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(metaViewportNotZoomToInput); `

const MIO: React.FC<Props<'MIO'>> = ({ route, navigation }) => {

    const [t] = useTranslation();

    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)
    const insets = useSafeAreaInsets()

    const type = route.params.type

    const [token, setToken] = useState<string | null>(null)
    const [userInfo, setUserInfo] = useState<string | null>(null)
    const [source, setSource] = useState<string | null>(null)
    const [MIOToken, setMIOToken] = useState<string>('')
    const [isMIDepositReady, SetIsMIDepositReady] = useState<boolean>(false)

    const cognitoState = useSelector((state: State) => state.cognito);
    const loginTrader = useSelector((state: State) => state.trading.accountDetail?.Login);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const mio = useSelector((state: State) => state.account.mio);
    const {selectedTradingAccount:AOTradingAccount} = useSelector((state:State)=>state.accountOpening)

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => null
        });

    }, [navigation]);

    useEffect(() => {
        if ( (loginTrader ?? AOTradingAccount) ) { return; }

        const fetchData = async () => {
            await requestMIOToken();
            // const headerToken = await requestMIOOAuth2Token()
            // await requestMIOUserInfoToken(headerToken)
        }
        
        fetchData().catch(console.error);

    }, [type]);

    useEffect(() => {
        if (!MIOToken) {
            // TODO: show alert and pop back...
            return
        } else if (MIOToken && MIOToken.trim() !== '') {
            SetIsShowLoading(false)
            // is ready to deposit
            const fetchData = async () => {
                var regex = /[?&]([^=#]+)=([^&#]*)/g, params = {}, match;
                while (match = regex.exec(MIOToken)) {
                    params[match[1]] = match[2];
                }
                const response = await checkIsMIDepositReady(mio.webDomain, params['token'], (loginTrader?.toString() ?? AOTradingAccount))
                if (response && response.ready) {
                    SetIsMIDepositReady(true)
                } else {
                    console.debug('[MIO useEffect [MIOToken]] checkIsMIDepositReady not ready sleep before re-fetch ')
                    await sleep(5000)
                    console.debug('[MIO useEffect [MIOToken]] checkIsMIDepositReady not ready await fetchData()')
                    await fetchData().catch((error) => {
                        console.error(`[MIO useEffect [MIOToken]] checkIsMIDepositReady fetchData() nested catch ${error}`)
                        SetIsMIDepositReady(true)
                    })
                }
            }

            fetchData().catch((error) => {
                console.error(`[MIO useEffect [MIOToken]] checkIsMIDepositReady fetchData() catch ${error}`)
                SetIsMIDepositReady(true)
            })
        }
    }, [MIOToken])

    useEffect(() => {
        if (!isMIDepositReady) return;
        switch(type) {
            case MIOType.BankManagement:
                // setSource((mio ? mio.webDomain : MIOWebDomain) + '/bank-management?token=' + token + '&userInfo=' + userInfo)
                setSource((mio ? mio.webDomain : MIOWebDomain) + '/bank-management' + MIOToken)
                break

            case MIOType.Deposit:
                // setSource((mio ? mio.webDomain : MIOWebDomain) + '/deposit?token=' + token + '&userInfo=' + userInfo)
                setSource((mio ? mio.webDomain : MIOWebDomain) + '/deposit' + MIOToken)
                break

            case MIOType.Withdraw:
                // setSource((mio ? mio.webDomain : MIOWebDomain) + '/withdraw?token=' + token + '&userInfo=' + userInfo)
                setSource((mio ? mio.webDomain : MIOWebDomain) + '/withdraw' + MIOToken)
                break
        }
    }, [isMIDepositReady])

    useEffect(() => {
        if ((loginTrader ?? AOTradingAccount) && (loginTrader ?? AOTradingAccount).toString().trim() !== '') {
            const fetchData = async () => {
                await requestMIOToken();
            }
            
            fetchData().catch(console.error);

        }
    }, [loginTrader, AOTradingAccount])

    const { individual } = useSelector((state: State) => state.accountOpening)
    const dispatch = useDispatch();
    const { SetAccountDetail } = bindActionCreators(OpenPositionsActionCreators, dispatch)
    const { SetSelectedTradingAccount } = bindActionCreators(AccountOpeningCreators, dispatch);

    const requestMIOToken = async() => {
        const session = await Auth.currentSession();
        var token = session.getAccessToken().getJwtToken();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        var cognitoUserName = payload['cognito:username'];

        if ( !(loginTrader ?? AOTradingAccount) ) {
            console.debug('[MIO requestMIOToken] tradingAccount missing will handle await getPersonMessage()')
            await getPersonMessage({
                token,
                payload: {uuid: individual?.id},
                onSuccess: async res => {
                  if (res.result === true && res.response && res.response.accountId.length >0 && res.response.accountId[0] && res.response.accountId[0] !== '') {
                    // const numberAccountId = Number(res.response.accountId[0])
                    // SetAccountDetail({Login: numberAccountId});
                    SetSelectedTradingAccount(res.response.accountId[0]);
                  } else {
                    console.debug('[MIO requestMIOToken] getPersonMessage onSuccess else sleep before re-fetch ')
                    await sleep(5000)
                    console.debug('[MIO requestMIOToken] getPersonMessage onSuccess else will await requestMIOToken()')
                    await requestMIOToken();
                  }
                },
                onFailed: error => {
                  console.error(`[MIO requestMIOToken] getPersonMessage().onFailed ${JSON.stringify(error)}`);
                  
                  const fetchData = async () => {
                    console.debug('[MIO requestMIOToken] getPersonMessage onFailed fetchData sleep before re-fetch ')
                    await sleep(5000)
                    console.debug('[MIO requestMIOToken] getPersonMessage onFailed fetchData await requestMIOToken()')
                    await requestMIOToken();
                  }

                  fetchData().catch(console.error);
                },
            });
            return;
        }

        var data = await getMIOToken(token, cognitoUserName, loginTrader ? loginTrader.toString():AOTradingAccount)
        if (data && data.token && data.token.trim() !== '') {
            setMIOToken(data.token)
        } else  {
            SetIsShowLoading(true)
        }
    }

    const requestMIOOAuth2Token = async() => {
        const session = await Auth.currentSession();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        var cognitoUserName = payload['cognito:username'];

        var authDomain = mio ? mio.authDomain : MIOAuthDomain;
        var clientId = mio ? mio.clientId : MIOClientId;
        var clientSecret = mio ? mio.clientSecret : MIOClientSecret;

        let oAuth2TokenResponse = await OAuth2Token(authDomain, clientId, clientSecret)

        let accessToken = oAuth2TokenResponse['access_token']

        if (!accessToken) {
            // TODO: show alert and pop back...
            return
        }
        setToken(accessToken)

        return accessToken
    }

    const requestMIOUserInfoToken = async(headerToken: string) => {
        var userId = '';
        tradingAccountList.forEach((item)=> {
            if (item.Account === loginTrader.toString()) {
                userId = item.userId;
            }
        })

        var userTokenDomain = mio ? mio.userTokenDomain : MIOUserTokenDomain;
        var entityCode = mio ? mio.entityCode : '';

        let userInfoTokenResponse = await UserInfoToken(userTokenDomain, entityCode, userId, loginTrader.toString(), headerToken)

        let encrypted = userInfoTokenResponse['encrypted']

        if (!encrypted) {
            // TODO: show alert and pop back...
            return
        }
        setUserInfo(encrypted)
    }

    const onPressBack =async () => {
        navigation.pop()
    }

    const handleShouldStartLoadWithRequest = (event) => {
        if (Platform.OS === 'ios') {
            return true;
            // Only allow to load page if the domain is MIO itself, otherwise ignore
            if (event.url.includes(MIOWebDomain) || event.url.includes("checkout.com") || event.url.includes("ckotech.co")) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    };

    const handleOnMessage = (event) => {

        var message = event.nativeEvent.data;
        try {
            const messageJson = JSON.parse(message);

            if (messageJson.action === 'open_url') {
                const fullUrl = MIOWebDomain + messageJson.redirect_url.toString();
                Linking.openURL(fullUrl);
            } else {
            }

        } catch (e) {
        }
    }

    const webViewConfig = {
        useWebKit: true,
        experimental: { 
          postBody: true
        }
      };

    return (
        source ? 
        <View style={{ flex: 1 }}>
             <WebView 
                containerStyle={{marginBottom: insets.bottom}}
                source={{ uri: source}}
                javaScriptCanOpenWindowsAutomatically={true}
                javaScriptEnabled={true}
                javaScriptEnabledAndroid
                webviewDebuggingEnabled
                onShouldStartLoadWithRequest={handleShouldStartLoadWithRequest}
                onMessage={handleOnMessage}
                {...webViewConfig}
                {...(Platform.OS === 'ios' ? {injectedJavaScript: INJECTEDJAVASCRIPT} : {})}
            />
        </View>
        :
            <WebView
                source={{ html: 
                    `
<head><meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,maximum-scale=1"></head>
<div style="display: flex; flex-direction: column; justify-content: center; height: 100%">
<img width="100%" src="${gifToBase64DataURI}" />
</div>
`
                }}
            />
    )
}

export default MIO