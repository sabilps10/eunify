import React, {useState, useEffect} from 'react';
import { Image, StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import PasswordMessage from './passwordMessage.component'
import {hasInValidCharacters} from '../../views/utils/CommonFunction'

import { useTranslation } from 'react-i18next';

interface definitionInterface{
    (passwordVerfied:boolean):void;
}

interface PasswordMessageDialogInterface {
    password: string,
    callback: definitionInterface
}

interface CheckPasswordInterface {
    length: boolean,
    lowerCase: boolean,
    upperCase: boolean,
    number: boolean,
    specialCharacter: boolean,
    passwordVerfied: boolean
}

const initCheckPassword : CheckPasswordInterface = {
    length: false,
    lowerCase: false,
    upperCase: false,
    number: false,
    specialCharacter: false,
    passwordVerfied: false
}

const PasswordMessageDialog = ({ password = '', callback }: PasswordMessageDialogInterface) => {
    const [t, i18n] = useTranslation();
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [checkPassword, setCheckPassword] = useState<CheckPasswordInterface>(initCheckPassword);

    function hasCase(str: string, isLowerCase: boolean) {
        if (str.length <= 0) {
            return false;
        }
        if (isLowerCase) {
            return (/[a-z]/.test(str));
        } else {
            return (/[A-Z]/.test(str));
        }
    }

    function hasNumber(str: string) {
        if (str.length <= 0) {
            return false;
        }
        return (/[0-9]/.test(str));
    }

    function hasSpecialCharacters(str: string) {
        if (str.length <= 0) {
            return false;
        }
        return (/\W|_/.test(str));
    }

    const updatePassword = async(value: string) => {
        var isLength = false;
        var isLowerCase = false;
        var isUpperCase = false;
        var isNumber = false;
        var isSpecialCharacter = false;
        var isInValidCharacters = false;

        if (value.length >= 8) {
            isLength = true;
        }
        if (hasCase(value, true)) {
            isLowerCase = true;
        }
        if (hasCase(value, false)) {
            isUpperCase = true;
        }
        if (hasNumber(value)) {
            isNumber = true;
        }
        if (hasSpecialCharacters(value)) {
            isSpecialCharacter = true;
        }

        if (hasInValidCharacters(value)) {
            isInValidCharacters = true;
        }

        console.debug('updatePassword', isInValidCharacters)
        var isVerify = isLength && isLowerCase && isUpperCase && isNumber && isSpecialCharacter && !isInValidCharacters
        setCheckPassword({
            length: isLength,
            lowerCase: isLowerCase,
            upperCase: isUpperCase,
            number: isNumber,
            specialCharacter: isSpecialCharacter,
            passwordVerfied: isVerify
        })

        callback(isVerify)
    }

    useEffect(() => {
        updatePassword(password)
    }, [password]);

    return (
        <View>
            <View style={styles.row}>
                <PasswordMessage message={t('password.characters')} isVerify={checkPassword.length} />
                <PasswordMessage message={t('password.lowercase')} isVerify={checkPassword.lowerCase} />
            </View>
            <View style={styles.row}>
                <PasswordMessage message={t('password.number')} isVerify={checkPassword.number} />
                <PasswordMessage message={t('password.uppercase')} isVerify={checkPassword.upperCase} />
            </View>
            <View style={styles.row}>
                <PasswordMessage message={t('password.specialCharacters')} isVerify={checkPassword.specialCharacter} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        padding: 4,
        alignSelf: 'flex-start'
    },
    row :{
        flexDirection: 'row'
    }
})

export default PasswordMessageDialog