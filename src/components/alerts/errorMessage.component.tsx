import React from 'react';
import { StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import Info from '../../../assets/svg/Info';

interface ErrorMessageInterface {
    message?: string,
    style?: ViewStyle
}

const ErrorMessage = ({ message = 'Error Message', style = {} }: ErrorMessageInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <Info color={colors.Red} />
            <View style={{ width: 4 }} />
            {message !== '' && <Text style={[globalStyles.Small_Note, { color: colors.Red }]} >{message}</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        alignSelf: 'flex-start'
    }
})

export default ErrorMessage