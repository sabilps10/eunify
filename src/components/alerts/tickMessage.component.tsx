import React from 'react';
import { StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import VerifyCodeTick from '../../../assets/svg/VerifyCodeTick';
import CheckListProcessing from '../../../assets/svg/CheckListProcessing';

interface PasswordMessageInterface {
    message?: string,
    style?: ViewStyle,
    isTick: boolean
}

const TickMessage = ({ message = 'No message', style = {}, isTick }: PasswordMessageInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            {
                isTick?
                    <VerifyCodeTick color={colors.Green} backgroundColor={colors.TitleFont} />
                :
                    <CheckListProcessing color={colors.Brand2} backgroundColor={colors.TitleFont} />
            }
            <View style={{ width: 4 }} />
            <View style={{ justifyContent: 'center', flex: 1}}>
            {message !== '' && <Text style={[globalStyles.Note, {color: colors.FixedMainFont}]} >{message}</Text>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 20,
        padding: 4,
        alignSelf: 'flex-start',
    }
})

export default TickMessage