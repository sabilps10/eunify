import React from 'react';
import { StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

interface PasswordMessageInterface {
    message?: string,
    style?: ViewStyle
}

const BulletMessage = ({ message = 'No message', style = {} }: PasswordMessageInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            <View style={{height: 30, justifyContent: 'center'}}>
                <View style={[styles.circle, {backgroundColor: colors.Brand2}]} />
            </View>
            <View style={{ width: 4 }} />
            <View style={{justifyContent: 'center', flex: 1}}>
                {message !== '' && <Text style={[globalStyles.Note, { color: colors.FixedMainFont }]} >{message}</Text>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 20,
        padding: 4,
        alignSelf: 'flex-start'
    },
    circle: {
        width: 8,
        height: 8,
        borderRadius: 100 / 2,
        marginEnd: 12
    },
})

export default BulletMessage