import React from 'react';
import { StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import ValidCorrect18px from '../../../assets/svg/ValidCorrect18px';
import ValidWrong18px from '../../../assets/svg/ValidWrong18px';

interface PasswordMessageInterface {
    message?: string,
    style?: ViewStyle,
    isVerify: boolean
}

const PasswordMessage = ({ message = 'Error Message', style = {}, isVerify }: PasswordMessageInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={[styles.container, style]}>
            {
                isVerify?
                    <ValidCorrect18px color={colors.White} backgroundColor={colors.Green} />
                :
                    <ValidWrong18px color={colors.Red} />
            }
            <View style={{ width: 4 }} />
            {message !== '' && <Text style={[globalStyles.Small_Note, { color: colors.MainFont }]} >{message}</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 20,
        padding: 4,
        alignSelf: 'flex-start'
    }
})

export default PasswordMessage