import React from 'react';
import { StyleSheet, Text, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import ArrowRight from '../../../assets/svg/ArrowRight';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface MediumButtonInterface {
    title?: string,
    onPress: () => void,
    component?: React.ReactNode,
    type?: 'Solid' | 'Outline' | 'White',
    style?: ViewStyle,
    textStyle?: TextStyle
}

const MediumButton = ({ 
    title = '', 
    onPress, 
    component = null, 
    type = 'Solid', 
    style = {},
    textStyle = {}
 }: MediumButtonInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressFunc = () => onPress()

    return (
        <TouchableOpacityComponent
            onPress={onPressFunc}
            style={[
                styles.container,
                type === 'Solid' && { backgroundColor: colors.Brand1 },
                type === 'Outline' && { borderColor: colors.Brand2, borderWidth: 1 },
                type === 'White' && { borderColor: colors.White, borderWidth: 1 },
                style
            ]}>
            {
                component ?
                    component :
                    <Text style={[
                        globalStyles.ButtonText,
                        type === 'Solid' && { color: colors.Text },
                        type === 'Outline' && { color: colors.Brand2 },
                        type === 'White' && { color: colors.White },
                        textStyle
                    ]}>{title}</Text>
            }
            <View style={styles.arrow}>
            {
                type === 'Solid' ?
                    <ArrowRight color={colors.MainFont} /> :
                    type === 'Outline' ?
                        <ArrowRight color={colors.Brand2} /> :
                        <ArrowRight color={colors.White} />
            }
            </View>
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 34,
        paddingVertical: 5,
        paddingLeft: 18,
        paddingRight: 10,
        borderRadius: 17,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
})

export default MediumButton