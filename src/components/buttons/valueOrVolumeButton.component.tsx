import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import { THEME_TYPE } from '../../types/types';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface OrderOptionButton {
    label: string,
    value: number,
    onPress: (value: number) => void,
    style?: ViewStyle,
    isEnable?: boolean
}

const ValueOrVolumeButton = ({ label, value, onPress, style = {}, isEnable = true }: OrderOptionButton) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const styles = makeStyles(colors)

    const onPressFunc = () => {
        if (isEnable) {
            onPress(value)
        }
    }

    return (
        <TouchableOpacityComponent style={[styles.container, style, !isEnable ? {borderWidth: 0, backgroundColor: colors.Grey2} : {} ]} onPress={onPressFunc}>
            <Text style={[globalStyles.Small_Note, !isEnable ? {color: colors.Grey3} : {}]}>{label}</Text>
        </TouchableOpacityComponent>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        paddingVertical: 3,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 20,
        borderColor: colors.Grey4,
    }
})

export default ValueOrVolumeButton