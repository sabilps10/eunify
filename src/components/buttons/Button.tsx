import React from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import tw from '../../utils/tailwind';

interface ButtonProps {
  label: string;
  onPress: () => void;
  disabled?: boolean;
  type?: 'filled' | 'outlined';
  icon?: boolean;
  size?: 'small' | 'middle';
}

const Button: React.FC<ButtonProps> = ({
  label,
  onPress,
  disabled = false,
  type = 'filled',
  icon = false,
  size = 'middle',
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      activeOpacity={0.8}
      style={[
        {...styles[type], ...styles[size]},
        disabled ? styles[`${type}_disabled`] : styles[`${type}_active`],
      ]}
      onPress={onPress}>
      <View style={tw`flex flex-row items-center justify-center`}>
        <Text style={[tw`${icon ? 'pl-[8px]' : ''}`, styles[`${type}_label`]]}>
          {label}
        </Text>
        {icon && (
          <Icon
            // eslint-disable-next-line react-native/no-inline-styles
            style={{fontSize: 24}}
            color="#CFA872"
            name="chevron-right"
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  filled: {
    borderRadius: 25,
    alignItems: 'center',
  },
  filled_label: {
    fontWeight: '600',
    fontSize: 16,
    color: '#444444',
    alignItems: 'center',
  },
  filled_active: {
    backgroundColor: '#FED661',
  },
  filled_disabled: {
    backgroundColor: '#9D9D9D',
  },
  // outlined style
  outlined: {
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  outlined_active: {
    borderColor: '#CFA872',
  },
  outlined_disabled: {
    borderColor: '#9D9D9D',
  },
  outlined_label: {
    fontWeight: '600',
    fontSize: 16,
    color: '#CFA872',
    alignItems: 'center',
  },
  middle: {
    paddingHorizontal: 20,
    paddingVertical: 12,
  },
  small: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
});

export default Button;
