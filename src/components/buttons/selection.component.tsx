import React from 'react';
import { Image, StyleSheet, Text, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface MediumButtonInterface {
    title?: string,
    onPress: () => void,
    isSelected: boolean
}

const SelectionButton = ({ 
    title = '', 
    onPress, 
    isSelected = false
}: MediumButtonInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressFunc = () => onPress()

    return (
        <TouchableOpacityComponent
            onPress={onPressFunc}
            style={[
                styles.container
            ]}>
            <View style={{ width: 8 }} />
            <Text style={[globalStyles.Big_Text, styles.textStyle, {color: isSelected ? colors.White : colors.Grey3, backgroundColor: isSelected ? colors.Brand2 : colors.White, borderColor: isSelected ? colors.Brand2 : colors.Text}]}>{title}</Text>
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    textStyle: {
        borderWidth: 0.5,
        borderRadius: 5,
        padding: 5,
        marginRight: 10,
        marginBottom: 10
    }
})

export default SelectionButton