import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonInterface {
    title: string,
    onPress: () => void,
    isEnable?: boolean
}

const ActionButton = ({ title, onPress, isEnable = true }: ActionButtonInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const func = () => onPress();

    return (
        <View style={styles.container}>
            <PressableComponent 
                disabled={!isEnable} 
                onPress={func} 
                style={[styles.button, { backgroundColor: isEnable ? colors.EnableBtn : colors.DisableBtn }]}>
                <Text style={[globalStyles.H4, { color: isEnable ? colors.EnableBtnFont : colors.DisableBtnFont }]}>
                    {title}
                </Text>
            </PressableComponent>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingVertical: 10,
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 12,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default ActionButton