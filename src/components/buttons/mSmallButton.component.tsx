import React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import ArrowRight from '../../../assets/svg/ArrowRight';
import CircleArrow20px from '../../../assets/svg/CircleArrow20px';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface MediumButtonInterface {
    title?: string,
    onPress: () => void,
    component?: React.ReactNode,
    type?: 'Solid' | 'Outline' | 'White',
    style?: ViewStyle,
    textStyle?: StyleProp<TextStyle>,
}

const MSmallButton = ({ 
    title = '', 
    onPress, 
    component = null, 
    type = 'Solid', 
    style = {},
    textStyle = {}
}: MediumButtonInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressFunc = () => onPress()

    return (
        <TouchableOpacityComponent
            onPress={onPressFunc}
            style={[
                styles.container,
                style
            ]}>
            {
                component ?
                    component :
                    // <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    //     <Text style={[
                    //         globalStyles.Big_Text_B,
                    //         type === 'Solid' && { color: colors.Text },
                    //         type === 'Outline' && { color: colors.LText },
                    //         type === 'White' && { color: colors.White },
                    //         textStyle
                    //     ]}>{title}</Text>
                    // </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={[
                            globalStyles.Big_Text_B,
                            type === 'Solid' && { color: colors.Text },
                            type === 'Outline' && { color: colors.LText },
                            type === 'White' && { color: colors.White },
                            textStyle
                        ]}>{title}</Text>
                    </View>
            }
            <View style={{ width: 4 }} />
            <View style={styles.arrow}>
            {
                type === 'Solid' ?
                    <CircleArrow20px color={colors.Brand3} backgroundColor={colors.White} /> :
                    type === 'Outline' ?
                        <CircleArrow20px color={colors.White} backgroundColor={colors.Brand3} /> :
                        <CircleArrow20px color={colors.Brand3} backgroundColor={colors.White} />
            }
            </View>
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
})

export default MSmallButton