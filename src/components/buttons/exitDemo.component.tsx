import React from 'react';
import { StyleSheet, Text, TouchableOpacity, useColorScheme, View, Image } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import IconExit from '../../../assets/icon/IconExit';

const ExitDemoButton = () => {
    const { colors } = useTheme();
    const scheme = useColorScheme();
    const [t, i18n] = useTranslation();
    const globalStyles = makeGlobalStyles(colors);

    return (
        <View style={{flexDirection: 'row'}}>
            <IconExit color={colors.Text}/>
        </View>
    )
}

const styles = StyleSheet.create({

})

export default ExitDemoButton