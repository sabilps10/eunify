import React, { useEffect, useState } from 'react';
import { Pressable, StyleSheet, Text, View, ViewStyle, TextStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import { useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonInterface {
    title: string,
    contract: string,
    isBuy: boolean,
    onPress?: () => void,
    isEnable?: boolean,
    viewStyle?: ViewStyle,
    textStyle?: TextStyle,
    isSelected: boolean
}

const BuySellTradeButton = ({ title, contract, isBuy, onPress, isEnable = true, viewStyle = {}, textStyle = {}, isSelected = false }: ActionButtonInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const func = () => onPress();

    const contractPrice = useSelector((state: State) => state.trading.prices[contract]);
    const contractSymbol = useSelector((state: State) => state.trading.symbols[contract]);

    const [displayPrice, setDisplayPrice] = useState<string>('')
    const [previousPrice, setPreviousPrice] = useState(0)
    const [priceColor, setPriceColor] = useState(colors.MainFont)

    var backgroundColor = isSelected ? colors.MainBg : colors.Grey2;
    var textColor = isSelected ? colors.MainFont : colors.Grey3;
    var buttonPriceColor = isSelected ? priceColor : colors.Grey3;

    useEffect(()=> {
        if (contractPrice && contractSymbol) {
            var price = isBuy ? contractPrice.Ask : contractPrice.Bid
            setDisplayPrice(price.toFixed(contractSymbol.Digits))
            if (Number(price) > previousPrice) {
                setPriceColor(colors.Up)
            }
            else if (Number(price) < previousPrice) {
                setPriceColor(colors.Down)
            }
            setPreviousPrice(Number(price));

            const priceTimeout = setTimeout(function () { setPriceColor(colors.Text) }, 500);

            return function cleanup() {
                clearTimeout(priceTimeout);
            }
        }
    }, [contractPrice, contractSymbol])

    return (
        <View style={styles.container}>
            <PressableComponent onPress={onPress} style={[styles.button, { backgroundColor: backgroundColor }, viewStyle]}>
                <Text style={[globalStyles.Big_Text, textStyle, { color: textColor }]}>{title}</Text>
                <Text style={[globalStyles.H3, { color: buttonPriceColor }]}>{displayPrice}</Text>
            </PressableComponent>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 4,
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default React.memo(BuySellTradeButton)