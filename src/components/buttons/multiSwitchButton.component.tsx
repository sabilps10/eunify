import React, { useEffect, useState } from 'react';
import { LayoutChangeEvent, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';


interface MultiSwitchButtonInterface {
    buttons: {
        title: string,
        function: Function
    }[],
    selectedIndex?: number,
    enable?: boolean
}

const MultiSwitchButton = ({ buttons, selectedIndex = 0, enable = true }: MultiSwitchButtonInterface) => {

    // console.debug("MultiSwitchButton: " + selectedIndex)
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const [containerWidth, setcontainerWidth] = useState<number>(0);
    const [selectedButtonIndex, setSelectedButtonIndex] = useState<number>(selectedIndex);

    const onLayout = (event: LayoutChangeEvent) => {
        setcontainerWidth(event.nativeEvent.layout.width);
        setSelectedButtonIndex(selectedIndex)
    }

    return (
        <View onLayout={onLayout} style={[styles.container, { backgroundColor: enable ? colors.MainBg : colors.Grey2, borderColor: colors.Brand3 }]}>
            {
                buttons.map((item, index) => {
                    const onPress = () => {
                        setSelectedButtonIndex(index);
                        item.function();
                    }
                    return (
                        <TouchableOpacityComponent disabled={!enable} style={styles.button} onPress={onPress} key={item.title}>
                            <Text style={[globalStyles.Body_Text_B, { color: selectedButtonIndex === index ? colors.MainFont : (enable ? colors.MainFont : colors.Grey3)}]}>
                                {item.title}
                            </Text>
                        </TouchableOpacityComponent>
                    )
                })
            }
            <View
                style={[styles.switcher, { width: containerWidth / buttons.length, backgroundColor: colors.Brand3, transform: [{ translateX: containerWidth * selectedIndex / buttons.length }]}]}>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // display: 'none',
        width: '100%',
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 27.5
    },
    button: {
        flex: 1,
        height: 39,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1,
    },
    switcher: {
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        left: 0,
        borderRadius: 28,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        shadowOpacity: 0.31,
        shadowRadius: 10,
        shadowColor: '#A69E9E'
    }
})

export default MultiSwitchButton;