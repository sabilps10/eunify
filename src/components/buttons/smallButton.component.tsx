import React from 'react';
import { StyleSheet, Text, TextStyle, TouchableOpacity, useColorScheme, View, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import ArrowRight from '../../../assets/svg/ArrowRight';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface MediumButtonInterface {
    title?: string,
    onPress: () => void,
    component?: React.ReactNode,
    type?: 'Solid' | 'Outline' | 'White',
    style?: ViewStyle,
    textStyle?: TextStyle
}

const SmallButton = ({ 
    title = '', 
    onPress, 
    component = null, 
    type = 'Solid', 
    style = {},
    textStyle = {}
}: MediumButtonInterface) => {
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressFunc = () => onPress()

    return (
        <TouchableOpacityComponent
            onPress={onPressFunc}
            style={[
                styles.container,
                style
            ]}>
            {
                component ?
                    component :
                    <Text style={[
                        globalStyles.Note,
                        type === 'Solid' && { color: colors.Text },
                        type === 'Outline' && { color: colors.LText },
                        type === 'White' && { color: colors.White },
                        textStyle
                    ]}>{title}</Text>
            }
            <View style={{ width: 8 }} />
            {
                type === 'Solid' ?
                    <ArrowRight color={colors.Brand3} /> :
                    type === 'Outline' ?
                        <ArrowRight color={colors.Brand3} /> :
                        <ArrowRight color={colors.White} />
            }
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default SmallButton