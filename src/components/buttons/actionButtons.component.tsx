import React from 'react';
import { Pressable, StyleProp, StyleSheet, Text, TextStyle, View } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import PressableComponent from '../../views/utils/PressableComponent';

interface ActionButtonsInterface {
    btn1Title: string,
    btn1Func: Function,
    btn1Style?: StyleProp<TextStyle>,
    btn2Title: string,
    btn2Func: Function,
    btn2Style?: StyleProp<TextStyle>,
}

const ActionButtons = ({ btn1Title, btn1Func, btn1Style, btn2Title, btn2Func, btn2Style }: ActionButtonsInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressBtn1Func = () => btn1Func();
    const onPressBtn2Func = () => btn2Func();

    return (
        <View style={styles.actionBtnsWrapper}>
            <PressableComponent onPress={onPressBtn1Func} style={[styles.btn1, { borderColor: colors.Brand2 }]}>
                <Text style={[globalStyles.H4, { color: colors.Brand2 }, btn1Style]}>{btn1Title}</Text>
            </PressableComponent>
            <PressableComponent onPress={onPressBtn2Func} style={[styles.btn2, { backgroundColor: colors.Brand1 }]}>
                <Text style={[globalStyles.H4, { color: colors.MainFont }, btn2Style]}>{btn2Title}</Text>
            </PressableComponent>
        </View>
    )
}

const styles = StyleSheet.create({
    actionBtnsWrapper: {
        paddingHorizontal: 16,
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn1: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        borderWidth: 1,
        paddingHorizontal: 20,
        paddingVertical: 12,
        marginBottom: 15
    },
    btn2: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        paddingHorizontal: 20,
        paddingVertical: 12
    }
});

export default ActionButtons