import React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle, TouchableOpacity, ViewStyle } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { makeGlobalStyles } from '../../styles/styles';
import TouchableOpacityComponent from '../../views/utils/TouchableOpacityComponent';

interface BigButtonInterface {
    title?: string,
    onPress: () => void,
    component?: React.ReactNode,
    type?: 'Solid' | 'Outline' | 'White',
    style?: ViewStyle,
    textStyle?: StyleProp<TextStyle>
}

const BigButton = ({ 
    title = '', 
    onPress, 
    component = null, 
    type = 'Solid', 
    style = {},
    textStyle = {}
}: BigButtonInterface) => {
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const onPressFunc = () => onPress()

    return (
        <TouchableOpacityComponent
            onPress={onPressFunc}
            style={[
                styles.container,
                type === 'Solid' && { backgroundColor: colors.Brand1 },
                type === 'Outline' && { borderColor: colors.Brand2, borderWidth: 1 },
                type === 'White' && { borderColor: colors.White, borderWidth: 1 },
                style
            ]}>
            {
                component ?
                    component :
                    <Text style={[
                        globalStyles.H4,
                        type === 'Solid' && { color: colors.Text },
                        type === 'Outline' && { color: colors.Brand2 },
                        type === 'White' && { color: colors.White },
                        textStyle
                    ]}>{title}</Text>
            }
        </TouchableOpacityComponent>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default BigButton