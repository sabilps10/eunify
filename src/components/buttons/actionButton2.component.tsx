import React from 'react';
import { ColorValue, Pressable, StyleProp, StyleSheet, Text, TextStyle, useColorScheme, View, ViewProps, ViewStyle } from 'react-native';
import { makeGlobalStyles } from '../../styles/styles';
import { useTheme } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import ArrowRight from '../../../assets/svg/ArrowRight';
import { THEME_TYPE } from '../../types/types';
import PressableComponent from '../../views/utils/PressableComponent';

interface OrderOptionButton {
    label: string,
    onPress: () => void,
    style?: ViewStyle,
    labelStyle?: StyleProp<TextStyle>
    iconColor?: ColorValue,
}

const ActionButton2 = ({ label, onPress, style = {}, labelStyle, iconColor }: OrderOptionButton) => {
    const [t, i18n] = useTranslation();
    
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);
    const styles = makeStyles(colors)
    const scheme = useColorScheme();

    const onPressFunc = () => onPress()

    return (
        <PressableComponent onPress={onPressFunc} style={[styles.button, style]}>
            <Text style={[globalStyles.H4, { color: colors.MainFont }, labelStyle]}>{label}</Text>
            <View style={{height: 24, width: 24, justifyContent: 'center', alignItems: 'center'}}>
                <ArrowRight color={iconColor ? iconColor : colors.MainFont} />
            </View>
        </PressableComponent>
    )
}

const makeStyles = (colors: THEME_TYPE) => StyleSheet.create({
    button: {
        alignSelf: 'center', 
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row', 
        paddingHorizontal: 15, 
        paddingVertical: 5, 
        borderRadius: 25, 
        borderWidth: 1, 
        backgroundColor: colors.Brand1, 
        borderColor: colors.White, 
    },
})

export default ActionButton2