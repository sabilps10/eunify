import { useTheme } from '@react-navigation/native'
import React, { ReactNode, useState } from 'react'
import { GestureResponderEvent, Pressable, StyleSheet, Text, useColorScheme, View } from 'react-native'
import ArrowDown from '../../assets/svg/ArrowDown'
import ArrowRight from '../../assets/svg/ArrowRight'
import { makeGlobalStyles } from '../styles/styles'
import PressableComponent from '../views/utils/PressableComponent'
import StackView from './stackView.component'

interface ExpandableViewInterface {
    title: string,
    expanded?: boolean,
    expandComponent: ReactNode,
    detailImage?: ReactNode,
    onPressDetail?: ((event: GestureResponderEvent) => void),
}

const ExpandableView = (params: ExpandableViewInterface) => {

    const scheme = useColorScheme()
    const { colors } = useTheme()
    const globalStyles = makeGlobalStyles(colors)

    const [isExpanded, setIsExpended] = useState<boolean>(params.expanded ?? true)

    const onPress = () => {
        console.debug('onPress')
        setIsExpended(!isExpanded)
    }

    return (
        <PressableComponent onPress={() => onPress()}>
            <View style={[styles.separator, {backgroundColor: colors.Grey3}]} />
            <View style={[styles.header]} >
                <StackView
                    spacing={11} 
                    style={{alignContent: 'center', justifyContent: 'center'}}>
                    <Text style={[globalStyles.Big_Text_B, styles.headerTitle]}>{params.title}</Text>
                    {
                        params.detailImage ? 
                            <PressableComponent
                                onPress={params.onPressDetail} >
                                {params.detailImage}
                            </PressableComponent>
                        :
                            null 
                    }
                </StackView>
                <View style={styles.arrow}>
                    { isExpanded ? <ArrowDown color={colors.Brand3} /> : <ArrowRight color={colors.Brand3} /> }
                </View>
            </View>
            {
                isExpanded ? params.expandComponent : null
            }
        </PressableComponent>
    )
}

const styles = StyleSheet.create({
    separator: {
        height: 1,
    },
    header: {
        height: 45,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    arrow: {
        height: 24, 
        width: 24, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
});

export default ExpandableView