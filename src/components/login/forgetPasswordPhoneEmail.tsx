import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Pressable, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../forms/inputs/input.component';
import ActionButton from '../buttons/actionButton.component';
import InputDropDownList from '../forms/inputs/inputDropdownList.component';
import TextLabel from '../forms/labels/textLabel.component';
import LoadingDialog from '../dialogs/loadingDialogs';

import { Props } from '../../routes/route';
import { Auth } from 'aws-amplify';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../redux/root-reducer';
import { bindActionCreators } from 'redux';
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { getTranslationDict, isEmail } from '../../views/utils/CommonFunction';
import ErrorMessage from '../alerts/errorMessage.component';
import { isEmpty, maskEmail, maskPhone } from '../../utils/stringUtils';
import { InputType } from '../../types/InputTypes';
import { CountryOptionType } from '../countryOptionView/countryOptionView';
import PressableComponent from '../../views/utils/PressableComponent';

const ForgetPasswordPhoneEmail: React.FC<Props<'ForgetPasswordPhoneEmail'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();

    const regionDataList = useSelector((state: State) => state.state.regionDataList);

    const [inputDropDownList, setInputDropDownList] = useState<{ label: string, value: string }[]>([])
    // const selectionOptions = ["852", "853"];

    const {type} = route.params;
    const { colors } = useTheme();
    // const [username, SetUserName] = useState<string>('');
    const [username, setUserName] = useState<InputType>({ value: "", error: "", mandatory: true, maxLength: 200, isValid: false});

    const [phonePrefix, SetPhonePrefix] = useState<string>(regionDataList[0].countryCode);
    const [phoneNumber, SetPhoneNumber] = useState<string>('');

    const [errorPhone, SetErrorPhone] = useState<string | undefined>(undefined);
    const [errorUsername, SetErrorUsername] = useState<string | undefined>(undefined);

    const dispatch = useDispatch();
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetIsShowLoading } = bindActionCreators(StateActionCreators, dispatch);

    // const selectedPhonePrefix = useSelector((state: State) => state.selection.selected?.forgotPasswordPhonePrefix);
    

    // useEffect(() => {
    //     if (selectedPhonePrefix)
    //         SetPhonePrefix(selectedPhonePrefix);
    // }, [selectedPhonePrefix])

    const userExist = async(user: string) => {
        return await Auth.signIn(user, '123').then(res => {
          return false;
        }).catch(error => {
          const code = error.code;
          console.debug('userExist ', user, error);
          switch (code) {
                case 'UserNotFoundException':
                    return false;
                case 'NotAuthorizedException':
                    return true;
                case 'PasswordResetRequiredException':
                    return false;
                case 'UserNotConfirmedException':
                    return false;
                default:
                    return false;
            }
        });
    }

    const userResetRequired = async (user: string) => {
        return await Auth.signIn(user, '123').then(res => {
            return false;
        }).catch(error => {
            const code = error.code;
            console.debug('userExist ', user, error);
            switch (code) {
                case 'PasswordResetRequiredException':
                    return true;
                default:
                    return false;
            }
        });
    }

    const sendForgotPassword = async () => {
        SetErrorUsername(undefined)
        if (type === 'email' && await userResetRequired(username.value)) {
            navigation.navigate('ForceChangePassword', { username: username.value })
            return;
        }

        if (type === 'email' && isEmpty(username.value)) {
            setUserName({...username, error: t("error.mandatory"), isValid: false})
            // SetErrorUsername(t('cognitoSignUp.errorMandatory'))
            return;
        }
        else if (type === 'email' && !isEmail(username.value)) {
            setUserName({...username, error: t("error.invalidEmailAddress"), isValid: false})
            // SetErrorUsername(t('cognitoSignUp.errorMandatory'))
            return;
        }
        else if (type === 'email' && isEmail(username.value) && !(await userExist(username.value))) {
            setUserName({...username, error: t("error.invalidEmailAddress"), isValid: false})
            // SetErrorUsername(t('cognitoSignUp.errorMandatory'))
            return;
        }

        if (type !== 'email' && isEmpty(phoneNumber)) {
            SetErrorPhone(t('cognitoSignUp.errorMandatory'))
            return;
        }

        SetIsShowLoading(true)
        var loginUsername = (type === 'email' ? username.value : '+' + phonePrefix + phoneNumber).trim();
        // var username = '+' + phonePrefix + phoneNumber;
        try
        {
            // navigation.navigate('CognitoForgetPasswordVerification', {username: loginUsername, displayUsername: type === 'email' ? maskEmail(username.value) : maskPhone(phonePrefix, phoneNumber)})
            navigation.navigate('CognitoForgetPasswordReset', {isChangePassword: false, username: loginUsername, displayUsername: type === 'email' ? maskEmail(username.value) : maskPhone(phonePrefix, phoneNumber)})
        }
        catch(error) {
            console.error(error);
            if (error instanceof Error) {
                if (error.name === 'UserNotFoundException') {
                    SetErrorUsername(type === 'email' ? t('cognitoLogin.error.invalidEmail') : t('cognitoLogin.error.invalidMobile'))
                } 
            } 
        };
        SetIsShowLoading(false)
    };

    const onSelectCountryCodeOption = (value: string) => {
        console.debug('onSelectCountryCodeOption', value)
        SetPhonePrefix(value);
      }

    const onPressShowAll = () => {
        // OpenCloseSelection({
        //     selectedOption: phonePrefix,
        //     selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
        //     isOpen: true,
        //     location: 'forgotPasswordPhonePrefix'
        // })
        navigation.navigate('CountryOptionView', {type: CountryOptionType.COUNTRY, onSelect: onSelectCountryCodeOption})
    }

    return (
        <ScrollView style={[styles.container, { backgroundColor: colors.MainBg }]}
        keyboardShouldPersistTaps='handled'>
            <View style={styles.contentWrapper}>
                {
                    type === 'email' ?
                        // <Input
                        //     label={t('cognitoSignUp.emailAddress')}
                        //     placeholder=''
                        //     value={username}
                        //     onChangeText={(value) => setUserName({...username, value})}
                        //     onEndEditing={(e) => {checkEmail()} }
                        // />
                        <Input
                            inputStyle={{color: colors.MainFont}}
                            label={t('cognitoSignUp.emailAddress')}
                            type='email'
                            value={username}
                            onChangeText={(value) => setUserName({...username, value, error: ''})}
                            // onEndEditing={(e) => {checkEmail()} }
                        />
                    :
                        <View>
                            <TextLabel labelStyle={{color: colors.MainFont}} label={t('cognitoSignUp.mobileNumber')}/>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <PressableComponent onPress={onPressShowAll}>
                                <InputDropDownList value={phonePrefix} />
                                </PressableComponent>
                                <View style={{ width: 10 }} />
                                <Input
                                style={{ flex: 1 }}
                                value={phoneNumber}
                                onChangeText={(value) => {SetPhoneNumber(value)}}
                                maxLength={20}
                                    type='phone' 
                                />
                            </View>
                            {errorUsername && <ErrorMessage message={errorUsername}/>}
                        </View>
                }
                
                <View style={{ padding: 30 }} />
                <ActionButton
                    title={t('common.submit')}
                    onPress={sendForgotPassword}
                    // isEnable={username.isValid || !isEmpty(username.value)}
                />  
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentWrapper: {
        flex: 1,
        marginTop: 36,
        paddingBottom: 76,
        marginHorizontal: 16,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    }
});

export default ForgetPasswordPhoneEmail