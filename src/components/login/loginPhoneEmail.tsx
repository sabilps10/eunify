import React, { useState, useContext, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, Pressable, Alert, KeyboardAvoidingView, Platform } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useTranslation } from 'react-i18next';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';

import Input from '../forms/inputs/input.component';
import ActionButton from '../buttons/actionButton.component';

import { Props } from '../../routes/route';
import { Auth, Hub } from 'aws-amplify';

import PasswordEyeIcon from '../forms/labels/passwordEyeIcon.component';
import TextLabel from '../forms/labels/textLabel.component';
import MSmallButton from '../buttons/mSmallButton.component';
import InputDropDownList from '../../components/forms/inputs/inputDropdownList.component';

import { AccountActionCreators } from '../../../src/redux/account/actionCreator';
import { bindActionCreators } from 'redux';
import { OpenPositionsActionCreators } from '../../redux/trading/actionCreator';
import { useSelector, useDispatch } from 'react-redux';

import { CognitoUserSession } from 'amazon-cognito-identity-js';
import { State } from '../../redux/root-reducer';

import commonStyles from '../../styles/styles'
import { SelectionActionCreators } from '../../redux/selection/actionCreator';
import { TradingAccount } from '../../redux/account/type';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { getTranslationDict } from '../../views/utils/CommonFunction';
import { AccountUtils } from '../../utils/accountUtils';
import ErrorMessage from '../alerts/errorMessage.component';
import { isEmpty, maskPhone } from '../../utils/stringUtils';
import { CognitoActionCreators } from '../../redux/cognito/actionCreator';
import PressableComponent from '../../views/utils/PressableComponent';
import { setChartURL, setTradingURL } from '../../utils/api/apiSetter';
import { CognitoState } from '../../redux/cognito/types';
import { SelectionContextLocation } from '../../redux/selection/type';


const LoginPhoneEmail: React.FC<Props<'loginPhoneEmail'>> = ({ route, navigation }) => {
    const [t, i18n] = useTranslation();
    const setting = useSelector((state: State) => state.state.setting);
    const isOpen = useSelector((state: State) => state.selection.selection.isOpen);

    const selectionOptions = ["852", "853"];

    const { type } = route.params;
    const { colors } = useTheme();
    const [username, SetUserName] = useState<string>('');
    const [password, SetPassword] = useState<string>('');
    const [isEnablePassword, SetIsEnablePassword] = useState<boolean>(false);

    const [phonePrefix, SetPhonePrefix] = useState<string>(selectionOptions[0]);
    const [phoneNumber, SetPhoneNumber] = useState<string>('');
    
    const [usernameError, setUsernameError] = useState<{isShow: boolean, message: string}>({isShow: false, message: ''});
    const [passwordError, setPasswordError] = useState<{isShow: boolean, message: string}>({isShow: false, message: ''});

    const dispatch = useDispatch();
    const { SetIsShowLoading, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);
    const { OpenCloseSelection } = bindActionCreators(SelectionActionCreators, dispatch);
    const { SetLoginCredentials } = bindActionCreators(CognitoActionCreators, dispatch);
    const { SetAccountDisplayName } = bindActionCreators(OpenPositionsActionCreators, dispatch)

    const selectedPhonePrefix = useSelector((state: State) => state.selection.selected?.loginPhonePrefix);

    useEffect(() => {
        Hub.listen('auth', ({ payload }) => {
            const { event } = payload;
            if (event === 'signIn_failure') {
                const data = payload.data;
                console.debug('signIn_failure', data)
            }
        })
    }, []);

    useEffect(() => {
        if (selectedPhonePrefix)
            SetPhonePrefix(selectedPhonePrefix);
    }, [selectedPhonePrefix])

    const { doLogin, doConnectWebsocket, doCloseWebsocket } = AccountUtils();

    const login = async () => {
        setPasswordError({isShow: false, message: ''})
        setUsernameError({isShow: false, message: ''})
        SetIsShowLoading(true);
        if (type === 'email' && isEmpty(username) || type !== 'email' && isEmpty(phoneNumber)) {
            setUsernameError({isShow: true, message: t('cognitoLogin.error.mandatory')})
        } else if (isEmpty(password)) {
            setPasswordError({isShow: true, message: t('cognitoLogin.error.mandatory')})
        } else {
            try {
                var loginUsername = (type === 'email' ? username : '+' + phonePrefix + phoneNumber).trim();
                // console.debug('signIn ', loginUsername, password)
                const user = await Auth.signIn(
                    loginUsername,
                    password
                );
                // console.debug('signIn ', JSON.stringify(user))
                const session = await Auth.currentSession();
                if (session !== null) {
                    console.debug('Has Cognito session');

                    SetLoginCredentials({
                        username: loginUsername,
                        password: password
                    } as  CognitoState)

                    var pushyDeviceID = setting?.PushyDeviceID ?? "";
                    var region: string = setting?.UserRegion;
                    // var data = await loginLevel(pushyDeviceID, region, false)
                    var data = await doLogin(pushyDeviceID, region, false)
                    if (data && data.success) {
                        var page = data.redirect
                        console.debug("doLogin: " + page)
                        switch (page) {
                            case 'AddDevices':
                                navigation.navigate('AddDevices', {displayPhoneNumber: maskPhone(phonePrefix, phoneNumber)})
                                break;
                            case 'EnableBiometrics':
                            case 'TabDirectory':
                                await doCloseWebsocket(true);
                                const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                                if (status === 101)
                                {
                                    SetAccountDisplayName(data.payload.currentTrader);
                                    setTradingURL(data.payload.tApiPath);
                                    setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                                }
                                navigation.navigate(page)
                                break;
                        }
                    }
                    else {
                        SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: ''})
                    }
                }
            } catch (error) {
                console.debug('error signing in', error);
                if (error instanceof Error) {
                    if (error.name === 'NotAuthorizedException') {
                        setPasswordError({isShow: true, message: t('cognitoLogin.error.incorrectPassword')})
                    } else if (error.name === 'UserNotFoundException') {
                        setUsernameError({isShow: true, message: type === 'email' ? t('cognitoLogin.error.invalidEmail') : t('cognitoLogin.error.invalidMobile')})
                    } 
                } 
            }
        }
        SetIsShowLoading(false);
    }

    const toForgotPassword = () => {
        navigation.navigate('CognitoForgetPassword')
    }

    const toSignUp = () => {
        navigation.navigate('CognitoSignUp')
    }

    const callbackEnablePasswordVisibility = (passwordVerfied: boolean) => {
        SetIsEnablePassword(!isEnablePassword)
    };

    const onPressShowAll = () => {
        console.debug("onPressAll");
        OpenCloseSelection({
            selectedOption: phonePrefix,
            selectOptions: getTranslationDict(selectionOptions, "phonePrefix", t),
            isOpen: !isOpen,
            location: SelectionContextLocation.LoginPhonePrefix
        })
    }

    return (
        <View style={[commonStyles.container, { backgroundColor: colors.Brand3 }]}>
            <KeyboardAvoidingView
                style={styles.contentWrapper}
                behavior={Platform.select({ android: undefined, ios: 'padding' })}
                keyboardVerticalOffset={Platform.select({ ios: 0, android: 500 })}>
                <ScrollView
                    contentContainerStyle={commonStyles.ScrollViewContainerStyle}
                    showsVerticalScrollIndicator={false}>
                    {
                        type === 'email' ?
                            <Input
                                label={t('cognitoSignUp.emailAddress')}
                                labelStyle={{ color: colors.White }}
                                placeholder=''
                                value={username}
                                onChangeText={(value) => SetUserName(value)}
                                showAlert={usernameError.isShow}
                                alertComponent={<ErrorMessage message={usernameError.message}/>}
                            />
                            :
                            <View>
                                <TextLabel label={t('cognitoSignUp.mobileNumber')} labelStyle={{ color: colors.White }} />
                                <View style={{ flexDirection: 'row' }}>
                                    <PressableComponent onPress={onPressShowAll}>
                                        <InputDropDownList
                                            value={phonePrefix}
                                        />
                                    </PressableComponent>
                                    <View style={{ width: 10 }} />
                                    <Input
                                        style={{ flex: 1 }}
                                        placeholder=''
                                        value={phoneNumber}
                                        type='phone'
                                        onChangeText={(value) => { SetPhoneNumber(value) }}
                                        showAlert={usernameError.isShow}
                                        alertComponent={<ErrorMessage message={usernameError.message}/>}
                                    />
                                </View>
                            </View>
                    }
                    <View style={[commonStyles.container]}>
                        <View style={{ padding: 20 }} />
                        <Input
                            placeholder=''
                            label={t('cognitoSignUp.password')}
                            labelStyle={{ color: colors.White }}
                            value={password}
                            type={isEnablePassword ? 'text' : 'password'}
                            onChangeText={(value) => SetPassword(value)}
                            rightComponent={<PasswordEyeIcon isEnable={isEnablePassword} callback={callbackEnablePasswordVisibility} />}
                            showAlert={passwordError.isShow}
                            alertComponent={<ErrorMessage message={passwordError.message}/>}
                        />
                        <View style={{ padding: 5 }} />
                        <PressableComponent onPress={toForgotPassword}>
                            <TextLabel label={t('cognitoLogin.toForgotPassword')} labelStyle={{ color: colors.White, textAlign: 'right' }} />
                        </PressableComponent>
                        <View style={[commonStyles.bottomContainer]}>
                            <MSmallButton title={t('cognitoLogin.notMemberToSignUp')} type='White' onPress={toSignUp} />
                            <ActionButton
                                title={t('cognitoLogin.login')}
                                onPress={login}
                            />
                        </View>

                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </View>
    )
}

const styles = StyleSheet.create({

    contentWrapper: {
        flex: 1,
        marginTop: 36,
        marginHorizontal: 16,
    },
    contentItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    dottedLine: {
        flex: 0.8,
        borderStyle: 'dotted',
        borderWidth: 1,
        borderRadius: 1
    }
});

export default LoginPhoneEmail