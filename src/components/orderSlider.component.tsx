import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, View, ViewStyle, useColorScheme, Text, TextStyle } from 'react-native';
import { Slider } from '@miblanchard/react-native-slider';
import { makeGlobalStyles } from '../styles/styles';
import { useTheme } from '@react-navigation/native';
import Arithmometer from './arithmometer.component';
import ErrorMessage from '../components/alerts/errorMessage.component';
import { t } from 'i18next';

interface OrderSliderInterface {
    style?: ViewStyle,
    sliderLabelStyle?: TextStyle,
    valueStyle?: TextStyle,
    titleStyle?: TextStyle,
    maxValue?: number,
    minValue?: number,
    stepValue?: number,
    contractSize?: number,
    currentLot: number,
    onChangeValue: (value: number) => void,
    isShowError?: boolean,
    errorMessage?: string,
    enableSlider?: boolean,
    optionButtonsType?: string,
    currentLotUpdate?: boolean,
    dp?: number
}

const OrderSlider = ({ style = {}, sliderLabelStyle = {}, valueStyle = {}, titleStyle = {}, 
    maxValue = 100, minValue = 0, stepValue = 1, contractSize = 10, currentLot, onChangeValue,
    isShowError = false, errorMessage = '', enableSlider = true, optionButtonsType = 'new', currentLotUpdate = false, dp = 1 }: OrderSliderInterface) => {

    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const [valueUpdate, setValueUpdate] = useState<boolean>(currentLotUpdate)

    const optionButtons = [
        {
            label: '0.01 ' + t('trade.lot'),
            value: 0.01
        },
        {
            label: '0.1 ' + t('trade.lot'),
            value: 0.1
        },
        {
            label: '0.5 ' + t('trade.lot'),
            value: 0.5
        },
        {
            label: '1 ' + t('trade.lot'),
            value: 1
        },
        {
            label: '5 ' + t('trade.lot'),
            value: 5
        },
    ]

    const optionButtonsAll = [
        {
            label: t('trade.all'),
            value: maxValue
        }
    ]

    useEffect(()=>{
        setValueUpdate(currentLotUpdate)
    }, [currentLotUpdate])

    return (
        <View style={style}>
            <View style={styles.sliderContainer}>
                {
                    enableSlider ? 
                        <View>
                            <View style={styles.row}>
                            {
                                isShowError ? <ErrorMessage message={errorMessage} />: null
                            }
                            </View>
                        </View>
                    : 
                        null
                }
                
            </View>
            <Arithmometer
                value={currentLot}
                valueUpdate={valueUpdate}
                valuePerStep={stepValue}
                onChangeValue={onChangeValue}
                style={{ paddingHorizontal: 16 }}
                title={t('trade.lot')}
                buttons={ optionButtonsType === 'all' ? optionButtonsAll : optionButtons}
                valueStyle={valueStyle}
                titleStyle={titleStyle}
                minimunValue={minValue}
                maximunValue={maxValue}
                headerComponent={null}
                arithmometerStyle={{width: '100%'}}
                dp={dp}
                isShowError={isShowError}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    sliderContainer: {
        justifyContent: 'center'
    },
    sliderThumb: {
        position: 'absolute',
        top: -35,
        bottom: 0,
        left: -10,
        right: 0
    },
    row: {
        flexDirection: 'row',
        height: 35,
        alignSelf: 'center',
        paddingTop: 10
    },
    availableLabel: {
        position: 'absolute',
        right: 0
    }
})

export default OrderSlider