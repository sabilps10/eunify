import React from 'react';
import { Image, Pressable, StyleSheet, Text, useColorScheme, View, ViewStyle } from 'react-native';
import {
    getIcon_flagWithCode
} from '../../../assets/constants';
import { makeGlobalStyles } from '../../styles/styles'
import { useTheme } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { bindActionCreators } from 'redux';
import { StateActionCreators } from '../../redux/state/actionCreator';
import { useDispatch } from 'react-redux';
import Info from '../../../assets/svg/Info';
import { format } from 'react-string-format';
import { getApplicationName } from 'react-native-device-info';
import PressableComponent from '../../views/utils/PressableComponent';

interface RegionInterface {
    region: string,
    style?: ViewStyle
}

const Region = ({ region = '', style = {} }: RegionInterface) => {
    const [t, i18n] = useTranslation();
    const scheme = useColorScheme();
    const { colors } = useTheme();
    const globalStyles = makeGlobalStyles(colors);

    const dispatch = useDispatch();
    const { SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

    const onPressInfo = () => {
        SetIsShowMessageDialog({isShowDialog: true, title: t('welcome.countryTitle'), message: format(t('welcome.countryMessage'), getApplicationName()), isShowBtn: false, isShowCancel: true})
    }

    return (
        <View style={[styles.container, style]}>
        <PressableComponent style={{paddingEnd: 5}} onPress={onPressInfo}>
            <Info color={colors.MainFont} />
        </PressableComponent>
        <View style={{justifyContent: 'center'}}>
            
            <Text style={[globalStyles.Note]} >{t(`region.${region}`)}</Text>
        </View>
        <View style={{ width: 4 }} />
        <View style={{width: 28, height:18 }} >
            <Image style= {{flex:1 , width: undefined, height: undefined, resizeMode: 'contain'}} source={getIcon_flagWithCode(region)} />
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: 20,
        padding: 4,
        alignSelf: 'flex-start'
    }
})

export default Region