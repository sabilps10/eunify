import { CompositeScreenProps, NavigatorScreenParams, RouteProp } from '@react-navigation/core';
import { StackNavigationProp } from '@react-navigation/stack';
import { TextStyle, ViewStyle } from 'react-native';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { SortingParameters } from '../../ios/AwesomeTSProject/charting/charting_library/charting_library';
import { MIOType } from '../components/MIO';
import { TabDirectoryWebViewTab } from '../views/tabDirectory/tabDirectoryWebView';

export type RootStackParamList = {
  Login: undefined;
  CognitoSignUp: undefined;
  CognitoLogin: { forceLogin?: boolean, backPage?: string, backPageParam?: {}};
  loginPhoneEmail: { type: string };
  ForgetPasswordPhoneEmail: {type: string};
  LaunchAppPage: undefined;
  Welcome: undefined;
  Introduction: undefined;
  EnableBiometrics: { isSetting: boolean };
  CognitoForgetPassword: undefined;
  WatchlistView: undefined;
  ProductDetailView: undefined,
  EditWatchlistView: undefined,
  ShareView: undefined,  
  SearchProductView: {categories?: {title: string, id: string}[], filter?: string},
  TabDirectory: NavigatorScreenParams<BottomTabParamList>,
  TabDirectoryWebView: {page: string, focusTab: TabDirectoryWebViewTab, param?: any},
  CognitoSignUpVerification: {username: string, email: string, phoneNumber: string, phone: string, phonePrefix: string},
  CognitoForgetPasswordVerification: {username: string, displayUsername: string, password: string}
  CognitoForgetPasswordReset: {isChangePassword:boolean, username?: string, displayUsername?: string, emailCode?: string, skipVerification?: {isSkip: boolean, username: string, password: string}}
  MeSettingChangeCognitoPassword: {isChangePassword:boolean, username?: string, displayUsername?: string, emailCode?: string, skipVerification?: {isSkip: boolean, username: string, password: string}}
  ContractDetailView: { contractCode: string, defaultTab?: number, defaultPositionTab?: number, isEnterByOrder?: boolean };
  NewOrder: { symbolName: string, isBuy: boolean, transactionId?: number };
  'NewOrder+UpdatePendingOrder': { symbolName: string, isBuy: boolean, transactionId?: number };
  ClosePosition: { ticketId: number };
  EditPosition: { ticketId: number };
  HistoryQuery: { type: string, inFrom: Date, inTo: Date, inCurrency: string[], onQuery: Function }
  PositionDetails: { type: string, ticketId: number };
  AddDevices: {displayPhoneNumber: string};
  MeSetting: undefined;
  MeSettingNotification: undefined;
  MeSettingUser: undefined;
  DeviceList: { isFromAddDevice: boolean };
  PermissionAccess: { type: number };
  CognitoSignUpCompleted: undefined;
  MeChangeMobileNumber: undefined;
  HistoryChartDetail: undefined;
  MeChangeUsername: undefined;
  CountryOptionView: { type: string, selectedId?: string, onSelect: (id: string) => void };
  PrivacyPolicy: undefined;
  TermsOfService: undefined;
  CaptchaPage: { onSuccess: (data: any) => void };
  ForceChangePassword: { username: string, skipVerification?: {isSkip: boolean, username: string, password: string}}
  UserVerification: { type: string, callBack: (phone: string, phonePrefix: string) => void }
  MIO: { type: MIOType };
  MeAboutUsScreen: undefined;
  AccountQuestion: {email?: string; token?: string} | any;
  RecommendedTradingAccount: undefined;
  AccountAgreement: undefined;
  AccountGettingReady: undefined;
  AccountPersonalInformation: {lastScreen?: string} | undefined;
  AccountContactInformation: {lastScreen?: string} | undefined;
  AccountFinancialInformation: {lastScreen?: string} | undefined;
  AccountTradingExperience: {lastScreen?: string} | undefined;
  AccountReviewApplication: undefined;
  AccountPromotion: undefined;
  AccountApplicationStatus: undefined;
  AccountStatus: undefined;
  EkycScreen: undefined;
  DropdownPage: { label: string; items: any[]; onSelect: (val: string) => void; selected: string; };
  CountryDropdownPage: { type: string; selectedId?: string; onSelect: (id: string) => void };
  AccountTermsCondition: undefined;
};

export type ScreenNavigationProp<
  T extends keyof RootStackParamList
  > = StackNavigationProp<RootStackParamList, T>;

export type ScreenRouteProp<T extends keyof RootStackParamList> = RouteProp<
  RootStackParamList,
  T
>;
export type Props<T extends keyof RootStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export type BottomTabParamList = {
  Tab_WatchlistView: undefined,
  Tab_Portfolio: {defaultTab?: any, positionTicket?: number},
  Tab_Me: undefined
}

export type BottomTabProps<T extends keyof BottomTabParamList> =
  CompositeScreenProps<
    BottomTabScreenProps<BottomTabParamList, T>,
    Props<keyof RootStackParamList>
  >;