import { actionTimeoutMilliseconds, entityId } from '../../config/constants';
import { State } from '../../redux/root-reducer';
import { store } from '../../redux/store';
import { fetchWithTimeout, tradingAPI } from './apiSetter';

const getCurrentEntity = () => {
  const state: State = store.getState();
  if (state.account.isDemoAccount && state.account.demoTradingAccount.Entity !== null)
    return state.account.demoTradingAccount.Entity;
  else if(state.account.account !== null && state.account.account.LoginLevel === 3 && state.account.tradingAccountList.length > 0)
    return state.account.tradingAccountList[0].Entity;
  
  return entityId;
}


export const getRecommendList = async (accessToken: string) => {

  const requestOptions = {
    method: 'GET',
    headers: {
      'Authorization': 'Bearer ' + accessToken
    }
  };

  const url = tradingAPI + "GetRecommendList?entity=" + getCurrentEntity();
  console.debug('getRecommendList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug(error);
    return { "status": 999 };
  }
};

export const getWatchList = async (accessToken: string) => {

  const requestOptions = {
    method: 'GET',
    headers: {
      'Authorization': 'Bearer ' + accessToken
    }
  };
  const url = tradingAPI + "GetWatchList";
  console.debug('GetWatchList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("watchlist here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("watchlist error");
    return { "status": 999 };
  }
};

export const addWatchList = async (accessToken: string, symbol: string) => {

  const body: any = {
    symbol: symbol
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "AddWatchList";
  console.debug('AddWatchList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("AddWatchList here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("AddWatchList error");
    return { "status": 999 };
  }
};

export const removeWatchList = async (accessToken: string, symbol: string) => {

  const body: any = {
    symbol: symbol
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "RemoveWatchList";
  console.debug('RemoveWatchList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("RemoveWatchList here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("RemoveWatchList error");
    return { "status": 999 };
  }
};

export const replaceWatchList = async (accessToken: string, symbol: string) => {

  const body: any = {
    symbols: symbol
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "ReplaceWatchList";
  console.debug('ReplaceWatchList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("ReplaceWatchList here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("ReplaceWatchList error");
    return { "status": 999 };
  }
};

export const getDealHistory = async (accessToken: string, login: number, startTime: number, endTime: number, offSet: number, limit: number, sortType: number, symbol?: string,) => {

  const body: any = {
    login: login,
    symbol: symbol,
    startTime: startTime,
    endTime: endTime,
    offset: offSet,
    limit: limit,
    sortType: sortType
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "GetDealHistory";
  console.debug('GetDealHistory:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    //console.debug("getDealHistory here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("getDealHistory error");
    return { "status": 999 };
  }
};

export const getTransactionHistory = async (accessToken: string, login: number, startTime: number, endTime: number, offSet: number, limit: number) => {

  const body: any = {
    login: login,
    startTime: startTime,
    endTime: endTime,
    offset: offSet,
    limit: limit
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "GetTransactionHistory";
  console.debug('GetTransactionHistory:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    //console.debug("getDealHistory here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("getTransactionHistory error");
    return { "status": 999 };
  }
};

export const getDealHistorySymbolList = async (accessToken: string, login: number, startTime: number, endTime: number) => {

  const body: any = {
    login: login,
    startTime: startTime,
    endTime: endTime
  };
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + accessToken
    },
    body: formBody.join('&'),
  };
  const url = tradingAPI + "GetDealHistorySymbolList";
  console.debug('GetDealHistorySymbolList accessToken:' + accessToken)
  console.debug('GetDealHistorySymbolList:' + url)
  try {
    const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
    const data = await response.json();
    console.debug("GetDealHistorySymbolList here: " + JSON.stringify(data))
    return data;
  }
  catch (error) {
    console.debug("GetDealHistorySymbolList error");
    return { "status": 999 };
  }
};