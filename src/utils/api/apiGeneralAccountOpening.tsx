import {
  entityId,
  host_accountOpening,
  host_loyaltyApi,
  loyalty_getExistingAccount,
} from '../../config/constants';

type ServiceProps = {
  token: string;
  onSuccess: (val: any) => void;
  onFailed?: (err: any) => void;
};

export const getCountryList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/countries`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getIdentificationTypeList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/identification-type`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getEmploymentStatusList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/employment-status`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getAnnualIncomeList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/annual-income`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getNetworthList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/networth`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getTradingFundList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/trading-fund`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getIndustriesList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/industries`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getTradeproductList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/trade-products`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getTradingDurationList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/trading-duration`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getTradingFrequencyList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/trading-frequency`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getPurposeOpeningList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/purpose-opening`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getJobTitleList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(`${host_accountOpening}/enum/job-titles`, {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(data => {
      if (data.response) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const checkMemberLoyalty = async ({
  email,
  token,
  onSuccess,
  onFailed,
}: {email: string} & ServiceProps) => {
  await fetch(
    `${host_loyaltyApi}${loyalty_getExistingAccount}?entity=${
      entityId === 'EIE' ? 'EIEHK' : 'XPro'
    }&email=${email}`,
    {
      method: 'GET',
      headers: {
        Authorization: token,
      },
    },
  )
    .then(res => res.json())
    .then(data => {
      if (data) {
        onSuccess(data);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};

export const getPromotionList = async ({
  token,
  onSuccess,
  onFailed,
}: ServiceProps) => {
  await fetch(
    `${host_loyaltyApi}/loyalty/promotion?entity=${
      entityId === 'EIE' ? 'EIEHK' : 'XPro'
    }&promotionType=Account Opening`,
    {
      method: 'GET',
      headers: {
        Authorization: token,
      },
    },
  )
    .then(res => res.json())
    .then(data => {
      if (data) {
        onSuccess(data?.response);
      } else {
        throw Error(data.error);
      }
    })
    .catch(err => onFailed(err));
};
