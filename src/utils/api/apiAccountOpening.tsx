import {entityId, host_accountOpening, host_cm} from '../../config/constants';
import axios, { CancelToken } from 'axios';

type ServiceProps = {
  token: string;
  payload?: any;
  params?: any;
  endpoint?: string;
  onSuccess: (val: any) => void;
  onFailed: (err: any) => void;
  setLoading?: (loading: boolean) => void;
  cancelToken?: CancelToken
};

const prefix = '/account-opening';

export const fetchCheckIndividual = async ({
  endpoint,
  token,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);
  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/${endpoint}?entity=${
        entityId === 'EIE' ? 'EIEHK' : 'XPro'
      }`,
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const getIndividual = async ({
  payload,
  token,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);
  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/draft/single`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: {
        ...payload,
        entity: entityId === 'EIE' ? 'EIEHK' : 'XPro',
      },
    });
    console.debug(`[utils/api/apiAccountOpening getIndividual] axios request ${res.request?.url} token ${token} response ${res.status} ${JSON.stringify(res.data)}`)
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const setApplicantInformation = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/applicant-information`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    console.debug(`[utils/api/apiAccountOpening setApplicantInformation] axios request ${res.request?.url} token ${token} response ${res.status} ${JSON.stringify(res.data)}`)
    onSuccess(res.data);
  } catch (error) {
    console.error(`[utils/api/apiAccountOpening setApplicantInformation] ${error}`)
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateApplicantInformation = async ({
  endpoint,
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/applicant-information/${endpoint}`,
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateTradingAgreement = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/trading-agreement`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updatePersonalInformation = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/personal-information`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateContactInformation = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/contact-information`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateFinancialInformation = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/employee-financial-information`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateTradingExperience = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/trading-experience`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      data: payload,
    });
    onSuccess(res);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const updateIndividual = async ({
  token,
  endpoint,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/${endpoint}`,
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      },
      data: payload,
    });
    onSuccess(res);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const confirmIndividual = async ({
  token,
  endpoint,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/confirm/${endpoint}`,
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      },
      data: payload,
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const setPromotion = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/promo`,
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      },
      data: payload,
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const validatePromotion = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
}: ServiceProps) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_cm}/cm/user/account-opening-validation`,
      method: 'POST',
      headers: {
        Authorization: token,
      },
      data: payload,
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const validateIdType = async ({
  token,
  idType,
  idNumber,
  onSuccess,
  onFailed,
  setLoading,
}: {
  idType: string;
  idNumber: string;
  token: string;
  onSuccess: (val: any) => void;
  onFailed: (err: any) => void;
  setLoading?: (loading: boolean) => void;
}) => {
  setLoading && setLoading(true);

  try {
    const res = await axios({
      url: `${host_accountOpening}${prefix}/individual/check/id/${idType}/${idNumber}`,
      method: 'GET',
      headers: {
        Authorization: token,
      },
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const getPersonMessage = async ({
  token,
  payload,
  onSuccess,
  onFailed,
  setLoading,
  cancelToken,
}: ServiceProps) => {
  setLoading && setLoading(true);
  try {
    console.debug(`[utils/api/apiAccountOpening getPersonMessage] axios request ${`${host_cm}/cm/user/getPersonMessage`} token ${token} ${JSON.stringify(payload)}`)
    const res = await axios({
      url: `${host_cm}/cm/user/getPersonMessage`,
      method: 'POST',
      headers: {
        Authorization: token,
      },
      data: payload,
      cancelToken: cancelToken,
    }).catch((errorAxiosRejectReason) => {
      if (axios.isCancel(errorAxiosRejectReason)) {
        console.debug(`[utils/api/apiAccountOpening getPersonMessage] try await axios().catch() axios.isCancel ${errorAxiosRejectReason}`)
        return Promise.reject(errorAxiosRejectReason)
      }
    });
    console.debug(`[utils/api/apiAccountOpening getPersonMessage] axios request ${res.request?.url} token ${token} response ${res.status} ${JSON.stringify(res.data)}`)
    onSuccess(res.data);
  } catch (error) {
    if (axios.isCancel(error)) {
      console.debug(`[utils/api/apiAccountOpening getPersonMessage] try {} catch {} axios.isCancel ${error}`)
      throw error
    }
    console.error(`[utils/api/apiAccountOpening getPersonMessage] ${error}`)
    onFailed(error);
  } finally {
    setLoading && setLoading(false);
  }
};

export const setUserLanguage = async ({
  token,
  id,
  lang,
  onSuccess,
  onFailed,
}) => {
  try {
    const res = await axios({
      url: `${host_accountOpening}/account-opening/individual/lang/${id}/${lang}`,
      method: 'PUT',
      headers: {
        Authorization: token,
      },
    });
    onSuccess(res.data);
  } catch (error) {
    onFailed(error);
  }
};
