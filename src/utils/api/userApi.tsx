/* eslint-disable no-trailing-spaces */
import {sha256} from 'react-native-sha256';
import {RSA, RSAKeychain} from 'react-native-rsa-native';
import {actionTimeoutMilliseconds, entityId, loginSecret} from '../../config/constants';
import {fetchWithTimeout, gatewayURL} from './apiSetter';
import { Asset } from 'react-native-image-picker';
import { getUniqueId } from 'react-native-device-info';
import DeviceInfo from 'react-native-device-info'
import { Auth } from 'aws-amplify';
import { Alert, Platform } from 'react-native';
import { isEmpty } from '../stringUtils';

export var lv3Registration = '';

export const setLv3Registration = (url: string) => {
  lv3Registration = url;
}

export const anyLogin = async (pushyToken: string, locale: number, regionId: string) => {
  var deviceId = await getUniqueId();

  var body: any = {
    token: pushyToken,
    locale: locale,
    regionId: regionId,
    deviceId: deviceId
  };

  if (!isEmpty(entityId)) {
    body = {
      token: pushyToken,
      locale: locale,
      entityId: entityId,
      regionId: regionId,
      deviceId: deviceId
    };
  }
  
  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    body: formBody.join('&'),
  };
  console.debug('anyLogin:' + gatewayURL + 'user/anylogin')
  var result = await fetchWithTimeout(gatewayURL + 'user/anylogin', requestOptions, actionTimeoutMilliseconds)
    .then(response => response.json())
    .then(data => {
      // console.debug(data);
      if (data.code === '00') {
        return data.data;
      } else {
        return undefined;
      }
    }).catch((error)=>{
      // Alert.alert(error.toString())
    });

  return result;
};

export const addPasswordCount = async (
  username: string
) => {

  var body: any = {
    username: username,
    entityId: entityId,
    wrongPassword: true
  };

  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      token: '',
      timestamp_now: '',
      oncestr: ''
    },
    body: formBody.join('&'),
  };
  var result = await fetch(gatewayURL + 'user/loginLevel', requestOptions)
    .then(response => response.json())
    .then(data => {
      // console.debug(data);
      if (data.code === '00') {
        return data.data;
      } else {
        return undefined;
      }
    }).catch((error)=>{
      console.debug(error.toString())
    });

  return result;
};
  
export const loginLevel = async (
    cognitoUserName: string,
    cognitoToken: string,
    pushyToken: string,
    regionId: string,
    isRefresh: boolean = false
  ) => {
    console.debug('loginLevel2 ' + cognitoUserName + ' ' + pushyToken)
    var deviceId = await getUniqueId();
    var deviceName= Platform.OS === 'android' ? await DeviceInfo.getDeviceName() : DeviceInfo.getModel()
    
    var body: any = {
      userId: cognitoUserName,
      deviceId: deviceId,
      deviceName: deviceName,
      isRefresh: isRefresh ? '1' : '0',
      entityId: entityId ? entityId: '',
      regionId: regionId
    };
  
    if (pushyToken !== undefined) {
      body = {
        userId: cognitoUserName,
        pushyToken: pushyToken,
        deviceId: deviceId, deviceName: deviceName,
        isRefresh: isRefresh ? '1' : '0',
        entityId: entityId ? entityId: '',
        regionId: regionId
      };
    }
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    var timestamp_now = Math.round(Date.now() / 1000).toString();
    var oncestr = cognitoUserName + timestamp_now + loginSecret;
    await sha256(oncestr).then(hash => {
      oncestr = hash;
    });

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken,
        timestamp_now: timestamp_now,
        oncestr: oncestr
      },
      body: formBody.join('&'),
    };
    var result = await fetchWithTimeout(gatewayURL + 'user/loginLevel', requestOptions, actionTimeoutMilliseconds)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        console.debug(error.toString())
      });
  
    return result;
  };

  export const refreshToken = async (
    tradeAccount: string,
    entityId: string
  ) => {
    const session = await Auth.currentSession();
    var token = session.getAccessToken().getJwtToken();
    var payload: { [key: string]: any; } = session.getIdToken().payload;
    // console.debug('cognito session', JSON.stringify(session))
    var cognitoUserName = payload['cognito:username'];
    
    var deviceId = await getUniqueId();

    var body: any = {
      userId: cognitoUserName,
      deviceId: deviceId,
      tradeAccount: tradeAccount,
      entityId: entityId
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    var timestamp_now = Math.round(Date.now() / 1000).toString();
    var oncestr = cognitoUserName + timestamp_now + loginSecret;
    await sha256(oncestr).then(hash => {
      oncestr = hash;
    });

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: token,
        timestamp_now: timestamp_now,
        oncestr: oncestr
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/refreshToken', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };
  
  export const switchAccount = async (
    cognitoUserName: string,
    cognitoToken: string,
    tradeAccount: string,
    tradeType: string,
    entityId: string
  ) => {
    var deviceId = await getUniqueId();
    
    const body: any = {
      userId: cognitoUserName,
      tradeType: tradeType,
      tradeAccount: tradeAccount,
      entityId: entityId,
      deviceId: deviceId
    };
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    var timestamp_now = Math.round(Date.now() / 1000).toString();
    var oncestr = cognitoUserName + timestamp_now + loginSecret;
    await sha256(oncestr).then(hash => {
      console.debug(hash);
      oncestr = hash;
    });
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken,
        timestamp_now: timestamp_now,
        oncestr: oncestr,
      },
      body: formBody.join('&'),
    };
    var result = fetchWithTimeout(gatewayURL + 'user/switchAccount', requestOptions, actionTimeoutMilliseconds)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const logout = async (
    cognitoUserName: string
  ) => {
    // console.debug('loginLevel2 ' + cognitoUserName + ' ' + pushyToken)
    var deviceId = await getUniqueId();

    var body: any = {
      userId: cognitoUserName,
      deviceId: deviceId,
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    var timestamp_now = Math.round(Date.now() / 1000).toString();
    var oncestr = cognitoUserName + timestamp_now + loginSecret;
    await sha256(oncestr).then(hash => {
      oncestr = hash;
    });

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/logout', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const completeSignUp = async (
    cognitoUserName: string,
    email: string,
    phone: string,
    phonePrefix: string
  ) => {
    var deviceId = await getUniqueId();

    var body: any = {
      userId: cognitoUserName,
      deviceId: deviceId,
      deviceName: Platform.OS === 'android' ? await DeviceInfo.getDeviceName() : DeviceInfo.getModel(),
      email: email,
      phone: phone,
      phonePrefix: phonePrefix,
      entityId: entityId
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };

    console.debug('completeSignUp formBody', formBody)
    var result = await fetch(gatewayURL + 'user/completeSignUp', requestOptions)
      .then(response => response.json())
      .then(data => {
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const userDeviceList = async () => {
    const session = await Auth.currentSession();
    var token = session.getAccessToken().getJwtToken();
    var payload: { [key: string]: any; } = session.getIdToken().payload;
    // console.debug('cognito session', JSON.stringify(session))
    var cognitoUserName = payload['cognito:username'];

    var body: any = {
      userId: cognitoUserName
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: token
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/getUserDevice', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const addUserDevice = async (
    cognitoUserName: string,
    cognitoToken: string
  ) => {
    var deviceId = await getUniqueId();
    var deviceName= Platform.OS === 'android' ? await DeviceInfo.getDeviceName() : DeviceInfo.getModel()

    var body: any = {
      userId: cognitoUserName,
      deviceId: deviceId,
      deviceName: deviceName
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/addUserDevice', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const removeUserDevice = async (
    cognitoUserName: string,
    cognitoToken: string,
    oldDeviceId: string,
    isReplace?: boolean
  ) => {
    var deviceId = await getUniqueId();
    var deviceName= Platform.OS === 'android' ? await DeviceInfo.getDeviceName() : DeviceInfo.getModel()

    var body: any = {
      userId: cognitoUserName,
      oldDeviceId: oldDeviceId,
      newDeviceId: isReplace ? deviceId : '',
      newDeviceName: isReplace ? deviceName : '',
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/removeUserDevice', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const registerDemo = async (
    cognitoUserName: string,
    cognitoToken: string,
    regionId: String,
    accessToken: String
  ) => {
    var body: any = {
      userId: cognitoUserName,
      regionId: regionId,
      accessToken: accessToken,
      //entityId: entityId
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/registerDemo', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const changeUserNickname = async (
    cognitoUserName: string,
    cognitoToken: string,
    nickname: string
  ) => {
    var body: any = {
      userId: cognitoUserName,
      nickname: nickname
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        token: cognitoToken
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/changeUserNickname', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const getTimeZoneName = async (
    timezone: string,
    locale: string
  ) => {

    var body: any = {
      timezone: timezone,
      locale: locale
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/getTimeZoneName', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const updateProfileMobile = async (
    cognitoUserName: string,
    phone: string,
    phonePrefix: string
  ) => {
    var body: any = {
      userId: cognitoUserName,
      phone: phone,
      phonePrefix: phonePrefix,
      entityId: entityId
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/updateProfileMobile', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const updateProfilePassword = async (
    cognitoUserName: string,
    password: string,
    username: string
  ) => {
    var body: any = {
      userId: cognitoUserName,
      password: password
    };

    if (username) {
      body = {
        username: username,
        password: password,
        entityId: entityId
      };
    }
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/updateProfilePassword', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };

  export const updateUserAttribute = async (
    cognitoUsername: string,
    attribute: string,
    value: string,
    forceUpdate: boolean
  ) => {
    var body: any = {
      username: cognitoUsername,
      attribute: attribute,
      value: value,
      entityId: entityId,
      forceUpdate: forceUpdate
    };
  
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      body: formBody.join('&'),
    };
    var result = await fetch(gatewayURL + 'user/updateUserAttribute', requestOptions)
      .then(response => response.json())
      .then(data => {
        // console.debug(data);
        if (data.code === '00') {
          return data.data;
        } else {
          return undefined;
        }
      }).catch((error)=>{
        // Alert.alert(error.toString())
      });
  
    return result;
  };