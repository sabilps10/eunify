export var gatewayURL = '';
export var tradingAPI = '';
export var chartAPI = '';
export var chartWSPath = '';

export const setAPIURL = (url: string) => {
    gatewayURL = url;
}

export const setTradingURL = (url: string) => {
    tradingAPI = url;
}

export const setChartURL = (path: string, wsPath: string) => {
    console.debug("setChartURL: " + path);
    chartAPI = path;
    chartWSPath = wsPath;
}

export async function fetchWithTimeout(url: RequestInfo, option: RequestInit = {}, timeout = 5000) {
    const controller = new AbortController();
    console.debug("timeout: " + timeout)
    const timeoutId = setTimeout(() => controller.abort(), timeout);
    const requestOptions = {
        ...option,
        signal: controller.signal
    }
    const response = await fetch(url, requestOptions);
    clearTimeout(timeoutId);
    return response;
}

export async function sleep(ms: number) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}