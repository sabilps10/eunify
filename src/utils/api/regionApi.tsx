/* eslint-disable no-trailing-spaces */
import { gatewayURL } from './apiSetter';

export const getRegion = async () => {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  };

  var path = gatewayURL + 'region/getRegion';

  console.debug('path ' + path);

  const result = await fetch(path, requestOptions).then(response => response.json())
  .then(data => {
    // console.debug(data);
    if (data.code === '00') {
      return data.data;
    } else {
      return undefined;
    }
  }).catch((error)=>{
    // Alert.alert(error.toString())
  });

  return result;
};