import base64 from 'react-native-base64'
import { entityId } from '../../config/constants'
// import { MIOUserTokenDomain } from '../../config/constants'
// import { MIOClientSecret } from '../../config/constants'
// import { MIOClientId } from '../../config/constants'
// import { MIOAuthDomain } from '../../config/constants'
import i18n from '../../locales/i18n'
import { gatewayURL } from './apiSetter'


export const OAuth2Token = async(MIOAuthDomain: string, MIOClientId: string, MIOClientSecret: string) => {
    // console.debug('OAuth2Token', MIOClientId, MIOClientSecret)
    let authorization = base64.encode(MIOClientId + ':' + MIOClientSecret)

    var headers = {
      'Authorization': 'Basic ' + authorization,
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'XSRF-TOKEN=ba98469b-2356-438a-9b72-47934d001923'
    }

    var body = {
      grant_type: "client_credentials"
    }
    var formBody = [];
    for (var key in body) {
      var encodedKey = encodeURIComponent(key);
      var encodedValue = encodeURIComponent(body[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    var requestOptions = {
      method: 'POST',
      headers: headers,
      body: formBody.join('&'),
    }

    // console.debug('requestOptions:' + JSON.stringify(requestOptions))

    var result = await fetch(MIOAuthDomain + '/oauth2/token', requestOptions)
      .then(response => response.json())
      .then(result => {
        // console.debug('result:' + JSON.stringify(result))
        return result
      })
      .catch(error => console.debug('error:' + error))

    return result
}

export const UserInfoToken = async(MIOUserTokenDomain: string, MIOEntityID: string, username: string, tradeAccount: string, headerToken: string) => {
  console.debug('UserInfoToken')

  var headers = {
    'Authorization': 'Bearer  ' + headerToken,
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
  var string = {
    entity_id: MIOEntityID,//'{XPro|EIEHK|EBL}',
    customer_account_no: username,
    trading_account_no: tradeAccount,
    lang: i18n.language,
  }
  var body = {
    string: JSON.stringify(string)
  }
  const requestOptions = {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(body),
  }

  // console.debug('requestOptions body:' + JSON.stringify(body))
  // console.debug('requestOptions:' + JSON.stringify(requestOptions))
  // console.debug('MIOUserTokenDomain:' + MIOUserTokenDomain)

  var result = await fetch(MIOUserTokenDomain, requestOptions)
    .then(response => response.json())
    .then(data => {
      // console.debug('UserInfoToken result:' + JSON.stringify(data))
      return data
    })
    .catch(error => console.debug('error:' + error))

  return result
}

export const getMIOToken = async (
  cognitoToken: string,
  cognitoUserName: string,
  tradeAccount: string
) => {
  var body: any = {
    userId: cognitoUserName,
    tradeAccount: tradeAccount,
    entityId: entityId,
    lang: i18n.language
  };

  var formBody = [];
  for (var key in body) {
    var encodedKey = encodeURIComponent(key);
    var encodedValue = encodeURIComponent(body[key]);
    formBody.push(encodedKey + '=' + encodedValue);
  }

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      token: cognitoToken,
    },
    body: formBody.join('&'),
  };
  var result = await fetch(gatewayURL + 'user/getMIOToken', requestOptions)
    .then(response => response.json())
    .then(data => {
      // console.debug(data);
      if (data.code === '00') {
        return data.data;
      } else {
        console.error(`[apiMIO getMIOToken] fetch ${gatewayURL + 'user/getMIOToken'} request ${JSON.stringify(requestOptions)} response ${JSON.stringify(data)}`)
        return undefined;
      }
    }).catch((error)=>{
      console.error(`[apiMIO getMIOToken] fetch ${gatewayURL + 'user/getMIOToken'} request ${JSON.stringify(requestOptions)} response try catch ${JSON.stringify(error)}`)
      // Alert.alert(error.toString())
    });

  return result;
};

export const checkIsMIDepositReady = async (
  mioServiceDomain: String,
  cognitoToken: string,
  tradeAccount: string
) => {

  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Authorization': 'Bearer ' + cognitoToken,
    },
  };
  // https://uat-aomio-cp.empfs.net/api/cp/mio/accounts/ready/deposit?entityId=EIEHK&tradingAccountNo=86087597
  const url = mioServiceDomain + '/api/cp/mio/accounts/ready/deposit' + `?entityId=${entityId === 'EIE' ? 'EIEHK' : 'XPro'}&tradingAccountNo=${tradeAccount}`
  console.debug(`[apiMIO checkIsMIDepositReady] fetch ${url} request ${JSON.stringify(requestOptions)}`)
  try {
    var result = await fetch(url, requestOptions)
    .then(async response => response.status < 500 ? response.json() : {[`httpError${response.status}${response.statusText}`]: await response.text()})
    .then(data => {
      console.debug(`[apiMIO checkIsMIDepositReady] fetch ${url} request ${JSON.stringify(requestOptions)} response ${JSON.stringify(data)}`)
      if (data.result) {
        return data.response;
      } else {
        return Promise.reject(Error(JSON.stringify(data)))
      }
    }).catch((error)=>{
      console.error(`[apiMIO checkIsMIDepositReady] fetch ${url} request ${JSON.stringify(requestOptions)} promise fetch().then().then().catch() ${error} ${JSON.stringify(error)}`)
      return Promise.reject(error)
    });
  } catch (error) {
    console.error(`[apiMIO checkIsMIDepositReady] try {} catch {} ${error} ${JSON.stringify(error)}`)
    throw error
  }

  return result;
};