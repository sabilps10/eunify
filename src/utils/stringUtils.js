export function isEmpty(str) {
    return (!str || str.length === 0 );
}

export function toCurrencyDisplay(value) {
    if (value || value === 0 || Number.isFinite(value))
        return value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');  // 12,345.67
    else 
        return '';
}

export function isEmail(str) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    // console.debug(str, reg.test(str))
    return reg.test(str);
}

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}

export function formatDate(date) {
    return (
        [
            date.getFullYear(),
            padTo2Digits(date.getMonth() + 1),
            padTo2Digits(date.getDate()),
        ].join('-') +
        ' ' +
        [
            padTo2Digits(date.getHours()),
            padTo2Digits(date.getMinutes()),
            padTo2Digits(date.getSeconds()),
        ].join(':')
    );
}

export function formatDateYYYY(date) {
    return (
        [
            date.getFullYear(),
            padTo2Digits(date.getMonth() + 1),
            padTo2Digits(date.getDate()),
        ].join('-')
    );
}

export function maskEmail(email){
    var a = email.split('.');
    if (a.length >= 2){
        var returnResult = '';
        var a1 = '';
        var a2 = '';
        if (a.length > 2) {
            for (var i = 0 ; i < a.length ; i++) {
                if (i === a.length - 1){
                    a2 = a[i]
                } else {
                    a1 += a[i];
                }
            }
        } else {
            a1 = a[0];
            a2 = a[1];
        }
        if (a1.length >= 6) {
            returnResult += a1.substring(0, 4)
            for (var i = 0 ; i < a1.length-6 ; i++){
                returnResult += '*';
            }
            returnResult += a1.substring(a1.length-2, a1.length)
            return returnResult + '.' + a2
        }
        else {
            return email;
        }
    } else {
        return email;
    }
}

export function maskPhone(countryCode, phoneNumber){
    if (phoneNumber) {
        var returnResult = '';
        returnResult += phoneNumber.substring(0, 2)
        for (var i = 0 ; i < phoneNumber.length-4 ; i++){
            returnResult += '*';
        }
        returnResult += phoneNumber.substring(phoneNumber.length-2, phoneNumber.length)
        return countryCode + '-' + returnResult;
    } else {
        return '';
    }
}

export function maskPhoneFull(phoneNumber, regionDataList){
    if (phoneNumber) {
        // const countryCodes = require('country-codes-list')
        // const myCountryCodesObject = countryCodes.customList('countryCallingCode', '+{countryCallingCode}')
        const countryKeys =  regionDataList.map(a => a.countryCode);
        var _phonePrefix = '';
        var _phoneNumber = '';
        if (countryKeys.includes(phoneNumber.substring(1, 5))){
            _phonePrefix = phoneNumber.substring(1, 5);
            _phoneNumber = phoneNumber.substring(5, phoneNumber.length);
        }
        else if (countryKeys.includes(phoneNumber.substring(1, 4))){
            _phonePrefix = phoneNumber.substring(1, 4);
            _phoneNumber = phoneNumber.substring(4, phoneNumber.length);
        }
        else if (countryKeys.includes(phoneNumber.substring(1, 3))){
            _phonePrefix = phoneNumber.substring(1, 3);
            _phoneNumber = phoneNumber.substring(3, phoneNumber.length);
        }
        else if (countryKeys.includes(phoneNumber.substring(1, 2))){
            _phonePrefix = phoneNumber.substring(1, 2);
            _phoneNumber = phoneNumber.substring(2, phoneNumber.length);
        }

        return maskPhone(_phonePrefix, _phoneNumber)
    } else {
        return '';
    }
}

export function isNullString(e) {
    return e === null || e === undefined || typeof e === 'undefined' || e === '';
  }
  
export function nullToEmpty(e) {
    if (isNull(e)) {
      return "";
    } else {
      return e;
    }
  }

  export function commify(n) {
    var parts = n.toString().split(".");
    const numberPart = parts[0];
    const decimalPart = parts[1];
    const thousands = /\B(?=(\d{3})+(?!\d))/g;
    return numberPart.replace(thousands, ",") + (decimalPart ? "." + decimalPart : "");
}