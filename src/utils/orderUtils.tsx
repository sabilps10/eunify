import { useTheme } from "@react-navigation/native";
import { StackView } from "@react-navigation/stack";
import { ColorValue, StyleProp, Text, TextStyle, View, ViewStyle } from "react-native";
import DashedLineView from "../components/dashedLineView.component";
import { PriceQuoteParam, SymbolContent } from "../redux/trading/type";
import { makeGlobalStyles } from "../styles/styles";

export enum OrderActionType {
    NEW_ORDER = "NEW_ORDER",
    EDIT_POSITION = "EDIT_POSITION",
    EDIT_ORDER = "EDIT_ORDER",
    CLOSE_POSITION = "CLOSE_POSITION",
    CANCEL_ORDER = "CANCEL_ORDER"
}

export enum OrderDirectionType {
    BUY = 0,
    SELL = 1
}

export enum PendingOrderType {
    LIMIT = 0,
    STOP = 1,
}

export enum PendingOrderBuySellType {
    BUY_LIMIT = 2,
    BUY_STOP = 4,
    SELL_LIMIT = 3,
    SELL_STOP = 5
}

export enum DetailsType {
    NEW_POSITION = "NEW_ORDER",
    CLOSE_POSITION = "CLOSE_POSITION",
    PENDING_ORDER = "PENDING_ORDER"
}

export enum ProfitStopLossOrderType {
    PRICE = 0,
    PIPS = 1,
    PERCENTAGE = 2
}

export enum CalcMode {
    FOREX = 0,
    CFD = 1,
    FUTURES = 2,
    CFD_INDEX = 3,
    CFD_LEVERAGE = 4
}

export enum ForexType {
    NON_FOREX = "NON_FOREX",
    INDIRECT_QUOTE = "INDIRECT_QUOTE",
    DIRECT_QUOTE = "DIRECT_QUOTE",
    CROSS_INDIRECT_QUOTE = "CROSS_INDIRECT_QUOTE",
    CROSS_DIRECT_QUOTE = "CROSS_DIRECT_QUOTE",
}

export function getConvertPrice(isSelectBuy: boolean, contractSymbols: SymbolContent, contractPrices: PriceQuoteParam, requiredMargin?: boolean, requiredMarginPrice?: number) {
    var convertPrice = 1;

    if (contractSymbols.MarginMode === 0) {
        const first = contractSymbols.SymbolName.substring(0, 3);
        const second = contractSymbols.SymbolName.substring(3, 6);
        const group = contractSymbols.SymbolName.substring(6, contractSymbols.SymbolName.length);
        
        if (first === 'USD'){
            //Direct Quote, do nothing
            // console.debug('updateRequireMargin Direct Quote')
        } else if (second === 'USD'){
            //Indriect Quote
            // console.debug('updateRequireMargin Indriect Quote')
            // convertPrice = isSelectBuy ? contractPrices[contractSymbols.SymbolName].Ask : contractPrices[contractSymbols.SymbolName].Bid
            convertPrice = requiredMargin ? requiredMarginPrice : (contractPrices[contractSymbols.SymbolName].Ask + contractPrices[contractSymbols.SymbolName].Bid)/2
        } else {
            const quote1 = contractPrices[first+'USD'+group];
            const quote2 = contractPrices['USD'+first+group];
            if (quote1) {
                //Cross Indriect Quote
                // console.debug('updateRequireMargin Cross Indriect Quote')
                // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
                convertPrice = (quote1.Ask + quote1.Bid)/2
            } else if (quote2) {
                //Cross Driect Quote
                // console.debug('updateRequireMargin Cross Driect Quote')
                convertPrice = 1/((quote2.Ask + quote2.Bid)/2)
            }
        }
    }
    return convertPrice;
}

export interface CalculateMarginInterface {
    symbol: string, // Debug use
    marginCurrency: string, // Debug use
    marginMode: number,
    isFixedMargin: boolean,
    margin: number, // initial margin or maintenance margin
    contractSize: number,
    percentage: number,
    tickSize: number,
    tickValue: number,
    accountLeverage: number,
    lots: number,
    price: number,
    marginRate: number,
}

export function calculateMargin(params: CalculateMarginInterface) {
    // console.debug('calculateMargin')
    // console.debug('calculateMargin, params:' + JSON.stringify(params))
    const {marginMode, isFixedMargin, margin, contractSize, percentage, tickSize, tickValue, accountLeverage, lots, price, marginRate } = params
 
    let isFutures = marginMode === CalcMode.FUTURES
    let neededMargin: number = 0

    if (isFixedMargin && !isFutures) {
        switch (marginMode) {
            case CalcMode.FOREX:
            case CalcMode.CFD_LEVERAGE:
                neededMargin = lots * margin / accountLeverage * percentage
                break
            case CalcMode.CFD:
            case CalcMode.FUTURES:
            case CalcMode.CFD_INDEX:
                neededMargin = lots * margin * percentage
                break
        }
    }
    else {
        switch (marginMode) {
            case CalcMode.FOREX:
                neededMargin = lots * contractSize / accountLeverage * percentage
                break
            case CalcMode.CFD:
                neededMargin = lots * contractSize * price * percentage
                break
            case CalcMode.FUTURES:
                neededMargin = lots * margin * percentage
                break
            case CalcMode.CFD_INDEX:
                neededMargin = lots * contractSize * price / tickSize * tickValue * percentage
                break
            case CalcMode.CFD_LEVERAGE:
                neededMargin = lots * contractSize * price / accountLeverage * percentage
                break
        }
    }
    // need calculate exchange rate
    if (marginRate) {
        neededMargin *= marginRate
    }
    return neededMargin
}

export function getPLConvertPrice(isBuy: boolean, contractSymbols: SymbolContent, contractPrices: PriceQuoteParam) {
    var convertPrice = 1;

    const first = contractSymbols.SymbolName.substring(0, 3);
    const second = contractSymbols.SymbolName.substring(3, 6);
    const group = contractSymbols.SymbolName.substring(6, contractSymbols.SymbolName.length);
    
    if (first === 'USD'){
        //Direct Quote
        convertPrice = 1/ (isBuy ? contractPrices[contractSymbols.SymbolName].Bid : contractPrices[contractSymbols.SymbolName].Ask)
    } else if (second === 'USD'){
        //Indriect Quote
    } else {
        const quote1 = contractPrices[second+'USD'+group];
        const quote2 = contractPrices['USD'+second+group];
        if (quote1) {
            //Cross Indriect Quote
            // console.debug('updateRequireMargin Cross Indriect Quote')
            // convertPrice = isSelectBuy ? quote1.Ask : quote1.Bid
            convertPrice = isBuy ? quote1.Bid : quote1.Ask
        } else if (quote2) {
            //Cross Driect Quote
            // console.debug('updateRequireMargin Cross Driect Quote')
            convertPrice = 1/ (isBuy ? quote2.Bid : quote2.Ask)
        }
    }

    return convertPrice;
}

export function getEstMargin(symbol: SymbolContent, lotSize: number, currentPrice: number, accountLeverage: number, convertPrice: number){
    switch(symbol.CalcMode) {
        case CalcMode.FOREX:
            var margin = lotSize * symbol.ContractSize / accountLeverage * symbol.Percentage * convertPrice ;
            return margin;
        case CalcMode.CFD:
            var margin = lotSize * symbol.ContractSize * currentPrice * symbol.Percentage;
            return margin;
        case CalcMode.FUTURES:
            var margin = lotSize * symbol.InitialMargin * symbol.Percentage;
            return margin;
        case CalcMode.CFD_INDEX:
            var margin = lotSize * symbol.ContractSize * currentPrice / symbol.TickSize * symbol.TickValue * symbol.Percentage;
            return margin;
        case CalcMode.CFD_LEVERAGE:
            var margin = lotSize * symbol.ContractSize * currentPrice / accountLeverage * symbol.Percentage;
            return margin;
        default:
            return 0;
    }
}

const contractItemComponent = (contractItemStyle: ViewStyle, contractItemTitleStyle: TextStyle, title: string, value: string, textColor?: ColorValue) => {
    return (
        <View style={contractItemStyle}>
            <Text style={[contractItemTitleStyle]}>
                {title}
            </Text>
            <DashedLineView />
            <Text style={[contractItemTitleStyle, textColor ? {color: textColor} : {}]}>
                {value}
            </Text>
        </View>
    )
}

export function getRejectedOrderComponent(contractItemStyle: ViewStyle, contractItemTitleStyle: TextStyle, contractInfos: {title: string, value: string, textColor?: ColorValue}[]){
    return (
        <View>
            {/* {
                contractInfos.map((item, index) => {
                    return contractItemComponent(contractItemStyle, contractItemTitleStyle, item.title, item.value, item.textColor)
                })
            } */}
        </View>
    )
}