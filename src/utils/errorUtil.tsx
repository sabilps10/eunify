export enum errorType{
    NORMAL = 0,
    NETWORK = 1,
    AUTH = 2,
    LOCK = 3,
}

export enum TradingResponseCode {
    RET_OK = 0,
    RET_CUST_OK = 200,
    RET_TIMEOUT = 999,
    RET_NETWORK = 1000
}