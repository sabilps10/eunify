import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Auth } from 'aws-amplify';

import { AccountActionCreators } from '../redux/account/actionCreator';
import { StateActionCreators } from '../redux/state/actionCreator';
import { changeUserNickname, loginLevel, refreshToken, registerDemo, switchAccount, updateProfileMobile, updateProfilePassword } from './api/userApi';
import { WebsocketUtils } from './websocketUtils';
import { TradingAccount } from '../redux/account/type';

import { useEffect, useState } from 'react';
import { State } from '../redux/root-reducer';
import { OpenPositionsActionCreators } from '../redux/trading/actionCreator';
import { useTranslation } from 'react-i18next';
import { errorType } from './errorUtil';
import ReactNativeBiometrics from 'react-native-biometrics';
import { ConnectionActionCreators } from '../redux/connection/actionCreator';
import { setChartURL, setTradingURL } from './api/apiSetter';
import { CognitoActionCreators } from '../redux/cognito/actionCreator';
import { Status } from '../redux/connection/type';
import { store } from '../redux/store';
import { actionTimeoutMilliseconds } from '../config/constants';
import { AccountOpeningCreators } from '../redux/accountOpening/actionCreator';
import { getIndividual, setUserLanguage } from './api/apiAccountOpening';

export interface LoginLevelReturn {
    success: boolean,
    paths: string,
    accessToken: string,
    loginLevel: number,
    redirect?: string,
    redirectParam?: {}
    payload?: any
} 

enum accountStatus{
    INCOMPELTE = '0',
    COMPLETED = '1',
    ERROR = '2'
}

export const AccountUtils = () => {
    const [t, i18n] = useTranslation();
    const dispatch = useDispatch();

    const isEnableBiometrics = useSelector((state: State) => state.state.setting?.IsEnableBiometrics);
    const isSkipRegBiometric = useSelector((state: State) => state.state.setting?.IsSkipRegBiometric ?? false);
    const tradingAccountList = useSelector((state: State) => state.account.tradingAccountList);
    const stateLoginLevel = useSelector((state: State) => state.account.account?.LoginLevel ?? 1);
    const access_token = useSelector((state: State) => state.account.account && state.account.account.AccessToken);
    const lang = useSelector((state: State) => state.state.setting.UserLanguage);

    const { SetConnectionStatus } = bindActionCreators(ConnectionActionCreators, dispatch);
    const { SetAccountDisplayName, SetOpenPositions, SetOrders, SetHistoryDeal, SetSymbolInfo, SetPriceQuoteInfo, SetRecommendList, SetHistoryTransaction, SetAccountDetail } = bindActionCreators(OpenPositionsActionCreators, dispatch)
    const { SetAccountInfo, SetIsDemoAccount, SetTradingAccountListInfo, SetDemoTradingAccountInfo, SetConnectedAccount, SetIsLogined, SetMIO } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetIsShowLoading, SetUserId, SetIsShowMessageDialog, SetGA4UserPropertyUUID } = bindActionCreators(StateActionCreators, dispatch);
    const { SetIndividual,SetResetDialogDeposit } = bindActionCreators(AccountOpeningCreators,dispatch)
    const { SetCurrentToken } = bindActionCreators(CognitoActionCreators, dispatch);

    const { startWebsocket, getClosestPath, closeWebsocket } = WebsocketUtils();

    const doLogin = async(pushyDeviceID: string, region: string, isRefresh: boolean, skipPhoneVerify?: boolean, forceBiometric?: boolean) => {
        var isSkipPhoneVerify = skipPhoneVerify !== undefined && skipPhoneVerify === true;
        
        var loginLevelReturn: LoginLevelReturn = {
            success: false,
            paths: undefined,
            accessToken: undefined,
            loginLevel: 0,
            payload: undefined
        }
        const session = await Auth.currentSession();
        var token = session.getAccessToken().getJwtToken();
        // var token = session.getRefreshToken().getToken();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        // console.debug('doLogin payload', payload)
        var email_verified = payload['email_verified'];
        var phone_number_verified = payload['phone_number_verified'];
        var phone_number = payload['phone_number'];

        if (!email_verified) {
            return loginLevelReturn
        }

        var cognitoUserName = payload['cognito:username'];
        // console.debug('loginLevel API isRefresh', isRefresh)
        var data = await loginLevel(cognitoUserName, token, pushyDeviceID, region, isRefresh);
        if (data) {
            // console.debug(JSON.stringify(data))
            SetCurrentToken(token);
            if (!isSkipPhoneVerify && (phone_number_verified === undefined || !phone_number_verified || phone_number === undefined)) {
                loginLevelReturn = {
                    ...loginLevelReturn,
                    accessToken: token,
                    redirect: 'UserVerification',
                    redirectParam: {type: 'phone_number'}
                }
                return loginLevelReturn
            }

            if (data.errorCode !== 0) {
                if (data.errorCode === errorType.AUTH) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.auth'), message: t('common.error.code') + data.errorCode})
                    return loginLevelReturn
                } else if (data.errorCode === errorType.NETWORK) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data.errorCode})
                    return loginLevelReturn
                } else if (data.errorCode === errorType.LOCK) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.lockTitle'), message: t('common.error.lock')})
                    return loginLevelReturn
                }
                
                return loginLevelReturn
            }

            if (!data.approvedDevice) {
                loginLevelReturn = {
                    ...loginLevelReturn,
                    success: true,
                    redirect: 'AddDevices'
                }
                return loginLevelReturn
            }

            // console.debug(`data: ${JSON.stringify(data)}`);

            await getIndividual({
                token,
                payload: {
                    username: cognitoUserName
                },
                onSuccess: (data) =>{
                    SetIndividual(data.response)
                },
                onFailed: () =>{}
            })

            
            await setUserLanguage({
                id: cognitoUserName,
                lang,
                token,
                onSuccess: (data) => {},
                onFailed: () => {}
            })

            SetAccountInfo({
                DisplayName: data.nickName,
                DisplayHeaderName: data.displayHeaderName
            })
            SetAccountDisplayName(data.currentTrader);
            SetConnectedAccount(data.currentTrader)
            SetUserId(data.nickName)
            SetGA4UserPropertyUUID(data.aid)
            var mtAccount = data.mtAccount;
            var mtDemoAccount = data.mtDemoAccount;
            
            var mtAccountList: TradingAccount[] = [];
            mtAccount.forEach(element => {
                var acc : TradingAccount = {
                Name: element.mt_name,
                Account: element.mt_account,
                Entity: element.mt_entity,
                isDemo: false,
                isDefault: element.is_default === '1',
                userId: element.user_id,
                countryCode: element.country_code
                }
                mtAccountList.push(acc)
            });
            SetTradingAccountListInfo(mtAccountList);

            if (mtDemoAccount && mtDemoAccount[0]) {
                var acc : TradingAccount = {
                    Name: mtDemoAccount[0].mt_name,
                    Account: mtDemoAccount[0].mt_account,
                    Entity: mtDemoAccount[0].mt_entity,
                    isDemo: true,
                    isDefault: mtDemoAccount[0].mt_account.toString() === data.currentTrader.toString(),
                    userId: '',
                    countryCode: ''
                }
                SetDemoTradingAccountInfo(acc);
            }
            SetIsDemoAccount(false)
            setTradingURL(data.tApiPath);
            setChartURL(data.chartPath, data.chartWSPath);
            SetIsLogined(true)

            if (data.mio) {
                SetMIO(data.mio)
            }

            const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })
            const { available, biometryType }  = await rnBiometrics.isSensorAvailable();

            // console.debug('rnBiometrics available', available, biometryType)
            // Alert.alert('forceBiometric ' + forceBiometric)
            if ((isEnableBiometrics || isSkipRegBiometric) && !forceBiometric) {
                loginLevelReturn = {
                    success: true,
                    redirect: 'TabDirectory',
                    paths: data.mtPaths,
                    accessToken: data.accessToken, 
                    loginLevel: data.loginLevel,
                    payload: data

                }
            } else {
                loginLevelReturn = {
                    success: true,
                    redirect: 'EnableBiometrics',
                    redirectParam: {isSetting: false},
                    paths: data.mtPaths,
                    accessToken: data.accessToken, 
                    loginLevel: data.loginLevel,
                    payload: data
                }
            }

            // console.debug("doLogin: " + JSON.stringify(loginLevelReturn))
            
            return loginLevelReturn;
        }
    }


    const doConnectWebsocket = async(path: string, requireReset: boolean, accessToken: string, loginLevel: number, isDemo: boolean, isReconnect: boolean = false) => {
        try {
            var wsPath = path;
            //console.log(`doConnectWebsocket Path: ${path}, isDemo: ${isDemo}, requireReset: ${requireReset}`)
            if (requireReset) {
                var mtPaths = JSON.parse(path);
                wsPath = await getClosestPath(mtPaths)
                SetOpenPositions([])
                SetOrders([])
                SetHistoryDeal([])
            }
            const connect = async(wsPath: string, accessToken: string, loginLevel: number, isDemo: boolean, isReconnect: boolean) =>
            {
                if (!isReconnect)
                    SetIsShowLoading(true)
                try
                {

                    const timeoutPromise = new Promise((resolve) => {
                        setTimeout(resolve, actionTimeoutMilliseconds, 'timeout');
                    });

                    const status = await Promise.race([startWebsocket(wsPath, accessToken, loginLevel, isDemo), timeoutPromise]).then((value) => { 
                        if (value === "timeout")
                        {
                            console.debug("startWebSocket timeout")
                            return 500
                        }
                        return value;
                    });
    
                    console.debug("connect status: " + status)
                    if (!isReconnect)
                    {
                        if (status === 401 || status === 403)
                        {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('cognitoLogin.error.loginFailed'), message: ''})
                        }
                        else if (status === 500)
                        {
                            SetIsShowMessageDialog({isShowDialog: true, title: t('cognitoLogin.error.serverError'), message: t('cognitoLogin.error.serverErrorContent'), callback: async()=>{await connect(wsPath, accessToken, loginLevel, isDemo, false)}, isShowCancel: true, cancelCallback: async() => {await resumeConnection()} })
                        }
                    }
                    return status;
                }
                finally
                {
                    if (!isReconnect)
                    {
                        //console.log("set is showing false")
                        SetIsShowLoading(false)
                    }
                        
                }
            }
            
            return await connect(wsPath, accessToken, loginLevel, isDemo, isReconnect);
        } catch (err) {
            console.error(err)
        }
    }

    const doCloseWebsocket = async(manual: boolean) => {
        closeWebsocket(manual)
    }

    const doSwitchAccount = async (tradeAccount: string, isDemo: boolean, entity: string)=> {
        var loginLevelReturn: LoginLevelReturn = {
            success: false,
            paths: undefined,
            accessToken: undefined,
            loginLevel: 0,
            payload: undefined
        }

        const session = await Auth.currentSession();
        var token = session.getAccessToken().getJwtToken();
        var payload: { [key: string]: any; } = session.getIdToken().payload;
        var cognitoUserName = payload['cognito:username'];
        const data = await switchAccount(cognitoUserName, token, tradeAccount, isDemo ? '0' : '1', entity);
        if (data) {

            if (data.errorCode === errorType.NETWORK) {
                SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data.errorCode})
                return;
            }
            SetOpenPositions([])
            SetOrders([])
            SetHistoryDeal([])
            SetSymbolInfo([])
            SetRecommendList([])
            
            loginLevelReturn = {
                success: true,
                paths: data.mtPaths,
                accessToken: data.accessToken, 
                loginLevel: stateLoginLevel,
                payload: data
            }
        }

        return loginLevelReturn;
    }

    const resumeConnection = async () => {

        const state: State = store.getState();
        const tradingAccount = state.account.isDemoAccount ? state.account.demoTradingAccount : state.account.tradingAccountList[0];
        const region = state.state.setting?.UserRegion
        const setting = state.state.setting
        const stateLoginLevel = state.account.account?.LoginLevel ?? 1

        if (stateLoginLevel === 3) {
            //console.log("onPressExitDemo stateLoginLevel: " + stateLoginLevel + " " + tradingAccount.Account)
            var data = await doSwitchAccount(tradingAccount.Account, tradingAccount.isDemo, tradingAccount.Entity)
            if (data && data.success) {
                await doCloseWebsocket(true);

                const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, tradingAccount.isDemo);
                if (status === 101)
                {
                    SetAccountDisplayName(data.payload.currentTrader);
                    setTradingURL(data.payload.tApiPath);
                    setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                }
                    
            }
        } else {
            var data = await doLogin(setting.PushyDeviceID ?? "", region, true)
            if (data && data.success) {
                await doCloseWebsocket(true);
                const status = await doConnectWebsocket(data.paths, true, data.accessToken, data.loginLevel, false);
                if (status === 101)
                {
                    SetAccountDisplayName(data.payload.currentTrader);
                    setTradingURL(data.payload.tApiPath);
                    setChartURL(data.payload.chartPath, data.payload.chartWSPath);
                }
            }
        }
    }

    return {
        doLogin,
        doConnectWebsocket,
        doCloseWebsocket,
        doRefreshToken: async(tradeAccount: string) => {
            var loginLevelReturn: LoginLevelReturn = {
                success: false,
                paths: undefined,
                accessToken: undefined,
                loginLevel: 0,
                payload: undefined
            }

            // var tradeAccount = connectedTradeAccount;
            var entityId = '';
            if (tradeAccount) {
                tradingAccountList.forEach(element => {
                    // console.debug(element)
                    if (element.Account === tradeAccount.toString()) {
                        entityId = element.Entity
                    }
                });
            }

            // console.debug('accountUtils refreshToken ', tradeAccount, entityId)
            
            var data = await refreshToken(tradeAccount.toString(), entityId);
            if (data) {
                // console.debug(`data: ${JSON.stringify(data)}`);
                 
                if (data.errorCode === errorType.NETWORK) {
                    SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data.errorCode})
                    return loginLevelReturn
                }

                const rnBiometrics = new ReactNativeBiometrics({ allowDeviceCredentials: false })
                const { available, biometryType }  = await rnBiometrics.isSensorAvailable();

                // console.debug('rnBiometrics available', available, biometryType)

                if (isEnableBiometrics) {
                    loginLevelReturn = {
                        success: true,
                        redirect: 'TabDirectory',
                        paths: data.mtPaths,
                        accessToken: data.accessToken, 
                        loginLevel: data.loginLevel,
                        payload: data
                    }
                } else {
                    loginLevelReturn = {
                        success: true,
                        redirect: 'EnableBiometrics',
                        redirectParam: {isSetting: false},
                        paths: data.mtPaths,
                        accessToken: data.accessToken, 
                        loginLevel: data.loginLevel,
                        payload: data
                    }
                }

                return loginLevelReturn;
            }
        },
        doSwitchAccount,
        registerDemo: async (region: string)=> {
            const session = await Auth.currentSession();
            var token = session.getAccessToken().getJwtToken();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var cognitoUserName = payload['cognito:username'];
            // console.debug('registerDemo access_token', access_token)
            const data = await registerDemo(cognitoUserName, token, region, access_token);
            if (data) {
                let acc : TradingAccount = {
                    Name: data.tradeAccount,
                    Account: data.tradeAccount,
                    Entity: data.entity,
                    isDemo: true,
                    isDefault: false
                }
                SetDemoTradingAccountInfo(acc);
                return acc
            }
            return undefined
        },
        changeUserNickname: async (nickname: string)=> {
            const session = await Auth.currentSession();
            var token = session.getAccessToken().getJwtToken();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var cognitoUserName = payload['cognito:username'];
            const data = await changeUserNickname(cognitoUserName, token, nickname);
            if (data) {
                SetAccountInfo({
                    DisplayName: nickname
                })
                return data;
            }
            return undefined;
        },
        updateProfileMobile: async (phone: string, phonePrefix: string)=> {
            const session = await Auth.currentSession();
            var token = session.getAccessToken().getJwtToken();
            var payload: { [key: string]: any; } = session.getIdToken().payload;
            var cognitoUserName = payload['cognito:username'];
            const data = await updateProfileMobile(cognitoUserName, phone, phonePrefix);
            if (data) {
                return data;
            }
            return undefined;
        },
        updateProfilePassword: async (password: string, username?: string)=> {
            var cognitoUserName: string = undefined;
            try {
                const session = await Auth.currentSession();
                var token = session.getAccessToken().getJwtToken();
                var payload: { [key: string]: any; } = session.getIdToken().payload;
                cognitoUserName = payload['cognito:username'];
            } catch (e) {}

            const data = await updateProfilePassword(cognitoUserName, password, username);
            if (data) {
                return data;
            }
            
            return undefined;
        },
        resetUser: async () => {
            SetOpenPositions([])
            SetOrders([])
            SetHistoryDeal([])
            SetSymbolInfo([])
            //SetPriceQuoteInfo([])
            SetRecommendList([])
            SetHistoryTransaction([])
        },
        signOut: async () => {
            try {
                await Auth.signOut()
            } 
            catch (e) {}
            finally {
                SetAccountInfo({
                    AccessToken: undefined,
                    Path: undefined,
                    LoginLevel: 1,
                    DisplayName: undefined,
                    MobileNumber: undefined
                })
                SetOpenPositions([])
                SetOrders([])
                SetHistoryDeal([])
                SetIndividual(null)
                SetResetDialogDeposit({resetDialogDeposit: false})
                SetTradingAccountListInfo(null)
                SetDemoTradingAccountInfo(null)
                SetIsDemoAccount(false)
                SetAccountDetail(null)
                console.debug("Disconnect here")
                SetConnectionStatus(Status.Disconneted)
                SetIsLogined(false)

            }
        }
    };
};