import React from 'react';

export const navigationRef: any = React.createRef();

export const navigate = (routeName: string, params: any) => {
  navigationRef.current?.navigate(routeName, params);
};

export const aoEntry = (individual: any, navigation: any) => {
  if (!individual) {
    navigation.navigate('AccountQuestion');
  } else {
    if (individual?.status === 'REJECTED') {
      navigation.navigate('AccountQuestion');
    } else if (
      individual?.status !== 'REJECTED' &&
      individual?.status !== 'DRAFT'
    ) {
      navigation.navigate('AccountStatus');
    } else {
      if (!individual?.tradeProduct) {
        navigation.navigate('AccountQuestion');
      } else {
        if (
          !individual?.isRecommendedTradingAccountPage &&
          !individual?.isAccountAgreementPage
        ) {
          navigation.navigate('RecommendedTradingAccount');
        }
        if (
          individual?.isRecommendedTradingAccountPage &&
          !individual?.isAccountAgreementPage
        ) {
          navigation.navigate('AccountAgreement');
        }
        if (
          individual?.isRecommendedTradingAccountPage &&
          individual?.isAccountAgreementPage
        ) {
          navigation.navigate('AccountGettingReady');
        }
      }
    }
  }
};
