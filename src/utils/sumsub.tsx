import axios from 'axios';
import {sumsub_url} from '../config/constants';

export const getSumsubToken = async (userId, entity) => {
  const res = await axios({
    url: sumsub_url,
    method: 'POST',
    data: {
      userId,
      kycType: entity === 'EIE' ? 'aoeiehk' : 'aoxpro',
    },
  });
  return res.data;
};
