import React, { useState, useContext, useEffect, useRef } from 'react';
import { Alert } from 'react-native';
import WebSocket, { WebSocketContext } from '../websocket/WebSocket';
import { RequestType } from '../websocket/request';

import { shallowEqual, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AccountActionCreators } from '../redux/account/actionCreator';
import { OpenPositionsActionCreators } from '../redux/trading/actionCreator';
import uuid from 'react-native-uuid';
import { useSelector } from 'react-redux';
import { State } from '../redux/root-reducer';

import { useTranslation } from 'react-i18next';
import { StateActionCreators } from '../redux/state/actionCreator';
import { getRecommendList, getWatchList, replaceWatchList } from './api/tradingApi';
import { store } from '../redux/store';
import { fetchWithTimeout } from './api/apiSetter';
import { TradingResponseCode } from './errorUtil';

export const WebsocketUtils = () => {
    const [t, i18n] = useTranslation();

    const stop = useRef(false);
    const ws = useContext(WebSocketContext);

    // const tradeAccount = useSelector((state: State) => state.account.account?.Login!);
    const login = useSelector((state: State) => state.trading.accountDetail && Number(state.trading.accountDetail.Login));

    const dispatch = useDispatch();
    const { SetAccountInfo, SetIsDemoAccount } = bindActionCreators(AccountActionCreators, dispatch);
    const { SetRecommendList, UpdateOpenPositionsPL } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    const { SetMyList, SetIsShowMessageDialog } = bindActionCreators(StateActionCreators, dispatch);

    async function checkLatency(path: string): Promise<Number> {
        var start = new Date();
        var end = new Date();
        var timeDiff = Number.MAX_VALUE;
        await fetchWithTimeout(path, {}, 2000)
        .then((response) => {
            end = new Date()
            timeDiff = end.getTime() - start.getTime();
        })
        .catch((error) => {
        }).finally(()=>{
            // console.warn('timeDiff', path, timeDiff)
            return timeDiff
        });
        return timeDiff;
    }

    return {
        startWebsocket: async (in_path: string, in_accessToken: string, loginLevel: number, isDemo: boolean) => {
            console.debug(`startWebsocket: ${in_path}, ${in_accessToken}`);
            
            if (!ws) return 500;

            const status = await ws.connect(in_path, in_accessToken);
            if (status === 101) {
                console.debug(`status: 101`);
                SetIsDemoAccount(isDemo)
                SetAccountInfo({
                    Path: in_path,
                    AccessToken: in_accessToken,
                    LoginLevel: loginLevel
                });

                let symbolList: string[] = await ws.getSymbolList({
                    pid: uuid.v4().toString(),
                    action: RequestType.GET_SYMBOL_LIST,
                });
                
                console.debug(`symbolList: ${symbolList}`)

                const response = await getRecommendList(in_accessToken);
                let recommendList: string[] = [];
                if (response.status == 0)
                {
                    recommendList = JSON.parse(response.recommend_list)

                    console.debug(`after recommendList: ${recommendList.length}`);
                    SetRecommendList(recommendList);
                    const state: State = store.getState()
                    const myList = state.state.myList
                    console.debug("myList: " + JSON.stringify(myList))
                    if (!myList)
                    {
                        console.debug("set MyList from recommendList")
                        SetMyList(recommendList);
                    }
                }

                console.debug(`loginLevel: ${loginLevel}`);
                if (loginLevel == 2 || loginLevel == 3) {
                    try {

                        const response = await getWatchList(in_accessToken);
                        if (response.status == 0)
                        {
                            if (!response.watch_list)
                            {
                                await replaceWatchList(in_accessToken, JSON.stringify(recommendList));
                            }
                            else
                            {
                                let watch_list: string[] = JSON.parse(response.watch_list)

                                console.debug(`getWatchList success: ${watch_list.length}`);
    
                                if (watch_list)
                                {
                                    SetMyList(watch_list)
                                }
                            }
                        }
                    }
                    catch (e) {
                        console.debug(`getWatchList failed: ${e}`);
                    }
                }

                if (loginLevel === 3 || isDemo) {
                    try {
                        await ws.getPositions({
                            pid: uuid.v4().toString(),
                            action: RequestType.GET_POSITIONS
                        });
                        console.debug("getPosition success");
                    }
                    catch (e) {
                        console.debug("getPosition failed");
                    }

                    try {
                        await ws.getOrders({
                            pid: uuid.v4().toString(),
                            action: RequestType.GET_ORDERS
                        });
                        console.debug("getorder success");
                    }
                    catch (e) {
                        console.debug("getPosition failed");
                    }
                }

                try {
                    ws.subscribeSymbols({
                        pid: uuid.v4().toString(),
                        action: RequestType.SUBSCRIBE_SYMBOLS,
                        data: { Symbols: symbolList }
                    });
                    console.debug(`Subscribe WatchList success: ${symbolList.length}`);
                }
                catch (e) {
                    console.debug("Subscribe WatchList failed");
                }

                console.debug("after all");
            }

            console.debug(`end startWebsocket: ${in_path}, ${in_accessToken}`);

            return status;
        },
        closeWebsocket: async (manual: boolean) => {
            await ws.closeWebsocket(manual);
        },
        startNewOrder: async (contractCode: string, type: number, volume: number, orderPrice: number, tp: number, sl: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.SUBMIT_MARKET_ORDER,
                    data: {
                        Login: Number(login),
                        UserRequestID: login.toString(),
                        Symbol: contractCode,
                        Type: type,
                        Volume: volume,
                        OrderPrice: orderPrice,
                        SL: sl,
                        TP: tp,
                        Dev: 0,
                        Comment: ''
                    }
                }
                try
                {
                    const result = await ws.submitMarketOrder(payload)
                    console.debug('startNewOrder result', result)

                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.orderFailure'), message: ''})
                    return {RtnCode: -1}
                }
            }
            return undefined;
        },
        startNewPendingOrder: async (contractCode: string, type: number, volume: number, orderPrice: number, tp: number, sl: number, typeTime: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.SUBMIT_NEW_PENDING_ORDER,
                    data: {
                        Login: login,
                        UserRequestID: login.toString(),
                        Symbol: contractCode,
                        Type: type,
                        Volume: volume,
                        OrderPrice: orderPrice,
                        SL: sl,
                        TP: tp,
                        Dev: 0,
                        TypeTime: typeTime, //0:GTC, 1:today
                        Comment: ''
                    }
                }
                try
                {
                    const result = await ws.submitPendingOrder(payload)
                    console.debug('startNewPendingOrder result', result)
                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.orderFailure'), message: ''})
                    return {RtnCode: -1}
                }
            }
            return undefined;
        },
        startDeletePendingOrder: async (ticket: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.SUBMIT_DELETE_PENDING_ORDER,
                    data: {
                        Login: login,
                        Ticket: ticket,
                        Type: 'P',
                    }
                }
                try
                {
                    const result = await ws.submitDeletePendingOrder(payload)
                    console.debug('startDeletePendingOrder result', result)
                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.cancelOrderFailure'), message: ''})
                    return {RtnCode: -1}
                }
            }
            return undefined;
        },
        startLiqOrder: async (contractCode: string, type: number, volume: number, orderPrice: number, transactionId: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.LIQ_POSITION,
                    data: {
                        Login: login,
                        UserRequestID: login.toString(),
                        Symbol: contractCode,
                        Type: type,
                        Volume: volume,
                        OrderPrice: orderPrice,
                        SL: 0,
                        TP: 0,
                        Dev: 0,
                        PositionID: transactionId,
                        Comment: ''
                    }
                }
                try
                {
                    const result = await ws.submitMarketOrder(payload)
                    console.debug('startLiqOrder result', result)
                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.closePositionFailure'), message: ''})
                    return {RtnCode: -1}
                }

            }
            return undefined;
        },
        startEditPosition: async (ticket: number, tp: number, sl: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.MODIFY_OPEN_POSITION,
                    data: {
                        Login: login,
                        Ticket: ticket,
                        Type: 'P',
                        SL: sl,
                        TP: tp
                    }
                }
                try
                {
                    const result = await ws.submitEditPosition(payload)
                    console.debug('startEditPosition result', result)
                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.editPositionFailure'), message: ''})
                    return {RtnCode: -1}
                }
            }
            return undefined;
        },
        startEditOrder: async (ticket: number, price: number, tp: number, sl: number) => {
            if (ws !== null && login) {
                var payload = {
                    pid: uuid.v4().toString(),
                    action: RequestType.MODIFY_PENDING_ORDER,
                    data: {
                        MsgSeq: '1',
                        Login: login,
                        Ticket: ticket,
                        Type: 'O',
                        SL: sl,
                        TP: tp,
                        OrderPrice: price
                    }
                }
                try
                {
                    const result = await ws.submitEditOrder(payload)
                    console.debug('startEditOrder result', result)
                    return result;
                }
                catch(error)
                {
                    if (error === "Timeout")
                        return {RtnCode: TradingResponseCode.RET_TIMEOUT}
                    else if (error === "Network")
                        return {RtnCode: TradingResponseCode.RET_NETWORK}
                    // SetIsShowMessageDialog({isShowDialog: true, title: t('trade.error.modifyOrderFailure'), message: ''})
                    return {RtnCode: -1}
                }
            }
            return undefined;
        },
        subscribeSymbolDetail: async (symbol: string) => {
            try
            {
                if (ws !== null) {
                    var payload = {
                        pid: uuid.v4().toString(),
                        action: RequestType.SUBSCRIBE_SYMBOL_DETAIL,
                        data: {
                            Symbol: symbol
                        }
                    }
                    return await ws.subscribeSymbolDetail(payload)
                }
            }
            catch(error)
            {
                console.debug(`subscribeSymbolDetail: ${error}`);
            }
            return false;
        },
        unsubscribeSymbolDetail: async (symbol: string) => {
            try
            {
                if (ws !== null) {
                    var payload = {
                        pid: uuid.v4().toString(),
                        action: RequestType.UNSUBSCRIBE_SYMBOL_DETAIL,
                        data: {
                            Symbol: symbol
                        }
                    }
                    return await ws.unsubscribeSymbolDetail(payload)
                }
            }
            catch(error)
            {
                console.debug(`unsubscribeSymbolDetail: ${error}`);
            }
            return false;
        },
        getClosestPath: async (paths: JSON) => {
            // console.warn('Object.keys(paths).length', Object.keys(paths).length)
            var path = paths[0].toString();
            var timeDiff = Number.MAX_VALUE;
            for (var i = 0 ; i < Object.keys(paths).length ; i++) {
                var pathTimeDiff = await checkLatency(paths[i])
                if (pathTimeDiff < timeDiff){
                    // console.warn('pathTimeDiff', paths[i], pathTimeDiff, timeDiff)
                    timeDiff = pathTimeDiff;
                    path = paths[i].toString()
                }
            }
            
            return path
        },
        stopProcess: () => {
            stop.current = true;
        }
    };
};
