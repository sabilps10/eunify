/** @type {import('tailwindcss').Config} */

module.exports = {
  darkMode: ['class', '[data-mode="dark"]'],
  content: [],
  theme: {
    extend: {
      fontFamily:{
        'montserrat': ['Montserrat'],
      },
      colors: {
        'brand3': '#00B1D2',
        'brand2': '#CFA872',
        'brand1': '#FED661',
      }
    },
  },
  plugins: [
  ],
}
