import React, { useState, createContext, useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../redux/root-reducer';

import { bindActionCreators } from 'redux';
import { AccountActionCreators } from '../redux/account/actionCreator';
import { ConnectionActionCreators } from '../redux/connection/actionCreator';
import { OpenPositionsActionCreators } from '../redux/trading/actionCreator';
import { ConnectionState, Status } from '../redux/connection/type';
import { SubscribeSymbolsRequest, SubmitMarketOrderRequest, SubmitPendingOrderRequest, SubmitDeletePendingOrderRequest, GetPositionsRequest, GetOrdersRequest, SubmitEditPositionRequest, SubmitEditOrderRequest, SubscribeSymbolDetailRequest, GetSymbolListRequest, RequestType } from './request';
import { store } from '../redux/store';
import { batch } from 'react-redux'
import { Alert } from 'react-native';
import { useAppState } from '@react-native-community/hooks';
import { actionTimeoutMilliseconds } from '../config/constants';

interface WebSocketContextType {
    needInsertQueue: boolean,
    isConnected: boolean;
    sendMessage: (message: string) => void;
    connect: (path: string, access_token: string) => Promise<any>;
    closeWebsocket: (manual: boolean) => Promise<any>;
    subscribeSymbols: (request: SubscribeSymbolsRequest) => Promise<any>;
    submitMarketOrder: (request: SubmitMarketOrderRequest) => Promise<any>;
    submitPendingOrder: (request: SubmitPendingOrderRequest) => Promise<any>;
    submitDeletePendingOrder: (request: SubmitDeletePendingOrderRequest) => Promise<any>;
    submitEditPosition: (request: SubmitEditPositionRequest) => Promise<any>;
    submitEditOrder: (request: SubmitEditOrderRequest) => Promise<any>;
    getSymbolList: (request: GetSymbolListRequest) => Promise<any>;
    getPositions: (request: GetPositionsRequest) => Promise<any>;
    getOrders: (request: GetOrdersRequest) => Promise<any>;
    subscribeSymbolDetail: (request: SubscribeSymbolDetailRequest) => Promise<any>;
    unsubscribeSymbolDetail: (request: SubscribeSymbolDetailRequest) => Promise<any>;
}

const WebSocketContext = createContext<WebSocketContextType | null>(null);

export { WebSocketContext }

interface RequestTasks {
    [pid: string]: RequestObj;
}

class RequestObj {
    resolve: any;
    reject: any;
}

let websocket: WebSocket;
let isConnected = false;
let priceTickQueue: any[] = [];
let tasks: RequestTasks = {};
let needInsertQueue = true;
let connectionStatus: Status;

const WebSocketProvider: React.FC<React.ReactNode> = ({ children }) => {

    console.debug(`WebSocketProvider ${isConnected}`);

    const dispatch = useDispatch();

    const { SetConnectionStatus } = bindActionCreators(ConnectionActionCreators, dispatch);
    const { SetOpenPositions, UpdateOpenPositionsPL, UpdateAccountDetailInit, AddOpenPositions, DeleteOpenPositions, SetSymbolInfo, SetPriceQuoteInfo, SetInitPriceQuoteInfo, SetOrders, AddOrders, DeleteOrders, SetAccountDetail, SetHistoryDeal, SetTraderSentiment, SetLiveStat, SetSymbolsUpdate, SetManualApprovalOrderRejecteUpdate } = bindActionCreators(OpenPositionsActionCreators, dispatch);
    
    const appState = useAppState();
    const [subscribeSymbolsRequest, setSubscribeSymbolsRequest] = useState<SubscribeSymbolsRequest>();

    useEffect(()=> {
        if (appState === 'background') {
            needInsertQueue = false;
            var unsubscribeSymbolsRequest = subscribeSymbolsRequest;
            unsubscribeSymbolsRequest = {
                ...unsubscribeSymbolsRequest,
                action: RequestType.UNSUBSCRIBE_SYMBOLS
            }
            new Promise<any>((resolve, reject) => {
                try {
                    sendMessage(JSON.stringify(unsubscribeSymbolsRequest));
                    resolve(true);
                    console.debug('unsubscribe ok')
                }
                catch (error) {
                    reject(false);
                    console.debug('unsubscribe fail', error.toString())
                }
            });
        } else if (appState === 'active') {
            needInsertQueue = true;
            subscribeSymbols(subscribeSymbolsRequest);
        }
    }, [appState])

    const subscribeSymbols = async (request: SubscribeSymbolsRequest) => {
        setSubscribeSymbolsRequest(request)
        const requestObj = tasks[request.pid] = new RequestObj();

        return await new Promise<any>((resolve, reject) => {
            try {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                sendMessage(JSON.stringify(request));
            }
            catch (error) {
                reject(false);
            }
        });
    }

    const submitMarketOrder = async (request: SubmitMarketOrderRequest): Promise<any> => {
        console.debug(`submitMarketOrder: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {

                try
                {
                    requestObj.resolve = resolve;
                    requestObj.reject = reject;
                    if (!isConnected)
                    {
                        reject('Network');
                        return;
                    }
                    sendMessage(JSON.stringify(request));

                    setTimeout(() => {
                        console.debug("submitMarketOrder Timeout");
                        reject('Timeout');
                    }, actionTimeoutMilliseconds);
                }
                catch(err)
                {
                    reject(new Error(err))
                }
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const submitPendingOrder = async (request: SubmitPendingOrderRequest): Promise<any> => {
        console.debug(`submitPendingOrder: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                if (!isConnected)
                {
                    reject('Network');
                    return;
                }
                sendMessage(JSON.stringify(request));

                setTimeout(() => {
                    console.debug("submitPendingOrder Timeout");
                    reject('Timeout');
                }, actionTimeoutMilliseconds);
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const submitDeletePendingOrder = async (request: SubmitDeletePendingOrderRequest): Promise<any> => {
        console.debug(`submitDeletePendingOrder: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                if (!isConnected)
                {
                    reject('Network');
                    return;
                }
                sendMessage(JSON.stringify(request));

                setTimeout(() => {
                    console.debug("submitDeletePendingOrder Timeout");
                    reject('Timeout');
                }, actionTimeoutMilliseconds);
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const submitEditPosition = async (request: SubmitEditPositionRequest): Promise<any> => {
        console.debug(`submitEditPosition: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                if (!isConnected)
                {
                    reject('Network');
                    return;
                }
                sendMessage(JSON.stringify(request));

                setTimeout(() => {
                    console.debug("submitEditPosition Timeout");
                    reject('Timeout');
                }, actionTimeoutMilliseconds);
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const submitEditOrder = async (request: SubmitEditOrderRequest): Promise<any> => {
        console.debug(`submitEditOrder: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                if (!isConnected)
                {
                    reject('Network');
                    return;
                }
                sendMessage(JSON.stringify(request));

                setTimeout(() => {
                    console.debug("submitEditOrder Timeout");
                    reject('Timeout');
                }, actionTimeoutMilliseconds);
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const getSymbolList = async (request: GetSymbolListRequest): Promise<any> => {
        console.debug(`getSymbolList: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                try
                {
                    requestObj.resolve = resolve;
                    requestObj.reject = reject;
                    sendMessage(JSON.stringify(request));
                }
                catch(err)
                {
                    reject(new Error(err))
                }
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const getPositions = async (request: GetPositionsRequest): Promise<any> => {
        console.debug(`getPositions: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                try
                {
                    requestObj.resolve = resolve;
                    requestObj.reject = reject;
                    sendMessage(JSON.stringify(request));
                }
                catch(err)
                {
                    reject(new Error(err))
                }
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const getOrders = async (request: GetOrdersRequest): Promise<any> => {
        console.debug(`getOrders: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                try
                {
                    requestObj.resolve = resolve;
                    requestObj.reject = reject;
                    sendMessage(JSON.stringify(request));
                }
                catch(err)
                {
                    reject(new Error(err))
                }
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const subscribeSymbolDetail = async (request: SubscribeSymbolDetailRequest): Promise<any> => {
        console.debug(`subscribeSymbolDetail: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                sendMessage(JSON.stringify(request));
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const unsubscribeSymbolDetail = async (request: SubscribeSymbolDetailRequest): Promise<any> => {
        console.debug(`unsubscribeSymbolDetail: ${JSON.stringify(request)}`)
        const requestObj = tasks[request.pid] = new RequestObj();
        try {
            const result = await new Promise<any>((resolve, reject) => {
                requestObj.resolve = resolve;
                requestObj.reject = reject;
                sendMessage(JSON.stringify(request));
            });
            return result;
        }
        finally {
            delete tasks[request.pid];
        }
    }

    const connect = async (path: string, access_token: string): Promise<any> => {
        const loginRequest = new RequestObj();
        console.debug("connect start: " + needInsertQueue);

        try {
            return await new Promise<any>((resolve, reject) => {
                loginRequest.resolve = resolve;
                loginRequest.reject = reject;
                webSocketConnect(path, access_token, loginRequest);
            });
        }
        catch (error) {
            console.debug(`result: ${error}`)
            return error;
        }
    }

    const closeWebsocket = async (manual: boolean): Promise<any> => {
        console.debug("closeWebsocket: " + manual)
        if (manual) {
            connectionStatus = Status.ManualDisconnected;
            SetConnectionStatus(Status.ManualDisconnected);
        } else if (connectionStatus === Status.ManualDisconnected) {
            SetConnectionStatus(Status.Disconneted);
        }
        
        if (websocket)
            websocket.close();
    }

    const sendMessage = (message: string) => {
        if (websocket != null) {
            websocket.send(message);
        }
    }

    let interval;

    const webSocketConnect = (path: string, access_token: string, loginRequest: RequestObj) => {
        let ws_url = path;
        console.debug(`websocket connect: ${ws_url}`);
        if (websocket)
        {            
            // console.warn("webSocketConnect readyState: " + websocket.readyState);
            websocket.close();
        }

        if (interval)
        {
            clearInterval(interval);
        }
            
        websocket = new WebSocket(ws_url, [], { headers: {
            "Authorization": `Bearer ${access_token}`
        }})

        websocket.onopen = () => {
            console.debug("websocket open");
            interval = setInterval(() => {
            
                if (priceTickQueue.length > 0) {
                    batch(() => {
                        priceTickQueue.forEach(() => {
                            let message = priceTickQueue.shift();
                            if (message.data)
                            {
                                SetPriceQuoteInfo(message.data.Symbol, message.data);
                                UpdateOpenPositionsPL(message.data.Symbol, message.data);
                            }
                            
                        });
                    });
                }
            }, 100);
            isConnected = true;
            connectionStatus = Status.Connected;
            SetConnectionStatus(Status.Connected);
            if (loginRequest != null) {
                console.debug("loginRequest resolved");
                loginRequest.resolve(101);
            }
        };

        websocket.onerror = (e: WebSocketErrorEvent) => {
            console.debug("websocket error", JSON.stringify(e));
            if (interval)
                clearInterval(interval);
            if (e.message && loginRequest) {
                console.debug("loginRequest reject");
                if (e.message.includes("403"))
                    loginRequest.reject(403);
                else if (e.message.includes("401"))
                    loginRequest.reject(401);
                else
                    loginRequest.reject(500);
            }

        };

        websocket.onclose = async (e) => {
            // console.warn(`websocket closed Status: ${connectionStatus}`);
            if (interval)
                clearInterval(interval);
            isConnected = false;
            if (connectionStatus !== Status.ManualDisconnected)
            {
                connectionStatus = Status.Disconneted;
                SetConnectionStatus(Status.Disconneted);
            }
        };



        websocket.onmessage = async (e) => {
            //console.debug(e.data);
            var message = JSON.parse(e.data);
            if (message.actionAck === "symbols_ack") {
                //console.debug(`symbols_ack: ${JSON.stringify(message.data)}`);
                SetSymbolInfo(message.data);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data.flatMap((symbol) => symbol.SymbolName));
            }
            else if (message.actionAck === "user_ack") {
                console.debug(`user_ack: ${JSON.stringify(message.data)}`);
                SetAccountDetail(message.data);
            }
            else if (message.actionAck === "tick_ack") {
                /*if (message.data && message.data.Symbol === "GBPUSD.hk") {
                    console.debug(`tick_ack: ${message.data.Symbol}, ${message.data.Bid}, ${message.data.Time}, ${needInsertQueue}`)
                }*/
                if (needInsertQueue === true)
                    priceTickQueue.push(message);
            }
            else if (message.actionAck === "new_market_order_ack") {
                console.debug(`new_market_order_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "new_pending_order_ack") {
                console.debug(`new_pending_order_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "delete_pending_order_ack") {
                console.debug(`delete_pending_order_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "liquidate_open_position_ack") {
                console.debug(`liquidate_open_position_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "modify_open_position_ack") {
                console.debug(`modify_open_position_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "modify_pending_order_ack") {
                console.debug(`modify_pending_order_ack: ${JSON.stringify(message.data)}`);
                const request = tasks[message.pid];
                if (request)
                    request.resolve(message.data);
            }
            else if (message.actionAck === "subscribe_ack") {
                console.debug("subscribe_ack: " + message.data.length)
                batch(() => {
                    message.data.forEach(data => {
                        if (message.data)
                        {
                            SetInitPriceQuoteInfo(data.Symbol, data);
                        }
                    });
                    UpdateAccountDetailInit();
                });
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
            }
            else if (message.actionAck === "positions_ack") {
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
                console.debug(`positions_ack: ${JSON.stringify(message.data)}`);
                SetOpenPositions(message.data);
            }
            else if (message.actionAck === "orders_ack") {
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
                console.debug(`orders_ack: ${JSON.stringify(message.data)}`);
                SetOrders(message.data);
            }
            else if (message.actionAck === "open_position_ack") {
                console.debug(`open_position_ack: ${JSON.stringify(message.data)}`);
                AddOpenPositions(message.data);
                // Force to calculate this new position PL.
                UpdateOpenPositionsPL(message.data.Symbol, message.data);
            }
            else if (message.actionAck === "liquidation_ack") {
                console.debug(`liquidation_ack: ${JSON.stringify(message.data)}`);
                DeleteOpenPositions(message.data.PositionID);
            }
            else if (message.actionAck === "pending_order_ack") {
                console.debug(`pending_order_ack: ${JSON.stringify(message.data)}`);
                if (message.data.Action == 1) {
                    AddOrders(message.data);
                }
                else if (message.data.Action == 2) {
                    AddOrders(message.data);
                }
                else if (message.data.Action == 3) {
                    DeleteOrders(message.data.Ticket);
                }

            }
            else if (message.actionAck === "add_watch_list_ack") {
                console.debug(`add_watch_list_ack: ${message.status}`);
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
            }
            else if (message.actionAck === "remove_watch_list_ack") {
                console.debug(`remove_watch_list_ack: ${message.status}`);
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
            }
            else if (message.actionAck === "replace_watch_list_ack") {
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
                console.debug(`replace_watch_list_ack: ${message.status}`);
            }
            else if (message.actionAck === "duplicate_login_ack") {
                console.debug("Duplicate login Detected");
                connectionStatus = Status.DuplicateLogin;
                SetConnectionStatus(Status.DuplicateLogin);
            }
            else if (message.actionAck === "subscribe_symbol_detail_ack") {
                const request = tasks[message.pid];
                if (request) {
                    request.resolve(message.status);
                }
                console.debug(`subscribe_symbol_detail_ack: ${message.status}`);
            }
            else if (message.actionAck === "trader_sentiment_ack") {
                SetTraderSentiment(message.data)
            }
            else if (message.actionAck === "live_stat_ack")
            {
                SetLiveStat(message.data)
            }
            else if (message.actionAck === "unsubscribe_symbol_detail_ack") {
                console.debug(`unsubscribe_symbol_detail_ack: ${message.status}`);
            }
            else if (message.actionAck === "symbols_update_ack") {
                // console.debug(`symbols_update_ack: ${message.status}`)
                // console.debug(`symbols_update_ack: ${JSON.stringify(message.data)}`)

                SetSymbolsUpdate(message.data)
            }
            else if (message.actionAck === "unsubscribe_ack")
            {
                console.debug(`unsubscribe_ack`)
            }
            else if (message.actionAck === "manual_approval_order_rejected_ack")
            {
                SetManualApprovalOrderRejecteUpdate(message.data);
            }
            else {
                console.debug(`other action: ${message.actionAck}`);
                console.debug(`other message: ${JSON.stringify(message.data)}`);
            }
        }
    }



    const state = {
        needInsertQueue,
        isConnected,
        sendMessage,
        connect,
        closeWebsocket,
        subscribeSymbols,
        submitMarketOrder,
        submitPendingOrder,
        getPositions,
        getOrders,
        submitDeletePendingOrder,
        submitEditPosition,
        submitEditOrder,
        subscribeSymbolDetail,
        unsubscribeSymbolDetail,
        getSymbolList
    };

    return (
        <WebSocketContext.Provider value={state}>
            {children}
        </WebSocketContext.Provider>
    )
}

export default WebSocketProvider;