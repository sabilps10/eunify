export enum RequestType {
    GET_SYMBOL_LIST = "symbol_list",
    SUBSCRIBE_SYMBOLS = 'subscribe',
    SUBMIT_MARKET_ORDER = 'new_market_order',
    SUBMIT_NEW_PENDING_ORDER = 'new_pending_order',
    MODIFY_OPEN_POSITION = 'modify_open_position',
    MODIFY_PENDING_ORDER = 'modify_pending_order',
    GET_POSITIONS = "positions",
    GET_ORDERS = "orders",
    REPLACE_WATCH_LIST = "replace_watch_list",
    LIQ_POSITION = "liquidate_open_position",
    SUBMIT_DELETE_PENDING_ORDER = 'delete_pending_order',
    SUBSCRIBE_SYMBOL_DETAIL = "subscribe_symbol_detail",
    UNSUBSCRIBE_SYMBOL_DETAIL = "unsubscribe_symbol_detail",
    UNSUBSCRIBE_SYMBOLS = 'unsubscribe',
}

export interface GetSymbolListRequest {
    pid: string;
    action: string;
}

export interface GetWatchListRequest {
    pid: string;
    action: string;
}

export interface GetPositionsRequest {
    pid: string;
    action: string;
}

export interface GetOrdersRequest {
    pid: string;
    action: string;
}

export interface SubscribeSymbolsRequest {
    pid: string;
    action: string;
    data: {
        Symbols: (string)[];
    };
}

export interface SubmitMarketOrderRequest {
    pid: string;
    action: string;
    data: {
        Login: number;
        UserRequestID: string;
        Symbol: string;
        Type: number;
        Volume: number;
        OrderPrice: number;
        SL: number;
        TP: number;
        Dev: number;
        Comment: string;
        PositionID?: number;
    };
}

export interface SubmitPendingOrderRequest {
    pid: string;
    action: string;
    data: {
        Login: number;
        UserRequestID: string;
        Symbol: string;
        Type: number;
        Volume: number;
        OrderPrice: number;
        SL: number;
        TP: number;
        Dev: number;
        Comment: string;
        TypeTime: number;
    };
}

export interface SubmitDeletePendingOrderRequest {
    pid: string;
    action: string;
    data: {
        Login: number;
        Ticket: number;
        Type: string;
    };
}

export interface SubmitEditPositionRequest {
    pid: string;
    action: string;
    data: {
        Login: number;
        Ticket: number;
        Type: string;
        SL: number;
        TP: number;
    };
}

export interface SubmitEditOrderRequest {
    pid: string;
    action: string;
    data: {
        Login: number;
        Ticket: number;
        OrderPrice: number
        SL: number;
        TP: number;
        Type: string;
    };
}


export interface SubscribeSymbolDetailRequest {
    pid: string;
    action: string;
    data: {
        Symbol: string;
    };
}