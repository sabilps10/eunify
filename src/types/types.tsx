import { ColorValue } from "react-native";

export interface THEME_TYPE {
    /* Deprecated */
    primary: ColorValue,
    background: ColorValue,
    card: ColorValue,
    text: ColorValue,
    border: ColorValue,
    notification: ColorValue,
    Background: ColorValue,
    Text: ColorValue,
    LText: ColorValue,
    Brand4: ColorValue,
    primaryBtn: ColorValue,
    secondaryBtn: ColorValue,
    White: ColorValue,
    Black: ColorValue,
    Grey5: ColorValue,
    Grey4: ColorValue,
    Grey3: ColorValue,
    Grey2: ColorValue,
    Grey1: ColorValue,
    Grey0: ColorValue,
    DM_Background: ColorValue,
    DM_Dk_Background: ColorValue,
    DM_Lt_Background: ColorValue,
    Neutral3: ColorValue,
    Red: ColorValue,
    Green: ColorValue,
    tabUnselect: ColorValue,
    fixedText: ColorValue,
    fixedBorder: ColorValue,
    fixedBackground: ColorValue,
    arrow: ColorValue,
    highlight: ColorValue,
    contentBackground: ColorValue,
    biometricsIcon: ColorValue,
    primaryBtnDisabled: ColorValue,
    primaryBtnFont: ColorValue,
    /* Deprecated */

    Brand1: ColorValue,
    Brand2: ColorValue,
    Brand3: ColorValue,

    Up: ColorValue,
    Down: ColorValue,

    LabelBtnSelectedBg: ColorValue,
    ValueBtnSelectedBg: ColorValue,

    InputBg: ColorValue,
    FixedInputBg: ColorValue,
    DropDownBg: ColorValue,
    SelectOptionBg: ColorValue,
    WrapperBg: ColorValue,
    PriceBoxBg: ColorValue,

    MainBg: ColorValue,
    FixedMainBg: ColorValue,
    MainMenuBg: ColorValue,
    CardSelectedBg: ColorValue,
    PopBg: ColorValue,

    ToastMessageBg: ColorValue,

    EnableBtn: ColorValue,
    DisableBtn: ColorValue,

    MainFont: ColorValue,
    FixedMainFont: ColorValue,
    ValueBtnFont: ColorValue,
    TabOptionFont: ColorValue,
    MainMenuFont: ColorValue,
    MainMenuImg: ColorValue,
    CardSelectedFont: ColorValue,
    
    InputPlaceholder: ColorValue,

    LabelBtnFont: ColorValue,
    TitleFont: ColorValue,
    FixedTitleFont: ColorValue,
    SubTitleFont: ColorValue,
    EnableBtnFont: ColorValue,
    FixedEnableBtnFont: ColorValue,
    DisableBtnFont: ColorValue,
    FixedDisableBtnFont: ColorValue,
    DialogTitleFont: ColorValue,

    TabOptionSelectedFont: ColorValue,
    LabelBtnSelectFont: ColorValue,
    ToastMessageFont: ColorValue,

    TabFont: ColorValue,

    TabSelectedFont: ColorValue,

    MainMenuSelectedFont: ColorValue,

    HeaderFont: ColorValue,
    HeaderDetailFont: ColorValue,
    DemoHeaderFont: ColorValue,

    SectionTitleFont: ColorValue,
    
    DashedLine: ColorValue,
    
    Indicator: ColorValue,
    FixedIndicator: ColorValue,

};

