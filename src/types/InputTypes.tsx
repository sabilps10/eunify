export type InputType = {
    value: string,
    error: string,
    mandatory: boolean,
    maxLength?: number,
    isValid: boolean
}

export type SignUpForm = {
    password: InputType,
    confirmPassword: InputType,
    // email: InputType,
    phonePrefix: InputType,
    // phoneNumber: InputType,
    referralCode: InputType
}

export type PasswordLoginForm = {
    password: InputType,
    email: InputType,
    phonePrefix: InputType,
    phoneNumber: InputType,
    type: string
}

export type AddDeviceForm = {
    code: InputType,
}