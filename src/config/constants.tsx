//HCRM
export const env: string = 'SIT'; // SIT or UAT
export const entityId: string = 'EIE'; // EIE or EMX

export const loginSecret = '57b3e39762koid4e5a9c13398pof8&de9363';

export const allowSkipSMS = false;

export let applauncherURL;

if (env === 'SIT') {
  applauncherURL =
    entityId === 'EIE' ? 'https://sit-ao-api.empfs.net/getmetadata.php' : '';
} else {
  applauncherURL =
    entityId === 'EIE'
      ? 'https://uat-ao-api.empfs.net/getmetadata.php'
      : 'https://uat-ao-api.emxpro.com/getmetadata.php';
}

export const applauncherBase =
  env === 'SIT'
    ? 'https://dbs6z606lrzqn.cloudfront.net/'
    : 'https://d31ea1zznhr9ty.cloudfront.net/'

export const applauncherSecret = '78214125432A462D';
export const licenseKey =
  entityId === 'EIE' ? 'IOosg7FUbyp8GaopjWsn' : '7iRU5909ChLP6FhsUOnP';
export const version = '1.0';

export const buildDate = '20230811';

export const appStoreLink = 'https://apps.apple.com/app/idyourappid'
export const playStoreLink = 'https://play.google.com/store/apps/details?id=com.yourpackagename'

export const iosCodePushDeploymentKey =
  entityId === 'EIE'
    ? 'dBn1OHQPE2e3u5l3ONwdUy6N3tB1s889Wzyh3'
    : 'KSfFp_2ut4xuT1c_yMOhzbO70pyB2jv_S76XU'; //iOS Staging
export const androidCodePushDeploymentKey =
  entityId === 'EIE'
    ? 'Zw5xO0jGTCWzfep3WMruV3se-CKqaJn8WVfCo'
    : 'Q8cFVcIuS56CZNm_LkW8cMhfI6uCnun48KGhE'; //Android Staging

// MIOJANUAR
export const MIOAuthDomain =
  env === 'SIT'
    ? 'https://mio-sit-user.auth.ap-southeast-1.amazoncognito.com'
    : 'https://aomio-uat-user.auth.ap-southeast-1.amazoncognito.com';
export const MIOClientId =
  env === 'SIT' ? '1flqcgoilamuppppk89f40ir2r' : '5pq9s21ou2fpff7a6dftqjd1nr';
export const MIOClientSecret =
  env === 'SIT'
    ? 'ankhfvrmdsrpmom3tifp70m04ulebjccia7pslct8l2pe7mp9r3'
    : 'edlmcqh0tcnvjdcuv4i6td5gfqal4rkkj1jjtk4aesfltlbiprs';
export const MIOWebDomain =
  env === 'SIT'
    ? 'https://sit-mio-cp.empfs.net'
    : 'https://uat-aomio-cp.empfs.net';
export const MIOUserTokenDomain =
  env === 'SIT'
    ? 'https://4n4tympo21.execute-api.ap-southeast-1.amazonaws.com/sit/encrypt'
    : 'https://5yog2qw6ri.execute-api.ap-southeast-1.amazonaws.com/uat/encrypt';

// export const captchaID = '82a29c2006278b0676ace7a4de2b20c2';
export const captchaID = '2e80d86ce5a7685f2811f47fd9e6a889';


export const cognito_identityPoolId = '';
export const cognito_region = 'ap-southeast-1';

export let cognito_userPoolId;
export let cognito_userPoolWebClientId;

if (env === 'SIT') {
  cognito_userPoolId =
    entityId === 'EIE'
      ? 'ap-southeast-1_nRf72622C'
      : 'ap-southeast-1_369nJaBN4';
  cognito_userPoolWebClientId =
    entityId === 'EIE'
      ? '4ajak3uvupv49upn9739f8fkng'
      : '6r2nm13d7mdlr2ebtok4pjq5ka';
} else {
  cognito_userPoolId =
    entityId === 'EIE'
      ? 'ap-southeast-1_IFXkofYMj'
      : 'ap-southeast-1_62Z0JgaQI';
  cognito_userPoolWebClientId =
    entityId === 'EIE'
      ? '3rvebvb5jlnf2olk2eum0e3t5d'
      : '1v1ko3rpit8nr6g3vbtbr7tq4';
}

// Xpro
// export const cognito_userPoolId = 'ap-southeast-1_369nJaBN4';
// export const cognito_userPoolWebClientId = '6r2nm13d7mdlr2ebtok4pjq5ka';

export const percentageOrderSpread = 5;

export const dialogDisplaySecond = 2;
export const resendCodeSecond = 60;
export const refreshTokenMinutes = 60;

export const priceChangePercentageDP = 2;
export const chartPort = 18081;
export const timeoutMinutes = 30;

export const actionTimeoutMilliseconds = 40000;

export const certPinConfig = {
  'empfs.net': {
    includeSubdomains: true,
    publicKeyHashes: [
      'dpK8k8vMUj5tjDqlRCYoEL+0K61tcxEtodAlbTV/gEY=',
      '8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=',
      '4Mk9EKftUQGm3fRNt686XGIPY879RgHh4+mxJgopcBo=',
      '8kGWrpQHhmc0jwLo43RYo6bmqtHgsNxhARjM5yFCe/w='
    ],
  },
}

//Loyalty
//export const host_loyaltyApi = 'https://';
export const host_loyaltyApi =
  env === 'SIT'
    ? 'https://aunwjcqqwg.execute-api.ap-southeast-1.amazonaws.com/v01'
    : 'https://q0k5hgjxi0.execute-api.ap-southeast-1.amazonaws.com/v01';

export const loyalty_getExistingAccount = '/loyalty/member/crm-user';

export const reward_signup_eiehk =
  env === 'SIT'
    ? 'https://efsg-gcrm-sit-ao.herokuapp.com/#/new-member-reg'
    : 'https://efsg-gcrm-uat-ao.herokuapp.com/#/new-member-reg';

export const reward_signup_emxpro =
  env === 'SIT'
    ? 'https://sit-clientportal.cn-xpro.net/trade-portal/home/registerQuick?promoCode=GENERAL&utm_source=app&utm_medium=myreward&utm_campaign=regular'
    : 'https://uat-clientportal.cn-xpro.net/trade-portal/home/registerQuick?promoCode=GENERAL&utm_source=app&utm_medium=myreward&utm_campaign=regular';

export const reward_contactus_emxpro =
  env === 'SIT'
    ? 'https://sit.emxpro.com/activity/20210421.html'
    : 'https://uat.emxpro.com/activity/20210421.html';

export const reward_login_loyaltyportal =
  env === 'SIT'
    ? 'https://efsg-gcrm-sit-ao.herokuapp.com/#/signin'
    : 'https://efsg-gcrm-uat-ao.herokuapp.com/#/signin';

export const host_rewardApi =
  env === 'SIT'
    ? 'https://efsg-gcrm-sit-ao.herokuapp.com/api/v1'
    : 'https://efsg-gcrm-uat-ao.herokuapp.com/api/v1';

export const emxproLiveChat = 'https://www.echatsoft.com/visitor/pc/chat.html?companyId=521916&echatTag=emxpro';

//Loyalty and AboutUs
export const reward_getRewardList = '/reward?queryParam=';
export const reward_pageSize = 'pageSize=6';
export const reward_defaultPointStart = '0';
export const reward_defaultPointEnd = '200000';
export const reward_whatsappUrl ='https://wa.me/85263898882?text=';
export const reward_efsgphone = '123456789';;//TO BE CHANGED
export const reward_efsgemail = 'apps@empfs.com';//TO BE CHANGED

// account opening
export const host_accountOpening =
  env === 'SIT'
    ? 'https://gld01hhosg.execute-api.ap-southeast-1.amazonaws.com/sit/core-service'
    : 'https://uat-aocm-api.empfs.net/core-service';

export const host_cm =
  env === 'SIT'
    ? 'https://q7ehc5ldvc.execute-api.ap-southeast-1.amazonaws.com'
    : 'https://e3x1ghmek0.execute-api.ap-southeast-1.amazonaws.com';

export const sumsub_url =
  env === 'SIT'
    ? 'https://xxjuzpikw7.execute-api.ap-southeast-1.amazonaws.com/sit/sumsub/requestToken'
    : 'https://fl6qq4eyb6.execute-api.ap-southeast-1.amazonaws.com/uat/sumsub/requestToken';
