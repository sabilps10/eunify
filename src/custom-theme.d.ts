import '@react-navigation/native';
import { THEME_TYPE } from './types/Types'

// Override the theme in react native navigation to accept our custom theme props.
declare module '@react-navigation/native' {
  export type ExtendedTheme = {
    dark: boolean;
    colors: THEME_TYPE
  };
  export function useTheme(): ExtendedTheme;
}