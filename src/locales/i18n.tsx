import i18n from "i18next";
import Backend from 'i18next-http-backend';
import { initReactI18next } from "react-i18next";
import { getLocales } from "react-native-localize";

import EN from './en.json';
import TC from './tc.json';
import SC from './sc.json';
import PrivacyPolicyEN from './privacy_policy_en.json';
import PrivacyPolicyTC from './privacy_policy_tc.json';
import PrivacyPolicySC from './privacy_policy_sc.json';
import TermsOfServicesEN from './terms_of_service_en.json';
import TermsOfServicesTC from './terms_of_service_tc.json';
import TermsOfServicesSC from './terms_of_service_sc.json';

// jest.mock('react-i18next', () => ({
//     useTranslation: () => ({ t: key => key })
// }));

i18n
    // load translation using http -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
    // learn more: https://github.com/i18next/i18next-http-backend
    // want your translations to be loaded from a professional CDN? => https://github.com/locize/react-tutorial#step-2---use-the-locize-cdn
    //.use(Backend)
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    //.use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        //backend: {
        //網頁載入時去下載語言檔的位置
        //    loadPath: "/locales/{{lng}}/translation.json",
        //},
        // the translations
        // (tip move them in a JSON file and import them,
        // or even better, manage them via a UI: https://react.i18next.com/guides/multiple-translation-files#manage-your-translations-with-a-management-gui)
        resources: {
            en: {
                translation: EN,
                privacy: PrivacyPolicyEN,
                terms: TermsOfServicesEN
            },
            tc: {
                translation: TC,
                privacy: PrivacyPolicyTC,
                terms: TermsOfServicesTC
            },
            sc: {
                translation: SC,
                privacy: PrivacyPolicySC,
                terms: TermsOfServicesSC
            }
        },
        lng: getLocales()[0].languageCode,
        fallbackLng: "en",
        compatibilityJSON: 'v3',
        interpolation: {
            escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
        },
    });

    export const getLanguage = () => i18n.language;
    export const getLangCode = () => getLanguage().substring(0, 2); // en, zh...
    
export default i18n;