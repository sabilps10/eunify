import {
    ViewStyle,
    ImageStyle,
    TextStyle,
    StyleSheet
} from 'react-native'
import { THEME_TYPE } from '../types/types'

export const LAYOUT_PADDING_HORIZONTAL = 16;

export const makeGlobalStyles = (colors: THEME_TYPE) => StyleSheet.create({

    // Layout
    container: {
        flex: 1,
    },
    container_mainBg: {
        flex: 1,
        backgroundColor: colors.MainBg
    },
    container_white_sheet: {
        flex: 1,
        backgroundColor: colors.White,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12
    },

    // Text Styles
    H1: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 40,
        // lineHeight: 40 * 1.2,
        color: colors.MainFont
    },
    H2: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 28,
        // lineHeight: 28 * 1.2,
        color: colors.MainFont
    },
    H3: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 21,
        // lineHeight: 21 * 1.4,
        color: colors.MainFont
    },
    H4: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 17,
        // lineHeight: 17 * 1.4,
        color: colors.MainFont
    },
    Big_Text_B: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 15,
        // lineHeight: 15 * 1.6,
        color: colors.MainFont
    },
    Big_Text: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 15,
        // lineHeight: 15 * 1.6,
        color: colors.MainFont
    },
    Body_Text_B: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 14,
        // lineHeight: 14 * 1.6,
        color: colors.MainFont
    },
    Body_Text: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        // lineHeight: 14 * 1.6,
        color: colors.MainFont
    },
    Note_B: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 13,
        // lineHeight: 13 * 1.6,
        color: colors.MainFont
    },
    Note: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        // lineHeight: 13 * 1.6,
        color: colors.MainFont
    },
    Small_Note_B: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 11.5,
        // lineHeight: 11.5 * 1.6,
        color: colors.MainFont
    },
    Small_Note: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 11.5,
        // lineHeight: 11.5 * 1.6,
        color: colors.MainFont
    },
    Tiny_Note: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 10.5,
        // lineHeight: 10.5 * 1.6,
        color: colors.MainFont
    },
    Form_Title: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        // lineHeight: 14 * 1.4,
        color: colors.MainFont
    },
    Form_Infield: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 16,
        // lineHeight: 16 * 1.4,
        color: colors.MainFont
    },
    Mobile_Menu: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 10,
        // lineHeight: 10,
        letterSpacing: 0.2,
        color: colors.MainFont,
        paddingBottom: 6
    },
    Body_Text_B_120: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 13,
        //lineHeight: 16,
        color: colors.MainFont
    },
    Note_120: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 13,
        //lineHeight: 14,
        color: colors.MainFont
    },
    Mobile_Menu_Bold: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 10,
        //lineHeight: 10,
        letterSpacing: 0.2,
        color: colors.MainFont
    },
    Chart_Text: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 9,
        //lineHeight: 10,
        color: colors.MainFont
    },
    ButtonText: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 15,
        //lineHeight: 20,
        letterSpacing: 0.3,
        color: colors.MainFont
    },
    Contract: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 24,
        //lineHeight: 22,
        color: colors.MainFont
    },
    Default: {
        fontFamily: 'Encode Sans Condensed',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 11,
        //lineHeight: 22,
        color: colors.White
    },
    MultipleLines: {
        flex: 1,
        flexGrow: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    Background: {
        backgroundColor: colors.background
    }
});


export default StyleSheet.create({
    container: {
        flex: 1
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 10,
    },
    ScrollViewContainerStyle: {
        flexGrow: 1
    },
    contentContainer: {
        paddingHorizontal: LAYOUT_PADDING_HORIZONTAL,
        flex: 1
    },
    inputPadding: {
        padding: 14
    }
});


