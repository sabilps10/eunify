import { ViewStyle } from "react-native";
import { ExtendedTheme } from '@react-navigation/native'
import { Color } from "./color";

export const LightTheme: ExtendedTheme = {
    dark: false,
    colors: {
        /* Deprecated */
        primary: '#000000',
        background: '#F2F2F2',
        card: 'rgb(255, 255, 255)',
        text: 'rgb(28, 28, 30)',
        border: 'rgb(199, 199, 204)',
        notification: 'rgb(255, 69, 58)',
        Background: '#DDDDDD',
        Text: '#444444',
        LText: '#EEEEEE',
        Brand4: '#4ABDAC',
        primaryBtn: '#FED661',
        secondaryBtn: '#CFA872',
        White: '#FFFFFF',
        Black: '#000000',
        Grey5: '#585858',
        Grey4: '#808080',
        Grey3: '#9D9D9D',
        Grey2: '#CACACA',
        Grey1: '#E6E6E6',
        Grey0: '#EEEEEE',
        DM_Background: '#2E3E49',
        DM_Dk_Background: '#19242C',
        DM_Lt_Background: '#415463',
        Neutral3: '#545D69',
        Red: '#F4465E',
        Green: '#4ABDAC',
        tabUnselect: '#00000040',
        fixedText: "#FFFFFF",
        fixedBorder: "#FFFFFF",
        fixedBackground: "#00B1D2",
        arrow: "#00B1D2",
        highlight: "#00B1D2",
        contentBackground: "#EEEEEE",
        biometricsIcon: "#CACACA",
        primaryBtnDisabled: "#CACACA",
        primaryBtnFont: "#444444",
        /* Deprecated */

        Brand1: Color.HEXFED661,
        Brand2: Color.HEXCFA872,
        Brand3: Color.HEX00B1D2,

        Up: Color.HEX4ABDAC,
        Down: Color.HEXF4465E,

        LabelBtnSelectedBg: Color.HEXCFA872,
        ValueBtnSelectedBg: Color.HEXCFA872,

        InputBg: Color.HEXEEEEEE,
        FixedInputBg: Color.HEXFFFFFF,
        DropDownBg: Color.HEXEEEEEE,
        SelectOptionBg: Color.HEXEEEEEE,
        WrapperBg: Color.HEXEEEEEE,
        PriceBoxBg: Color.HEXEEEEEE,

        MainBg: Color.HEXFFFFFF,
        FixedMainBg: Color.HEXFFFFFF,
        MainMenuBg: Color.HEXFFFFFF,
        CardSelectedBg: Color.HEXFFFFFF,
        PopBg: Color.HEXFFFFFF,

        ToastMessageBg: Color.HEX9D9D9DE6,

        EnableBtn: Color.HEXFED661,
        DisableBtn: Color.HEXCACACA,

        MainFont: Color.HEX444444,
        FixedMainFont: Color.HEX444444,
        ValueBtnFont: Color.HEX444444,
        TabOptionFont: Color.HEX444444,
        MainMenuFont: Color.HEX444444,
        MainMenuImg: Color.HEX9D9D9DE6,
        CardSelectedFont: Color.HEX444444,
        
        InputPlaceholder: Color.HEX44444480,

        LabelBtnFont: Color.HEX9D9D9D,
        TitleFont: Color.HEXFFFFFF,
        FixedTitleFont: Color.HEXFFFFFF,
        SubTitleFont: Color.HEX9D9D9D,
        EnableBtnFont: Color.HEX444444,
        FixedEnableBtnFont: Color.HEX444444,
        DisableBtnFont: Color.HEX808080,
        FixedDisableBtnFont: Color.HEX808080,
        DialogTitleFont: Color.HEX808080,

        TabOptionSelectedFont: Color.HEX444444,
        LabelBtnSelectFont: Color.HEXFFFFFF,
        ToastMessageFont: Color.HEXFFFFFF,

        TabFont: Color.HEX00000040,

        TabSelectedFont: Color.HEXCFA872,

        MainMenuSelectedFont: Color.HEX00B1D2,

        HeaderFont: Color.HEXFFFFFF,
        HeaderDetailFont: Color.HEX444444,
        DemoHeaderFont: Color.HEX444444,

        SectionTitleFont: Color.HEX000000,

        DashedLine: Color.HEX9D9D9D,

        Indicator: Color.HEXFFFFFF,
        FixedIndicator: Color.HEXFFFFFF,
    },
};

export const DarkTheme: ExtendedTheme = {
    dark: true,
    colors: {
        /* Deprecated */
        primary: '#F2F2F2',
        background: '#808080',
        card: 'rgb(255, 255, 255)',
        text: 'rgb(28, 28, 30)',
        border: 'rgb(199, 199, 204)',
        notification: 'rgb(255, 69, 58)',
        Background: '#DDDDDD',
        Text: '#EEEEEE',
        LText: '#EEEEEE',
        Brand4: '#4ABDAC',
        primaryBtn: '#FED661',
        secondaryBtn: '#CFA872',
        White: '#FFFFFF',
        Black: '#000000',
        Grey5: '#585858',
        Grey4: '#808080',
        Grey3: '#9D9D9D',
        Grey2: '#CACACA',
        Grey1: '#E6E6E6',
        Grey0: '#EEEEEE',
        DM_Background: '#2E3E49',
        DM_Dk_Background: '#19242C',
        DM_Lt_Background: '#415463',
        Neutral3: '#545D69',
        Red: '#F4465E',
        Green: '#4ABDAC',
        tabUnselect: '#00000040',
        fixedText: "#FFFFFF",
        fixedBorder: "#FFFFFF",
        fixedBackground: "#00B1D2",
        arrow: "#00B1D2",
        highlight: "#00B1D2",
        contentBackground: "#415463",
        biometricsIcon: "#808080",
        primaryBtnDisabled: "#CACACA",
        primaryBtnFont: "#444444",
        /* Deprecated */

        Brand1: Color.HEXFED661,
        Brand2: Color.HEXCFA872,
        Brand3: Color.HEX00B1D2,

        Up: Color.HEX4ABDAC,
        Down: Color.HEXF4465E,

        LabelBtnSelectedBg: Color.HEXCFA872,
        ValueBtnSelectedBg: Color.HEXCFA872,

        InputBg: Color.HEX415463,
        FixedInputBg: Color.HEXFFFFFF,
        DropDownBg: Color.HEX415463,
        SelectOptionBg: Color.HEX415463,
        WrapperBg: Color.HEX415463,
        PriceBoxBg: Color.HEX415463,

        MainBg: Color.HEX2E3E49,
        FixedMainBg: Color.HEXFFFFFF,
        MainMenuBg: Color.HEX2E3E49,
        CardSelectedBg: Color.HEXFFFFFF,
        PopBg: Color.HEXFFFFFF,

        ToastMessageBg: Color.HEX9D9D9DE6,

        EnableBtn: Color.HEXFED661,
        DisableBtn: Color.HEXCACACA,

        MainFont: Color.HEXEEEEEE,
        FixedMainFont: Color.HEX444444,
        ValueBtnFont: Color.HEXEEEEEE,
        TabOptionFont: Color.HEXEEEEEE,
        MainMenuFont: Color.HEXEEEEEE,
        MainMenuImg: Color.HEX9D9D9DE6,
        CardSelectedFont: Color.HEXEEEEEE,
        
        InputPlaceholder: Color.HEXEEEEEE80,

        LabelBtnFont: Color.HEX9D9D9D,
        TitleFont: Color.HEXFFFFFF,
        FixedTitleFont: Color.HEXFFFFFF,
        SubTitleFont: Color.HEX9D9D9D,
        EnableBtnFont: Color.HEX444444,
        FixedEnableBtnFont: Color.HEX444444,
        DisableBtnFont: Color.HEX808080,
        FixedDisableBtnFont: Color.HEX808080,
        DialogTitleFont: Color.HEX808080,

        TabOptionSelectedFont: Color.HEXEEEEEE,
        LabelBtnSelectFont: Color.HEXFFFFFF,
        ToastMessageFont: Color.HEXFFFFFF,

        TabFont: Color.HEX00000040,

        TabSelectedFont: Color.HEXCFA872,

        MainMenuSelectedFont: Color.HEX00B1D2,

        HeaderFont: Color.HEXFFFFFF,
        HeaderDetailFont: Color.HEX444444,
        DemoHeaderFont: Color.HEX444444,

        SectionTitleFont: Color.HEXFFFFFF,

        DashedLine: Color.HEX9D9D9D,
        
        Indicator: Color.HEXFFFFFF,
        FixedIndicator: Color.HEXFFFFFF,
    },
};

// Effect Style
export const Layer_Shadow: ViewStyle = {
    backgroundColor: '#C4C4C4',
    shadowColor: 'rgba(0,0,0,0.15)',
    shadowRadius: 8,
    shadowOffset: { width: 0, height: -4 }
}

export const Card_Shadow: ViewStyle = {
    backgroundColor: '#C4C4C4',
    shadowColor: 'rgba(0,0,0,0.15)',
    shadowRadius: 8,
    shadowOffset: { width: 8, height: 8 }
}

// Grid Styles
export const Mobile360: ViewStyle = {
    backgroundColor: '#FFFFFF'
}
