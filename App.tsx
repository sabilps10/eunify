import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
import React, { useEffect, useState } from 'react';
import { StatusBar, StyleSheet, useColorScheme, LogBox, Alert, Settings, Platform, NativeModules, Text, BackHandler, Linking } from 'react-native';

import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './src/redux/store';

import { GestureHandlerRootView } from 'react-native-gesture-handler';

import { RootStackParamList, ScreenNavigationProp } from './src/routes/route';

import WebSocketProvider from './src/websocket/WebSocket';

import { Amplify, Auth } from 'aws-amplify';

import EditWatchlistView from './src/views/watchlist/editWatchlist/editWatchlist';
import ShareView from './src/views/watchlist/share';
import SearchProductView from './src/views/searchProduct/searchProductView';
import TabDirectory, { TabDirectoryTab } from './src/views/tabDirectory/tabDirectory';
import CognitoSignUp from './src/views/login/CognitoSignUp';
import CognitoSignUpVerification from './src/views/login/CognitoSignUpVerificataion';
import CognitoSignUpCompleted from './src/views/login/CognitoSignUpCompleted';
import CognitoForgetPassword from './src/views/login/CognitoForgetPassword';
import CognitoForgetPasswordVerification from './src/views/login/CognitoForgetPasswordVerification';
import CognitoForgetPasswordReset from './src/views/login/CognitoForgetPasswordReset';
import CognitoLogin from './src/views/login/CognitoLogin';
import EnableBiometrics from './src/views/login/EnableBiometrics';
import AddDevices from './src/views/devices/AddDevices';
import Welcome from './src/views/welcome/Welcome';
import Introduction from './src/views/welcome/Introduction';
import NewOrder from './src/views/trade/newOrder';
import ClosePosition from './src/views/trade/closePosition';
import EditPosition from './src/views/trade/editPosition';
import { WebsocketUtils } from './src/utils/websocketUtils'
import HistoryQuery from './src/views/history/historyQuery';
import PositionDetails from './src/views/positionDetails/positionDetails';
import MeSetting from './src/views/me/meSetting';
import MeSettingNotification from './src/views/me/meSettingNotification';
import MeSettingUser from './src/views/me/meSettingUser';
import MeChangeUsername from './src/views/me/meChangeUsername';
import MeChangeMobileNumber from './src/views/me/meChangeMobileNumber';
import DeviceList from './src/views/devices/DeviceList';

import AboutUs from './src/views/aboutUs/AboutUs';

import { LightTheme, DarkTheme } from './src/theme/theme'
import { useSelector } from 'react-redux';
import { State } from './src/redux/root-reducer';
import { AccountUtils } from './src/utils/accountUtils'
import NetInfo from "@react-native-community/netinfo";

import { iosCodePushDeploymentKey, androidCodePushDeploymentKey, cognito_identityPoolId, cognito_region, cognito_userPoolId, cognito_userPoolWebClientId, dialogDisplaySecond, refreshTokenMinutes, playStoreLink, appStoreLink, certPinConfig, actionTimeoutMilliseconds } from './src/config/constants';

import SplashScreen from 'react-native-splash-screen';
import ContractDetailView from './src/views/watchlist/contractDetail/contractDetailView';
import i18n from './src/locales/i18n';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Status } from './src/redux/connection/type';
import { AccountActionCreators } from './src/redux/account/actionCreator';
import { useAppState } from '@react-native-community/hooks';
import { ConnectionActionCreators } from './src/redux/connection/actionCreator';
import { StateActionCreators } from './src/redux/state/actionCreator';

import { setAPIURL, fetchWithTimeout, setTradingURL, tradingAPI, setChartURL } from './src/utils/api/apiSetter';
import { applauncherURL, licenseKey, version, applauncherSecret } from './src/config/constants';
// @ts-ignore
import Pushy from 'pushy-react-native';

import BottomSelection from './src/components/bottomSelectionSheet/bottomSelection.component';
import { anyLogin, loginLevel, setLv3Registration } from './src/utils/api/userApi';
import { CognitoUserSession } from 'amazon-cognito-identity-js';
import PermissionAccess from './src/views/permission/permissionAccess';

import codePush from 'react-native-code-push';
import MessageDialog from './src/components/dialogs/messageDialog';
import { t } from 'i18next';
import { errorType } from './src/utils/errorUtil';
import { getRegion } from './src/utils/api/regionApi';
import Orientation from 'react-native-orientation-locker';
import CodePush from 'react-native-code-push';
import LoadingDialog from './src/components/dialogs/loadingDialogs';
import ToastDialog from './src/components/dialogs/toastDialogs';
import HistoryChartDetail from './src/views/history/historyChartDetail';
import OrderConfirmDialog from './src/components/dialogs/orderConfirmDialog';
import CountryOptionView from './src/components/countryOptionView/countryOptionView';
import { CountryContent, PortfolioTabType, RegionContent } from './src/redux/state/types';
import PrivacyPolicy from './src/views/welcome/PrivacyPolicy';
import TermsOfService from './src/views/welcome/TermsOfService';
import CaptchaPage from './src/views/login/CaptchaPage';
import AddDeviceDialog from './src/components/dialogs/addedDeviceDialogs';
import CheckUpdate from './src/views/welcome/CheckUpdate';
import analytics from '@react-native-firebase/analytics';
import ForceChangePassword from './src/views/login/ForceChangePassword';
import UserVerification from './src/views/login/UserVerification';

import MyRewardScreen from './src/views/loyalty/MyRewardScreen';

import { current } from '@reduxjs/toolkit';
import MIO from './src/components/MIO';
import { CognitoActionCreators } from './src/redux/cognito/actionCreator';
import GAScreenName from './src/json/GAScreenName.json';
import { timeoutMinutes } from './src/config/constants';
import AccountQuestion from './src/views/accountOpening/AccountQuestion';
import DropdownPage from './src/components/forms/inputs/DropdownPage';
import RecommendedTradingAccount from './src/views/accountOpening/RecommendedTradingAccount';
import AccountAgreement from './src/views/accountOpening/AccountAgreement';
import AccountGettingReady from './src/views/accountOpening/AccountGettingReady';
import AccountPersonalInformation from './src/views/accountOpening/AccountPersonalInformation';
import AccountTermsCondition from './src/views/accountOpening/TermsOfService';
import AccountContactInformation from './src/views/accountOpening/AccountContactInformation';
import BottomSheetDropdown from './src/components/bottomSelectionSheet/BottomSheetDropdown';
import AccountFinancialInformation from './src/views/accountOpening/AccountFinancialInformation';
import AccountTradingExperience from './src/views/accountOpening/AccountTradingExperience';
import AccountReviewApplication from './src/views/accountOpening/AccountReviewApplication';
import AccountPromotion from './src/views/accountOpening/AccountPromotion';
import AccountApplicationStatus from './src/views/accountOpening/AccountApplicationStatus';
import DepositDialog from './src/views/accountOpening/DialogCustom';
import EkycScreen from './src/views/Ekyc/EkycScreen';
import AccountStatus from './src/views/accountOpening/AccountStatus';
import TabDirectoryWebView from './src/views/tabDirectory/tabDirectoryWebView';
import { CognitoTempActionCreators } from './src/redux/cognitoTemp/actionCreator';
import { requestTrackingPermission } from 'react-native-tracking-transparency';
import { navigationRef } from "./src/utils/navigation"
import CountryDropdownPage from './src/components/forms/inputs/CountryDropdownPage';
import {getCountryList} from './src/utils/api/apiGeneralAccountOpening';
import { setUserLanguage } from './src/utils/api/apiAccountOpening';

import { initializeSslPinning } from 'react-native-ssl-public-key-pinning';

import JailMonkey from 'jail-monkey'
import { SetCountryList } from './src/redux/state/action';
Text.defaultProps = {
  ...Text.defaultProps,
  allowFontScaling: false,
};

const initI18n = i18n;
var applauncherErrorCode = 0;

const App: React.FC<RootStackParamList> = () => {

  const [applauncherStatus, setApplauncherStatus] = useState<boolean>(false);
  // const [applauncherErrorCode, setApplauncherErrorCode] = useState<number>(0);

  Orientation.lockToPortrait(); //this will lock the view to Portrait

  //Get Data from Applauncher
  var CryptoJS = require('crypto-js');
  const License = licenseKey;
  const Version = version;
  const key = applauncherSecret;

  // console.debug(licenseKey, Version, key)

  const encryptedKey = CryptoJS.AES.encrypt(
    `license=${License}&version=${Version}`,
    CryptoJS.enc.Utf8.parse(key),
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
      drop: 128,
    }
);

  const url = applauncherURL + '?key=' + encryptedKey.ciphertext.toString(CryptoJS.enc.Hex);
  // console.debug('applauncherURL ', url)

  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  };

  const fetchData = async () => {
    console.debug("call fetchData");
    
    let success = false;
    let errorMessage = "Server Error. Please try again";

    const response = await NetInfo.fetch();
    if (!response.isConnected)
    {
      errorMessage = "No Internet connection. Please check your network setting and try again."
    }
    else
    {
      console.debug("call initializeSslPinning");
      if ((Platform.OS === 'ios' || Platform.OS === 'android') && certPinConfig) {
        await initializeSslPinning(certPinConfig);
      }
      
      try {
        const response = await fetchWithTimeout(url, requestOptions, actionTimeoutMilliseconds);
        console.debug("call fetchData1");
        const data = await response.json();
  
        if (data.Status === '0') {
          if (data.Connections) {
            success = true;
            setAPIURL(data.Connections.gateway);
            // setAPIURL('http://192.168.123.42:8889/');
            setApplauncherStatus(success);
            setLv3Registration(data.Connections.lv3Registration)
          }
        } else if (data.Status === '-2') {
          // Alert.alert(t('common.error.forceUpdate'), "", [
          //   { text: t('common.ok'), onPress: () => BackHandler.exitApp() }
          // ]);
          success = true;
          setApplauncherStatus(success);
          applauncherErrorCode = -2;
        }
      }
      catch (err) {
        console.error(err);
      }
    }

    if (!success) {
      Alert.alert("Error", errorMessage, [
        { text: "Retry", onPress: () => fetchData() }
      ]);
    }
  }

  useEffect(() => {
    console.debug("Get launcher");

    if (JailMonkey.isJailBroken()) {
      Alert.alert("Error", "Detected Root or Jailbreak Device", [
        { text: "OK", onPress: () => {BackHandler.exitApp()} }
      ]);
    }
    else
    {
      fetchData().catch(console.error);
    }
    

    LogBox.ignoreLogs([
      'Non-serializable values were found in the navigation state',
    ]);
    // analytics().setUserId(null)
  }, [])

  useEffect(() => {
    if (applauncherStatus === true) {
      console.debug("SplashScreen Hide");
      // SplashScreen.hide();
    }
  }, [applauncherStatus]);

  Amplify.configure({
    Auth: {
      region: cognito_region,
      userPoolId: cognito_userPoolId,
      userPoolWebClientId: cognito_userPoolWebClientId
    }
  });

  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor} >
        {
          applauncherStatus === true ?
            < WebSocketProvider >
              <AppContainer />
            </WebSocketProvider> :
            <></>
        }
      </PersistGate>
    </Provider >
  );
};

const AppContainer = () => {
  const routeNameRef = React.useRef(null);
  const navigationRef = React.useRef(null);
  
  const Stack = createNativeStackNavigator();

  const dispatch = useDispatch();
  const { Reset, SetAccountInfo, ResetAccount } = bindActionCreators(AccountActionCreators, dispatch);
  const { SetSetting, SetIsShowMessageDialog, SetRegionList, SetCountryList, SetIsShowLoading, SetIsShowToastDialog, SetIsShowProgress, SetOnTouch, SetPortfolioCurrentTab, SetIsManualLogout, SetAllowTrack } = bindActionCreators(StateActionCreators, dispatch);
  const { SetCurrentToken } = bindActionCreators(CognitoActionCreators, dispatch);
  const { SetConnectionStatus } = bindActionCreators(ConnectionActionCreators, dispatch);
  const { SetLoginTempCredentials } = bindActionCreators(CognitoTempActionCreators, dispatch);
  
  const { signOut } = AccountUtils();

  const theme = useSelector((state: State) => state.state.setting?.Theme);
  const connectionStatus = useSelector((state: State) => state.connection.webSocketStatus);
  const account = useSelector((state: State) => state.account.account);
  const userId = useSelector((state: State) => state.state.userId);
  const region = useSelector((state: State) => state.state.setting?.UserRegion)
  const isFirstTime = useSelector((state: State) => state.state.setting?.IsFirstTime)
  const pushyDeviceID = useSelector((state: State) => state.state.setting?.PushyDeviceID)
  const setting = useSelector((state: State) => state.state.setting)
  const tradeAccount = useSelector((state: State) => state.trading.accountDetail?.Login);
  const isSwitchingConnection = useSelector((state: State) => state.connection.isSwitchingConnection);
  const currentToken = useSelector((state: State) => state.cognito.currentToken);
  const userLanguage = useSelector((state: State) => state.state.setting?.UserLanguage?? undefined);
  const isIOSLocaleFixed = useSelector((state: State) => state.state.setting?.IsIOSLocaleFixed ?? undefined);
  const isAndroidLocaleFixed = useSelector((state: State) => state.state.setting?.IsAndroidLocaleFixed ?? undefined);
  const isLogined = useSelector((state: State) => state.account.isLogined);
  const onTouch = useSelector((state: State) => state.state.onTouch);
  const IsEnableBiometrics = useSelector((state: State) => state.state.setting.IsEnableBiometrics);
  const isManualLogout = useSelector((state: State) => state.state.isManualLogout);
  const allowTrack = useSelector((state: State) => state.state.allowTrack);
  const ga4UserPropertyUUID = useSelector((state: State) => state.state.ga4UserPropertyUUID);

  const appState = useAppState();

  const [codePushReady, setCodePushReady] = useState<boolean>(false);
  const [permissionReady, setPermissionReady] = useState<boolean>(false);
  const [isRefreshingToken, setIsRefreshingToken] = useState<boolean>(false);

  const getPushyDeviceId = () => {
    Pushy.listen();
    Pushy.register().then((deviceToken: string) => {
      // console.debug('Pushy ' + deviceToken);
      SetSetting({
        ...setting,
        PushyDeviceID: deviceToken
      });
    }).catch((err: Error) => {
      // Handle registration errors
      console.error(err);
    });
  }

  const doCodePushUpdate = async () => {
    var key = Platform.OS === "android" ? androidCodePushDeploymentKey : iosCodePushDeploymentKey;
    await CodePush.sync(
      { deploymentKey: key, installMode: CodePush.InstallMode.IMMEDIATE, rollbackRetryOptions: {delayInHours: 1, maxRetryAttempts: 5} },
      codePushStatusDidChange,
      codePushDownloadDidProgress
    )
  }

  const codePushStatusDidChange = (syncStatys) => {
    if (syncStatys === CodePush.SyncStatus.DOWNLOADING_PACKAGE || syncStatys === CodePush.SyncStatus.INSTALLING_UPDATE) {
      SetIsShowProgress({ isShowDialog: true, progress: 0, total: 100 });
    } else if (syncStatys === CodePush.SyncStatus.UPDATE_INSTALLED) {
      SetIsShowProgress({ isShowDialog: false, progress: 0, total: 0 });
      //SetIsShowToastDialog({ isShowDialog: true, message: t('codePush.downloadCompleted') })
    }
    console.debug('codePushStatusDidChange', syncStatys)
  }

  const codePushDownloadDidProgress = (progress) => {
    console.debug('codePushDownloadDidProgress', progress)
    SetIsShowProgress({ isShowDialog: true, progress: progress.receivedBytes, total: progress.totalBytes });
  }

  useEffect(() => {
    if (allowTrack) {
      if (!userId) {
        // analytics().setUserId(null)
        analytics().setUserProperty('uuid', '')
        return
      }
      // analytics().setUserId(null)
      analytics().setUserProperty('uuid', `${ga4UserPropertyUUID}`)
      console.log(`ga4UserPropertyUUID ${ga4UserPropertyUUID}`)
      console.log(`userId ${userId}`)
    } else {
      // analytics().setUserId(null)
      analytics().setUserProperty('uuid', '')
    }
  }, [userId, allowTrack, ga4UserPropertyUUID])

  useEffect(() => {
    SetSetting({
      Theme: 'light'
    })
  }, [])

  useEffect(() => {
    //Guest Page
    const fetchData = async () => {
      const data = await getRegion();
      //console.debug(`Region Data: ${JSON.stringify(data)}`);
      if (data) {
        // const regionList: string[] = data.map((e: any) => e.regionCode);
        const regionDataList: RegionContent[] = data.map((e: any) => {
          return {
            regionCode: e.regionCode,
            countryCode: e.countryCode,
            countrySymbol: e.countrySymbol
          }
        });
        SetRegionList(regionDataList)
      }
    }
    fetchData().catch(console.error);
  }, []);

  
  const doCheckAppUpdate = async () =>
  {
    let isCheckedUpdate = false;
    
    var key = Platform.OS === "android" ? androidCodePushDeploymentKey : iosCodePushDeploymentKey;
    try
    {
      console.debug("Before check update")
      const update = await CodePush.checkForUpdate(key)
      isCheckedUpdate = true;
      if (!update || update.failedInstall) { 
        console.debug('update | update.failedInstall')
        CodePush.notifyAppReady();
        setCodePushReady(true); 
      }
      else {
        doCodePushUpdate();
      }
    }
    finally
    {
      console.debug("after checkUpdate: " + isCheckedUpdate)
      if (!isCheckedUpdate)
      {
        Alert.alert("Check Update Error", "Please retry", [
          { text: "Retry", onPress: () => doCheckAppUpdate() }
        ]);
      }
    }
  }

  useEffect(() => {
    getCountryList({
      token: currentToken,
      onSuccess: data => {
        const countryList: CountryContent[] = data.response.map(
          (el: CountryContent) => ({...el}),
        );
        SetCountryList(countryList);
      },
    });
  }, [currentToken]);

  useEffect(() => {
    console.debug("App Start");

    if (userLanguage) {
      i18n.changeLanguage(userLanguage);
    } else {
      const getIOSLanguage = () => {
        var iOSLanguage: string = NativeModules.SettingsManager.settings.AppleLocale
        console.debug('AppleLocale:' + NativeModules.SettingsManager.settings.AppleLocale)
        console.debug('AppleLanguages:' + NativeModules.SettingsManager.settings.AppleLanguages)

        if (iOSLanguage == null) {
          iOSLanguage = NativeModules.SettingsManager.settings.AppleLanguages[0]
        }
        if (iOSLanguage.includes('en')) {
          iOSLanguage = 'en'
        } else if (iOSLanguage.includes('zh-Hant') || iOSLanguage.includes('HK') || iOSLanguage.includes('TW')) {
          iOSLanguage = 'tc'
        }
        else if (iOSLanguage.includes('zh-Hans') || iOSLanguage.includes('CN')) {
          iOSLanguage = 'sc'
        }
        else {
          iOSLanguage = 'en'
        }
        console.debug('App, iOSLanguage:' + iOSLanguage)
        return iOSLanguage
      }
      const getAdnroidLanguage = () => {
        var androidLanguage = NativeModules.I18nManager.localeIdentifier
        console.debug('androidI18nManager.localeIdentifier:' + NativeModules.I18nManager.localeIdentifier)

        androidLanguage = !androidLanguage.includes('en') ? (androidLanguage.includes('HK') || androidLanguage.includes('TW')) ? 'tc' : (androidLanguage.includes('CN') ? 'sc' : 'en') : 'en'

        return androidLanguage
      }

      let language = Platform.OS === 'ios' ? getIOSLanguage() : getAdnroidLanguage()
      i18n.changeLanguage(language);
    }

    const displayForceUpdate = () => {
      SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.forceUpdateTitle'), message: t('common.error.forceUpdateMessage'), btnLabel: t('common.error.forceUpdateButton'), callback: async() => {
          await Linking.openURL(Platform.OS === 'ios' ? appStoreLink : playStoreLink)
          displayForceUpdate();
        }
      })
    }

    if (applauncherErrorCode === -2) {
      displayForceUpdate();
      return;
    }

    SetIsShowLoading(false);
    if (region) {
      SetSetting({
        ...setting,
        IsFirstTime: false
      })
    }

    doCheckAppUpdate()

    if (Platform.OS === 'ios' && !isIOSLocaleFixed) {
      const fixIOSLocale = () => {
        var iOSLocaleFixed: string = NativeModules.SettingsManager.settings.AppleLocale ?? NativeModules.SettingsManager.settings.AppleLanguages[0]
        console.debug('AppleLocale:' + NativeModules.SettingsManager.settings.AppleLocale)
        console.debug('AppleLanguages:' + NativeModules.SettingsManager.settings.AppleLanguages)

        if (iOSLocaleFixed.includes('en')) {
          iOSLocaleFixed = 'en'
        } else if (iOSLocaleFixed.includes('zh-Hant') || iOSLocaleFixed.includes('yue-Hant')) {
          iOSLocaleFixed = 'tc'
        }  else if (iOSLocaleFixed.includes('zh-Hans')) {
          iOSLocaleFixed = 'sc'
        } else if (iOSLocaleFixed.includes('HK') || iOSLocaleFixed.includes('TW')) {
          iOSLocaleFixed = 'tc'
        } else if (iOSLocaleFixed.includes('CN')) {
          iOSLocaleFixed = 'sc'
        } else {
          iOSLocaleFixed = 'en'
        }
        console.debug('App, iOSLanguage FIXED:' + iOSLocaleFixed)
        return iOSLocaleFixed
      }

      i18n.changeLanguage(fixIOSLocale());

      SetSetting({
        // UserLanguage: i18n.language, // not set
        IsIOSLocaleFixed: true
      })
    }

    if (Platform.OS === 'android' && !isAndroidLocaleFixed) {
      const fixAndroidLocale = () => {
        var androidLocaleFixed = NativeModules.I18nManager.localeIdentifier
        console.debug('androidI18nManager.localeIdentifier:' + NativeModules.I18nManager.localeIdentifier)

        if (androidLocaleFixed.includes('en')) {
          androidLocaleFixed = 'en'
        } else if (androidLocaleFixed.includes('zh') && androidLocaleFixed.includes('_#Hant')) {
          androidLocaleFixed = 'tc'
        } else if (androidLocaleFixed.includes('zh') && androidLocaleFixed.includes('_#Hans')) {
          androidLocaleFixed = 'sc'
        } else if (androidLocaleFixed.includes('HK') || androidLocaleFixed.includes('TW') || androidLocaleFixed.includes('zh_MO')) {
          androidLocaleFixed = 'tc'
        } else if (androidLocaleFixed.includes('CN')) {
          androidLocaleFixed = 'sc'
        } else {
          androidLocaleFixed = 'en'
        }
        console.debug('App, androidLocale FIXED:' + androidLocaleFixed)
        return androidLocaleFixed
      }

      i18n.changeLanguage(fixAndroidLocale());

      SetSetting({
        // UserLanguage: i18n.language, // not set // will be set by Welcome screen anyway
        IsAndroidLocaleFixed: true
      })
    }

    SetIsShowLoading(false);
    SetIsShowMessageDialog({ isShowDialog: false, title: '', message: '' })
    SetIsShowToastDialog({ isShowDialog: false, message: '' })
    SetIsShowProgress({ isShowDialog: false, progress: 0, total: 0 })
    SetPortfolioCurrentTab(PortfolioTabType.Position)
    SetLoginTempCredentials({
      username: '',
      password: ''
  })

    SplashScreen.hide();
  }, []);

  useEffect(() => {
    if (isFirstTime) {
      console.debug("getPushyDeviceId");
      getPushyDeviceId();
    }
  }, [isFirstTime]);

  const { doRefreshToken, doConnectWebsocket } = AccountUtils();
  useEffect(() => {
    const interval = setInterval(async () => {
      console.debug('connectionStatus', connectionStatus, account.LoginLevel)
      if (connectionStatus === Status.Connected) {
        try {
          if (account && (account.LoginLevel === 2 || account.LoginLevel === 3)) {
            console.debug(new Date(), '------------do refreshToken--------------')
            setIsRefreshingToken(true);
            const cognitoUser = await Auth.currentAuthenticatedUser();
            const currentSession = await Auth.currentSession();
            var data = await doRefreshToken(tradeAccount && tradeAccount.toString());
            if (data && data.success) {
              //console.debug("refresh: " + JSON.stringify(data))
              SetAccountInfo({
                AccessToken: data.accessToken,
                LoginLevel: data.loginLevel
              });
            }

            setIsRefreshingToken(false);
          } else if (account && account.LoginLevel === 1) {
            console.debug(new Date(), '------------do refreshToken anylogin--------------')
            if (region) {
              const data = await anyLogin(setting.PushyDeviceID ?? "", 0, region);
              if (data) {
                //console.debug("refresh anyLogin: " + JSON.stringify(data));
                if (data.errorCode === errorType.NORMAL) {
                  setTradingURL(data.tApiPath);
                  setChartURL(data.chartPath, data.chartWSPath);
                  SetAccountInfo({
                    AccessToken: data.accessToken,
                    LoginLevel: 1
                  });
                } else {
                  SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data.errorCode })
                }
              }
            }
          }
        } catch (error) {
          console.debug(error)
        }
      }
    }, refreshTokenMinutes * 60 * 1000);
    return () => clearInterval(interval);
  }, [connectionStatus, account?.LoginLevel, tradeAccount]);

  useEffect(() => {
    const interval = setInterval(async () => {
      try {
        if (account && (account.LoginLevel === 2 || account.LoginLevel === 3)) {
          
          const currentSession = await Auth.currentSession();
          var token = currentSession.getAccessToken().getJwtToken();
          if (token !== currentToken) {
            setIsRefreshingToken(true);
            // console.debug('new cognito token', token)
            var data = await doRefreshToken(tradeAccount && tradeAccount.toString());
            if (data && data.success) {
              SetAccountInfo({
                AccessToken: data.accessToken,
                LoginLevel: data.loginLevel
              });
            }
            SetCurrentToken(token)
            setIsRefreshingToken(false);
          }
        }
      } catch(e) {}

    }, 1000);
    return () => clearInterval(interval);
  }, [account?.LoginLevel, currentToken, tradeAccount]);

  const reconnect = async () => {
    if (account && (account.AccessToken) && account.Path && account.LoginLevel) {

      console.debug(`Reconnect: AppContainer UseEffect4: ${account.DisplayName}, ${tradingAPI}`);
      const state: State = store.getState();
      const isDemo = state.account.isDemoAccount ?? false;

      const status = await doConnectWebsocket(account.Path, false, account.AccessToken!, account.LoginLevel, isDemo, true)
      console.debug(`reconnect status: ${status}`);
      if (status === 403 || status === 401) {
        // token expired, reconnect as guest
        console.debug("Restore Account Info")
        // ResetAccount();
        signOut();
      }
    }
    else {
      console.debug("Not connect");
      const data = await anyLogin(setting.PushyDeviceID ?? "", 0, region);
      if (data) {
        // console.debug(JSON.stringify(data));
        if (data.errorCode === errorType.NORMAL) {
          setTradingURL(data.tApiPath);
          setChartURL(data.chartPath, data.chartWSPath);
          await doConnectWebsocket(data.mtPaths, true, data.accessToken, 1, false, true)
        } else {
          SetIsShowMessageDialog({ isShowDialog: true, title: t('common.error.network'), message: t('common.error.code') + data.errorCode })
        }
      }
    }
  }

  useEffect(() => {

    console.debug(`appState: ${appState}, connectionStatus1: ${connectionStatus}`);
    if (!region) {
      console.debug("no region");
      return;
    }

    console.debug(`debug isSwitchingConnection: ${isSwitchingConnection}`);
    if (appState === 'active' && connectionStatus === Status.Disconneted && !isRefreshingToken && !isSwitchingConnection && applauncherErrorCode !== -2) {

      let needRetry = true;
      let interval
      const reconnectTimeout = () => {
        interval = setTimeout(async() => {
          const start = Date.now()
          console.debug('reconnecting start')
          await reconnect();
          console.debug('reconnecting end: ' + (Date.now() - start))
          if (needRetry)
            reconnectTimeout();
        }, 1000)
      };

      reconnectTimeout();
      return function cleanup() {
        needRetry = false;
        if (interval)
          clearTimeout(interval)
      }
    }


  }, [connectionStatus, appState, region, account, isRefreshingToken, isSwitchingConnection]);

  useEffect(() => {
    
    if (connectionStatus === Status.DuplicateLogin) {
      console.debug("Duplicate Login");
      SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.duplicateLogin'), message: t('common.error.duplicateLoginMsg'), btnLabel: t('common.gotIt'), linkMessage: t('common.error.duplicateLoginLinkMsg'), linkMessageCallBack: ()=>{}})
      signOut();
      // navigationRef.current?.navigate(TabDirectoryTab.QUOTE);
      navigationRef.current?.reset({
        index: 1,
        routes: [{name: 'TabDirectory'}, {name: 'CognitoLogin', params: {backPage: TabDirectoryTab.QUOTE}}]
      });
      //navigationRef.current?.navigate('CognitoLogin', {backPage: TabDirectoryTab.ME});
    }
  }, [connectionStatus]);

  const [intervalTimeout, setIntervalTimeout] = useState<NodeJS.Timer>(null);
  // const [onTouch, setOnTouch] = useState<boolean>(false);
  useEffect(() => {
      if (isLogined) {
          console.debug('onTouch')
          clearInterval(intervalTimeout);
          setIntervalTimeout( setInterval(async () => {
            SetIsShowMessageDialog({isShowDialog: true, title: t('common.error.timeout'), message: t('common.error.timeoutMsg'), btnLabel: t('common.ok')})
              signOut();
              // navigationRef.current?.navigate(TabDirectoryTab.QUOTE);
              //navigationRef.current?.navigate('CognitoLogin', {backPage: TabDirectoryTab.ME});
              navigationRef.current?.reset({
                index: 1,
                routes: [{name: 'TabDirectory'}, {name: 'CognitoLogin', params: {backPage: TabDirectoryTab.QUOTE}}]
              });
              clearInterval(intervalTimeout);
          }, timeoutMinutes * 60 * 1000))
      } else {
        clearInterval(intervalTimeout);
      }
  }, [isLogined, onTouch])

  useEffect(() => {
      if (codePushReady && IsEnableBiometrics && !isManualLogout) {
        //navigationRef.current?.navigate('CognitoLogin', {backPage: TabDirectoryTab.ME});
        navigationRef.current?.reset({
          index: 1,
          routes: [{name: 'TabDirectory'}, {name: 'CognitoLogin', params: {backPage: TabDirectoryTab.QUOTE}}]
        });
      }
  }, [codePushReady])

  useEffect(()=> {
    const fetchData = async () => {
      const trackingStatus = await requestTrackingPermission();
      console.warn('trackingStatus', trackingStatus)
      if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
        // enable tracking features
        SetAllowTrack(true)
      } else {
        SetAllowTrack(false)
      }

      setPermissionReady(true)
    }
      
    fetchData().catch(console.error);
    
  }, [])

  useEffect(() => {
    const setLang = async (lang: string) => {
      const currentSession = await Auth.currentSession();
      const token = await currentSession.getAccessToken().getJwtToken();
      const cognitoId = currentSession.getIdToken().payload['cognito:username'];
      await setUserLanguage({
        id: cognitoId,
        lang,
        token,
        onSuccess: () => {},
        onFailed: () => {},
      });
    };
    if (userLanguage) {
      setLang(userLanguage);
    }
  },[userLanguage])

  const config = {
    screens: {
      Welcome: "welcome",
      CognitoLogin: "login",
      CognitoSignUp: "signup",
      TabDirectory: {
        path: 'tab',
        screens: {
          WatchlistView: 'watchlist'
        }
      },
    }
  }

  return (
    codePushReady && permissionReady ? (
    // <GestureHandlerRootView style={{ flex: 1 }} onTouchStart={()=> {SetOnTouch(!onTouch)}}>
      <GestureHandlerRootView style={{ flex: 1 }}>
      <StatusBar 
        translucent backgroundColor="transparent" 
        barStyle={theme === 'dark' ? 'light-content' : 'dark-content'} />
      <NavigationContainer
        ref={navigationRef}
        onReady={() => {
          routeNameRef.current = navigationRef.current.getCurrentRoute().name
        }}
        onStateChange={async () => {
          const previousRouteName = routeNameRef.current;
          const currentRouteName = navigationRef.current.getCurrentRoute().name
  
          if (previousRouteName !== currentRouteName) {
            console.debug('currentRouteName:' + currentRouteName)
            let screen_name: string = GAScreenName[currentRouteName]
            console.debug('screen_name:' + screen_name)

            // If not found from Mapping JSON, it will NOT be send to GA.
            if (!screen_name || screen_name.length === 0) return
              if (allowTrack) {
                await analytics().logScreenView({
                  screen_name: screen_name,
                  screen_class: currentRouteName,
                });
                console.debug(`[AppContainer NavigationContainer onStateChange] analytics().logScreenView(${JSON.stringify({screen_name: screen_name, screen_class: currentRouteName})})`)
              }
          }
          routeNameRef.current = currentRouteName;
        }}
        theme={theme === 'dark' ? DarkTheme : LightTheme}
        linking={{
          prefixes: ["emp://app"],
          config
        }}
        >
        <Stack.Navigator
          screenOptions={{ header: () => null }}>
          <Stack.Screen name="TabDirectory" component={TabDirectory}
          options={{
            gestureEnabled: false,
          }}/>
          {
            region && isFirstTime === false ? (
              <>
              </>
            ) : (
              <>
                <Stack.Screen name="Welcome" component={Welcome} 
                options={{
                  gestureEnabled: false,
                }}/>
                <Stack.Screen name="Introduction" component={Introduction} />
              </>
            )
          }
          
          <Stack.Screen name="TabDirectoryWebView" component={TabDirectoryWebView} options={{gestureEnabled: false}}/>
          <Stack.Screen name="EditWatchlistView" component={EditWatchlistView} />
          <Stack.Screen name="ShareView" component={ShareView} />
          <Stack.Screen name="SearchProductView" component={SearchProductView} />
          <Stack.Screen name="CognitoForgetPassword" component={CognitoForgetPassword} />
          <Stack.Screen name="CognitoSignUp" component={CognitoSignUp} />
          <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
          <Stack.Screen name="TermsOfService" component={TermsOfService}/>
          <Stack.Screen name="CognitoSignUpVerification" component={CognitoSignUpVerification} />
          <Stack.Screen name="CognitoSignUpCompleted" component={CognitoSignUpCompleted} />
          <Stack.Screen name="CognitoForgetPasswordVerification" component={CognitoForgetPasswordVerification} />
          <Stack.Screen name="CognitoForgetPasswordReset" component={CognitoForgetPasswordReset} />
          <Stack.Screen name="MeSettingChangeCognitoPassword" component={CognitoForgetPasswordReset} />
          <Stack.Screen name="CognitoLogin" component={CognitoLogin} />
          <Stack.Screen name="EnableBiometrics" component={EnableBiometrics} 
            options={{
              gestureEnabled: false,
            }}/>
          <Stack.Screen name="AddDevices" component={AddDevices} />
          <Stack.Screen name="NewOrder" component={NewOrder} />
          <Stack.Screen name="NewOrder+UpdatePendingOrder" component={NewOrder} />
          <Stack.Screen name="ClosePosition" component={ClosePosition} />
          <Stack.Screen name="EditPosition" component={EditPosition} />
          <Stack.Screen name="HistoryQuery" component={HistoryQuery} />
          <Stack.Screen name="PositionDetails" component={PositionDetails} />
          <Stack.Screen name="ContractDetailView" component={ContractDetailView} />
          <Stack.Screen name="MeSetting" component={MeSetting} />
          <Stack.Screen name="MeSettingNotification" component={MeSettingNotification} />
          <Stack.Screen name="MeSettingUser" component={MeSettingUser} />
          <Stack.Screen name="DeviceList" component={DeviceList} />
          <Stack.Screen name="PermissionAccess" component={PermissionAccess} />
          <Stack.Screen name="MeChangeUsername" component={MeChangeUsername} />
          <Stack.Screen name="MeChangeMobileNumber" component={MeChangeMobileNumber} />
          <Stack.Screen name="HistoryChartDetail" component={HistoryChartDetail} />
          <Stack.Screen name="CountryOptionView" component={CountryOptionView} />
          <Stack.Screen name="CaptchaPage" component={CaptchaPage} />
          <Stack.Screen name="ForceChangePassword" component={ForceChangePassword} />
          <Stack.Screen name="UserVerification" component={UserVerification} />
          <Stack.Screen name="MIO" component={MIO} />
          <Stack.Screen name="MeAboutUsScreen" component={AboutUs} />
          <Stack.Screen name="MeMyRewardScreen" component={MyRewardScreen} />
          <Stack.Screen name="AccountQuestion" component={AccountQuestion} />
          <Stack.Screen
            name="RecommendedTradingAccount"
            component={RecommendedTradingAccount}
          />
          <Stack.Screen name="AccountAgreement" component={AccountAgreement} />
          <Stack.Screen
            name="AccountGettingReady"
            component={AccountGettingReady}
          />
          <Stack.Screen
            name="AccountPersonalInformation"
            component={AccountPersonalInformation}
          />
          <Stack.Screen
            name="AccountTermsCondition"
            component={AccountTermsCondition}
          />
          <Stack.Screen
            name="AccountContactInformation"
            component={AccountContactInformation}
          />
          <Stack.Screen
            name="AccountFinancialInformation"
            component={AccountFinancialInformation}
          />
          <Stack.Screen
            name="AccountTradingExperience"
            component={AccountTradingExperience}
          />
          <Stack.Screen
            name="AccountReviewApplication"
            component={AccountReviewApplication}
          />
          <Stack.Screen name="AccountPromotion" component={AccountPromotion} />
          <Stack.Screen
            name="AccountApplicationStatus"
            component={AccountApplicationStatus}
            options={{gestureEnabled: false}}
          />
          <Stack.Screen name="AccountStatus" component={AccountStatus} />
          <Stack.Screen name="EkycScreen" component={EkycScreen} />
          <Stack.Screen name="DropdownPage" component={DropdownPage} />
          <Stack.Screen
            name="CountryDropdownPage"
            component={CountryDropdownPage}
          />
        </Stack.Navigator >
        <MessageDialog />
        <OrderConfirmDialog />
        <BottomSelection />
        <ToastDialog autoDisableTimeout={dialogDisplaySecond * 1000} />
        <LoadingDialog />
        <AddDeviceDialog autoDisableTimeout={dialogDisplaySecond * 1000}/>
        <BottomSheetDropdown />
        <DepositDialog />
      </NavigationContainer >
    </GestureHandlerRootView >)
      : ( 
        <GestureHandlerRootView style={{ flex: 1 }}>
          <StatusBar 
            translucent backgroundColor="transparent" 
            barStyle={theme === 'dark' ? 'light-content' : 'dark-content'} />
          <NavigationContainer
            theme={theme === 'dark' ? DarkTheme : LightTheme}
            linking={{
              prefixes: ["emp://app"],
              config
            }}>
            <Stack.Navigator
              screenOptions={{ header: () => null }}>
              <Stack.Screen name="CheckUpdate" component={CheckUpdate} />
            </Stack.Navigator >
            <MessageDialog />
            <LoadingDialog />
          </NavigationContainer >
        </GestureHandlerRootView >
        )
  )
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  input: {
    width: 200,
    borderWidth: 1,
    borderColor: '#555',
    borderRadius: 5,
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  }
});

// export default codePush(codePushOptions)(App)
export default App