package com.mfinance.copymaster.nativeModules;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;
import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_WEAK;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;


public class BiometricNativeModule extends ReactContextBaseJavaModule {
    BiometricNativeModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "BiometricNativeModule";
    }

    private BiometricManager getBiometricManager() {
        return BiometricManager.from(this.getReactApplicationContext());
    }

    private int bioType = BIOMETRIC_STRONG | BIOMETRIC_WEAK;

    private boolean isBiometricCompatibleDevice() {
        BiometricManager biometricManager = BiometricManager.from(this.getReactApplicationContext());
        switch (biometricManager.canAuthenticate(bioType)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                System.out.println("ReactMethod BIOMETRIC_SUCCESS");
                return true;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                System.out.println("ReactMethod BIOMETRIC_ERROR_NO_HARDWARE");
                return false;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                System.out.println("ReactMethod BIOMETRIC_ERROR_HW_UNAVAILABLE");
                return false;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                System.out.println("ReactMethod BIOMETRIC_ERROR_NONE_ENROLLED");
                return false;
        }
        return false;
    }

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private Promise promise;
    private String language = "en";

    public final class RESULT {
        public static final int NOT_REGISTERED = -1;
        public static final int FAILED = 0;
        public static final int SUCCESS = 1;
    }

    private void startDialog() {
        executor = ContextCompat.getMainExecutor(this.getReactApplicationContext());
        biometricPrompt = new BiometricPrompt((FragmentActivity) this.getCurrentActivity(), executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
//                System.out.println("onAuthenticationError");
//                Toast.makeText(getApplicationContext(),
//                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
//                        .show();
                promise.resolve(errorCode);
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
//                System.out.println("onAuthenticationSucceeded");
//                Toast.makeText(getApplicationContext(),
//                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                promise.resolve(RESULT.SUCCESS);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
//                System.out.println("onAuthenticationFailed");
//                Toast.makeText(getApplicationContext(), "Authentication failed",
//                        Toast.LENGTH_SHORT)
//                        .show();
//                callBack.invoke(RESULT.FAILED);
            }
        });

        String title = language.equals("tc") ? "生物認證" : (language.equals("sc") ? "生物认证" : "Biometric login");
        String subTitle = language.equals("tc") ? "使用您的生物認證登錄" : (language.equals("sc") ? "使用您的生物认证登录" : "Log in using your biometric credential");
        String cancel = language.equals("tc") ? "取消" : (language.equals("sc") ? "取消" : "Cancel");

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(title)
                .setSubtitle(subTitle)
                .setAllowedAuthenticators(bioType)
                .setNegativeButtonText(cancel)
                .build();

        this.getCurrentActivity().runOnUiThread(() -> {
            biometricPrompt.authenticate(promptInfo);
        });

//        });
    }

    @ReactMethod
    public void startBiometric(String language, Promise promise) {
        this.language = language;
        this.promise = promise;
//        System.out.println("ReactMethod startBiometric "+ language);
//        System.out.println("ReactMethod isBiometricCompatibleDevice " + isBiometricCompatibleDevice());

        Integer result = RESULT.NOT_REGISTERED;
        if (isBiometricCompatibleDevice()) {
            startDialog();
        } else {
            promise.resolve(result);
        }
    }
}
