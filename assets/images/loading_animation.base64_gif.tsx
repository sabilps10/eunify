import { entityId } from '../../src/config/constants'
import EIE from './loading_EIE.1320x600.base64_gif'
import EMX from './loading_EMX.1325x500.base64_gif'

export default {
    EIE: EIE,
    EMX: EMX
}[entityId]
