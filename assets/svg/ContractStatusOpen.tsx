import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { ClipPath, Defs, G, Path, Rect } from "react-native-svg"

const ContractStatusOpen = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={18}
            height={18}
            viewBox="0 0 18 18"
        >
            <Defs>
                <ClipPath id="a">
                    <Path
                        data-name="Rectangle 1229"
                        transform="translate(5353 -2779)"
                        fill={backgroundColor}
                        stroke={color}
                        strokeWidth={1}
                        d="M0 0H18V18H0z"
                    />
                </ClipPath>
            </Defs>
            <G transform="translate(-5353 2779)" clipPath="url(#a)">
                <Path
                    d="M7.992.5A7.5 7.5 0 1015.5 8 7.5 7.5 0 007.992.5zM8 14a6 6 0 116-6 6 6 0 01-6 6z"
                    transform="translate(5353.862 -2777.859)"
                    fill="#00b1d2"
                />
            </G>
        </Svg>
    )
}

export default ContractStatusOpen
