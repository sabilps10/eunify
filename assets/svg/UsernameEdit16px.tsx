import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const UsernameEdit16px= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={16}
          height={16}
          viewBox="0 0 16 16"
      >
          <G transform="translate(-952 -1044) translate(952 1044)">
              <G fill={color}>
                  <Path
                      data-name="Path 1407"
                      d="M3.188 12.313V3.188h5.145V2H2.5a.5.5 0 00-.5.5V13a.5.5 0 00.5.5h10.833a.5.5 0 00.5-.5V6.667h-1.187v5.646z"
                  />
                  <Path
                      data-name="Path 1408"
                      d="M11.3 2a.542.542 0 01.383.159l1.978 1.979a.542.542 0 010 .766l-5.6 5.6a.541.541 0 01-.383.159H5.7a.542.542 0 01-.542-.542V8.153a.542.542 0 01.158-.383l5.6-5.612A.542.542 0 0111.3 2zm0 1.308L6.242 8.377v1.206h1.215l5.06-5.062z"
                      fillRule="evenodd"
                  />
              </G>
              <Path data-name="Rectangle 1041" fill={backgroundColor} d="M0 0H16V16H0z" />
          </G>
      </Svg>
  )
}

export default UsernameEdit16px
