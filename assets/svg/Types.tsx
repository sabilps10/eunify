import { ColorValue, ViewStyle } from "react-native";

export interface IconInterface {
    backgroundColor?: ColorValue,
    color?: ColorValue,
    style?: ViewStyle,
  }
  