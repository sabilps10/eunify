import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MeHelp= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1589 -1014)">
              <G data-name="Group 626">
                  <Path
                      data-name="Path 1364"
                      d="M218.188 564.875h1a3.019 3.019 0 01.281-1.2 5.916 5.916 0 011-1.141 5.5 5.5 0 001.078-1.265 2.58 2.58 0 00.328-1.3 2.78 2.78 0 00-.906-2.125 3.441 3.441 0 00-4.094-.313 3.422 3.422 0 00-1.25 1.5l.938.406a3.156 3.156 0 01.875-1.093 2.024 2.024 0 011.25-.375 2.248 2.248 0 011.562.562 1.847 1.847 0 01.625 1.438 1.886 1.886 0 01-.3 1.031 4.858 4.858 0 01-1.015 1.062 4.633 4.633 0 00-1.079 1.266 3.408 3.408 0 00-.293 1.547zm.531 3.5a.969.969 0 000-1.937.911.911 0 00-.688.3.944.944 0 000 1.344.912.912 0 00.688.293zm-7.156 3.25a1.815 1.815 0 01-1.813-1.812v-14.25a1.815 1.815 0 011.813-1.813h14.25a1.813 1.813 0 011.812 1.813v14.25a1.813 1.813 0 01-1.812 1.812zm0-1.062h14.25a.807.807 0 00.75-.75v-14.25a.8.8 0 00-.75-.75h-14.25a.8.8 0 00-.75.75v14.25a.807.807 0 00.75.75zM208.188 575a1.815 1.815 0 01-1.813-1.812v-15.313h1.063v15.313a.807.807 0 00.75.75H223.5V575z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1010"
                  transform="translate(1589 1014)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeHelp
