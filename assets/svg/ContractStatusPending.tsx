import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { ClipPath, Defs, G, Path, Rect } from "react-native-svg"

const ContractStatusPending = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={18}
            height={18}
            viewBox="0 0 18 18"
        >
        <Defs>
          <ClipPath id="a">
            <Path data-name="Rectangle 1232" fill={backgroundColor} d="M0 0H18V18H0z" />
          </ClipPath>
        </Defs>
        <G clipPath="url(#a)" fill={color}>
          <Path
            data-name="Path 1813"
            d="M8.993 1.5A7.5 7.5 0 1016.5 9a7.5 7.5 0 00-7.507-7.5zM9 15a6 6 0 116-6 6 6 0 01-6 6z"
          />
          <Path
            data-name="Path 1814"
            d="M9.375 5.25H8.25v4.5l3.938 2.363.563-.923-3.375-2z"
          />
        </G>
      </Svg>
    )
}

export default ContractStatusPending
