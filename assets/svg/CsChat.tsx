import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const CsChat= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1799 -1333)">
              <Path
                  data-name="Path 1403"
                  d="M12 22.2v-1.5h6.7a.29.29 0 00.213-.088.32.32 0 00.087-.237V19.5h-3.55v-7.075H19V11a6.745 6.745 0 00-2.05-4.95A6.745 6.745 0 0012 4a6.745 6.745 0 00-4.95 2.05A6.745 6.745 0 005 11v1.425h3.55V19.5H5.3a1.793 1.793 0 01-1.8-1.8V11a8.175 8.175 0 01.675-3.3A8.583 8.583 0 018.7 3.175a8.4 8.4 0 016.6 0A8.583 8.583 0 0119.825 7.7 8.175 8.175 0 0120.5 11v9.375a1.766 1.766 0 01-.525 1.3 1.736 1.736 0 01-1.275.525zM5.3 18h1.75v-4.075H5V17.7a.3.3 0 00.3.3zm11.65 0H19v-4.075h-2.05zm-9.9 0z"
                  transform="translate(1799 1332.5)"
                  fill={color}
              />
              <Path
                  data-name="Rectangle 1038"
                  transform="translate(1799 1333)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default CsChat
