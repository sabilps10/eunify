import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg"

const FavAdded= (config: IconInterface) => {

    const { color, backgroundColor } = config;

    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24">
                <G id="fav_added" transform="translate(-1443 -1150)">
                    <G
                        id="Group_343"
                        data-name="Group 343"
                        transform="translate(1118.5 1048)"
                    >
                <Path
                    id="Path_771"
                    data-name="Path 771"
                    d="M331.275,121.7a.866.866,0,0,1-.863-.063.848.848,0,0,1-.412-.762V107.3a1.793,1.793,0,0,1,1.8-1.8h9.4a1.793,1.793,0,0,1,1.8,1.8v13.575a.849.849,0,0,1-.413.762.864.864,0,0,1-.862.063l-5.225-2.25Z"
                    fill={color} />
                </G>
                <Rect
                    id="Rectangle_1017"
                    data-name="Rectangle 1017"
                    width={24}
                    height={24}
                    transform="translate(1443 1150)"
                    fill={backgroundColor} />
            </G>
        </Svg>
    )
}

export default FavAdded
