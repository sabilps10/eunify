import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path} from "react-native-svg"

const MeMyProfile= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1441 -861)">
              <G data-name="Group 616">
                  <Path
                      data-name="Path 1352"
                      d="M71.563 413.813h5.093v-1h-5.093zm0 2.906h5.093v-1h-5.093zM71.344 407h7.093a1.813 1.813 0 011.813 1.812v12.625a1.813 1.813 0 01-1.813 1.813H59.563a1.813 1.813 0 01-1.813-1.813v-12.625A1.813 1.813 0 0159.563 407h7.093v-5.187a1.068 1.068 0 011.063-1.063h2.562a1.007 1.007 0 01.766.313 1.049 1.049 0 01.3.75zm-3.625 1.062h2.562v-6.249h-2.562zm-2.531 7.657a1.438 1.438 0 10-1-2.453 1.4 1.4 0 000 2.031 1.4 1.4 0 001 .422zm-3.375 3h6.718v-.157a1.424 1.424 0 00-.235-.8 1.321 1.321 0 00-.609-.516 7.216 7.216 0 00-1.312-.391 6.535 6.535 0 00-1.187-.109 6.229 6.229 0 00-1.219.125 8.94 8.94 0 00-1.312.375 1.325 1.325 0 00-.61.516 1.426 1.426 0 00-.234.8zm4.843-10.657h-7.093a.747.747 0 00-.75.75v12.625a.745.745 0 00.75.75h18.874a.745.745 0 00.75-.75v-12.625a.747.747 0 00-.75-.75h-7.093a.908.908 0 01-.359.844 1.888 1.888 0 01-.985.219h-2a1.888 1.888 0 01-.985-.219.908.908 0 01-.359-.844z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1000"
                  transform="translate(1441 861)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeMyProfile
