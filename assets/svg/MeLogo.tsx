import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path,Circle,Text,TSpan , SvgXml} from "react-native-svg"
import {Image} from "react-native"

const MeLogo= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  const xml = `
  <?xml version="1.0" encoding="UTF-8"?>
<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 30 30">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient);
      }

      .cls-2, .cls-3 {
        fill: #00b2d3;
      }

      .cls-3 {
        fill-rule: evenodd;
      }
    </style>
    <linearGradient id="linear-gradient" x1="16.4" y1="15.39" x2="16.4" y2="24.61" gradientUnits="userSpaceOnUse">
      <stop offset=".4" stop-color="#ffa200"/>
      <stop offset=".72" stop-color="#ffce00"/>
    </linearGradient>
  </defs>
  <path class="cls-3" d="M17.72,3.24c-1.44,.14-2.57,1.31-2.68,2.76-.1-1.45-1.23-2.61-2.67-2.76,1.43-.14,2.57-1.3,2.68-2.74,.11,1.44,1.24,2.6,2.67,2.74Z"/>
  <path class="cls-1" d="M19.56,24.32s-.04-.02-.06-.04c-.03-.02-.05-.05-.08-.07-.03-.02-.06-.05-.09-.08-.08-.07-.16-.15-.23-.22-.44-.48-.69-1.09-.73-1.7-.02-.16-.04-.32-.05-.44v-2.01h0c0-2.04-3.2-1.82-4.53-1.82-.69,0-1.37,0-2.06,0v8.26c0,1.81,1.47,3.28,3.28,3.28,.92,0,1.75-.38,2.35-.99h0s3.69-3.68,3.69-3.68c-.53-.02-1.05-.19-1.49-.51Z"/>
  <g>
    <path class="cls-2" d="M28.87,12.4c-1.28-1.28-3.36-1.28-4.64,0l-5.91,5.89v3.49c0,.12,.02,.27,.05,.44,.04,.61,.29,1.21,.73,1.7,.07,.08,.15,.15,.23,.22,.03,.03,.06,.05,.09,.08,.03,.02,.05,.05,.08,.07,.02,.01,.04,.02,.06,.04,.45,.32,.97,.48,1.49,.51l-.14,.14,7.95-7.93c1.28-1.28,1.28-3.36,0-4.64Z"/>
    <path class="cls-2" d="M15.05,11.39h-6.92c-.06,0-1.07-.07-1.89-.51-.29-.13-.56-.32-.8-.56-.43-.43-.66-.89-.76-1.44l-3.53,3.53c-.59,.59-.98,1.35-.98,2.26,0,1.81,1.47,3.28,3.28,3.28,3.45,0,6.88,0,10.35,0,1.33,0,4.53-.22,4.53,1.82v-5.1c0-.08,0-.17-.01-.26-.13-1.69-1.54-3.02-3.27-3.02Z"/>
  </g>
</svg>
  `
  return (
    <Image source={require('../icon/Eunify-logo-v1-blue.png')} />

    // <SvgXml xml={xml} width={30} height={30} color="#ffffff"></SvgXml>
    //   <Svg
    //       xmlns="http://www.w3.org/2000/svg"
    //       width={config.style?.width ?? 30}
    //       height={config.style?.height ?? 30}
    //       viewBox="0 0 30 30"
    //   >
    //       <G transform="translate(-1663 -1014)">
    //           <Circle
    //               data-name="Ellipse 79"
    //               cx={13.636}
    //               cy={13.636}
    //               r={13.636}
    //               transform="translate(1664.363 1015.364)"
    //               fill={backgroundColor}
    //           />
    //           <Text
    //               transform="translate(1664.997 1032.064)"
    //               fontSize={9}
    //               fill={color}
    //               fontFamily="Montserrat-Regular, Montserrat"
    //           >
    //               <TSpan x={0} y={0}>
    //                   {"LOGO"}
    //               </TSpan>
    //           </Text>
    //           <Path
    //               data-name="Rectangle 1011"
    //               transform="translate(1663 1014)"
    //               d="M0 0H30V30H0z"
    //           />
    //       </G>
    //   </Svg>
    
  )
}

export default MeLogo
