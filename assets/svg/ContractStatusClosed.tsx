import * as React from "react"
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg"
import { IconInterface } from "./Types";

const ContractStatusClosed = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={18}
            height={18}
            viewBox="0 0 18 18"
        >
            <Defs>
                <ClipPath id="a">
                    <Path
                        data-name="Rectangle 1231"
                        transform="translate(5403 -2779)"
                        fill={backgroundColor}
                        stroke={color}
                        strokeWidth={1}
                        d="M0 0H18V18H0z"
                    />
                </ClipPath>
            </Defs>
            <G transform="translate(-5403 2779)" clipPath="url(#a)">
                <Path
                    d="M11.443 4.685L6.5 9.627 3.807 6.943 2.75 8l3.75 3.75 6-6zM8 .5A7.5 7.5 0 1015.5 8 7.5 7.5 0 008 .5zM8 14a6 6 0 116-6 6 6 0 01-6 6z"
                    transform="translate(5404 -2778)"
                    fill="#4abdac"
                />
            </G>
        </Svg>
    )
}

export default ContractStatusClosed
