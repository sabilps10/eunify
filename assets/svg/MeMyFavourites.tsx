import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MeMyFavourites= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1515 -1014)">
              <G data-name="Group 625">
                  <Path
                      data-name="Path 1363"
                      d="M151.844 573.406a.544.544 0 01-.375-.14.5.5 0 01-.157-.391v-17.312a.8.8 0 00-.75-.75h-12.718a.534.534 0 01-.531-.532.533.533 0 01.531-.531h12.718a1.813 1.813 0 011.813 1.813v17.312a.5.5 0 01-.156.391.543.543 0 01-.375.14zm-17.156 1.813l6.624-2.844 6.625 2.844v-16.281a.8.8 0 00-.75-.75h-11.749a.8.8 0 00-.75.75zm.218 1.125a.9.9 0 01-.875-.078.86.86 0 01-.406-.766v-16.562a1.813 1.813 0 011.813-1.813h11.749a1.731 1.731 0 011.3.531 1.774 1.774 0 01.516 1.282V575.5a.86.86 0 01-.406.766.848.848 0 01-.844.078l-6.438-2.75zm-.218-18.156z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1009"
                  transform="translate(1515 1014)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeMyFavourites
