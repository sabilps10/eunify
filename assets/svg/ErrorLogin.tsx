import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { ClipPath, Defs, G, Path, Rect } from "react-native-svg"

const ErrorLogin = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={66}
            height={66}
            viewBox="0 0 66 66"
        >
            <Defs>
            <ClipPath id="a">
                <Path
                data-name="Rectangle 1233"
                transform="translate(1063 956)"
                fill="#fff"
                stroke="#707070"
                strokeWidth={1}
                d="M0 0H66V66H0z"
                />
            </ClipPath>
            </Defs>
            <G transform="translate(-1063 -956)" clipPath="url(#a)">
                <G data-name="Google Icon">
                    <Path
                    data-name="Path 1815"
                    d="M33.069 36.3a1.191 1.191 0 00.825-.31 1.1 1.1 0 00.343-.858V20.625a1.05 1.05 0 00-.343-.756 1.17 1.17 0 00-1.995.825V35.2a1.013 1.013 0 00.345.79 1.188 1.188 0 00.825.31zM33 45.238a1.609 1.609 0 001.135-.412 1.428 1.428 0 00.445-1.1 1.581 1.581 0 10-3.161 0 1.428 1.428 0 00.445 1.1 1.609 1.609 0 001.136.412zm0 12.512A24.556 24.556 0 018.25 33 24.556 24.556 0 0133 8.25 24.6 24.6 0 0157.75 33 24.6 24.6 0 0133 57.75zm0-2.338a21.593 21.593 0 0015.847-6.565A21.593 21.593 0 0055.412 33a21.593 21.593 0 00-6.565-15.847A21.593 21.593 0 0033 10.588a21.593 21.593 0 00-15.847 6.565A21.593 21.593 0 0010.588 33a21.593 21.593 0 006.565 15.847A21.593 21.593 0 0033 55.412z"
                    fill="#f4465e"
                    transform="translate(1062.75 955.75)"
                    />
                </G>
            </G>
        </Svg>
    )
}

export default ErrorLogin
