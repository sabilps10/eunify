import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartBars = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 963" fill={backgroundColor} d="M0 0H30V30H0z" />
          <G data-name="Group 601" fill={color}>
              <G data-name="Group 599" transform="translate(-339.722 -508.595)">
                  <Path
                      data-name="Rectangle 964"
                      transform="translate(344.333 523.958)"
                      d="M0 0H0.5V1H0z"
                  />
                  <Path
                      data-name="Path 1327"
                      d="M363.109 524.958h-1.015v-1h1.015zm-2.03 0h-1.016v-1h1.016zm-2.031 0h-1.015v-1h1.015zm-2.03 0H356v-1h1.016zm-2.031 0h-1.016v-1h1.016zm-2.031 0h-1.015v-1h1.015zm-2.031 0h-1.015v-1h1.015zm-2.031 0h-1.015v-1h1.015zm-2.03 0h-1.015v-1h1.015z"
                  />
                  <Path
                      data-name="Rectangle 965"
                      transform="translate(364.125 523.958)"
                      d="M0 0H0.5V1H0z"
                  />
              </G>
              <G data-name="Group 600">
                  <Path
                      data-name="Path 1328"
                      d="M347.431 523.25l1.81-3.544 1.617 1.787 1.56-3.588 2.812 5.345h1.13l-4.028-7.655-1.773 4.078-1.55-1.713-2.7 5.29z"
                      transform="translate(-339.722 -508.595)"
                  />
                  <Path
                      data-name="Path 1329"
                      d="M363.7 523.25l1.367-2.646-.889-.459-1.6 3.1z"
                      transform="translate(-339.722 -508.595)"
                  />
                  <Path
                      data-name="Path 1330"
                      d="M361.263 525.792l-1.767 3.419-1.8-3.419h-1.13l2.936 5.581 2.884-5.581z"
                      transform="translate(-339.722 -508.595)"
                  />
                  <Path
                      data-name="Path 1331"
                      d="M345.01 525.792l-1.288 2.523.891.455 1.52-2.977z"
                      transform="translate(-339.722 -508.595)"
                  />
              </G>
          </G>
      </Svg>
  )
}

export default ChartBars
