import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const HelpEmail= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1726 -943)">
              <G data-name="Google Icon">
                  <Path
                      data-name="Path 1405"
                      d="M5.563 23.75a1.814 1.814 0 01-1.813-1.812V8.063A1.814 1.814 0 015.563 6.25h18.874a1.814 1.814 0 011.813 1.813v13.875a1.814 1.814 0 01-1.813 1.813zM15 14.938L4.813 8.156v13.782a.746.746 0 00.75.75h18.874a.746.746 0 00.75-.75V8.156zm0-1.219l9.781-6.406H5.25zM4.813 21.938a.746.746 0 00.75.75h-.75v-.75z"
                      fill={color}
                      transform="translate(1726.25 942.75)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1040"
                  transform="translate(1726 943)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default HelpEmail
