import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartCandles= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 931" fill={backgroundColor} d="M0 0H30V30H0z" />
          <Path
              data-name="Union 2"
              d="M7421 2099v-3.5h-2v-14h2v-3.5h1v3.5h2v14h-2v3.5zm-1-4.5h3v-12h-3zm9 1v-2.5h-2v-9h2v-2.5h1v2.5h2v9h-2v2.5zm-1-3.5h3v-7h-3z"
              transform="translate(-7410 -2073)"
              fill={color}
          />
      </Svg>
  )
}

export default ChartCandles
