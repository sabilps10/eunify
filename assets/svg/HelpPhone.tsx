import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const HelpPhone= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1682 -943)">
              <G data-name="Google Icon-1">
                  <Path
                      data-name="Path 1404"
                      d="M6.406 24.844A1.285 1.285 0 015.063 23.5v-2.781a1.477 1.477 0 01.313-.953 1.335 1.335 0 01.844-.485l2.438-.531a1.52 1.52 0 01.812.031 1.624 1.624 0 01.688.469l2.687 2.781a22.526 22.526 0 005.078-3.75 26.076 26.076 0 003.953-5.125l-2.75-2.594a1.218 1.218 0 01-.421-.656 1.9 1.9 0 01-.016-.875l.656-2.844a1.36 1.36 0 01.485-.875A1.431 1.431 0 0120.75 5h2.843a1.283 1.283 0 01.969.391 1.323 1.323 0 01.375.952 14.161 14.161 0 01-1.687 6.485 23.427 23.427 0 01-4.375 5.972 23.891 23.891 0 01-5.984 4.36 14.161 14.161 0 01-6.485 1.684zM19.875 9.812l2.531 2.375a19.976 19.976 0 001.109-3.062 10.629 10.629 0 00.391-2.719.323.323 0 00-.344-.344h-2.75a.507.507 0 00-.313.094.422.422 0 00-.156.281l-.624 2.813a.784.784 0 000 .313.4.4 0 00.156.249zm-13.438 14a15.43 15.43 0 002.609-.344 10.971 10.971 0 002.8-.969l-2.471-2.53a.782.782 0 00-.234-.172.267.267 0 00-.235.016l-2.406.468a.455.455 0 00-.3.172.517.517 0 00-.109.328v2.688a.312.312 0 00.109.25.354.354 0 00.237.093z"
                      fill={color}
                      transform="translate(1681.938 943)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1039"
                  transform="translate(1682 943)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default HelpPhone
