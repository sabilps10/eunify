import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ArrowDown = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={12}
            height={7}
            viewBox="0 0 12 7">
            <Path
                id="arrow_down"
                d="M216,213.286,220.375,209,216,204.714l.875-1.714L223,209l-6.125,6Z"
                transform="translate(215 -216) rotate(90)"
                fill={color}
            />
        </Svg>
    )
}

export default ArrowDown
