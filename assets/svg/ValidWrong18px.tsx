import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg"

const ValidWrong18px = (config: IconInterface) => {

    const { color, backgroundColor } = config;
    return (
        <Svg
        xmlns="http://www.w3.org/2000/svg"
        width={18}
        height={18}
        viewBox="0 0 18 18"
        >
            <G id="valid_wrong_18px" transform="translate(-1603 -1383)">
            <G id="Status" transform="translate(1602.875 1382.857)">
            <Path
                id="Path_1387"
                data-name="Path 1387"
                d="M12.048,6.236a.141.141,0,0,0-.141-.141l-1.16.005L9,8.184,7.255,6.1,6.093,6.1a.14.14,0,0,0-.141.141.146.146,0,0,0,.033.091L8.272,9.054,5.986,11.777a.146.146,0,0,0-.033.091.141.141,0,0,0,.141.141L7.255,12,9,9.921,10.746,12l1.16.005a.14.14,0,0,0,.141-.141.146.146,0,0,0-.033-.091L9.73,9.052l2.287-2.725A.141.141,0,0,0,12.048,6.236Z"
                fill={color}
            />
            <Path
                id="Path_1388"
                data-name="Path 1388"
                d="M9,1.143a7.875,7.875,0,1,0,7.875,7.875A7.876,7.876,0,0,0,9,1.143ZM9,15.557a6.539,6.539,0,1,1,6.539-6.539A6.54,6.54,0,0,1,9,15.557Z"
                fill={color}
            />
            </G>
            <Rect
                id="Rectangle_1024"
                data-name="Rectangle 1024"
                width={18}
                height={18}
                transform="translate(1603 1383)"
                fill={'none'}
            />
            </G>
        </Svg>
    )
}

export default ValidWrong18px
