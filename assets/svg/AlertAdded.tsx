import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const AlertAdded = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
        <G data-name="Group 342">
          <Path
              data-name="Path 770"
              d="M299.75 120.875v-1.5h2v-7.25a5.649 5.649 0 011.25-3.588 5.572 5.572 0 013.25-2.037v-.7a1.25 1.25 0 112.5 0v.7a5.572 5.572 0 013.25 2.037 5.649 5.649 0 011.25 3.588v7.25h2v1.5zm7.75 2.925a1.793 1.793 0 01-1.8-1.8h3.6a1.793 1.793 0 01-1.8 1.8z"
              fill= {color}
              transform="translate(-1406 -1150) translate(1110.25 1048.45)"
          />
        </G>
        <Path
            data-name="Path 1384"
            d="M0 0h24v24H0z"
            transform="translate(-1406 -1150) translate(1406 1150)"
            fill={backgroundColor}
        />
      </Svg>
  )
}

export default AlertAdded
