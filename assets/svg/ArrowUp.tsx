import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ArrowUp = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={12}
            height={7}
            viewBox="0 0 12 7">
            <Path
                id="arrow_up"
                d="M0,1.714,4.375,6,0,10.286.875,12,7,6,.875,0Z"
                transform="translate(0 7) rotate(-90)"
                fill={color}
            />
        </Svg>
    )
}

export default ArrowUp
