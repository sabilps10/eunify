import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MeNotification= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1589 -943)">
              <G data-name="Group 622">
                  <Path
                      data-name="Path 1359"
                      d="M210.375 496.125h13.75v-1.063h-13.75zm0-3.844h13.75v-1.062h-13.75zm0-3.843h13.75v-1.063h-13.75zm18.125 15.906l-3.844-3.844h-16.843a1.813 1.813 0 01-1.813-1.812v-13.875a1.813 1.813 0 011.813-1.813h18.874a1.813 1.813 0 011.813 1.813zm-21.437-19.531v14.625h18.062l2.312 2.343v-16.968a.744.744 0 00-.75-.75h-18.874a.744.744 0 00-.75.75zm0 0z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1006"
                  transform="translate(1589 943)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeNotification
