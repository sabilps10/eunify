import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartHighLow= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G data-name="Group 602" fill={color}>
              <Path
                  data-name="Path 1325"
                  d="M352.062 571.063h-5.187V557h5.187zm-4.187-1h3.187V558h-3.187z"
                  transform="translate(-338.875 -549)"
              />
              <Path
                  data-name="Path 1326"
                  d="M359.937 568.875h-5.187v-9.812h5.187zm-4.187-1h3.187v-7.812h-3.187z"
                  transform="translate(-338.875 -549)"
              />
          </G>
          <Path data-name="Rectangle 967" fill={backgroundColor} d="M0 0H30V30H0z" />
      </Svg>
  )
}

export default ChartHighLow
