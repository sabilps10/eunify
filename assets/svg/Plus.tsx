import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const Plus= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1616 -1373)">
              <Path
                  data-name="+-24"
                  d="M12.938 12.938v6.25a.188.188 0 01-.187.188h-1.5a.188.188 0 01-.187-.187v-6.25H4.813a.188.188 0 01-.187-.187v-1.5a.188.188 0 01.188-.187h6.25V4.813a.188.188 0 01.188-.187h1.5a.188.188 0 01.188.188v6.25h6.25a.188.188 0 01.188.188v1.5a.188.188 0 01-.187.188z"
                  transform="translate(1616.375 1373.375)"
                  fill={color}
                  fillRule="evenodd"
              />
              <Path
                  data-name="Rectangle 1022"
                  transform="translate(1616 1373)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default Plus
