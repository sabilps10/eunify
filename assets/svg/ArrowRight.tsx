import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ArrowRight = (config: IconInterface) => {

  const { color, backgroundColor, style } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={7}
          height={12}
          viewBox="0 0 7 12"
      >
        <Path
            d="M216 213.286l4.375-4.286-4.375-4.286.875-1.714 6.125 6-6.125 6z"
            transform="translate(-216 -203)"
            fill={color}
        />
      </Svg>
  )
}

export default ArrowRight
