import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MePromotions= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1441 -1014)">
              <G data-name="Group 624">
                  <Path
                      data-name="Path 1362"
                      d="M62.375 574.188l2-6.657-5.219-3.781h6.469l2.125-6.938 2.125 6.938h6.469l-5.219 3.781 2 6.657-5.375-4.094zm2-2.875l3.375-2.594 3.375 2.594L69.844 567 73 564.844h-3.875l-1.375-4.375-1.375 4.375H62.5L65.657 567zm12.5 2.875l-1.468-1.125-1.376-4.563 3.594-2.594h1.813l-4.157 3zm-5.156-12.938l-.531-1.75.781-2.688 1.344 4.438z"
                      fill={color}
                      transform="translate(1387 463)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1008"
                  transform="translate(1441 1014)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MePromotions
