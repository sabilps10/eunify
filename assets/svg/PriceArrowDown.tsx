import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg"

const ArrowDown = (config: IconInterface) => {

    const { color, backgroundColor } = config

    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={16}
            height={16}
            viewBox="0 0 16 16">
            <G id="arrow_down_green" transform="translate(-744 -488)">
                <Path
                    id="Up-24"
                    d="M8,12.833l5.2-9H2.8Z"
                    transform="translate(744.196 487.667)"
                    fill={color}
                />
                <Rect
                    id="Rectangle_882"
                    data-name="Rectangle 882"
                    width={16}
                    height={16}
                    transform="translate(744 488)"
                    fill={backgroundColor}
                />
            </G>
        </Svg>
    )
}

export default ArrowDown
