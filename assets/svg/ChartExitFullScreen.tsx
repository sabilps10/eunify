import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartExitFullScreen= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 935" fill={backgroundColor} d="M0 0H30V30H0z" />
          <G data-name="Group 590" fill={color}>
              <Path
                  data-name="Path 1300"
                  d="M465.4 219.646v1h5.292v-5.291h-1v3.528l-3.875-3.876-.763.763 3.876 3.876z"
                  transform="translate(-458.053 -208.007)"
              />
              <Path
                  data-name="Path 1301"
                  d="M480.874 220.646v-1h-3.53l3.876-3.876-.763-.763-3.876 3.877v-3.529h-1v5.291z"
                  transform="translate(-458.053 -208.007)"
              />
              <Path
                  data-name="Path 1302"
                  d="M476.581 227.3l3.876 3.876.763-.763-3.875-3.876h3.529v-1h-5.293v5.292h1z"
                  transform="translate(-458.053 -208.007)"
              />
              <Path
                  data-name="Path 1303"
                  d="M465.815 231.174l3.875-3.875v3.529h1v-5.292h-5.29v1h3.529l-3.875 3.876z"
                  transform="translate(-458.053 -208.007)"
              />
          </G>
      </Svg>
  )
}

export default ChartExitFullScreen
