import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartBaseline = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 932" fill={backgroundColor} d="M0 0H30V30H0z" />
          <Path
              data-name="Union 3"
              d="M7421.5 2095v-1.5h-2.5v-1h2.5V2080h1v3.5h2.5v1h-2.5v10.5zm9-2v-10.5h-2.5v-1h2.5v-3.5h1v10.5h2.5v1h-2.5v3.5z"
              transform="translate(-7411.5 -2071.5)"
              fill={color}
          />
      </Svg>
  )
}

export default ChartBaseline
