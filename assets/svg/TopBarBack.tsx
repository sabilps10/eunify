import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const TopBarBack= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1537 -1236)">
              <Path
                  data-name="Path 1383"
                  d="M33 43.214L26.625 49.5l6.25 6.161L32 57.375 24 49.5l8.125-8z"
                  transform="translate(1520 1198.5)"
                  fill={color}
              />
              <Path
                  data-name="Rectangle 1013"
                  transform="translate(1537 1236)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default TopBarBack
