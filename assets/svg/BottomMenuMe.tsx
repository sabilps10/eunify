import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const  BottomMenuMe = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1175 -718)">
              <G data-name="Group 367">
                  <Path
                      data-name="Path 819"
                      d="M307.942 705.063a16.914 16.914 0 013.483-1.875 11.347 11.347 0 017.782 0 16.921 16.921 0 013.484 1.875 10.75 10.75 0 002.063-3.188 10.188 10.188 0 10-18.875 0 10.75 10.75 0 002.063 3.188zm7.374-6.532a3.681 3.681 0 112.641-1.078 3.59 3.59 0 01-2.641 1.078zm0 10.719a11.054 11.054 0 01-4.39-.875 11.217 11.217 0 01-5.984-5.984 11.443 11.443 0 010-8.782 11.217 11.217 0 015.984-5.984 11.453 11.453 0 018.781 0 11.217 11.217 0 015.984 5.984 11.455 11.455 0 010 8.782 11.217 11.217 0 01-5.984 5.984 11.059 11.059 0 01-4.391.875zm0-1.063a10.322 10.322 0 003.469-.609 9.529 9.529 0 003.062-1.765 11.258 11.258 0 00-3-1.641 10.149 10.149 0 00-3.531-.609 10.508 10.508 0 00-3.546.593 10.03 10.03 0 00-2.985 1.657 9.545 9.545 0 003.063 1.765 10.32 10.32 0 003.468.609zm0-10.718a2.685 2.685 0 001.891-4.563 2.6 2.6 0 00-1.891-.75 2.657 2.657 0 100 5.313z"
                      fill={color}
                      transform="translate(875 35)"
                  />
              </G>
              <Path
                  data-name="Rectangle 984"
                  transform="translate(1175 718)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default BottomMenuMe
