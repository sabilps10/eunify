import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartLine= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 930" fill={backgroundColor} d="M0 0H30V30H0z" />
          <Path
              data-name="Path 1295"
              d="M-5784.965-1580.609l-.663-.749 4.821-4.272 3.294 3.127 4.06-4.763h3.675l3.42-4.29.782.624-3.721 4.666h-3.694l-4.444 5.214-3.395-3.222z"
              transform="translate(5791 1601)"
              fill={color}
          />
      </Svg>
  )
}

export default ChartLine
