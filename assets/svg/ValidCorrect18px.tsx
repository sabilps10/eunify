import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ValidCorrect18px= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={18}
          height={18}
          viewBox="0 0 18 18"
      >
          <G transform="translate(-1672 -1425)">
              <Path
                  d="M8 .143a7.875 7.875 0 107.875 7.875A7.876 7.876 0 008 .143z"
                  transform="translate(1673.875 1425.857)"
                  fill={backgroundColor}
              />
              <Path
                  d="M1.652 2.351a.633.633 0 10-.895.895L3.032 5.52l.037.037a.6.6 0 00.846 0l3.928-3.928a.6.6 0 000-.846L7.794.734a.6.6 0 00-.846 0L3.491 4.19z"
                  transform="translate(1677.58 1431.441)"
                  fill={color}
              />
              <Path
                  data-name="Rectangle 1025"
                  transform="translate(1672 1425)"
                  fill={'none'}
                  d="M0 0H18V18H0z"
              />
          </G>
      </Svg>
  )
}

export default ValidCorrect18px
