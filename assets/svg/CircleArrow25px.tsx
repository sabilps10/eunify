import * as React from 'react';
import {IconInterface} from './Types';
import Svg, {G, Path} from 'react-native-svg';

const CircleArrow25px = (config: IconInterface) => {
  const {color, backgroundColor} = config;
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={25}
      height={25}
      viewBox="0 0 20 20">
      <G transform="translate(-1324 -1226)">
        <Path
          data-name="Path 1385"
          d="M332 172.27a8.75 8.75 0 108.75 8.75 8.751 8.751 0 00-8.75-8.75z"
          transform="translate(-2 -2.02) translate(1004 1057)"
          fill={backgroundColor}
        />
        <Path
          data-name="Path 1386"
          d="M329.5 184.571l3.646-3.571-3.646-3.571.729-1.429 5.1 5-5.1 5z"
          transform="translate(-2 -2.02) translate(1004 1057)"
          fill={color}
        />
        <Path
          data-name="Rectangle 1019"
          transform="translate(1324 1226)"
          d="M0 0H20V20H0z"
        />
      </G>
    </Svg>
  );
};

export default CircleArrow25px;
