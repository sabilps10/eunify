import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ClosePosition= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1569.5 -1373.5)">
              <Path
                  data-name="Rectangle 1032"
                  transform="translate(1569.5 1373.5)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
              <G data-name="Group 637">
                  <Path
                      data-name="Path 1393"
                      d="M7.25 13.25h9.5v-1.5h-9.5zM12 22a9.263 9.263 0 01-3.712-.75 9.432 9.432 0 01-5.038-5.038 9.563 9.563 0 010-7.425A9.435 9.435 0 018.288 3.75 9.263 9.263 0 0112 3a9.27 9.27 0 013.713.75 9.437 9.437 0 015.037 5.037 9.563 9.563 0 010 7.425 9.435 9.435 0 01-5.037 5.038A9.27 9.27 0 0112 22zm0-1.5a7.721 7.721 0 005.675-2.325A7.721 7.721 0 0020 12.5a7.721 7.721 0 00-2.325-5.675A7.721 7.721 0 0012 4.5a7.721 7.721 0 00-5.675 2.325A7.721 7.721 0 004 12.5a7.721 7.721 0 002.325 5.675A7.721 7.721 0 0012 20.5z"
                      fill={color}
                      transform="translate(1569.5 1373)"
                  />
              </G>
          </G>
      </Svg>
  )
}

export default ClosePosition
