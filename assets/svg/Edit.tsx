import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const Edit= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1604 -1373)">
              <Path
                  data-name="Path 1394"
                  d="M40.781 18.969V5.281H48.5V3.5h-8.75a.749.749 0 00-.75.75V20a.749.749 0 00.75.75H56a.749.749 0 00.75-.75v-9.5h-1.781v8.469z"
                  transform="translate(1568.125 1373.5)"
                  fill={color}
              />
              <Path
                  data-name="Path 1395"
                  d="M52.958 3.5a.813.813 0 01.575.238L56.5 6.707a.813.813 0 010 1.149l-8.4 8.406a.813.813 0 01-.575.238h-2.974a.813.813 0 01-.812-.812V12.73a.812.812 0 01.238-.574l8.407-8.417a.812.812 0 01.574-.239zm0 1.962l-7.6 7.6v1.809h1.823l7.59-7.594z"
                  transform="translate(1568.125 1373.5)"
                  fill={color}
                  fillRule="evenodd"
              />
              <Path
                  data-name="Rectangle 1031"
                  transform="translate(1604 1373)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default Edit
