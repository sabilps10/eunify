import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const  BottomMenuHome = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-905 -718)">
              <G data-name="Group 363">
                  <Path
                      data-name="Path 815"
                      d="M37.473 708a.909.909 0 01-.907-.906v-10.782l-2.125 1.656a.384.384 0 01-.375.11.53.53 0 01-.343-.234.4.4 0 01-.125-.36.58.58 0 01.219-.359l10.969-8.406a1 1 0 01.234-.11.962.962 0 01.594 0 1 1 0 01.234.11l10.969 8.406a.58.58 0 01.219.359.48.48 0 01-.125.391.623.623 0 01-.344.187.507.507 0 01-.375-.094l-2.125-1.656v10.782a.909.909 0 01-.907.906zm.156-1.062H53v-11.407l-7.687-5.875-7.687 5.875zm2.687-5.469a1.018 1.018 0 10-.671-.3.927.927 0 00.672.3zm5 0a1.018 1.018 0 10-.672-.3.927.927 0 00.673.3zm5 0a1.018 1.018 0 10-.672-.3.925.925 0 00.673.3z"
                      fill={color}
                      transform="translate(875 35)"
                  />
              </G>
              <Path
                  data-name="Rectangle 980"
                  transform="translate(905 718)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default BottomMenuHome
