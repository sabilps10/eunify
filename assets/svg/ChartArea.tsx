import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const  ChartArea = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <Path data-name="Rectangle 929"  fill={backgroundColor} d="M0 0H30V30H0z" />
          <G data-name="Group 580"  fill={color}>
              <Path
                  data-name="Path 1295"
                  d="M-5784.579-1539.812l-.7-.719 5.025-4.866 2.63 2.808 4.928-5.083 2.782-.826 2.763-2.034.69.723-3.052 2.311-2.721.826-5.39 5.628-2.7-2.892z"
                  transform="translate(-1627.726 -504.468) translate(7419 2063)"
              />
              <Path
                  data-name="Path 1295"
                  d="M112.858 299.5h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.949 0h-.976v-1h.976zm-1.95 0h-.975v-1h.975zm-1.949 0h-.975v-1h.975zm-1.95 0h-.975v-1h.975zm16.6-1h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.949 0h-.976v-1h.976zm-1.95 0h-.975v-1h.975zm-1.949 0h-.975v-1h.975zm-1.95 0h-.975v-1h.975zm16.6-1h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.949 0h-.976v-1h.976zm-1.95 0h-.975v-1h.975zm-1.949 0h-.975v-1h.975zm-1.95 0h-.975v-1h.975zm14.6-1h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-3.9 0h-.975v-1h.975zm-1.949 0h-.975v-1h.975zm14.646-1h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-5.848 0h-.975v-1h.975zm12.7-1H111v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm6.8-1h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm4.9-1h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm-1.95 0h-.974v-1h.974zm4.9-1h-.974v-1h.974zm-1.949 0h-.975v-1h.975zm.949-1h-.974v-1h.974zm1-1h-.974v-1h.974z"
                  transform="translate(-1627.726 -504.468) translate(1539.662 226.5)"
              />
          </G>
      </Svg>
  )
}

export default ChartArea
