import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ChartColumns= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G data-name="chart_columns" fill={color}>
              <Path
                  data-name="Path 1322"
                  d="M357.27 410.979h-5.187v-14.062h5.187zm-4.187-1h3.187v-12.062h-3.187z"
                  transform="translate(-340.166 -388.917)"
              />
              <Path
                  data-name="Path 1323"
                  d="M350.354 410.958h-5.188v-9.813h5.188zm-4.188-1h3.188v-7.813h-3.188z"
                  transform="translate(-340.166 -388.917)"
              />
              <Path
                  data-name="Path 1324"
                  d="M364.187 410.958H359V405h5.188zm-4.188-1h3.188V406H360z"
                  transform="translate(-340.166 -388.917)"
              />
          </G>
          <Path data-name="Rectangle 966" fill={backgroundColor} d="M0 0H30V30H0z" />
      </Svg>
  )
}

export default ChartColumns
