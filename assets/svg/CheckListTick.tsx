import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const CheckListTick= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1710.5 -1284.5)">
              <Path
                  data-name="Path 1389"
                  d="M12 1.523a10.5 10.5 0 1010.5 10.5A10.5 10.5 0 0012 1.523z"
                  fill={backgroundColor}
                  transform="translate(1710.5 1284.477)"
              />
              <Path
                  data-name="Path 1390"
                  d="M8.869 11.135a.844.844 0 10-1.193 1.193l3.033 3.033.049.049a.8.8 0 001.127 0l5.238-5.238a.8.8 0 000-1.127l-.066-.066a.8.8 0 00-1.127 0l-4.61 4.608z"
                  fill={color}
                  transform="translate(1710.5 1284.477)"
              />
              <Path
                  data-name="Rectangle 1026"
                  transform="translate(1710.5 1284.5)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default CheckListTick
