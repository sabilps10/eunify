import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const IconExit= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={18}
          height={18}
          viewBox="0 0 18 18"
      >
          <G transform="translate(-327 -39)">
              <Path
                  data-name="Rectangle 843"
                  transform="translate(327 39)"
                  fill={backgroundColor}
                  d="M0 0H18V18H0z"
              />
              <G data-name="Group 508">
                  <Path
                      data-name="Path 1177"
                      d="M329.4 56a1.793 1.793 0 01-1.8-1.8V40.3a1.793 1.793 0 011.8-1.8h6.725V40H329.4a.354.354 0 00-.3.3v13.9a.354.354 0 00.3.3h6.725V56zm10.725-4.475l-1.025-1.1L341.525 48h-8.4v-1.5h8.4l-2.425-2.425 1.025-1.1 4.275 4.275z"
                      fill={color}
                      transform="translate(0 .802)"
                  />
              </G>
          </G>
      </Svg>
  )
}

export default IconExit
