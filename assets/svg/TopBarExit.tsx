import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const TopBarExit= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1858 -1361)">
              <Path
                  data-name="Path 1396"
                  d="M5.4 21a1.793 1.793 0 01-1.8-1.8V5.3a1.793 1.793 0 011.8-1.8h6.725V5H5.4a.354.354 0 00-.3.3v13.9a.354.354 0 00.3.3h6.725V21zm10.725-4.475l-1.025-1.1L17.525 13h-8.4v-1.5h8.4L15.1 9.075l1.025-1.1L20.4 12.25z"
                  transform="translate(1858.4 1360.5)"
                  fill={color}
              />
              <Path
                  data-name="Rectangle 1033"
                  transform="translate(1858 1361)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default TopBarExit
