import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const FavAdd= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1529 -1150)">
              <G data-name="Group 424">
                  <Path
                      data-name="Path 951"
                      d="M330 122.25V107.3a1.793 1.793 0 011.8-1.8h5.7v1.5h-5.7a.354.354 0 00-.3.3v12.65l5-2.15 5 2.15V113h1.5v9.25l-6.5-2.8zm1.5-15.25zm10 4v-2h-2v-1.5h2v-2h1.5v2h2v1.5h-2v2z"
                      fill={color}
                      transform="translate(1204 1048.125)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1018"
                  transform="translate(1529 1150)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default FavAdd
