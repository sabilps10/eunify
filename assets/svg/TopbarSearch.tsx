import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const TopBarSearch= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1600 -1238)">
              <G data-name="Group 38">
                  <Path
                      data-name="Path 142"
                      d="M283.225 57.1l-5.95-5.95a5.35 5.35 0 01-1.737 1 6.064 6.064 0 01-2.038.35 5.907 5.907 0 01-4.325-1.762 6.1 6.1 0 010-8.6 6.064 6.064 0 019 8.113l6 5.975a.576.576 0 01.175.425.619.619 0 01-.2.45.635.635 0 01-.925 0zm-9.75-5.85a4.758 4.758 0 004.8-4.8 4.679 4.679 0 00-1.387-3.425 4.618 4.618 0 00-3.413-1.4 4.8 4.8 0 00-4.825 4.825 4.609 4.609 0 001.413 3.4 4.669 4.669 0 003.412 1.4z"
                      fill={color}
                      transform="translate(1335.6 1201.625)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1014"
                  transform="translate(1600 1238)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default TopBarSearch
