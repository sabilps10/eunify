import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const AlertAdd = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
        <G transform="translate(-1482 -1150)">
          <G data-name="Group 423">
            <Path
                data-name="Path 950"
                d="M300.375 120.875a.589.589 0 01-.45-.188.65.65 0 01-.175-.462.62.62 0 01.175-.438.589.589 0 01.45-.187h1.425v-7.55a5.742 5.742 0 011.237-3.6 5.393 5.393 0 013.263-2.025v-.7a1.141 1.141 0 01.337-.838 1.257 1.257 0 011.713 0 1.114 1.114 0 01.35.838v.7a5.441 5.441 0 013.275 2.025 5.7 5.7 0 011.25 3.6v7.55h1.4a.589.589 0 01.45.187.651.651 0 01.175.463.62.62 0 01-.175.437.589.589 0 01-.45.188zm7.125 2.925a1.747 1.747 0 01-1.262-.525A1.714 1.714 0 01305.7 122h3.6a1.793 1.793 0 01-1.8 1.8zm-4.45-4.2h8.9v-7.55a4.341 4.341 0 00-1.287-3.175 4.249 4.249 0 00-3.138-1.3 4.439 4.439 0 00-4.475 4.475z"
                fill={color}
                transform="translate(1186.25 1048.45)"
            />
          </G>
          <Path
              data-name="Rectangle 1016"
              transform="translate(1482 1150)"
              fill={backgroundColor}
              d="M0 0H24V24H0z"
          />
        </G>
      </Svg>
  )
}

export default AlertAdd
