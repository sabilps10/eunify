import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path,Circle } from "react-native-svg"

const ProfilePic= (config: IconInterface) => {

    const { color, backgroundColor, style } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={style?.width ?? 120}
            height={style?.height ?? 120}
            viewBox="0 0 120 120"
        >
            <G id="profile_pic" transform="translate(-20 -20)">
                <Circle
                    id="Ellipse_83"
                    data-name="Ellipse 83"
                    cx={60}
                    cy={60}
                    r={60}
                    transform="translate(20 20)"
                    fill={backgroundColor}
                />
                <G id="Group_638" data-name="Group 638">
                    <Path
                        id="Path_1406"
                        data-name="Path 1406"
                        d="M40.668,117.668A88.579,88.579,0,0,1,59.248,108,59.174,59.174,0,0,1,80,104.5a59.174,59.174,0,0,1,20.752,3.5,88.579,88.579,0,0,1,18.58,9.668,57.317,57.317,0,0,0,11-17,52.713,52.713,0,0,0,4-20.668q0-22.668-15.832-38.5T80,25.668q-22.668,0-38.5,15.832T25.668,80a52.718,52.718,0,0,0,4,20.668A57.317,57.317,0,0,0,40.668,117.668ZM80,82.832A19.629,19.629,0,0,1,60.168,63,19.629,19.629,0,0,1,80,43.168,19.629,19.629,0,0,1,99.832,63,19.629,19.629,0,0,1,80,82.832ZM80,140a59.528,59.528,0,0,1-23.5-4.668A58.871,58.871,0,0,1,37.416,122.5a60.8,60.8,0,0,1-12.748-19.084A58.922,58.922,0,0,1,20,80,59.31,59.31,0,0,1,37.5,37.416,60.8,60.8,0,0,1,56.584,24.668,58.922,58.922,0,0,1,80,20a59.633,59.633,0,0,1,60,60,58.922,58.922,0,0,1-4.668,23.416A60.8,60.8,0,0,1,122.584,122.5,58.871,58.871,0,0,1,103.5,135.332,59.528,59.528,0,0,1,80,140Z"
                        fill={color}
                    />
                </G>
            </G>
        </Svg>
    )
}

export default ProfilePic
