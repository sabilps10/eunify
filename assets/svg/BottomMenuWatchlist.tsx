import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const BottomMenuWatchlist = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-973 -718)">
              <G data-name="Group 364">
                  <Path
                      data-name="Path 816"
                      d="M110.285 696.219h8.376a.531.531 0 000-1.063h-8.376a.459.459 0 00-.359.157.54.54 0 00-.141.375.527.527 0 00.141.39.485.485 0 00.359.141zm0 3.469h3.938a.461.461 0 00.359-.157.569.569 0 000-.75.46.46 0 00-.359-.156h-3.938a.462.462 0 00-.359.156.569.569 0 000 .75.462.462 0 00.359.157zm0-6.938h8.376a.543.543 0 00.375-.141.5.5 0 00.156-.39.534.534 0 00-.531-.532h-8.376a.462.462 0 00-.359.157.582.582 0 00-.141.406.483.483 0 00.5.5zm-2.906 11.875a1.815 1.815 0 01-1.813-1.812v-14.25a1.815 1.815 0 011.813-1.813h14.25a1.815 1.815 0 011.813 1.813v14.25a1.815 1.815 0 01-1.813 1.812zm0-1.062h14.25a.751.751 0 00.531-.2.711.711 0 00.219-.547v-14.25a.746.746 0 00-.75-.75h-14.25a.714.714 0 00-.547.218.755.755 0 00-.2.532v14.25a.7.7 0 00.75.75zM104 708a1.815 1.815 0 01-1.813-1.812v-14.782a.531.531 0 011.063 0v14.782a.748.748 0 00.75.75h14.781a.531.531 0 010 1.062z"
                      fill={color}
                      transform="translate(875 35)"
                  />
              </G>
              <Path
                  data-name="Rectangle 981"
                  transform="translate(973 718)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default BottomMenuWatchlist
