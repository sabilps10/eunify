import * as React from "react";
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg";

const ShowPassword = (config: IconInterface) => {
    
  const { color, backgroundColor } = config;
  return (
    <Svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
    >
      <G transform="translate(-1358 -698)">
        <Path
          data-name="Path 1321"
          d="M11 11.575A4.056 4.056 0 0015.075 7.5a3.928 3.928 0 00-1.188-2.887A3.928 3.928 0 0011 3.425a3.928 3.928 0 00-2.887 1.188A3.928 3.928 0 006.925 7.5 4.056 4.056 0 0011 11.575zm0-1.375a2.69 2.69 0 01-2.7-2.7A2.689 2.689 0 0111 4.8a2.687 2.687 0 012.7 2.7 2.689 2.689 0 01-2.7 2.7zm0 4.3a11.054 11.054 0 01-6.287-1.9A11.116 11.116 0 01.55 7.5 11.258 11.258 0 0111 .5a11.255 11.255 0 0110.45 7 11.118 11.118 0 01-4.162 5.1A11.059 11.059 0 0111 14.5zm0-1.5a9.545 9.545 0 005.188-1.488A9.77 9.77 0 0019.8 7.5a9.777 9.777 0 00-3.612-4.013A9.55 9.55 0 0011 2a9.55 9.55 0 00-5.188 1.487A9.777 9.777 0 002.2 7.5a9.77 9.77 0 003.612 4.012A9.545 9.545 0 0011 13z"
          transform="translate(1358.45 702.5)"
          fill={color}
        />
        <Path
          data-name="Rectangle 1021"
          transform="translate(1358 698)"
          fill={backgroundColor}
          d="M0 0H24V24H0z"
        />
      </G>
    </Svg>
  )
}

export default ShowPassword