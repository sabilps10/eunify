import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MeBackCard= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1663 -943)">
              <G data-name="Group 623">
                  <Path
                      data-name="Path 1360"
                      d="M281.563 502.75a1.813 1.813 0 01-1.813-1.812v-13.875a1.815 1.815 0 011.813-1.813h18.874a1.815 1.815 0 011.813 1.813v13.875a1.813 1.813 0 01-1.813 1.812zm-.75-12.656h20.374v-3.031a.8.8 0 00-.75-.75h-18.874a.8.8 0 00-.75.75zm0 2.687v8.157a.8.8 0 00.75.75h18.874a.8.8 0 00.75-.75v-8.157z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1007"
                  transform="translate(1663 943)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeBackCard
