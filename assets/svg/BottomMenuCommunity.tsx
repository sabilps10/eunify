import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const  BottomMenuCommunity = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1041 -718)">
              <G data-name="Group 365">
                  <Path
                      data-name="Path 817"
                      d="M192.035 710.469l-4-3.938a4.077 4.077 0 01-1.219.657 4.32 4.32 0 01-1.376.219 4.287 4.287 0 114.282-4.282 4.268 4.268 0 01-.235 1.422 3.945 3.945 0 01-.7 1.234l4 3.938a.468.468 0 010 .75.416.416 0 01-.343.156.624.624 0 01-.409-.156zm-6.594-4.125a3.2 3.2 0 10-2.28-.938 3.107 3.107 0 002.28.938zm-17.625-2a.478.478 0 01-.216-.344.465.465 0 01.094-.375l4.406-7.093a.87.87 0 01.719-.438.963.963 0 01.781.281l2.906 3.438 4.219-6.782a.812.812 0 01.75-.437.9.9 0 01.781.406l1.781 2.688c-.187.041-.37.083-.547.124a4.382 4.382 0 00-.515.157l-1.469-2.188-4.125 6.75a.87.87 0 01-.719.438.922.922 0 01-.781-.312l-2.969-3.469-4.344 7a.435.435 0 01-.327.234.545.545 0 01-.425-.078zm19.782-8.5a2.852 2.852 0 00-.516-.14q-.266-.048-.546-.079l5.531-8.75a.437.437 0 01.328-.234.543.543 0 01.422.078.475.475 0 01.218.344.56.56 0 01-.062.375z"
                      fill={color}
                      transform="translate(875 35)"
                  />
              </G>
              <Path
                  data-name="Rectangle 982"
                  transform="translate(1041 718)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default BottomMenuCommunity
