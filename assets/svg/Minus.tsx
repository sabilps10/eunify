import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const Minus = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1657 -1373)">
              <Path
                  data-name="--24"
                  d="M19.188 11.063H4.813a.188.188 0 00-.187.188v1.5a.188.188 0 00.188.188h14.374a.188.188 0 00.188-.187v-1.5a.188.188 0 00-.188-.189z"
                  transform="translate(1657 1372.938)"
                  fill={color}
              />
              <Path
                  data-name="Rectangle 1023"
                  transform="translate(1657 1373)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default Minus
