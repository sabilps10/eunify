import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg"

const Info = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={18}
            height={18}
            viewBox="0 0 18 18"
        >
            <G id="info" transform="translate(-1181 -877)">
            <G id="info-24" transform="translate(1181 877)">
                <Path
                id="Path_1687"
                data-name="Path 1687"
                d="M9.75,5.625A.75.75,0,1,1,9,4.875.75.75,0,0,1,9.75,5.625Z"
                fill={color}
                />
                <Path
                id="Path_1688"
                data-name="Path 1688"
                d="M7.5,8.438a.563.563,0,0,1,.563-.562H9.188a.562.562,0,0,1,.563.563v3.188h.563a.563.563,0,0,1,0,1.125H8.063a.563.563,0,0,1,0-1.125h.563V9H8.063A.563.563,0,0,1,7.5,8.438Z"
                fill={color}
                />
                <Path
                id="Path_1689"
                data-name="Path 1689"
                d="M9,.75A8.25,8.25,0,1,0,17.25,9,8.25,8.25,0,0,0,9,.75ZM1.875,9A7.125,7.125,0,1,1,9,16.125,7.125,7.125,0,0,1,1.875,9Z"
                fill={color}
                fillRule="evenodd"
                />
            </G>
            <Rect
                id="Rectangle_1139"
                data-name="Rectangle 1139"
                width={18}
                height={18}
                transform="translate(1181 877)"
                fill={backgroundColor}
            />
            </G>
        </Svg>        
    )
}

export default Info
