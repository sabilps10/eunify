import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const BottomMenuPortfolio = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1108 -718)">
              <G data-name="Group 366">
                  <Path
                      data-name="Path 818"
                      d="M248.941 686.812a11.12 11.12 0 0110.094 10.032H251a3.051 3.051 0 00-.8-1.235 3.519 3.519 0 00-1.266-.765zm1.059 1.251v6.062a4.24 4.24 0 01.968.7 3.215 3.215 0 01.688.954h6.125a10.373 10.373 0 00-2.781-5.013 9.607 9.607 0 00-5-2.703zm-3.281-1.251v8.032a3.446 3.446 0 00-1.626 1.25 3.261 3.261 0 00-.624 1.938 3.081 3.081 0 00.624 1.875 3.323 3.323 0 001.626 1.187v8.094a10.946 10.946 0 01-7.235-3.61 10.823 10.823 0 01-2.89-7.546 10.992 10.992 0 012.89-7.594 10.74 10.74 0 017.239-3.626zm-1.063 1.251a9.584 9.584 0 00-5.765 3.531 10.062 10.062 0 00-2.235 6.438 9.914 9.914 0 002.25 6.39 9.664 9.664 0 005.75 3.516v-6.156a5.5 5.5 0 01-1.625-1.626 3.89 3.89 0 01-.621-2.156 4.144 4.144 0 01.61-2.172 4.742 4.742 0 011.64-1.64zM251 699.125h8.031a11.264 11.264 0 01-10.094 10.063v-8.094a2.947 2.947 0 001.281-.75 3.293 3.293 0 00.782-1.219zm.656 1.031a6.242 6.242 0 01-.719.938 3.049 3.049 0 01-.937.688v6.156a9.726 9.726 0 005.015-2.735 10.43 10.43 0 002.766-5.047z"
                      fill={color}
                      transform="translate(875 35)"
                  />
              </G>
              <Path
                  data-name="Rectangle 983"
                  transform="translate(1108 718)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default BottomMenuPortfolio
