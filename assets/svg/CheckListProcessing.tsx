import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path,Circle } from "react-native-svg"

const CheckListProcessing= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1756.5 -1236.5)">
              <G transform="translate(1756.5 1236.477)">
                  <Path
                      data-name="Path 1391"
                      d="M12 1.523a10.5 10.5 0 1010.5 10.5A10.5 10.5 0 0012 1.523z"
                      fill={backgroundColor}
                  />
                  <Circle
                      data-name="Ellipse 80"
                      cx={1}
                      cy={1}
                      r={1}
                      transform="translate(11 11)"
                      fill={color}
                  />
                  <Circle
                      data-name="Ellipse 81"
                      cx={1}
                      cy={1}
                      r={1}
                      transform="translate(7 11)"
                      fill={color}
                  />
                  <Circle
                      data-name="Ellipse 82"
                      cx={1}
                      cy={1}
                      r={1}
                      transform="translate(15 11)"
                      fill={color}
                  />
              </G>
              <Path
                  data-name="Rectangle 1027"
                  transform="translate(1756.5 1236.5)"
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default CheckListProcessing
