import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Rect } from "react-native-svg"

const TopBarMoney= (config: IconInterface) => {

    const { color, backgroundColor } = config;
    
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24">
            <G id="topbar_money" transform="translate(-996 -583)">
            <G
                id="Group_552"
                data-name="Group 552"
                transform="translate(671.5 548.375)">
            <Path
                id="Path_1267"
                data-name="Path 1267"
                d="M337.85,47.575A1.964,1.964,0,1,1,339.275,47,1.935,1.935,0,0,1,337.85,47.575Zm-6.5,3.35a1.5,1.5,0,0,1-1.475-1.475V41.7a1.5,1.5,0,0,1,1.475-1.475h13a1.376,1.376,0,0,1,1.025.438A1.436,1.436,0,0,1,345.8,41.7v7.75a1.436,1.436,0,0,1-.425,1.037,1.376,1.376,0,0,1-1.025.438Zm1.15-.85h10.7a1.727,1.727,0,0,1,.513-1.25,1.665,1.665,0,0,1,1.237-.525V42.85a1.665,1.665,0,0,1-1.237-.525,1.727,1.727,0,0,1-.513-1.25H332.5a1.793,1.793,0,0,1-1.775,1.775V48.3a1.793,1.793,0,0,1,1.775,1.775Zm-3.85,3.55a1.377,1.377,0,0,1-1.025-.438,1.44,1.44,0,0,1-.425-1.037V43.625a.4.4,0,0,1,.125-.313.457.457,0,0,1,.6,0,.4.4,0,0,1,.125.313V52.15a.617.617,0,0,0,.188.425.557.557,0,0,0,.412.2h13.775a.425.425,0,0,1,0,.85Zm2.7-3.55h-.625v-9h.625a.642.642,0,0,0-.625.625v7.75a.642.642,0,0,0,.625.625Z"
                fill={color} />
            </G>
            <Rect
                id="Rectangle_1059"
                data-name="Rectangle 1059"
                width={24}
                height={24}
                transform="translate(996 583)"
                fill={backgroundColor} />
            </G>
        </Svg>
    )
}

export default TopBarMoney
