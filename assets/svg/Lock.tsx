import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { ClipPath, Defs, G, Path, Rect } from "react-native-svg"

const Lock = (config: IconInterface) => {
    
    const { color, backgroundColor } = config;
    return (
        <Svg
            xmlns="http://www.w3.org/2000/svg"
            width={14}
            height={14}
            viewBox="0 0 14 14"
        >
            <Defs>
                <ClipPath id="a">
                    <Path
                    data-name="Rectangle 1234"
                    transform="translate(1042 923)"
                    fill={backgroundColor}
                    stroke={color}
                    strokeWidth={1}
                    d="M0 0H14V14H0z"
                    />
                </ClipPath>
            </Defs>
            <G
                data-name="Mask Group 6"
                transform="translate(-1042 -923)"
                clipPath="url(#a)"
            >
            <Path
                data-name="Path 1816"
                d="M7 .583a2.846 2.846 0 012.093.853 2.847 2.847 0 01.853 2.093v1.138h.5a1.22 1.22 0 011.225 1.225v5.7a1.2 1.2 0 01-.358.875 1.168 1.168 0 01-.867.365H3.559a1.168 1.168 0 01-.867-.365 1.2 1.2 0 01-.358-.875v-5.7a1.22 1.22 0 011.225-1.225h.5V3.529a2.847 2.847 0 01.853-2.093A2.846 2.846 0 017 .583zm0 1.225a1.717 1.717 0 00-1.72 1.721v1.138h3.441V3.529A1.717 1.717 0 007 1.808zm0 5.76a1.171 1.171 0 10.839.343A1.141 1.141 0 007 7.568z"
                fill="#9d9d9d"
                transform="translate(1041.999 923.292)"
            />
            </G>
        </Svg>
    )
}

export default Lock