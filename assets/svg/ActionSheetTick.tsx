import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const ActionSheetTick = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
        <G transform="translate(-1179.5 -1211.5)">
          <G data-name="Group 636" fill={color}>
            <Path
                data-name="Path 994"
                d="M332 562.523a10.5 10.5 0 1010.5 10.5 10.5 10.5 0 00-10.5-10.5zm0 19.219a8.718 8.718 0 118.719-8.719 8.721 8.721 0 01-8.719 8.719z"
                transform="translate(859.5 570.477) translate(0 80)"
            />
            <Path
                data-name="Path 995"
                d="M328.869 572.135a.854.854 0 00-.274-.183.834.834 0 00-.646 0 .852.852 0 00-.457.457.839.839 0 00.184.919l3.032 3.033.05.049a.79.79 0 00.258.173.8.8 0 00.869-.173l5.238-5.238a.8.8 0 000-1.127l-.065-.066a.8.8 0 00-1.128 0l-4.61 4.608z"
                transform="translate(859.5 570.477) translate(0 80)"
            />
          </G>
          <Path
              data-name="Rectangle 1012"
              transform="translate(1179.5 1211.5)"
              fill={backgroundColor}
              d="M0 0H24V24H0z"
          />
        </G>
      </Svg>
  )
}

export default ActionSheetTick
