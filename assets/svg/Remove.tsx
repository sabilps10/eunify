import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const Remove= (config: IconInterface) => {

  const { color, backgroundColor, style } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={style?.width ?? 24}
          height={style?.height ?? 24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1585.5 -1321.5)">
              <G fill={color}>
                  <Path
                      data-name="Path 1398"
                      d="M16.064 8.814a.188.188 0 00-.188-.187l-1.547.007L12 11.411 9.672 8.636l-1.549-.007a.187.187 0 00-.187.188.2.2 0 00.045.122l3.049 3.633L7.98 16.2a.2.2 0 00-.045.122.188.188 0 00.187.188l1.55-.01L12 13.727l2.327 2.773 1.547.007a.187.187 0 00.188-.187.2.2 0 00-.045-.122l-3.045-3.63 3.049-3.633a.188.188 0 00.043-.121z"
                      transform="translate(1585.5 1320.977)"
                  />
                  <Path
                      data-name="Path 1399"
                      d="M12 2.023a10.5 10.5 0 1010.5 10.5A10.5 10.5 0 0012 2.023zm0 19.219a8.719 8.719 0 118.719-8.719A8.72 8.72 0 0112 21.242z"
                      transform="translate(1585.5 1320.977)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1035"
                  transform="translate(1585.5 1321.5)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default Remove
