import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MoveArrow= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={24}
          height={24}
          viewBox="0 0 24 24"
      >
          <G transform="translate(-1756 -1320)">
              <G data-name="Google Icon">
                  <Path
                      data-name="Path 1397"
                      d="M15 22.15l-4.65-4.65 1.075-1.05 2.825 2.825V11.75h1.5v7.525l2.825-2.825 1.075 1.05zm-6.75-8.9V5.725L5.425 8.55 4.35 7.5 9 2.85l4.65 4.65-1.075 1.05L9.75 5.725v7.525z"
                      fill={color}
                      transform="translate(1755.65 1319.15)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1034"
                  transform="translate(1756 1320)"
                  fill={backgroundColor}
                  d="M0 0H24V24H0z"
              />
          </G>
      </Svg>
  )
}

export default MoveArrow
