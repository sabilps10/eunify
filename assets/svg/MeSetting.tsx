import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path } from "react-native-svg"

const MeSetting= (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
      <Svg
          xmlns="http://www.w3.org/2000/svg"
          width={30}
          height={30}
          viewBox="0 0 30 30"
      >
          <G transform="translate(-1515 -861)">
              <G data-name="Group 617">
                  <Path
                      data-name="Path 1353"
                      d="M132.375 420.094v-1.063h6.313v1.063zm0-15.125v-1.063h11.375v1.063zm9.875 17.656V416.5h1.063v2.531h10.312v1.063h-10.312v2.531zm-4.625-7.563v-2.531h-5.25v-1.062h5.25v-2.531h1.063v6.124zm4.625-2.531v-1.062h11.375v1.062zm5.063-5.062v-6.094h1.062v2.531h5.25v1.063h-5.25v2.5z"
                      fill={color}
                      transform="translate(1387 464)"
                  />
              </G>
              <Path
                  data-name="Rectangle 1001"
                  transform="translate(1515 861)"
                  fill={backgroundColor}
                  d="M0 0H30V30H0z"
              />
          </G>
      </Svg>
  )
}

export default MeSetting
