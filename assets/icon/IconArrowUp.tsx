import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

const IconArrowUp = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
    <Svg
      width={16}
      height={16}
      viewBox="0 0 16 16"
    >
      <G transform="translate(-744 -488)">
        <Path
          d="M8 3.833l5.2 9H2.8z"
          transform="translate(744.196 487.167)"
          fill={color}
        />
        <Path
          data-name="Rectangle 882"
          transform="translate(744 488)"
          fill={backgroundColor}
          d="M0 0H16V16H0z"
        />
      </G>
    </Svg>
  )
}

export default IconArrowUp
