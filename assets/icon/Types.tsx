import { ColorValue } from "react-native";

export interface IconInterface {
    backgroundColor?: ColorValue,
    color?: ColorValue
  }
  