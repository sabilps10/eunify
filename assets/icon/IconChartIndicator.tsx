import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

const IconChartIndicator = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
    <Svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
    >
      <Path data-name="Rectangle 933" fill={backgroundColor} d="M0 0H30V30H0z" />
      <G data-name="Group 585" fill={color}>
        <Path
          data-name="Path 1296"
          d="M339.042 218.917a.5.5 0 000-1h-2.571l-.011-3.084a2.042 2.042 0 014.083 0 .5.5 0 101 0 3.042 3.042 0 10-6.083 0l.011 3.082h-2.845a.5.5 0 000 1h2.849l.026 7.208a2.042 2.042 0 11-4.083 0 .5.5 0 00-1 0 3.042 3.042 0 106.083 0l-.026-7.206z"
          transform="translate(-322.418 -205.792)"
        />
        <Path
          data-name="Path 1297"
          d="M343.812 228.647l-2.043-2.043 2.042-2.042a.5.5 0 00-.707-.707l-2.042 2.042-2.042-2.042a.5.5 0 10-.707.707l2.042 2.042-2.043 2.043a.5.5 0 00.354.854.5.5 0 00.353-.147l2.043-2.043 2.043 2.043a.5.5 0 10.707-.707z"
          transform="translate(-322.418 -205.792)"
        />
      </G>
    </Svg>
  )
}

export default IconChartIndicator
