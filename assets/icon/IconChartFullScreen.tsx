import * as React from "react"
import { IconInterface } from "./Types";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

const IconChartFullScreen = (config: IconInterface) => {

  const { color, backgroundColor } = config;
  return (
    <Svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
    >
      <Path data-name="Rectangle 936" fill={backgroundColor} d="M0 0H30V30H0z" />
      <G data-name="Group 591" fill={color}>
        <Path
          data-name="Path 1304"
          d="M441.845 216.007v-1h-5.292v5.293h1v-3.53l3.875 3.876.763-.763-3.876-3.876z"
          transform="translate(-429.553 -208.007)"
        />
        <Path
          data-name="Path 1305"
          d="M447.427 215.007v1h3.53l-3.876 3.876.763.763 3.876-3.876v3.53h1v-5.292z"
          transform="translate(-429.553 -208.007)"
        />
        <Path
          data-name="Path 1306"
          d="M451.72 229.412l-3.876-3.876-.763.763 3.875 3.876h-3.529v1h5.293v-5.292h-1z"
          transform="translate(-429.553 -208.007)"
        />
        <Path
          data-name="Path 1307"
          d="M441.428 225.536l-3.875 3.875v-3.529h-1v5.292h5.292v-1h-3.529l3.875-3.876z"
          transform="translate(-429.553 -208.007)"
        />
      </G>
    </Svg>
  )
}

export default IconChartFullScreen
