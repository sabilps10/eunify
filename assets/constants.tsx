export const getIcon_back_24 = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/back_24.png')
        default:
            return require('./icon/back_24.png')
    }
}

export const getIcon_back_24_white = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/back_24_white.png')
        default:
            return require('./icon/back_24_white.png')
    }
}

export const getIcon_companyLogo = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/company_logo.png')
        default:
            return require('./icon/company_logo.png')
    }
}

export const getIcon_companyLogo2 = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/company_logo2.png')
        default:
            return require('./icon/company_logo2.png')
    }
}

export const getIcon_flagWithCode = (code: string) => {
    switch (code) {
        case 'HK':
            return require('./icon/flag_HK.png')
        case 'CN':
            return require('./icon/flag_CN.png')
        case '1':
            return require('./icon/flag/1.png')
        case '2':
            return require('./icon/flag/2.png')
        case '3':
            return require('./icon/flag/3.png')
        case '4':
            return require('./icon/flag/4.png')
        case '5':
            return require('./icon/flag/5.png')
        case '6':
        return require('./icon/flag/6.png')
        case '7':
        return require('./icon/flag/7.png')
        case '8':
        return require('./icon/flag/8.png')
        case '9':
        return require('./icon/flag/9.png')
        case '10':
        return require('./icon/flag/10.png')
        case '11':
        return require('./icon/flag/11.png')
        case '12':
        return require('./icon/flag/12.png')
        case '13':
        return require('./icon/flag/13.png')
        case '14':
        return require('./icon/flag/14.png')
        case '15':
        return require('./icon/flag/15.png')
        case '16':
        return require('./icon/flag/16.png')
        case '17':
        return require('./icon/flag/17.png')
        case '18':
        return require('./icon/flag/18.png')
        case '19':
        return require('./icon/flag/19.png')
        case '20':
        return require('./icon/flag/20.png')
        case '21':
        return require('./icon/flag/21.png')
        case '22':
        return require('./icon/flag/22.png')
        case '23':
        return require('./icon/flag/23.png')
        case '24':
        return require('./icon/flag/24.png')
        case '25':
        return require('./icon/flag/25.png')
        case '26':
        return require('./icon/flag/26.png')
        case '27':
        return require('./icon/flag/27.png')
        case '28':
        return require('./icon/flag/28.png')
        case '29':
        return require('./icon/flag/29.png')
        case '30':
        return require('./icon/flag/30.png')
        case '31':
        return require('./icon/flag/31.png')
        case '32':
        return require('./icon/flag/32.png')
        case '33':
        return require('./icon/flag/33.png')
        case '34':
        return require('./icon/flag/34.png')
        case '35':
        return require('./icon/flag/35.png')
        case '36':
        return require('./icon/flag/36.png')
        case '37':
        return require('./icon/flag/37.png')
        case '38':
        return require('./icon/flag/38.png')
        case '39':
        return require('./icon/flag/39.png')
        case '40':
        return require('./icon/flag/40.png')
        case '41':
        return require('./icon/flag/41.png')
        case '42':
        return require('./icon/flag/42.png')
        case '43':
        return require('./icon/flag/43.png')
        case '44':
        return require('./icon/flag/44.png')
        case '45':
        return require('./icon/flag/45.png')
        case '46':
        return require('./icon/flag/46.png')
        case '47':
        return require('./icon/flag/47.png')
        case '48':
        return require('./icon/flag/48.png')
        case '49':
        return require('./icon/flag/49.png')
        case '50':
        return require('./icon/flag/50.png')
        case '51':
        return require('./icon/flag/51.png')
        case '52':
        return require('./icon/flag/52.png')
        case '53':
        return require('./icon/flag/53.png')
        case '54':
        return require('./icon/flag/54.png')
        case '55':
        return require('./icon/flag/55.png')
        case '56':
        return require('./icon/flag/56.png')
        case '57':
        return require('./icon/flag/57.png')
        case '58':
        return require('./icon/flag/58.png')
        case '59':
        return require('./icon/flag/59.png')
        case '60':
        return require('./icon/flag/60.png')
        case '61':
        return require('./icon/flag/61.png')
        case '62':
        return require('./icon/flag/62.png')
        case '63':
        return require('./icon/flag/63.png')
        case '64':
        return require('./icon/flag/64.png')
        case '65':
        return require('./icon/flag/65.png')
        case '66':
        return require('./icon/flag/66.png')
        case '67':
        return require('./icon/flag/67.png')
        case '68':
        return require('./icon/flag/68.png')
        case '69':
        return require('./icon/flag/69.png')
        case '70':
        return require('./icon/flag/70.png')
        case '71':
        return require('./icon/flag/71.png')
        case '72':
        return require('./icon/flag/72.png')
        case '73':
        return require('./icon/flag/73.png')
        case '74':
        return require('./icon/flag/74.png')
        case '75':
        return require('./icon/flag/75.png')
        case '76':
        return require('./icon/flag/76.png')
        case '77':
        return require('./icon/flag/77.png')
        case '78':
        return require('./icon/flag/78.png')
        case '79':
        return require('./icon/flag/79.png')
        case '80':
        return require('./icon/flag/80.png')
        case '81':
        return require('./icon/flag/81.png')
        case '82':
        return require('./icon/flag/82.png')
        case '83':
        return require('./icon/flag/83.png')
        case '84':
        return require('./icon/flag/84.png')
        case '85':
        return require('./icon/flag/85.png')
        case '86':
        return require('./icon/flag/86.png')
        case '87':
        return require('./icon/flag/87.png')
        case '88':
        return require('./icon/flag/88.png')
        case '89':
        return require('./icon/flag/89.png')
        case '90':
        return require('./icon/flag/90.png')
        case '91':
        return require('./icon/flag/91.png')
        case '92':
        return require('./icon/flag/92.png')
        case '93':
        return require('./icon/flag/93.png')
        case '94':
        return require('./icon/flag/94.png')
        case '95':
        return require('./icon/flag/95.png')
        case '96':
        return require('./icon/flag/96.png')
        case '97':
        return require('./icon/flag/97.png')
        case '98':
        return require('./icon/flag/98.png')
        case '99':
        return require('./icon/flag/99.png')
        case '100':
        return require('./icon/flag/100.png')
        case '101':
        return require('./icon/flag/101.png')
        case '102':
        return require('./icon/flag/102.png')
        case '103':
        return require('./icon/flag/103.png')
        case '104':
        return require('./icon/flag/104.png')
        case '105':
        return require('./icon/flag/105.png')
        case '106':
        return require('./icon/flag/106.png')
        case '107':
        return require('./icon/flag/107.png')
        case '108':
        return require('./icon/flag/108.png')
        case '109':
        return require('./icon/flag/109.png')
        case '110':
        return require('./icon/flag/110.png')
        case '111':
        return require('./icon/flag/111.png')
        case '112':
        return require('./icon/flag/112.png')
        case '113':
        return require('./icon/flag/113.png')
        case '114':
        return require('./icon/flag/114.png')
        case '115':
        return require('./icon/flag/115.png')
        case '116':
        return require('./icon/flag/116.png')
        case '117':
        return require('./icon/flag/117.png')
        case '118':
        return require('./icon/flag/118.png')
        case '119':
        return require('./icon/flag/119.png')
        case '120':
        return require('./icon/flag/120.png')
        case '121':
        return require('./icon/flag/121.png')
        case '122':
        return require('./icon/flag/122.png')
        case '123':
        return require('./icon/flag/123.png')
        case '124':
        return require('./icon/flag/124.png')
        case '125':
        return require('./icon/flag/125.png')
        case '126':
        return require('./icon/flag/126.png')
        case '127':
        return require('./icon/flag/127.png')
        case '128':
        return require('./icon/flag/128.png')
        case '129':
        return require('./icon/flag/129.png')
        case '130':
        return require('./icon/flag/130.png')
        case '131':
        return require('./icon/flag/131.png')
        case '132':
        return require('./icon/flag/132.png')
        case '133':
        return require('./icon/flag/133.png')
        case '134':
        return require('./icon/flag/134.png')
        case '135':
        return require('./icon/flag/135.png')
        case '136':
        return require('./icon/flag/136.png')
        case '137':
        return require('./icon/flag/137.png')
        case '138':
        return require('./icon/flag/138.png')
        case '139':
        return require('./icon/flag/139.png')
        case '140':
        return require('./icon/flag/140.png')
        case '141':
        return require('./icon/flag/141.png')
        case '142':
        return require('./icon/flag/142.png')
        case '143':
        return require('./icon/flag/143.png')
        case '144':
        return require('./icon/flag/144.png')
        case '145':
        return require('./icon/flag/145.png')
        case '146':
        return require('./icon/flag/146.png')
        case '147':
        return require('./icon/flag/147.png')
        case '148':
        return require('./icon/flag/148.png')
        case '149':
        return require('./icon/flag/149.png')
        case '150':
        return require('./icon/flag/150.png')
        case '151':
        return require('./icon/flag/151.png')
        case '152':
        return require('./icon/flag/152.png')
        case '153':
        return require('./icon/flag/153.png')
        case '154':
        return require('./icon/flag/154.png')
        case '155':
        return require('./icon/flag/155.png')
        case '156':
        return require('./icon/flag/156.png')
        case '157':
        return require('./icon/flag/157.png')
        case '158':
        return require('./icon/flag/158.png')
        case '159':
        return require('./icon/flag/159.png')
        case '160':
        return require('./icon/flag/160.png')
        case '161':
        return require('./icon/flag/161.png')
        case '162':
        return require('./icon/flag/162.png')
        case '163':
        return require('./icon/flag/163.png')
        case '164':
        return require('./icon/flag/164.png')
        case '165':
        return require('./icon/flag/165.png')
        case '166':
        return require('./icon/flag/166.png')
        case '167':
        return require('./icon/flag/167.png')
        case '168':
        return require('./icon/flag/168.png')
        case '169':
        return require('./icon/flag/169.png')
        case '170':
        return require('./icon/flag/170.png')
        case '171':
        return require('./icon/flag/171.png')
        case '172':
        return require('./icon/flag/172.png')
        case '173':
        return require('./icon/flag/173.png')
        case '174':
        return require('./icon/flag/174.png')
        case '175':
        return require('./icon/flag/175.png')
        case '176':
        return require('./icon/flag/176.png')
        case '177':
        return require('./icon/flag/177.png')
        case '178':
        return require('./icon/flag/178.png')
        case '179':
        return require('./icon/flag/179.png')
        case '180':
        return require('./icon/flag/180.png')
        case '181':
        return require('./icon/flag/181.png')
        case '182':
        return require('./icon/flag/182.png')
        case '183':
        return require('./icon/flag/183.png')
        case '184':
        return require('./icon/flag/184.png')
        case '185':
        return require('./icon/flag/185.png')
        case '186':
        return require('./icon/flag/186.png')
        case '187':
        return require('./icon/flag/187.png')
        case '188':
        return require('./icon/flag/188.png')
        case '189':
        return require('./icon/flag/189.png')
        case '190':
        return require('./icon/flag/190.png')
        case '191':
        return require('./icon/flag/191.png')
        case '192':
        return require('./icon/flag/192.png')
        case '193':
        return require('./icon/flag/193.png')
        case '194':
        return require('./icon/flag/194.png')
        case '195':
        return require('./icon/flag/195.png')
        case '196':
        return require('./icon/flag/196.png')
        default:
            return require('./icon/flag_HK.png')
    }
}

export const getImage_intro = (code: number, language: string) => {
    switch (code) {
        case 0:
            if (language === 'tc')
                return require('./images/intro_01_tc.png')
            else if (language === 'sc')
                return require('./images/intro_01_sc.png')
            else
                return require('./images/intro_01.png')
        case 1:
            if (language === 'tc')
                return require('./images/intro_02_tc.png')
            else if (language === 'sc')
                return require('./images/intro_02_sc.png')
            else
                return require('./images/intro_02.png')
        case 2:
            if (language === 'tc')
                return require('./images/intro_03_tc.png')
            else if (language === 'sc')
                return require('./images/intro_03_sc.png')
            else
                return require('./images/intro_03.png')
        default:
            return require('./images/intro_03.png')
    }
}

export const getIcon_whatsapp = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/whatsapp.png')
        default:
            return require('./icon/whatsapp.png')
    }
}

export const getImage_loyaltybackground = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./images/loyalty_background.png')
        default:
            return require('./images/loyalty_background.png')
    }
}

export const getIcon_next_tight = (theme: string | null | undefined) => {
    switch (theme) {
        case 'dark':
            return require('./icon/next_tight.png')
        default:
            return require('./icon/next_tight.png')
    }
}